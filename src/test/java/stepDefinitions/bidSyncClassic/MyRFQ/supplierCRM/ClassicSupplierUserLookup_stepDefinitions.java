package stepDefinitions.bidSyncClassic.MyRFQ.supplierCRM;

import core.CoreAutomation;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.common.helpers.GeographyHelpers;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.helpers.StringHelpers;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utility.database.SQLConnector;

public class ClassicSupplierUserLookup_stepDefinitions {

	private EventFiringWebDriver driver;
	private StringHelpers stringHelpers;
	private GeographyHelpers geographyHelpers;
	private HelperMethods helperMethods;

	public ClassicSupplierUserLookup_stepDefinitions(CoreAutomation automation) {
		this.driver      = automation.getDriver();
		stringHelpers    = new StringHelpers();
		geographyHelpers = new GeographyHelpers();
		helperMethods = new HelperMethods();
	}

	/* ----- helpers ----- */

	private String getSourceIdForAccount(String userEmail) {
	    String query = String.format("SELECT * FROM public.users_link where user_name='%s'", userEmail);
	    System.out.printf(GlobalVariables.getTestID() + " Running query on auth: %s", query);
	    String sourceId = "";
        try {
            ResultSet rs = SQLConnector.executeQuery("auth", query);
            String message = String.format("No link found for user_name '%s'", userEmail);
            Assert.assertTrue(message, rs.next());
            sourceId = rs.getString("source_id");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            Assert.fail("Exception occurred while querying the auth table.");
        }
        return sourceId;
    }

	private void iTypeIntoTheEmailFieldInTheUserSearchPane(String arg1) {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(1);
		driver.switchTo().frame(1);
		driver.findElement(By.xpath("//input[@name='semail']")).sendKeys(arg1);
	}
	/* ------------ Test Steps ---------------- */

	@And("^I click the Users link at the bottom of the Supplier Edit page$")
	public void iClickTheUsersLinkAtTheBottomOfTheSupplierEditPage() {
		driver.switchTo().defaultContent();
		driver.switchTo().frame(1);
		driver.switchTo().frame(2);
		driver.findElement(By.xpath("//a[contains(.,'Users')]")).click();
	}
	
	@And("^I type the random email into the Email field in the User Search pane$")
	public void iTypeTheRandomEmailIntoTheEmailFieldInTheUserSearchPane() {
        RandomChargebeeUser storedUser = GlobalVariables.getStoredObject("StoredUser");
		iTypeIntoTheEmailFieldInTheUserSearchPane(storedUser.getEmailAddress());
	}
	
	@And("^I click the Find button in the User Search pane$")
	public void iClickTheFindButtonInTheUserSearchPane() {
		driver.findElement(By.xpath("//a[contains(.,'Find')]")).click();
	}
	
	@And("^I click the Edit button for the vendor user in the User Search pane$")
	public void iClickTheEditButtonForTheVendorUserInTheUserSearchPane() {
		helperMethods.addSystemWait(5);  // refresh issue causing stale element
		driver.findElement(By.xpath("//a[contains(.,'Edit')]")).click();
	}

	@Then("^the vendor user edit pane should appear without error$")
	public void theVendorUserEditPaneShouldAppearWithoutError() {
		// if this element can be found, the frame isn't filled with error 500		
		WebDriverWait wait = new WebDriverWait(driver, 5, 200);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='status']")));		
	}
	
	@Then("^I will validate the Company and User fields of the Supplier Edit page hold the correct information$")
	public void iWillValidateTheCompanyAndUserFieldsOfTheSupplierEditPageHoldTheCorrectInformation() {
		// validate Company information
		driver.switchTo().defaultContent();
		driver.switchTo().frame(1);
		driver.switchTo().frame(0);
		
		// putting in a bunch of print statements so if an assert fails we know exactly where to look
        
        RandomChargebeeUser storedUser = GlobalVariables.getStoredObject("StoredUser");
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert company name = %s\n", storedUser.getCompany());
		String strCompanyName = driver.findElement(By.xpath("//input[@name='companyname']")).getAttribute("value");
		assertEquals(storedUser.getCompany().getName(), strCompanyName);
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert company address = %s\n", storedUser.getAddress());
		String strCompanyAddress = driver.findElement(By.xpath("//input[@name='addr1_company']")).getAttribute("value");
		assertEquals(storedUser.getAddress(), strCompanyAddress);
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert company city = %s\n", storedUser.getCity());
		String strCompanyCity = driver.findElement(By.xpath("//input[@name='city_company']")).getAttribute("value");
		assertEquals(storedUser.getCity(), strCompanyCity);
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert company state = %s\n", storedUser.getState());
		Select stateSelect = new Select(driver.findElement(By.xpath("//select[@name='state_company']")));
		String strCompanyState = stateSelect.getFirstSelectedOption().getText();
		assertEquals(storedUser.getState().trim(), strCompanyState.trim());
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert company zip = %s\n", storedUser.getZipCode());
		String strCompanyZip = driver.findElement(By.xpath("//input[@name='zip_company']")).getAttribute("value");
		assertEquals(storedUser.getZipCode(), strCompanyZip);
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert contact first name = %s\n", storedUser.getFirstName());
		String strFirstName = driver.findElement(By.xpath("//input[@name='fname']")).getAttribute("value");
		assertEquals(storedUser.getFirstName(), strFirstName);
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert contact last name = %s\n", storedUser.getLastName());
		String strLastName = driver.findElement(By.xpath("//input[@name='lname']")).getAttribute("value");
		assertEquals(storedUser.getLastName(), strLastName);
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert company Contact first/last = %s\n", storedUser.getFirstName() + " " + storedUser.getLastName());
		String strContactName = driver.findElement(By.xpath("//input[@name='selectedcontactfullname']")).getAttribute("value");
		assertEquals(storedUser.getFirstName() + " " + storedUser.getLastName(), strContactName);
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert company contact email = %s\n", storedUser.getEmailAddress());
		String strEmail = driver.findElement(By.xpath("//input[@name='email']")).getAttribute("value");
		assertEquals(storedUser.getEmailAddress(), strEmail);
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert company Contact phone = %s\n", storedUser.getPhoneNumber());
		String strPhone = driver.findElement(By.xpath("//input[@name='phone']")).getAttribute("value");
		assertEquals(stringHelpers.removeWhitespaceAndSpecialCharsFromString(storedUser.getPhoneNumber()),
				stringHelpers.removeWhitespaceAndSpecialCharsFromString(strPhone));
		
		// STATUS
        System.out.print("INFO: assert company status = Active\n");
		assertEquals( "1", driver.findElement(By.xpath("//input[@name='status']")).getAttribute("value"));
		
		
		// validate User information
		driver.switchTo().defaultContent();
		driver.switchTo().frame(1);
		driver.switchTo().frame(1);
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert user first name = %s\n", storedUser.getFirstName());
		strFirstName = driver.findElement(By.xpath("//input[@name='fname']")).getAttribute("value");
		assertEquals(storedUser.getFirstName(), strFirstName);
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert contact last name = %s\n", storedUser.getLastName());
		strLastName = driver.findElement(By.xpath("//input[@name='lname']")).getAttribute("value");
		assertEquals(storedUser.getLastName(), strLastName);
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert company contact email = %s\n", storedUser.getEmailAddress());
		strEmail = driver.findElement(By.xpath("//input[@name='email']")).getAttribute("value");
		assertEquals(storedUser.getEmailAddress(), strEmail);
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert user phone = %s\n", storedUser.getPhoneNumber());
		strPhone = driver.findElement(By.xpath("//input[@name='phone']")).getAttribute("value");
		assertEquals(stringHelpers.removeWhitespaceAndSpecialCharsFromString(storedUser.getPhoneNumber()),
				stringHelpers.removeWhitespaceAndSpecialCharsFromString(strPhone));
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert user address = %s\n", storedUser.getAddress());
		strCompanyAddress = driver.findElement(By.xpath("//input[@name='addr1']")).getAttribute("value");
		assertEquals(storedUser.getAddress(), strCompanyAddress);
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert user city = %s\n", storedUser.getCity());
		strCompanyCity = driver.findElement(By.xpath("//input[@name='city']")).getAttribute("value");
		assertEquals(storedUser.getCity(), strCompanyCity);
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert user state = %s\n", storedUser.getState());
		strCompanyState = driver.findElement(By.xpath("//select[@name='state']")).getAttribute("value");
		assertEquals(storedUser.getState(), geographyHelpers.getStateOrProvinceAbbreviationFromStateName(strCompanyState));
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: assert user zip = %s\n", storedUser.getZipCode());
		strCompanyZip = driver.findElement(By.xpath("//input[@name='zip']")).getAttribute("value");
		assertEquals(storedUser.getZipCode(), strCompanyZip);
		
		
		// STATUS
        System.out.print("INFO: assert user status = Active\n");
		assertEquals( "1", driver.findElement(By.xpath("//input[@name='status']")).getAttribute("value"));
		
		// PRIVS
        System.out.print("INFO: assert Create invoice priv is on\n");
		assertEquals( "on", driver.findElement(By.xpath("//input[@name='priv_150']")).getAttribute("value"));
        
        System.out.print("INFO: assert Edit purchase order priv is on\n");
		assertEquals( "on", driver.findElement(By.xpath("//input[@name='priv_149']")).getAttribute("value"));
        
        System.out.print("INFO: assert Manage catalogs priv is on\n");
		assertEquals( "on", driver.findElement(By.xpath("//input[@name='priv_156']")).getAttribute("value"));
        
        System.out.print("INFO: assert View invoices priv is on\n");
		assertEquals( "on", driver.findElement(By.xpath("//input[@name='priv_15']")).getAttribute("value"));
        
        System.out.print("INFO: assert Edit Organization Information from BidSync priv is on\n");
		assertEquals( "on", driver.findElement(By.xpath("//input[@name='priv_17']")).getAttribute("value"));
        
        System.out.print("INFO: assert Invoice notifications priv is on\n");
		assertEquals( "on", driver.findElement(By.xpath("//input[@name='priv_151']")).getAttribute("value"));
        
        System.out.print("INFO: assert Respond to Bid priv is on\n");
		assertEquals( "on", driver.findElement(By.xpath("//input[@name='priv_18']")).getAttribute("value"));
	}
	
   
    @And("^I will verify that the Pro and Classic accounts are linked$")
    public void iWillVerifyThatTheProAndClassicAccountsAreLinked() {
	    RandomChargebeeUser storedUser = GlobalVariables.getStoredObject("StoredUser");
	    String userEmail = storedUser.getEmailAddress();
	    String sourceIdThatIndicatesALink = "8b2469b7-4185-4340-b9c5-144748870147";
	    String accountSourceId = getSourceIdForAccount(userEmail);
	    String message = "Account's source id does not indicate a link.";
        Assert.assertEquals(message, sourceIdThatIndicatesALink, accountSourceId);
    }
    
}
