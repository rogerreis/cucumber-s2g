/**
 * 
 */
package pages.bidSyncPro.supplier.companySettings.agencyInteractions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.companySettings.CompanySettingsCommon;
import pages.bidSyncPro.supplier.companySettings.manageUsers.WaitingForApproval;

/**
 * <h1>Class AgencyInteraction</h1>
 * @author dmidura
 * <p>details: This class holds page objects and methods to manipulate the Agency Interaction page that is visible at Company Settings > Agency Interaction.
 * NOTE: When a user has selected to join a company, but has not yet been approved by the Company Owner, then this page will be blank for the joining user.
 */
public class AgencyInteraction extends CompanySettingsCommon{
    
    // xpaths
    private final String manageQualificationsTab_xpath  = "//div[@class='mat-tab-label-content']//span[text()='manage qualifications']";
    private final String companyOrdersTab_xpath         = "//div[@class='mat-tab-label-content']//span[text()='company orders']";
    private final String companyContractsTab_xpath      = "//div[@class='mat-tab-label-content']//span[text()='company contracts']";
    private final String searchPublicContractsTab_xpath = "//div[@class='mat-tab-label-content']//span[text()='search public contracts']";

	public AgencyInteraction(EventFiringWebDriver driver) {
		super(driver);
	}
	
	/* ----- getters ----- */
	private WebElement  getManageQualificationsTab()  {return findByVisibility(By.xpath(this.manageQualificationsTab_xpath));  }
	private WebElement  getCompanyOrdersTab()         {return findByVisibility(By.xpath(this.companyOrdersTab_xpath));         }
	private WebElement  getCompanyContracts()         {return findByVisibility(By.xpath(this.companyContractsTab_xpath));      }
	private WebElement  getSearchPublicContractsTab() {return findByVisibility(By.xpath(this.searchPublicContractsTab_xpath)); }
    
	/* ----- methods ----- */
	
	/***
	 * <h1>clickManageQualificationsTab</h1>
	 * <p>purpose: Click on "Manage Qualifications" tab
	 * @return AgencyInteraction
	 */
	public AgencyInteraction clickManageQualificationsTab() {
		this.getManageQualificationsTab().click();
		return this; // TODO: Update return after Manage Qualifications refactor
	}

	/***
	 * <h1>clickCompanyOrdersTab</h1>
	 * <p>purpose: Click on "Company Orders" tab
	 * @return CompanyOrders
	 */
	public CompanyOrders clickCompanyOrdersTab() {
		this.getCompanyOrdersTab().click();
		return new CompanyOrders(driver);
	}

	/***
	 * <h1>clickCompanyContractsTab</h1>
	 * <p>purpose: Click on "Company Contracts" tab
	 * @return AgencyInteraction
	 */
	public AgencyInteraction clickCompanyContractsTab() {
		this.getCompanyContracts().click();
		return this; // TODO: Update return after Company Contracts refactor
	}
	
	/***
	 * <h1>clickSearchPublicContractsTab</h1>
	 * <p>purpose: Click on "Search Public Contracts" tab
	 * @return AgencyInteraction
	 */
	public AgencyInteraction clickSearchPublicContractsTab() {
		this.getSearchPublicContractsTab().click();
		return this; // TODO: Update return after SearchPublicContracts refactor
	}
	
	/*** 
	 * <h1>isTheUserAllowedToViewThisPage()</h1>
	 * <p>purpose: Return the user's current viewing status of this page<br>
	 * (When user has tried to join a company, but is waiting on approval from a company owner, then the
	 * user should not be able to view this page. When the user has joined the company, then the user should
	 * be able to view this page).
	 * @return true if user is currently viewing page | false if user is not currently viewing the page
	 */
    public boolean isTheUserAllowedToViewThisPage() {
        return new WaitingForApproval(driver).areWeHere();
    }
}
