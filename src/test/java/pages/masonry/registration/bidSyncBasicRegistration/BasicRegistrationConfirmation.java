package pages.masonry.registration.bidSyncBasicRegistration;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.masonry.registration.CommonTopBar;

/***
 * <h1>Class BasicRegistrationConfirmation</h1>
 * @author dmidura
 * <p>details: This class contains page objects and methods for the BidSync Basic Registration
 * Confirmation screen that appears after a user successfully completes Basic registration</p>
 */
public class BasicRegistrationConfirmation extends CommonTopBar {
	
	// xpaths
	private final String thankYouTxt_xpath   = "//h2[@class='mb-30'][contains(.,'Thank you')]";
	private final String checkEmailTxt_xpath = "//p[contains(., \"Please\")][contains(.., \"email address\")]";

	public BasicRegistrationConfirmation(EventFiringWebDriver driver) {
		super(driver);
	}
	
	// getters
	private WebElement getThankYouTxt()   { return this.findByVisibility(By.xpath(this.thankYouTxt_xpath));   }
	private WebElement getCheckEmailTxt() { return this.findByVisibility(By.xpath(this.checkEmailTxt_xpath)); }
	
	/* ----- verifications ----- */
	
	/***
	 * <h1>verifyConfirmationMessage</h1>
	 * <p>purpose: Verify that confirmation message displays per:<br>
	 * Thank you, [firstName], for ordering BidSync Basic<br>
	 * Please check [email] to verify your email address<br>
	 * Fail test if message does not display as above 
	 * @param firstName = first name user registered with
	 * @param email = email address user registered with
	 * @return BasicRegistration Confirmation
	 */
	public BasicRegistrationConfirmation verifyConfirmationMessage(String firstName, String email) {
		String expectedThankYou = String.format("Thank you, %s, for ordering BidSync Basic", firstName);
		String expectedEmail    = String.format("Please check %s to verify your email address", email);
		
		String actualThankYou   = this.getThankYouTxt().getText();
		String actualEmail      = this.getCheckEmailTxt().getText();
		
		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(actualThankYou).isEqualTo(expectedThankYou);
		softly.assertThat(actualEmail).isEqualTo(expectedEmail);
		softly.assertAll();

		return this;
	}
}
