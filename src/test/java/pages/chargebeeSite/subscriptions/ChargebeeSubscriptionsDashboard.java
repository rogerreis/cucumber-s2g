package pages.chargebeeSite.subscriptions;

import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.chargebeeSite.common.CommonNavSliderMenu;
import pages.chargebeeSite.common.GenericChargebeeSearch;

/***
 * <h1>ChargebeeSubscriptionsDashboard</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects on the "Subscriptions" dashboard home<br>
 * 			   on the Chargebee test site. Note: SubscriptionsTable class holds the actual subscriptions in the table</p>
 */
public class ChargebeeSubscriptionsDashboard extends CommonNavSliderMenu {
	
	//Other 
	private EventFiringWebDriver driver;
	private String strPagePath = System.getProperty("chargebeeTestSiteURL") + "subscriptions";
	
	public ChargebeeSubscriptionsDashboard (EventFiringWebDriver driver) {
		super(driver);
		this.driver 			= driver;
	}
	
	/* ----- methods ----- */
	
	/***
	 * <h1>navigateToHere</h1>
	 * <p>purpose: Navigate directly to the Chargebee Subscriptions Dashboard<br>
	 * 	Useful way to return to Chargebee if you've navigated the browser away during a test</p>
	 * @return ChargebeeSubscriptionsDashboard
	 */
	public ChargebeeSubscriptionsDashboard navigateToHere() {
		driver.navigate().to(this.strPagePath);
		return this;
	}

	/***
	 * <h1>getSubscriptionsTable</h1>
	 * <p>purpose: Access to the subscriptions table
	 * @return SubscriptionsTable
	 */
	public SubscriptionsTable getSubscriptionsTable() {
		return new SubscriptionsTable(driver);
	}
	
	/***
	 * <h1>getSearchBar</h1>
	 * <p>purpose: Access to the Generic Chargebee Search bar (above table)</p>
	 * @return GenericChargebeeSearch
	 */
	public GenericChargebeeSearch getSearchBar() {
		return new GenericChargebeeSearch(driver);
	}
	
	/* ----- verifications ----- */

	/***
	 * <h1>verifyPageLoad</p>
	 * <p>purpose: Verify that Subscriptions screen has loaded. Fail test if subscriptions hasn't loaded</p>
	 * @return ChargebeeSubscriptionsDashboardCustomers 
	 */
	public ChargebeeSubscriptionsDashboard verifyPageLoad() {
		int time = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, time)
			.withMessage("Test Failed: Did not navigate to the subscriptions dashboard after " + time + "seconds")
			.until(ExpectedConditions.urlToBe(this.strPagePath));
		return this;
	}	
}
