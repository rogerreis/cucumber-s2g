package pages.bidSyncPro.supplier.emails;

import org.openqa.selenium.support.events.EventFiringWebDriver;


public class RegistrationCompleteEmail extends GenericEmail {
	
	public RegistrationCompleteEmail(EventFiringWebDriver driver) {
		super(driver, "RegistrationEmail.json");
	}
}