package pages.common.helpers;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import io.cucumber.datatable.DataTable;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/***
 * <h1>Helper Methods</h1>
 * @author dmidura
 * <p>details: This class houses helper methods.</p>
 * <p>TODO: Currently dividing this class up into pageObjects, registrationHelpers (for only bidsync pages),
 * common bidsync pro helpers (for only bidsync pages), string helpers, etc. pageObjects will be inheritable
 * from top level, all other classes should be called</p>
 */
public class HelperMethods {

	public HelperMethods(){
	}

	/***
	 * <h1>addSystemWait</h1>
	 * <p>purpose: Have the automation wait a specified time. <br>
	 *    (This is useful if we need ARGO to catch up before a test step can be run) </p>
	 * @param lWaitTime = the wait time (in seconds) for the system to wait <br>
	 *        Note: Suggested wait time is 3 seconds or less
	 * @return None
	 */
	public void addSystemWait (long lWaitTime) {
		try {
			System.out.printf(GlobalVariables.getTestID() + " INFO: System is waiting %d seconds for ARGO to catch up...\n", lWaitTime);
			TimeUnit.SECONDS.sleep(lWaitTime);
		} catch (InterruptedException e) {
			System.err.println("InterruptedException" + e.getMessage());
		}	
	}

	/***
	 * <h1>addSystemWait</h1>
	 * <p>purpose: Have the automation wait a specified time. <br>
	 *    (This is useful if we need ARGO to catch up before a test step can be run) </p>
	 * @param lWaitTime = the wait time for the system to wait <br>
	 * @param timeUnit = the time unit describing lWaitTime<br>
	 * 		= "MINUTES" | "SECONDS" | "MILISECONDS" | "MICROSECONDS"</p> 
	 *        Note: Suggested wait time is 3 seconds or less
	 * @return None
	 */
	public void addSystemWait (long lWaitTime, TimeUnit timeUnit) {

		try {
			System.out.printf(GlobalVariables.getTestID() + " INFO: System is waiting %o %s for ARGO to catch up...\n", lWaitTime, timeUnit.toString());
			timeUnit.sleep(lWaitTime);
		} catch (InterruptedException e)
		{
			System.err.println("InterruptedException" + e.getMessage());
		}	

	}

	/***
	 * <h1>putDataTableIntoMap</h1>
	 * <p>purpose: Given Selenium dataTable, return as a Map                </p>
	 * 
	 * @param dTable = Selenium datatable (raw) in the format of            <br>
	 *        | key1 | value11 | value12 | ... | value1x |          		<br>
	 *        | key2 | value21 | value22 | ... | value2x |         			<br>
	 *        .                                                             <br>
	 *        .                                                             <br>
	 *        .                                                             <br>
	 *        | keyX | valueX1 | valueX2 | ... | valueXx |                  <br>
	 *        where keys are required, but values may be blank				<br> 
	 * @return a HashMap where            									<br>
	 *        keys = key1 , key2 ... keyX			                        <br>
	 *        are mapped to the values                                      <br>
	 *        ArrayList for key1 = (value11, value12, ..., value11) 		<br>
	 *        ArrayList for key2 = (value21, value22, ..., value22) 		<br>
	 *        .                                                             <br>
	 *        .                                                             <br>
	 *        .                                                             <br>
	 *        ArrayList for keyX = (valueX1, valueX2, ..., valueXx) 		<br>
	 */
	public Map <String, List<String>> putDataTableIntoMap (DataTable dTable) {
		Map <String, List <String>> inputsMap = new HashMap<>();

		// Grab everything out of the dataTable
		List <List <String>> inputsList = dTable.asLists();
		int intDataTableSize            = inputsList.get(0).size();

		// And save it out to use
		for (int i = 0; i<inputsList.size(); i++) {
			inputsMap.put(inputsList.get(i).get(0), new ArrayList<>(inputsList.get(i).subList(1, intDataTableSize)));
			
			// Get rid of any blanks entries that were inputed via dataTable
			inputsMap.get(inputsList.get(i).get(0)).removeIf(x->x.isEmpty());
		}	
		
		System.out.println(GlobalVariables.getTestID() + " INFO: Extracted data from DataTable and returning as Map");
		return inputsMap;
	}

	 
	/* ----- obsolete (to be deleted after pulled out of all existing code) ----- */
	
	/***
	 * <h1>generateNewEmail</h1>
	 * <p>purpose: Create a randomized-ish new email. </p>
	 * @return String = random email for mailinator<br>
	 * 	Ex: <randomString>@phimail.mailinator.com
	 * @deprecated - use RandomHelpers
	 */
	public String generateNewEmail () {
		String[] rand        = UUID.randomUUID().toString().split("-");
		String strNewEmail   =  rand[1] + rand[2] + "@phimail.mailinator.com";
		System.out.printf(GlobalVariables.getTestID() + " Created random email %s\n", strNewEmail);
		return strNewEmail;
	}	

    /***
     * @deprecated - use BasePageActions class
     */
    public void waitForVisibilityOfXpath(WebDriver driver, String xpath, int timeout) {
	    new WebDriverWait(driver, timeout)
        		.withMessage("Test Timed out: Object was not visible after " + timeout + " seconds\n" +
        					  "object xpath = \"" + xpath + "\"")
	    		.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
    }
    
    /***
     * @deprecated - use BasePageActions class
     */
    public void waitForVisibilityOfId(EventFiringWebDriver driver, String id, int timeout) {
        new WebDriverWait(driver, timeout)
        		.withMessage("Test Timed out: Object was not visible after " + timeout + " seconds\n"+
        					  "object id = \"" + id + "\"")
        		.until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
    }
    
    /***
     * <h1>scrollIdIntoView</h1>
     * <p>purpose: Scroll the screen in order to make an object visible.<br>
     * 	The object will be visible at the TOP of the screen</p>
     * @param driver
     * @param id = id of the object you want to be visible
     * @deprecated - use BasePageActions class
     */
    public void scrollIdIntoView(EventFiringWebDriver driver, String id) {
        WebElement element = driver.findElement(By.id(id));
        driver.executeScript("arguments[0].scrollIntoView(true);", element);
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.id(id)));
    }

}

