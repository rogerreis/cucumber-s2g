package data.bidSyncPro.registration;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import pages.common.helpers.GeographyHelpers;

/***
 * <h1>enum: SubscriptionGrouping</h1>
 * @author dmidura
 * <p>details: This enum defines what a user can select<br>
 * as the grouping for Chargebee registration.<br>
 * National Plans: Grouping = USA<br>
 * Regional Plans: Grouping = REGION_A - REGION_H<br>
 * State Plans: Grouping = AL - WY <br>
 * Basic Plans: Grouping = NONE</p> 
 */
public enum SubscriptionGrouping {
	USA,
	NONE,
	REGION_A,
	REGION_B,
	REGION_C,
	REGION_D,
	REGION_E,
	REGION_F,
	REGION_G,
	REGION_H,
	AL,
	AK,
	AZ,
	AR,
	CA,
	CO,
	CT,
	DE,
	DC,
	FL,
	GA,
	HI,
	ID,
	IL,
	IN,
	IA,
	KS,
	KY,
	LA,
	ME,
	MD,
	MA,
	MI,
	MN,
	MS,
	MO,
	MT,
	NE,
	NV,
	NH,
	NJ,
	NM,
	NY,
	NC,
	ND,
	OH,
	OK,
	OR,
	PA,
	RI,
	SC,
	SD,
	TN,
	TX,
	UT,
	VT,
	VA,
	WA,
	WV,
	WI,
	WY;

	
	private GeographyHelpers geographyHelpers = new GeographyHelpers();

    /***
     * <h1>getChargbeeGroupingName</h1>
     * <p>purpose: Return a string for the grouping that<br>
     * 	displays per Chargebee<br>
     * exs: <br>
     * 	National Plan: "USA"<br>
     * 	Regional Plan: "Region A - West"<br>
     * 	State Plan:    "Texas"</p>
     * @return String
     */
	public String getChargebeeGroupingName() {
		switch(this) {
		case USA:
			return "USA";
		case REGION_A:
			return "Region A - West";
		case REGION_B:
			return "Region B - Mountain West";
		case REGION_C:
			return "Region C - Southwest";
		case REGION_D:
			return "Region D - Great Plains";
		case REGION_E:
			return "Region E - Midwest";
		case REGION_F:
			return "Region F - Southeast";
		case REGION_G:
			return "Region G - Mid-Atlantic";
		case REGION_H:
			return "Region H - Northeast";
		case NONE:
			return "";
		default:
			// This is the state plan -- we are provided the abbreviation, so return the full name
			return geographyHelpers.getStateOrProvinceAbbreviationFromStateName(this.toString());
		}
	}
	
	/***
	 * <h1>getLocationsInMyGrouping</h1>
	 * <p>purpose: Based on the subscription grouping, return all the states in my group<br>
	 * 	"USA"      ---> return all states<br>
	 * 	"REGION_A" ---> WA, OR, CA, HI, AK<br>
	 * 	"REGION_B" ---> MN, ID, WY, NV, UT, CO, AZ, KS, NE<br>
	 * 	"REGION_C" ---> NM, TX, OK, AR, LA, MS<br> 
	 * 	"REGION_D" ---> ND, SD, MN, IA, MO, IL, WI<br>
	 * 	"REGION_E" ---> MI, IN, OH, PA<br> 
	 * 	"REGION_F" ---> FL, AL, GA, TN, SC<br> 
	 * 	"REGION_G" ---> KY, NC, VA, WV, DC, MD, DE, NJ<br>
	 * 	"REGION_H" ---> NY, RI, CT, VT, NH, MA, ME</p>
	 * @return
	 */
	public List<String> getLocationsInMyGrouping(){

		// Define the states in each region
		// based on your current region, return the states in the region
		switch(this) {
		case USA:
			return this.getUSALocations();
		case REGION_A:
			return this.getRegionALocations();
		case REGION_B:
			return this.getRegionBLocations();
		case REGION_C:
			return this.getRegionCLocations();
		case REGION_D:
			return this.getRegionDLocations();
		case REGION_E:
			return this.getRegionELocations();
		case REGION_F:
			return this.getRegionFLocations();
		case REGION_G:
			return this.getRegionGLocations();
		case REGION_H:
			return this.getRegionHLocations();
		case NONE:
			// basic plan doesn't include any locations
			return Arrays.asList("Basic_Only");
		default:
			// This is the state plan -- we are provided the abbreviation, so just return the abbreviation
			return Collections.singletonList(this.toString());
		}
	}

	/***
	 * <h1>getUSALocations</h1>
	 * <p>purpose: Return states in the "USA" national grouping</p>
	 * @return all USA states
	 */
	private List<String> getUSALocations() {
        return Arrays.asList("AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "GA", "HI",
        "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS",
        "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR",
        "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY");
	}

	/***
	 * <h1>getRegionALocations</h1>
	 * <p>purpose: Return states in the "West" region</p>
	 * @return WA, OR, CA, HI, AK
	 */
	private List<String> getRegionALocations() {
		//West
        return Arrays.asList("WA", "OR", "CA", "HI", "AK");
	}

	/***
	 * <h1>getRegionBLocations</h1>
	 * <p>purpose: Return states in the "Mountain West" region</p>
	 * @return MN, ID, WY, NV, UT, CO, AZ, KS, NE
	 */
	private List<String> getRegionBLocations() {
		// Mountain West
        return Arrays.asList("MN", "ID", "WY", "NV", "UT", "CO", "AZ", "KS", "NE");
	}

	/***
	 * <h1>getRegionCLocations</h1>
	 * <p>purpose: Return states in the "Southwest" region</p>
	 * @return NM, TX, OK, AR, LA, MS
	 */
	private List<String> getRegionCLocations() {
		// Southwest
        return Arrays.asList("NM", "TX", "OK", "AR", "LA", "MS");
	}

	/***
	 * <h1>getRegionDLocations</h1>
	 * <p>purpose: Return states in the "Great Plains" region</p>
	 * @return ND, SD, MN, IA, MO, IL, WI
	 */
	private List<String> getRegionDLocations() {
		// Great Plains
        return Arrays.asList("ND", "SD", "MN", "IA", "MO", "IL", "WI");
	}

	/***
	 * <h1>getRegionELocations</h1>
	 * <p>purpose: Return states in the "Midwest" region</p>
	 * @return MI, IN, OH, PA
	 */
	private List<String> getRegionELocations() {
		// Midwest
        return Arrays.asList("MI", "IN", "OH", "PA");
	}

	/***
	 * <h1>getRegionFLocations</h1>
	 * <p>purpose: Return states in the "Southeast" region</p>
	 * @return FL, AL, GA, TN, SC
	 */
	private List<String> getRegionFLocations() {
		// Southeast
        return Arrays.asList("FL", "AL", "GA", "TN", "SC");
	}

	/***
	 * <h1>getRegionGLocations</h1>
	 * <p>purpose: Return states in the "Mid-Atlantic" region</p>
	 * @return KY, NC, VA, WV, DC, MD, DE, NJ
	 */
	private List<String> getRegionGLocations() {
		// Mid-Atlantic 
        return Arrays.asList("KY", "NC", "VA", "WV", "DC", "MD", "DE", "NJ");
	}

	/***
	 * <h1>getRegionHLocations</h1>
	 * <p>purpose: Return states in the "Northeast" region</p>
	 * @return NY, RI, CT, VT, NH, MA, ME
	 */
	private List<String> getRegionHLocations() {
		// North
        return Arrays.asList("NY", "RI", "CT", "VT", "NH", "MA", "ME");
	}

}
