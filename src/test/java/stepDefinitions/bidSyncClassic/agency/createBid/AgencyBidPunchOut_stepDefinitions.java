package stepDefinitions.bidSyncClassic.agency.createBid;

import cucumber.api.java.en.And;
import org.junit.Assert;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidListRow;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

import java.util.List;

/***
 * <p>TODO: refactor this classs</p>
 *
 */
public class AgencyBidPunchOut_stepDefinitions {

	private EventFiringWebDriver driver;
	private DashboardCommon dashboardCommon;
	private HelperMethods helperMethods;
	String new_bid_title = GlobalVariables.getStoredObject("bid_title");

	public AgencyBidPunchOut_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
		this.helperMethods = new HelperMethods();
		dashboardCommon = new DashboardCommon(driver);
	}
		
	@When("^I begin a search$")
	public void I_begin_a_search() {
		System.out.println(GlobalVariables.getTestID() + " Ready to begin search.\n");
		dashboardCommon.searchFor(new_bid_title);
	}
		
	@Then("^I will see my bid in the results")	
	public void I_will_see_my_bid_in_the_results() {
		System.out.println(GlobalVariables.getTestID() + " Select the bid from the results.\n");
        BidList bidList = new BidList(driver);
        List<BidListRow> bids = bidList.allVisibleBids();
        String message = String.format("Zero or multiple bids found for title '%s'", new_bid_title);
        Assert.assertEquals(message, 1, bids.size());
        message = String.format("Bid does not have title '%s'", new_bid_title);
        Assert.assertEquals(message, new_bid_title, bids.get(0).bidInfo.getBidTitle());
		/*
		String myBidXpath = String.format("//*[@id='matAllBidsList']//mat-list-item//a[contains(.,'%s')]",new_bid_title);
		if(driver.findElements(By.xpath(myBidXpath)).size() == 1) {			
			System.out.println(GlobalVariables.getTestID() + " Test passed: Test Bid was found.\n");
			driver.findElement(By.xpath(myBidXpath)).click();
		}else{
            Assert.fail("Test Bid was not found.");
		}
				
		//Wait for iFrame to load with bid details
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(By.linkText("See Bid Details")).click();
		TimeUnit.SECONDS.sleep(10);
		ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles()); 
		driver.switchTo().window(availableWindows.get(1));
		TimeUnit.SECONDS.sleep(10);
				driver.switchTo().window(availableWindows.get(0));		
		driver.get(System.getProperty("supplierBaseURL") + "dashboard/all-bids");
		TimeUnit.SECONDS.sleep(5);
		*/
	}
    
    @And("^I will wait for the bid to sync with Bidsync Pro$")
    public void iWillWaitForTheBidToSyncWithBidsyncPro() {
        System.out.println(GlobalVariables.getTestID() + " Waiting for bid to sync to BidSync Pro. Batch runs every 60 seconds.\n");
        helperMethods.addSystemWait(70);
    }
}

