package pages.bidSyncPro.supplier.account.myInfo;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import pages.bidSyncPro.supplier.account.accountSettings.AccountSettings;
import pages.common.helpers.HelperMethods;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.datatable.DataTable;
import data.bidSyncPro.registration.RandomCompanyInformation;

/***
 * <h1>Class UpdateProfileDialog</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects on the main screen of the Update User Profile Dialog<br>
 * 	that is accessed off of the My Profile (admin/my-profile) page</p>
 */
public class UpdateProfileDialog extends MyInfo {
	
	/* ------ xpaths ------ */
	
	// entire card
	private final String card_xpath           = "//mat-card[@id='matUpdateUserProfileCard']";
	
	// top bar
	private final String closeBtn_xpath       = "//mat-card[@id='matUpdateUserProfileCard']//button";
	
	// form
	private final String firstName_xpath      = "//input[@id='firstName']";
	private final String lastName_xpath       = "//input[@id='lastName']";
	private final String jobTitle_xpath       = "//input[@id='jobTitle']";
	private final String changeEmailBtn_xpath = "//a[contains(text(), 'To change your email visit the Account Settings tab.')]";
	private final String addressLine1_xpath   = "//input[@id='line1']";
	private final String addressLine2_xpath   = "//input[@id='line2']";
	private final String country_xpath        = "//mat-select[@id='country']";
	private final String state_xpath          = "//mat-select[@id='state']";
	private final String city_xpath           = "//input[@id='city']";
	private final String zip_xpath            = "//input[@id='zipcode']";
	private final String phoneNumber_xpath    = "//input[@id='phone']";
	private final String phoneExt_xpath       = "//input[@id='ext']";
	private final String faxNumber_xpath      = "//input[@id='fax']";
	private final String faxExt_xpath         = "//input[@id='faxExt']";
	
	// bottom bar
	private final String companyBtn_xpath     = "//button[@id='companyAction'][contains(span,'COMPANY')]";
	private final String cancelBtn_xpath      = "//button[@id='cancelButton'][contains(span, 'CANCEL')]";
	private final String saveBtn_xpath        = "//button[@id='saveButton'][contains(span, 'SAVE')]";
	
	private final String phoneValidationMsg_xpath = "//*[@id=\"matValidPhoneRequiredError\"]/span[contains(text(),'Invalid Phone Number')]";
	
	// Other
	protected EventFiringWebDriver driver;
	protected HelperMethods helperMethods;
	
	public UpdateProfileDialog(EventFiringWebDriver driver){
	    super(driver);
		this.driver   = driver;
		helperMethods = new HelperMethods();
		// we need to wait a bit for the elements to properly load
        helperMethods.addSystemWait(200, TimeUnit.MILLISECONDS);
	}

	/* -------- getters ------- */
	
	// top bar
	private WebElement getCloseBtn() { return findByVisibility(By.xpath(this.closeBtn_xpath)); }
	
	// form
	private WebElement getFirstName()      { return findByVisibility(By.xpath(this.firstName_xpath));                     }
	private WebElement getLastName()       { return findByVisibility(By.xpath(this.lastName_xpath));                      }
	private WebElement getJobTitle()       { return findByVisibility(By.xpath(this.jobTitle_xpath));                      }
	private WebElement getChangeEmailBtn() { return findByVisibility(By.xpath(this.changeEmailBtn_xpath));                }
	private WebElement getAddressLine1()   { return findByVisibility(By.xpath(this.addressLine1_xpath));                  }
	private WebElement getAddressLine2()   { return findByVisibility(By.xpath(this.addressLine2_xpath));                  }
	private WebElement getCountry()        { return findByVisibility(By.xpath(this.country_xpath));                       }
	private WebElement getState()          { return findByVisibility(By.xpath(this.state_xpath));                         }
	private WebElement getCity()           { return findByVisibility(By.xpath(this.city_xpath));                          }
	private WebElement getZip()            { return findByVisibility(By.xpath(this.zip_xpath));                           }
	private WebElement getPhoneNumber()    { return findByScrollIntoViewBottomOfScreen(By.xpath(this.phoneNumber_xpath)); }
	private WebElement getPhoneExt()       { return findByScrollIntoViewBottomOfScreen(By.xpath(this.phoneExt_xpath));    }
	private WebElement getFaxNumber()      { return findByScrollIntoViewBottomOfScreen(By.xpath(this.faxNumber_xpath));   }
	private WebElement getFaxExt()         { return findByScrollIntoViewBottomOfScreen(By.xpath(this.faxExt_xpath));      }
	
	// bottom bar
	private WebElement getCompanyBtn()     { return findByVisibility(By.xpath(this.companyBtn_xpath));                    }
	private WebElement getCancelBtn()      { return findByVisibility(By.xpath(this.cancelBtn_xpath));                     }
	private WebElement getSaveBtn()        { return findByVisibility(By.xpath(this.saveBtn_xpath));                       }
	
	private WebElement getPhoneValidationMsg(){ return findByVisibility(By.xpath(this.phoneValidationMsg_xpath));                       }
	
	/* ----- setters ----- */
	// form
	private void setFirstName(String strInput)    {this.getFirstName().sendKeys(strInput);          }
	private void setLastName(String strInput)     {this.getLastName().sendKeys(strInput);           }
	private void setJobTitle(String strInput)     {this.getJobTitle().sendKeys(strInput);           } 
	private void setAddressLine1(String strInput) {this.getAddressLine1().sendKeys(strInput);       }
	private void setAddressLine2(String strInput) {this.getAddressLine2().sendKeys(strInput);       } 
	private void setCountry(String strInput)      {
		// Open Country dropdown and select specified country option
		this.getCountry().click();
		findByPresence(By.xpath("//span[@class='mat-option-text']"
				+ "[contains(.,'" + strInput + "')]")).click(); }

	private void setState(String strInput)        {
		// Open State dropdown and select specified state option
		this.getState().click();
		findByPresence(By.xpath("//span[@class='mat-option-text']"
				+ "[contains(.,'" + strInput + "')]")).click(); }

	private void setCity(String strInput)         {this.getCity().sendKeys(strInput);               } 
	private void setZip(String strInput)          {this.getZip().sendKeys(strInput);                } 
	private void setPhoneNumber(String strInput)  {this.getPhoneNumber().sendKeys(strInput);        } 
	private void setPhoneExt(String strInput)     {this.getPhoneExt().sendKeys(strInput);           } 
	private void setFaxNumber(String strInput)    {this.getFaxNumber().sendKeys(strInput);          } 
	private void setFaxExt(String strInput)       {this.getFaxExt().sendKeys(strInput);             } 

	/* ----- methods ----- */
	
	/***
	 * <h1>updateUserProfileFromDataTable</h1>
	 * <p>purpose: Given a DataTable of inputs,<br>
	 * update the user profile information on the<br>
	 * Update User Profile dialog.
	 * @param dTable = DataTable of form:<br>
	 *	| "First Name"       | value | <br>
	 *	| "Last Name"        | value | <br>
	 *	| "Job Title"        | value | <br>
	 *	| "Address Line 1"   | value | <br>
	 *	| "Address Line 2"   | value | <br>
	 *	| "Country"          | value | <br>
	 *	| "State"            | value | <br>
	 *	| "City"             | value | <br>
	 *	| "Zip"              | value | <br>
	 *	| "Phone Number"     | value | <br>
	 *	| "Phone Number Ext" | value | <br>
	 *	| "Fax Number"       | value | <br>
	 *	| "Fax Number Ext"   | value | </p>
	 * @return UpdateProfileDialog
	 */
	public UpdateProfileDialog updateUserProfileFromDataTable(DataTable dTable) {
		
		Map<String, List<String>> InputsMap = helperMethods.putDataTableIntoMap(dTable);
		this.clearAllInputsOnProfile();

		this.setFirstName(InputsMap.get("First Name").get(0));
		this.setLastName(InputsMap.get("Last Name").get(0));
		this.setJobTitle(InputsMap.get("Job Title").get(0));
		this.setAddressLine1(InputsMap.get("Address Line 1").get(0));
		this.setAddressLine2(InputsMap.get("Address Line 2").get(0));
		this.setCountry(InputsMap.get("Country").get(0));
		this.setState(InputsMap.get("State").get(0));
		this.setCity(InputsMap.get("City").get(0));
		this.setZip(InputsMap.get("Zip").get(0));
		if(InputsMap.get("Phone Number").isEmpty())
			this.setPhoneNumber(" ");
		else
			this.setPhoneNumber(InputsMap.get("Phone Number").get(0));
		this.setPhoneExt(InputsMap.get("Phone Ext").get(0));
		this.setFaxNumber(InputsMap.get("Fax Number").get(0));
		this.setFaxExt(InputsMap.get("Fax Ext").get(0));

		return this;
	}
	
	/***
	 * <h1>clearAllInputsOnProfile</h1>
	 * <p>purpose: Clears all the input field values in the profile</p>
	 */
	private void clearAllInputsOnProfile() {
		this.getFirstName().clear();
		this.getLastName().clear();
		this.getJobTitle().clear();
		this.getAddressLine1().clear();
		this.getAddressLine2().clear();
		this.getCity().clear();
		this.getZip().clear();
		this.getPhoneNumber().clear();
		this.getPhoneExt().clear();
		this.getFaxNumber().clear();
		this.getFaxExt().clear();
	}
	
	/***
	 * <h1>clickOnSaveBtn</h1>
	 * <p>purpose: Click on the "SAVE" button on the Update Profile Dialog to save updates to the dialog</p>
	 * @return MyProfile
	 */
	public MyInfo clickOnSaveBtn() {
		this.getSaveBtn().click();
		return new MyInfo(driver);
	}

	/***
	 * <h1>clickOnCancelBtn</h1>
	 * <p>purpose: Click on the "CANCEL" button on the Update Profile Dialog to cancel updates to the dialog and close dialog</p>
	 * @return MyProfile
	 */
	public MyInfo clickOnCancelBtn() {
		this.getCancelBtn().click();
		return new MyInfo(driver);
	}
	
	/***
	 * <h1>closeDialog</h1>
	 * <p>purpose: Click on the "X" on the Update Profile Dialog to close dialog</p>
	 * @return MyProfile
	 */
	public MyInfo closeUserProfileDialog() {
		this.getCloseBtn().click();
		return new MyInfo(driver);
	}
	
	/***
	 * <h1>clickOnChangeEmailBtn</h1>
	 * <p>purpose: Click on the "To change your email visit the my account tab"<br>
	 * 	button on the Update User Profile Dialog. (User should then navigate to My Account tab)</p>
	 * @return MyProfile
	 */
	public AccountSettings clickOnChangeEmailBtn() {
		this.getChangeEmailBtn().click();
		return new AccountSettings(driver);
	}
	
	/***
	 * <h1>clickOnCompanyBtn</h1>
	 * <p>purpose: From the Update User Profile Dialog, click on the<br>
	 * 	COMPANY button to view the Current Company dialog.</p>
	 * @return CurrentCompanyDialog
	 */
    public CurrentCompanyDialog clickOnCompanyBtn() {
		this.getCompanyBtn().click();
		return new CurrentCompanyDialog(driver);
	}
    
    /***
     * <h1>changeCompany</h1>
     * <p>purpose: From the Update User Profile Dialog, click on the <br>
     * 	COMPANY button to view the Current Company dialog. Once the dialog is<br>
     * 	open, click on "Join New Company" and then change company to newCompany</p>
     * @param newCompany RandomCompanyInformation for your company
     * @return MyInfo
     */
    public MyInfo changeCompany(RandomCompanyInformation newCompany) {
	    return this.clickOnCompanyBtn()
                .clickOnJoinNewCompanyBtn()
                .changeTo(newCompany);
    }
    
    /***
     * <h1>waitForDialogToDisappear</h1>
     * <p>purpose: Wait for the Update Profile Dialog to hide<br>
     * 	Fail test if Update Profile Dialog is still visible after 10 seconds</p>
     * @return MyInfo
     */
    public MyInfo waitForDialogToDisappear() {
        waitForInvisibility(By.xpath(this.card_xpath), 10);
        return new MyInfo(driver);
    }
    
    /***
     * <h1>waitForDialogToDisappear</h1>
     * <p>purpose: Wait for the Update Profile Dialog to hide<br>
     * 	Fail test if Update Profile Dialog is still visible after 10 seconds</p>
     * @return MyInfo
     */
    public void validatePhoneNumber() {
        new WebDriverWait(driver, DEFAULT_TIMEOUT)
        			.withMessage("No validation error on screen for empty phone number")
        			.until(ExpectedConditions.visibilityOf(getPhoneValidationMsg()));
    }
}
