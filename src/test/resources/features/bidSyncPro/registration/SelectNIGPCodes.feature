@must @registration @level3
Feature: Supplier Registration - NIGP Codes Screen


@BIDS-234
Scenario: Select NIGP Codes screen properly initializes
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to NIGP screen
	And I verify the nigp page initialization
	Then I will verify keywords added during registration populated
	
@BIDS-234
Scenario: verify addition and deletion of NIGP code while registration & 'UseKeywordsInMyProfile' button
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to NIGP screen
	Then I will verify keywords added during registration populated
	And I will verify NIGP codes addition and removal	
	Then I complete supplier registration from Increase Bid Relevancy page
	Then I will be logged into BidSync for the first time and verify page initialization 
    And I have navigated to Account > "Bid Profile"
    And I will verify that the Select NIGP Codes are populated on bid profile page
	And click on edit nigp codes 
	And I will verify that the Select NIGP Codes screen has properly intialized in modal dialog window
	
@BIDS-234
Scenario: Test to verify whether the NIGP code during registration persists on bid profile page
	Given I have registered a random user
	And I have navigated to Account > "Bid Profile"
	And I will verify that the Select NIGP Codes are populated on bid profile page
	Then click on edit nigp codes 
	And I will verify that the Select NIGP Codes screen has properly intialized in modal dialog window
	
@BIDS-234
Scenario: Test to verify 'Back' and 'Save & Continue' button
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to NIGP screen
	Then I verify the nigp page initialization
	And I click on 'Back' button and verify the redirection
	
@BIDS-356
Scenario: If No available NIGP list, message should be displayed
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to NIGP screen
	Then enter "Apple, Orange, Pineapple" as keywords
	#And verify 'No Matching Codes' message   mandrews: not sure this is possible with elasticsearch  
	Then I verify that the keywords "Apple, Orange, Pineapple" can be removed one by one

#//TODO
#There is a limt to pull 100 records at a time jira#BIDS-393	
@QA-53 
Scenario: verify the total number of results returning by shuffling the search keyword
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to NIGP screen
	Then enter "Car, Truck, Firetruck" as keywords
#	Verify the results are same once we change the order of keywords	
	