package pages.bidSyncPro.registration;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;

import pages.bidSyncPro.common.BidSyncProCommon;
import pages.common.helpers.GlobalVariables;

/***
 * <h1>Class SelectNIGPCodes</h1>
 * @author ssomaraj
 * <p>details: This class contains page objects and methods for the "Select NIGP Codes" screen</p>
 *
 */
public class SelectNIGPCodes extends BidSyncProCommon {
	
	private EventFiringWebDriver driver;
	
    private final String cardTitle_xpath         = "//mat-card-title[@id='matCompanyNIGPTitle'][contains(., 'NIGP Codes')]";
		
    private final String searchNIGPCodesInput_xpath      = "//input[@id='searchNIGPCodes']";
    private final String deleteNIGPCodesBtn_xpath        = "//button[contains(., 'clear')]";
    private final String useKeywordsInMyProfileBtn_xpath = "//button[@id='clearNIGPCodes'][contains(.,'USE KEYWORDS IN MY PROFILE')]";
    private final String availableNIGPCodesTxt_xpath     = "//span[contains(text(), 'Available NIGP Codes')]";
    private final String selectedNIGPCodesTxt_xpath      = "//span[contains(text(), 'Selected NIGP Codes')]";
	
	private final String saveButton_xpath          = "//span[contains(text(),'SAVE')]";
	private final String cancelButton_xpath        = "//span[contains(text(),'CANCEL')]";
    	
	private final String expectedInputPlaceholder = "Enter Keywords or NIGP Codes";	
	private final String editNigpIcon_xpath = "//div[@id='divNigpCodesTitle']//mat-icon[@class='mat-icon notranslate material-icons mat-icon-no-color'][contains(text(),'mode_edit')]";
	
//	   -----Locators for NIGP screen on Registration Flow ------  //
	
	private final String header_xpath      = "//h1[contains(., 'Select NIGP Codes')]";
//	private final String title_xpath             = "//mat-card-title[@id='matCompanyNIGPTitle']";
    private final String headerSummaryTxt_xpath        = "//div[@id='nigpCodesComponent']" +
                                                     "[contains(span, 'Based on your keywords the appropriate NIGP codes have been selected.')]" +
                                                     "[contains(div,  'You can edit the codes now or later in the company profile.')]";
	
	private final String nigpTitle_RegistrationFlow_xpath= "//mat-card-title[@id='matNigpTitle']";
	private final String nigpSearchBoxLabel_id = "searchNIGPCodes";
	private final String keyword1_xpath = "//mat-chip[contains(text(),'Keyword 1')]";
	private final String keyword2_xpath = "//mat-chip[contains(text(),'Keyword 2')]";
	private final String keyword3_xpath = "//mat-chip[contains(text(),'Keyword 3')]";
	
//	private final String selectedSearchkeywordsLabel_xpath= "//strong[contains(text(),'Selected Search Keywords')]";
	private final String useKeywordsInMyProfileButton_RegistrationFlow_xpath = "//span[contains(text(),'USE KEYWORDS IN MY PROFILE')]";
	private final String firstNigpCodePlusIcon_xpath ="//div[@class='tab-content search-content ng-star-inserted']/div[1]/button";
	private final String firstNigpCode_xpath = "//div[@class='tab-content search-content ng-star-inserted']/div[1]/div/span";
	private final String firstNigpDescription_xpath = "//div[@class='tab-content search-content ng-star-inserted']/div[1]/div/div/span";
	private final String firstSelectedNigpCode_xpath = "//button[@aria-label='Remove Nigp Codes']/following-sibling::div//span[contains(@class,'ng-star-inserted')]";
	private final String firstSelectedNigpDescription_xpath = "//button[@aria-label='Remove Nigp Codes']/following-sibling::div//span[not(contains(@class,'ng-star-inserted'))]";
	private final String removeNIGPCodes_xpath = "//button[@aria-label='Remove Nigp Codes']";
	private final String selectedNIGPKeywords_xpath = "//mat-chip";
	private final String availableNIGPCodes_xpath = "//button[@aria-label='Add or Remove Nigp Codes']";
	
	private final String saveAndContinueButton_xpath = "//span[contains(text(),'SAVE & CONTINUE')]";
	private final String backButton_xpath = "//span[contains(text(),'BACK')]";
	
    private final String footerPhone_xpath         = "//footer[@id='fxFooterLayout']//span[contains(.,'(800) 990-9339')]";
    private final String footerEmail_xpath         = "//footer[@id='fxFooterLayout']//span[contains(.,'s2g-support@periscopeholdings.com')]";
    private final String noMatchingCodeMessage = "//em[contains(text(),'No Matching Codes for Search')]";
	
	public SelectNIGPCodes(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	/* ----- getters ----- */
	
	// Header
    public WebElement getHeader()      		{ return findByVisibility(By.xpath(this.header_xpath));      }
    public WebElement getHeaderSummaryTxt() { return findByVisibility(By.xpath(this.headerSummaryTxt_xpath)); }
    public WebElement getCardTitle()  		{ return findByVisibility(By.xpath(this.cardTitle_xpath));  }
    
    // Card
    public WebElement getSearchNIGPCodesInput()      { return findByVisibility(By.xpath(this.searchNIGPCodesInput_xpath));      }
    public WebElement getDeleteNIGPCodesBtn()        { return findByVisibility(By.xpath(this.deleteNIGPCodesBtn_xpath));        }
    public WebElement getUseKeywordsInMyProfileBtn() { return findByVisibility(By.xpath(this.useKeywordsInMyProfileBtn_xpath)); }
    public WebElement getAvailableNIGPCodesTxt()     { return findByVisibility(By.xpath(this.availableNIGPCodesTxt_xpath));     }
    public WebElement getSelectNIGPCodesTxt()        { return findByVisibility(By.xpath(this.selectedNIGPCodesTxt_xpath));      }
    public WebElement getUseKeywordsInMyProfileButton_RegistrationFlow(){ return findByVisibility(By.xpath(this.useKeywordsInMyProfileButton_RegistrationFlow_xpath));}
    public WebElement getFirstNigpCodePlusIcon()     { return findByVisibility(By.xpath(this.firstNigpCodePlusIcon_xpath));     }
    public WebElement getFirstNigpCode()     		 { return findByVisibility(By.xpath(this.firstNigpCode_xpath));      		}
    public WebElement getFirstNigpDescription()      { return findByVisibility(By.xpath(this.firstNigpDescription_xpath));      }
    public WebElement getFirstSelectedNigpCode()     		 { return findByVisibility(By.xpath(this.firstSelectedNigpCode_xpath));      		}
    public WebElement getFirstSelectedNigpDescription()      { return findByVisibility(By.xpath(this.firstSelectedNigpDescription_xpath));      }
    public WebElement getEditNigpIcon()    			 { return findByVisibility(By.xpath(this.editNigpIcon_xpath));      		}
	
	// Footer
    public WebElement getFooterPhone() { return findByVisibility(By.xpath(this.footerPhone_xpath)); }
    public WebElement getFooterEmail() { return findByVisibility(By.xpath(this.footerEmail_xpath)); }
    
    public WebElement getTitle_RegistrationFlow()   { return findByVisibility(By.xpath(this.nigpTitle_RegistrationFlow_xpath)); }
    public WebElement getNigpSearchBox()      	    { return findByVisibility(By.id(this.nigpSearchBoxLabel_id));      }
    public WebElement getKeyword1() 				{ return findByVisibility(By.xpath(this.keyword1_xpath)); }
    public WebElement getKeyword2() 				{ return findByVisibility(By.xpath(this.keyword2_xpath)); }
    public WebElement getKeyword3() 				{ return findByVisibility(By.xpath(this.keyword3_xpath)); }
//    public WebElement getAvailableNigpCodes() 		{ return findByVisibility(By.xpath(this.availableNigpCodes_xpath)); }
//    public WebElement getSelectedNigpCodes() 		{ return findByVisibility(By.xpath(this.selectedNigpCodes_xpath)); }
//    public WebElement getSelectedSearchkeywordsLabel(){ return findByVisibility(By.xpath(this.selectedSearchkeywordsLabel_xpath)); }
    
    public WebElement getCancelButton() { 
		findByScrollIntoViewBottomOfScreen(By.xpath(this.cancelButton_xpath));
		return findByClickability(By.xpath(this.cancelButton_xpath)); }
    
    public WebElement getBackButton() { 
		findByScrollIntoViewBottomOfScreen(By.xpath(this.backButton_xpath));
		return findByClickability(By.xpath(this.backButton_xpath)); }
    
    public WebElement getSaveButton() {
		findByScrollIntoViewBottomOfScreen(By.xpath(this.saveButton_xpath));
		return findByClickability(By.xpath(this.saveButton_xpath));
	}
	
    public WebElement getSaveAndContinueButton() {
		findByScrollIntoViewBottomOfScreen(By.xpath(this.saveAndContinueButton_xpath));
		return findByClickability(By.xpath(this.saveAndContinueButton_xpath));
	}
    
	private List<WebElement> getAvailableNIGPCodes() {
		return findAllOrNoElements(driver, By.xpath(this.availableNIGPCodes_xpath));
	}
    
	private List<WebElement> getSelectedNIGPCodes() {
		return findAllOrNoElements(driver, By.xpath(this.removeNIGPCodes_xpath));
	}
    
	private List<WebElement> getNIGPCodesKeywords() {
		findByVisibility(By.xpath(this.selectedNIGPKeywords_xpath));
		return findAllOrNoElements(driver, By.xpath(this.selectedNIGPKeywords_xpath));
	}
	
	
    
	/* ----- methods ----- */

    public SelectNIGPCodes  clickOnUseKeyWordsInMyProfileButton() {
    	getUseKeywordsInMyProfileButton_RegistrationFlow().click();
    	return this;
    }
    
    public SelectNIGPCodes  clickOnFirstNigpCodePlusIcon() {
    	getFirstNigpCodePlusIcon().click();
    	return this;
    }
    
    public SelectNIGPCodes  clickOnSaveAndContinueButton() {
    	getSelectedNigpCodeAndDescription();
    	getSaveAndContinueButton().click();
    	return this;
    }
    
    public SelectNIGPCodes  clickOnBackButton() {
    	getBackButton().click();
    	return this;
    }
    
    public SelectNIGPCodes  clickOnEditNigp() {
    	getEditNigpIcon().click();
    	return this;
    }
    
    public SelectNIGPCodes getSelectedNigpCodeAndDescription() {
    	String selectedNigpCode = null;
    	String selectedNigpDescription = null;
    	if(doesPageObjectExist(driver, By.xpath(this.firstSelectedNigpCode_xpath)))
	    	selectedNigpCode = getFirstSelectedNigpCode().getText();
    	if(doesPageObjectExist(driver, By.xpath(this.firstSelectedNigpDescription_xpath)))
	    	selectedNigpDescription = getFirstSelectedNigpDescription().getText();
	    	GlobalVariables.addGlobalVariable("selectedNigpCode", selectedNigpCode);
	    	GlobalVariables.addGlobalVariable("selectedNigpDescription", selectedNigpDescription);
    	return this;
    }
	/* ----- verifications ----- */
	public SelectNIGPCodes clickUseKeywordsInMyProfileButton() {
		getUseKeywordsInMyProfileBtn().click();
		return this;
	}
    
    /***
	 * <h1>verifyPageInitialization</h1>
	 * <p>purpose: Verify that the "Select NIGP Codes" screen has initialized per:<br>
	 * 	1. All Page Objects<br>
	 * 	2. All Buttons<br>
	 * 	3. Placeholder text</p>
	 * @return SelectNIGPCodes
	 */
	public SelectNIGPCodes verifyPageInitialization() {
		try {
			// Header
			this.getCardTitle();
			
			// Card
			this.getSearchNIGPCodesInput();
			this.getDeleteNIGPCodesBtn();
			this.getUseKeywordsInMyProfileBtn();
			this.getAvailableNIGPCodesTxt();
			this.getSelectNIGPCodesTxt();
			
			// Bottom Buttons
			this.getSaveButton();
			this.getCancelButton();
			
		} catch (ElementNotFoundException e) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Select NIGP Codes\" screen did not initialize per:\n" + e); }
		
		// Placeholders
		Assert.assertEquals("Test Failed: Input field did not initialize with expected placeholder message", 
				this.expectedInputPlaceholder, this.getSearchNIGPCodesInput().getAttribute("placeholder"));

		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"Select NIGP Codes\" screen has properly initialized");
		return this;
	}
	
	/***
	 * <h1>verifyPageInitialization</h1>
	 * <p>
	 * purpose: Verify that the "Select NIGP Codes" screen has initialized per:<br>
	 * 1. All Page Objects<br>
	 * 2. All Buttons<br>
	 * 3. Placeholder text
	 * </p>
	 * 
	 * @return SelectNIGPCodes
	 */
	public SelectNIGPCodes verifyPageInitializationinModalDialogWindow() {
		try {
			// Card
			this.getSearchNIGPCodesInput();
			this.getDeleteNIGPCodesBtn();
			this.getUseKeywordsInMyProfileBtn();
			this.getAvailableNIGPCodesTxt();
			this.getSelectNIGPCodesTxt();
			this.verifyNigpOnBidProfile_EditModal();
			// Bottom Buttons
			this.getSaveButton();
			this.getCancelButton();

		} catch (ElementNotFoundException e) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Select NIGP Codes\" screen did not initialize per:\n" + e);
		}

		// Placeholders
		Assert.assertEquals("Test Failed: Input field did not initialize with expected placeholder message",
				this.expectedInputPlaceholder, this.getSearchNIGPCodesInput().getAttribute("placeholder"));

		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"Select NIGP Codes\" screen has properly initialized");
		return this;
	}

	
	
	/***
	 * <h1>verify nigp pageInitialization on registration </h1>
	 * <p>purpose: Verify that the "Select NIGP Codes" screen has initialized per:<br>
	 * 	1. All Page Objects<br>
	 * 	2. All Buttons<br>
	 * 	3. Placeholder text</p>
	 * @return SelectNIGPCodes
	 */
	
	public SelectNIGPCodes verifyPageInitialization_RegistrationFlow() {
		try {
			this.getHeader();
			this.getHeaderSummaryTxt();
			this.getTitle_RegistrationFlow();
			this.getNigpSearchBox();
//			this.getKeyword1();
//			this.getKeyword2();
//			this.getKeyword3();
			this.getAvailableNIGPCodesTxt();
			this.getSelectNIGPCodesTxt();
			this.getUseKeywordsInMyProfileButton_RegistrationFlow();
			
			this.getSearchNIGPCodesInput();
			this.getDeleteNIGPCodesBtn();
			this.getUseKeywordsInMyProfileBtn();
			this.getAvailableNIGPCodesTxt();
			this.getSelectNIGPCodesTxt();
			
			// Bottom Buttons
			this.getSaveAndContinueButton();
			this.isSaveAndContinueButtonDisabled();
			this.getBackButton();
			
			// Footer
			this.getFooterEmail();
			this.getFooterPhone();
			
		} catch (ElementNotFoundException e) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Select NIGP Codes\" screen did not initialize per:\n" + e); }
		
		// Placeholders
		Assert.assertEquals("Test Failed: Input field did not initialize with expected placeholder message", 
				this.expectedInputPlaceholder, this.getSearchNIGPCodesInput().getAttribute("placeholder"));

		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"Select NIGP Codes\" screen has properly initialized");
		return this;
	}
	
	public SelectNIGPCodes verifyNigpOnBidProfile() {
			WebElement nigpCode = findByVisibility(By.xpath("//div[contains(text(),'"+GlobalVariables.getGlobalVariable("selectedNigpCode")+"')]"));
			WebElement nigpDescription = findByVisibility(By.xpath("//div[contains(text(),'"+GlobalVariables.getGlobalVariable("selectedNigpDescription")+"')]"));
			new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Invalid NIGP code and Description")
				.until(ExpectedConditions.and(ExpectedConditions.visibilityOf(nigpCode), ExpectedConditions.visibilityOf(nigpDescription)));
		return this;
	}
	
	public SelectNIGPCodes verifyNigpOnBidProfile_EditModal() {
		WebElement nigpCode = findByVisibility(By.xpath(this.firstSelectedNigpCode_xpath));
		WebElement nigpDescription = findByVisibility(By.xpath(this.firstSelectedNigpDescription_xpath));
		new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Invalid NIGP code and Description")
			.until(ExpectedConditions.and(ExpectedConditions.visibilityOf(nigpCode), ExpectedConditions.visibilityOf(nigpDescription)));
	return this;
}
	
	public SelectNIGPCodes verifyBackButton() {
		new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Filed to redirect the page on click of back button")
		.until(ExpectedConditions.urlContains("/setup/keywords"));
		return this;
	}
	
	public SelectNIGPCodes isSaveAndContinueButtonDisabled() {
		new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Save & Continue button is enabled")
		.until(ExpectedConditions.attributeContains(By.id("saveButton"), "disabled", "true"));
		return this;
	}
	

	public SelectNIGPCodes enterNIGPCodes(String... nigpCodes) {
		for (String nigpCode : nigpCodes) {
			this.getSearchNIGPCodesInput().sendKeys(nigpCode);
		}
		return this;
	}

	public SelectNIGPCodes enterNIGPCodesByKeywordsInProfile() {
		List<String> nigpKeywords = new ArrayList<String>();
		clickUseKeywordsInMyProfileButton();
		nigpKeywords = getNIGPKeywords();
		selectAvailableNIGPCodes(3);
		removeSelectedNIGPCodes(1);
		getSelectedNigpCodeAndDescription();
		this.getSaveButton().click();
		return this;
	}

	public List<String> getNIGPKeywords() {
		List<String> nigpKeywords = new ArrayList<String>();
		for (WebElement nigpKeyword : getNIGPCodesKeywords())
			nigpKeywords.add(nigpKeyword.getText().split("\\r?\\n")[0]);
		return nigpKeywords;
	}

	public SelectNIGPCodes selectAvailableNIGPCodes(int count) {
		for (WebElement nigpCode : getAvailableNIGPCodes()) {
			nigpCode.click();
			--count;
			if(count<1)
				break;
		}
		return this;
	}
	
	public int returnSelectedNIGPCodesCount() {
		return getSelectedNIGPCodes().size();
	}
	
	public SelectNIGPCodes removeSelectedNIGPCodes(int count) {
		for (WebElement nigpCode : getSelectedNIGPCodes()) {
			nigpCode.click();
			--count;
			if(count<1)
				break;
		}
		return this;
	}
	
	public SelectNIGPCodes verifyNoMatchingCodeMessage() {
		new WebDriverWait(driver, 10).withMessage("Not able to load 'No Matching Code' message")
		.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(noMatchingCodeMessage)));
		return this;
	}
	
	
	public SelectNIGPCodes verifyKeywordsCanBeRemovedOneByOne(String[] keyword) {
		String xpath = "//mat-chip[contains(text(),'"+ keyword[0] +"')]/mat-icon[contains(text(),'cancel')]";
		String xpath1 = "//mat-chip[contains(text(),'"+ keyword[1] +"')]/mat-icon[contains(text(),'cancel')]";
		findByClickability(By.xpath(xpath)).click();
		findByClickability(By.xpath(xpath1)).click();
		new WebDriverWait(driver, 2).withMessage("Not able to delete the keyword")
		.until(ExpectedConditions.and(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath)),
				ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath))));
		return this;
	}
	
}
