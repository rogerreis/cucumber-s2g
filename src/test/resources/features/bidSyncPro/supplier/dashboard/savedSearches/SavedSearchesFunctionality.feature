@must @savedSearches @search @bidSearch_detail @integrations @level2
Feature: Dashboard - Saved Searches - Functionality Tests
  This feature file exercises the objects related to saving and retrieving searches via Filter Results on the Supplier Dashboard
  	ARGO-418: Saved Searches - deleting saved searches
  	ARGO-887: Creating/Loading Saved Searches 
  	BIDSYNC-39: Search criteria clears after looking at bid
  	BIDSYNC-579: Missing Case for open Filter Results section when opening saved search from "All Bids" 


@ARGO-887
Scenario: Before first filter save, Saved Searches button is not visible on the Supplier dashboard.
    After first filter save, Saved Searches button is both visible and functional on the Supplier dashboard.

	Given I register a new user with a BidSync Basic Plan from the Masonry website
	When I complete supplier registration and log in for the first time
	Then I will verify that the Saved Searches dropdown is not visible on the dashboard
#	When I open the Filter Results Section
	And I create a saved search with a random name
	Then I will verify that the Saved Searches dropdown is visible on the dashboard
	And I will verify that my saved search appears in the Saved Searches dropdown

	
@ARGO-887
Scenario: Filter Results Save Popup Initializes as Expected
	1.  All objecst are present
	2.  Default placeholder message display for input
    Given I have logged into the Supplier side as user "Basic"
    When I view the Save Search Popup
    Then I will verify that the Save Search Popup has correctly initialized

	
@ARGO-887
Scenario: Filter Results Collapsible Section Allows for Cancel and Save of Search Names on Save Search Popup
    Given I have logged into the Supplier side as user "Basic"
#    And I open the Filter Results Section
    When I create a search but then cancel its save
    Then I will verify that the cancelled saved search does not appear in the Saved Searches dropdown 
    And I will verify that my search did not save out to the database
    When I resave the search with a random name
	Then I will verify that my saved search appears in the Saved Searches dropdown 
	And I will verify that my search has saved out to the database


@ARGO-418 
Scenario: User cannot save a search with an existing name

    Given I have logged into the Supplier side as user "Basic"
#    And I open the Filter Results Section
    And I have saved a generic search with a random name
    When I try to save another search with the same name
	Then I will verify that I cannot save the search with duplicated name


@ARGO-418
Scenario: User can Delete a Saved Search

    Given I have logged into the Supplier side as user "Basic"
#    And I open the Filter Results Section
    And I have saved a generic search with a random name
    When I delete the search from the Saved Searches list
    Then I will verify that the search has been deleted from the Saved Searches list
    

@ARGO-418
Scenario: User can Load a Saved Search and the All Bids List will Display Results per the Search Criteria

    Given I have logged into the Supplier side as user "Basic"
    When I retrieve the first search in the Saved Searches dropdown
    Then I will verify that All Bids is both highlighted and selected, and also displays correct results per the displayed Filter Results search filter
	And I delete the search from the Saved Searches list


@ARGO-887
Scenario: User cannot Save a Search with a Blank Name
	
    Given I have logged into the Supplier side as user "Basic"
    When I try to save a search with a blank name
    Then I will verify that validation will reject the blank name in the Saved Search popup

    
@BIDSYNC-39 @BIDSYNC-579
Scenario: User navigates to All Bids list and closes Filter Results. Retrieving a saved search will re-open Filter Results and display search results.
	Given I have logged into the Supplier side as user "Basic"
	And I have navigated to the "All Bids" bid tab
	And I close the Filter Results Section
	When I retrieve the first search in the Saved Searches dropdown
	Then I will verify that my saved search displays filters in the Filter Results section
	And I will verify that All Bids is both highlighted and selected, and also displays correct results per the displayed Filter Results search filter
	And I delete the search from the Saved Searches list

@BIDS-487 @BIDS-495
Scenario: User can saved Search and after running a generic search verify that the filter name was cleared
	Given I have logged into the Supplier side as user "Basic"
	When I create a saved search with a random name
	When I run a generic search from the Filter Results section
	Then I will verify that Saved Search name is saved in the Saved Searches dashboard
	