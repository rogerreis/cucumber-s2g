@must @Bid_List @bidSearch_detail @level3
Feature: Dashboard - Match Score, Part 2
  This feature tests the match score indicators for bids found on the supplier dashboard
  BIDSYNC-282: Match Score Not Consistent

  
@BIDSYNC-282
Scenario Outline: Match Score displays correctly per bids returned for user's default keywords in "New For You" list
	Note: Currently using the QANewNational@phimail.mailinator.com/P@ssw0rd account in the QA environment.
	This user account has default keywords of "water", "river", "lake" and negative default keywords of "ocean"
	dmidura: If keywords for this account need to be updated to diff values for this test, that's fine. 
          Please make sure that there are 3 positive and 1 negative keywords saved to the account
          (account is used by other tests)
 
	Given I have logged into the Supplier side as user "User to Verify Default Keywords"
	And I have navigated to the "New For You" bid tab
	And I have verified that "<keyword>" positive default keywords are displayed on the Filter Results section
	When I select all bids from bid list with a match score between "<lowerBounds>" "<upperBounds>"
	Then I will verify that the match score indicator for all selected bids displays the text "<text>" with the indicator color "<color>"
	 
	Examples: Required matches
   	|keyword 	   	| lowerBounds | upperBounds | text       | color  				  |
    |water			| 90          | 100         | BEST MATCH | rgba(114, 172, 82, 1)  |
    |water			| 75          | 89          | GOOD       | rgba(114, 172, 82, 1)  |
    |water			| 50          | 74          | POSSIBLE   | rgba(253, 189, 55, 1)  |
    |water			| 0           | 49          | UNLIKELY   | rgba(255, 41, 4, 0.56) |