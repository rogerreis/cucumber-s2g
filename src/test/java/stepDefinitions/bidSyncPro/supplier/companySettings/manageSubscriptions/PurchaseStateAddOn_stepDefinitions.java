package stepDefinitions.bidSyncPro.supplier.companySettings.manageSubscriptions;


import core.CoreAutomation;
import cucumber.api.java.en.And;
import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.bidSyncPro.common.DatabaseCommonSearches;
import pages.common.helpers.GlobalVariables;

public class PurchaseStateAddOn_stepDefinitions {

	public PurchaseStateAddOn_stepDefinitions(CoreAutomation automation) {
	}
	
	@And ("^I will verify that the ARGO database correctly displays the company owner subscription and addOns$")
	public void I_will_verify_that_the_ARGO_database_correctly_displays_the_company_owner_subscription_and_addOns() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that ARGO database correctly display subscription + addOns");
		RandomChargebeeUser owner = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");

		new DatabaseCommonSearches().verifySubscriptionAndAddOns(owner);
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Verified that company owner's subscription + addOns are correctly recorded in ARGO database");
	}
}
