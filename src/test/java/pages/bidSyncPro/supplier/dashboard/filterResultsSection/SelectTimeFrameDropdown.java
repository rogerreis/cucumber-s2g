package pages.bidSyncPro.supplier.dashboard.filterResultsSection;

import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

/***
 * <h1>Class SelectTimeFrameDropdown</h1>
 * @author dmidura
 * <p>details: This class houses the objects and methods for the Select Time Frame dropdown on the Filter Results<br>
 * 	This dropdown is available under the Bid End Date section, after the "Include Past Bids" checkbox is checked</p>
 */
public class SelectTimeFrameDropdown extends BidEndDateSection {
	
	// xpaths

	private final String past6Months_xpath      = "//div[@class='mat-menu-content']//button[contains(text(),'Past 6 Months')]";
	private final String pastYear_xpath         = "//div[@class='mat-menu-content']//button[contains(text(),'Past Year')]";
	private final String past3Years_xpath       = "//div[@class='mat-menu-content']//button[contains(text(),'Past 3 Years')]";
	private final String customTimeFrame_xpath  = "//div[@class='mat-menu-content']//button[contains(text(),'Custom Time Frame')]";
	
	
	// Other
	private EventFiringWebDriver driver;
	
	public SelectTimeFrameDropdown(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	/* ----- getters ----- */
	private WebElement getPast6Months()     { return findByVisibility(By.xpath(this.past6Months_xpath));     }
	private WebElement getPastYear()        { return findByVisibility(By.xpath(this.pastYear_xpath));        }
	private WebElement getPast3Years()      { return findByVisibility(By.xpath(this.past3Years_xpath));      }
	private WebElement getCustomTimeFrame() { return findByVisibility(By.xpath(this.customTimeFrame_xpath)); }
	
	/* ----- methods ----- */

	/***
	 * <h1>selectPast6Months</h1>
	 * <p>purpose: Select "Past 6 Months" from the "Select Time Frame" dropdown<br>
	 * 	 under the Bid End Date section of the Filter Results section</p>
	 * @return
	 */
	public BidEndDateSection selectPast6Month() {
		this.getPast6Months().click();
		return new BidEndDateSection(driver);
	}

	/***
	 * <h1>selectPastYear</h1>
	 * <p>purpose: Select "Past Year" from the "Select Time Frame" dropdown<br>
	 * 	 under the Bid End Date section of the Filter Results section</p>
	 * @return
	 */
	public BidEndDateSection selectPastYear() {
		this.getPastYear().click();
		return new BidEndDateSection(driver);
	}

	/***
	 * <h1>selectPast3Years</h1>
	 * <p>purpose: Select "Past 3 Years" from the "Select Time Frame" dropdown<br>
	 * 	 under the Bid End Date section of the Filter Results section</p>
	 * @return
	 */
	public BidEndDateSection selectPast3Years() {
		this.getPast3Years().click();
		return new BidEndDateSection(driver);
	}

	/***
	 * <h1>selectCustomTimeFrame</h1>
	 * <p>purpose: Select "Custom Time Frame" from the "Select Time Frame" dropdown<br>
	 * 	 under the Bid End Date section of the Filter Results section</p>
	 * @return
	 */
	public CustomTimeFrameSection selectCustomTimeFrame() {
		this.getCustomTimeFrame().click();
		return new CustomTimeFrameSection(driver);
	}
	
	/***
	 * <h1>selectTimeFrame</h1>
	 * <p>purpose: Given a timeFrameOptions value, select that corresponding
	 * 	value from the SELECT TIME FRAME dropdown.</p>
	 * @param TimeFrameOptions
	 * @return BidEndDateSection
	 */
	public BidEndDateSection selectTimeFrame(TimeFrameOptions timeFrame) {
		switch (timeFrame) {
		case PAST_6_MONTHS:
			return this.selectPast6Month();
		case PAST_YEAR:
			return this.selectPastYear();
		case PAST_3_YEARS:
			return this.selectPast3Years();
		case CUSTOM_TIME_FRAME:
			return this.selectCustomTimeFrame();
		default:
			fail("ERROR: \"" + timeFrame + "\" is not a valid time frame!");
			return null;
		}
	}
}



