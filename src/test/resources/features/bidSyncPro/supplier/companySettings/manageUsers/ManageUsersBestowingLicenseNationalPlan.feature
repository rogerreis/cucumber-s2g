@must @ManageUsers @subscriptions @level3
Feature: Company Settings > Management Users - Bestowing License Confers Subscription Privileges, National Plan
	ARGO-1258: "Manage Additional Subscription Licenses (assign/remove purchased licenses)"

dmidura: Per 12.10.2018 discussion with jbarlow, during registration,  Federal and Military addOn packages are correlated to
the user's subscription plan (i.e. Federal addOn to National sees all states, but Federal addOn to KS State plan will only see the KS Federal bids)
Canada addOn is for all Canadian provinces. This behavior may change in the future, in which case the BidListRow.verifyBidAccessIsCorrect()
method should be updated.
  

@ARGO-1258
Scenario Outline: After user is assigned subscription, user can view subscription bids per subscription license.
	After user license is revoked, user cannot view bids per old subscription license.
	dmidura: Step with BidSync Basic Bids is no longer correct 

	Given I register a new company owner with a "<subscriptionType>" subscription plan, with add-ons and some licenses
	And I logout of BidSync Pro
	And I register a new user with a BidSync Basic Plan from the Masonry website
	And the new user joins the new company with available subscription licenses
	When the new user views search results in their All Bids tab
	Then the new user will only have access to BidSync Basic bids
	When the new user is assigned a subscription license by the company owner
	And I logout of BidSync Pro
	Then the new user will be able to view bids per their newly assigned license
	When the company owner revokes the additional license from the new user
	Then the revoked user will only have access to BidSync Basic bids

	Examples:
	| subscriptionType           |
	| BidSync National - Yearly  |
    | BidSync National - Monthly |
    | BidSync National - 6 Month |