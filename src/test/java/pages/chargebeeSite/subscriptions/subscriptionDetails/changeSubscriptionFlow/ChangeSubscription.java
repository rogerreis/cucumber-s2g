package pages.chargebeeSite.subscriptions.subscriptionDetails.changeSubscriptionFlow;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import data.bidSyncPro.registration.AdditionalStateAddOn;
import data.bidSyncPro.registration.SubscriptionAddOns;
import data.bidSyncPro.registration.SubscriptionPlan;
import pages.chargebeeSite.common.CommonNavSliderMenu;
import pages.chargebeeSite.subscriptions.subscriptionDetails.SubscriptionDetails;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

/***
 * <h1>ChangeSubscription</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects "Change Subscription - <companyName>" page that is<br>
 * 	accessed by clicking on "Change Subscription" button on the Subscriptions->Subscription Detail page<br>
 * 			   on the Chargebee test site</p>
 */
public class ChangeSubscription extends CommonNavSliderMenu {

	// Other
	private EventFiringWebDriver driver;
	private final String urlFragment                       = "edit";
	
	// xpaths
	// Plan Name Dropdown
	private final String planNameDropdownBtn_xpath 		   = "//div[@id='s2id_plan_code']//a";
	private final String planNameDropdownInput_xpath 	   = "//input[@id='s2id_autogen1_search']";
	private final String planNameDropdownFirstResult_xpath = "//ul[@id='select2-results-1']/li[1]";

	// Recurring AddOns
	private final String addAddOnLink_xpath                = "//a[@data-local-id='add-addon-modal-content'][contains(.,'Add Addon')]";

	// Additional States addOn Dropdowns
	private final String additionalState1Dropdown_xpath    = "//label[@for='Additional State #1']/..//select[contains(@id,'subscription.subscription_custom_field')]";
	private final String additionalState2Dropdown_xpath    = "//label[@for='Additional State #2']/..//select[contains(@id,'subscription.subscription_custom_field')]";
	private final String additionalState3Dropdown_xpath    = "//label[@for='Additional State #3']/..//select[contains(@id,'subscription.subscription_custom_field')]";
	private final String additionalState4Dropdown_xpath    = "//label[@for='Additional State #4']/..//select[contains(@id,'subscription.subscription_custom_field')]";
	private final String additionalState5Dropdown_xpath    = "//label[@for='Additional State #5']/..//select[contains(@id,'subscription.subscription_custom_field')]";
	Collection <String> expectedStates = new ArrayList<String>();
	
	// Bottom Buttons
	private final String changeSubscriptionBtn_xpath 	   = "//input[@id='change-sub-preview'][@value='Change Subscription']";
	
	public ChangeSubscription(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	// getters
	// Plan Name Dropdown
	private WebElement getPlanNameDropdownBtn()         {return findByVisibility(By.xpath(this.planNameDropdownBtn_xpath));               }
	private WebElement getPlanNameDropdownInput()       {return findByVisibility(By.xpath(this.planNameDropdownInput_xpath));             }
	private WebElement getPlanNameDropdownFirstResult() {return findByVisibility(By.xpath(this.planNameDropdownFirstResult_xpath));       }
	
	// Recurring AddOns
	private WebElement getAddAddonLink()                {return findByVisibility(By.xpath(this.addAddOnLink_xpath));                      }

	// Additional States addOn Dropdowns
	private Select getAdditionalState1Dropdown()        {return new Select(findByVisibility(By.xpath(this.additionalState1Dropdown_xpath)));}
	private Select getAdditionalState2Dropdown()        {return new Select(findByVisibility(By.xpath(this.additionalState2Dropdown_xpath)));}
	private Select getAdditionalState3Dropdown()        {return new Select(findByVisibility(By.xpath(this.additionalState3Dropdown_xpath)));}
	private Select getAdditionalState4Dropdown()        {return new Select(findByVisibility(By.xpath(this.additionalState4Dropdown_xpath)));}
	private Select getAdditionalState5Dropdown()        {return new Select(findByVisibility(By.xpath(this.additionalState5Dropdown_xpath)));}
	
	// Bottom Buttons
	private WebElement getChangeSubscriptionBtn()       {return findByScrollIntoViewBottomOfScreen(By.xpath(this.changeSubscriptionBtn_xpath)); }
	
	/* ----- helpers -----*/
	
	/***
	 * <h1>getSelectedSubscriptionPlan
	 * <p>purpose: Get the currently selected subscription plan
	 * @return SubscriptionPlan representing the currently displayed plan in the "Plan Name" dropdown
	 */
	private SubscriptionPlan getSelectedSubscriptionPlan() { return SubscriptionPlan.fromString(this.getPlanNameDropdownBtn().getAttribute("value")); }
	
	/***
	 * <h1>getDropdownByNumber</h1>
	 * <p>purpose: Given the dropdown number, return the correct Additional State dropdown 
	 * @param dropdownNumber = [1,5] is the number of the dropdown
	 * @return Select for the appropriate Additional State dropdown
	 */
	private Select getDropdownByNumber(int dropdownNumber) {
		Select dropdown = null;
		
		switch (dropdownNumber) {
		case 1: 
			dropdown = this.getAdditionalState1Dropdown();
			break;
		case 2:	
			dropdown = this.getAdditionalState2Dropdown();
			break;
		case 3: 
			dropdown = this.getAdditionalState3Dropdown();
			break;
		case 4: 
			dropdown = this.getAdditionalState4Dropdown();
			break;
		case 5: 
			dropdown = this.getAdditionalState5Dropdown();
			break;
		default: fail("ERROR: Wrong value for dropdown");
	}

	return dropdown;
	}

	/*----- methods -----*/
	
	/***
	 * <h1>updatePlanName</h1>
	 * <p>purpose: Update your user's Chargebee Subscription</p>
	 * @param String strSubscriptionType = the subscription type
	 * @return ChangeSubscription
	 */
	public ChangeSubscription updatePlanName (String strSubscriptionType) {
		new HelperMethods().addSystemWait(2);
		if(this.getPlanNameDropdownBtn().getText().equals(strSubscriptionType)) {
			return this;
		}
		this.getPlanNameDropdownBtn().click();
		this.getPlanNameDropdownInput().sendKeys(strSubscriptionType);
		Pattern pattern = Pattern.compile(strSubscriptionType);
		// Click the first item in the dropdown
		int time = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, time)
			.withMessage("Test Failed: Could not select \"" + strSubscriptionType + "\" from dropdown after \"" + time + "\" seconds")
			.until(
					ExpectedConditions.or(
					ExpectedConditions.stalenessOf(this.getPlanNameDropdownFirstResult()),
					ExpectedConditions.textMatches(By.xpath(this.planNameDropdownFirstResult_xpath), pattern)
					));

		this.getPlanNameDropdownFirstResult().click();

		return this;
	}

	/***
	 * <h1>clickChangeSubscriptionBtn</h1>
	 * <p>purpose: Click on the "Change Subscription" button at the bottom of the<br>
	 * 	"Change Subscription - My Company" page</p>
	 * @return PreviewOfChangesVerificationPopup
	 */
	public PreviewOfChangesVerificationPopup clickChangeSubscriptionBtn() {
		this.getChangeSubscriptionBtn().click();
		return new PreviewOfChangesVerificationPopup(driver);
	}
	
	/***
	 * <h1>setAdditionalStates</h1>
	 * <p>purpose: Given a list of AdditionalStateAddOn that represent up to five
	 * 	additional state addOns, set these values in the Additional States dropdowns
	 * @param stateAddOns list of AdditionalStateAddOn representing states to set in the Additional
	 * 	States dropdown. List should contain between 1-5 states.
	 * @return ChangeSubscription
	 */
	public ChangeSubscription setAdditionalStates(List <AdditionalStateAddOn> stateAddOns) {
		for (int i = 1; i < stateAddOns.size() + 1; i++) {
			this.getDropdownByNumber(i)
				.selectByValue(stateAddOns.get(i-1).toString());
		}
		return this;
	}
	
	/***
	 * <h1>addRecurringAddOn</h1>
	 * <p>purpose: Select Federal | Military | Canada + correct subscription from the "Recurring AddOns" dropdown.
	 * @param addOn = SubscriptionAddOns to specify Canada | Military | Federal
	 * @return ChangeSubscription
	 */
	public ChangeSubscription addRecurringAddOn(SubscriptionAddOns addOn) {
		SubscriptionPlan plan = this.getSelectedSubscriptionPlan();
		this.getAddAddonLink().click();
		return new RecurringAddOnPopup(driver, addOn, plan).setAddOn();
	}
	
	/***
	 * <h1>addRecurringAddOn</h1>
	 * <p>purpose: Select "Additional States" from the "Recurring AddOns" dropdown.
	 * 	Set the number of additional states to purchase per the size of the stateAddOns list.
	 * 	Click on "Add" to add.
	 * @param stateAddOns = list of the AdditionalStateAddOn to purchase
	 * @return ChangeSubscription
	 */
	public ChangeSubscription addRecurringAddOn(List <AdditionalStateAddOn> stateAddOns) {
		this.getAddAddonLink().click();
		return new RecurringAddOnPopup(driver, stateAddOns).setAddOn();
	}
	
	/***
	 * <h1>purchaseAdditionalStates</h1>
	 * <p>purpose: Enact all steps required to purchase additional states on the "Change Subscription" screen.
	 * 	Click on "Add Addon" link to view the Recurring AddOn popup. From there, select "Additional States" as the
	 * 	Recurring addOn, and also set the "Quantity" field. Click "Add" to dismiss the popup.
	 *	Next, set the values of the "Additional State #" dropdowns in "Change Subscription" screen to values in the stateAddOns list.
	 *	Click "Change Subscription" button at the bottom of the "Change Subscription" and then "Confirm"
	 *	on the "Preview of Changes" popup. After completing purchase, screen navigates to the "Subscription Details"
	 * @param stateAddOns = list of the AdditionalStateAddOn to purchase. Should be between 1-5 states
	 * @return SubscriptionDetails
	 */
	public SubscriptionDetails purchaseAdditionalStates(List <AdditionalStateAddOn> stateAddOns) {
		System.out.println(GlobalVariables.getTestID() + " INFO: Purchasing additional states \"" + stateAddOns.toString() + "\"");
		return this
				.addRecurringAddOn(stateAddOns)
				.setAdditionalStates(stateAddOns)
				.clickChangeSubscriptionBtn()
				.clickConfirmBtn();
	}

	/*----- verifications -----*/
	
	/***
	 * <h1>verifyPageLoad</h1>
	 * <p>purpose: Verify that the Change Subscriptions page has loaded</p>
	 * @return ChangeSubscription
	 */
	public ChangeSubscription verifyPageLoad() {
		int time = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, time)
			.withMessage("Test Failed: Change Subscriptions page did not load in \"" + time + "\" seconds")
			.until(ExpectedConditions.urlContains(this.urlFragment));
		return this;
	}
	
	/***
	 * <h1>verifyAdditionalStateDropdownContainAllOptions</h1>
	 * <p>purpose: Verify that each of the five Additional State dropdowns
	 * 	contain all expected values (these are the String values of the
	 * 	enum AdditionalStateAddOn). Fail test if the any of the Additional
	 * 	State dropdowns does not contain all expected values.
	 * @return ChangeSubscription
	 */
	public ChangeSubscription verifyAdditionalStateDropdownsContainAllOptions() {
		SoftAssertions softly = new SoftAssertions();

		// Get all the available states we expect to see in the list
		String allStates = Arrays.toString(AdditionalStateAddOn.values());
		
		// Verify these values are actually appearing in the dropdown
		for (int i=1; i<6; i++) {
			List<String> dropdownValues = new ArrayList<String>();
			this.getDropdownByNumber(i).getOptions()
					.forEach(ele->dropdownValues.add(ele.getAttribute("value").trim()));
			dropdownValues.remove(0);
			System.out.println(dropdownValues);
			softly.assertThat(allStates.equals(dropdownValues.toString())).withFailMessage("ERROR: Dropdown %s is incorrectly displaying values", i).isTrue();
		}
		
		softly.assertAll();
		return this;
	}

	

}
