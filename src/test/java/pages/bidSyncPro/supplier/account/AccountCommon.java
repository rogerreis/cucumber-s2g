package pages.bidSyncPro.supplier.account;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.account.accountSettings.AccountSettings;
import pages.bidSyncPro.supplier.account.bidProfile.BidNotifications;
import pages.bidSyncPro.supplier.account.myInfo.MyInfo;
import pages.bidSyncPro.supplier.account.support.Support;
import pages.bidSyncPro.supplier.account.training.Training;
import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;

/***
 * <h1>Class AccountCommon</h1>
 * <p>details: This class houses methods and objects that are common to all pages off of the /admin/my-account screen<br>
 * 	Includes: All account (row) navigation buttons, such as "Back to Bids", My Info", etc</p>
 */
public class AccountCommon extends CommonTopBar {
    
    // xpaths
    private final String backToBidsButton_xpath         = "//button[contains(span, 'Back to Bid Opportunities')]";
    private final String bidInfoButton_xpath            = "//a[contains(@href, '/admin/bid-notifications')]";
    private final String myProfileButton_xpath          = "//a[contains(@href, '/admin/my-profile')]";
    private final String trainingButton_xpath           = "//a[contains(@href, '/admin/training')]";
    private final String supportButton_xpath            = "//a[contains(@href, '/admin/support')]";
    private final String accountSettingsButton_xpath    = "//a[contains(@href, '/admin/my-account')]";
    
    public AccountCommon(EventFiringWebDriver driver) {
        super(driver);
    }
    
    /* ----- getters ----- */
    
    private WebElement getBackToBidsButton     () { return findByPresence(By.xpath(this.backToBidsButton_xpath));     }
    private WebElement getBidProfileButton     () { return findByPresence(By.xpath(this.bidInfoButton_xpath));        }
    private WebElement getMyProfileButton      () { return findByPresence(By.xpath(this.myProfileButton_xpath));      }
    private WebElement getTrainingButton       () { return findByPresence(By.xpath(this.trainingButton_xpath));       }
    private WebElement getSupportButton        () { return findByPresence(By.xpath(this.supportButton_xpath));        }
    private WebElement getAccountSettingsButton() { return findByPresence(By.xpath(this.accountSettingsButton_xpath));}
    
    /* ----- methods ----- */
    
    /***
     * <h1>clickBackToBids</h1>
     * <p>purpose: Click on "BACK TO BIDS" button<br>
     * 	that appears in top LH corner of screen.<br>
     * 	App should navigate to dashboard</p>
     * @return BidList
     */
    public BidList clickBackToBids() {
        getBackToBidsButton().click();
        return new BidList(driver);
    }
    
    /***
     * <h1>clickNotifications</h1>
     * <p>purpose: Click on "BID PROFILE" button<br>
     * 	App should navigate to /admin/bid-notifications</p>
     * @return BidNotifications
     */
    public BidNotifications clickBidProfile() {
        getBidProfileButton().click();
        return new BidNotifications(driver);
    }

    /***
     * <h1>clickMyInfo</h1>
     * <p>purpose: Click on "MY INFO" button<br>
     * 	App should navigate to /admin/my-profile</p>
     * @return MyInfo
     */
    public MyInfo clickMyInfo() {
        getMyProfileButton().click();
        return new MyInfo(driver);
    }

    /***
     * <h1>clickTraining</h1>
     * <p>purpose: Click on "TRAINING" button<br>
     * 	App should navigate to /admin/training</p>
     * @return Training
     */
    public Training clickTraining() {
        getTrainingButton().click();
        return new Training(driver);
    }

    /***
     * <h1>clickSupport</h1>
     * <p>purpose: Click on "SUPPORT" button<br>
     * 	App should navigate to /admin/support</p>
     * @return Support
     */
    public Support clickSupport() {
        getSupportButton().click();
        return new Support(driver);
    }

     /***
     * <h1>clickAccountSettings</h1>
     * <p>purpose: Click on "ACCOUNT SETTINGS" button<br>
     * 	App should navigate to /admin/my-account</p>
     * @return AccountSettings
     */
    public AccountSettings clickAccountSettings() {
        getAccountSettingsButton().click();
        return new AccountSettings(driver);
    }
}
