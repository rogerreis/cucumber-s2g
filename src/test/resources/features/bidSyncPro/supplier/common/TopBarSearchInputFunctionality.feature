@must @topBar @bidSearch_detail @level3
Feature: Dashboard - Top Bar Search Input Functionality
	This feature file exercises the functionality of the top bar search input on the Supplier Dashboard
	BIDSYNC-39: Search Criteria clears after looking at a bid
	

@BIDSYNC-39 @BIDSYNC-561
Scenario: Top Bar search results still display after user views bid. Filter Results displays keyword, but top bar search term is deleted.
	
	Given I have logged into the Supplier side as user "National FMC"
	And I run a search on the Top Bar with keyword "road"
	And I randomly select a bid from the currently displayed bid list from All BIDS tab
	And I will verify that all of the objects on the bid details screen have correctly populated
	When I click on the BACK TO BIDS button to return to the bid list
	Then I will verify that the Top Bar search term is blank
	And I will verify the Filter Results section displays a filter with only my Top Bar search term as the keyword after clicking Back to Bid
	And I will verify that All Bids is both highlighted and selected, and also displays correct results per the displayed Filter Results search filter


# --- Top Bar search doesn't combine with previous filters in Filter Results section ---#

@BIDSYNC-39 @BIDSYNC-568
Scenario: User runs Top Bar search. Search term deletes from Top Bar and displays in Filter Results as keyword. Previous filter is deleted from Filter Results.

	Given I have logged into the Supplier side as user "National FMC"
	And I run a generic search from the Filter Results section
	When I run a search on the Top Bar with keyword "road"
	Then I will verify that the Top Bar search term is blank
	And I will verify the Filter Results section displays a filter with only my Top Bar search term as the keyword
	
	
@BIDSYNC-39 @BIDSYNC-568
Scenario: User runs Top Bar search. Search term deletes from Top Bar and displays in Filter Results as keyword. Previously retrieved saved search filter is deleted from Filter Results.
	dmidura: The "Basic" account should be used in this test b/c it has saved searches

	Given I have logged into the Supplier side as user "Basic"
    And I retrieve the first search in the Saved Searches dropdown
	When I run a search on the Top Bar with keyword "road"
	Then I will verify that the Top Bar search term is blank
	And I will verify the Filter Results section displays a filter with only my Top Bar search term as the keyword
	
