package pages.bidSyncPro.supplier.account.myInfo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.pageObjects.BasePageActions;

public class RequestToJoinCompanyDialog extends BasePageActions {
    
    // ids
    private final String sendRequestButton_id = "sendRequestButton";
    private final String closeButton_id       = "nextButton";
    
    // Other
    private EventFiringWebDriver driver;
    
    RequestToJoinCompanyDialog(EventFiringWebDriver driver) {
    	super(driver);
        this.driver = driver;
    }
    
    /* ----- getters ----- */
    private WebElement getSendRequestButton() { return findByVisibility(By.id(this.sendRequestButton_id)); }
    private WebElement getCloseButton()       { return findByVisibility(By.id(this.closeButton_id));       }
    
    /* ----- methods ----- */

    public MyInfo sendRequest() {
        getSendRequestButton().click();
        getCloseButton().click();
        return new MyInfo(driver);
    }
}
