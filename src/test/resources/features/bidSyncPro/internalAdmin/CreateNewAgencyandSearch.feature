@should @administration @level4
Feature: BidSync Pro Internal Admin - Create New Agency and Search
	This will perform an admin login
	dmidura: Test is not functioning correctly, need to refactor
	
Scenario: Login as admin and create a new agency
	Given I navigate to the Admin login page	 
	And I enter my admin as "supplier@periscopeholdings.com" and Password as "test_1234"
	Then I will be on the admin Home page
	#When I create a new agency as "John's SSD"	
	When I Search for my agency as "Kirk's SSD"
	Then I will find my new agency
	When I select Metrics
	Then I will see the Metrics results	
	When I select collected solicitations
	When I search for my user as "clindley@mailinator.com"		