package stepDefinitions.bidSyncPro.registration;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import pages.bidSyncPro.registration.SelectRegions;
import pages.bidSyncPro.supplier.account.bidProfile.SalesTerritories;
import pages.bidSyncPro.supplier.dashboard.BidListTabNames;
import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

/**
 * This class is to keep the step definitions for Sales region selection on
 * registration process
 * 
 * @author ssomaraj
 */
public class SelectRegion_stepDefinitions {

	private EventFiringWebDriver driver;
	private SelectRegions selectRegions;
	private SalesTerritories salesTerritories;
	private HelperMethods helperMethods;

	public SelectRegion_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
		selectRegions = new SelectRegions(driver);
		salesTerritories = new SalesTerritories(driver);
		helperMethods = new HelperMethods();
	}

	@And("verify 'Select All' will select all the states and 'Clear All' will clear them all in Unites States tab$")
	public void verify_Select_All_will_select_all_the_states_and_Clear_All_will_clear_them_all_in_Unites_States_tab() {
		try {
			System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Select All and Clear All functionality test - US states");
			selectRegions.clickOnSelectAllUSButton().verifySelectAllSelectsAllStatesOrProvince(true);
			helperMethods.addSystemWait(2);
			selectRegions.clickOnListViewTab().verifyListView(true, true);
			helperMethods.addSystemWait(2);
			selectRegions.clickOnUSTab().clickOnClearAllUSButton().verifyClearAllClearsAllStatesOrProvince();
			helperMethods.addSystemWait(2);
			selectRegions.clickOnListViewTab().verifyListView(false, true);
		} catch (Exception exp) {
			fail("TEST FAILED : verify_Select_All_will_select_all_the_states_and_Clear_All_will_clear_them_all_in_Unites_States_tab()"
					+ exp);
		}
	}

	@And("verify 'Select All' will select all the province and 'Clear All' will clear them all in Canada tab$")
	public void verify_Select_All_will_select_all_the_province_and_Clear_All_will_clear_them_all_in_Canada_tab() {
		try {
			System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Select All and Clear All functionality test- Canadian province");
//  #Fix for release OCT 7
//	Added wait time for the page to load
			selectRegions.clickOnCanadaTab();
			helperMethods.addSystemWait(2);
			selectRegions.clickOnSelectAllCanadaButton()
					.verifySelectAllSelectsAllStatesOrProvince(false);
			helperMethods.addSystemWait(2);
			selectRegions.clickOnListViewTab().verifyListView(true, false);
			helperMethods.addSystemWait(2);
			selectRegions.clickOnCanadaTab().clickOnClearAllCanadaButton().verifyClearAllClearsAllStatesOrProvince();
			helperMethods.addSystemWait(2);
			selectRegions.clickOnListViewTab().verifyListView(false, false);
		} catch (Exception exp) {
			fail("TEST FAILED : verify_Select_All_will_select_all_the_states_and_Clear_All_will_clear_them_all_in_Unites_States_tab()"
					+ exp);
		}
	}

	@And("verify 'Select All' US states and 'Select All' Canada province will add up list view$")
	public void verify_Select_All_US_states_and_Select_All_Canada_province_will_add_up_list_view() {
		try {
			System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Select All US state and canada province to verify view list");
			boolean selectAll = true;
//  #Fix for release OCT 7
//			Added wait time for the page to load
			selectRegions.clickOnUSTab();
			helperMethods.addSystemWait(2);
			selectRegions.clickOnSelectAllUSButton();
			helperMethods.addSystemWait(2);
			selectRegions.clickOnCanadaTab().clickOnSelectAllCanadaButton();
			helperMethods.addSystemWait(2);
			selectRegions.clickOnListViewTab().verifyListView(selectAll);
		} catch (Exception exp) {
			fail("TEST FAILED : verify_Select_All_US_states_and_Select_All_Canada_province_will_add_up_list_view()"
					+ exp);
		}
	}

	@And("verify 'Select All' will select both us states and canada province and 'Clear All' will clear them off in list view$")
	public void verify_Select_All_will_select_both_us_states_and_canada__province_and_Clear_All_will_clear_then_off_in_list_view() {
		try {
			System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Select All US state and canada province from list view");
			boolean selectAll = true;
			boolean isUs = true;
			selectRegions.clickOnSelectAllListViewButton().verifyListView(selectAll).clickOnUSTab()
					.verifySelectAllSelectsAllStatesOrProvince(isUs);
			helperMethods.addSystemWait(2);
			isUs = false;// its canada
			selectRegions.clickOnCanadaTab().verifySelectAllSelectsAllStatesOrProvince(isUs)
											.clickOnListViewTab()
											.clickOnClearAllListViewButton();
			
		} catch (Exception exp) {
			fail("TEST FAILED : verify_Select_All_will_select_both_us_states_and_canada__province_and_Clear_All_will_clear_then_off_in_list_view()"
					+ exp);
		}

	}

	@And("verify state List options in list view and can add states$")
	public void verify_state_List_options_in_list_view_and_can_add_states() {
		try {
			selectRegions.clickOnListViewTab().getSearchState().click();
			selectRegions.verifyStateDropDownListOptions();
			salesTerritories.selectStateFromDropdownList(new ArrayList<String>(Arrays.asList("California","Texas","Arizona","Florida"))) ;
		} catch (Exception exp) {
			fail("TEST FAILED : verify_state_List_options_in_list_view()"
					+ exp);
		}
	}

	@And("verify sales region on filter section and bid selection$")
	public void verify_sales_region_on_filter_section_and_bid_selection() {
			helperMethods.addSystemWait(7);
			selectRegions.verifySalesRegionOnFilter();
			helperMethods.addSystemWait(2);
			//Verify States in All Bids Tab 
			selectRegions.verifyStateforBidList(false);
	}

	@And("verify state for all bids matches the profile region$")
	public void verify_state_for_all_bids_matches_the_profile_region() {
		try {
			selectRegions.verifyStateforBidList(true);
			new DashboardCommon(driver).openBidTab(BidListTabNames.ALL_BIDS);
			selectRegions.verifyStateforBidList(false);
		} catch (Exception exp) {
			fail("TEST FAILED : verify_state_for_all_bids_matches_the_profile_region()" + exp);
		}
	}
	
//	  #Fix for release OCT 7
//   Implemented the undefined step
	@And("verify save & continue button is not active$")
	public void verify_save_and_continue_button_is_not_active() {
		try {
			helperMethods.addSystemWait(2);
			selectRegions.verifyContinueButtonDisabled();
		} catch (Exception exp) {
			fail("TEST FAILED : verify_save_and_continue_button_is_not_active()" + exp);
		}
	}
}
