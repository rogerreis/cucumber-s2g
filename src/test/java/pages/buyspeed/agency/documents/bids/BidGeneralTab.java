package pages.buyspeed.agency.documents.bids;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import pages.common.helpers.DateHelpers;

/***
 * <h1>Class BidGeneralTab</h1>
 * @author dmidura
 * <p>details: This class houses page objects and methods for the Bid General screen
 * (the first tab in the Bid)
 */
public class BidGeneralTab extends BidNavigation {
	
	// xpaths
	// Bid Requireds
	private final String bidAvailableDateField_xpath   = "//input[@id='availableDateStr']";
	private final String bidOpeningDateField_xpath     = "//input[@id='openingDateStr']";
	
	// Custom Columns
	private final String bid_Vendor_TypeDropdown_xpath = "//select[@id='generic3']";
	private final String bid_TermDropdown_xpath        = "//select[@id='generic4']";
	
	// Bottom Button Panel
	private final String saveAndContinueBtn_xpath      = "//input[@type='button'][@value='Save & Continue']";

	// other
	private EventFiringWebDriver driver;	
	private DateHelpers   dateHelpers;

	public BidGeneralTab (EventFiringWebDriver driver) {
		super(driver);
		this.driver   = driver;
		dateHelpers   = new DateHelpers();
	}
	
	/* ----- getters ----- */
	

	// Bid Requireds
	private WebElement getBidAvailableDateField() { return findByVisibility(By.xpath(this.bidAvailableDateField_xpath)); }
	private WebElement getBidOpeningDateField()   { return findByVisibility(By.xpath(this.bidOpeningDateField_xpath));   }

	// Custom Columns
	private Select getBid_Vendor_TypeDropdown() { return new Select (findByVisibility(By.xpath(this.bid_Vendor_TypeDropdown_xpath))); }
	private Select getbid_TermDropdown()        { return new Select (findByVisibility(By.xpath(this.bid_TermDropdown_xpath)));       }

	// Bottom Button Panel
	private WebElement getSaveAndContinueBtn()  { return findByScrollIntoViewTopOfScreen(By.xpath(this.saveAndContinueBtn_xpath));         }
	
	/* ----- methods ----- */
	
	/***
	 * <h1>formatBidDateForBidFields</h1>
	 * <p>purpose: "Bid Available Date" and "Bid Opening Date" have<br>
	 * 	fields as MM/DD/YYYY hh:mm:ss <AM | PM>. Format Calendar in this String<br>
	 *  form, so you can just sendKeys() to fill the field, rather than<br>
	 *  using the date lookups</p>
	 * @param calendar = date to format
	 * @return String as MM/DD/YYYY hh:ss:ss <AM | PM>
	 */
	private String formatBidDateForBidFields(Calendar calendar) {
		Date date = calendar.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		return sdf.format(date);
	}

	/****
	 * <h1>setBidAvailableDateToNow</h1>
	 * <p>purpose: Set the Bid Available Date to the current time </p>
	 * @return BidGeneralTab
	 */
	public BidGeneralTab setBidAvailableDateToNow() {
		this.setBidAvailableDate(dateHelpers.getCurrentTimeAsCalendar());
		return this;
	}
	
	/***
	 * <h1>setBidAvailableDate</h1>
	 * <p>purpose: Set the bid available date</p>
	 * @param bidAvailableDate
	 * @return BidGeneralTab
	 */
	public BidGeneralTab setBidAvailableDate(Calendar bidAvailableDate) {
		this.getBidAvailableDateField().sendKeys(this.formatBidDateForBidFields(bidAvailableDate));
		return this;
	}

	/***
	 * <h1>setBidOpeningDate</h1>
	 * <p>purpose: Set the bid opening date</p>
	 * @param bidOpeningDate
	 * @return BidGeneralTab
	 */
	public BidGeneralTab setBidOpeningDate(Calendar bidOpeningDate) {
		this.getBidOpeningDateField().sendKeys(this.formatBidDateForBidFields(bidOpeningDate));
		return this;
	}

	/***
	 * <h1>setBidOpeningDateToOneDayFromNow</h1>
	 * <p>purpose: Set the bid opening date to one day from the current time</p>
	 * @return BidGeneralTab
	 */
	public BidGeneralTab setBidOpeningDateToOneDayFromNow() {
		Calendar bidOpeningDate = dateHelpers.addNumberOfDaysToCalendarDate(dateHelpers.getCurrentTimeAsCalendar(),1);
		this.setBidOpeningDate(bidOpeningDate);
		return this;
	}

	/***
	 * <h1>clickSaveAndContinueBtn</h1>
	 * <p>purpose: Click on the "Save & Continue" button on the Bid General tab</p>
	 * @return BidGeneralTab
	 */
	public BidGeneralTab clickSaveAndContinueBtn() {
		this.getSaveAndContinueBtn().click();
		return this;
	}

	/***
	 * <h1>setCustomColumnsOnBidGeneralTab</h1>
	 * <p>purpose: This methods sets the required custom columns (if they're enabled)
	 * 	on the Bid General screen to the first value in each columns dropdown</p>
	 * @return BidGeneralTab
	 */
	public BidGeneralTab setCustomColumnsOnBidGeneralTab() {
		// We don't care what value we set these to, only that they're
		// set if the bid is requiring them
		
		if (this.doesPageObjectExist(driver, By.xpath(this.bid_TermDropdown_xpath))) {
			this.getbid_TermDropdown().selectByIndex(1); }

		if (this.doesPageObjectExist(driver, By.xpath(this.bid_Vendor_TypeDropdown_xpath))) {
			this.getBid_Vendor_TypeDropdown().selectByIndex(1); }
		
		return this;
	}

}
