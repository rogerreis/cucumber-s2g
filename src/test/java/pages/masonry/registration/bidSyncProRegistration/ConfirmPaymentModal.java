package pages.masonry.registration.bidSyncProRegistration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class ConfirmPaymentModal</h1>
 * <p>details: This class houses page objects and methods for the "Confirm Payment" modal (4th and final modal in Chargebee<br>
 * 	punchout registration flow) that is part of the BidSync Pro registration off the Masonry site</p>
 */
public class ConfirmPaymentModal extends BasePageActions {
    
    // ids
    private final String acceptTOSCheckbox_id        = "tos_agreed";
    
    // xpaths
    private final String payAndSubscribeButton_xpath = "//button";
    
    // Other
    private EventFiringWebDriver driver;
    
    public ConfirmPaymentModal(EventFiringWebDriver driver) {
    	super(driver);
        this.driver        = driver;
    }
    
    /* ----- getters ----- */
    private WebElement getAcceptTOSCheckbox()     { return findByVisibility(By.id(this.acceptTOSCheckbox_id));           } 
    private WebElement getPayAndSubscribeButton() { return findByVisibility(By.xpath(this.payAndSubscribeButton_xpath)); } 

    /* ----- methods ----- */
    
    /***
     * <h1>agreeToTermsAndSubscribe</h1>
     * <p>purpose: Click on T&C checkbox to accept and then submit the subscription for the "Confirm Payment" modal<br>
     * 	(4th and final Chargebee modal in Chargebee punchout registration).<br>
     */
    public void agreeToTermsAndSubscribe() {
        getAcceptTOSCheckbox().click();
        getPayAndSubscribeButton().click();
        // done with modal windows, switch back to default
        driver.switchTo().defaultContent();
    }
}
