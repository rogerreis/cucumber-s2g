package pages.bidSyncPro.supplier.companySettings.manageSubscriptions;

import static org.junit.Assert.fail;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import data.bidSyncPro.registration.SubscriptionPlan;
import pages.bidSyncPro.common.DatabaseCommonSearches;
import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>ManageSubscriptionChargebeePopup</h1>
 * @author dmidura
 * <p>details: This class contains objects and methods to manipulate the <br>
 * 	"Manage Subscriptions" Chargebee Popup that appears after the user <br>
 *  clicks on "Manage Subscriptions" button on the Manage Subscriptions page</p>
 */
public class ManageSubscriptionsChargebeePopup extends BasePageActions {

	// xpaths
	private final String genericSubscription_xpath        = "//div[@data-cb-id=\"subscription_details\"]";
	private final String genericSubscriptionNameTxt_xpath = "//div[@data-cb-id='subscription_details']" +
			"//div[@class='cb-bar__title'][1]/span";
	private final String closeBtn_xpath                   = "//div[@data-cb-id='cb_close']//i";
	
	private final String bidSyncLogo_xpath                = "//div[@class='cb-header__logo']/img";
	private final String accountInformationBtn_xpath      = "//div[@data-cb-id='portal_account']";

	// Other
	private EventFiringWebDriver driver;
	private int NumberOfDisplayedSubscriptions;
	
	public ManageSubscriptionsChargebeePopup(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	/* ----- getters ----- */
	private int getNumberOfDisplayedSubscriptions()    { return this.NumberOfDisplayedSubscriptions; }

	private WebElement getGenericSubscriptionNameTxt() { return findByVisibility(By.xpath(this.genericSubscriptionNameTxt_xpath)); }
	private WebElement getCloseBtn()                   { return findByVisibility(By.xpath(this.closeBtn_xpath));                   }
	private WebElement getFirstSubscriptionBtn()       { return findByVisibility(By.xpath(this.genericSubscription_xpath + "[1]"));}
	
	/* ---- setters ----- */
	/***
	 * <h1>setNumberOfDisplayedSubscriptions</h1>
	 * <p>purpose: Determine the number of currently displaying subscriptions<br>
	 * 	and then set</p>
	 * @return
	 */
	private void setNumberOfDisplayedSubscriptions() {
		// Make sure that the iframe is actually displaying first
		waitForVisibility(By.xpath(this.bidSyncLogo_xpath));
		waitForVisibility(By.xpath(this.accountInformationBtn_xpath));

		List <WebElement> subscriptions = this.findAllOrNoElements(driver, By.xpath(this.genericSubscription_xpath));
		this.NumberOfDisplayedSubscriptions =  subscriptions.size();
	}
	
	/* ----- methods ----- */
	
	/***
	 * <h1>switchToPopup</h1>
	 * <p>purpose: Switch focus to the Chargebee Portal (iframe) popup</p>
	 * @return ManageSubscriptionsChargebeePopup
	 */
	public ManageSubscriptionsChargebeePopup switchToPopup() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Switching to Chargebee Portal iframe");
		driver.switchTo().frame("cb-frame");
		waitForVisibility(By.xpath(this.genericSubscription_xpath), Integer.parseInt(System.getProperty("defaultTimeOut")));
		return this;
	}

	/***
	 * <h1>closePopup</h1>
	 * <p>purpoose: Click "x" to close the Chargebee Portal popup</p>
	 * @return ManageSubscriptions
	 */
	public ManageSubscriptions closePopup() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Closing Chargebee Portal iframe, and returning to ARGO");
		this.getCloseBtn().click();
		driver.switchTo().parentFrame();
		return new ManageSubscriptions(driver);
	}
	
	/***
	 * <h1>clickOnSubscriptionToUpdate</h1>
	 * <p>purpose: Click on the first subscription on the Chargebee subscription punchout<br>
	 * 	in order to view the Subscription Details screen</p>
	 * @param SubscriptionPlan
	 * @return SubscriptionDetailsChargebeePopup  
	 */
	public SubscriptionDetailsChargebeePopup clickOnSubscriptionToUpdate() {
		
		// Click on the first Subscription
		this.getFirstSubscriptionBtn().click();
		
		return new SubscriptionDetailsChargebeePopup(driver);
	}
	
	/* ----- verifications ----- */
	
	/***
	 * <h1>verifyManageSubscriptionsOnlyDisplaysSubscription</h1>
	 * <p>purpose: Verify that there is only one subscription plan listed, and that it is subscriptionPlan<br>
	 * 	Fail test if there is not one subscription plan, there is more than one subscription plan, or the<br>
	 * 	subscription plan is not listed as strSubscriptionPlan</p>
	 * @param SubscriptionPlan
	 * @return ManageSubscriptionsChargebeePopup
	 */
	public ManageSubscriptionsChargebeePopup verifyManageSubscriptionsOnlyDisplaysSubscription(SubscriptionPlan subscriptionPlan) {
		this.setNumberOfDisplayedSubscriptions();
		
		// Verify that only one subscription plan is displaying
		if (this.getNumberOfDisplayedSubscriptions() == 0) {
			fail("Test Failed: No subscriptions are displaying in \"Manage Subscriptions\"");
		} else if (this.getNumberOfDisplayedSubscriptions() > 1) {
			fail("Test Failed: More than one subscription plan is listed in \"Manage Subscriptions\""); }
		
		// Then verify that the displayed subscription is our subscription
		if (!this.getGenericSubscriptionNameTxt().getText().equals(subscriptionPlan.getChargebeeSubscriptionDropdownName())) {
			fail("Test Failed: Displayed subscription is not \"" + subscriptionPlan.getChargebeeSubscriptionName() + "\""); }
		
		return this;
	}
	
	/***
	 * <h1>verifySubscriptionPlanUpdatesInARGODatabase</h1>
	 * <p>purpose: Verify that the subscription plan on the UI matches the<br>
	 * 	subscription plan listed in the ARGO database. Fail test if plans don't match</p>
	 * @param subscriptionPlan = SubscriptionPlan that is visible on the UI
	 * @param strUserEmail = user's registration email
	 * @return ManageSubscriptionsChargebeePopup
	 */
	public ManageSubscriptionsChargebeePopup verifySubscriptionPlanUpdatesInARGODatabase(SubscriptionPlan subscriptionPlan, String strUserEmail) {
		DatabaseCommonSearches databaseSearches = new DatabaseCommonSearches();

		// First get the listed subscription in the subscription table
		String subscriptionPlanInDatabase = databaseSearches.getChargebeeSubscriptionPlanFromUserEmail(strUserEmail.toLowerCase());
		
		// Now verify that it matches what was set on the UI
		if (!subscriptionPlanInDatabase.equals(subscriptionPlan.getChargebeeSubscriptionBackEndName())){
			fail ("Test Failed: Subscription plan in database is \"" + subscriptionPlanInDatabase + 
					"\", but UI was recorded as \"" + subscriptionPlan.getChargebeeSubscriptionBackEndName() + "\""); }
		
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that subscription plan was updated in both UI and database as \"" + subscriptionPlanInDatabase + "\"");

		return this;
	}
	

}
