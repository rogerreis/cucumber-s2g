package pages.bidSyncPro.supplier.account.bidProfile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.bidSyncPro.supplier.account.AccountCommon;
import pages.bidSyncPro.supplier.common.ChipList;

public class BidNotifications extends AccountCommon {

    // xpaths
    private final String updateKeywordsButton_xpath         = "//button[contains(@aria-label, 'update keywords')]";
    private final String notificationsToggle_xpath          = "//mat-slide-toggle";
    private final String keywordsChipList_xpath             = "//mat-chip[@color='accent']/../.";
    private final String negativeKeywordsChipList_xpath     = "//mat-chip[@color='danger']/../.";
    
    private final String bidNotificationToggleInput         = "//div[@class='mat-slide-toggle-bar']/input";
    
    public BidNotifications(EventFiringWebDriver driver) {
        super(driver);
    }
    
    /* ----- getters ----- */

    private WebElement getUpdateKeywordsButton() { return findByVisibility(By.xpath(this.updateKeywordsButton_xpath)); }
    private WebElement getNotificationsToggle()  { return findByVisibility(By.xpath(this.notificationsToggle_xpath));  }
    private WebElement getBidNotificationToggleInput()  { return findByVisibility(By.xpath(this.bidNotificationToggleInput));  }
    
    /* ----- methods ----- */
    
    /***
     * <h1>openKeywordsDialog</h1>
     * <p>purpose: Click on the edit (pencil) icon to view the keywords dialog</p>
     * @return KeywordsDialog
     */
    public KeywordsDialog openKeywordsDialog() {
        getUpdateKeywordsButton().click();
        return new KeywordsDialog(driver);
    }
    
    /***
     * <h1>toggleNotifications</h1>
     * <p>purpose: Click on the notifications slider to toggle checked/unchecked</p>
     * @return BidNotifications
     */
    public BidNotifications toggleNotifications() {
        getNotificationsToggle().click();
        return this;
    }
    
    /***
     * <h1>getKeywordsChiplist</h1>
     * <p>purpose: Access to the (positive) Keywords chip list.</p>
     * @return ChipList for the positive Keywords
     */
    private ChipList getKeywordsChipList() {
    	return new ChipList(driver, this.keywordsChipList_xpath);
    }

    /***
     * <h1>getKeywordsChiplist</h1>
     * <p>purpose: Access to the Negative Keywords chip list.</p>
     * @return ChipList for the negative keywords
     */
    private ChipList getNegativeKeywordsChipList() {
    	return new ChipList(driver, this.negativeKeywordsChipList_xpath);
    }
   
    private boolean areNotificationsActive() {
        return getNotificationsToggle().getAttribute("class").contains("mat-checked");
    }
    
    /***
     * <h1>waitForKeywordChipListToLoad</h1>
     * <p>purpose: wait for each chip in the (positive) keywords chip list
     * 	to be visible on the screen</p>
     * @return BidNotifications
     */
    public BidNotifications waitForKeywordChipListToLoad() {
    	this.getKeywordsChipList().getAllChipsInChipList();
    	return this;
    }
    
    public BidNotifications verifyBidNotificationIsOn(){
    	new WebDriverWait(driver, 20).withMessage("Daily bid notification toggle button should be enabled by default")
		.until(
			ExpectedConditions.attributeContains(By.xpath(bidNotificationToggleInput), "aria-checked", "true"));
    	return this;
    }
}
