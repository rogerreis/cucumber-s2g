@must @bidSearch_detail @level3
Feature: Dashboard > Bid Detail - Bid Details 
	ARGO-1656: Invalid bids should return "bid not found"
	ARGO-831: Bid Detail Page
	BIDSYNC-359: Special Characters in Document Names 

@ARGO-1656
Scenario: Invalid bid should return "Bid not Found"
	Given I have logged into the Supplier side as user "Basic"
	When I try to view an invalid bid
	Then I will see a bid not found screen

	
@ARGO-831 @BIDSYNC-359
Scenario Outline: Bid Details should appear for all bids coming from Buyspeed, Source, or Scraped Sites
dmidura: Current workaround if place for ARGO-2391: "Days Remaining" is confusing on Bid Details
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to the "All Bids" bid tab
	And I run a search on the Top Bar with keyword "<keyword>"
	When I randomly select a bid from the currently displayed bid list from All BIDS tab
	Then I will verify that all of the objects on the bid details screen have correctly populated
	
	Examples: Random bids to view
	| keyword |
	| water   |
	| truck   |
	| pipe    |
	| road    |
	| school  |
	

