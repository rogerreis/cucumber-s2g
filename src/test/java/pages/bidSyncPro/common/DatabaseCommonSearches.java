package pages.bidSyncPro.common;

import static org.junit.Assert.fail;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.assertj.core.api.SoftAssertions;

import data.bidSyncPro.registration.RandomChargebeeUser;
import data.bidSyncPro.registration.SubscriptionGrouping;
import pages.common.helpers.GlobalVariables;
import utility.database.SelectQueryBuilder;

/***
 * <h1>CommonSearches</h1>
 * @author dmidura
 * <p>details: This class contains common SQL searches that can be run on the ARGO database</p>
 */
public class DatabaseCommonSearches {
	SelectQueryBuilder dbQuery;
	
	public DatabaseCommonSearches() {
		dbQuery = new SelectQueryBuilder();
	}
	
	
	
	/*----- methods ----- */
	
	/***
	 * <h1>getUserIdFromUserEmail</h1> 
	 * <p>purpose: Given a user email address, return the user_id from<br>
	 * 	the database that is associated with your user.<br> 
	 * 	This is the user_id stored in the users table of the auth database</p>
	 * <p>query: select user_id from users where username = strUserEmail
	 * @param strUserEmail = user email that you registered with (note: email is case sensitive)
	 * @return String = user_id
	 */
	public String getUserIdFromUserEmail(String strUserEmail) {
		List <UUID> returns = 
				dbQuery.setDatabase("auth")
				.setTable("users")
				.setFilterColumn("username")
				.setFilterTerm(strUserEmail)
				.setResultColumn("user_id")
				.run();
	
		 if (returns.size() > 1) {
			fail("Test Failed: Database has more than one account for the user email " + strUserEmail); }
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: user_id returned from query = %s\n", returns.get(0));
		return returns.get(0).toString();
	}
	
	/***
	 * <h1>getSupplierIdFromCompanyNameAndOwnerEmail</h1>
	 * <p>purpose: Given a company name and the owner' email address<br>
	 * 	return the supplier_id for the company that is stored in the database.<br>
	 * 	This is the supplier_id stored in the supplier table of the supplier database</p>
	 * @param strCompanyName = name of company
	 * @param strOwnerEmail = company owner's registration email address (note: email is case sensitive)
	 * @return supplier_id
	 */
	public String getSupplierIdInSupplierDatabaseFromCompanyNameAndOwnerEmail(String strCompanyName, String strOwnerEmail) {
		
		// First get the company owner's user_id
		// the user_id is the supplier_owner id in the next query
		String user_id = this.getUserIdFromUserEmail(strOwnerEmail);
		
		// Search the database for your company
    	String strQuery       = "select * from supplier where name~'" + strCompanyName + "' and supplier_owner = '" + user_id + "'";
    	String strDBName      = "supplier";
		String strResultField = "supplier_id";
		List <String> returns = dbQuery.runSearchReturnField(strQuery, strDBName, strResultField);

		if (returns.size() > 1) {
			fail("Test Failed: Database has more than one record for company \"" + strCompanyName + "\" with owner \"" + strOwnerEmail +"\""); }
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: supplier_id in supplier database returned from query = %s\n", returns.get(0));
		return returns.get(0);
	}
	
	/***
	 * <h1>getSupplierIdInAuthDatabaseUserEmail</h1>
	 * <p>purpose: Given the user's registration email, get the supplier_id stored in the auth database in the users table</p>
	 * <p>query: select supplier_id from users where username = strUserEmail
	 * @param strUserEmail = user's registration email (note: email is case sensitive)
	 * @return String with the supplier_id
	 */
	public String getSupplierIdInAuthDatabaseUserEmail(String strUserEmail) {
		List <UUID> returns = dbQuery.setDatabase("auth")
				.setTable("users")
				.setFilterColumn("username")
				.setFilterTerm(strUserEmail)
				.setResultColumn("supplier_id")
				.run();
	
		if (returns.size() > 1) {
			fail("Test Failed: Database has more than one supplier_id for user \"" + strUserEmail + "\"");}
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: supplier_id in auth database returned from query = %s\n", returns.get(0).toString());
		return returns.get(0).toString();
}
	
	/***
	 * <h1>getChargebeeSubscriptionPlanFromUserEmail</h1>
	 * <p>purpose: Given a user email, retrieve the Chargebee Subscription Plan<br>
	 * 	currently recorded for the user in the ARGO database.</p>
	 * <p>query: select name from subscription where user_id = value_extracted_with_strUserEmail
	 * @param strUserEmail = user's registration email (note: email is case sensitive)
	 * @return Chargebee Subscription Plan
	 */
	public String getChargebeeSubscriptionPlanFromUserEmail(String strUserEmail) {
		
		// First, retrieve the user_id for this user
		String user_id = this.getUserIdFromUserEmail(strUserEmail);
		
		List <String> returns = dbQuery.setDatabase("subscription")
				.setTable("subscription")
				.setFilterColumn("user_id")
				.setFilterTerm(user_id)
				.setResultColumn("name")
				.run();

		if (returns.size() > 1) {
			fail("Test Failed: There is more than one subscription plan recorded in database for user \"" + strUserEmail + "\"");}
		if (returns.size() < 0) {
			fail("Test Failed: No subscription plan recorded in database for user \"" + strUserEmail + "\"");}
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: Subscription Plan returned from query = %s\n", returns.get(0));
		return returns.get(0);
		
	}
	
	/***
	 * <h1>getChargebeeSubscriptionPlanFromUserEmail</h1>
	 * <p>purpose: Given a user email, retrieve the Chargebee Subscription Plan<br>
	 * 	currently recorded for the user in the ARGO database.</p>
	 * <p>query: select name from subscription where user_id = value_extracted_with_strUserEmail
	 * @param strUserEmail = user's registration email (note: email is case sensitive)
	 * @return Chargebee Subscription Plan
	 */
	public ResultSet getChargebeeSubscriptionPlanFromUserEmail_ResultSet(String strUserEmail) {
		
		// First, retrieve the user_id for this user
		String user_id = this.getUserIdFromUserEmail(strUserEmail);
		
		ResultSet resultSet = dbQuery.setDatabase("subscription")
				.setTable("subscription")
				.setFilterColumn("user_id")
				.setFilterTerm(user_id)
				.setResultColumn("name")
				.returnResultSet();

		return resultSet;
		
	}
	
	/***
	 * <h1>getSubscriptionIdFromUserEmail</h1>
	 * <p>purpose: Given a user email, retrieve the subscription_id <br>
	 * 	currently recorded for the user in the ARGO database.</p>
	 * <p>query: select subscription_id from subscription where user_id = value_extracted_with_strUserEmail
	 * @param strUserEmail = user's registration email (note: email is case sensitive)
	 * @return subscription_id
	 */
	public String getSubscriptionIdFromUserEmail(String strUserEmail) {
		// First, retrieve the user_id for this user
		String user_id = this.getUserIdFromUserEmail(strUserEmail);

			List <UUID> returns = dbQuery.setDatabase("subscription")
				.setTable("subscription")
				.setFilterColumn("user_id")
				.setFilterTerm(user_id)
				.setResultColumn("subscription_id")
				.run();

		if (returns.size() > 1) {
			fail("Test Failed: There is more than one subscription id recorded in database for user \"" + strUserEmail + "\"");}
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: subscription_id returned from query = %s\n", returns.get(0));
		return returns.get(0).toString();
	}

	/***
	 * <h1>getSubscriptionAddOnsFromuserEmail</h1>
	 * <p>purpose: Given a user email, retrieve the Subscription Addons (as displayed in Chargebee) <br>
	 * 	that are currently recorded for the user in the ARGO database.</p>
	 * <p>query: select name from subscription where subscription_id = value_extracted_with_strUserEmail
	 * @param strUserEmail = user's registration email (note: email is case sensitive)
	 * @return List <String> SubscriptionAddOns displayed as in Chargebee (i.e. will need to extract your text)
	 */
	public List <String> getSubscriptionAddOnsFromUserEmail(String strUserEmail){
		// First, retrieve the subscription_id for this user
		String subscription_id = this.getSubscriptionIdFromUserEmail(strUserEmail);
		
			List <String> returns = dbQuery.setDatabase("subscription")
				.setTable("subscription_addon")
				.setFilterColumn("subscription_id")
				.setFilterTerm(subscription_id)
				.setResultColumn("name")
				.run();
	
		if (returns.size() > 3) {
			fail("Test Failed: There are more than 3 subscription addOns recorded for user with email \"" + strUserEmail + "\" in the supplier table");}

		System.out.printf(GlobalVariables.getTestID() + " INFO: Subscription AddOns returned from query = %s\n", returns);
		return returns;
		
	}
	
	/***
	 * <h1>getSubscriptionGroupingAndAdditionalStates</h1>
	 * <p>purpose: Given a user email, retrieve the Subscription Grouping and Additional States
	 * 	that are currently recorded for the user in the ARGO database.<br>
	 * 	Note: For Basic Plan, Notification State is the Grouping
	 * 	Note: States are displayed in full text, not as abbreviations
	 * @param strUserEmail = user's registration email (note: email is case sensitive)
	 * @return
	 */
	public List <String> getSubscriptionGroupingAndAdditionalStates(String strUserEmail) {
		// First retrieve the subscription_id for this user
		String subscription_id = this.getSubscriptionIdFromUserEmail(strUserEmail);
		
		// Now get the subscription groupings - this requires two calls
		// First get the list of grouping Ids
		List <UUID> groupingIds = dbQuery.setDatabase("subscription")
				.setTable("subscription_groupings")
				.setFilterColumn("subscription_id")
				.setFilterTerm(subscription_id)
				.setResultColumn("grouping_id")
				.run();
		
		// Now translate the groupingIds and return
		List <String> returns = new ArrayList<>();
		groupingIds.forEach(id->returns.add(translateGroupingId(id.toString())));

		System.out.printf(GlobalVariables.getTestID() + " INFO: Subscription Groupings and Additional States returned from query = %s\n", returns);
		return returns;
	}
	
	/***
	 * <h1>translateGroupingId</h1>
	 * <p>purpose: Helper to translate the groupingId retrieved in getSubscriptionGroupingAndAdditionalStates()
	 * 	into text value
	 * @param groupingId retrieved by getSubscriptionGroupingAndAdditionalStates()
	 * @return String for grouping (ex: "Idaho" )
	 */
	private String translateGroupingId(String groupingId) {
			List <String> returns = dbQuery.setDatabase("subscription")
				.setTable("grouping")
				.setFilterColumn("grouping_id")
				.setFilterTerm(groupingId)
				.setResultColumn("name")
				.run();
			
			if(returns.size() < 1) { fail("Test Failed: Grouping id \""+ groupingId + "\" is not mapped in database"); }
			if(returns.size() > 1) { fail("Test Failed: Grouping id \""+ groupingId + "\" is mapped more than once in database"); }
			
			return returns.get(0);
	}
	
	/* ----- verifications ----- */
	
	/***
	 * <h1>verifySubscriptionAndAddOns</h1>
	 * <p>purpose: Verify that the ARGO database has recorded subscription plan and grouping, subscription addOns,
	 * 	and additional states as expected. Expected values should be given in the RandomChargebeeUser user input.<br>
	 * 	Fail test if ARGO database has not correctly recorded subscription or addOns<br>
	 * 	Note: For Basic Plan, Grouping is the Notification State</p>
	 * @param user = RandomChargebeeUser data with expected SubscriptionPlan, SubscriptionAddOns, AdditionalStateAddOns populated
	 * @return DatabaseCommonSearches
	 */
	public DatabaseCommonSearches verifySubscriptionAndAddOns (RandomChargebeeUser user) {
		String email = user.getEmailAddress();
		
		// Subscription
		String recordedSubscription = this.getChargebeeSubscriptionPlanFromUserEmail(email);
		String expectedSubscription = user.getSubscriptionPlan().getChargebeeSubscriptionBackEndName();
		
		// Subscription AddOns
		List <String> recordedSubscriptionAddOns = this.getSubscriptionAddOnsFromUserEmail(email);
		List <String> expectedSubscriptionAddOns = user.getSubscriptionAddOnsInChargebeeFormat();
		
		// Grouping and Additional States
		List <String> recordedGroupingAndAdditionalStates = this.getSubscriptionGroupingAndAdditionalStates(email);
		List <String> expectedGroupingAndAdditionalStates = new ArrayList<>();
		user.getAdditionalStateAddOns().forEach(state->expectedGroupingAndAdditionalStates.add(state.getStateName()));
		if (!user.getSubscriptionGrouping().equals(SubscriptionGrouping.NONE)) { 
			// Grouping for Pro Plan
			expectedGroupingAndAdditionalStates.add(user.getSubscriptionGrouping().getChargebeeGroupingName()); 

		// Workaround to BIDSYNC-629: No Notification State set after Cancelling Pro Account
		// (When canceling Pro plan, account downgrades to Basic, but notification state is not correctly set.
		//  For the State Pro, the Basic currently uses the State plan's grouping. For Regional and National Plans,
		//	the notification state is null). Update from else if logic to else when workaround is complete.
		} else if ( 
				user.getSubscriptionGrouping().equals(SubscriptionGrouping.NONE) &
				user.getNotificationState() != null) {
			// Grouping for Basic Plan
			expectedGroupingAndAdditionalStates.add(user.getNotificationStateAsFullName());
		}
		
		// Workaround for BIDSYNC-406: Re-map entry for the state of Oklahoma in grouping_mapping table
		// When complete, remove this entire if statement
		int s = expectedGroupingAndAdditionalStates.indexOf("Oklahoma");
		if (s >= 0) {
			System.out.println(GlobalVariables.getTestID() + " INFO: Workaround for BIDSYNC-406. Oklahoma incorrectly mapped to New Mexico");
			expectedGroupingAndAdditionalStates.set(s, "New Mexico"); }
		
		// Now compare
		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(recordedSubscription)               .withFailMessage("Test Failed: Expected <%s> subscription plan, but database recorded <%s>",                         expectedSubscription,                recordedSubscription)               .isEqualTo(expectedSubscription);
		softly.assertThat(recordedSubscriptionAddOns)         .withFailMessage("Test Failed: Expected <%s> subscription addOns, but database recorded <%s>",                       expectedSubscriptionAddOns,          recordedSubscriptionAddOns)         .containsOnlyElementsOf(expectedSubscriptionAddOns);
		softly.assertThat(recordedGroupingAndAdditionalStates).withFailMessage("Test Failed: Expected <%s> subscription grouping + additional states, but database recorded <%s>", expectedGroupingAndAdditionalStates, recordedGroupingAndAdditionalStates).containsOnlyElementsOf(expectedGroupingAndAdditionalStates);
		
		softly.assertAll();
		return this;
	}
	
	
	
}
