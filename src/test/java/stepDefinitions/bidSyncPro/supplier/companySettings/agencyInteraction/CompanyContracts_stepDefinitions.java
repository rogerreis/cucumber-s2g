package stepDefinitions.bidSyncPro.supplier.companySettings.agencyInteraction;
 
import java.util.HashMap;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import pages.bidSyncPro.supplier.companySettings.agencyInteractions.PublicContractAgencySearchPage;
import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.toBeDeprecated.StepDefinition;

/***
 * <h1>Class CompanyContracts_stepDefinitions</h1>
 * <p>details: This class houses test steps to test the Company Contracts Page</p>
 * @author jlara
 */
public class CompanyContracts_stepDefinitions extends StepDefinition {
  
	private PublicContractAgencySearchPage companyContractsPage;
	HashMap<String, String> columnHeaders;
	
	public CompanyContracts_stepDefinitions(CoreAutomation automation) throws Throwable {
		super(automation);
		companyContractsPage = new PublicContractAgencySearchPage(driver);
		
		//put button keys (identifiers) and values (button text) into a map for looping
		columnHeaders = new HashMap<String, String>() {{
		}};
		
	} // end of constructor

		
	/* ------------ Test Steps ---------------- */


	/***
	 * <h1>I_have_navigated_to_the_Company_Contracts_Page</h1>
	 * <p>Purpose: Navigate to the Company Contracts Page</p>
	 * @param None
	 * @return None
	 * @throws Throwable
	 */
	@Given("^I have navigated to the Company Contracts Page$")
	public void I_have_navigated_to_the_Company_Contracts_Page() throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Navigate to the Company Contracts Page");

		companyContractsPage.verifyNavigation();
	}
	
	/***
	 * <h1>I_will_verify_that_the_Company_Contracts_Page_has_required_criteria_fields</h1>
	 * <p>Purpose: Verify that the Company Contracts Page has required criteria fields</p>
	 * @param None
	 * @return None
	 * @throws Throwable
	 */
	@And("^I will verify that the Company Contracts Page has required criteria fields$")
	public void I_will_verify_that_the_Company_Contracts_Page_has_required_criteria_fields() throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify that the Company Contracts Page has required criteria fields");

		helper.addSystemWait(2);
		//labels
		companyContractsPage.verifyInnerText("LabelSearchFor", "Search");
		companyContractsPage.verifyInnerText("LabelExpiring", "Expiring");
		companyContractsPage.verifyInnerText("LabelExpiringFrom", "From");
		companyContractsPage.verifyInnerText("LabelExpiringTo", "To");
		companyContractsPage.verifyInnerText("DirectionsDateFormat", "format date");
		
		//try to find title, description, and title & description label for radio buttons
		
		//inputs, radio buttons, select, submit
		companyContractsPage.verifyVisibility("SearchTerm");
		companyContractsPage.verifyVisibility("SearchTypeTitle");
		companyContractsPage.verifyVisibility("SearchTypeDescription");
		companyContractsPage.verifyVisibility("SearchTypeTitleDescription");
		companyContractsPage.verifyVisibility("ExpiringFrom");
		companyContractsPage.verifyVisibility("ExpiringTo");
		companyContractsPage.verifyVisibility("CheckboxCurrent");
		companyContractsPage.verifyVisibility("CheckboxExpired");
		companyContractsPage.verifyInnerText("FindButton", "Find");
	}
	
	/***
	 * <h1>I_will_verify_that_the_Company_Contracts_Page_has_required_Results_table_headers</h1>
	 * <p>Purpose: Verify that the Company Contracts Page has required Results table headers</p>
	 * @param None
	 * @return None
	 * @throws Throwable
	 */
	@And("^I will verify that the Company Contracts Page has required Results table headers$")
	public void I_will_verify_that_the_Company_Contracts_Page_has_required_Results_table_headers() throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify that the Company Contracts Page has required Results table headers");

		//column headers: Fill in when page is fixed
		//companyContractsPage.verifyInnerText("ResultsHdrOrderId", "Order ID");
	}
	
	/***
	 * <h1>I_will_verify_that_the_Company_Contracts_Page_Results_table_can_be_sorted</h1>
	 * <p>Purpose: Verify that the Company Contracts Page Results table can be sorted</p>
	 * @param None
	 * @return None
	 * @throws Throwable
	 */
	@And("^I will verify that the Company Contracts Page Results table can be sorted$")
	public void I_will_verify_that_the_Company_Contracts_Page_Results_table_can_be_sorted() throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify that the Company Contracts Page Results table can be sorted");

		//column headers: Fill in when page is fixed
		//companyContractsPage.click("ResultsHdrOrderId");
		//companyContractsPage.verifyVisibility("ResultsHdrOrderIdDesc");
		
	}
	
	
	/***
	* <h1>I_will_verify_that_the_Company_Contracts_Page_Search_returns_Results</h1>
	* <p>Purpose: Verify that the Company Contracts Page Search returns Results</p>
	* @param None
	* @return None
	* @throws Throwable
	*/
	@And("^I will verify that the Company Contracts Page Search returns Results$")
	public void I_will_verify_that_the_Company_Contracts_Page_Search_returns_Results() throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify that the Company Contracts Page Search returns Results");
	
		//column headers: Fill in when page is fixed
		//companyContractsPage.click("ResultsHdrOrderId");
		//companyContractsPage.verifyVisibility("ResultsHdrOrderIdDesc");
		
		//enter text in search and
		companyContractsPage.sendKeys("SearchTerm", "parts");
		companyContractsPage.click("SearchTypeTitleDescription");
		companyContractsPage.click("SearchTypeDescription");
		companyContractsPage.click("SearchTypeTitle");
		companyContractsPage.sendKeys("ExpiringFrom", "01/01/2000");
		companyContractsPage.sendKeys("ExpiringTo", "01/01/2020");
		companyContractsPage.click("CheckboxExpired");
		companyContractsPage.click("CheckboxCurrent");
		companyContractsPage.click("FindButton");
	}
	

}

