package pages.masonry.registration;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import data.bidSyncPro.registration.SubscriptionPlan;
import pages.common.helpers.GlobalVariables;
import pages.masonry.registration.bidSyncProRegistration.BidSyncProCheckout;

import java.util.Objects;

/***
 * <h1>Class Pricing</h1>
 * <p>details: This class houses page objects and methods for the Pricing page</p>
 */
public class Pricing extends CommonTopBar {
    
    // xpaths
    private final String mostPopularLabel_xpath        = "//div[contains(@class, 'orange-discout-text')]";
    private final String getStatePro_xpath_template    = "//a[contains(@href, '/checkout?i=123&s=%s')]";
    private final String getRegionalPro_xpath_template = "//a[contains(@href, '/checkout?i=124&s=%s')]";
    private final String getNationalPro_xpath_template = "//a[contains(@href, '/checkout?i=125&s=%s')]";
    private final String monthlyPlanRadioButton        = "//div[contains(@data-target, '.month1')]";
    private final String sixMonthPlanRadioButton       = "//div[contains(@data-target, '.month6')]";
    private final String annualPlanRadioButton         = "//div[contains(@data-target, '.month12')]";
    private final String comparePlansButton            = "//a[contains(@href, '/plans')][contains(@class, 'big-orange-btn')]";
    
    // Other
    private final String thisPagesURL                  = System.getProperty("bidSyncSiteURL") + "pricing";
    
    public Pricing(EventFiringWebDriver driver) {
        super(driver);
    }
    
    /* ----- getters ----- */
    private WebElement getBidsyncStateProButton(String durationCode) {
        // duration codes are either 'm', 's', or 'y' for monthly, six month, and annually, respectively
        String fullXpath = String.format(this.getStatePro_xpath_template, durationCode);
        return findByVisibility(By.xpath(fullXpath)); }
    
    private WebElement getBidSyncRegionalProButton(String durationCode) {
        String fullXpath = String.format(this.getRegionalPro_xpath_template, durationCode);
        return findByVisibility(By.xpath(fullXpath)); }
    
    private WebElement getBidSyncNationalProButton(String durationCode) {
        String fullXpath = String.format(this.getNationalPro_xpath_template, durationCode);
        return findByVisibility(By.xpath(fullXpath)); }
    
    private WebElement getMonthlyPlanRadioButton()  { return findByVisibility(By.xpath(this.monthlyPlanRadioButton));  } 
    private WebElement getSixMonthPlanRadioButton() { return findByVisibility(By.xpath(this.sixMonthPlanRadioButton)); } 
    private WebElement getAnnualPlanRadioButton()   { return findByVisibility(By.xpath(this.annualPlanRadioButton));   } 
    private WebElement getComparePlansButton()      { return findByVisibility(By.xpath(this.comparePlansButton));      } 

    /* ----- methods ----- */
    /***
     * <h1>navigateToHere</h1>
     * <p>purpose: Navigate to the the pricing page (bidSyncSiteURL/pricing)</p>
     * @return Pricing 
     */
    public Pricing navigateToHere() {
        driver.navigate().to(thisPagesURL);
        waitForVisibility(By.xpath(this.mostPopularLabel_xpath), 10);
        return this;
    }
    
    /***
     * <h1>clickComparePlansBtn</h1>
     * <p>purpose: Click on the "COMPARE PLANS" button to navigate to the Plans page</p>
     * @return Plans
     */
    public Plans clickComparePlansBtn() {
        findByScrollIntoViewBottomOfScreen(By.xpath(this.comparePlansButton));
        getComparePlansButton().click();
        return new Plans(driver);
    }
    
    /***
     * <h1>selectPaidPlans</h1>
     * <p>purpose: Based on the subscriptionPlan, first select the correct radio button<br>
     * 	for plan duration ("Monthly", "6 Month", "Annual"), and then click on the correct<br>
     * 	BidSync Pro Plan ("State", "Regional", "National"). After clicking on the plan,<br>
     * 	user will navigate to the checkout page</p>
     * @param subscriptionPlan = user's desired BidSync Pro subscription
     * @return BidSyncProCheckout
     */
    public BidSyncProCheckout selectPaidPlan(SubscriptionPlan subscriptionPlan) {
        Objects.requireNonNull(subscriptionPlan, "Subscription plan not populated.");
        System.out.println(GlobalVariables.getTestID() + " Selecting paid plan.");
        // Check correct radio button for plan duration
        String durationCode = "";
        if(subscriptionPlan.isMonthlyPlan()) {
            durationCode = "m";
            getMonthlyPlanRadioButton().click();
        } else if(subscriptionPlan.isSixMonthPlan()) {
            durationCode = "s";
            getSixMonthPlanRadioButton().click();
        } else if(subscriptionPlan.isAnnualPlan()) {
            durationCode = "y";
            getAnnualPlanRadioButton().click();
        }
        
        // Click on correct subscription plan button
        if(subscriptionPlan.isStatePlan()) {
            getBidsyncStateProButton(durationCode).click();
            return new BidSyncProCheckout(driver);
        } else if(subscriptionPlan.isRegionalPlan()) {
            getBidSyncRegionalProButton(durationCode).click();
            return new BidSyncProCheckout(driver);
        } else if(subscriptionPlan.isNationalPlan()) {
            getBidSyncNationalProButton(durationCode).click();
            return new BidSyncProCheckout(driver);
        }
    
        Assert.fail(String.format("Invalid subscription plan '%s'", subscriptionPlan.toString()));
        return null;
    }

}
