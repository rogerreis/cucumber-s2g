package pages.bidSyncClassic.myRFQ;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class SearchForSupplier</h1>
 * @author dmidura
 * <p>details: This class contains page objects and methods for the 
 * 	"Search for Supplier" screen accessed by clicking on "Supplier CRM" link
 *  on the "Information Center" screen of BidSync Classic MYRFQ.
 */
public class SearchForSupplier extends BasePageActions{
	
	// Search Bar xpaths -- add others as needed (OID, Name/Region, etc)
	private final String userSearch_xpath       = "//a[contains(text(),'User')]";
	
	// User Search Functionality xpaths
	private final String userSearchInput_xpath  = "//input[@type='text'][@name='usersrch']";
	private final String emailRadioButton_xpath = "//input[@type='radio'][@value='email']";
	private final String findButton_xpath       = "//a[contains(text(),'Find')]";
	
	// Results
	private final String resultsTable_xpath     = "//form[@name='search']//table[3]";
	private final String noResultsText_xpath    = this.resultsTable_xpath + "//td/strong[contains(text(),'No results found')]";
	private final String headerResultRow_xpath  = this.resultsTable_xpath + "//tr[@bgcolor='#687FAF']";  // dmidura: Gross, but not much else to use
	private final String lightResultRow_xpath   = this.resultsTable_xpath + "//tr[@bgcolor='#E0E0E0']";
	private final String darkResultRow_xpath    = this.resultsTable_xpath + "//tr[@bgcolor='#B0BDD8']";
	
	// Other 
	private EventFiringWebDriver driver;

	public SearchForSupplier(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
		new WebDriverWait(driver, DEFAULT_TIMEOUT)
		.withMessage("ERROR: Could not switch to frame to access \"Search for Supplier\"")
		.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("search"));
	}
	
	/* ----- getters ----- */
	
	// Search Bar xpaths -- add others as needed (OID, Name/Region, etc)
	private WebElement getUserSearch()       { return findByVisibility(By.xpath(this.userSearch_xpath));       }
	
	// User Search Functionality xpaths
	private WebElement getUserInput()        { return findByVisibility(By.xpath(this.userSearchInput_xpath));  }
	private WebElement getEmailRadioButton() { return findByVisibility(By.xpath(this.emailRadioButton_xpath)); }
	private WebElement getfindButton()       { return findByVisibility(By.xpath(this.findButton_xpath));       }

	// Results
	
	/* ----- methods ----- */
	
	/***
	 * <h1>selectUserSearch</h1>
	 * <p>purpose: Click on the "User" option in the top bar to view the User Search functionality
	 * @return SearchForSupplier
	 */
	public SearchForSupplier selectUserSearch() {
		this.getUserSearch().click();
		return this;
	}
	
	/***
	 * <h1>searchForUserByEmail</h1>
	 * <p>purpose: Assuming that the "User" option has been selected by the top bar view,
	 * 	select the "Email" radiobutton, enter email into "Search User" input, and click  "Find"
	 * @param email = user email to search with
	 * @return SearchForSupplier
	 */
	public SearchForSupplier searchForUserByEmail(String email) {
		this.getEmailRadioButton().click();
		this.getUserInput().sendKeys(email);
		this.getfindButton().click();
		return this;
	}
	
	/***
	 * <h1>selectReturnedUserRecord</h1>
	 * <p>purpose: Assuming you've run a search and there are some returned records available,
	 * 	click on the company link for the record associated with email to navigate to the 
	 * "General Information for the Supplier record
	 * @param email = user email associated with a supplier that you want to retrieve
	 */
	public SupplierGeneralInformation selectReturnedUserRecord(String email) {

		// Cycling through the results table and clicking on the customer associated with our email
		int count  = 0;
		int numLightRows = this.findAllOrNoElements(driver, By.xpath(this.lightResultRow_xpath)).size();
		int numDarkRows  = this.findAllOrNoElements(driver, By.xpath(this.darkResultRow_xpath)).size();
		
		for (int i=1; i < (numDarkRows + numLightRows) + 1; i++) {
			// Note: BS Classic isn't tagging the table. There is a blank row above the header, so in order to 
			// get to the actual data in the table, we'll need to skip two rows from the table start
			String thisRecord_xpath = String.format(this.headerResultRow_xpath + "/../tr[%s]/td[5]/a[contains(.,'%s')]", i + 2, email );
			if (this.doesPageObjectExist(driver, By.xpath(thisRecord_xpath))) { 
				// We found a match
				count = i; 
				break;
			}
		}
		Assert.assertTrue("ERROR: No record located for \"" + email + "\"", count != 0);
		
		// Now click on the customer link for this record
		String thisRecord_xpath = String.format(this.headerResultRow_xpath + "/../tr[%s]/td[2]/a", count + 2 );
		findByVisibility(By.xpath(thisRecord_xpath)).click();
		
		return new SupplierGeneralInformation(driver);
	}
	
	
	/* ----- Verifications ----- */
	
	/***
	 * <h1>verifyNumberOfRecordsReturned</h1>
	 * <p>purpose: Verify that exactly numberRecords was returned from the search.
	 * 	Fail test if not.
	 * @param minNumberRecords = number of records expected to be returned
	 * @return SearchForSupplier
	 */
	public SearchForSupplier verifyMinNumberRecordsReturned(int numberRecords) {
		if (numberRecords != 0) { 
			// Verify that the "No Results" text is not displayed
			waitForInvisibility(By.xpath(this.noResultsText_xpath), 2);
		
			// We need to find the row for minNumberRecords and verify nothing else is displayed
			// Note: BS Classic isn't tagging the table. There is a blank row above the header, so in order to 
			// get to the actual data in the table, we'll need to skip two rows from the table start
			String numberRecords_xpath = String.format(this.headerResultRow_xpath + "/../tr[%s]", numberRecords + 2 );
			String extraRecord_xpath   = String.format(this.headerResultRow_xpath + "/../tr[%s]", numberRecords + 2 + 1);

			// Also for some awesome reason, BS Classic is including table rows as spacers, so we're just going
			// to verify that all the rows we get are for actual content.
			String expectedRowColor = findByVisibility(By.xpath(numberRecords_xpath)).getAttribute("bgcolor");
			String extraRecordColor = findByVisibility(By.xpath(extraRecord_xpath)).getAttribute("bgcolor");
			Assert.assertTrue("ERROR: No results located",  ( expectedRowColor.equals("#E0E0E0") || expectedRowColor.equals("#B0BDD8") ));
			Assert.assertEquals("ERROR: Too many results returned", null, extraRecordColor);
			
		} else {
			// Verify no results
			try {
				waitForVisibility(By.xpath(this.noResultsText_xpath));
				waitForInvisibility(By.xpath(this.lightResultRow_xpath));
				waitForInvisibility(By.xpath(this.darkResultRow_xpath));
			} catch (TimeoutException t) {
				fail("ERROR: Results were returned, but expected no results");
			}
		}
		
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that only \"" + numberRecords + "\" results were returned by search");
		return this;
	}
}
