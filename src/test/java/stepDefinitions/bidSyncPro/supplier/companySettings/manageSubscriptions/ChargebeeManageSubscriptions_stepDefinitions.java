package stepDefinitions.bidSyncPro.supplier.companySettings.manageSubscriptions;

import core.CoreAutomation;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.bidSyncPro.registration.SubscriptionPlan;

import org.junit.Assert;

import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import utility.database.SQLConnector;

import static org.junit.Assert.*;

import java.sql.ResultSet;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.Select;

public class ChargebeeManageSubscriptions_stepDefinitions {

	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;

	public ChargebeeManageSubscriptions_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
		helperMethods = new HelperMethods();
	}

	/* ------------ Test Steps ---------------- */

	@Given("^I have clicked on the current subscription$")
	public void iHaveClickedOnTheSubscription() {
		driver.switchTo().frame("cb-frame");
		String subscriptionXpath = "//div[@data-cb-id='subscription_details']";
		helperMethods.waitForVisibilityOfXpath(driver, subscriptionXpath, 10);
		driver.findElement(By.xpath(subscriptionXpath)).click();
	}

	@Given("^I have clicked on Edit Subscription$")
	public void iHaveClickedOnEditSubscription() {
		WebDriverWait wait = new WebDriverWait(driver, 10, 100);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(),'Edit Subscription')]")));
		driver.findElement(By.xpath("//div[contains(text(),'Edit Subscription')]")).click();
	}

	@When("^I change my subscription to \"([^\"]*)\"$")
	public void iChangeMySubscriptionTo(String strSubscriptionSelection) {
		// click subscription dropdown
		WebDriverWait wait = new WebDriverWait(driver, 10, 100);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@class='cb-payment__select']")));
		Select dropdown = new Select(driver.findElement(By.xpath("//select[@class='cb-payment__select']")));
		driver.findElement(By.xpath("//select[@class='cb-payment__select']")).click();
		// select subscription
		dropdown.selectByVisibleText(strSubscriptionSelection);
		//System.out.printf(GlobalVariables.getTestID() + " INFO: strSubscriptionSelection = %s\n", strSubscriptionSelection);
		//helperMethods.addSystemWait(5);

	}

	@When("^click the Update Subscription button$")
	public void clickTheUpdateSubscriptionButton() {
		// click update subscription
		WebDriverWait wait = new WebDriverWait(driver, 20, 100);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(.,' Update Subscription ')]")));
		helperMethods.addSystemWait(4);
		driver.findElement(By.xpath("//button[contains(.,' Update Subscription ')]")).click();
		// verify we're back on Edit Subscription page and selected subscription is in place
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Subscription Details')]")));
		driver.findElement(By.xpath("//div[contains(text(),strSubscriptionSelection)]"));
	}
	
	@Then("^the subscriptions for user \"([^\"]*)\" will be updated in the database to show \"([^\"]*)\"$")
	public void theSubscriptionsForUserWillBeUpdatedInTheDatabaseToShow(String strEmail, String strSubscription) throws Throwable {
		SubscriptionPlan plan = SubscriptionPlan.fromString(strSubscription);
		// give the database a few seconds to update
		helperMethods.addSystemWait(15);
		// get the user_id
        String email = new SupplierLogin(driver).loadUserAccounts().get(strEmail).getEmail().toLowerCase();
		String strQuery  = "select user_id from users where username='" + email + "'";
		String strDBname = "auth";
		System.out.printf(GlobalVariables.getTestID() + " INFO: Running query \"%s\" on database \"%s\" \n", strQuery, strDBname);
		ResultSet rs = SQLConnector.executeQuery(strDBname, strQuery);
		String strUserID = null;
		while (rs.next()) { strUserID = rs.getString(1); }
		String message = String.format("No user_id found for username '%s'", email);
        Assert.assertNotNull(message, strUserID);
		// get the subscription
		strQuery  = "select name from subscription where user_id = '" + strUserID + "'";
		strDBname = "subscription";
		System.out.printf(GlobalVariables.getTestID() + " INFO: Running query \"%s\" on database \"%s\" \n", strQuery, strDBname);
		rs = SQLConnector.executeQuery(strDBname, strQuery);
		String strSubscriptionName = null;
		while (rs.next()) { strSubscriptionName = rs.getString(1); }
		//Changebee backend is changed 
		assertEquals(plan.getChargebeeSubscriptionBackEndName(), strSubscriptionName);
	}
}
