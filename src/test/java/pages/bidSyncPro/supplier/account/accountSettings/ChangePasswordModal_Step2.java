/**
 * 
 */
package pages.bidSyncPro.supplier.account.accountSettings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.HelperMethods;

/**
 * @author jlara
  * <p>TODO: Refactor this class</p>*
 */
public class ChangePasswordModal_Step2 {

	private final String xpathModal = "//bidsync-change-password/div/mat-card[@id='matChangePasswordComponentCard']";
	//text: Change Password
	private final String xpathHeaderLabel = xpathModal + "//mat-card-title[@id='matChangePasswordTitle']";
	//text: An email will be sent to <EMAIL> with a link to change your password
	private final String xpathDescriptionLabel = xpathModal + "//mat-card-content/div";
	
	//contains text: CANCEL
	private final String cancelButton_id = "btnCancel2";
	//contains text: CONFIRM
	private final String confirmButton_id = "btnConfirm2";
	
	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;

	public ChangePasswordModal_Step2(EventFiringWebDriver driver) {
	    this.driver = driver;
	    this.helperMethods = new HelperMethods();
	}

	public AccountSettings clickCancelButton() {
		getCancelButton().click();
		return new AccountSettings(driver);
	}

	public ChangePasswordModal_Step3 clickConfirmButton() {	
		getConfirmButton().click();
        helperMethods.addSystemWait(1);
		return new ChangePasswordModal_Step3(driver);
	}

	
	public WebElement getHeaderLabel() {
	    helperMethods.waitForVisibilityOfXpath(driver, xpathHeaderLabel, Integer.parseInt(System.getProperty("defaultTimeOut")));
	    return driver.findElement(By.xpath(xpathHeaderLabel));
	}

	public WebElement getDescriptionLabel() {
        helperMethods.waitForVisibilityOfXpath(driver, xpathDescriptionLabel, Integer.parseInt(System.getProperty("defaultTimeOut")));
	    return driver.findElement(By.xpath(xpathDescriptionLabel));
	}

	private WebElement getCancelButton() {
	    helperMethods.waitForVisibilityOfId(driver, this.cancelButton_id, Integer.parseInt(System.getProperty("defaultTimeOut")));
	    return driver.findElement(By.id(cancelButton_id));
	}

	private WebElement getConfirmButton() {
        helperMethods.waitForVisibilityOfId(driver, this.confirmButton_id, Integer.parseInt(System.getProperty("defaultTimeOut")));
	    return driver.findElement(By.id(confirmButton_id));
	}
	
	

}
