package pages.bidSyncPro.supplier.common;

import static org.junit.Assert.fail;

import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.common.helpers.GlobalVariables;

public class LocalBrowserStorageBidSyncPro {
	
	// Other
	private EventFiringWebDriver driver;
	
	public LocalBrowserStorageBidSyncPro(EventFiringWebDriver driver) {
		this.driver = driver;
	}
	
	/* ----- getters ------ */

	/***
	 * <h1>getPartialToken</h1>
	 * <p>purpose: Return the partialToken currently displayed in the local browser storage<br>
	 * 	or null if there is not partialToken</p>
	 * @return String
	 */
	public String getPartialToken() {
		Object partialToken = driver.executeScript("return localStorage.getItem('partialToken');");
		
		if (partialToken == null) { return null;}
		return partialToken.toString();
	}
		
	/**
	 * <h1>getPartialTokenExpiration</h1>
	 * <p>purpose: Grab the partialTokenExpiration from the local storage<br>
	 * 	Return as a Calendar object | null if no partialTokenExpiration exists in local storage</p>
	 * @return Calendar
	 */
	public Calendar getPartialTokenExpiration() {
		Object partialTokenExpiration = driver.executeScript("return localStorage.getItem('partialTokenExpiration');");

		// No partialTokenExpiration exists in local storage
		if (partialTokenExpiration == null) { return null;} 

		// partialTokenExpiration exists in local storage. Format as Calendar object
		Calendar cal = Calendar.getInstance();
		Date date = new Date();
		date.setTime(Long.parseLong(partialTokenExpiration.toString()));
		cal.setTime(date);
		return cal;
	}
	
	/**
	 * <h1>getRememberMe</h1>
	 * <p>purpose: Grab rememberMe from the local storage<br>
	 * 	Return as a String | null if no rememberMe exists in local storage</p>
	 * @return String
	 */
	public String getRememberMe() {
		Object rememberMe = driver.executeScript("return localStorage.getItem('rememberMe');");
		
		if (rememberMe == null) { return null; }
		return rememberMe.toString();
	}
	 
	/**
	 * <h1>getToken</h1>
	 * <p>purpose: Grab token from the local storage<br>
	 * 	Return as a String | null if no token exists in local storage</p>
	 * @return String
	 */
	 public String getToken() {
		Object token = driver.executeScript("return localStorage.getItem('token');");
		
		if (token == null ) {return null;}
		return token.toString();
	 }
	 
	/* ----- setters ----- */

	/***
	 * <h1>setPartialTokenExpiration</h1>
	 * <p>purpose: set the partialTokenExpiration in local browser storage to argument</p>
	 * @param setTime = Calendar time to set the partialTokenExpiration to
	 * @return ThreeLeveSecureAccess
	 */
	public LocalBrowserStorageBidSyncPro setPartialTokenExpiration(Calendar setTime) {
		String time = String.valueOf(setTime.getTimeInMillis());
		System.out.println(GlobalVariables.getTestID() + " INFO: Setting partialTokenExpiration to \"" + time + "\"");
		driver.executeScript("window.localStorage.setItem('partialTokenExpiration', arguments[0]);", time);
		return this;
	}

	/* ----- methods ----- */

	/***
	 * <h1>printLocalStorage</h1>
	 * <p>purpose: Print each local storage variables for three level access,<br>
	 * 	if each existing variable</p>
	 */
	public void printLocalStorage() {
			// Verify that rememberMe is active
		String token                    = this.getToken();
		String rememberMe               = this.getRememberMe();
		String partialToken             = this.getPartialToken();
		Calendar partialTokenExpiration = this.getPartialTokenExpiration();
		Object user                     = driver.executeScript("return localStorage.getItem('user');");
		
		System.out.println(GlobalVariables.getTestID() + " --------- Local Storage Values ----------------");
		if (token != null)               {System.out.println(GlobalVariables.getTestID() + " token        = " + token.toString()); }
		if (rememberMe != null)          {System.out.println(GlobalVariables.getTestID() + " rememberMe   = " + rememberMe.toString());}
		if (partialToken != null)        {System.out.println(GlobalVariables.getTestID() + " partialToken = " + partialToken.toString());}
		if (partialTokenExpiration!=null){System.out.println(GlobalVariables.getTestID() + " partialTokenExpiration = " + partialTokenExpiration.getTimeInMillis());}
		if (user != null)                {System.out.println(GlobalVariables.getTestID() + " user         = " + user.toString());}
		System.out.println(GlobalVariables.getTestID() + " -----------------------------------------------");
	}
	
	/***
	 * <h1>isRememberMeSetInLocalStorage</h1>
	 * <p>purpose: Determine if rememberMe is set to "true" in local storage</p>
	 * @return true is remember == true | false if rememberMe != true
	 */
	public Boolean isRememberMeSetInLocalStorage() {

		Object rememberMe = this.getRememberMe();
		if (rememberMe == null) { 
			return false;
		} else if ( !rememberMe.toString().equals("true")) {
			return false; }

		return true;
	}
	

	/***
	 * <h1>deleteToken</h1>
	 * <p>purpose: Delete the token from local storage<br>
	 * 	This can be used to simulate a token expiring, for <br>
	 * 	tests where we don't actually need to wait 30 minutes for token expiration</p>
	 */
	public void deleteToken() {
		Object token = this.getToken();
		if (token == null) {fail("ERROR: No token set in localStorage");}
		System.out.println(GlobalVariables.getTestID() + " INFO: token = " + token);

		driver.executeScript("localStorage.removeItem('token');");
	}
	
	/***
	 * <h1>waitForPartialToken</h1>
	 * <p>purpose: Wait for the partialToken to appear<br>
	 * 	in local storage. Fail test if partialToken does not appear after 8 seconds</p>
	 */
	public void waitForPartialToken() {
		new WebDriverWait(driver, 10).until(ExpectedConditions.jsReturnsValue("return localStorage.getItem('partialToken');"));
	}
	
}
