package pages.bidSyncPro.supplier.companySettings.agencyInteractions;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.toBeDeprecated.Document;

/**
* @author jlara
* <p>TODO: Refactor this class</p>
*/
public class PublicContractAgencySearchPage extends Document{

	public final static Map <String, String> REGIONS;

	static {
       Map<String, String> tm = new HashMap<>();
		tm.put("-1", "All Regions");
		tm.put("/al", "Alabama");
		tm.put("/ak", "Alaska");
		tm.put("/az", "Arizona");
		tm.put("/ar", "Arkansas");
		tm.put("/ca", "California");
		tm.put("/co", "Colorado");
		tm.put("/ct", "Connecticut");
		tm.put("/de", "Delaware");
		tm.put("/fl", "Florida");
		tm.put("/ga", "Georgia");
		tm.put("/hi", "Hawaii");
		tm.put("/id", "Idaho");
		tm.put("/il", "Illinois");
		tm.put("/in", "Indiana");
		tm.put("/ia", "Iowa");
		tm.put("/ks", "Kansas");
		tm.put("/ky", "Kentucky");
		tm.put("/la", "Louisiana");
		tm.put("/me", "Maine");
		tm.put("/md", "Maryland");
		tm.put("/ma", "Massachusetts");
		tm.put("/mi", "Michigan");
		tm.put("/mn", "Minnesota");
		tm.put("/ms", "Mississippi");
		tm.put("/mo", "Missouri");
		tm.put("/mt", "Montana");
		tm.put("/ne", "Nebraska");
		tm.put("/nv", "Nevada");
		tm.put("/nh", "New Hampshire");
		tm.put("/nj", "New Jersey");
		tm.put("/nm", "New Mexico");
		tm.put("/ny", "New York");
		tm.put("/nc", "North Carolina");
		tm.put("/nd", "North Dakota");
		tm.put("/oh", "Ohio");
		tm.put("/ok", "Oklahoma");
		tm.put("/or", "Oregon");
		tm.put("/pa", "Pennsylvania");
		tm.put("/ri", "Rhode Island");
		tm.put("/sc", "South Carolina");
		tm.put("/sd", "South Dakota");
		tm.put("/tn", "Tennessee");
		tm.put("/tx", "Texas");
		tm.put("/ut", "Utah");
		tm.put("/vt", "Vermont");
		tm.put("/va", "Virginia");
		tm.put("/wa", "Washington");
		tm.put("/dc", "Washington D.C.");
		tm.put("/wv", "West Virginia");
		tm.put("/wi", "Wisconsin");
		tm.put("/wy", "Wyoming");
		tm.put("/ab", "Alberta");
		tm.put("/bc", "British Columbia");
		tm.put("/mb", "Manitoba");
		tm.put("/nb", "New Brunswick");
		tm.put("/nl", "Newfoundland and Labrador");
		tm.put("/nt", "Northwest Territories");
		tm.put("/ns", "Nova Scotia");
		tm.put("/nu", "Nunavut");
		tm.put("/on", "Ontario");
		tm.put("/pe", "Prince Edward Island");
		tm.put("/qc", "Quebec");
		tm.put("/sk", "Saskatchewan");
		tm.put("/yt", "Yukon");

       REGIONS = Collections.unmodifiableMap(tm);
   }

	private final String xpath = "//form[@name='theform' and @action='/DPX']";

	private final String xpathSectionSearchHdrLabel = xpath + "//table/tbody/tr/th[normalize-space()='Search']";
	private final String xpathSectionAgencyColHdrLabel = xpath + "//table/tbody/tr/th[contains(.,'Agency')]";
	private final String xpathSectionContractsColHdrLabel = xpath + "//table/tbody/tr/th[normalize-space()='Contracts']";

	private final String xpathAgencyNameSearchLabel = xpath + "//table//td/b[text()='Agency Name']";
	private final String xpathAgencyNameSearchTextBox = xpath + "//input[@name='srchagname']";

	private final String xpathRegionSearchLabel = xpath + "//table//td/b[text()='Region']";
	private final String xpathRegionSearchSelect = xpath + "//select[@name='region']";
	private final String xpathSearchButton = xpath + "//a[@class='secondaryButton' and text()='Search']";

	public PublicContractAgencySearchPage(EventFiringWebDriver driver) throws Throwable {
		super(driver);

		//this is the url of the parent page. this page and its elements are contained in an iframe
		this.setPagePath(System.getProperty("supplierBaseURL")+ "admin/agency-interaction");



     //iframe[@src='https://stage.bidsync.com/DPX?ac=agencycontagency']
	} // end of constructor

	public WebElement getAgencyFromTable(String agencyName) throws Exception {
		if(driver == null)
			throw new Exception("Must driver not initialized.");
		String locExpr = String.format("%s//table/tbody/tr/td[@nowrap and contains(.,'%s')]/following-sibling::td/a[text()='Contracts']",xpath,agencyName);
		System.out.printf(GlobalVariables.getTestID() + " INFO: Adding PageElement %s {%s: %s } in %s%n",agencyName, "XPath", locExpr, this.getClass());
		return driver.findElement(By.xpath(locExpr));
	} // end of getAgencyFromTable()

	

	public PublicContractAgencySearchPage clickSearchButton() {	
		getSearchButton().click();
		return this;	
	}
	
	public PublicContractAgencySearchPage sendKeysAgencyNameSearchTextBox(CharSequence... keys) {
		this.getAgencyNameSearchTextBox().sendKeys(keys);
		return this;
	}
	

	public PublicContractAgencySearchPage selectRegionSearchOptionByValue(String optionValue) {
		getRegionSearchSelect().selectByValue(optionValue);
		return this;
	} // end of selectRegionSearchOptionByValue()
	

	public PublicContractAgencySearchPage selectRegionSearchOptionByText(String optionText) {
		getRegionSearchSelect().selectByVisibleText(optionText);
		return this;
	} // end of selectRegionSearchOptionByText()
	
	
	public WebElement getSectionSearchHdrLabel() {	return driver.findElement(By.xpath(xpathSectionSearchHdrLabel));	}

	public WebElement getSectionAgencyColHdrLabel() {	return driver.findElement(By.xpath(xpathSectionAgencyColHdrLabel));	}

	public WebElement getSectionContractsColHdrLabel() {	return driver.findElement(By.xpath(xpathSectionContractsColHdrLabel));	}

	public WebElement getAgencyNameSearchLabel() {	return driver.findElement(By.xpath(xpathAgencyNameSearchLabel));	}

	public WebElement getAgencyNameSearchTextBox() {	return driver.findElement(By.xpath(xpathAgencyNameSearchTextBox));	}

	public WebElement getRegionSearchLabel() {	return driver.findElement(By.xpath(xpathRegionSearchLabel));	}

	public Select getRegionSearchSelect() {	return new Select(driver.findElement(By.xpath(xpathRegionSearchSelect)));	}

	public WebElement getSearchButton() {	return driver.findElement(By.xpath(xpathSearchButton));	}

}
