package pages.bidSyncPro.supplier.dashboard.bidLists;

import com.google.common.collect.Comparators;

import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.RandomHelpers;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.text.WordUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import static org.junit.Assert.fail;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

public class BidList extends DashboardCommon {
    
    // xpaths
    // Column Headers
//    private final String columnHeaders_xpath  = "//div[contains(@class, 'header-title')]//a[not(ancestor::div[contains(@style, 'display: none')])]";
	private final String columnHeaders_xpath  = "//div[@class='header-title']//a";
    private final String matchHeader_xpath    = "//a[contains(.,'Match')]";
    private final String bidTitleHeader_xpath = "//a[contains(., 'Bid Title')]";
//    private final String bidIdHeader_xpath    = "//div[contains(@style, 'display: none;')]//a[contains(., 'Bid ID')]";
//   private final String bidIdHeader_xpath    = "(//div[@class='header-title']//a[contains(text(), 'Bid ID')])[1]";
//    private final String bidIdHeader_xpath    = "(//div[@class='header-title']//div[not(contains(@style,'none'))][3]//div//a[contains(text(), 'Bid ID')])";
    private final String bidIdHeader_xpath   = "(//div[@class='header-title']//a[contains(text(), 'Bid ID')])[%s]";
    
    private final String agencyHeader_xpath   = "//a[contains(., 'Agency')]";
//    private final String stateHeader_xpath    = "//div[contains(@style, 'display: none;')]//a[contains(., 'State')]";
    private final String stateHeader_xpath    = "(//div[@class='header-title']//a[contains(text(), 'State')])[2]";
    private final String endDateHeader_xpath  = "//a[contains(., 'End Date')]";
    private final String addedHeader_xpath    = "//a[contains(., 'Added')]";
    
    // Bids
    private final String allVisibleBids_xpath = "//mat-card-content//mat-list-item";
	private final String sorryNoSearchResultsTxt_xpath = "//mat-card-content[@id='matAllBidsContent']//div[@id='no-bids']"+
			 									"[contains(h3, 'Sorry, your search did not match any bids.;)]"+
												"[contains(strong, 'Try something like:')][contains(ul, 'Using more general terms')]" +
												"[contains(ul, 'Checking your spelling')]";
	private final String verifyEndDate_xpath = "//span[@aria-label='Bid End Date']";

	private final String bidMatchType = "//bidsync-relevancy-meter//span[contains(text(),'#MatchType#')]";
	private final String bidId = "//mat-list-item//span[contains(@aria-label,'bid number') and contains (text(),'#BidId#')]";
	// Load More
   private final String loadMoreBidsButton_id = "loadMoreBids";
    
    // other
    private int paginationLimit = 20;
    
    public BidList(EventFiringWebDriver driver) {
        super(driver);
    }
    
    /* ----- getters ----- */
    
    // Column Headers
    private WebElement getMatchHeader()        { return findByVisibility(By.xpath(this.matchHeader_xpath));      }
    private WebElement getBidTitleHeader()     { return findByVisibility(By.xpath(this.bidTitleHeader_xpath));   }
    private WebElement getBidIdHeader()   
    {
    	// Screen Size problem - Bid Id values displayed in two different places when minimize and maximize the screen.
    	String bidIdHeader_xpath1 = String.format(this.bidIdHeader_xpath, "1");
    	boolean isBidIdVisible = driver.findElement(By.xpath(bidIdHeader_xpath1)).isDisplayed();
    	if(!isBidIdVisible) {
    		bidIdHeader_xpath1 = String.format(this.bidIdHeader_xpath, "2");
    	}
    	return findByVisibility(By.xpath(bidIdHeader_xpath1));
    
    
    }
    private WebElement getAgencyHeader()       { return findByVisibility(By.xpath(this.agencyHeader_xpath));     }
    private WebElement getStateHeader()        { return findByVisibility(By.xpath(this.stateHeader_xpath));      }
    private WebElement getEndDateHeader()      { return findByVisibility(By.xpath(this.endDateHeader_xpath));    }
    private WebElement getAddedHeader()        { return findByVisibility(By.xpath(this.addedHeader_xpath));      }
    private WebElement getExpiredDate()        { return findByVisibility(By.xpath(this.verifyEndDate_xpath));      }

    // Load More
    private WebElement getLoadMoreBidsButton() { return findByVisibility(By.id(this.loadMoreBidsButton_id));     }
    private WebElement getBidMatchType(String matchType) { return findByVisibility(By.xpath(this.bidMatchType.replace("#MatchType#", matchType)));     }
    private WebElement getBidId(String bidId) { return findByVisibility(By.xpath(this.bidMatchType.replace("#BidId#", bidId)));     }
    
    /* ----- helpers ----- */
    /**
     * <h1>clickOnMatchHeader</h1>
     * Click on match header
     */
    public void clickOnMatchHeader() {
    	this.getMatchHeader().click();
    }
    /***
     * <h1>getNumberOfCurrentlyDisplayedBids</h1>
     * <p>purpose: Quick method to get the number of currently<br>
     * 	displayed bids. This does not populate the BidList, just<br>
     * 	gives a quick count of the number of visible bids.</p>
     * @return int = number of currently displayed bids
     */
    public int getNumberOfCurrentlyDisplayedBids() {
        List<WebElement> bidElements = this.findAllOrNoElements(driver, By.xpath(allVisibleBids_xpath));
        return bidElements.size();
    }
    
    /***
     * <h1>isLoadMoreBidsDisplaying</h1>
     * <p>purpose: Determine if load more bids button <br>
     * 	is currently displayed on screen</p>
     * @return true if displaying | false if not displaying
     */
    private Boolean isLoadMoreBidsDisplaying() {
    	return !driver.findElements(By.id(this.loadMoreBidsButton_id)).isEmpty();
    }

     private List<List<BidListRow>> partitionBidsBy(List<BidListRow> bids, String column) {
        List<List<BidListRow>> partitions = new ArrayList<>();
        List<BidListRow> partition = new ArrayList<>();
        for(BidListRow bid : bids) {
            if(partition.isEmpty()) {
                partition.add(bid);
            } else {
                BidListRow previousBid = partition.get(partition.size() - 1);
                if(!bid.columnsMatch(previousBid, column)) {
                    partitions.add(partition);
                    partition = new ArrayList<>();
                }
                partition.add(bid);
            }
        }
        partitions.add(partition);
        return partitions;
    }
    
    /* ----- methods ----- */
    
    
    /* ----- column headers ----- */
    
    /***
     * <h1>columnHeaders</h1>
     * <p>purpose: Retrieves the web elements for the bid table column headers</p>
     * @return List<WebElement>
     */
    public List<WebElement> columnHeaders() {
        waitForVisibility(By.xpath(this.columnHeaders_xpath));
        return this.driver.findElements(By.xpath(columnHeaders_xpath));
    }

     /***
     * <h1>sortByColumn</h1>
     * <p>purpose: clicks the given column header to sort the table by that column</p>
     * @param columnName - name of the column to sort by
     * @return void
     */
    public void sortByColumn(String columnName) {
    	// Make sure that the column header is in the viewport before attempting to click header rows
    	waitForSpinner(driver);
    	findByScrollIntoViewBottomOfScreen(By.xpath(this.bidTitleHeader_xpath));
        switch(columnName) {
            case "Bid Title":
                getBidTitleHeader().click();
                break;
            case "Bid ID":
                getBidIdHeader().click();
                break;
            case "Agency":
                getAgencyHeader().click();
                break;
            case "State":
                getStateHeader().click();
                break;
            case "End Date":
                getEndDateHeader().click();
                break;
            case "Added":
                getAddedHeader().click();
                break;
            default:
                String message = String.format("No column header named %s.", columnName);
                Assert.fail(message);
        }

        // after selecting sort method, wait for unsorted list to update
        waitForSpinner(driver);
    }
    
    /* ----- Bid Count Banner -----*/
    /***
     * <h1>getBidCountBanner</h1>
     * <p>purpose: Access to the Bid Count Banner<br>
     *  Note: Bid Count Banner should only appear on the "All Bids" list
     * @return BidCountBanner
     */
    public BidCountBanner getBidCountBanner() {
    	return new BidCountBanner(driver);
    }
    
    /* ----- bids in list ----- */
    
    /***
     * <h1>allVisibleBids</h1>
     * <p>purpose: Returns all bids visible in the table as a list of BidListRow Objects</p>
     * @return List<BidListRow>
     */
    public List<BidListRow> allVisibleBids() {
        return this.firstNumberOfVisibleBids(this.getNumberOfCurrentlyDisplayedBids());
    }
    
    /***
     * <h1>getBidsTillMatchType</h1>
     * <p>purpose: Returns all bids visible in the table as a list of BidListRow Objects</p>
     * @return List<BidListRow>
     */
    public List<BidListRow> getBidsTillMatchType(String matchType) {
        return this.getBidListTillMatchType(matchType);
    }

    /***
     * <h1>allVisibleBids in saved  Bid Tab</h1>
     * <p>purpose: Returns all bids visible in the table as a list of BidListRow Objects</p>
     * @return List<BidListRow>
     */
    public List<BidListRow> allVisibleBidsInNewForYouTab() {
        return this.firstNumberOfVisibleBidsForNewForYouBids(this.getNumberOfCurrentlyDisplayedBids());
    }


    /***
     * <h1>firstNumberOfVisibleBids</h1>
     * <p>purpose: Returns the first numVisibleBids bids visible in the table as a list of BidListRow Objects, where 
     * 	the first bid is displayed immediately below the column headers on table load, and the second bid below and so on</p>
     * @param numVisibleBids = the number of bids to be returned. First bid is 1. <br>
     * 	Note: Use allVisibleBids() to get complete list of all the visible bids. 
     * @return List<BidListRow>
     */
    public List<BidListRow> firstNumberOfVisibleBids(int numVisibleBids) {
    	Assert.assertTrue("ERROR: There are less than \"" + numVisibleBids + "\" visible bids displaying", numVisibleBids <= this.getNumberOfCurrentlyDisplayedBids());
        List<BidListRow> bidListRows = new ArrayList<>();
        for(int i=1; i < numVisibleBids + 1; ++i) {
            String thisRowsXpath = String.format("%s[%d]", allVisibleBids_xpath, i);
            // bidListRows.add(new BidListRow(this.driver, thisRowsXpath, false)); # Please dont add param 'true/false' as either required from this step   
            bidListRows.add(new BidListRow(this.driver, thisRowsXpath)); 
        }

        return bidListRows;
    }
    
    /***
     * <h1>firstNumberOfVisibleBids</h1>
     * <p>purpose: Returns the first numVisibleBids bids visible in the table as a list of BidListRow Objects, where 
     * 	the first bid is displayed immediately below the column headers on table load, and the second bid below and so on</p>
     * @param numVisibleBids = the number of bids to be returned. First bid is 1. <br>
     * 	Note: Use allVisibleBids() to get complete list of all the visible bids. 
     * @return List<BidListRow>
     */
	public List<BidListRow> getBidListTillMatchType(String matchType) {
//    	Assert.assertTrue("ERROR: There are less than \"" + numVisibleBids + "\" visible bids displaying", numVisibleBids <= this.getNumberOfCurrentlyDisplayedBids());
		List<BidListRow> bidListRows = new ArrayList<>();
		int i = 0;
		waitForSpinner(driver);
		while ((!doesPageObjectExist(driver,
				By.xpath(this.bidMatchType.replace("#MatchType#", WordUtils.capitalizeFully(matchType)))))
				&& doesPageObjectExist(driver, By.id(this.loadMoreBidsButton_id)) && i < 8) {
			scrollIntoViewBottomOfScreen(getLoadMoreBidsButton());
			getLoadMoreBidsButton().click();
		}
		for (i = 1; i < this.getNumberOfCurrentlyDisplayedBids() + 1; ++i) {
			String thisRowsXpath = String.format("%s[%d]", allVisibleBids_xpath, i);
			bidListRows.add(new BidListRow(this.driver, thisRowsXpath));
		}

		return bidListRows;
	}
	
	
	public List<BidListRow> getBidListTillBidIdExists(String bidId) {
//    	Assert.assertTrue("ERROR: There are less than \"" + numVisibleBids + "\" visible bids displaying", numVisibleBids <= this.getNumberOfCurrentlyDisplayedBids());
		List<BidListRow> bidListRows = new ArrayList<>();
		int i = 0;
		waitForSpinner(driver);
		while ((!doesPageObjectExist(driver,
				By.xpath(this.bidId.replace("#BidId#", WordUtils.capitalizeFully(bidId)))))
				&& doesPageObjectExist(driver, By.id(this.loadMoreBidsButton_id)) && i < 8) {
			scrollIntoViewBottomOfScreen(getLoadMoreBidsButton());
			getLoadMoreBidsButton().click();
		}
		for (i = 1; i < this.getNumberOfCurrentlyDisplayedBids() + 1; ++i) {
			String thisRowsXpath = String.format("%s[%d]", allVisibleBids_xpath, i);
			bidListRows.add(new BidListRow(this.driver, thisRowsXpath));
		}

		return bidListRows;
	}

    /***
     * <h1>firstNumberOfVisibleBids</h1>
     * <p>purpose: Returns the first numVisibleBids bids visible in the table as a list of BidListRow Objects, where
     * 	the first bid is displayed immediately below the column headers on table load, and the second bid below and so on</p>
     * @param numVisibleBids = the number of bids to be returned. First bid is 1. <br>
     * 	Note: Use allVisibleBids() to get complete list of all the visible bids.
     * @return List<BidListRow>
     */
    public List<BidListRow> firstNumberOfVisibleBidsForNewForYouBids(int numVisibleBids) {
        Assert.assertTrue("ERROR: There are less than \"" + numVisibleBids + "\" visible bids displaying", numVisibleBids <= this.getNumberOfCurrentlyDisplayedBids());
        List<BidListRow> bidListRows = new ArrayList<>();
        for(int i=1; i < numVisibleBids + 1; ++i) {
            String thisRowsXpath = String.format("%s[%d]", allVisibleBids_xpath, i);
            bidListRows.add(new BidListRow(this.driver, thisRowsXpath));
        }

        return bidListRows;
    }

    
    /***
     * <h1>getFirstVisibleBid</h1>
     * <p>purpose: Quickly access the first bid. Also scroll to bid<br>
     * @return BidListRow for the first bid in the list in All bids tab
     */
    public BidListRow getFirstVisibleBid() {
    	return this.getBidByIndexInAllBids(0);
    }
    

    /***
     * <h1>getLastVisibleBid</h1>
     * <p>purpose: Quickly access the last visible bid. Also scroll to bid<br>
     * @return BidListRow for the last visible bid in the list
     */
    public BidListRow getLastVisibleBidInNewForYouTab() {
    	int index = this.getNumberOfCurrentlyDisplayedBids() == 0 ? 0 : this.getNumberOfCurrentlyDisplayedBids() -1;
    	return this.getBidByIndex(index);
    }

    /***
     * <h1>getLastVisibleBid</h1>
     * <p>purpose: Quickly access the last visible bid. Also scroll to bid<br>
     * @return BidListRow for the last visible bid in the list in either
     */
    public BidListRow getLastVisibleBid() {
        int index = this.getNumberOfCurrentlyDisplayedBids() == 0 ? 0 : this.getNumberOfCurrentlyDisplayedBids() -1;
        return this.getBidByIndexInAllBids(index);
    }
    
   
    /***
     * <h1>getRandomBidInVisibleList</h1>
     * <p>purpose: Randomly select a bid in the visible bid list. Scroll list to display to bid</p>
     * @return BidListRow for the selected bid
     */
    public BidListRow getRandomBidInVisibleList() {
        waitForSpinner(driver);
    	int index = new RandomHelpers().getRandomInt(this.getNumberOfCurrentlyDisplayedBids());
    	return getBidByIndex(index);
    }

    /***
     * <h1>getRandomBidInVisibleList</h1>
     * <p>purpose: Randomly select a bid in the visible bid list in All Bids. Scroll list to display to bid</p>
     * @return BidListRow for the selected bid
     */
    public BidListRow getRandomBidInVisibleListInAllBidsTab() {
        waitForSpinner(driver);
        int index = new RandomHelpers().getRandomInt(this.getNumberOfCurrentlyDisplayedBids());
        return getBidByIndexInAllBids(index);
    }

    /***
     * <h1>getBidByIndex</h1>
     * <p>purpose: Returns a specified bid in by index. Bid will automatically be scrolled into view</p>
     * @param index - which bid to get. 0 is the first bid.
     * @return BidListRow
     */
    public BidListRow getBidByIndex(int index) {
    	System.out.println(GlobalVariables.getTestID() + " INFO: Getting bid at index \"" + index + "\" out of \"" + (this.getNumberOfCurrentlyDisplayedBids()) + "\" displayed bids");
    	Assert.assertTrue("ERROR: Index out of bounds ", index <= this.getNumberOfCurrentlyDisplayedBids()-1);
        String thisRowsXpath = String.format("%s[%d]", allVisibleBids_xpath, index + 1);
        //#Fix for release OCT 7
        scrollIntoViewBottomOfScreen(By.xpath(thisRowsXpath));
        return (new BidListRow(this.driver, thisRowsXpath));
    }

    /***
     * <h1>getBidByIndex</h1>
     * <p>purpose: Returns a specified bid in by index. Bid will automatically be scrolled into view</p>
     * @param index - which bid to get. 0 is the first bid.
     * @return BidListRow
     */
    public BidListRow getBidByIndexInAllBids(int index) {
        System.out.println(GlobalVariables.getTestID() + " INFO: Getting bid at index \"" + index + "\" out of \"" + (this.getNumberOfCurrentlyDisplayedBids()) + "\" displayed bids");
        Assert.assertTrue("ERROR: Index out of bounds ", index <= this.getNumberOfCurrentlyDisplayedBids()-1);
        String thisRowsXpath = String.format("%s[%d]", allVisibleBids_xpath, index + 1);
        //#Fix for release OCT 7
        scrollIntoViewBottomOfScreen(By.xpath(thisRowsXpath));
        return (new BidListRow(this.driver, thisRowsXpath));
    }
    
    /***
     * <h1>getBidById</h1>
     * <p>purpose: Returns the bid with the ID that matches the given ID</p>
     * @param id - which id to search for
     * @return BidListRow
     */
    public BidListRow getBidById(String id) {
        List<BidListRow> allBids = allVisibleBids();
        return CollectionUtils.extractSingleton(CollectionUtils.select(allBids, bidListRow -> bidListRow.bidInfo.getBidId().equals(id)));
    }
    
    /***
     * <h1>getBidByBidNumber</h1>
     * <p>purpose: Return the bid with the bid number that matches bidNumber</p>
     * @param bidNumber - bid number<br>
     * 	ex: "ADSPO17-00005651"</p>
     * @return BidListRow
     */
    public BidListRow getBidByBidNumber (String bidNumber) {
        List<BidListRow> allBids = allVisibleBids();
        return CollectionUtils.extractSingleton(CollectionUtils.select(allBids, bidListRow -> bidListRow.bidInfo.getBidNumber().equals(bidNumber)));
    }
    
    /***
     * <h1>waitForBids</h1>
     * <p>purpose: This is a workaround method created to deal with slow loading bids per BIDSYNC-546: Improve performance of bid searches<br>
     * 	Make the "Your Saved Bids" list wait until all the saved bids have loaded. Saved bids are identified by bid id
     * @param bidIds = list of bid ids that should be displayed (i.e. all the saved bids)
     * @return BidList
     */
    public BidList waitForBidsInYourSavedBids(List <String> bidIds) {
    	System.out.println(GlobalVariables.getTestID() + " INFO: Workaround for BIDSYNC-546. Bids in \"Your Saved Bids\" can take a long time to load");
        
        for (String id : bidIds) {
        	String xpath = this.allVisibleBids_xpath + String.format("//div[contains(@class, 'result-title')]//a[contains(@href, '%s')]", id);
        	findByVisibility(By.xpath(xpath));
        }

    	return this;
    }

   
    /* ----- pagination ----- */
    
    /***
     * <h1>loadMoreBids</h1>
     * <p>purpose: Click on the "Load More Bids" button.
     * 	Then, wait for spinner to allow bids to load<br>
     * 	Note: This method expects "Load More Bids" button to
     * 	be available and will fail test if not available.
     * 	Use loadMoreBidsIfAvailable() if you want to 
     * 	click "Load More Bids" only if it is available</p>
     * @return BidList
     */
    public BidList loadMoreBids() {
        getLoadMoreBidsButton().click();
        waitForSpinner(driver);
        return this;
    }

    /***
     * <h1>loadMoreBidsIfAvailable</h1>
     * <p>purpose: If available, click on the "Load More Bids" button.
     * 	Then, wait for spinner to allow bids to load
     * @return BidList
     */
	public BidList loadMoreBidsIfAvailable() {
		scrollIntoViewBottomOfScreen(
				findByVisibility(By.xpath("//div[@class='environment ng-tns-c0-0 ng-star-inserted']")));
		if (this.isLoadMoreBidsDisplaying()) {
			getLoadMoreBidsButton().click();
			waitForSpinner(driver);
		}
		return this;
	}
    
    /***
     * <h1>paginateThroughAllBids</h1>
     * <p>purpose: When the number of bids in the bid list can be <br>
     * 	divided by 20 (our pagination limit), click on the <br>
     * 	"Load More Bids" button, if the button is visible</p>
     * @return BidList
     */
    public BidList paginateThroughAllBids() {

    	// Based on the current number of visible bids,
    	// determine if we might need to paginate
    	// (we shouldn't paginate if we haven't reached the paginationLimit)
    	int numberOfBids = this.getNumberOfCurrentlyDisplayedBids();
    	
    	System.out.println(GlobalVariables.getTestID() + " INFO: There are \"" + numberOfBids + "\" bids currently displaying in bid list");
    	while (numberOfBids % this.paginationLimit == 0) {

    		// If we've reached the pagination limit, look for load more bids button
    		if (this.isLoadMoreBidsDisplaying()) { 

    			// and click if visible
    			System.out.println(GlobalVariables.getTestID() + " INFO: Clicking \"Load More Bids\" button to paginate bids");
    			this.loadMoreBids(); 
    			
    			// and let the list update
    			numberOfBids = this.getNumberOfCurrentlyDisplayedBids();
    			System.out.println(GlobalVariables.getTestID() + " INFO: There are \"" + numberOfBids + "\" bids currently displaying in bid list");
    		} else {
    			// if not visible, then we're viewing the case of the returned number of bids
    			// meets our pagination limit, but we don't have more bids
    			break; }
    	}
    	
    	return this;
    }
     
    /* ----- verifications ----- */

    /***
     * <h1>isSortedBy</h1>
     * <p>purpose: Returns true if the bid table is sorted by the given column and in the given sort order</p>
     * @param column - name of the column that the table should be sorted by
     * @param sortOrder - either "ascending" or "descending"
     * @return boolean
     */
    public boolean isSortedBy(String column, String sortOrder) {
        if (!sortOrder.equals("ascending") && !sortOrder.equals("descending")) {
            Assert.fail(String.format("Unknown sort order '%s'. Typo?\n", sortOrder));
        }
        return verifySort(this.allVisibleBids(), column, sortOrder);
    }

    //#Fix for release OCT 7
    /***
     * <h1>isSortedByInNewForYouTab</h1>
     * <p>purpose: Returns true if the bid table is sorted by the given column and in the given sort order</p>
     * @param column - name of the column that the table should be sorted by
     * @param sortOrder - either "ascending" or "descending"  in New for you Bid Tab
     * @return boolean
     */
    public boolean isSortedByInNewForYouTab(String column, String sortOrder) {
        if (!sortOrder.equals("ascending") && !sortOrder.equals("descending")) {
            Assert.fail(String.format("Unknown sort order '%s'. Typo?\n", sortOrder));
        }

        return verifySort(this.allVisibleBidsInNewForYouTab(), column, sortOrder);
    }


    /***
     * <h1>verifySort</h1>
     * <p>purpose: Verify the sorting of a list of bids</p>
     * @param bids      = list of bids to verify sort
     * @param column    = column to sort by
     * @param sortOrder = "ascending" | "descending"
     * @return true if bids are sorted per sortOrder of column | false if bids are not sorted per sortOrder of column
     */
    private boolean verifySort(List<BidListRow> bids, String column, String sortOrder) {
        return Comparators.isInOrder(bids, buildComparator(column, sortOrder.equals("ascending")));
    }
    
    /***
     * <h1>isSortedBy</h1>
     * <p>
     *     purpose: verifies if the bid table is sorted primarily by the first column, secondarily by the
     *     second column, and in the given sort order.
     * </p>
     * @param column1 - first column that the table should be sorted by
     * @param sortOrder1 - either "ascending" or "descending"
     * @param column2 - second column that the table should be sorted by
     * @param sortOrder2 - either "ascending" or "descending"
     * @return String - 'primary failed', 'secondary failed', or 'sorted correctly'
     */
    public String isSortedBy(String column1, String sortOrder1, String column2, String sortOrder2) {
        List<BidListRow> allBids = this.allVisibleBids();
        if(!verifySort(allBids, column1, sortOrder1)) {
            return "primary failed";
        }
        List<List<BidListRow>> partitions = partitionBidsBy(allBids, column1);
        for(List<BidListRow> partition : partitions) {
            if(!verifySort(partition, column2, sortOrder2)) {
                return "secondary failed";
            }
        }
        return "sorted correctly";
    }
    
    
    
    /***
     * <h1>buildComparator</h1>
     * <p>purpose: Comparator to determine column sorting of the bid</p>
     * @param column = column in the bid list to determine sorting
     * @param isSortAscending = true if testing for ascending | false if testing for descending
     * @return Comparator
     */
    private Comparator<BidListRow> buildComparator(String column, boolean isSortAscending) {
        return (bid1, bid2) -> {
            BidListRow left  = isSortAscending ? bid1 : bid2;
            BidListRow right = isSortAscending ? bid2 : bid1;
            switch(column) {
                case "Bid Title":
                	return left.bidInfo.getBidTitle().compareTo(right.bidInfo.getBidTitle());
                case "Bid ID":
                    return left.bidInfo.getBidNumber().compareToIgnoreCase(right.bidInfo.getBidNumber());
                case "Agency":
                    return left.bidInfo.getAgency().compareTo(right.bidInfo.getAgency());
                case "State":
                    return left.bidInfo.getState().compareToIgnoreCase(right.bidInfo.getState());
                case "End Date":
                    return left.bidInfo.getEndDateAsCalendar().compareTo(right.bidInfo.getEndDateAsCalendar());
                case "Added":
                    return left.bidInfo.getAddedDateInHours().compareTo(right.bidInfo.getAddedDateInHours());
                case "Match":
                    return left.getRelevancyPercentage().compareTo(right.getRelevancyPercentage());
                default:
                    Assert.fail(String.format("Unknown column name '%s'.\n", column));
                    return 0;
            }
        };
    }
    
    /***
     * <h1>verifyMinimumNumberOfBidResults</h1>
     * <p>purpose: Verify that a minimum of number<br>
     * 	of bid list results is appearing in the visible bid list<br>
     *  Fail test if the actual number of results currently displaying in the bid list<br>
     *  is less than the expected number of bid results.</p>
     * @param minExpectedNumberOfBidListResults = minimum number of bids you're expecting to see<br>
     * 	in the bid list</p>
     * @return BidList
     */
    public BidList verifyMinimumNumberOfBidResults(int minExpectedNumberOfBidListResults) {
    	int numberOfBids = this.getNumberOfCurrentlyDisplayedBids();
    	if ( numberOfBids < minExpectedNumberOfBidListResults) {
    		fail ("Test Failed: Expected minimum of \"" + minExpectedNumberOfBidListResults + "\" bids to display, but got \"" + numberOfBids+ "\" bids instead"); }
    	
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified that there are at least \"" + minExpectedNumberOfBidListResults + "\" bids in the bid list");
    	return this;
    }
    
    /***
     * <h1>verifyMaximumNumberOfBidResults</h1>
     * <p>purpose: Verify that a maximum of number<br>
     * 	of bid list results is appearing in the visible bid list<br>
     *  Fail test if the actual number of results currently displaying in the bid list<br>
     *  is less than the expected number of bid results.</p>
     * @param maxExpectedNumberOfBidListResults = maximum number of bids you're expecting to see<br>
     * 	in the bid list</p>
     * @return BidList
     */
    public BidList verifyMaximumExpectedNumberOfBidResults(int maxExpectedNumberOfBidListResults) {
    	int numberOfBids = this.getNumberOfCurrentlyDisplayedBids();
    	if ( numberOfBids > maxExpectedNumberOfBidListResults) {
    		fail ("Test Failed: Expected maximum of \"" + maxExpectedNumberOfBidListResults + "\" bids to display, but got \"" + numberOfBids+ "\" bids instead"); }
    	
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified that there are at most \"" + maxExpectedNumberOfBidListResults + "\" bids in the bid list");
    	return this;

    }
    
    /***
     * <h1>verifyLoadMoreBidsDoesNotDisplay</h1>
     * <p>purpose: Verify that "Load More Bids" button is not<br.
     * 	currently displayed onscreen. Fail test if displayed</p>
     * @return BidList
     */
    public BidList verifyLoadMoreBidsDoesNotDisplay() {
    	if (this.isLoadMoreBidsDisplaying()) {
    		fail("Test Failed: \"Load More Bids\" button is currently displayed"); }
    	
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"Load More Bids\" button is not displaying");
    	return this;
    }

	/***
	 * <h1>verifyNoSearchResultsReturn</h1>
	 * <p>purpose: Verify that the this text is displaying on All Bids tab:<br>
	 * 	"SORRY, YOUR SEARCH DID NOT MATCH ANY BIDS.<br>
	 * 	Try something like:<br>
	 * 	Using more general terms<br>
	 * 	Checking your spelling</p>
	 * @return BidList
	 */
	public BidList verifyNoSearchResultsReturn () {
		waitForInvisibility(By.xpath(this.sorryNoSearchResultsTxt_xpath));
		return this;
	}
	
	/***
	 * <h1>verifyColumnHeadersLoaded</h1>
	 * <p>purpose: Verify each column header is visible. These include:<br>
	 * 	1. Match<br>
	 * 	2. Bid Title<br>
	 * 	3. Bid ID<br>
	 * 	4. Agency<br>
	 * 	5. State<br>
	 * 	6. End Date<br>
	 * 	7. Added</p>
	 * @return BidList
	 */
	public BidList verifyColumnHeadersLoaded() {
		
		// Verify each column header is visible
		this.getMatchHeader();
		this.getBidTitleHeader();
		this.getBidIdHeader();
		this.getAgencyHeader();
		this.getStateHeader();
		this.getEndDateHeader();
		this.getAddedHeader();
		
		return this;
	}
	
	/***
	 * <h1>verifyYourSavedBidsColumnHeadersLoaded</h1>
	 * <p>purpose: Verify each column header is visible. These include:<br>
	 * 	1. Bid Title<br>
	 * 	2. Bid ID<br>
	 * 	3. Agency<br>
	 * 	5. State<br>
	 * 	6. End Date<br>
	 * 	7. Added</p>
	 * @return BidList
	 */
	public BidList verifyYourSavedBidsColumnHeadersLoaded() {
		
		// Verify each column header is visible
		this.getBidTitleHeader();
		this.getBidIdHeader();
		this.getAgencyHeader();
		this.getStateHeader();
		this.getEndDateHeader();
		this.getAddedHeader();
		
		return this;
	}
	
	/***
	 * <h1>verifyBidExpired</h1>
	 * <p>purpose: Verify whether the bid is expired after sorting it in ascending order<br>
	 * @return BidList
	 */
	
	public BidList verifyBidExpired() {
		String currentMonth = new SimpleDateFormat("MM").format(Calendar.getInstance().getTime());
		String currentDate = new SimpleDateFormat("dd").format(Calendar.getInstance().getTime());

		int month = Integer.parseInt(currentMonth);
		int date = Integer.parseInt(currentDate);

		String getDatefromUI = getExpiredDate().getText();
		System.out.println(GlobalVariables.getTestID() + " Info: Verified Bid Expired date = " + getDatefromUI);
		String[] arrOfStr = getDatefromUI.split("/", 3);

		String objMonthInUI = (String) Array.get(arrOfStr, 0);
		String objDateInUI = (String) Array.get(arrOfStr, 1);

		int dateInUI = Integer.parseInt(objDateInUI);
		int monthInUI = Integer.parseInt(objMonthInUI);

		Assert.assertTrue("Test Failed: All Bids tab  expired bids when sorting by End Date in ascending order",
				monthInUI >= month);
		Assert.assertTrue("Test Failed: All Bids tab shows expired bids when sorting by End Date in ascending order",
				dateInUI >= date);

		return this;

	}
   
}
