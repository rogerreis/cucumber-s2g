package pages.bidSyncPro.supplier.account.bidProfile;

import static org.junit.Assert.fail;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.common.Chip;
import pages.bidSyncPro.supplier.common.ChipList;
import pages.common.helpers.GlobalVariables;

/***
 * <h1>Class KeywordsDialog</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects related to the objects on the "Keywords" popup that is located off of the admin->Bid Notifications screen</p>
 */
public class KeywordsDialog extends BidNotifications{
	
	// xpaths
	
	// top of popup
	private final String strCard_xpath                  = "//mat-card[@id='matUserKeywordsComponentCard']";
	private final String closeDialogBtn_xpath           = strCard_xpath + "//div[@id='divCloseUpdateUserProfile']//button";
	
	// positive keywords
	private final String enterKeywordsInput_xpath       = strCard_xpath + "//input[@id='keyword']";
	private final String positiveKeywordsChipList_xpath = strCard_xpath + "//mat-chip[@color='accent']/../.";
	
	// negative keywords
	private final String addNegativeKeywords_xpath      = strCard_xpath + "//input[@id='negKeyword']";
	private final String negativeKeywordsChipList_xpath = strCard_xpath + "//mat-chip[@color='danger']/../.";

	// Elements
	private ChipList positiveKeywordsChipList;
	private ChipList negativeKeywordsChipList;

	// Other
	private EventFiringWebDriver driver;

	public KeywordsDialog (EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
		this.setNegativeKeywordsChipList();
		this.setPositiveKeywordsChipList();
	}
	
	// Getters
	public WebElement getCloseDialogBtn()           { return findByVisibility(By.xpath(this.closeDialogBtn_xpath));      } 
	public WebElement getEnterKeywordsInput()       { return findByVisibility(By.xpath(this.enterKeywordsInput_xpath));  } 
	public WebElement getAddNegativeKeywordsInput() { return findByVisibility(By.xpath(this.addNegativeKeywords_xpath)); } 

	private ChipList getPositiveKeywordsChipList() { return this.positiveKeywordsChipList; }
	private ChipList getNegativeKeywordsChipList() { return this.negativeKeywordsChipList; }
	
	// Setters
	private void setPositiveKeywordsChipList() { this.positiveKeywordsChipList = new ChipList (driver, this.positiveKeywordsChipList_xpath);}
	private void setNegativeKeywordsChipList() { this.negativeKeywordsChipList = new ChipList (driver, this.negativeKeywordsChipList_xpath);}
	
	/* ----- methods ----- */
	
	/***
	 * <h1>verifyPositiveKeywordSavedInChipList</h1>
	 * <p>purpose: Verify that a keyword is displaying the positive keywords chip list.<br>
	 * 	Fail test if not displaying</p>
	 * @param strKeywordToVerify = keyword to look for in the chip list
	 * @return KeywordsDialog 
	 */
	public KeywordsDialog verifyPositiveKeywordSavedInChipList (String strKeywordToVerify) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Determining if keyword \"%s\" correctly saved out to the Positive Keyword chip list\n", strKeywordToVerify);
		this.getPositiveKeywordsChipList().verifyChipInChipList(strKeywordToVerify);
		return this;
	}

	/***
	 * <h1>verifyPositiveKeywordSavedInChipList</h1>
	 * <p>purpose: Verify that a list of keywords are displaying in the positive keywords chip list.<br>
	 * 	Fail test if not displaying</p>
	 * @param keywordsToVerify = list of keywords to check
	 * @return KeywordsDialog 
	 */
	public KeywordsDialog verifyPositiveKeywordsSavedInChipList (List <String> keywordsToVerify) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Determining if keywords correctly saved out to the Positive Keyword chip list\n");
		keywordsToVerify.forEach( keyword-> {
			this.getPositiveKeywordsChipList().verifyChipInChipList(keyword);
			});
		return this;
	}
	/***
	 * <h1>verifyNegativeKeywordSavedInChipList</h1>
	 * <p>purpose: Verify that a keyword is displaying the negative keywords chip list.<br>
	 * 	Fail test if not displaying</p>
	 * @param strKeywordToVerify = keyword to look for in the chip list
	 * @return KeywordsDialog 
	 */
	public KeywordsDialog verifyNegativeKeywordSavedInChipList (String strKeywordToVerify) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Determining if keyword \"%s\" correctly saved out to the Negative Keywords chip list\n", strKeywordToVerify);
		this.getNegativeKeywordsChipList().verifyChipInChipList(strKeywordToVerify);
		return this;
	}

	/***
	 * <h1>verifyNegativeKeywordSavedInChipList</h1>
	 * <p>purpose: Verify that a list of keywords are displaying in the negative keywords chip list.<br>
	 * 	Fail test if not displaying</p>
	 * @param keywordsToVerify = list of keywords to check
	 * @return KeywordsDialog 
	 */
	public KeywordsDialog verifyNegativeKeywordsSavedInChipList (List <String> keywordsToVerify) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Determining if keywords correctly saved out to the negative keyword chip list\n");
		keywordsToVerify.forEach( keyword-> {
			this.getNegativeKeywordsChipList().verifyChipInChipList(keyword);
			});
		return this;
	}
	
	
	/***
	 * <h1>deleteAllNegativeKeywords</h1> 
	 * <p>purpose: Delete all the keywords in the negative keyword chip list</p>
	 * @return KeywordsDialog
	 */
	public KeywordsDialog deleteAllNegativeKeywords () {
		System.out.println(GlobalVariables.getTestID() + " INFO: Deleting all the chips in the Negative Keywords chip list");
		this.getNegativeKeywordsChipList().deleteAllChipsInChipList();
		
		return this;
	}
	
	/***
	 * <h1>deleteAllPositiveKeywords</h1> 
	 * <p>purpose: Delete all the keywords in the positive keyword chip list</p>
	 * @return KeywordsDialog
	 */
	public KeywordsDialog deleteAllPositiveKeywords () {
		System.out.println(GlobalVariables.getTestID() + " INFO: Deleting all the chips in the Positive Keywords chip list");
		
		this.getPositiveKeywordsChipList().deleteAllChipsInChipList(); 

		return this;
	}
	
	/***
	 * <h1>verifyPositiveKeywordsChipListIsEmpty</h1>
	 * <p>purpose: Verify that there are no keywords in the positive keywords chip list.<br>
	 * 	(i.e. We only have the three example keywords: "Keyword 1", "Keyword 2", "Keyword 3")<br>
	 * 	Fail the test if there are keywords in the chip list</p>
	 * @return KeywordsDialog
	 */
	public KeywordsDialog verifyPositiveKeywordsChipListIsEmpty() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Verifying that positive keywords chip list is empty");
		
		// We should have three chips, corresponding to three example keywords
		// in order for the positive keywords to be "empty"
		if (this.getPositiveKeywordsChipList().isChipInChipList("Keyword 1") |
			this.getPositiveKeywordsChipList().isChipInChipList("Keyword 2") |
			this.getPositiveKeywordsChipList().isChipInChipList("Keyword 3") |
			this.getPositiveKeywordsChipList().getAllChipsInChipList().size() > 0) { 
				fail("Test Failed: Positive Keywords chip list is not empty"); }
		return this;
	}
	
	/***
	 * <h1>verifyNegativeKeywordsChipListIsEmpty</h1>
	 * <p>purpose: Verify that there are no keywords in the negative keywords chip list.<br>
	 * 	Fail the test if there are keywords in the chip list</p>
	 * @return KeywordsDialog
	 */
	public KeywordsDialog verifyNegativeKeywordsChipListIsEmpty() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Verifying that positive keywords chip list is empty");
		if (!this.getNegativeKeywordsChipList().isChipListEmpty()) { fail("Test Failed: Negative Keywords chip list is not empty"); }
		return this;
	}
	
	/***
	 * <h1>clickCloseKeywordsDialog</h1>
	 * <p>purpose: Assuming that the Keywords Dialog is open, click on the "X" in the top corner to close the popup</p>
	 * @return BidNotifications
	 */
	public BidNotifications clickCloseKeywordsDialog () {
		System.out.println(GlobalVariables.getTestID() + " INFO: Clicking the close button on the Keywords Dialog");
		this.getCloseDialogBtn().click();
		return new BidNotifications(driver);
	}

	/***
	 * <h1>setPositiveKeyword</h1>
	 * <p>purpose: Input positive keyword, strKeyword, into the "Add Keywords" input and then press RETURN</p>
	 * @param strKeyword = Positive keyword to enter
	 * @return KeywordsDialog
	 */
	public KeywordsDialog setPositiveKeyword (String strKeyword) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Inputting positive keyword \"%s\" into the keyword input field\n", strKeyword);
		getEnterKeywordsInput().sendKeys(strKeyword);
		getEnterKeywordsInput().sendKeys(Keys.RETURN);
		return this;
	}

	/***
	 * <h1>setNegativeKeyword</h1>
	 * <p>purpose: Input negative keyword, strKeyword, into the "Add Negative Keywords (Optional)" input and then press RETURN</p>
	 * @param strKeyword = Negative keyword to enter
	 * @return KeywordsDialog
	 */
	public KeywordsDialog setNegativeKeyword (String strKeyword) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Inputting negative keyword \"%s\" into the keyword input field\n", strKeyword);
		getAddNegativeKeywordsInput().sendKeys(strKeyword);
		getAddNegativeKeywordsInput().sendKeys(Keys.RETURN);
		return this;
	}
	
	/**
	 * <h1>getAllPositiveKeywordsChips</h1>
	 * <p>purpose: Return a List of all the chips in the positive keywords chip list</p>
	 * @return List <Chip>
	 */
	public List <Chip> getAllPositiveKeywordsChips(){
		System.out.println(GlobalVariables.getTestID() + " INFO: Returning all chips in Positive Keywords chip list");
		return positiveKeywordsChipList.getAllChipsInChipList();
	}

	/**
	 * <h1>getAllNegativeKeywordsChips</h1>
	 * <p>purpose: Return a List of all the chips in the negative keywords chip list</p>
	 * @return List <Chip>
	 */
	public List <Chip> getAllNegativeKeywordsChips(){
		System.out.println(GlobalVariables.getTestID() + " INFO: Returning all chips in Negative Keywords chip list");
		return negativeKeywordsChipList.getAllChipsInChipList();
	}

	/***
	 * <h1>verifyPositiveKeywordsNotEmpty</h1>
	 * <p>purpose: Verify that Positive Keywords displays at least one chip.<br>
	 * 	Fail test if chiplist is empty</p>
	 * @return KeywordsDialog
	 */
	public KeywordsDialog verifyPositiveKeywordsNotEmpty() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Verifying chips exist in Default Positive Keywords chip list");
		positiveKeywordsChipList.verifyChipListIsNotEmpty();
		return this;
	}
	
	/***
	 * <h1>verifyNegativeKeywordsNotEmpty</h1>
	 * <p>purpose: Verify that Negative Keywords displays at least one chip.<br>
	 * 	Fail test if chiplist is empty</p>
	 * @return KeywordsDialog
	 */
	public KeywordsDialog verifyNegativeKeywordsNotEmpty() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Verifying chips exist in Default Negative Keywords chip list");
		negativeKeywordsChipList.verifyChipListIsNotEmpty();
		return this;
	}
}
