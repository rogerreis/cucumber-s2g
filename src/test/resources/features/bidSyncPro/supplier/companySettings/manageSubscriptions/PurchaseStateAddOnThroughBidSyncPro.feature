@ignore
Feature: Company Settings > Manage Subscriptions > Purchase State AddOn through BidSync Pro
   BIDSYNC-510: Add On Pro Plan - Purchase Additional States
   
 dmidura: Below are not required for BIDSYNC-510, but I want to add them to be thorough

@should
Scenario Outline: Selecting "Additional State" addOn from Chargebee punchout will allow user access selected states.
	Canceling user subscription reverts user to BASIC plan, removes all addOns, and removes access to selected additional states.

	Given I register a new company owner with a "<subscriptionPlan>" subscription plan, with add-ons and some licenses
	And I logout of BidSync Pro
# TODO When I randomly purchase Additional States addOns from the Chargebee punchout in BidSync Pro
	Then I will verify that the Chargebee test site correctly displays the company owner subscription and addOns
	And I will verify that the ARGO database correctly displays the company owner subscription and addOns
	And I will verify that in BidSync Pro the company owner can view bids in their subscription and addOns
	When the Chargebee admin cancels the subscription for the company owner
	Then I will verify that the Chargebee test site correctly displays the company owner subscription and addOns
	And I will verify that the ARGO database correctly displays the company owner subscription and addOns
	And I will verify that in BidSync Pro the company owner can view bids in their subscription and addOns

	# Note: National plan doesn't need additional States. Basic plan needs to upgrade to paid plan first.	
	Examples: Subscriptions that will purchase Additional State addOn
	| subscriptionPlan           |
	| BidSync Regional - Yearly  |
	| BidSync Regional - Monthly |
	| BidSync Regional - 6 Month |
	| BidSync State - Yearly     |
	| BidSync State - Monthly    |
	| BidSync State - 6 Month    |
	
# Additional: Add test for conferring/revoking license with Additional State addOn
# Additional: Add test for company manager canceling subscription and verify Additional State addOn is removed