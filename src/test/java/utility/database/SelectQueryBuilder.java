package utility.database;

import org.junit.Assert;

import pages.common.helpers.GlobalVariables;

import static org.junit.Assert.fail;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <h1>Class SelectQueryBuilder</h1>
 * <p>details: builds and runs a SQL query on the [dbName] database of the form <br>
 *  "select [resultColumn] from [table] where [filterColumn] = '[filterTerm]'"<br>
 */
public class SelectQueryBuilder {
    
    private boolean usingPro = true;
    private String dbName;
    private String table;
    private String resultColumn;
    private String filterColumn;
    private String filterTerm;
    
    public SelectQueryBuilder() {}
    
    public SelectQueryBuilder setDatabase(String dbName) {
        this.dbName = dbName;
        return this;
    }
    
    public SelectQueryBuilder useProDatabase() {
        this.usingPro = true;
        return this;
    }
    
    public SelectQueryBuilder useClassicDatabase() {
        this.usingPro = false;
        return this;
    }
    
    public SelectQueryBuilder setTable(String table) {
        this.table = table;
        return this;
    }
    
    public SelectQueryBuilder setResultColumn(String resultColumn) {
        this.resultColumn = resultColumn;
        return this;
    }
    
    public SelectQueryBuilder setFilterColumn(String filterColumn) {
        this.filterColumn = filterColumn;
        return this;
    }
    
    public SelectQueryBuilder setFilterTerm(String filterTerm) {
        this.filterTerm = filterTerm;
        return this;
    }
    
    /**
     * <h1>run</h1>
     * <p>purpose: Run the following query on the database: <br>
     * select [resultColumn] from [table] where [filterColumn] = "[filterTerm]"
     * @return results from query
     */
    @SuppressWarnings("unchecked") // impossible to avoid with conversion to a generic type, as far as I know
    public <T> List<T> run() {
        Objects.requireNonNull(dbName);
        Objects.requireNonNull(table);
        Objects.requireNonNull(resultColumn);
        Objects.requireNonNull(filterColumn);
        Objects.requireNonNull(filterTerm);
        String query = String.format("select %s from %s where %s = '%s'", resultColumn, table, filterColumn, filterTerm);
        System.out.println(String.format("Running query: %s", query));
        List<T> results = new ArrayList<>();
        try {
            ResultSet rs;
            if(usingPro) {
                rs = SQLConnector.executeQuery(dbName, query);
            } else {
                return SQLConnector.executeQueryOnBidsyncClassic(dbName, query, resultColumn);
            }
            while (rs.next()) {
                results.add((T)rs.getObject(resultColumn));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            Assert.fail(String.format("Errors when running query '%s'", query));
        }
        return results;
    }
    
    /**
     * <h1>run</h1>
     * <p>purpose: Run the following query on the database: <br>
     * select [resultColumn] from [table] where [filterColumn] = "[filterTerm]"
     * @return resultset from query
     */
    public <T> ResultSet returnResultSet() {
        Objects.requireNonNull(dbName);
        Objects.requireNonNull(table);
        Objects.requireNonNull(resultColumn);
        Objects.requireNonNull(filterColumn);
        Objects.requireNonNull(filterTerm);
        String query = String.format("select %s from %s where %s = '%s'", resultColumn, table, filterColumn, filterTerm);
        System.out.println(String.format("Running query: %s", query));
        ResultSet rs = null;
        try {
            rs = SQLConnector.executeQuery(dbName, query);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            Assert.fail(String.format("Errors when running query '%s'", query));
        }
        return rs;
    }
    
    /***
	 * <h1>runSearchReturnField</h1>
	 * <p>purpose: helper to run a search on BidSync Pro database. Enter query, database name, and column you<br>
	 * 	want the result for. Returns a List <String> of all database results that match<br>
	 * 	query in database for column. Fail if no records are located</p>
	 * @param strQuery = SQL query
	 * @param strDBName = database name
	 * @param returnField = column you're interested in
	 * @return List <String> containing all records for that search
	 */
	public List<String> runSearchReturnField (String strQuery, String strDBName, String returnField) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Running query \"%s\" on database \"%s\" \n", strQuery, strDBName);

		// Run search and grab results
		ResultSet rs = null;
		try {
			rs = SQLConnector.executeQuery(strDBName, strQuery);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		List <String> records = new ArrayList<>();
		
		try {
			while (rs.next()) {
				records.add(rs.getString(returnField)); }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if (records.isEmpty()) { fail("ERROR: Did not locate any records for search!");}
		
		return records;
	}
	
	/***
	 * <h1>runSearchReturnFields</h1>
	 * <p>purpose: helper to run a search on BidSync Pro database. Enter query, database name, and column names you<br>
	 * 	want the result for. Returns a Map <String, String> of all database results that match<br>
	 * 	query in database for column. Fail if no records are located</p>
	 * @param strQuery = SQL query
	 * @param strDBName = database name
	 * @param returnFields = String [] of column names (as written in database)
	 * @return Map <String, String> containing all records for that search
	 */
	public Map<String, String> runSearchReturnFields(String strQuery, String strDBName, String [] returnFields){
		System.out.printf(GlobalVariables.getTestID() + " INFO: Running query \"%s\" on database \"%s\" \n", strQuery, strDBName);
		
		// Run search and grab results
		ResultSet rs = null;
			try {
				rs = SQLConnector.executeQuery(strDBName, strQuery);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}

		Map <String, String> records = new HashMap <String, String>();

		try {
			while (rs.next()) {
				for (String returnField : returnFields) {
					records.put(returnField, rs.getString(returnField)); }}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Assert.assertTrue("ERROR: No records returned for search!", !records.isEmpty());

		return records;
	}
    
    
}
