package data.bidSyncPro.registration;

/***
 * <h1>enum SubscriptionPlan</h1>
 * <p>details: This enum defines what a user can select<br>
 * for the subscription plan (Pro or Basic) for Chargebee registration.</p>
 */
public enum SubscriptionPlan {
    BASIC,
    STATE_MONTHLY,
    STATE_SIX_MONTH,
    STATE_ANNUAL,
    REGIONAL_MONTHLY,
    REGIONAL_SIX_MONTH,
    REGIONAL_ANNUAL,
    NATIONAL_MONTHLY,
    NATIONAL_SIX_MONTH,
    NATIONAL_ANNUAL;
    
	/***
	 * <h1>fromString</h1>
	 * <p>purpose: Given a shorthand form of the subscription plan, return the enum value<br>
	 * 	Ex: input = "basic" | "BidSync Basic", return BASIC</p>
	 * @param subscriptionPlan = shorthand form of the subscription plan, as given by Chargebee
	 * @return SubscriptionPlan (enum value)
	 */
    public static SubscriptionPlan fromString(String subscriptionPlan) {
        
    	SubscriptionPlan subPlan = null;
    	
    	switch (System.getProperty("server")) {

        // QA         
		case "qa":
			if(subscriptionPlan.equalsIgnoreCase("basic") | subscriptionPlan.equals("BidSync Basic")) {
				subPlan = BASIC;
	        } else if (subscriptionPlan.equalsIgnoreCase("state monthly") | subscriptionPlan.equals("BidSync State - Monthly") | subscriptionPlan.equals("BidSync State - Monthly Online")) {
	        	subPlan = STATE_MONTHLY;
	        } else if (subscriptionPlan.equalsIgnoreCase("state six month") | subscriptionPlan.equals("BidSync State - 6 Month") | subscriptionPlan.equals("BidSync State - 6 Month Online")) {
	        	subPlan = STATE_SIX_MONTH;
	        } else if (subscriptionPlan.equalsIgnoreCase("state annual") | subscriptionPlan.equals("BidSync State - Yearly") | subscriptionPlan.equals("BidSync State - Yearly Online")) {
	        	subPlan = STATE_ANNUAL;
	        } else if (subscriptionPlan.equalsIgnoreCase("regional monthly") | subscriptionPlan.equals("BidSync Regional - Monthly") | subscriptionPlan.equals("BidSync Regional - Monthly Online")) {
	        	subPlan = REGIONAL_MONTHLY;
	        } else if (subscriptionPlan.equalsIgnoreCase("regional six month") | subscriptionPlan.equals("BidSync Regional - 6 Month") | subscriptionPlan.equals("BidSync Regional - 6 Month Online")) {
	        	subPlan = REGIONAL_SIX_MONTH;
	        } else if (subscriptionPlan.equalsIgnoreCase("regional annual") | subscriptionPlan.equals("BidSync Regional - Yearly") | subscriptionPlan.equals("BidSync Regional - Yearly Online")) {
	        	subPlan = REGIONAL_ANNUAL;
	        } else if (subscriptionPlan.equalsIgnoreCase("national monthly") | subscriptionPlan.equals("BidSync National - Monthly") | subscriptionPlan.equals("BidSync National - Monthly Online")) {
	        	subPlan = NATIONAL_MONTHLY;
	        } else if (subscriptionPlan.equalsIgnoreCase("national six month") | subscriptionPlan.equals("BidSync National - 6 Month")| subscriptionPlan.equals("BidSync National - 6 Month Online")) {
	        	subPlan = NATIONAL_SIX_MONTH;
	        } else if (subscriptionPlan.equalsIgnoreCase("national annual")| subscriptionPlan.equals("BidSync National - Yearly") | subscriptionPlan.equals("BidSync National - Yearly Online")) {
	        	subPlan = NATIONAL_ANNUAL;
	        } else {
	            System.out.println(String.format("ERROR: Unrecognized subscription plan '%s'", subscriptionPlan));
	            subPlan = null; }
			break;
			
		//  Dev
		case "dev":
			if(subscriptionPlan.equalsIgnoreCase("basic") | subscriptionPlan.equals("BidSync Basic")) {
				subPlan = BASIC;
	        } else if (subscriptionPlan.equalsIgnoreCase("state monthly") | subscriptionPlan.equals("BidSync State - Monthly") | subscriptionPlan.equals("BidSync State - Monthly Online")) {
	        	subPlan = STATE_MONTHLY;
	        } else if (subscriptionPlan.equalsIgnoreCase("state six month") | subscriptionPlan.equals("BidSync State - 6 Month") | subscriptionPlan.equals("BidSync State - 6 Month Online")) {
	        	subPlan = STATE_SIX_MONTH;
	        } else if (subscriptionPlan.equalsIgnoreCase("state annual") | subscriptionPlan.equals("BidSync State - Yearly") | subscriptionPlan.equals("BidSync State - Yearly Online")) {
	        	subPlan = STATE_ANNUAL;
	        } else if (subscriptionPlan.equalsIgnoreCase("regional monthly") | subscriptionPlan.equals("BidSync Regional - Monthly") | subscriptionPlan.equals("BidSync Regional - Monthly Online")) {
	        	subPlan = REGIONAL_MONTHLY;
	        } else if (subscriptionPlan.equalsIgnoreCase("regional six month") | subscriptionPlan.equals("BidSync Regional - 6 Month") | subscriptionPlan.equals("BidSync Regional - 6 Month Online")) {
	        	subPlan = REGIONAL_SIX_MONTH;
	        } else if (subscriptionPlan.equalsIgnoreCase("regional annual") | subscriptionPlan.equals("BidSync Regional - Yearly") | subscriptionPlan.equals("BidSync Regional - Yearly Online")) {
	        	subPlan = REGIONAL_ANNUAL;
	        } else if (subscriptionPlan.equalsIgnoreCase("national monthly") | subscriptionPlan.equals("BidSync National - Monthly") | subscriptionPlan.equals("BidSync National - Monthly Online")) {
	        	subPlan = NATIONAL_MONTHLY;
	        } else if (subscriptionPlan.equalsIgnoreCase("national six month") | subscriptionPlan.equals("BidSync National - 6 Month")| subscriptionPlan.equals("BidSync National - 6 Month Online")) {
	        	subPlan = NATIONAL_SIX_MONTH;
	        } else if (subscriptionPlan.equalsIgnoreCase("national annual")| subscriptionPlan.equals("BidSync National - Yearly") | subscriptionPlan.equals("BidSync National - Yearly Online")) {
	        	subPlan = NATIONAL_ANNUAL;
	        } else {
	            System.out.println(String.format("ERROR: Unrecognized subscription plan '%s'", subscriptionPlan));
	            subPlan = null; }
			break;
        	
		case "stage":
			if(subscriptionPlan.equalsIgnoreCase("basic") | subscriptionPlan.equals("BidSync Basic")) {
				subPlan = BASIC;
	        } else if (subscriptionPlan.equalsIgnoreCase("state monthly") | subscriptionPlan.equals("BidSync State - Monthly") | subscriptionPlan.equals("BidSync State - Monthly Online")) {
	        	subPlan = STATE_MONTHLY;
	        } else if (subscriptionPlan.equalsIgnoreCase("state six month") | subscriptionPlan.equals("BidSync State - 6 Month") | subscriptionPlan.equals("BidSync State - 6 Month Online")) {
	        	subPlan = STATE_SIX_MONTH;
	        } else if (subscriptionPlan.equalsIgnoreCase("state annual") | subscriptionPlan.equals("BidSync State - Yearly") | subscriptionPlan.equals("BidSync State - Yearly Online")) {
	        	subPlan = STATE_ANNUAL;
	        } else if (subscriptionPlan.equalsIgnoreCase("regional monthly") | subscriptionPlan.equals("BidSync Regional - Monthly") | subscriptionPlan.equals("BidSync Regional - Monthly Online")) {
	        	subPlan = REGIONAL_MONTHLY;
	        } else if (subscriptionPlan.equalsIgnoreCase("regional six month") | subscriptionPlan.equals("BidSync Regional - 6 Month") | subscriptionPlan.equals("BidSync Regional - 6 Month Online")) {
	        	subPlan = REGIONAL_SIX_MONTH;
	        } else if (subscriptionPlan.equalsIgnoreCase("regional annual") | subscriptionPlan.equals("BidSync Regional - Yearly") | subscriptionPlan.equals("BidSync Regional - Yearly Online")) {
	        	subPlan = REGIONAL_ANNUAL;
	        } else if (subscriptionPlan.equalsIgnoreCase("national monthly") | subscriptionPlan.equals("BidSync National - Monthly") | subscriptionPlan.equals("BidSync National - Monthly Online")) {
	        	subPlan = NATIONAL_MONTHLY;
	        } else if (subscriptionPlan.equalsIgnoreCase("national six month") | subscriptionPlan.equals("BidSync National - 6 Month")| subscriptionPlan.equals("BidSync National - 6 Month Online")) {
	        	subPlan = NATIONAL_SIX_MONTH;
	        } else if (subscriptionPlan.equalsIgnoreCase("national annual")| subscriptionPlan.equals("BidSync National - Yearly") | subscriptionPlan.equals("BidSync National - Yearly Online")) {
	        	subPlan = NATIONAL_ANNUAL;
	        } else {
	            System.out.println(String.format("ERROR: Unrecognized subscription plan '%s'", subscriptionPlan));
	            subPlan = null; }
			break;
			}
    	return subPlan;
    	}
   
    /***
     * <h1>isMonthlyPlan</h1>
     * <p>purpose: Is the subscription Plan an Monthly Plan?</p>
     * @return true if subscription = STATE_MONTHLY | REGIONAL_MONTHLY | NATIONAL_MONTHLY, false otherwise
     */
    public boolean isMonthlyPlan() {
        switch(this) {
            case STATE_MONTHLY:
            case REGIONAL_MONTHLY:
            case NATIONAL_MONTHLY:
                return true;
            default:
                return false;
        }
    }

    /***
     * <h1>isSixMonth</h1>
     * <p>purpose: Is the subscription Plan a Six Month Plan?</p>
     * @return true if subscription = STATE_SIX_MONTH | REGIONAL_SIX_MONTH | NATIONAL_SIX_MONTH, false otherwise
     */
    public boolean isSixMonthPlan() {
        switch(this) {
            case STATE_SIX_MONTH:
            case REGIONAL_SIX_MONTH:
            case NATIONAL_SIX_MONTH:
                return true;
            default:
                return false;
        }
    }

    /***
     * <h1>isAnnualPlan</h1>
     * <p>purpose: Is the subscription Plan an Annual Plan?</p>
     * @return true if subscription = STATE_ANNUAL | REGIONAL_ANNUAL | NATIONAL_ANNUAL, false otherwise
     */
    public boolean isAnnualPlan() {
        switch(this) {
            case STATE_ANNUAL:
            case REGIONAL_ANNUAL:
            case NATIONAL_ANNUAL:
                return true;
            default:
                return false;
        }
    }

    /***
     * <h1>isStatePlan</h1>
     * <p>purpose: Is the subscription Plan a State Plan?</p>
     * @return true if subscription = STATE_MONTHLY | STATE_SIX_MONTH | STATE_ANNUAL, false otherwise
     */
    public boolean isStatePlan() {
        switch(this) {
            case STATE_MONTHLY:
            case STATE_SIX_MONTH:
            case STATE_ANNUAL:
                return true;
            default:
                return false;
        }
    }

    /***
     * <h1>isRegionalPlan</h1>
     * <p>purpose: Is the subscription Plan a Regional Plan?</p>
     * @return true if subscription = REGIONAL_MONTHLY | REGIONAL_SIX_MONTH | REGIONAL_ANNUAL, false otherwise
     */
    public boolean isRegionalPlan() {
        switch(this) {
            case REGIONAL_MONTHLY:
            case REGIONAL_SIX_MONTH:
            case REGIONAL_ANNUAL:
                return true;
            default:
                return false;
        }
    }
    
    /***
     * <h1>isNationalPlan</h1>
     * <p>purpose: Is the subscription Plan a National Plan?</p>
     * @return true if subscription = NATIONAL_MONTHLY | NATIONAL_SIX_MONTH | NATIONAL_ANNUAL, false otherwise
     */
    public boolean isNationalPlan() {
        switch(this) {
            case NATIONAL_MONTHLY:
            case NATIONAL_SIX_MONTH:
            case NATIONAL_ANNUAL:
                return true;
            default:
                return false;
        }
    }
    
    /***
     * <h1>getLocationType</h1>
     * <p>purpose: Based on the value of the subscription plan,
     * 	return a String with the location type
     * @return "Basic" | "State" | "Regional" | "National" 
     */
    public String getLocationType() {
    	if (this.isStatePlan()) {
    		return "State";
    	} else if (this.isRegionalPlan()) {
    		return "Regional";
    	} else if (this.isNationalPlan()) {
    		return "National";
    	} else {
    		return "Basic";
    	}
    }
    
    /***
     * <h1>getChargbeeSuscriptionName</h1>
     * <p>purpose: Return a string for the subscription plan that<br>
     * 	displays per Chargebee</p>
     * @return String
     */
    public String getChargebeeSubscriptionName() {
    	String subName = null;
    	
    	switch (System.getProperty("server")) {

        // QA         
		case "qa":
	    	switch (this) {
	    	case BASIC:
	    		subName =  "BidSync Basic";
	    		break;
	    	case STATE_MONTHLY:
	    		subName =  "BidSync State - Monthly Online";
	    		break;
	    	case STATE_SIX_MONTH:
	    		subName =  "BidSync State - 6 Month Online";
	    		break;
	    	case STATE_ANNUAL:
	    		subName =  "BidSync State - Yearly Online";
	    		break;
	    	case REGIONAL_MONTHLY:
	    		subName =  "BidSync Regional - Monthly Online";
	    		break;
	    	case REGIONAL_SIX_MONTH:
	    		subName =  "BidSync Regional - 6 Month Online";
	    		break;
	    	case REGIONAL_ANNUAL:
	    		subName =  "BidSync Regional - Yearly Online";
	    		break;
	    	case NATIONAL_MONTHLY:
	    		subName =  "BidSync National - Monthly Online";
	    		break;
	    	case NATIONAL_SIX_MONTH:
	    		subName =  "BidSync National - 6 Month Online";
	    		break;
	    	case NATIONAL_ANNUAL:
	    		subName =  "BidSync National - Yearly Online";
	    		break;
	    	default:
	    		subName =  "";
	    	}
			break;
			
		//  Dev
		case "dev":
			switch (this) {
	    	case BASIC:
	    		subName =  "BidSync Basic";
	    		break;
	    	case STATE_MONTHLY:
	    		subName =  "BidSync State - Monthly Online";
	    		break;
	    	case STATE_SIX_MONTH:
	    		subName =  "BidSync State - 6 Month Online";
	    		break;
	    	case STATE_ANNUAL:
	    		subName =  "BidSync State - Yearly Online";
	    		break;
	    	case REGIONAL_MONTHLY:
	    		subName =  "BidSync Regional - Monthly Online";
	    		break;
	    	case REGIONAL_SIX_MONTH:
	    		subName =  "BidSync Regional - 6 Month Online";
	    		break;
	    	case REGIONAL_ANNUAL:
	    		subName =  "BidSync Regional - Yearly Online";
	    		break;
	    	case NATIONAL_MONTHLY:
	    		subName =  "BidSync National - Monthly Online";
	    		break;
	    	case NATIONAL_SIX_MONTH:
	    		subName =  "BidSync National - 6 Month Online";
	    		break;
	    	case NATIONAL_ANNUAL:
	    		subName =  "BidSync National - Yearly Online";
	    		break;
	    	default:
	    		subName =  "";
	    	}
			break;
        	
		case "stage":
			switch (this) {
	    	case BASIC:
	    		subName =  "BidSync Basic";
	    		break;
	    	case STATE_MONTHLY:
	    		subName =  "BidSync State - Monthly Online";
	    		break;
	    	case STATE_SIX_MONTH:
	    		subName =  "BidSync State - 6 Month Online";
	    		break;
	    	case STATE_ANNUAL:
	    		subName =  "BidSync State - Yearly Online";
	    		break;
	    	case REGIONAL_MONTHLY:
	    		subName =  "BidSync Regional - Monthly Online";
	    		break;
	    	case REGIONAL_SIX_MONTH:
	    		subName =  "BidSync Regional - 6 Month Online";
	    		break;
	    	case REGIONAL_ANNUAL:
	    		subName =  "BidSync Regional - Yearly Online";
	    		break;
	    	case NATIONAL_MONTHLY:
	    		subName =  "BidSync National - Monthly Online";
	    		break;
	    	case NATIONAL_SIX_MONTH:
	    		subName =  "BidSync National - 6 Month Online";
	    		break;
	    	case NATIONAL_ANNUAL:
	    		subName =  "BidSync National - Yearly Online";
	    		break;
	    	default:
	    		subName =  "";
	    	}
			break;
			}
    	return subName;
    }
    
    /***
     * <h1>getChargbeeSuscriptionDropdownName</h1>
     * <p>purpose: Return a string for the subscription plan that<br>
     * 	displays in Chargebee dropdown menu</p>
     * @return String
     */
    public String getChargebeeSubscriptionDropdownName() {
    	String subName = null;
    	
    	switch (System.getProperty("server")) {

        // QA         
		case "qa":
	    	switch (this) {
	    	case BASIC:
	    		subName =  "BidSync Basic";
	    		break;
	    	case STATE_MONTHLY:
	    		subName =  "BidSync State - Monthly Online";
	    		break;
	    	case STATE_SIX_MONTH:
	    		subName =  "BidSync State - 6 Month Online";
	    		break;
	    	case STATE_ANNUAL:
	    		subName =  "BidSync State - Yearly Online";
	    		break;
	    	case REGIONAL_MONTHLY:
	    		subName =  "BidSync Regional - Monthly Online";
	    		break;
	    	case REGIONAL_SIX_MONTH:
	    		subName =  "BidSync Regional - 6 Month Online";
	    		break;
	    	case REGIONAL_ANNUAL:
	    		subName =  "BidSync Regional - Yearly Online";
	    		break;
	    	case NATIONAL_MONTHLY:
	    		subName =  "BidSync National - Monthly Online";
	    		break;
	    	case NATIONAL_SIX_MONTH:
	    		subName =  "BidSync National - 6 Month Online";
	    		break;
	    	case NATIONAL_ANNUAL:
	    		subName =  "BidSync National - Yearly Online";
	    		break;
	    	default:
	    		subName =  "";
	    	}
			break;
			
		//  Dev
		case "dev":
			switch (this) {
	    	case BASIC:
	    		subName =  "BidSync Basic";
	    		break;
	    	case STATE_MONTHLY:
	    		subName =  "BidSync State - Monthly Online";
	    		break;
	    	case STATE_SIX_MONTH:
	    		subName =  "BidSync State - 6 Month Online";
	    		break;
	    	case STATE_ANNUAL:
	    		subName =  "BidSync State - Yearly Online";
	    		break;
	    	case REGIONAL_MONTHLY:
	    		subName =  "BidSync Regional - Monthly Online";
	    		break;
	    	case REGIONAL_SIX_MONTH:
	    		subName =  "BidSync Regional - 6 Month Online";
	    		break;
	    	case REGIONAL_ANNUAL:
	    		subName =  "BidSync Regional - Yearly Online";
	    		break;
	    	case NATIONAL_MONTHLY:
	    		subName =  "BidSync National - Monthly Online";
	    		break;
	    	case NATIONAL_SIX_MONTH:
	    		subName =  "BidSync National - 6 Month Online";
	    		break;
	    	case NATIONAL_ANNUAL:
	    		subName =  "BidSync National - Yearly Online";
	    		break;
	    	default:
	    		subName =  "";
	    	}
			break;
        	
		case "stage":
			switch (this) {
	    	case BASIC:
	    		subName =  "BidSync Basic";
	    		break;
	    	case STATE_MONTHLY:
	    		subName =  "BidSync State - Monthly";
	    		break;
	    	case STATE_SIX_MONTH:
	    		subName =  "BidSync State - 6 Month";
	    		break;
	    	case STATE_ANNUAL:
	    		subName =  "BidSync State - Yearly";
	    		break;
	    	case REGIONAL_MONTHLY:
	    		subName =  "BidSync Regional - Monthly";
	    		break;
	    	case REGIONAL_SIX_MONTH:
	    		subName =  "BidSync Regional - 6 Month";
	    		break;
	    	case REGIONAL_ANNUAL:
	    		subName =  "BidSync Regional - Yearly";
	    		break;
	    	case NATIONAL_MONTHLY:
	    		subName =  "BidSync National - Monthly";
	    		break;
	    	case NATIONAL_SIX_MONTH:
	    		subName =  "BidSync National - 6 Month";
	    		break;
	    	case NATIONAL_ANNUAL:
	    		subName =  "BidSync National - Yearly";
	    		break;
	    	default:
	    		subName =  "";
	    	}
			break;
			}
    	return subName;
    }
    
    
    /***
     * <h1>getChargebeeSubscriptionDBName</h1>
     * <p>purpose: DB is storing extension -Online for each planfor Chargebee</p>
     * @return String
     */
    public String getChargebeeSubscriptionBackEndName() {
    	
    	String subName = null;
    	
    	switch (System.getProperty("server")) {

        // QA         
		case "qa":
	    	switch (this) {
	    	case BASIC:
	    		subName =  "BidSync Basic";
	    		break;
	    	case STATE_MONTHLY:
	    		subName =  "BidSync State - Monthly Online";
	    		break;
	    	case STATE_SIX_MONTH:
	    		subName =  "BidSync State - 6 Month Online";
	    		break;
	    	case STATE_ANNUAL:
	    		subName =  "BidSync State - Yearly Online";
	    		break;
	    	case REGIONAL_MONTHLY:
	    		subName =  "BidSync Regional - Monthly Online";
	    		break;
	    	case REGIONAL_SIX_MONTH:
	    		subName =  "BidSync Regional - 6 Month Online";
	    		break;
	    	case REGIONAL_ANNUAL:
	    		subName =  "BidSync Regional - Yearly Online";
	    		break;
	    	case NATIONAL_MONTHLY:
	    		subName =  "BidSync National - Monthly Online";
	    		break;
	    	case NATIONAL_SIX_MONTH:
	    		subName =  "BidSync National - 6 Month Online";
	    		break;
	    	case NATIONAL_ANNUAL:
	    		subName =  "BidSync National - Yearly Online";
	    		break;
	    	default:
	    		subName =  "";
	    	}
			break;
			
		//  Dev
		case "dev":
	    	switch (this) {
	    	case BASIC:
	    		subName = "BidSync Basic";
	    		break;
	    	case STATE_MONTHLY:
	    		subName =  "BidSync State - Monthly Online";
	    		break;
	    	case STATE_SIX_MONTH:
	    		subName =  "BidSync State - 6 Month Online";
	    		break;
	    	case STATE_ANNUAL:
	    		subName =  "BidSync State - Yearly Online";
	    		break;
	    	case REGIONAL_MONTHLY:
	    		subName =  "BidSync Regional - Monthly Online";
	    		break;
	    	case REGIONAL_SIX_MONTH:
	    		subName =  "BidSync Regional - 6 Month Online";
	    		break;
	    	case REGIONAL_ANNUAL:
	    		subName =  "BidSync Regional - Yearly Online";
	    		break;
	    	case NATIONAL_MONTHLY:
	    		subName =  "BidSync National - Monthly Online";
	    		break;
	    	case NATIONAL_SIX_MONTH:
	    		subName =  "BidSync National - 6 Month Online";
	    		break;
	    	case NATIONAL_ANNUAL:
	    		subName =  "BidSync National - Yearly Online";
	    		break;
	    	default:
	    		subName =  "";
	    	}
			break;
        	
		case "stage":
	    	switch (this) {
	    	case BASIC:
	    		subName =  "BidSync Basic";
	    		break;
	    	case STATE_MONTHLY:
	    		subName =  "BidSync State - Monthly Online";
	    		break;
	    	case STATE_SIX_MONTH:
	    		subName =  "BidSync State - 6 Month Online";
	    		break;
	    	case STATE_ANNUAL:
	    		subName =  "BidSync State - Yearly Online";
	    		break;
	    	case REGIONAL_MONTHLY:
	    		subName =  "BidSync Regional - Monthly Online";
	    		break;
	    	case REGIONAL_SIX_MONTH:
	    		subName =  "BidSync Regional - 6 Month Online";
	    		break;
	    	case REGIONAL_ANNUAL:
	    		subName =  "BidSync Regional - Yearly Online";
	    		break;
	    	case NATIONAL_MONTHLY:
	    		subName =  "BidSync National - Monthly Online";
	    		break;
	    	case NATIONAL_SIX_MONTH:
	    		subName =  "BidSync National - 6 Month Online";
	    		break;
	    	case NATIONAL_ANNUAL:
	    		subName =  "BidSync National - Yearly Online";
	    		break;
	    	default:
	    		subName =  "";
	    	}
			break;
			}
    	return subName;
    }
}

