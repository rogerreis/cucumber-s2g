Feature: Database example
  This is just an example on how to use database access

#  Scenario: Database query example
#    Then the query "select users.username, user_setting.setting_value FROM users INNER JOIN user_setting ON users.user_id=user_setting.user_id WHERE username = 'supplier@periscopeholdings.com'" against database "auth" will contain "false" in column 2
    
  Scenario: Get bid info from json in database
  	1) query the database for a particular type of bid
  	2) parse the json for data you need   
  	
  	Given I have collected the result for query "select cleaned_detail from solicitation where detail similar to '%(bid_document)%' AND date_created > current_date - interval '1' day AND process_detail = 'Solicitation is processed and sent to survivor' limit 1" against database "solicitation_collector" and put it in a global variable named "glbCleanedDetail"
  	And I have retrieved the value of "bidTitle" from the json in global variable "glbCleanedDetail" and put it in another global variable named "glbTitle"
  	And I put the value of global variable named "glbTitle" into bidSearch and do the search

  	
