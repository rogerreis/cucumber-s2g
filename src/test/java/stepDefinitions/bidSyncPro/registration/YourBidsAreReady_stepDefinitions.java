package stepDefinitions.bidSyncPro.registration;

import core.CoreAutomation;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import pages.bidSyncPro.registration.YourBidsAreReady;
import pages.common.helpers.GlobalVariables;

import org.openqa.selenium.support.events.EventFiringWebDriver;

public class YourBidsAreReady_stepDefinitions {
	
	private EventFiringWebDriver driver;
	
	public YourBidsAreReady_stepDefinitions(CoreAutomation automation) {
		this.driver   = automation.getDriver();
	}
	@When ("^I click on the EDIT COMMODITY CODES button on the Your Bids are Ready screen$")
	public void I_click_on_the_EDIT_COMMODITY_CODES_button_on_the_Your_Bids_are_Ready_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on \"EDIT COMMODITY CODES\"");
		YourBidsAreReady yourBidsAreReady = new YourBidsAreReady(driver);
		// Status message can block the button, so wait for it to appear and disappear
		yourBidsAreReady
			.waitForStatusMessageToDisappear();
		yourBidsAreReady
			.clickEditCommodityCodes();
	}	

	@When ("^I click on the INCREASE BID RELEVANCY button on the Your Bids are Ready screen$")
	public void I_click_on_the_INCREASE_BID_RELEVANCY_button_on_the_Your_Bids_are_Ready_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on \"INCREASE BID RELEVANCY\"");

		new YourBidsAreReady(driver)
			.clickIncreaseBidRelevancyBtn();
	}
	
	@Then("^I will verify that the Your Bids are Ready screen has properly initialized$")
	public void I_will_verify_that_the_Your_Bids_are_Ready_screen_has_properly_initialized() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying \"Your Bids are Ready\" screen has initialized");

		new YourBidsAreReady(driver)
			.verifyPageInitialization();
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: \"Your Bids Are Ready\" has correctly initialized");
	}

}
