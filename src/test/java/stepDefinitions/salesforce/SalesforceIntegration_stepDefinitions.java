package stepDefinitions.salesforce;

import cucumber.api.java.en.Then;
import data.salesforceIntegration.Click;
import pages.common.helpers.GlobalVariables;

import org.assertj.core.api.SoftAssertions;
import utility.database.SQLConnector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class SalesforceIntegration_stepDefinitions {
    
    @Then("^I verify the clicks have been correctly tracked in the database$")
    public void iVerifyTheClicksHaveBeenCorrectlyTrackedInTheDatabase() {
        List<Click> clickLog = GlobalVariables.getStoredObject("clickLog");
        Map<Click, Integer> clickCounts = getClickCountsFromLog(clickLog);
        SoftAssertions softly = new SoftAssertions();
        for(Entry<Click, Integer> entry : clickCounts.entrySet()) {
            Click click = entry.getKey();
            Integer expectedCount = entry.getValue();
            Integer actualCount = getClickCountFromDatabase(click);
            String description = String.format("Click count for user '%s' | bid '%s' | '%s'", click.getUserId(), click.getBidId(), click.getSource());
            softly.assertThat(actualCount).as(description).isEqualTo(expectedCount);
        }
        softly.assertAll();
    }
    
    private Map<Click, Integer> getClickCountsFromLog(List<Click> clickLog) {
        Map<Click, Integer> clickCounts = new HashMap<>();
        for(Click click : clickLog) {
            if(clickCounts.containsKey(click)) {
                int count = clickCounts.get(click);
                clickCounts.replace(click, ++count);
            } else {
                clickCounts.put(click, 1);
            }
        }
        return clickCounts;
    }
    
    private Integer getClickCountFromDatabase(Click click) {
        String query = String.format("select count(*) from bid_click where user_id = '%s' and solicitation_id = '%s' and source = '%s';",
                click.getUserId(),
                click.getBidId(),
                click.getSource()
        );
        
        System.out.printf(GlobalVariables.getTestID() + " QUERY: %s\n", query);
        
        List<Integer> counts = new ArrayList<>();
        try (ResultSet rs = SQLConnector.executeQuery("survivor", query)) {
            while (rs.next()) {
                counts.add(rs.getInt("count"));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    
        if (counts.isEmpty()) {
            return 0;
        }
        return counts.get(0);
    }
    
}
