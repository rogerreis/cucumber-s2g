package pages.bidSyncPro.registration;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class CompanyLookupResult</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects to define what an expected Company Lookup result should contain<br>
 * 		The Company Lookup may be accessed off of the "Get Invited To Bids" screen</p>
 */
public class CompanyLookupResult extends BasePageActions {

	// WebElement describing the Company Lookup Result
	private String rootXPath;			// XPath for the CompanyLookupOption

	// Company Lookup Result Elements
	private String strCompanyName;		// Company Name 		ex: "New York Times"
	private String strCompanyLogoSRC;	// Company Logo SRC		ex: "https://logo.clearbit.com/nytimes.com"
	private String strCompanyURL; 		// Company URL			ex: "nytimes.com"
	
	// Other
	private EventFiringWebDriver driver;
	
	/***
	 * <h1>CompanyLookupResult</p>
	 * @param driver = driver
	 * @param strXPath = XPath that describes a result row of the Company Lookup
	 */
	public CompanyLookupResult (EventFiringWebDriver driver, String strXPath) {
		super(driver);
		this.driver    = driver;
		this.rootXPath = strXPath;
		setCompanyName();
		setCompanyLogoSRC();
		setCompanyURL();
	}
	
	/***
	 * <h1>setCompanyName</h1>
	 * <p>purpose: Grab and set company name from the Company Lookup result.<br>
	 * 	Note: Company Name must always be present</p>
	 * @return None
	 */
	private void setCompanyName() {
		String XPath = this.rootXPath + "//div[@class='right']/div[@class='name-text']/span";

		List <WebElement> ourEle = this.findAllOrNoElements(driver, By.xpath(XPath));

		this.strCompanyName = ourEle.get(0).getAttribute("innerText");
		System.out.println(GlobalVariables.getTestID() + " \"" + this.strCompanyName + "\"");
	}

	/***
	 * <h1>setCompanyLogoSrc</h1>
	 * <p>purpose: Grab and set company logo SRC from the Company Lookup result</p>
	 * @return None
	 */
	private void setCompanyLogoSRC() {
		String XPath = this.rootXPath + "//div[@class='left']/img[@id='imgCompanySvg']";

		List <WebElement> ourEle = this.findAllOrNoElements(driver, By.xpath(XPath));

		if (ourEle.isEmpty()) { this.strCompanyLogoSRC = "";
		} else { this.strCompanyLogoSRC = ourEle.get(0).getAttribute("src"); }
	}

	/***
	 * <h1>setCompanURL</h1>
	 * <p>purpose: Grab and set company URL from the Company Lookup result</p>
	 * @return None
	 */
	private void setCompanyURL() {
		String XPath = this.rootXPath + "//div[@class='domain-text']/span";
		
		// We might or might not have a URL displaying
		if (this.doesPageObjectExist(driver, By.xpath(XPath))) {
			WebElement ourEle = driver.findElement(By.xpath(XPath));
			this.strCompanyURL = ourEle.getAttribute("innerText");
		} else {
			System.out.println(GlobalVariables.getTestID() + " INFO: No Company URL exists");
			this.strCompanyURL = ""; }

	}
	
	/***
	 * <h1>getXPathForMainElement</h1>
	 * <p>purpose: Return the xpath that described this result row in the Company Lookup</p>
	 * @return strXPath = XPath that describes this result row in the Company Lookup
	 */
	public String getXPathForMainElement() { return this.rootXPath;         }

	public String getCompanyName()         { return this.strCompanyName;    }
	public String getCompanyLogoSRC()      { return this.strCompanyLogoSRC; }
	public String getCompanyURL()          { return this.strCompanyURL;     }
		
}
