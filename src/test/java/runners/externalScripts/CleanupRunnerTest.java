package runners.externalScripts;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

// This runner is used by jenkins to run cleanup tests
// Tests should be tagged with @Cleanup to be included in the jenkins nightly cleanup run.
// Tests tagged ~@ignore will be skipped from the jenkins nightly run.
@RunWith(Cucumber.class)
@CucumberOptions(
		plugin ={"pretty" , "json:target/cucumber/cucumber.json", "html:target/cucumber"},
        features = "src/test/resources/features",
        glue = {"stepDefinitions"},
        tags = {"~@ignore", "@Cleanup"},

        dryRun = false,
        snippets=SnippetType.CAMELCASE
)

public class CleanupRunnerTest {

}
