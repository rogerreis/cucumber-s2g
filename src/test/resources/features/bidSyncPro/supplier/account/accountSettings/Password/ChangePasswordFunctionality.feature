@must @BidSync_AccountChangePassword @administration @level3
Feature: Account > Account Settings - Change Password: Functionality
	BIDSYNC-77: Resetting the password needs to delete the JWT token
	BIDSYNC-238: Password Reset Screen, Incorrect Error


@BIDSYNC-77
Scenario: If user changes password from My Account, then JWT token is deleted and then reissued in local storage
	Given I have registered a random user
	When I try to change my password from the My Account page
	Then I will verify that my original JWT token is deleted in local browser storage and a new JWT token is issued


@BIDSYNC-238
Scenario: Password reset error message will hide after updates confirmed password to match password
	Given I have registered a random user
	And I try to change my password from the My Account page
	When I submit a password that does not match the confirmation password into the Reset Your Password dialog
	Then I will see an error message for the mismatched password and confirm password fields on the Reset Your Password dialog
	And I will see that the Reset Password button is disabled on the Reset Your Password dialog
	When I update the confirm password field to match the password field on the Reset Your Password dialog
	Then I will verify that the Reset Your Password dialog does not display any error message
	And I can successfully submit my new password through the Reset Your Password dialog and log in with this new password
   
    
@ignore @ARGO-2430
Scenario: Change in password is reflected in both Pro and Classic
	This scenario hits the Bidsync Classic database, and that requires a proprietary
	oracle dependency that isn't in maven central.  See the instructions above the
	oracle dependency in pom.xml if this breaks for you.
	Tag as @must again when the jenkins image has been updated with the Oracle jar
    
	Given I have registered a random user
	When I change that users password to a new random password
	Then the password change will be reflected in both the Pro and Classic databases
