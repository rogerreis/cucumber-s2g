/**
 * 
 */
package pages.bidSyncPro.supplier.account.accountSettings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.HelperMethods;

/**
 * @author jlara
  * <p>TODO: Refactor this class</p>*
 */
public class ChangePasswordModal_Step3 {

	private final String xpathModal = "//bidsync-change-password/div/mat-card[@id='matChangePasswordComponentCard']";
	//text: Change Password
	private final String xpathHeaderLabel = xpathModal + "//mat-card-title[@id='matChangePasswordTitle']";
	//text: Please check your email. The token expires at 6:08 PM.
	private final String xpathDescriptionLabel = xpathModal + "//mat-card-content/p";
	//contains text: CLOSE
	private final String xpathCloseButton = xpathModal + "//mat-dialog-actions/button";
	
	public final String pagePath = "admin/my-account";
	
	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;
	
	public ChangePasswordModal_Step3(EventFiringWebDriver driver) {
	    this.driver = driver;
	    this.helperMethods = new HelperMethods();
	}
	
	
	public AccountSettings clickCloseButton() {
		getCloseButton().click();
		return new AccountSettings(driver);
	}

	
	public WebElement getHeaderLabel() {
        this.helperMethods.waitForVisibilityOfXpath(driver, this.xpathHeaderLabel, Integer.parseInt(System.getProperty("defaultTimeOut")));
	    return driver.findElement(By.xpath(xpathHeaderLabel));
	}

	public WebElement getDescriptionLabel() {
        this.helperMethods.waitForVisibilityOfXpath(driver, this.xpathDescriptionLabel, Integer.parseInt(System.getProperty("defaultTimeOut")));
	    return driver.findElement(By.xpath(xpathDescriptionLabel));
	}

	public WebElement getCloseButton() {
        this.helperMethods.waitForVisibilityOfXpath(driver, this.xpathCloseButton, Integer.parseInt(System.getProperty("defaultTimeOut")));
	    return driver.findElement(By.xpath(xpathCloseButton));
	}
	

}
