package pages.bidSyncPro.supplier.common;

import pages.bidSyncPro.supplier.companySettings.agencyInteractions.AgencyInteraction;
import pages.bidSyncPro.supplier.companySettings.companyProfile.CompanyProfile;
import pages.bidSyncPro.supplier.companySettings.manageSubscriptions.ManageSubscriptions;
import pages.bidSyncPro.supplier.companySettings.manageUsers.ManageUsers;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.BasePageActions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CompanySettingsDropDown extends BasePageActions {
	HelperMethods helperMethods = new HelperMethods();
    private EventFiringWebDriver driver;
    
    // xpaths 
    // Nav Menu Buttons
    private final String companyProfile_xpath         = "//button[@role='menuitem' and span[contains(text(),'Company Profile')]]/span";
    private final String manageSubscriptions_xpath    = "//button[@role='menuitem' and span[contains(text(),'Manage Subscriptions')]]/span";
    private final String agencyInteraction_xpath      = "//button[@role='menuitem' and span[contains(text(),'Agency Interaction')]]/span";
    private final String manageUsers_xpath            = "//button[@role='menuitem' and span[contains(text(),'Manage Users')]]/span";
    
    // Nav Menu Images
    private final String companyProfileImg_xpath      = "//button[@role=\"menuitem\"and contains(span, \"Company Profile\")]/mat-icon[@role=\"img\" and contains(text(), \"domain\")]";
    private final String manageUsersImg_xpath         = "//button[@role=\"menuitem\"and contains(span, \"Manage Users\")]/mat-icon[@role=\"img\" and contains(text(), \"group\")]";
    private final String manageSubscriptionsImg_xpath = "//button[@role=\"menuitem\"and contains(span, \"Manage Subscriptions\")]/mat-icon[@role=\"img\" and contains(text(), \"assignment\")]";
    private final String agencyInteractionImg_xpath   = "//button[@role=\"menuitem\"and contains(span, \"Agency Interaction\")]/mat-icon[@role=\"img\" and contains(text(), \"work\")]";
    
    public CompanySettingsDropDown(EventFiringWebDriver driver) {
    	super(driver);
        this.driver = driver;
    }
    
    /* ----- getters ----- */
    // Nav Menu Buttons
    private WebElement getCompanyProfile()         {  
    	waitForVisibility(By.xpath(this.companyProfile_xpath));
		helperMethods.addSystemWait(500, TimeUnit.MILLISECONDS);
		return findByClickability(By.xpath(this.companyProfile_xpath));
    	}  
    private WebElement getManageSubscriptions()    { 
    	waitForVisibility(By.xpath(this.manageSubscriptions_xpath));
		helperMethods.addSystemWait(500, TimeUnit.MILLISECONDS);
		return findByClickability(By.xpath(this.manageSubscriptions_xpath));
    	} 
    private WebElement getAgencyInteraction()      { 
    	waitForVisibility(By.xpath(this.agencyInteraction_xpath));
		helperMethods.addSystemWait(500, TimeUnit.MILLISECONDS);
		return findByClickability(By.xpath(this.agencyInteraction_xpath));
    	} 
    private WebElement getManageUsers()            {            
    	waitForVisibility(By.xpath(this.manageUsers_xpath));
		helperMethods.addSystemWait(500, TimeUnit.MILLISECONDS);
		return findByClickability(By.xpath(this.manageUsers_xpath));
		} 

    // Nav Menu Images
    private WebElement getCompanyProfileImg()      { return findByVisibility(By.xpath(this.companyProfileImg_xpath));      } 
    private WebElement getManageSubscriptionsImg() { return findByVisibility(By.xpath(this.manageSubscriptionsImg_xpath)); } 
    private WebElement getAgencyInteractionImg()   { return findByVisibility(By.xpath(this.agencyInteractionImg_xpath));   } 
    private WebElement getManageUsersImg()         { return findByVisibility(By.xpath(this.manageUsersImg_xpath));         } 

    /* ----- methods ----- */
    
    /***
     * <h1>openCompanyProfile</h1>
     * <p>purpose: Select the "Company Profile" option on the "Company Setting" nav menu.<br>
     * 	App should navigate forward on selection</p>
     * @return CompanyProfile 
     */
    public CompanyProfile openCompanyProfile() {
    	((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getCompanyProfile());
        return new CompanyProfile(driver);
    }
    
    /***
     * <h1>openManageUsers</h1>
     * <p>purpose: Select the "Manage Users" option on the "Company Setting" nav menu<br>
     * 	App should navigate forward on selection</p>
     * @return ManageUsers 
     */
    public ManageUsers openManageUsers() {
    	((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getManageUsers());
    	return new ManageUsers(driver);
    }
    
    /***
     * <h1>openManageSubscriptions</h1>
     * <p>purpose: Select the "Manage Subscriptions" option on the "Company Setting" nav menu<br>
     * 	App should navigate forward on selection</p>
     * @return ManageSubscriptions 
     * @throws InterruptedException 
     */
    public ManageSubscriptions openManageSubscriptions() throws InterruptedException {
    	((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getManageSubscriptions());
        return new ManageSubscriptions(driver);
    }
    
    /***
     * <h1>openAgencyInteraction</h1>
     * <p>purpose: Select the "Agency Interaction" option on the "Company Setting" nav menu<br>
     * 	App should navigate forward on selection</p>
     * @return AgencyInteractionPage 
     */
    public AgencyInteraction openAgencyInteraction() {
    	((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getAgencyInteraction());
        return new AgencyInteraction(driver);
    }
    
    /***
     * <h1>verifyAllOptionsInCompanySettingsDropdown</h1>
     * <p>purpose: Verify that all the options (buttons + images) in the company settings dropdown appear</p>
     * @return CompanySettingsDropdown
     */
    public CompanySettingsDropDown verifyAllOptionsInCompanySettingsDropdown() {
    	this.getManageUsers();
    	this.getManageUsersImg();
    	this.getAgencyInteraction();
    	this.getAgencyInteractionImg();
    	this.getCompanyProfile();
    	this.getCompanyProfileImg();
    	this.getManageSubscriptions();
    	this.getManageSubscriptionsImg();
    	
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified all options and images in the \"Company Settings\" menu have correctly loaded");
        return this;
    }
}
