package stepDefinitions.bidSyncPro.supplier.dashboard.bidLists;

import core.CoreAutomation;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidListRow;
import pages.common.helpers.GlobalVariables;

import org.junit.Assert;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.List;
import java.util.stream.Collectors;

public class MatchScore_stepDefinitions {
	
	EventFiringWebDriver driver; 

	public MatchScore_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
	}
	
	@When ("^I select a bid with a match score between \"(.*?)\" and \"(.*?)\" from the bid list$")
	public void I_select_a_bid_with_a_match_score_between_lowerBounds_and_upperBounds_from_the_bid_list(int lowerBounds, int upperBounds){
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Selecting bid with match score between \"%d\" - \"%d\"\n", lowerBounds, upperBounds);
		
		// Locate the  bid in the list with the match score within our range
		// NOTE: You'll need to make sure that you have canned results that will return values
		if(lowerBounds==0){
			new BidList(driver).clickOnMatchHeader();
		}
		BidListRow bid = 
			new BidList(driver)
			.allVisibleBids().stream()
			.filter(selectedBid -> selectedBid.isBidWithinMatchScoreBounds(lowerBounds, upperBounds).equals(true))
			.findFirst().orElse(null);
		
		// dmidura: for now, let's assume that we have canned bids that will be returned per our search result
		// and that we don't have to click on the "Load More" button 
		Assert.assertNotNull("Test Failed: No Bid located within Match Score bounds", bid);
		System.out.println(GlobalVariables.getTestID() + " INFO: Bid \"" + bid.bidInfo.getBidTitle() + "\" selected");
		
		// Store out the selected bid to be used later on
		if((System.getProperty("server").contentEquals("stage")) && (lowerBounds == 50)) { // mandrews:the bids for "POSSIBLE" don't exist on STAGE
			// do nothing and skip the assertion 
		} else { 
			GlobalVariables.storeObject("selectedBid", bid);
		}
	}
	
	@When ("^I select a bid with a match score between \"(.*?)\" and \"(.*?)\" from the bid list of match type \"(.*?)\"$")
	public void I_select_a_bid_with_a_match_score_between_lowerBounds_and_upperBounds_from_the_bid_list_of_match_type(int lowerBounds, int upperBounds, String matchType){
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Selecting bid with match score between \"%d\" - \"%d\"\n", lowerBounds, upperBounds);
		
		// Locate the  bid in the list with the match score within our range
		// NOTE: You'll need to make sure that you have canned results that will return values
		if(lowerBounds==0){
			new BidList(driver).clickOnMatchHeader();
		}
		BidListRow bid = 
			new BidList(driver)
			.getBidsTillMatchType(matchType).stream()
			.filter(selectedBid -> selectedBid.isBidWithinMatchScoreBounds(lowerBounds, upperBounds).equals(true))
			.findFirst().orElse(null);
		
		// dmidura: for now, let's assume that we have canned bids that will be returned per our search result
		// and that we don't have to click on the "Load More" button 
		if((System.getProperty("server").contentEquals("stage")) && (lowerBounds == 50)) { // mandrews:the bids for "POSSIBLE" don't exist on STAGE
			// do nothing and skip the assertion 
		} else { 
			// else do the normal assertion
			Assert.assertNotNull("Test Failed: No Bid located within Match Score bounds", bid);
			System.out.println(GlobalVariables.getTestID() + " INFO: Bid \"" + bid.bidInfo.getBidTitle() + "\" selected");
		
			// Store out the selected bid to be used later on
			GlobalVariables.storeObject("selectedBid", bid);
		}
	}

	@Then ("^I will verify that the match score indicator displays the text \"(.*?)\" with the indicator color \"(.*?)\"$")
	public void I_will_verify_that_the_match_score_indicator_displays_the_text_with_the_indicator_color(String text, String color){
		if((System.getProperty("server").contentEquals("stage")) && (text.contentEquals("POSSIBLE"))) { // mandrews:the bids for "POSSIBLE" don't exist on STAGE
			// do nothing and skip the assertion 
		} else { 
			// else do the normal assertion
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Verifying match score indicator is \"%s\" text and \"%s\" color\n", text, color);
		
		// Grab our selected bid
		BidListRow bid = GlobalVariables.getStoredObject("selectedBid");

		// Now verify that the match score displays the correct text and color
		bid.verifyMatchScoreText(text)
		   .verifyMatchScoreColor(color);
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Match score indicator displays correct text and color");
		}
	}
	
	@Then ("^I will verify that no match indicator is displayed for each bid$")
	public void I_will_verify_that_no_match_indicator_is_displayed_for_the_bid() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that match indicator is not displayed for each bid");
		new BidList(driver)
			.allVisibleBids().stream()
			.forEach(bid->bid.verifyNoMatchScoreIndicator());
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Match score indicator does not display for bid");
	}


	@Then("^I will verify each bid contains an indicator, color, and text as expected \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
	public void iWillVerifyEachBidContainsAnIndicatorColorAndTextAsExpected(int lowerBounds, int upperBounds, String text, String color) {
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Verifying there is an indicator with the appropriate color and text");

		new BidList(driver)
				.allVisibleBids().stream()
				.filter(selectedBid -> selectedBid.isBidWithinMatchScoreBounds(lowerBounds, upperBounds).equals(true))
				.forEach(
						bid->bid.verifyMatchScoreColor(color)
								.verifyMatchScoreText(text)
				);

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Match score indicator displays correct text and color for results between " + lowerBounds + " & " + upperBounds );

	}

	@When("^I select all bids from bid list with a match score between \"([^\"]*)\" \"([^\"]*)\"$")
	public void iSelectAllBidsFromBidListWithAMatchScoreBetween(int lowerBounds, int upperBounds) throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Selecting all bids from bid list with match score between \"" + lowerBounds + "\" - \"" + upperBounds + "\"");

		List<BidListRow> bids =
				new BidList(driver)
						.allVisibleBids().stream()
						.filter(selectedBid -> selectedBid.isBidWithinMatchScoreBounds(lowerBounds, upperBounds).equals(true))
						.collect(Collectors.toList());

		GlobalVariables.storeObject("selectedBids", bids);

		if(bids.isEmpty()) {
			System.out.println(GlobalVariables.getTestID() + " INFO: No bids between \"" + lowerBounds + "\" and \"" + upperBounds + "\".");
		} else {
			System.out.println(GlobalVariables.getTestID() + " INFO: \"" + bids.size() + "\" bids found between \"" + lowerBounds + "\" and \"" + upperBounds + "\".");
		}

	}

	@Then("^I will verify that the match score indicator for all selected bids displays the text \"([^\"]*)\" with the indicator color \"([^\"]*)\"$")
	public void iWillVerifyThatTheMatchScoreIndicatorForAllSelectedBidsDisplaysTheTextWithTheIndicatorColor(String text, String color) throws Throwable {
		List<BidListRow> bids = GlobalVariables.getStoredObject("selectedBids");
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Verifying match score indicator for all selected bids displays \"" + color + "\" color and \"" + text + "\" text");
		if(text.equals("UNLIKELY")){
			new BidList(driver).clickOnMatchHeader();
		}
		bids.forEach(bid -> {
			bid.verifyMatchScoreText(text)
					.verifyMatchScoreColor(color);

			System.out.println(GlobalVariables.getTestID() + " Test Passed: Match score indicator displays correct text and color");
		});
	}
}