package stepDefinitions.hooks;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import core.CoreAutomation;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import pages.common.helpers.GlobalVariables;

/***
 * <h1>Class Hooks</h1>
 * <p>details: This class contains methods for displaying information about each test run to console
 */
public class Hooks {
	private EventFiringWebDriver driver;
	private final String line = ("\n========================================================\n");    	
	private final Boolean displayPerformanceLogs = true; //dmidura: Turn on perf logs when trying to track down failures

	public Hooks(CoreAutomation automation) {
		this.driver     = automation.getDriver();
	}

	@Before
	public void beforeCallingScenario(Scenario scenario) {

		/* ---- Generic print out for starting each test ------*/
		System.out.println(line + GlobalVariables.getTestID() + " Starting Scenario: " + scenario.getName() + "\n");
	}

	@After
	public void afterRunningScenario(Scenario scenario) {
		
		/* ---- Generic print out for ending each test ------*/
		String out = String.format(
		   line 
		   + "Finishing Scenario: " + scenario.getName() + ""
		   + "\nTest Status: " + scenario.getStatus()
		   + "\nLocal time: " + new java.util.Date().toString() 
		   + "\nUTC: " + Instant.now().toString())
		   + "\nUsers created in this test: " + GlobalVariables.getUsers().toString()
		   + line;
		System.out.println(out);

		/* ---- Print screen shot (for Jenkins) ------*/
		final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
		scenario.embed(screenshot, "image/png"); // stick it in the report

		/* ---- Print out for failed tests ------*/
		// get browser logs if failed - chrome only
		if (scenario.isFailed() && System.getProperty("browser").equals("chrome") && displayPerformanceLogs) {
			LogEntries logs = driver.manage().logs().get("browser");
			
			System.out.println(GlobalVariables.getTestID() + " ********BROWSER LOGS 1:");
			for (LogEntry entry : logs) {
				System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
			}
			
			System.out.println(GlobalVariables.getTestID() + " ********BROWSER LOGS 2:");
			for (LogEntry entry : driver.manage().logs().get(LogType.BROWSER)) {
				System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
			}

			System.out.println(GlobalVariables.getTestID() + " ********PERFORMANCE LOGS:");
			for (LogEntry entry : driver.manage().logs().get(LogType.PERFORMANCE)) {
				JsonElement jsonTree = new JsonParser().parse(entry.getMessage());

				String method = jsonTree.getAsJsonObject().get("message")
						.getAsJsonObject().get("method").getAsString();
				
				// mandrews: uncomment these to debug. They will print the entire json we're parsing
				//System.out.println(method);
				//System.out.println(entry.toString());
				
				// we only want the network-response-received items
				if (method.compareTo("Network.responseReceived") == 0) {
					if (jsonTree.getAsJsonObject().get("message")
							.getAsJsonObject().get("params")
							.getAsJsonObject().get("response")
							.getAsJsonObject().get("mimeType") != null) {

						String strMimeType = jsonTree.getAsJsonObject().get("message")
								.getAsJsonObject().get("params")
								.getAsJsonObject().get("response")
								.getAsJsonObject().get("mimeType").getAsString();

						// we only want the json stuff from the back end.  Not images and such.
						if (strMimeType.compareTo("application/json") == 0) {
							String strURL = jsonTree.getAsJsonObject().get("message")
									.getAsJsonObject().get("params")
									.getAsJsonObject().get("response")
									.getAsJsonObject().get("url").getAsString();

							// get rid of stuff where the matching request didn't have any headers (noise)
							if (jsonTree.getAsJsonObject().get("message").getAsJsonObject().get("params")
									.getAsJsonObject().get("response")
									.getAsJsonObject().get("requestHeaders") != null) {
								
								
								String strRequestMethod = "";
								
								// get the method of the request that matches this response:  "GET", "PUT", "POST", etc
								if (jsonTree.getAsJsonObject().get("message").getAsJsonObject().get("params")
										.getAsJsonObject().get("response")
										.getAsJsonObject().get("requestHeaders")
										.getAsJsonObject().get(":method") != null) {
									
											strRequestMethod = jsonTree.getAsJsonObject().get("message")
											.getAsJsonObject().get("params")
											.getAsJsonObject().get("response")
											.getAsJsonObject().get("requestHeaders")
											.getAsJsonObject().get(":method").getAsString();
								}
								// get the timer for how long it took
								String strReceiveHeadersEnd = jsonTree.getAsJsonObject().get("message")
										.getAsJsonObject().get("params")
										.getAsJsonObject().get("response")
										.getAsJsonObject().get("timing")
										.getAsJsonObject().get("receiveHeadersEnd").getAsString();
								System.out.println(strRequestMethod + " | " + strURL + "  | responseTime(ms): " + strReceiveHeadersEnd);

							}
						}
					}
				}
				
				
				
				// Also get websocket conversations
				// NOTE: Regular REST calls give the response time, but websocket does not.
				// To get delay response time on websocket requests, subtract the request timestamp from the response timestamp.
				// The result will be in seconds.
				if (method.compareTo("Network.webSocketFrameSent") == 0) {
					if (jsonTree.getAsJsonObject().get("message")
							.getAsJsonObject().get("params")
							.getAsJsonObject().get("response")
							.getAsJsonObject().get("payloadData") != null) {
						
						String strRequestID = jsonTree.getAsJsonObject().get("message")
								.getAsJsonObject().get("params")
								.getAsJsonObject().get("requestId").getAsString();

						String strPayload = jsonTree.getAsJsonObject().get("message")
								.getAsJsonObject().get("params")
								.getAsJsonObject().get("response")
								.getAsJsonObject().get("payloadData").getAsString();
						
						Float frameSentTimeStamp = jsonTree.getAsJsonObject().get("message")
								.getAsJsonObject().get("params")
								.getAsJsonObject().get("timestamp").getAsFloat();
						
								System.out.println(GlobalVariables.getTestID() + " stomp: " + method + " | requestId: " + strRequestID + "  | timestamp(secs): " + frameSentTimeStamp);
								System.out.println(GlobalVariables.getTestID() + " payload: " + strPayload + "\n");

							}
						}
				
				if (method.compareTo("Network.webSocketFrameReceived") == 0) {
					if (jsonTree.getAsJsonObject().get("message")
							.getAsJsonObject().get("params")
							.getAsJsonObject().get("response")
							.getAsJsonObject().get("payloadData") != null) {
						
						String strRequestID = jsonTree.getAsJsonObject().get("message")
								.getAsJsonObject().get("params")
								.getAsJsonObject().get("requestId").getAsString();

						String strPayload = jsonTree.getAsJsonObject().get("message")
								.getAsJsonObject().get("params")
								.getAsJsonObject().get("response")
								.getAsJsonObject().get("payloadData").getAsString();
						
						Float frameSentTimeStamp = jsonTree.getAsJsonObject().get("message")
								.getAsJsonObject().get("params")
								.getAsJsonObject().get("timestamp").getAsFloat();
						
								System.out.println(GlobalVariables.getTestID() + " stomp: " + method + " | requestId: " + strRequestID + "  | timestamp(secs): " + frameSentTimeStamp);
								System.out.println(GlobalVariables.getTestID() + " payload: " + strPayload + "\n");

							}
						}
				
				if (method.compareTo("Network.webSocketClosed") == 0) {
					String strRequestID = jsonTree.getAsJsonObject().get("message")
							.getAsJsonObject().get("params")
							.getAsJsonObject().get("requestId").getAsString();
						
					Float frameTimeStamp = jsonTree.getAsJsonObject().get("message")
							.getAsJsonObject().get("params")
							.getAsJsonObject().get("timestamp").getAsFloat();
						
					System.out.println(GlobalVariables.getTestID() + " stomp: " + method + " | requestId: " + strRequestID + "  | timestamp(secs): " + frameTimeStamp);
					}
				}
			}
				
				

		// mandrews: we need to keep this driver.quit() here to free up the selenium
		// grid node
		// i.e. without it, selenium grid says "all the nodes are busy, so I'll just
		// wait here forever"
		driver.quit();
	}}

