package pages.chargebeeSite.subscriptions.subscriptionDetails.changeSubscriptionFlow;

import java.util.List;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import data.bidSyncPro.registration.AdditionalStateAddOn;
import data.bidSyncPro.registration.SubscriptionAddOns;
import data.bidSyncPro.registration.SubscriptionPlan;
import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.BasePageActions;

public class RecurringAddOnPopup extends BasePageActions{
	
	// xpaths
	// Select AddOn Dropdown
	private final String addAddonText_xpath                   = "//div[@class='cn-modal__wrap']//div[@class='cn-modal__title'][contains(.,'Add Addon')]";
	private final String selectAddOnDropdownBtn_xpath         = "//div[@id='addon-id-row']//a";
	private final String selectAddOnDropdownInput_xpath       = "//div[@id='select2-drop']//div[@class='select2-search']//input";
	private final String selectAddOnDropdownFirstResult_xpath = "//div[@id='select2-drop']//ul[@role='listbox']/li[1]";
	
	// Additional States Quantity -- only appears if "Additional States" is selected in Select AddOn Dropdown
	private final String additionalStatesQuantityInput_xpath  = "//input[@id='addon-quantity']";
	
	// Bottom Buttons
	private final String addBtn_xpath                         = "//input[@type='submit'][@value='Add']";
	
	// Other
	private EventFiringWebDriver driver;
	private int numStateAddOns;            // Number of Additional States to purchase (0, if none)
	private String addOnSelectionText;     // Text for "Select AddOn" dropdown
	
	/***
	 * <h1>RecurringAddOnPopup</h1>
	 * <p>details: Use this constructor to add Recurring AddOns for Additional State addOns
	 * @param driver = EventFiringWebDriver
	 * @param stateAddOns = list of Additional State addOns, between 1-5 states
	 */
	public RecurringAddOnPopup (EventFiringWebDriver driver, List <AdditionalStateAddOn> stateAddOns) {
		super(driver);
		this.driver = driver;
		
		int size = stateAddOns.size();
		Assert.assertTrue("ERROR: Can only purchase between [1,5] additional states", size > 0 & size < 6);

		this.numStateAddOns = size;
		this.setAddOnSelectionTextBasedOnInput(stateAddOns);
	}
	
	/***
	 * <h1>RecurringAddOnPopup</h1>
	 * <p>details: Use this constructor to add Recurring AddOns for Federal | Military | Canada
	 * TODO: Complete constructor for Subscription AddOns
	 * @param driver = EventFiringWebDriver
	 * @param addOn = SubscriptionAddOns is Federal | Military | Canada, the addOn you want to add
	 * @param plan = the current SubscriptionPlan selected 
	 */
	public RecurringAddOnPopup (EventFiringWebDriver driver, SubscriptionAddOns addOn, SubscriptionPlan plan) {
		super(driver);
		this.driver = driver;
		this.numStateAddOns = 0;
		this.setAddOnSelectionTextBasedOnInput(addOn, plan);
	}
	
	// getters

	// Select AddOn Dropdown
	private WebElement getAddOnText()                      { return findByVisibility(By.xpath(this.addAddonText_xpath));                   }
	private WebElement getSelectAddOnDropdownBtn()         { return findByVisibility(By.xpath(this.selectAddOnDropdownBtn_xpath));         }
	private WebElement getSelectAddOnDropdownInput()       { return findByVisibility(By.xpath(this.selectAddOnDropdownInput_xpath));       }
	private WebElement getSelectAddOnDropdownFirstResult() { return findByVisibility(By.xpath(this.selectAddOnDropdownFirstResult_xpath)); }
	
	// Additional States Quantity -- only appears if "Additional States" is selected in Select AddOn Dropdown
	private WebElement getAdditionalStatesQuantityInput()  { return findByVisibility(By.xpath(this.additionalStatesQuantityInput_xpath));  }

	// Bottom Buttons
	private WebElement getAddBtn()                         { return findByVisibility(By.xpath(this.addBtn_xpath));                         }
	
	/* ----- helpers ----- */
	private void setAddOnSelectionTextBasedOnInput(List <AdditionalStateAddOn> stateAddOns) {
		this.addOnSelectionText = "Additional States";
	}

	private void setAddOnSelectionTextBasedOnInput(SubscriptionAddOns addOn, SubscriptionPlan plan) {
		// TODO: return string for dropdown selection
		
		// Text form:
		// Federal Bids - State Plan
		// Military Bids - Regional Plan
		// Canada Bids - Regional Plan
	}
	
	/* ----- methods ----- */
	
	/***
	 * <h1>setAddOn</h1>
	 * <p>purpose: Based on the inputs to the class constructor, set the AddOn.
	 * 	If Additional State addOn, also select the number of states. Click "Add" button.
	 * @return ChangeSubscription
	 */
	public ChangeSubscription setAddOn() {
		return this.updateSelectAddOnDropdown()
				.setAdditionalStatesQuantityIfVisible()
				.clickAddBtn();
	}
	
	/***
	 * <h1>updateSelectAddOnDropdown</h1>
	 * <p>purpose: Update value in the "Select AddOn" dropdown per value inputed into class constructor
	 * @return
	 */
	public RecurringAddOnPopup updateSelectAddOnDropdown() {
		// Make sure the popup loads
		this.verifyPageLoad();
		HelperMethods helper = new HelperMethods();
		helper.addSystemWait(3);
		
		// Set value of Select AddOn dropdown
		this.getSelectAddOnDropdownBtn().click();
		this.getSelectAddOnDropdownInput().sendKeys(this.addOnSelectionText);
		Pattern pattern = Pattern.compile(this.addOnSelectionText);
		
		// Click the first item in the dropdown
		int time = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, time)
			.withMessage("Test Failed: Could not select \"" + this.addOnSelectionText + "\" from dropdown after \"" + time + "\" seconds")
			.until(
					ExpectedConditions.or(
					ExpectedConditions.stalenessOf(this.getSelectAddOnDropdownFirstResult()),
					ExpectedConditions.textMatches(By.xpath(this.selectAddOnDropdownFirstResult_xpath), pattern)
					));

		this.getSelectAddOnDropdownFirstResult().click();
		return this;
	}
	
	/***
	 * <h1>setAdditionalStatesQuantityIfVisible</h1>
	 * <p>purpose: If "Additional States" was selected in the Selected AddOn dropdown, then
	 * 	set the "Quantity" input per the number of states (determined in class constructor).
	 * @return RecurringAddOnPopup
	 */
	public RecurringAddOnPopup setAdditionalStatesQuantityIfVisible() {
		if(this.numStateAddOns != 0) {
			this.getAdditionalStatesQuantityInput().clear();
			this.getAdditionalStatesQuantityInput().sendKeys(String.valueOf(this.numStateAddOns));
		}

		return this;
	}
	
	/***
	 * <h1>clickAddBtn</h1>
	 * <p>purpose: Click on the "Add" button.
	 * @return ChangeSubscription
	 */
	public ChangeSubscription clickAddBtn() {
		this.getAddBtn().click();
		return new ChangeSubscription(driver);
	}
	
	/* ----- verifications ----- */
	/***
	 * <h1>verifyPageLoad</h1>
	 * <p>purpose: verify "Recurring AddOn" popup has loaded
	 * @return RecurringAddOnPopup
	 */
	public RecurringAddOnPopup verifyPageLoad() {
		this.getAddOnText();
		this.getSelectAddOnDropdownBtn();
		this.getAddBtn();
		return this;
	}

}
