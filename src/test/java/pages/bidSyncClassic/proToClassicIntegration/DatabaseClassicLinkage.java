package pages.bidSyncClassic.proToClassicIntegration;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import org.junit.Assert;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import utility.database.SQLConnector;
import utility.database.SelectQueryBuilder;

/***
 * <h1>Class DatabaseClassicLinkage</h1>
 * @author dmidura
 * <p>details: This class contains database calls that should be 
 * 	used to verify that the Pro to Classic linkage is functioning
 * (i.e. that a BidSync Pro account is correctly linked with a Classic BidSync account)
 *
 */
public class DatabaseClassicLinkage {
	
	// Other
	private SelectQueryBuilder dbQuery;
	
	public DatabaseClassicLinkage(EventFiringWebDriver driver) {
		dbQuery = new SelectQueryBuilder();
	}
	
	/***
	 * <h1>getUserLinkIdFromUserName</h1>
	 * <p>purpose: Given the userName, return the user_link_id by querying the Pro database</p>
	 * <p>query: select user_link_id from users_link where user_name = userName</p>
	 * @param userName = user email from registration
	 * @return  UUID for the user link
	 */
	public UUID getUserLinkIdFromUserName(String userName) {
		List <UUID> returns = dbQuery.setDatabase("auth")
				.setTable("users_link")
				.setFilterColumn("user_name")
				.setFilterTerm(userName)
				.setResultColumn("user_link_id")
				.run();
		Assert.assertEquals("ERROR: Wrong number of returns from database", 1, returns.size());
		return returns.get(0);
	}
	
	/***
	 * <h1>getBidSyncClassicLinkEnabledState</h1>
	 * <p>purpose: Get the state of the Classic to Pro linkage. (Must be set to true for Classic
	 * 	to Pro linkage to work).</p>
	 * <p>query: select config_value from config where config_key='bidsync-classic.link.enabled'
	 * @return true if Classic to Pro linkage is enabled | false if not
	 */
	public Boolean getBidSyncClassicLinkEnabledState() {
		List <String> returns = dbQuery.setDatabase("config")
				.setTable("config")
				.setFilterColumn("config_key")
				.setFilterTerm("bidsync-classic.link.enabled")
				.setResultColumn("config_value")
				.run();
		Assert.assertEquals("ERROR: Wrong number of returns from database", 1, returns.size());
		return Boolean.valueOf(returns.get(0));
	}
	
	/***
	 * <h1>deleteUserLinkFromDatabase</h1>
	 * <p>purpose: Delete the user_link_id from the ARGO database</p>
	 * <p>query: delete from users_link where user_link_id = userLinkId</p>
	 * @param userLinkId = user_link_id (can be obtained by running getUserLinkFromUserName method)
	 * @return DatabaseClassicLinkage
	 */
	public DatabaseClassicLinkage deleteUserLinkFromDatabase(UUID userLinkId) {
		String query = String.format("delete from users_link where user_link_id=\'%s\'", userLinkId.toString());
		System.out.println(query);
		try {
			SQLConnector.executeCommand("auth", query);
		} catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            Assert.fail(String.format("Errors when running query '%s'", query));
        }
		
		return this;
	}

}
