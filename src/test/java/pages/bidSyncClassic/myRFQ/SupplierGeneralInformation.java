package pages.bidSyncClassic.myRFQ;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.common.helpers.GeographyHelpers;
import pages.common.pageObjects.BasePageActions;

public class SupplierGeneralInformation extends BasePageActions {
	
	// xpaths

	// Dynamic User info
	private final String companyNameInput_xpath     = "//input[@name='companyname']";
	private final String addressLine1Input_xpath    = "//input[@name='addr1_company']";
	private final String cityInput_xpath            = "//input[@name='city_company']";
	private final String stateSelection_xpath       = "//select[@name='state_company']";
	private final String zipInput_xpath             = "//input[@name='zip_company']";

	private final String firstNameInput_xpath       = "//input[@name='fname']";
	private final String lastNameInput_xpath        = "//input[@name='lname']";
	private final String emailInput_xpath           = "//input[@name='email']";
	private final String contactPhoneInput_xpath    = "//input[@name='phone']";
	private final String contactPhoneExtInput_xpath = "//input[@name='phoneext']";
	
	// User account status
	private final String activeRadioStatus_xpath    = "//input[@name='status'][@value='1']";
	private GeographyHelpers geoHelper;
	
	
	public SupplierGeneralInformation(EventFiringWebDriver driver) {
		super(driver);
		
		// Switch into "main" frame and then into "geninfo"
		new WebDriverWait(driver, DEFAULT_TIMEOUT)
			.withMessage("ERROR: Could not switch to frame to access Supplier \"General Information\"")
			.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("main"));

		new WebDriverWait(driver, DEFAULT_TIMEOUT)
			.withMessage("ERROR: Could not switch to frame to access Supplier \"General Information\"")
			.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("geninfo"));
		
		geoHelper = new GeographyHelpers();
	}
	
	/* ----- getters -----*/
	// Dynamic User info
	private WebElement getCompanyNameInput()     { return findByVisibility(By.xpath(this.companyNameInput_xpath));     }
	private WebElement getAddressLine1Input()    { return findByVisibility(By.xpath(this.addressLine1Input_xpath));    }
	private WebElement getCityInput()            { return findByVisibility(By.xpath(this.cityInput_xpath));            }
	private WebElement getFirstNameInput()       { return findByVisibility(By.xpath(this.firstNameInput_xpath));       }
	private WebElement getLastNameInput()        { return findByVisibility(By.xpath(this.lastNameInput_xpath));        }
	private WebElement getEmailInput()           { return findByVisibility(By.xpath(this.emailInput_xpath));           }
	private WebElement getContactPhoneInput()    { return findByVisibility(By.xpath(this.contactPhoneInput_xpath));    }
	private WebElement getContactPhoneExtInput() { return findByVisibility(By.xpath(this.contactPhoneExtInput_xpath)); }
	private WebElement getStateSelection()       { return findByVisibility(By.xpath(this.stateSelection_xpath));       }
	private WebElement getZipInput()             { return findByVisibility(By.xpath(this.zipInput_xpath));             }
	
	// User account status
	private WebElement getActiveRadioStatus()    { return findByVisibility(By.xpath(this.activeRadioStatus_xpath));    }

	/* ----- methods ----- */

	/***
	 * <h1>verifySupplierInformation</h1>
	 * <p>purpose: Verify that the Supplier info is as userInfo. Specifically:<br>
	 * 	1. First Name<br>
	 * 	2. Last Name<br>
	 * 	3. User Contact Phone + Phone Ext<br>
	 * 	4. User Email<br>
	 * 	5. User Address (line 1 only)<br>
	 * 	6. User City<br>
	 * 	7. User State<br>
	 * 	8. User Zip<br>
	 * 	9. User Company<br>
	 * 	10. Verify User status is active<br>
	 * 	Fail test if any of the above are incorrect</p>
	 * @param user = RandomChargebeeUser for our user
	 * @return SupplierGeneralInformation
	 */
	public SupplierGeneralInformation verifySupplierInformation(RandomChargebeeUser user) {
		
		// Actual Values
		String firstName      = this.getFirstNameInput().getAttribute("value");
		String lastName       = this.getLastNameInput().getAttribute("value");
		String phone          = this.getContactPhoneInput().getAttribute("value").replaceAll("-", "");
		String phoneExt       = this.getContactPhoneExtInput().getAttribute("value");
		String email          = this.getEmailInput().getAttribute("value");
		String address        = this.getAddressLine1Input().getAttribute("value");
		String city           = this.getCityInput().getAttribute("value");
		String state          = this.getStateSelection().getAttribute("value");
		String zip            = this.getZipInput().getAttribute("value");
		String company        = this.getCompanyNameInput().getAttribute("value");
		
		// Expected Values
		String expFirstName   = user.getFirstName();
		String expLastName    = user.getLastName();
		String expEmail       = user.getEmailAddress();
		String expPhone       = user.getPhoneNumber();
		String expPhoneExt    = user.getPhoneExtension();
		String expAddress     = user.getAddress();
		String expCity        = user.getCity();
		String expState       = geoHelper.getStateOrProvinceAbbreviationFromStateName(user.getState());
		String expZip         = user.getZipCode();
		String expCompany     = user.getCompany().getName();


		// Compare Actual Values to Expected Values
		SoftAssertions softly = new SoftAssertions();

		softly.assertThat(firstName)  .withFailMessage("Incorrect First Name for user.    Expected <%s>, Received <%s>", expFirstName, firstName).isEqualTo(expFirstName);
		softly.assertThat(lastName)   .withFailMessage("Incorrect Last Name for user.     Expected <%s>, Received <%s>", expLastName,  lastName ).isEqualTo(expLastName);
		softly.assertThat(email)      .withFailMessage("Incorrect Email Address for user. Expected <%s>, Received <%s>", expEmail,     email    ).isEqualTo(expEmail);
		softly.assertThat(phone)      .withFailMessage("Incorrect Phone Number for user.  Expected <%s>, Received <%s>", expPhone,     phone    ).isEqualTo(expPhone);
		softly.assertThat(phoneExt)   .withFailMessage("Incorrect Phone Ext for user.     Expected <%s>, Received <%s>", expPhoneExt,  phoneExt ).isEqualTo(expPhoneExt);
		softly.assertThat(address)    .withFailMessage("Incorrect Address Line1 for user. Expected <%s>, Received <%s>", expAddress,   address  ).isEqualTo(expAddress);
		softly.assertThat(city)       .withFailMessage("Incorrect City for user address.  Expected <%s>, Received <%s>", expCity,      city     ).isEqualTo(expCity);
		softly.assertThat(state)      .withFailMessage("Incorrect State for user address. Expected <%s>, Received <%s>", expState,     state    ).isEqualTo(expState);
		softly.assertThat(zip)        .withFailMessage("Incorrect Zip for user address.   Expected <%s>, Received <%s>", expZip,       zip      ).isEqualTo(expZip);
		softly.assertThat(company)    .withFailMessage("Incorrect Company Name for user.  Expected <%s>, Received <%s>", expCompany,   company  ).isEqualTo(expCompany);
	
		// User is active
		softly.assertThat(this.getActiveRadioStatus().getAttribute("checked")).withFailMessage("User status is not active!").isNotBlank();

		softly.assertAll();
	
		
		return this;
	}

}
