package stepDefinitions.bidSyncPro.supplier.companySettings.companyProfile;

import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.apache.commons.collections4.CollectionUtils;
import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.bidSyncPro.registration.RandomChargebeeUser;
import data.bidSyncPro.registration.RandomCompanyInformation;
import pages.bidSyncPro.common.BidSyncProCommon;
import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.bidSyncPro.supplier.common.Marketplace;
import pages.bidSyncPro.supplier.companySettings.companyProfile.CompanyProfile;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.helpers.RandomHelpers;
import utility.database.SelectQueryBuilder;

public class CompanyProfile_stepDefinitions {

	public CoreAutomation automation;
	public EventFiringWebDriver driver;
	private HelperMethods helperMethods;
	private CommonTopBar commonTopBar;
	private BidSyncProCommon bidSyncProCommon;
	private CompanyProfile companyProfile;
	private Marketplace marketplace;

	public CompanyProfile_stepDefinitions(CoreAutomation automation) {
		this.automation = automation;
		this.driver     = automation.getDriver();
		helperMethods   = new HelperMethods();
		commonTopBar = new CommonTopBar(driver);
		bidSyncProCommon = new BidSyncProCommon(driver);
		companyProfile = new CompanyProfile(driver);
		marketplace = new Marketplace(driver);
	}
		
	
    /* ------------ Helper Methods ---------------- */


    /* ------------ Test Steps ---------------- */

	@Given("^I click the Edit Company Name icon$")
	public void iClickTheEditCompanyNameIcon() {
		helperMethods.addSystemWait(1000, TimeUnit.MILLISECONDS);
		driver.findElement(By.xpath("//button[@aria-label='update company profile']")).click();
	}

	@Given("^I change the supplier Company Name to random chars$")
	public void iChangeTheSupplierCompanyNameToRandomChars() {
	    helperMethods.addSystemWait(1000, TimeUnit.MILLISECONDS);
	    bidSyncProCommon.waitForSpinner(driver);
		driver.findElement(By.xpath("//input[@id='companyname']")).clear();
        helperMethods.addSystemWait(1000, TimeUnit.MILLISECONDS);
	    driver.findElement(By.xpath("//input[@id='companyname']")).clear();  // sometimes doesn't work so try again
		
		// change the company name
		String strRandomCompanyName = new RandomHelpers().getRandomString(10);
		System.out.printf(GlobalVariables.getTestID() + " INFO: strRandomCompanyName = %s\n", strRandomCompanyName);
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		user.getCompany().setName(strRandomCompanyName);
		driver.findElement(By.xpath("//input[@id='companyname']")).sendKeys(strRandomCompanyName);
	}

	@Given("^I change the supplier Industry to \"([^\"]*)\"$")
	public void iChangeTheSupplierIndustryTo(String arg1) {
		driver.findElement(By.xpath("//input[@id='companytype']")).clear();
		driver.findElement(By.xpath("//input[@id='companytype']")).sendKeys(arg1);
	}
	
	@Given("^I click the Save button on the supplier Company Profile page$")
	public void iClickTheSaveButtonOnTheSupplierCompanyProfilePage() {
		driver.findElement(By.xpath("//button[@id='saveButton']")).click();
		driver.findElement(By.xpath("//button[@id='saveButton']")).sendKeys(Keys.ESCAPE);
		helperMethods.addSystemWait(2000, TimeUnit.MILLISECONDS);
	}

	@Given("^I click the Edit Company Information icon$")
	public void iClickTheEditCompanyInformationIcon() {
		driver.findElement(By.xpath("//button[@aria-label='update company profile']")).click();
	}
	
	@Given("^I change the supplier address to the address below$")
	public void iChangeTheSupplierAddressToTheAddressBelow(Map<String, String> table) {
	    helperMethods.addSystemWait(1000, TimeUnit.MILLISECONDS);
		driver.findElement(By.xpath("//input[@id='addressLine1']")).clear();
        helperMethods.addSystemWait(1000, TimeUnit.MILLISECONDS);
		driver.findElement(By.xpath("//input[@id='addressLine1']")).clear();  // sometimes the clear isn't reliable
		driver.findElement(By.xpath("//input[@id='addressLine1']")).sendKeys(table.get("Address Line1"));
		driver.findElement(By.xpath("//input[@id='city']")).clear();
		driver.findElement(By.xpath("//input[@id='city']")).sendKeys(table.get("City"));
		this.helperMethods.scrollIdIntoView(driver, "zip");
		driver.findElement(By.xpath("//input[@id='zip']")).clear();
		driver.findElement(By.xpath("//input[@id='zip']")).sendKeys(table.get("Zip"));
		
		// open state dropdown
		Actions builder = new Actions(driver);
		String shiftTab = Keys.chord(Keys.SHIFT, Keys.TAB);
        builder.sendKeys(shiftTab)
        		.sendKeys(shiftTab)
        		.sendKeys(Keys.ENTER);
        builder.perform();

        // select state
        driver.findElement(By.xpath("//span[contains(text(),'" + table.get("State") + "')]")).click();
	}
    
    @When("^I fill in the Company Information dialog with random valid data$")
    public void iFillInTheCompanyInformationDialogWithRandomValidData() {
	    RandomCompanyInformation companyInformation = new RandomCompanyInformation();
	    GlobalVariables.storeObject("CompanyInformation", companyInformation);
	    companyProfile
                .fillInCompanyInformation(companyInformation)
                .acceptChanges();
    }
    
    @When("^I fill in the Company Information dialog but cancel the changes$")
    public void iFillInTheCompanyInformationDialogButCancelTheChanges() {
	    helperMethods.addSystemWait(3); // give time for fields to populate
	    RandomCompanyInformation currentCompanyInformation = companyProfile
                .getCompanyInformation();
	    GlobalVariables.storeObject("CurrentCompanyInformation", currentCompanyInformation);
        companyProfile
                .fillInCompanyInformation(new RandomCompanyInformation())
                .cancelChanges();
    }
    
    @Then("^the Company Information will reflect the changes made$")
    public void theCompanyInformationWillReflectTheChangesMade() {
	    RandomCompanyInformation expectedCompany = GlobalVariables.getStoredObject("CompanyInformation");
	    RandomCompanyInformation actualCompany = companyProfile
                .waitForNewInformationToPopulate(expectedCompany)
                .getCompanyInformation();
    
        Pattern ssnPattern = Pattern.compile("^X{3}-X{2}-\\d{4}$");
        String message = String.format("FEIN '%s' doesn't match the pattern 'XXX-XX-1234'.", actualCompany.getFEIN());
        Assert.assertTrue(message, ssnPattern.matcher(actualCompany.getFEIN()).matches());
    
        message = String.format("SSN '%s' doesn't match the pattern 'XXX-XX-1234'.", actualCompany.getSSN());
        Assert.assertTrue(message, ssnPattern.matcher(actualCompany.getSSN()).matches());
    
        validateCompanyInformation(expectedCompany, actualCompany);
    }
    
    @Then("^the Company Information data will not have been changed$")
    public void theCompanyInformationDataWillNotHaveBeenChanged() {
        RandomCompanyInformation expectedCompany = GlobalVariables.getStoredObject("CurrentCompanyInformation");
        RandomCompanyInformation actualCompany = companyProfile
                .getCompanyInformation();
        validateCompanyInformation(expectedCompany, actualCompany);
    }
    
    private void validateCompanyInformation(RandomCompanyInformation expectedCompany, RandomCompanyInformation actualCompany) {
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(actualCompany.getName()).as("Name").isEqualTo(expectedCompany.getName());
        softly.assertThat(actualCompany.getAddressLine1()).as("Address Line 1").isEqualTo(expectedCompany.getAddressLine1());
        softly.assertThat(actualCompany.getAddressLine2()).as("Address Line 2").isEqualTo(expectedCompany.getAddressLine2());
        softly.assertThat(actualCompany.getCity()).as("City").isEqualTo(expectedCompany.getCity());
        softly.assertThat(actualCompany.getFEINLastFourDigits()).as("FEIN").isEqualTo(expectedCompany.getFEINLastFourDigits());
        softly.assertThat(actualCompany.getSSNLastFourDigits()).as("SSN").isEqualTo(expectedCompany.getSSNLastFourDigits());
        softly.assertThat(actualCompany.getTypeOfIndustry()).as("Type Of Industry").isEqualTo(expectedCompany.getTypeOfIndustry());
        softly.assertThat(actualCompany.getAnnualRevenue()).as("Annual Revenue").isEqualTo(expectedCompany.getAnnualRevenue());
        softly.assertThat(actualCompany.getNumberOfEmployees()).as("Number of Employees").isEqualTo(expectedCompany.getNumberOfEmployees());
        softly.assertThat(actualCompany.getYearEstablished()).as("Year Established").isEqualTo(expectedCompany.getYearEstablished());
        softly.assertAll();
    }
    
    @When("^the company owner joins a random company$")
    public void theCompanyOwnerJoinsARandomCompany() {
        RandomChargebeeUser companyOwner = GlobalVariables.getStoredObject("StoredUser");
        GlobalVariables.storeObject("OldCompany", companyOwner.getCompany());
        RandomCompanyInformation newCompany = new RandomCompanyInformation();
        new SupplierLogin(driver)
                .loginWithCredentials(companyOwner.getEmailAddress(), companyOwner.getPassword())
                .openAccountDropDown()
                .clickMyInfo()
                .openUpdateProfileDialog()
                .changeCompany(newCompany);
    }
    
    @And("^the company owner accepts all applicants$")
    public void theCompanyOwnerAcceptsAllApplicants() throws InterruptedException {
        RandomChargebeeUser companyOwner = GlobalVariables.getStoredObject("StoredUser");
        new SupplierLogin(driver)
                .loginWithCredentials(companyOwner.getEmailAddress(), companyOwner.getPassword())
                .openCompanySettingsDropDown()
                .openManageUsers()
                .acceptAllRequests()
                .logout();
    }
    
    @Then("^the second user to join the company is the new company owner$")
    public void theSecondUserToJoinTheCompanyIsTheNewCompanyOwner() {
        helperMethods.addSystemWait(5);
        RandomChargebeeUser originalCompanyOwner = GlobalVariables.getStoredObject("StoredUser");
        RandomChargebeeUser secondUser = GlobalVariables.getStoredObject("CompanyJoiner 1");
        String secondUserId = getUserIdForUsername(secondUser.getEmailAddress());
        String currentCompanyOwnerId = getOwnerForSupplier(secondUser.getCompany().getName());
        System.out.println(String.format("original companyOwnerId: %s", originalCompanyOwner.getId()));
        System.out.println(String.format("secondUserId: %s", secondUserId));
        String message = "Second user is not the company owner after the first user left.";
        Assert.assertEquals(message, secondUserId, currentCompanyOwnerId);
    }
    
    private String getUserIdForUsername(String emailAddress) {
        List<UUID> results = new SelectQueryBuilder()
                .setDatabase("auth")
                .setTable("users")
                .setResultColumn("user_id")
                .setFilterColumn("username")
                .setFilterTerm(emailAddress)
                .run();
        return CollectionUtils.extractSingleton(results).toString();
    }
    
    private String getOwnerForSupplier(String company) {
        List<UUID> results = new SelectQueryBuilder()
                .setDatabase("supplier")
                .setTable("supplier")
                .setResultColumn("supplier_owner")
                .setFilterColumn("name")
                .setFilterTerm(company)
                .run();
        return CollectionUtils.extractSingleton(results).toString();
    }
    
    @When("^the owner of the first company joins the second$")
    public void theOwnerOfTheFirstCompanyJoinsTheSecond() {
	    RandomChargebeeUser companyOwner = GlobalVariables.getStoredObject("StoredUser");
        RandomChargebeeUser userFromAnotherCompany = GlobalVariables.getStoredObject("UserFromAnotherCompany");
        new SupplierLogin(driver)
                .loginWithCredentials(companyOwner.getEmailAddress(), companyOwner.getPassword())
                .openAccountDropDown()
                .clickMyInfo()
                .openUpdateProfileDialog()
                .changeCompany(userFromAnotherCompany.getCompany());
    }
    
    @Then("^the old company is deleted$")
    public void theOldCompanyIsDeleted() {
	    helperMethods.addSystemWait(5); // give the system time to delete the company
        RandomCompanyInformation oldCompany = GlobalVariables.getStoredObject("OldCompany");
        boolean companyStillExists = findWhetherCompanyExists(oldCompany.getName());
        String message = String.format("Company '%s' still exists", oldCompany.getName());
        Assert.assertFalse(message, companyStillExists);
    }
    
    private boolean findWhetherCompanyExists(String companyName) {
        List<UUID> results = new SelectQueryBuilder()
                .setDatabase("supplier")
                .setTable("supplier")
                .setResultColumn("supplier_owner")
                .setFilterColumn("name")
                .setFilterTerm(companyName)
                .run();
        return !results.isEmpty();
    }
    @Then("I verify that the company address is populated in the drodown$")
    public void iVerifyThatTheCompanyAddressIsPopulatedInTheDrodown() {
    	RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
    	companyProfile.verifyCompanyAdressIsPopulated(user);
    }
    
	@When("I go to company settings > company profile$")
	public void iGoToCompanySettingsCompanyProfile() {
		try {
			RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
			commonTopBar.openCompanySettingsDropDown().openCompanyProfile();
		} catch (Exception exp) {
			fail("Test failed : Invalid company name on company profile page");
		}
	}
    
    @Then("I verify that the company name is populated on company profile page$")
    public void iVerifyThatTheCompanyNameIsPopulated() {
    	try {
	    	RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
	    	companyProfile.verifyCompanyNameIsPopulated(user);
    	}catch(Exception exp) {
    		fail("Test failed : Invalid company name on company profile page");
    	}
    }
    
    @Then("I should see a popup to update company profile and update the company profile should be redirected to Marketplace$")
    public void IShouldSeePopupToUpdateCompanyProfileAndUpdateTheCompanyprofileShouldBeRedirectedToMarketplace() {
    	try {
	    	companyProfile.clickOnCompanyProfileLink().addCompanyFEIN();
	    	helperMethods.addSystemWait(2);
	    	new Marketplace(driver).clickOnMarketplaceIcon();
//	    	((JavascriptExecutor)driver).executeScript("arguments[0].click();",new Marketplace(driver).getMarketPlaceIcon());
	    	helperMethods.addSystemWait(3);
	    	new Marketplace(driver).verifyMarketPlaceRedirect();
    	}catch(Exception exp) {
    		fail("Test failed : Failed to redirect to Marketplace" + exp);
    	}
    }
}