package pages.bidSyncPro.supplier.account.accountSettings;

import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.account.AccountCommon;

public class AccountSettings extends AccountCommon {
    
    // xpaths
	private final String strPagePath                = System.getProperty("supplierBaseURL") + "admin/my-account";
    private final String changePasswordButton_xpath = "//button[contains(@aria-label, 'change password')]";
    private final String changeEmailButton_xpath    = "//button[contains(@aria-label, 'change email')]";
    private final String accessibilityToggle_xpath  = "//mat-slide-toggle";
    
    public AccountSettings(EventFiringWebDriver driver) {
        super(driver);
    }
    
    private String getPagePath () { return this.strPagePath; }

    /* ----- getters ----- */
    private WebElement getChangePasswordButton() { return findByScrollIntoViewBottomOfScreen(By.xpath(this.changePasswordButton_xpath)); }
    private WebElement getChangeEmailButton   () { return findByScrollIntoViewBottomOfScreen(By.xpath(this.changeEmailButton_xpath));    }
    private WebElement getAccessiblityToggle  () { return findByVisibility(By.xpath(this.accessibilityToggle_xpath), Integer.parseInt(System.getProperty("defaultTimeOut")));                }

    
    /* ----- methods ----- */
    
    public ChangePasswordModal_Step1 clickChangePasswordButton() {
        getChangePasswordButton().click();
        return new ChangePasswordModal_Step1(driver);
    }
    
    public void clickChangeEmailButton() {
        getChangeEmailButton().click();
        // TODO: return new ChangeEmailButton(driver);
    }
    
    public AccountSettings toggleAccessibilitySetting() {
        getAccessiblityToggle().click();
        return this;
    }
    
    public boolean areAccessibilitySettingsActive() {
        return getAccessiblityToggle().getAttribute("class").contains("mat-checked");
    }
     /***
     * <h1>changePasswordTo</h1>
     * <p>purpose: Click on edit Password button. Enter newPassword into input, click "Confirm", and then close the popup</p>
     * @param newPassword = new password
     * @return
     */
    public AccountSettings changeEmailTo(String newEmail) {
        getChangeEmailButton().click();
        new ChangeEmailModal_Step1(driver)
                .sendKeysEmailTextBox(newEmail)
                .sendKeysConfirmEmailTextBox(newEmail)
                .clickConfirmButton();
        new ChangeEmailModal_Step2(driver)
                .clickCloseButton();
        return this;
    }
    
    /***
     * <h1>changePassword</h1>
     * <p>purpose: Click on edit Password button. Click "Confirm" to send email, and then close the popup</p>
     * @return AccountSettings
     */
    public AccountSettings changePassword() {
        getChangePasswordButton().click();
        new ChangePasswordModal_Step1(driver)
                .clickChangePasswordButton()
                .clickConfirmButton()
                .clickCloseButton();
        return this;
    }
    
    /* ----- verifications ----- */
    
    /***
     * <h1>verifyUserIsOnMyAccountTab</h1>
     * <p>purpose: Verify that current URL is set to display My Accounts tab</p>
     * @return MyAccount
     */
    public AccountSettings verifyUserIsOnMyAccountTab() {
        getChangeEmailButton(); // wait for element to appear
    	if (!driver.getCurrentUrl().equals(this.getPagePath())) {
    	    fail("Test Failed: Current screen is not My Accounts tab");
    	}
    	return this;
    }
    
}
