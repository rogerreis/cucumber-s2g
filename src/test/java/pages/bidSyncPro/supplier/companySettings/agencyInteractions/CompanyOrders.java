/**
 * 
 */
package pages.bidSyncPro.supplier.companySettings.agencyInteractions;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * <h1>Class CompanyOrders</h1>
 * <p>details: This class contains page objects and methods for the Company Settings > Agency Interaction > Company Orders screen
 * NOTE: This screen is an iframe. The class constructor will automatically switch to the necessary frame
 */
public class CompanyOrders extends AgencyInteraction {
		
	// xpaths
	private final String formXPath                       = "//form[@name='theform']";

	//labels
	private final String searchForLabel_xpath            = "//table//td[contains(.,'Search for')]";
	private final String shippingStatusLabel_xpath       = this.formXPath + "//table//td[contains(.,'Shipping Status')]";
	private final String createdBetweenLabel_xpath       = this.formXPath + "//table//td[contains(.,'Created between')]";
	private final String createdBetweenFromLabel_xpath   = this.formXPath + "//table//td/b[text()='From']";
	private final String createdBetweenToLabel_xpath     = this.formXPath + "//table//td/strong[contains(.,'To')]";
	private final String directionsDateFormatText_xpath  = this.formXPath + "//font[@class='small' and contains(.,'format date')]";

	//inputs, radio buttons, select, submit
	private final String searchTermInput_xpath           = this.formXPath + "//input[@name='tofind']";
	private final String orderIdRadionButton_xpath       = this.formXPath + "//input[@name='tofindtype' and @value='10']";
	private final String purchaseDocRadionButton_xpath   = this.formXPath + "//input[@name='tofindtype' and @value='5']";
	private final String productCodeRadionButton_xpath   = this.formXPath + "//input[@name='tofindtype' and @value='9']";
	private final String agencyNameRadionButton_xpath    = this.formXPath + "//input[@name='tofindtype' and @value='8']";
	private final String shippingStatusDropDown_xpath    = this.formXPath + "//select[@name='shiptostatus']";
	private final String fromDateInput_xpath             = this.formXPath + "//input[@name='fromdate']";
	private final String fromDateLookupButton_xpath      = this.formXPath + "//input[@name='fromdate']/..//a[@class='secondaryButton'][1]";
	private final String toDateInput_xpath               = this.formXPath + "//input[@name='todate']";
	private final String toDateLookupButton_xpath        = this.formXPath + "//input[@name='todate']/..//a[@class='secondaryButton'][2]";
	private final String searchButton_xpath              = this.formXPath + "//a[text()='Search']";

	// Search Results: Column Headers
    //only desc sort appears when clicked. doesn't sort
	private final String resultsHdrOrderId_xpath         = this.formXPath + "//th//a[contains(.,'Order ID')]";
	private final String resultsHdrOrderIdDesc_xpath     = this.formXPath + "//th[contains(.,'Order ID')]//img[@src='/images/rfq/arrow_down.gif']";
        
    //only desc sort appears when clicked. doesn't sort
	private final String resultsHdrPurchaseDoc_xpath     = this.formXPath + "//th//a[contains(.,'Purchase Document #')]";
	private final String resultsHdrPurchaseDocDesc_xpath = this.formXPath + "//th[contains(.,'Purchase Document #')]//img[@src='/images/rfq/arrow_down.gif']";
        
    //both sorts appear when clicked, but don't sort
	private final String resultsHdrDate_xpath            = this.formXPath + "//th//a[contains(.,'Date')]";
	private final String resultsHdrDateAsc_xpath         = this.formXPath + "//th[contains(.,'Date')]//img[@src='/images/rfq/arrow_up.gif']";
	private final String resultsHdrDateDesc_xpath        = this.formXPath + "//th[contains(.,'Date')]//img[@src='/images/rfq/arrow_down.gif']";
 
    //only desc sort appears when clicked. doesn't sort
	private final String resultsHdrAgency_xpath          = this.formXPath + "//th//a[contains(.,'Agency')]";
	private final String resultsHdrAgencyDesc_xpath      = this.formXPath + "//th[contains(.,'Agency')]//img[@src='/images/rfq/arrow_down.gif']";
	private final String resultsHdrTotalAmount_xpath     = this.formXPath + "//th[contains(.,'Total Amount')]";
	private final String resultsHdrOrderStatus_xpath     = this.formXPath + "//th[contains(.,'Order Status')]";
	private final String resultsHdrShippingStatus_xpath  = this.formXPath + "//th[contains(.,'Shipping Status')]";
	private final String resultsHdrReceivedStatus_xpath  = this.formXPath + "//th[contains(.,'Received Status')]";

	public CompanyOrders(EventFiringWebDriver driver) {
		super(driver);
		driver.switchTo().parentFrame();
		new WebDriverWait(driver, 5)
			.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("bidsyncFrame")));
	}
	
	// getters

	//labels
	public WebElement getSearchForLabel()           { return this.findByVisibility(By.xpath(this.searchForLabel_xpath));            }
	public WebElement getShippingStatusLabel()      { return this.findByVisibility(By.xpath(this.shippingStatusLabel_xpath));       }
	public WebElement getCreatedBetweenLabel()      { return this.findByVisibility(By.xpath(this.createdBetweenLabel_xpath));       }
	public WebElement getCreatedBetweenFromLabel()  { return this.findByVisibility(By.xpath(this.createdBetweenFromLabel_xpath));   }
	public WebElement getCreatedBetweenToLabel()    { return this.findByVisibility(By.xpath(this.createdBetweenToLabel_xpath));     }
	public WebElement getDirectionsDateFormatText() { return this.findByVisibility(By.xpath(this.directionsDateFormatText_xpath));  }

	//inputs, radio buttons, select, submit
	public WebElement getSearchTermInput()          { return this.findByVisibility(By.xpath(this.searchTermInput_xpath));           }
	public WebElement getOrderIdRadioButton()       { return this.findByVisibility(By.xpath(this.orderIdRadionButton_xpath));       }
	public WebElement getPurchaseDocRadioButton()   { return this.findByVisibility(By.xpath(this.purchaseDocRadionButton_xpath));   }
	public WebElement getProductCodeRadioButton()   { return this.findByVisibility(By.xpath(this.productCodeRadionButton_xpath));   }
	public WebElement getAgencyNameRadioButton()    { return this.findByVisibility(By.xpath(this.agencyNameRadionButton_xpath));    }
	public WebElement getShippingStatusDropDown()   { return this.findByVisibility(By.xpath(this.shippingStatusDropDown_xpath));    }
	public WebElement getFromDateInput()            { return this.findByVisibility(By.xpath(this.fromDateInput_xpath));             }
	public WebElement getFromDateLookupButton()     { return this.findByVisibility(By.xpath(this.fromDateLookupButton_xpath));      }
	public WebElement getToDateInput()              { return this.findByVisibility(By.xpath(this.toDateInput_xpath));               }
	public WebElement getToDateLookupButton()       { return this.findByVisibility(By.xpath(this.toDateLookupButton_xpath));        }
	public WebElement getSearchButton()             { return this.findByVisibility(By.xpath(this.searchButton_xpath));              }

	//column headers
    //only desc sort appears when clicked. doesn't sort
	public WebElement getResultsHdrOrderId()        { return this.findByVisibility(By.xpath(this.resultsHdrOrderId_xpath));         }
	public WebElement getResultsHdrOrderIdDesc()    { return this.findByVisibility(By.xpath(this.resultsHdrOrderIdDesc_xpath));     }
        
    //only desc sort appears when clicked. doesn't sort
	public WebElement getResultsHdrPurchaseDoc()    { return this.findByVisibility(By.xpath(this.resultsHdrPurchaseDoc_xpath));     }
	public WebElement getResultsHdrPurchaseDocDesc(){ return this.findByVisibility(By.xpath(this.resultsHdrPurchaseDocDesc_xpath)); }
        
    //both sorts appear when clicked, but don't sort
	public WebElement getResultsHdrDate()           { return this.findByVisibility(By.xpath(this.resultsHdrDate_xpath));            }
	public WebElement getResultsHdrDateAsc()        { return this.findByVisibility(By.xpath(this.resultsHdrDateAsc_xpath));         }
	public WebElement getResultsHdrDateDesc()       { return this.findByVisibility(By.xpath(this.resultsHdrDateDesc_xpath)) ;       }
 
    //only desc sort appears when clicked. doesn't sort
	public WebElement getResultsHdrAgency()         { return this.findByVisibility(By.xpath(this.resultsHdrAgency_xpath));          }
	public WebElement getResultsHdrAgencyDesc()     { return this.findByVisibility(By.xpath(this.resultsHdrAgencyDesc_xpath));      }
	public WebElement getResultsHdrTotalAmount()    { return this.findByVisibility(By.xpath(this.resultsHdrTotalAmount_xpath));     }
	public WebElement getResultsHdrOrderStatus()    { return this.findByVisibility(By.xpath(this.resultsHdrOrderStatus_xpath));     }
	public WebElement getResultsHdrShippingStatus() { return this.findByVisibility(By.xpath(this.resultsHdrShippingStatus_xpath));  }
	public WebElement getResultsHdrReceivedStatus() { return this.findByVisibility(By.xpath(this.resultsHdrReceivedStatus_xpath));  }
	
	/* ----- methods ----- */
	
	
	/* ----- verifications ----- */
	
	/***
	 * <h1>verifyPageInitialization</h1>
	 * <p>purpose: Verify that the Company Orders tab has initialized per:<br>
	 * 1. All labels display<br>
	 * 2. All radio buttons display<br> 
	 * @return
	 */
	public CompanyOrders verifyPageInitialization() {
		SoftAssertions softly = new SoftAssertions();

		//labels
		softly.assertThat(this.getCreatedBetweenLabel()     .getText().trim()).isEqualTo("Created between");
		softly.assertThat(this.getDirectionsDateFormatText().getText().replaceFirst("^\\[", "").replaceFirst("\\]$", "")).isEqualTo("( Please format date as Apr 23, 2004 or 04/23/2004 )");
		softly.assertThat(this.getSearchForLabel()          .getText()) .isEqualTo("Search for");
		softly.assertThat(this.getShippingStatusLabel()     .getText()) .isEqualTo("Shipping Status");
		softly.assertThat(this.getCreatedBetweenFromLabel() .getText()) .isEqualTo("From");
		softly.assertThat(this.getCreatedBetweenToLabel()   .getText()) .isEqualTo("To");
		
		//inputs, radio buttons, select, submit
		softly.assertThat(this.getSearchTermInput())                    .isNotNull();
		softly.assertThat(this.getOrderIdRadioButton())                 .isNotNull();
		softly.assertThat(this.getPurchaseDocRadioButton())             .isNotNull();
		softly.assertThat(this.getProductCodeRadioButton())             .isNotNull();
		softly.assertThat(this.getAgencyNameRadioButton())              .isNotNull();
		softly.assertThat(this.getShippingStatusDropDown())             .isNotNull();
		softly.assertThat(this.getToDateInput())                        .isNotNull();
		softly.assertThat(this.getToDateLookupButton())                 .isNotNull();
		softly.assertThat(this.getFromDateInput())                      .isNotNull();
		softly.assertThat(this.getFromDateLookupButton())               .isNotNull();
		softly.assertThat(this.getSearchButton().getText())             .isEqualTo("Search");	
		
		softly.assertAll();
		return this;
	}
		
	
	/***
	 * TODO
	 * @return
	 */
	public CompanyOrders verifySearchResultColumnHeaders() {
		SoftAssertions softly = new SoftAssertions();

		//column headers
		softly.assertThat(this.getResultsHdrOrderId().getText())        .isEqualTo("Order ID");
		softly.assertThat(this.getResultsHdrPurchaseDoc().getText())    .isEqualTo("Purchase Document #");
		softly.assertThat(this.getResultsHdrDate().getText())           .isEqualTo("Date");
		softly.assertThat(this.getResultsHdrAgency().getText())         .isEqualTo("Agency");
		softly.assertThat(this.getResultsHdrTotalAmount().getText())    .isEqualTo("Total Amount");
		softly.assertThat(this.getResultsHdrOrderStatus().getText())    .isEqualTo("Order Status");
		softly.assertThat(this.getResultsHdrShippingStatus().getText()) .isEqualTo("Shipping Status");
		softly.assertThat(this.getResultsHdrReceivedStatus().getText()) .isEqualTo("Received Status");

        softly.assertAll();
        return this;
	}

	
}
