@should @BidSync_AccountChangeEmail @administration @level4
Feature: Account > Account Settings - Account Settings Change Email
dmidura: Tagging this @should b/c the test is currently broken

  @ARGO-1623 @ARGO-1922
  Scenario: Account Settings Change Email
    Given I have registered a random user
    And I have navigated to Account > "My Account"
    And I have clicked on the "Change Email" Section on Account > My Account
    And I will use Email "jlaraxyz@mailinator.com" to check if the Email Availability Spinner is Visible
    And I will use Email "jjjj" to check the Valid Email Error
    And I will use Emails "jlaraxyz@mailinator.com" and "jlara@phimail.mailinator.com" to check the Mismatch Email Error
    When I will use a random email to navigate to Change Email Modal Step 2
    And I have clicked the Close Button on Account Settings Change Email Step 2
    And I have received that Account Settings Change Email Emails the new email in Mailinator Inbox
    
    #TODO: Check can't login with old email address
  
  @ignore @ARGO-2430
  Scenario: Change in email address is reflected in both Pro and Classic
    This scenario hits the Bidsync Classic database, and that requires a proprietary
    oracle dependency that isn't in maven central.  See the instructions above the
    oracle dependency in pom.xml if this breaks for you.
    Tag as @must again when the jenkins build has been updated.
    
    Given I have registered a random user
     When I change that users email to a new random email
     Then the email change will be reflected in both the Pro and Classic databases
