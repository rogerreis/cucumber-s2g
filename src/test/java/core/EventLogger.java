package core;
 
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

import pages.common.helpers.GlobalVariables;
 
/***
 * <h1>Class EventLogger</h1>
 * <p>details: This class defines each method for system logging.<br>
 * 	Note: debug property must be set to "true" in config.json in order for this logging to run</p>
 */
public class EventLogger implements WebDriverEventListener{
 
	public void afterChangeValueOf(WebElement arg0, WebDriver arg1) {
		System.out.println(GlobalVariables.getTestID() + " Changed value of " + arg0.toString()); }
 
	public void afterClickOn(WebElement arg0, WebDriver arg1) {
		System.out.println(GlobalVariables.getTestID() + " Clicked on " + arg0.toString()); }
 
	public void afterFindBy(By arg0, WebElement arg1, WebDriver arg2) {
		if (arg1 == null) {
	            System.out.println(GlobalVariables.getTestID() + " Found " + arg0.toString());
	        } else {
	        	System.out.println(GlobalVariables.getTestID() + " Found " + arg1.toString() 
				+ " Using method " + arg0.toString()); }	    
	}
 
	public void afterNavigateBack(WebDriver arg0) {
		System.out.println(GlobalVariables.getTestID() + " Navigated back to " + arg0.getCurrentUrl()); }
 
	public void afterNavigateForward(WebDriver arg0) {
		System.out.println(GlobalVariables.getTestID() + " Navigated forward to " + arg0.getCurrentUrl()); }
 
	public void afterNavigateTo(String arg0, WebDriver arg1) {
		System.out.println(GlobalVariables.getTestID() + " Navigated to: " + arg0 + " on browser: " + arg1.toString()); }
 
	public void afterScript(String arg0, WebDriver arg1) {
		System.out.println(GlobalVariables.getTestID() + " Ran script: " + arg0); }
 
	public void onException(Throwable arg0, WebDriver arg1) {
		System.out.println(GlobalVariables.getTestID() + " Exception occured at " + arg0.getMessage()); }

		@Override
	public void afterChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {
		
	    String[] stringArray = new String[keysToSend.length];
	    for (int index = 0; index < keysToSend.length; index++) {
	        stringArray[index] = keysToSend[index].toString();
	    }
		
		System.out.println(GlobalVariables.getTestID() + " Changed value of " + element.toString() + " to " + String.valueOf(stringArray[0]));
	}	

	public void beforeChangeValueOf(WebElement arg0, WebDriver arg1) {}
	public void beforeClickOn(WebElement arg0, WebDriver arg1) {}
	public void beforeFindBy(By arg0, WebElement arg1, WebDriver arg2) {}
	public void beforeNavigateBack(WebDriver arg0) {}
	public void beforeNavigateForward(WebDriver arg0) {}
	public void beforeNavigateTo(String arg0, WebDriver arg1){}
	public void beforeScript(String arg0, WebDriver arg1) {}
	@Override
	public void beforeAlertAccept(WebDriver driver) {}
	@Override
	public void afterAlertAccept(WebDriver driver) {}
	@Override
	public void afterAlertDismiss(WebDriver driver) {}
	@Override
	public void beforeAlertDismiss(WebDriver driver) {}
	@Override
	public void beforeNavigateRefresh(WebDriver driver) {}
	@Override
	public void afterNavigateRefresh(WebDriver driver) {}
	@Override
	public void beforeChangeValueOf(WebElement element, WebDriver driver, CharSequence[] keysToSend) {}
	@Override
	public <X> void afterGetScreenshotAs(OutputType<X> arg0, X arg1) {}
	@Override
	public void afterSwitchToWindow(String arg0, WebDriver arg1) {}
	@Override
	public <X> void beforeGetScreenshotAs(OutputType<X> arg0) {}
	@Override
	public void beforeSwitchToWindow(String arg0, WebDriver arg1) {}
	@Override
	public void afterGetText(WebElement arg0, WebDriver arg1, String arg2) { }
	@Override
	public void beforeGetText(WebElement arg0, WebDriver arg1) { }
}