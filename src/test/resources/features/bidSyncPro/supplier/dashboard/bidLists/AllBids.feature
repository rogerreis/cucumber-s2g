@must @Bid_List @bidSearch_detail @level3
Feature: Dashboard - Bid Lists - All Bids
  This feature tests the List of bids found on the supplier dashboard.
  ARGO-1235: Your Saved Bids
  ARGO-1499: No logic to deal with no results for "LOAD MORE BIDS"
  ARGO-1813: Sorting the bid table before loading more bids causes the load to hang
  ARGO-2124: Dates and expiration displaying incorrectly for bids
  ARGO-2125: All Bids tab showing expired bids when you sort in Ascending order
  ARGO-2281: Huge slowdown on "Load More" bids
  BIDSYNC-152: Make column titles static when user scrolls bids list

  
@ARGO-1235
Scenario Outline: "All Bids" bid list displays correct table headers
	Given I have logged into the Supplier side as user "National FMC"
	When I have navigated to the "<tab>" bid tab
	Then I verify that the List columns are displayed
    
Examples:
	| tab             |
	| All Bids        |
	

Scenario Outline: Load more bids
	Given I have logged into the Supplier side as user "National FMC"
	When I have navigated to the "<tab>" bid tab
	Then I will verify that the List can load more bids
  
	Examples:
	| tab             |
	| All Bids        |
	
	
@ARGO-2125
Scenario: All Bids tab should not show expired bids when sorting by End Date in ascending order
Note: Currently "Upgrade Plan" bids are excluded from this test
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to the "All Bids" bid tab
	And I sort by "End Date"
 	Then I verify whether the table displays expired bid
 	
 
@BIDSYNC-152 
Scenario Outline: Column titles should be static when the user scrolls the "All Bids" bid list or loads more
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to the "<tab>" bid tab
	When I view the last bid currently displaying in the bid list in "<tab>" bid tab
	Then I verify that the List columns are displayed
	When I click on load more, if it is available
	And I view the last bid currently displaying in the bid list in "<tab>" bid tab
	Then I verify that the List columns are displayed

Examples:
	| tab             |
	| All Bids        |


@ignore @ARGO-1813
Scenario Outline: Load more bids obeys previous sort
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to the "<tab>" bid tab
	And I sort by "<column>"
	When I load more bids
	Then the List is sorted in ascending order by "<column>"
  
	Examples:
	| tab         | column    |
	| All Bids    | Bid Title |
	| All Bids    | Bid ID    |
	| All Bids    | Agency    |
	| All Bids    | State     |
	| All Bids    | End Date  |
	| All Bids    | Added     |

  
@ignore @ARGO-2124
Scenario Outline: Dates in the Bid list are correct
Note: Currently "Upgrade Plan" bids are excluded from this test
dmidura: Test is currently broken. Marking @should until can fix
	Given I have logged into the Supplier side as user "National FMC"
	When I have navigated to the "<tab>" bid tab
	Then I will verify that the bids in the list have the correct dates

	Examples:
	| tab             |
	| All Bids        |
  
    
#dmidura: timeouts on spinner do not seem to be properly functioning in jenkins. Ignoring test until can figure out why.
@ARGO-1499 @ARGO-2281 @BIDS-485
Scenario: "Load More Bids" button only appears when there are more bids to load on All Bids. Pagination is not slow.
 	Given I have logged into the Supplier side as user "National FMC"
 	And I run a search on the Top Bar with keyword "state"
 	When I am viewing all the bids on the "All Bids" tab
	Then I will not see a load more bids button

@BIDS-472
Scenario: Sorting should work properly and included all search criteria each time you sort
	Given I have logged into the Supplier side as user "Basic"
 	And I run a search on the Top Bar with keyword "test"
 	And I retrieve the Bid Count Banner value for a generic search with "Basic" bid access
 	And I have navigated to the "All Bids" bid tab
 	And I sort by "State"
 	Then verify the same number of bids are available in the bid list for user "Basic"
	Then I sort by "Agency"
 	Then verify the same number of bids are available in the bid list for user "Basic"
	
  