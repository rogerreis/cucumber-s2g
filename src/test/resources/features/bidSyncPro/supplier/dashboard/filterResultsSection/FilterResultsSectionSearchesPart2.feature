@must @filterResults @search @bidSearch_detail @level4
Feature: Dashboard - Filter Results Section - Search Tests, Part 2
  This feature file exercises the objects related to the search ability of the Filter Results collapsible section on the Supplier Dashboard
  	ARGO-1539: Load More & Date Range Issues
  	BIDSYNC-50: Ability to search for bids by bid numbers

@ARGO-1539
Scenario: Custom Time Frame: After user searches for bids using a Custom Time Frame
	then bids displayed after clicking on "Load More" should adhere to the time frame

  Given I have logged into the Supplier side as user "Basic"
#  And I open the Filter Results Section
  And I run a search on the Filter Results section with a Custom Timeframe
  When I click on the Load More button in the All Bids list
  Then I will verify that bids within All Bids tab are displayed within the Custom Time Frame

      
@BIDSYNC-50
Scenario: Filter Results keyword search will return results for a search by bid number that contains spaces in the bid number
	Given I have logged into the Supplier side as user "National FMC"
	And I randomly select a bid with spaces in the bid number from the "All Bids" bid list
	When I run a search on the Filter Results section using the bid number as the keyword
	Then I will verify that the correct bid is returned per the search for the bid number

	
@BIDSYNC-50
Scenario: Filter Results keyword search will return results for a search by bid number that contains dashes in the bid number
	Given I have logged into the Supplier side as user "National FMC"
	And I randomly select a bid with dashes in the bid number from the "All Bids" bid list
	When I run a search on the Filter Results section using the bid number as the keyword
	Then I will verify that the correct bid is returned per the search for the bid number