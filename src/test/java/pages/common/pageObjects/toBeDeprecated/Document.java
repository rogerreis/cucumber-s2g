package pages.common.pageObjects.toBeDeprecated;

import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

/**
 * Inherit from this class to create a page
 * @author jlara
 *
 */
@Deprecated
public class Document {

	private String myPagePath = "";
	protected EventFiringWebDriver driver;
	public Map <String, DocElement> myPageElements;
	
	private static HelperMethods helper = new HelperMethods();

	/***
	 * Document constructor
	 * @param driver
	 */
	public Document (EventFiringWebDriver driver) {
		myPageElements = new HashMap<>();
		DocElement.setDriver(this.driver = driver);
	} // end of constructor
	
	/***
	 * Document constructor
	 * @param driver
	 * @param pagePath
	 */
	public Document (EventFiringWebDriver driver, String pagePath) {
		this(driver);
		//myPageElements = new HashMap <String, DocElement>();
		//DocElement.setDriver(this.driver = driver);
		myPagePath = pagePath;
	} // end of constructor
	
	/***
	 * 
	 * @param pagePath
	 */
	public void setPagePath(String pagePath) {
		myPagePath = pagePath;
		System.out.printf(GlobalVariables.getTestID() + " INFO: Set page path = %s%n",myPagePath);
	} // end of setPagePath()
	
	/***
	 * 
	 * @return String
	 */
	public String getPagePath() {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Getting page path = %s%n",myPagePath);
		return myPagePath;
	} // end of getPagePath()

	/***
	 * 
	 * @param name
	 * @param byType
	 * @param locationExpression
	 * @throws Exception
	 */
	public void addElement(String name, ByTypes byType, String locationExpression) throws Exception {
		myPageElements.put(name, new DocElement(name, byType, locationExpression));
	} // end of addElement()
	
	/***
	 * Registers a page element.
	 * @param name
	 */
	public DocElement registerPageElement(String name) {
		//helper.addSystemWait(1);
		myPageElements.get(name).findElement();
		return myPageElements.get(name);
	} // end of registerPageElement()
	
	/***
	 * Registers page element
	 */
//	public void registerPageElements() throws InterruptedException {
//		System.out.println(GlobalVariables.getTestID() + " INFO: Registering PageElements for " + this.getClass());
//		myPageElements.forEach((k,v)->registerPageElement(k));
//	} // end of registerPageElements()

	/**
	 * Navigate to the document's url
	 * @throws InterruptedException *
	 * 
	 */
	public void navigate() {
		navigate(myPagePath);
	} // end of navigate()
	
	/***
	 * Navigate to any url
	 * @param url
	 */
	public void navigate(String url) {
		driver.navigate().to(url);
		helper.addSystemWait(2);
	} // end of navigate()
	
	/***
	 * Send Keys (text) to an element
	 * @param elementName
	 * @param keysToSend
	 */
	public void sendKeys(String elementName, CharSequence... keysToSend) {
		myPageElements.get(elementName).getElement().sendKeys(keysToSend);
	} // end of sendKeys()
	

	/***
	 * Select a visible option by value
	 * @param elementName
	 * @param optionValue
	 */
	public void selectOptionByValue(String elementName, String optionValue) {
		Select select = new Select(registerPageElement(elementName).getElement());
		select.selectByValue(optionValue);
	} // end of selectOptionByValue()
	

	/***
	 * Select a visible option by text
	 * @param elementName
	 * @param optionText
	 */
	public void selectOptionByText(String elementName, String optionText) {
		Select select = new Select(registerPageElement(elementName).getElement());
		select.selectByVisibleText(optionText);
	} // end of selectOptionByText()
	
	/***
	 * Click on an element
	 * @param elementName
	 */
	public void click(String elementName) {
		DocElement element = registerPageElement(elementName);
		System.out.printf(GlobalVariables.getTestID() + " INFO: Clicking '%s' at %s%n", element.getName(), element.getLocationExpression() );
		element.getElement().click();
		//helper.addSystemWait(2);
	} // end of click()
	
	/***
	 * Return text of an element
	 * @param elementName
	 * @return
	 */
	public String getText(String elementName) {
		DocElement element = registerPageElement(elementName);
		System.out.printf(GlobalVariables.getTestID() + " INFO: GetText '%s' in %s%n", element.getText(), elementName );
		return element.getText();
	} // end of getText()
	
	
	/***
	 * Verify that we navigated to the correct page
	 * @return
	 */
	public boolean verifyNavigation() {
		
		if (!driver.getCurrentUrl().equals(getPagePath()))
			fail(String.format("FAILED: Did not navigate to '%s'",driver.getCurrentUrl())); 
		System.out.printf(GlobalVariables.getTestID() + " PASSED: Navigated to '%s'%n", driver.getCurrentUrl() );
		return true;
	} // end of verifyNavigation()
	

	/***
	 * Verify that inner text matches expected value
	 * @param elementName
	 * @param expectedValue
	 * @return 
	 */
	public boolean verifyInnerText(String elementName, String expectedValue) {
		DocElement element = registerPageElement(elementName);
		if(!element.getText().contains(expectedValue))
			fail(String.format("FAILED: [%s] not found in %s",expectedValue, elementName));
		
		System.out.printf(GlobalVariables.getTestID() + " PASSED: [%s] found in %s (Raw Text: %s)%n",expectedValue,elementName,element.getText());
		return true;
	} // end of verifyInnerText()
	

	/***
	 * Verify that inner text matches expected value
	 * @param elementName
	 * @return boolean
	 */
	public boolean verifyVisibility(String elementName) {
		DocElement element = registerPageElement(elementName);
		if (element.isVisible()) {
			System.out.printf(GlobalVariables.getTestID() + " PASSED: [%s] is visible%n",elementName);
			return true;
		} // end of if
		else {
			fail(String.format("FAILED: [%s] is not visible%n",elementName));
			return false;
		} // end of else
	} // end of verifyVisibility()
} // end of class Document