package pages.buyspeed.common;

import static org.junit.Assert.fail;


/***
 * <h1>enum TopBarSearchType</h1>
 * @author dmidura
 * <p>details: Contains all the options for the top bar search dropdown
 */
public enum TopBarSearchType {
	CONTRACTS,
	BID_SOLICITATIONS,
	CONTRACTS_BLANKETS,
	CREDIT_MEMOS,
	INVOICES,
	PURCHASE_ORDERS,
	REQUISITIONS,
	QUOTES,
	VENDORS;
	
	@Override
	public String toString() {
		// Return the type that is given by the Top Bar dropdown
		switch (this) {
		case CONTRACTS:
			return "Contracts";
		case BID_SOLICITATIONS:
			return "Bid Solicitations";
		case CONTRACTS_BLANKETS:
			return "Contract/Blankets";
		case CREDIT_MEMOS:
			return "Credit Memos";
		case INVOICES:
			return "Invoices";
		case PURCHASE_ORDERS:
			return "Purchase Orders";
		case REQUISITIONS:
			return "Requisitions";
		case QUOTES:
			return "Quotes";
		case VENDORS:
			return "Vendors";
		default:
			fail("ERROR: Selected enum value is not in TopBarSearchTypes");
		}
		return null;
	}
}
