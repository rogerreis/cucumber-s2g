package pages.bidSyncPro.supplier.login;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.Assert;

import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.bidSyncPro.supplier.account.accountSettings.ResetPasswordModal;
import pages.common.helpers.Constants;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.helpers.RandomHelpers;
import pages.common.pageObjects.BasePageActions;
import pages.mailinator.Mailinator;

/***
 * <h1>Class ForgotYourPassword</h1>
 * @author dmidura
 * <p>details: This class holds all the objects and methods related to the "Forgot Your Password" dialogs off the supplier login</p>
 *
 */
public class ForgotYourPassword extends BasePageActions {
	
	private EventFiringWebDriver driver;
	private Mailinator mailinator;
	
	public ForgotYourPassword (EventFiringWebDriver driver) {
		super(driver);
		this.driver 		= driver;
		mailinator = new Mailinator(driver);
	}

	private final String forgotPasswordInput_id = "forgottenUser";
	private final String forgotPasswordHeader_xpath = "//*[@id='matForgotPasswordCardContent']/h2[contains(text(),'Forgot Your Password')]";
	public final String informationOnUsername_xpath	= "//*[@id='matForgotPasswordCardContent']/ul/li[contains(text(),'Usernames are in the form of an email address')]";
	public final String informationOnPassword_xpath = "//*[@id='matForgotPasswordCardContent']/ul/li[contains(text(),'Passwords are case sensitive')]";
	public final String resetPasswordInstruction_xpath = "//*[@id='matForgotPasswordCardContent']/p[contains(text(),'To reset your password, enter your Periscope S2G email.')]";
	
	// Bottom Buttons
	private final String cancelBtn_xpath           = "//button[@id='cancelButton'][contains(span, 'Cancel')]";
	private final String continueBtn_xpath         = "//button[@id='emailSentButton'][contains(span, 'Continue')]";
	private final String tryForFreeBtn_id 			= "tryForFreeButton";
	// Page Transition Text (appears after clicking "Continue")
	private final String resetLinkSentTxt_xpath   = "//div[@id='emailTextSent']//strong[contains(text(), 'Your password reset link was sent to your email.')]";
	private final String footerNotSignedUpText_xpath = "//*[@id=\"divLogin\"]//div/span[contains(text(), 'Not signed up')]";
	
	private final String footerHelpText_xpath = "//*[@id='divLogin']//div[contains(text(),'Need Help')]";
	private final String passwordHistoryErrorMessage_xpath = "//*[@id='errorRecoverMessage']";
	
	/* ----- getters ----- */
	
	
	private WebElement getForgotPasswordInput() {
		return findByVisibility(By.id(this.forgotPasswordInput_id));
	}

	private WebElement getForgotPasswordHeader() {
		return findByVisibility(By.xpath(forgotPasswordHeader_xpath));
	}

	private WebElement getInformationOnUsername() {
		return findByVisibility(By.xpath(informationOnUsername_xpath));
	}

	private WebElement getInformationOnPassword() {
		return findByVisibility(By.xpath(informationOnPassword_xpath));
	}

	private WebElement getResetPasswordInstruction() {
		return findByVisibility(By.xpath(resetPasswordInstruction_xpath));
	}

	// Bottom Buttons
	private WebElement getCancelBtn() {
		return findByVisibility(By.xpath(this.cancelBtn_xpath));
	}

	private WebElement getContinueBtn() {
		return findByVisibility(By.xpath(this.continueBtn_xpath));
	}

	private WebElement getTryForFreeBtn() {
		return findByVisibility(By.id(this.tryForFreeBtn_id));
	}

	// Page Transition Text (appears after clicking "Continue")
	private WebElement getTransitionText() {
		return findByVisibility(By.xpath(this.resetLinkSentTxt_xpath));
	}

	private WebElement getFooterNotSignedUpText() {
		return findByVisibility(By.xpath(footerNotSignedUpText_xpath));
	}

	private WebElement getFooterHelpText() {
		return findByVisibility(By.xpath(footerHelpText_xpath));
	}
	

    /* ----- methods ----- */
    
    /***
     * <h1>clickCancelButton</h1>
     * <p>purpose: On the "Forgot Your Password" screen, click the "Cancel" button<br>
     * 	to navigate back to the Supplier login screen and cancel the password reset</p>
     * @return SupplierLogin
     */
    public SupplierLogin clickCancelBtn() {
    	this.getCancelBtn().click();
    	return new SupplierLogin(driver);
    }

    /***
     * <h1>clickContinueButton</h1>
     * <p>purpose: On the "Forgot Your Password" screen, click the "Continue" button</p>
     * @return ForgotYourPassword
     */
    public ForgotYourPassword clickContinueBtn() {
    	this.getContinueBtn().click();
    	return this;
    }
    
    /***
     * <h1>setForgotPasswordInput</h1>
     * <p>purpose: Set the value of the "Forgot Your Password" input to the userEmail</p>
     * @param userEmail = email you're going to reset the password with
     * @return ForgotYourPassword
     */
    public ForgotYourPassword setForgotPasswordInput(String userEmail) {
    	this.getForgotPasswordInput().sendKeys(userEmail);
    	return this;
    }
    /**
     * <h1>Method to enter email id to the mail id field and press enter key.
     * 
     * <p>setForgotPasswordInputForRegisteredUser()
     * @return ForgotYourPassword
     */
    public ForgotYourPassword setForgotPasswordInputForRegisteredUser() {
    	RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
    	setForgotPasswordInputAndPressEnterKey(user.getEmailAddress());
    	return this;
    }
    
    /**
     * <h1>Method to enter email id to the mail id field and press enter key.</h1>
     * 
     * <p>setForgotPasswordInputAndPressEnterKey(String mailId)
     * 
     * @param mailId
     * @return ForgotYourPassword
     */
    public ForgotYourPassword setForgotPasswordInputAndPressEnterKey(String mailId) {
    	setForgotPasswordInput(mailId);
    	this.getForgotPasswordInput().sendKeys(Keys.ENTER);
    	return this;
    }
    
    /**
     * <h1>Method to set the password and press enter key</h1>
     * 
     * <p> setNewPasswordsAndTapEnterKey()
     */
    public void setNewPasswordsAndTapEnterKey() {
    	String password = new RandomHelpers().getRandomPasswordForBidSync();
    	GlobalVariables.storeObject("newPassword", password);
    	new ResetPasswordModal(driver)
        .setPasswordInput(password)
        .setConfirmPasswordInput(password)
        .getConfirmPasswordInput().sendKeys(Keys.ENTER);
    	
    }
    /**
     * <h1>Method to retrieve email from mailinator.</h1>
     * 
     * <p>retrieveEmailFromMailinator(String userEmail)
     * @param userEmail
     */
    public void retrieveEmailFromMailinator(String userEmail) {
		mailinator.navigateToMailinatorInbox(userEmail)
			.openPasswordResetEmail()
			.clickOnTokenURLInEmail()
//		mailinator.deleteOpenEmail()
			.switchToWindow("S2G");
	}
    
    /***
	 * <h1>forgotPassword</h1>
	 * <p>purpose: From Supplier Login screen, click "Forgot Your Password".<br>
	 * 	Enter password into "Forgot Your Password" input and then click "Continue"<br>
	 * 	Verify that "Your password reset link was sent to your email." then displays</p>
	 * @param userEmail = email you're using for the reset
	 */
	public void forgotPassword(String userEmail) {
		new SupplierLogin(driver)
			.clickForgotYourPassword()
			.setForgotPasswordInput(userEmail)
			.clickContinueBtn()
			.verifyPageTransition();
	}

	/***
	 * <h1>retrieveEmail</h1>
	 * <p>purpose: Retrieve the "Password Reset" email. Verify email content. Click on URL and navigate</p>
	 * @param userEmail = email you're using for the reset
	 */
	public void retrieveEmail(String userEmail) {
		mailinator.navigateToMailinatorInbox(userEmail)
			.openPasswordResetEmail()
			.verifyBodyTextIsCorrect()
			.verifyFooterTextIsCorrect()
			.clickOnTokenURLInEmail()
			.switchToWindow("S2G");
	}

	/**
     * <h1>Method to enter email id in the text box provided
     * <p>enterEmailId(String email)<p>
     * 
     * @param email
     */
    public void enterEmailId(String email) {
    	this.getForgotPasswordInput().sendKeys(email);
    }
    
    /**
     * Method to click on Continue button
     */
    public void clickOnContinue() {
    	this.getContinueBtn().click();
    }
    
    
    /**
     * Method to click on Cancel button
     */
    public void clickOnCancel(){
    	this.getCancelBtn().click();
    }
    
    
    public void enterPasswordInPasswordHistory() {
    	String password = GlobalVariables.getStoredObject("newPassword");
    	System.out.println(GlobalVariables.getTestID() + " INFO: Entering new password = \"" + password + "\"");
    	new ResetPasswordModal(driver)
        .setPasswordInput(password)
        .setConfirmPasswordInput(password)
        .getConfirmPasswordInput().sendKeys(Keys.ENTER);
    }
    
    /***
     * <h1>IsEmailInputFieldEnabled</h1>
     * <p>purpose: Determine if the email input field is enabled | disabled.<br>
     * 	Expected behavior is that field should be enabled until user enters email<br>
     * 	and clicks on the "Continue" button. After "Continue", then the field<br>
     * 	should displays as disabled</p>
     * @return true if input is enabled | or false if input is disabled
     */
    private Boolean IsEmailInputFieldEnabled() { return this.getForgotPasswordInput().isEnabled(); }
    
    
    /* ----- verifications ----- */

    /***
     * <h1>verifyPageInitialization</h1>
     * <p>purpose: Verify that the "Forgot Your Password" screen initializes per:<br>
     * 	1. Expected Header<br>
     * 	2. Expected Text<br>
     * 	3. Input<br>
     * 	4. "Cancel" and "Continue" button</p>
     *  5. footer
     * @return ForgotYourPassword
     */
    public void verifyPageInitialization() {
    	this.getForgotPasswordHeader();
    	this.getInformationOnUsername();
    	this.getInformationOnPassword();
    	this.getResetPasswordInstruction();
    	this.getForgotPasswordInput();
    	this.getCancelBtn();
    	this.getContinueBtn();
    	this.getFooterNotSignedUpText();
    	this.getTryForFreeBtn();
    	this.getFooterHelpText();
    }

    /***
     * <h1>verifyPageTransition</h1>
     * <p>purpose: After entering email into "Forgot Your Password" input and clicking "Continue",<br>
     * 	run this method to verify that the "Forgot Your Password" screen has transitioned to correctly<br>
     * 	display the "Your password reset link was sent to your email." text. Fail test if page did not transition</p>
     * @return ForgotYourPassword
     */
    public ForgotYourPassword verifyPageTransition() {
    	this.getTransitionText();
    	return this;
    }
    
    /**
     * <h1> Method to verify bid list url>/h1>
     * <p> verifybidListPage
     * 
     */
    public void verifybidListPage() {
    	int time = DEFAULT_TIMEOUT * 3 ;
	    new WebDriverWait(driver, time)
		.withMessage("Test Failed: Page did not navigate to \"" + Constants.bidListUrl + "\" within \"" + time + "\" seconds")
		.until(ExpectedConditions.urlToBe(Constants.bidListUrl));
    }
    
   
    public void verifyPasswordHistoryErrorMessage() {
    	int time = DEFAULT_TIMEOUT;
    	new WebDriverWait(driver, time)
		.withMessage("Test failed : Could not locate error message after \"" + time + "\" seconds")
		.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(this.passwordHistoryErrorMessage_xpath), Constants.passwordHistoryErrorMessage));
    }
    
    /***
     * <h1>verifyEmailInputDisabled</h1>
     * <p>purpose: Verify that the email input field is disabled.<br>
     * 	(This is expected behavior after user has entered an email <br>
     * 	into the input field and clicked on "Continue").<br>
     * 	Fail test if email input field is not disabled to user input</p>
     * @return ForgotYourPassword
     */
    public ForgotYourPassword verifyEmailInputIsDisabled() {
    	Assert.isTrue(this.IsEmailInputFieldEnabled(), "Test Failed: Email input field is not disabled to user input");
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified that email input field is disabled");
    	return this;
    }
    
}
    
