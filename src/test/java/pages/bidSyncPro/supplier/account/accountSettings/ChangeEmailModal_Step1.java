package pages.bidSyncPro.supplier.account.accountSettings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.HelperMethods;

/**
 * @author jlara
 * <p>TODO: Refactor this class</p>
 */
public class ChangeEmailModal_Step1 {
    
    private EventFiringWebDriver driver;
    private HelperMethods helperMethods;
    private String pagePath = "";

	private final String xpathModal = "//bidsync-change-email/div//mat-card[@id='matChangeEmailComponentCard']";
	
	//text: Change Your Email
	private final String xpathHeaderLabel = xpathModal + "//mat-card-title[@id='matChangeEmailTitle']";
	//text: Your email is essential for your account identification, On the next sign in, you will have to use the email you select now.
	private final String xpathDescriptionLabel = xpathModal + "//mat-card-content/p";
	
	
	//contains text: New Email
	private final String xpathEmailLabel = xpathModal + "//mat-form-field[@id='matNewEmailField']//span/label[@for='newEmail']";
	
	//email text box
	private final String xpathEmailTextBox = xpathModal + "//mat-form-field[@id='matNewEmailField']//input[@name='newEmail']";
	
	//check email availability 
	private final String xpathCheckEmailAvailabilitySpinner = xpathModal + "//mat-form-field[@id='matNewEmailField']//div/spinner";
	private final String xpathCheckEmailAvailabilityLabel = xpathModal + "//mat-form-field[@id='matNewEmailField']//div/span[text()='...checking email availability']";
			
	//text: Please enter a valid email
	private final String xpathEnterValidEmailErrorLabel = xpathModal + "//mat-error[@id='matNewEmailError']/span";
	
	
	//contains text: Confirm Email
	private final String xpathConfirmEmailLabel = xpathModal + "//mat-form-field[@id='matConfirmEmailField']//span/label[@for='confirmEmail']";
	
	//confirm email text box
	private final String xpathConfirmEmailTextBox = xpathModal + "//mat-form-field[@id='matConfirmEmailField']//input[@name='confirmEmail']";
	
	//contains text: Emails do not match
	private final String xpathEmailsDoNotMatchErrorLabel = xpathModal + "//mat-error[@id='matConfirmEmailError']/span";
	
	
	//conatains text: CANCEL
	private final String idCancelButton = "btnCancel";
	//conatains text: CONFIRM
	private final String idConfirmButton = "btnConfirm";
	
	
	public ChangeEmailModal_Step1(EventFiringWebDriver driver) {
		this.driver = driver;
        this.helperMethods = new HelperMethods();
		this.pagePath = System.getProperty("supplierBaseURL") + "admin/agency-interaction";
	}

	public ChangeEmailModal_Step1 sendKeysEmailTextBox(CharSequence... keys) {
		this.getEmailTextBox().sendKeys(keys);
		return this;
	}
	
	public ChangeEmailModal_Step1 sendKeysConfirmEmailTextBox(CharSequence... keys) {
		this.getConfirmEmailTextBox().sendKeys(keys);
		return this;
	}

	// TODO: make this private
	public AccountSettings clickCancelButton() {
		getCancelButton().click();
		return new AccountSettings(driver);
	}

	public void clickConfirmButton() {	
		getConfirmButton().click();
		//return new ChangePasswordModal_Step3(driver);	
	}

	private WebElement getHeaderLabel() {
	    helperMethods.waitForVisibilityOfXpath(driver, xpathHeaderLabel, 10);
	    return driver.findElement(By.xpath(xpathHeaderLabel));
	}
	
	private WebElement getDescriptionLabel() {
	    helperMethods.waitForVisibilityOfXpath(driver, xpathDescriptionLabel, 10);
	    return driver.findElement(By.xpath(xpathDescriptionLabel));
	}
	
	private WebElement getEmailLabel() {
	    helperMethods.waitForVisibilityOfXpath(driver, xpathEmailLabel, 10);
	    return driver.findElement(By.xpath(xpathEmailLabel));
	}
    
    // TODO: make this private
	public WebElement getEmailTextBox() {
	    helperMethods.waitForVisibilityOfXpath(driver, xpathEmailTextBox, 10);
	    return driver.findElement(By.xpath(xpathEmailTextBox));
	}
    
    // TODO: make this private
	public WebElement getCheckEmailAvailabilitySpinner() {
	    helperMethods.waitForVisibilityOfXpath(driver, xpathCheckEmailAvailabilitySpinner, 10);
	    return driver.findElement(By.xpath(xpathCheckEmailAvailabilitySpinner));
	}
    
    // TODO: make this private
	public WebElement getCheckEmailAvailabilityLabel() {
	    helperMethods.waitForVisibilityOfXpath(driver, xpathCheckEmailAvailabilityLabel, 10);
	    return driver.findElement(By.xpath(xpathCheckEmailAvailabilityLabel));
	}
    
    // TODO: make this private
	public WebElement getEnterValidEmailErrorLabel() {
	    helperMethods.waitForVisibilityOfXpath(driver, xpathEnterValidEmailErrorLabel, 10);
	    return driver.findElement(By.xpath(xpathEnterValidEmailErrorLabel));
	}
	
	private WebElement getConfirmEmailLabel() {
	    helperMethods.waitForVisibilityOfXpath(driver, xpathConfirmEmailLabel, 10);
	    return driver.findElement(By.xpath(xpathConfirmEmailLabel));
	}
    
    // TODO: make this private
	public WebElement getConfirmEmailTextBox() {
	    helperMethods.waitForVisibilityOfXpath(driver, xpathConfirmEmailTextBox, 10);
	    return driver.findElement(By.xpath(xpathConfirmEmailTextBox));
	}
    
    // TODO: make this private
	public WebElement getEmailsDoNotMatchErrorLabel() {
	    helperMethods.waitForVisibilityOfXpath(driver, xpathEmailsDoNotMatchErrorLabel, 10);
	    return driver.findElement(By.xpath(xpathEmailsDoNotMatchErrorLabel));
	}
	
	private WebElement getCancelButton() {
	    helperMethods.waitForVisibilityOfId(driver, idCancelButton, 10);
	    return driver.findElement(By.id(idCancelButton));
	}
	
	private WebElement getConfirmButton() {
	    helperMethods.waitForVisibilityOfId(driver, idConfirmButton, 10);
	    return driver.findElement(By.id(idConfirmButton));
	}

}
