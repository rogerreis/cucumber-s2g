package pages.bidSyncPro.supplier.companySettings.manageSubscriptions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.pageObjects.BasePageActions;

/***
 * <h1>SubscriptionDetailsChargebeePopup</h1>
 * @author dmidura
 * <p>details: This class contains objects and methods to manipulate the <br>
 * 	"Subscription Details" Chargebee Popup that appears after the user <br>
 *  clicks on a subscription on the "Manage Subscriptions" Chargebee Popup</p>
 */
public class SubscriptionDetailsChargebeePopup extends BasePageActions {
	
	// xpaths
	private final String editSubscriptionBtn_xpath       = "//div[contains(@class, 'cb-section__action')][contains(text(), 'Edit Subscription')]";
	private final String subcriptionDetailsBackBtn_xpath = "//div[contains(span,'Subscription Details')]";
	
	// other
	private EventFiringWebDriver driver;
	
	public SubscriptionDetailsChargebeePopup(EventFiringWebDriver driver){
		super(driver);
		this.driver   = driver;
	}
	
	/* ----- getters ------ */
	private WebElement getEditSubscriptionBtn()        { return findByVisibility(By.xpath(this.editSubscriptionBtn_xpath));      }
	private WebElement getSubscriptionDetailsBackBtn() { return findByVisibility(By.xpath(this.subcriptionDetailsBackBtn_xpath));}

	/* ------ methods ----- */

	/***
	 * <h1>clickOnEditSubscription</h1>
	 * <p>purpose: Click on the "Edit Subscription" button to navigate<br>
	 * 	to the "Edit Subscription Details" page</p>
	 * @return EditSubscriptionDetailsChargebeePopup  
	 */
	public EditSubscriptionDetailsChargebeePopup clickOnEditSubscription() {
		this.getEditSubscriptionBtn().click();
		return new EditSubscriptionDetailsChargebeePopup(driver);
	}

	/***
	 * <h1>clickOnSubscriptionDetailsBackBtn</h1>
	 * <p>purpose: Click on the "Subscription Details" back button to navigate<br>
	 * 	to the "Manage Subscriptions" page</p>
	 * @return ManageSubscriptionsChargebeePopup 
	 */
	public ManageSubscriptionsChargebeePopup clickOnSubscriptionDetailsBackBtn() {
		this.getSubscriptionDetailsBackBtn().click();
		return new ManageSubscriptionsChargebeePopup(driver);
	}
}
