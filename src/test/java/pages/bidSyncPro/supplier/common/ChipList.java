package pages.bidSyncPro.supplier.common;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterResultsSectionNewClass;
import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;


public class ChipList extends BasePageActions{
	
	// other
	EventFiringWebDriver driver;
	private String chipList_xpath;
	FilterResultsSectionNewClass filterResultsSectionNewClass;

	/***
	 * 
	 * @param driver
	 * @param strChipListXPath = xpath to the top of the chip list 
	 * 	ex: //mat-chip-list[@id='matKeywordsChipList']
	 */
	public ChipList (EventFiringWebDriver driver, String strChipListXPath) {
		super(driver);
		this.driver = driver;
		this.chipList_xpath = strChipListXPath;
		 filterResultsSectionNewClass = new FilterResultsSectionNewClass(this.driver);
	}
	
	/***
	 * <h1>getAllChipsInChipList</h1>
	 * <p>purpose: Locate and return all the chips in the chip list</p>
	 * @return List <Chip>
	 */
	public List<Chip> getAllChipsInChipList() {

		// Get WebElement for each chip
		List <WebElement> allChips = this.findAllOrNoElements(driver, By.xpath(this.chipList_xpath + "//mat-chip[not(contains(text(),'Keyword'))]"));

		// Return as a list
		List<Chip> allChipsInList = new ArrayList<>();

		for( int i = 1; i<allChips.size() + 1; i++) {
			allChipsInList.add(new Chip(driver, this.chipList_xpath + "//mat-chip[" + i + "]"));  }
		return allChipsInList;
	}
	
	/**
	 * <h1>getChipInChipList</h1>
	 * <p>purpose: Return access to one chip in the chiplist</p>
	 * @param strChipName = name of the chip in the list you want
	 * @return Chip
	 */
	public Chip getChipInChipList (String strChipName) {
	   List<Chip> allChips = this.getAllChipsInChipList();
		try {
			return CollectionUtils.extractSingleton(CollectionUtils.select(allChips, Chip -> Chip.getChipText().equals(strChipName)));
		} catch(NullPointerException | IllegalArgumentException e) {
			fail("Test Failed: Chip \"" + strChipName + "\" isn't in the chip list");
			return null;
		}
	}
	
	/***
	 * <h1>getTextForAllChipsInChipList</h1> 
	 * <p>purpose: Get a list of just the text displaying for all the chips in the chip list</p>
	 * @return List <String>
	 */
	public List <String> getTextForAllChipsInChipList(){
		List <String> allChipText = new ArrayList<String>();
		this.getAllChipsInChipList().forEach(chip->allChipText.add(chip.getChipText()));
		return allChipText;
	}
	
	/***
	 * <h1>deleteAllChipsInChipList</h1>
	 * <p>purpose: Delete all the chips in the chip list</p>
	 */
	public void deleteAllChipsInChipList () {
		
		int intNumberChips = this.getAllChipsInChipList().size();
		System.out.println(GlobalVariables.getTestID() + " We have " + intNumberChips + " in the chip list");
		
		if (intNumberChips != 0) {
			for (Chip chip :  this.getAllChipsInChipList()) {
				String chipText = chip.getChipText();
				System.out.println(GlobalVariables.getTestID() + " Here's the chiptext " + chipText);
				
				// Sometimes we show three example keywords
				// We can't delete these, so don't try
				if ((intNumberChips < 4)          &
					(chipText.contains("Keyword 1") |
					 chipText.contains("Keyword 2") |
					 chipText.contains("Keyword 3"))) {
					System.out.println(GlobalVariables.getTestID() + " INFO: Skipping keyword deletion b/c this keyword is an example keyword");
					continue;
				}
				chip.deleteChipByKeyword(); }
		}
	}
	
	/***
	 * <h1>isChipInChipList</h1> 
	 * <p>purpose: Search the chip list for a specified chip text</p>
	 * @param strChipText = chip text to look for
	 * @return true if in the chip list | false if not in the chip list
	 */
	public Boolean isChipInChipList (String strChipText) {
		for (Chip chip : this.getAllChipsInChipList()) {
			if (chip.getChipText().equals(strChipText)) {
				return true; }
		}
		return false;
	}
	
	/***
	 * <h1>isChipListEmpty</h1>
	 * <p>purpose: Determine if the chip list is empty</p>
	 * @return true if empty | false if not empty
	 */
	public Boolean isChipListEmpty () {
		 return this.getAllChipsInChipList().isEmpty();
	}
	
	/* ----- Verifications ----- */
	
	/***
	 * <h1>verifyChipsInChipList</h1>
	 * <p>purpose: For each chip in List, verify that chip is currently displaying in the chip list. <br>
	 * 	Fail test if chip is not in the chip list</p>
	 * @param allChipst = List of containing the text for each chip you're expecting to see displayed in the chip list
	 * @return ChipList
	 */
	public ChipList verifyChipsInChipList(List <String> allChips){
		if (allChips.isEmpty()) { this.verifyChipListIsEmpty();}

		for (String chipText : allChips) {
			this.verifyChipInChipList(chipText); }

		return this;
	}

	/***
	 * <h1>verifyChipInChipList</h1>
	 * <p>purpose: Verify that a given chip is currently displaying in the chip list. <br>
	 * 	Fail test if chip is not in the chip list</p>
	 * @param strChipText = text for the chip
	 * @return ChipList
	 */
	public ChipList verifyChipInChipList(String strChipText){
		if(!isChipInChipList(strChipText)) { fail ("Test Failed: Chip \"" + strChipText + "\" is not in the chip list");}
		return this;
	}

	/***
	 * <h1>verifyChipNotInChipList</h1>
	 * <p>purpose: Verify that a given chip is not currently displaying in the chip list. <br>
	 * 	Fail test if chip is in the chip list</p>
	 * @param strChipText = text for the chip
	 * @return ChipList
	 */
	public ChipList verifyChipNotInChipList(String strChipText){
		if(isChipInChipList(strChipText)) { fail ("Test Failed: Chip \"" + strChipText + "\" is currently displaying in the chip list");}
		return this;
	}
	
	/**
	 *<h1>verifyChipListIsEmpty</h1>
	 *<p>purpose: Verify that the chip list is empty (i.e. contains no chips). Fail test if it is not</p>
	 * @return ChipList
	 */
	public ChipList verifyChipListIsEmpty() {
		if (!isChipListEmpty()) { fail ("Test Failed: Chip list is not empty");}
		return this;
	}

	/**
	 *<h1>verifyChipListIsNotEmpty</h1>
	 *<p>purpose: Verify that the chip list is not empty (i.e. there is a least 1 chip in the list). Fail test if chip list is empty</p>
	 * @return ChipList
	 */
	public ChipList verifyChipListIsNotEmpty() {
		if (isChipListEmpty()) { fail ("Test Failed: Chip list is empty");}
		return this;
	}
	
	/***
	 * <h1>verifyDeletionOfEachChipInChipList</h1>
	 * <p>purpose: Given a List of text for chips you want to delete from the chip list<br>
	 * 	delete each chip. As each chip is deleted verify that the chip is actually removed from<br>
	 * 	the chip list. Fail test if chip isn't initially in chip list or if chip is still present<br>
	 * 	in chip list after deletion</p>
	 * @param chipsToDelete = List of the text containing the name of each chip you want to delete from the chip list
	 * @return ChipList
	 */
	public ChipList verifyDeletionOfEachChipInChipList(List <String> chipsToDelete) {
		for (String chipText : chipsToDelete) {
			this.verifyChipInChipList(chipText);
			this.getChipInChipList(chipText).deleteChip();
			this.verifyChipNotInChipList(chipText); }
		
		return this;
	}
	
	public ChipList verifyDeletionOfChipInChipList(String chipToDelete){
		this.verifyChipInChipList(chipToDelete);
		this.getChipInChipList(chipToDelete).deleteChip();
		this.verifyChipNotInChipList(chipToDelete); 
		return this;
	}
}
