package pages.bidSyncPro.supplier.companySettings.manageSubscriptions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.bidSyncPro.supplier.companySettings.CompanySettingsCommon;

public class ManageSubscriptions extends CompanySettingsCommon {
    
    // xpaths
    private final String manageSubscriptionsButton_xpath = "//span[contains(text(), 'Manage Subscriptions')]";
    
    // ids
    //private final String manageUsersTab_id = "AdminProfile_1";
    
    public ManageSubscriptions(EventFiringWebDriver driver) {
        super(driver);
    }
    
    private WebElement getManageSubscriptionsButton() { return findByClickability(By.xpath(this.manageSubscriptionsButton_xpath)); }
    
    //private WebElement getManageUsersTab() {
        //return this.driver.findElement(By.id(this.manageUsersTab_id));
    //}
    
    /***
     * <h1>clickManageSubscriptions</h1>
     * <p>purpose: Click on the "Manage Subscriptions" button on the<br>
     * "Manage Your Subscriptions" page. OnClick a popup appears that accesses<br>
     * 	the Subscriptions in Chargebee. This method will also switch the iframe appropriately</p>
     * @return ManageSubscriptionsChargebeePopup  
     */
    public ManageSubscriptionsChargebeePopup clickManageSubscriptions() {
    	new WebDriverWait(driver, DEFAULT_TIMEOUT).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(@class,'loader-wrapper')]")));
        getManageSubscriptionsButton().click();
        ManageSubscriptionsChargebeePopup chargebeepopup = new ManageSubscriptionsChargebeePopup(driver);
        chargebeepopup.switchToPopup();
        return chargebeepopup;
    }
    
    //public void openManageUsersTab() {
     //   getManageUsersTab().click();
    //}
}
