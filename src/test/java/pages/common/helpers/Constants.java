
package pages.common.helpers;

/**
 * Class to hold all constants used in this project
 * 
 * @author ssomaraj
 *
 */
public class Constants {
		public static String supplierCompanySetupUrl = System.getProperty("supplierBaseURL") + "setup/company";
		public static String bidListUrl = System.getProperty("supplierBaseURL") + "dashboard/new-bids";
		public static String setPasswordUrl = System.getProperty("supplierBaseURL") + "setup/password";
		
		/*	-------		Error Messages ---------- 	*/
		
		public static String passwordHistoryErrorMessage = "Password is not valid as it is one of the most recent ones used";
}
