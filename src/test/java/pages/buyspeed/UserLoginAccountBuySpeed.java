package pages.buyspeed;

import pages.common.helpers.GlobalVariables;

/***
 * <h1>Class UserLoginAccount</h1>
 * @author dmidura
 * <p>details: This class holds info about BuySpeed user login credentials. <br>
 *             Note: To save a login, enter the login credentials into userAccounts_BuySpeed.json<br>
 *                   Then, use methods in BuySpeedLogin to access the login info.</p>
 *
 */
public class UserLoginAccountBuySpeed {
	
	// User login credentials
	private String AccountReference;		// Something to call the account. ex: "Generic Seller User"
	private String Name;					// The name for the account       ex: "buyspeed"
	private String Password;				// The password for the account   ex: "password"

	public UserLoginAccountBuySpeed () {
	}
	
	/* --------------- Methods --------------- */
	
	/***
	 * <h1>setUserAccount</h1>
	 * <p>purpose: Set the user account per required information </p>
	 * @param strAccountReference = Something to call the account. <br>
	 * 								ex: "Generic Seller User"<br>
	 * 								Note: This is what is used to reference the account across cucumber.
	 * @param strUserName = the name for this account
	 * @param strUserPassword = the password for this account
	 */
	public void setUserAccount (String strAccountReference, String strUserName, String strUserPassword) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Setting account info for %s\n", strAccountReference);
		setAccountReference (strAccountReference);
		setName (strUserName);
		setPassword (strUserPassword);
	}

	/***
	 * <h1>getAccountReference</h1>
	 * <p>purpose: get the currently stored value of the Account Reference</p>
	 * @return String = the Account Reference
	 */
	public String getAccountReference() {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Getting Account Reference = %s\n", AccountReference);
		return AccountReference;
	}

	/***
	 * <h1>setAccountReference</h1>
	 * <p>purpose: Set the user account reference </p>
	 * @param strAccountReference = Something to call the account. <br>
	 * 								ex: "Generic Seller User"<br>
	 * 								Note: This is what is used to reference the account across cucumber.
	 */
	public void setAccountReference(String accountReference) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Setting Account Reference = %s\n", accountReference);
		AccountReference = accountReference;
	}

	/***
	 * <h1>getName</h1>
	 * <p>purpose: get the currently stored value of the name for this account</p>
	 * @param None
	 * @return String = the Name
	 */
	public String getName() {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Getting Name = %s\n", Name);
		return Name;
	}

	/***
	 * <h1>setName</h1>
	 * <p>purpose: Set the user name </p>
	 * @param strUserName = the name for this account
	 * @return None
	 */
	public void setName(String email) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Setting Name = %s\n", email);
		Name = email;
	}

	/***
	 * <h1>getPassword</h1>
	 * <p>purpose: get the currently stored value of the password for account</p>
	 * @param None
	 * @return String = the Password
	 */
	public String getPassword(){
		System.out.printf(GlobalVariables.getTestID() + " INFO: Getting Password = %s\n", Password);
		return Password;
	}

	/***
	 * <h1>setPassword</h1>
	 * <p>purpose: Set the password </p>
	 * @param strUserPassword = the password for this account
	 * @return None
	 */
	public void setPassword(String password) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Setting Password = %s\n", password);
		Password = password;
	}
	
}
