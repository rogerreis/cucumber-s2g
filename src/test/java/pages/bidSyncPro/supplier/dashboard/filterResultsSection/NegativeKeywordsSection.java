package pages.bidSyncPro.supplier.dashboard.filterResultsSection;

import static org.junit.Assert.fail;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.common.ChipList;

/***
 * <h1>Class NegativeKeywordsSection</h1>
 * @author dmidura
 * <p>details: This class houses the objects and methods for the Negative Keywords section on the Filter Results</p>
 */
public class NegativeKeywordsSection extends FilterResultsSectionNewClass {
	
	// Other 
	private EventFiringWebDriver driver;	
	private final String PlaceholderMessage = "Enter Keywords";
	
	// xpaths
	private final String negKeywordsInput_xpath       = "//input[@id='txtNegKeyword']";
	private final String negKeywordsInputBox_xpath    = "//mat-form-field[@id='matNegKeywordFormField']//input[@id='txtNegKeyword']";
	private final String negKeywordsUnderline_xpath   = "//mat-form-field[@id='matNegKeywordFormField']//div[contains(@class, 'mat-form-field-underline')]";
	private final String negKeywordsAddBtn_xpath      = "//button[@id='btnAddNegKeyword']";
	private final String negKeywordsChipList_xpath    = "//mat-expansion-panel[@id='matNegKeywordExpPanel']//mat-chip-list";
	private final String negKeywordsTxt_xpath         = "//p[@id='hideBidWords'][contains(., 'Hides bids with any of these words')]";
	
	public NegativeKeywordsSection(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	/* ----- getters ----- */
	private WebElement getKeywordsAddBtn()    { return findByScrollIntoViewBottomOfScreen(By.xpath(this.negKeywordsAddBtn_xpath)); }
	private WebElement getKeywordsInput()     { return findByVisibility(By.xpath(this.negKeywordsInput_xpath));                    }
	private WebElement getKeywordsUnderline() { return findByVisibility(By.xpath(this.negKeywordsUnderline_xpath));                }
	private WebElement getKeywordsInputBox()  { return findByVisibility(By.xpath(this.negKeywordsInputBox_xpath));                 }
	private WebElement getNegKeywordsTxt()    { return findByVisibility(By.xpath(this.negKeywordsTxt_xpath));                      }

	/* ----- methods ----- */

	/***
	 * <h1>clickAddBtn</h1>
	 * <p>purpose: Click on the add (+) button on the Keyword Input</p>
	 * @return KeywordsSection
	 */
	private NegativeKeywordsSection clickAddBtn() { 
		this.getKeywordsAddBtn().click(); 
		return this;
	}
	
	/***
	 * <h1>addNegativeKeyword</h1>
	 * <p>purpose: Add a negative keyword into the input and then click on "+"</p>
	 * @param strKeyword = your negative keyword to add
	 * @return FilterResultsSectionNew = access to the Negative Keywords chip list
	 */
	public FilterResultsSectionNewClass addNegativeKeyword (String strKeyword) {
		this.getKeywordsInput().sendKeys(strKeyword);
		this.clickAddBtn();
		return new FilterResultsSectionNewClass(driver);
	}

	/***
	 * <h1>addNegativeKeywords<h1>
	 * <p>purpose: Add a list of keywords into the input, clicking on "+" after each addition</p>
	 * @param List <String> keywordsList = list of all your keywords
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass addNegativeKeywords (List <String> keywordsList){
		for (String strKeyword : keywordsList) {
			this.getKeywordsInput().sendKeys(strKeyword);
			this.clickAddBtn();
		}
		return new FilterResultsSectionNewClass(driver);
	}
	/***
	 * <h1>getNegativeKeywordsChipList</h1>
	 * <p>purpose: Access the Negative Keywords ChipList</p>
	 * @return ChipList
	 */
	public ChipList getNegativeKeywordsChipList(){
		return new ChipList(driver, this.negKeywordsChipList_xpath);
	}
	
	
	/* ----- verifications ----- */
	
	/***
	 * <h1>verifySectionInitalization</h1>
	 * <p>purpose: Verify that the Negative Keywords section has initialized per:<br>
	 * 	1. input bar with "+" button<br>
	 * 	2. Default placeholder message in input of "Enter Keywords"<br>
	 * 	3. Text "Hides bids with any of these words" displays<br>
	 * 	4. Chip list is empty<br>
	 * 	Fail test if any of the above conditions are not met</p>
	 * @return NegativeKeywordsSection
	 */
	public NegativeKeywordsSection verifySectionInitialization() {
		
		// 1. Verify that input with "+" button is visible
		if(!this.getKeywordsInput().isDisplayed()) {
			fail("Test Failed: \"Negative Keywords\" input is not displayed when initializing \"Negative Keywords\" section"); }
		
		if(!this.getKeywordsUnderline().isDisplayed()) {
			fail("Test Failed: \"Negative Keywords\" input underline is not displayed when initializing \"Negative Keywords\" section"); }

		if(!this.getKeywordsAddBtn().isDisplayed()) {
			fail("Test Failed: \"Negative Keywords\" add button (\"+\") is not displayed when initializing \"Negative Keywords\" section"); }
		
		// And the correct placeholder message is displaying
		if (!this.getKeywordsInputBox().getAttribute("placeholder").equals(this.PlaceholderMessage)) {
			fail("Test Failed: Negative Keywords input did not initialize with expected placeholder \"" + this.PlaceholderMessage + "\""); }
		
		// 2. Chip list should be >= 0
		if (this.getNegativeKeywordsChipList().getAllChipsInChipList().size() < 0) {
			fail("Test Failed: \"Negative Keywords\" chip list contains a negative amount of chips on section initialization");}
		
		// 3. "Hides bids with any of these words" text should be displayed
		if(!this.getNegKeywordsTxt().isDisplayed()) {
			fail("Test Failed: \"Hides bids with any of these words\" text is not displaying under \"Negative Keywords\" section on section initialization");}
		
		// 4. Close/Open "Negative Keywords" section header and verify section is closed/opened.
		this.closeNegativeKeywordsSection()
			.verifyNegativeKeywordsSectionIsClosed()
			.openNegativeKeywordsSection()
			.verifyNegativeKeywordsSectionIsOpen();
		
		return this;
	}

}
