@must @registration @level3
Feature: Supplier Registration - Keywords Screen

@ARGO-2065
Scenario: User does not receive error when registering with more than two keywords
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to the Enter Keywords screen
	When I enter more than two keywords on the Enter Keywords screen
	Then I will verify that I do not see a profile error on the Enter Keywords screen
	When I delete more than two keywords on the Enter Keywords screen
	Then I will verify that I do not see a profile error on the Enter Keywords screen

# TODO: ARGO-2577