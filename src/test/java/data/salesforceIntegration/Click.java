package data.salesforceIntegration;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Click {
    
    private String userId;
    private String bidId;
    private String source;
    
    public Click(String userId, String bidId, String source) {
        this.userId = userId;
        this.bidId = bidId;
        if(source.equals("Your Saved Bids")) {
            this.source = "Saved Bids";
        } else if (source.equals("New For You")) {
            this.source = "New Bids";
        } else {
            this.source = source;
        }
    }
    
    /***
     * <h1>equals</h1>
     * <p>purpose: Necessary override so this class functions correctly with comparators</p>
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if(o == this) return true;
        if (!(o instanceof Click)) {
            return false;
        }
        Click other = (Click) o;
        return new EqualsBuilder()
                .append(userId, other.getUserId())
                .append(bidId, other.getBidId())
                .append(source, other.getSource())
                .isEquals();
    }
    
    /***
     * <h1>hashCode</h1>
     * <p>purpose: Necessary override so this class functions correctly with mapping objects</p>
     * @return int
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(userId)
                .append(bidId)
                .append(source)
                .toHashCode();
    }
        
        
    public String getUserId() {
        return this.userId;
    }
    
    public String getBidId() {
        return this.bidId;
    }
    
    public String getSource() {
        return this.source;
    }
}
