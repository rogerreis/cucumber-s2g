@must @topBar @bidSearch_detail @level2
Feature: Dashboard - Top Bar Functionality
	This feature file exercises the functionality of the top bar objects on the Supplier Dashboard

# Basic initialization test
Scenario: Supplier Dashboard Top Bar MouseOver Actions are Correctly Enabled per:
	1. Navigation icons display tooltips

	Given I have logged into the Supplier side as user "Basic"
	Then I will verify that the Supplier Home Screen Header Elements correctly display MouseOver actions


# Basic initialization test
Scenario Outline: Supplier Dashboard Top Bar Nav Menus display all options and icons in menu

	Given I have logged into the Supplier side as user "Basic"
	When I open the top bar nav menu "<navMenu>"
	Then I will verify that the options and icons for "<navMenu>" nav menu display as expected in the dropdown
    
	Examples: Top Bar Nav Menus
	| navMenu          |
	| Bid List         |
	| Company Settings |
	| Account          |

@BIDS-547
Scenario: Supplier Dashboard Top Bar Nav Menus display all options and icons in menu for new user
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I complete Supplier Registration
	When I open the top bar nav menu "Marketplace"
	#Then I should see a popup to update company profile and update the company profile should be redirected to Marketplace
    
