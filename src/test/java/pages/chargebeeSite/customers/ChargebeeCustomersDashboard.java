package pages.chargebeeSite.customers;

import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.chargebeeSite.common.CommonNavSliderMenu;
import pages.chargebeeSite.common.GenericChargebeeSearch;

/***
 * <h1>ChargebeeCustomersDashboard</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects on the "Customers" dashboard home<br>
 * 			   on the Chargebee test site. Note: CustomersTable class holds the actual customer records in the table</p>
 */
public class ChargebeeCustomersDashboard extends CommonNavSliderMenu {
	
	//Other 
	private EventFiringWebDriver driver;
	private String strPagePath = System.getProperty("chargebeeTestSiteURL") + "customers";
	
	public ChargebeeCustomersDashboard (EventFiringWebDriver driver) {
		super(driver);
		this.driver 			= driver;
	}
	
	/* ----- methods ----- */
	
	/***
	 * <h1>navigateToHere</h1>
	 * <p>purpose: Navigate directly to the Chargebee Customers Dashboard<br>
	 * 	Useful way to return to Chargebee if you've navigated the browser away during a test</p>
	 * @return ChargebeeCustomersDashboard
	 */
	public ChargebeeCustomersDashboard navigateToHere() {
		driver.navigate().to(this.strPagePath);
		return this;
	}

	/***
	 * <h1>getCustomersTable</h1>
	 * <p>purpose: Access to the customers table
	 * @return CustomersTable
	 */
	public CustomersTable getCustomersTable() {
		return new CustomersTable(driver);
	}
	
	/***
	 * <h1>getSearchBar</h1>
	 * <p>purpose: Access to the Generic Chargebee Search bar (above table)</p>
	 * @return GenericChargebeeSearch
	 */
	public GenericChargebeeSearch getSearchBar() {
		return new GenericChargebeeSearch(driver);
	}
	
	/* ----- verifications ----- */

	/***
	 * <h1>verifyPageLoad</p>
	 * <p>purpose: Verify that Customers screen has loaded. Fail test if subscriptions hasn't loaded</p>
	 * @return ChargebeeCustomersDashboardCustomers 
	 */
	public ChargebeeCustomersDashboard verifyPageLoad() {
		int time = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, time)
			.withMessage("Test Failed: Did not navigate to the customers dashboard after " + time + "seconds")
			.until(ExpectedConditions.urlToBe(this.strPagePath));
		return this;
	}	
}
