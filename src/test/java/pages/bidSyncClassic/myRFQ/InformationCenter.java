package pages.bidSyncClassic.myRFQ;

import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class InformationCenter</h1>
 * @author dmidura
 * <p>details: This class contains page objects and methods for the 
 * 	"Information Center" screen in BidSync Classic MYRFQ. This page 
 * 	functions as the landing screen of a login to MYRFQ.
 */
public class InformationCenter extends BasePageActions{
	
	// xpath
	private final String supplierCRMLink_xpath = "//a[contains(.,'Supplier CRM')]";
	private final String bidExtractLink_xpath = "//a[contains(text(),'Bid Extract')]";
	private final String bidExtractSearchLink_xpath = "//a[contains(text(),'Bid Extract')]";
	private final String bidExtractExractorLink_xpath = "//select[@name='extractor']";
	
	// Other
	private EventFiringWebDriver driver;
	
	public InformationCenter (EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
		new WebDriverWait(driver, DEFAULT_TIMEOUT)
		.withMessage("ERROR: Could not switch to frame to access \"Information Center\" section")
		.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("main"));
	}
	
	// getters
	private WebElement getSupplierCRMLink() { return findByVisibility(By.xpath(this.supplierCRMLink_xpath)); }
	private WebElement getBidExtractLink() { return findByVisibility(By.xpath(this.bidExtractSearchLink_xpath)); }
	
	/* ----- methods ----- */
	
	/***
	 * <h1>clickSupplierCRMLink</h1>
	 * <p>purpose: Click on the "Supplier CRM" link to navigate to Supplier Search
	 * @return SearchForSupplier
	 */
	public SearchForSupplier clickSupplierCRMLink() {
		this.getSupplierCRMLink().click();
		return new SearchForSupplier(driver);
	}
}
