package stepDefinitions.bidSyncPro.supplier.companySettings.agencyInteraction;
 
import static org.junit.Assert.fail;

import org.openqa.selenium.WebElement;
import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import pages.bidSyncPro.supplier.companySettings.agencyInteractions.ContractsForAgencyPage;
import pages.bidSyncPro.supplier.companySettings.agencyInteractions.PublicContractAgencySearchPage;
import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.toBeDeprecated.StepDefinition;

/***
 * <h1>Class AllBids_stepDefinitions</h1>
 * <p>details: This class houses test steps to test the All Bids Page</p>
 * @author jlara
 */
public class PublicContractAgency_stepDefinitions extends StepDefinition {
  
	private PublicContractAgencySearchPage publicContractsPage;
	private ContractsForAgencyPage contractsForAgencyPage;
	
	public PublicContractAgency_stepDefinitions(CoreAutomation automation) throws Throwable {
		super(automation);
		publicContractsPage = new PublicContractAgencySearchPage(driver);
		contractsForAgencyPage = new ContractsForAgencyPage(driver);
		
	} // end of constructor
		
	/* ------------ Test Steps ---------------- */


	/***
	 * <h1>I_have_navigated_to_the_Public_Contracts_Page</h1>
	 * <p>Purpose: Navigate to the Public Contracts Page</p>
	 * @param None
	 * @return None
	 * @throws Throwable
	 */
	@Given("^I have navigated to the Public Contracts Page$")
	public void I_have_navigated_to_the_Public_Contracts_Page() throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Navigate to the Public Contracts Page");

		publicContractsPage.verifyNavigation();
	}
	
	/***
	 * <h1>I_will_verify_that_the_Public_Contracts_Page_has_required_criteria_fields</h1>
	 * <p>Purpose: Verify that the Public Contracts Page has required criteria fields</p>
	 * @param None
	 * @return None
	 * @throws Throwable
	 */
	@And("^I will verify that the Public Contracts Page has required criteria fields$")
	public void I_will_verify_that_the_Public_Contracts_Page_has_required_criteria_fields() throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify that the Public Contracts Page has required criteria fields");

		helper.addSystemWait(2);
		//labels
		verifyInnerText(publicContractsPage.getAgencyNameSearchLabel(), "Agency Name");
		verifyInnerText(publicContractsPage.getRegionSearchLabel(), "Region");
	
		//inputs, radio buttons, select, submit
		verifyVisible(publicContractsPage.getAgencyNameSearchTextBox());
		verifyVisible((WebElement) publicContractsPage.getRegionSearchSelect());
				
		PublicContractAgencySearchPage.REGIONS.forEach((k,v)->{
			try {
				publicContractsPage.selectRegionSearchOptionByText(v);
//				
//				if(!allBidsPage.getText(k).contains(v))
//					fail(String.format("FAILED: Cannot sort %s on %s",v,allBidsPage.getText(k)));
//				
//				System.out.printf(GlobalVariables.getTestID() + " PASSED: Sorted %s on %s%n",v,allBidsPage.getText(k));
//
//				allBidsPage.click(k);
//				v = "Sorted Descending";
//				if(!allBidsPage.getText(k).contains(v))
//					fail(String.format("FAILED: Cannot sort %s on %s",v,allBidsPage.getText(k)));
//				
				System.out.printf(GlobalVariables.getTestID() + " PASSED: Selected %s from %s%n",v,publicContractsPage.getRegionSearchSelect());
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		});
		
		verifyInnerText(publicContractsPage.getSearchButton(), "Search");
	}
	
	/***
	 * <h1>I_will_verify_that_the_Public_Contracts_Page_has_required_Results_table_headers</h1>
	 * <p>Purpose: Verify that the Public Contracts Page has required Results table headers</p>
	 * @param None
	 * @return None
	 * @throws Throwable
	 */
	@And("^I will verify that the Public Contracts Page has required Results table headers$")
	public void I_will_verify_that_the_Public_Contracts_Page_has_required_Results_table_headers() throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify that the Public Contracts Page has required Results table headers");

		verifyInnerText(publicContractsPage.getSectionSearchHdrLabel(), "Search");
		verifyInnerText(publicContractsPage.getSectionAgencyColHdrLabel(), "Agency");
		verifyInnerText(publicContractsPage.getSectionContractsColHdrLabel(), "Contracts");
	}
	
	@And("^I will verify that the Public Contracts Page Search returns Empty Results$")
	public void I_will_verify_that_the_Public_Contracts_Page_Search_returns_Empty_Results() throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify that the Public Contracts Page Search returns empty Results");
	
		//enter text in search and
		publicContractsPage.sendKeysAgencyNameSearchTextBox("randomName")
							.selectRegionSearchOptionByValue("-1")
							.clickSearchButton();
		//publicContractsPage.verifyInnerText("NoResultsMessage", "Contracts");
		
	}
	
	
	/***
	* <h1>I_will_verify_that_the_Public_Contracts_Page_Search_returns_Results</h1>
	* <p>Purpose: Verify that the Public Contracts Page Search returns Results</p>
	* @param None
	* @return None
	* @throws Throwable
	*/
	@And("^I will verify that the Public Contracts Page Search returns Results for \"(.*)\"$")
	public void I_will_verify_that_the_Public_Contracts_Page_Search_returns_Results_for_Agency(String strAgencyName) throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify that the Public Contracts Page Search returns Results");
	
		//column headers: Fill in when page is fixed
		//companyContractsPage.click("ResultsHdrOrderId");
		//companyContractsPage.verifyVisible("ResultsHdrOrderIdDesc");
		
		//enter text in search and
		publicContractsPage.sendKeysAgencyNameSearchTextBox(strAgencyName)
							.clickSearchButton();
	}
	
	@Given ("^I click the Public Contracts Page Search Results Contract for \"(.*)\"$")
	public void I_click_the_Public_Contracts_Page_Search_Results_Contract_for_Agency (String strAgencyName) throws Throwable {
		
		WebElement we = publicContractsPage.getAgencyFromTable(strAgencyName);
		
		if(we.isDisplayed()) 
			we.click();
		else
			fail(String.format("FAILED: Contract for Agency '%s' not found.",strAgencyName)); 
	}

	@Given ("^I have navigated to the Public Contracts Agency Page for \"(.*)\"$")
	public void I_have_navigated_to_the_Public_Contracts_Agency_Page_for_Agency (String strAgencyName) throws Throwable {
		contractsForAgencyPage.verifyNavigation();
	}

	/***
	 * 
	 * @throws Throwable
	 */
	@And("^I will verify that the Public Contracts Agency Page has required criteria fields$")
	public void I_will_verify_that_the_Public_Contracts_Agency_Page_has_required_criteria_fields() throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify that the Public Contracts Agency Page has required criteria fields");

		helper.addSystemWait(2);
		//labels
		verifyInnerText(contractsForAgencyPage.getSectionSearchHdrLabel(),"Search");
		verifyInnerText(contractsForAgencyPage.getSearchForLabel(),"Search For");
		
		verifyInnerText(contractsForAgencyPage.getContractTypeLabel(),"Contract Type");
		verifyInnerText(contractsForAgencyPage.getQualificationsLabel(),"Qualifications");
		verifyInnerText(contractsForAgencyPage.getExpiredBetweenLabel(),"Expires between");
		verifyInnerText(contractsForAgencyPage.getExpiredBetweenFromLabel(),"From");
		verifyInnerText(contractsForAgencyPage.getExpiredBetweenToLabel(),"To");
		//DirectionsDateFormat
		verifyInnerText(contractsForAgencyPage.getClassificationsLabel(),"Classifications");
		verifyInnerText(contractsForAgencyPage.getOptionsLabel(),"Options");
		
		//inputs, radio buttons, select, submit
		
		verifyVisible(contractsForAgencyPage.getSearchForTextBox());
		verifyVisible(contractsForAgencyPage.getSearchForTitleDescriptionRadioButton());
		verifyVisible(contractsForAgencyPage.getSearchForContractNumberRadioButton());
		verifyVisible((WebElement) contractsForAgencyPage.getContractTypeSelect());
		//verifyVisible(contractsForAgencyPage.getQualificationsCheckboxBRG());
		verifyVisible(contractsForAgencyPage.getExpiredBetweenFromTextBox());
		verifyVisible(contractsForAgencyPage.getExpiredBetweenToTextBox());
		//classifications text box
		//expired contracts checkbox
		verifyInnerText(publicContractsPage.getSearchButton(), "Search");
	}
	
}

