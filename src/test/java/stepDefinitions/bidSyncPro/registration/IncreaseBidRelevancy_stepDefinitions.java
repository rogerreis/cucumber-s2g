package stepDefinitions.bidSyncPro.registration;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import pages.bidSyncPro.registration.IncreaseBidRelevancy;
import pages.common.helpers.GlobalVariables;

public class IncreaseBidRelevancy_stepDefinitions {
	
	
	private EventFiringWebDriver driver;
	
	public IncreaseBidRelevancy_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
	}

	@And ("^I can Increase Bid Relevancy five times before returning to the Your Bids are Ready screen$")
	public void I_can_Increase_Bid_Relevancy_five_times_before_returning_to_the_Your_Bids_are_Ready_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking Thumbs Up/Thumbs Down five times before reaching \"Your Bids are Ready\" screen");

		new IncreaseBidRelevancy(driver)
			.clickThumbsUpOrThumbsDownAtRandom(5)
			.verifyPageInitialization();
	}

	@Then ("^I will be on the first Increase Bid Relevancy page$")
	public void I_will_be_on_the_first_Increase_Bid_Relevancy_page() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying app is on the first Bid Relevancy screen");

		new IncreaseBidRelevancy(driver)
			.verifyNavigatedToFirstBidRelevancyPage();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: App is on the first Bid Relevancy screen");
	}

	

}
