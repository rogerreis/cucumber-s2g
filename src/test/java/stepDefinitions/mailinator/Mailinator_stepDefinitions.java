package stepDefinitions.mailinator;

import core.CoreAutomation;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import pages.common.helpers.GlobalVariables;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Mailinator_stepDefinitions {

	private EventFiringWebDriver driver;

	public Mailinator_stepDefinitions(CoreAutomation automation) {
		this.driver     = automation.getDriver();
	}
	

    /* ------------ Test Steps ---------------- */
	
	@Given("^I navigate to Mailinator team inbox$")
	public void iNavigateToMailinatorTeamInbox() {
		System.out.println(GlobalVariables.getTestID() + " ---CLEANUP: Navigating to team inbox");

	    driver.get("https://phimail.mailinator.com/v3/index.jsp?token=b312ec9815a4432c9cf436cfe744d312");
		// mandrews: maximize blows up for some reason when using grid,
		// so only maximize when running locally.
		if (System.getProperty("env").toString().equals("local") && !System.getProperty("os.name").toLowerCase().startsWith("mac os x")) {
			driver.manage().window().maximize();
		}
		// don't let IE11 systems resize the browser smaller
		if (System.getProperty("browser").equals("IE11")) {
			driver.manage().window().maximize();
		}
		if (System.getProperty("env").toString().equals("grid")) {
			if (System.getProperty("browser").equals("chrome")) {
				// can't just maximize chrome, you have to set specific resolution
				//driver.manage().window().setSize(new Dimension(1366, 768));
				driver.manage().window().setSize(new Dimension(1440, 1440));
			}
		}
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: Browser resolution = %s\n", driver.manage().window().getSize());
		//System.out.print("Navigating to login page\n");
		
		new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[contains(@id,'team')]")));
		driver.findElement(By.xpath("//p[contains(@id,'team')]")).click();
		
		
	}

	@When("^I delete all emails in the Mailinator team inbox$")
	public void iSelectAllEmailsInTheMailinatorTeamInbox() {
		System.out.println(GlobalVariables.getTestID() + " ---CLEANUP: Executing deletion of all emails in inbox");
		// It can only delete up to 5 messages at a time  :P
		// Just delete 1 at a time...whatever
		Boolean end = true;
		int num = 0;

		// dmidura: there are a ton of emails in this inbox, so just keep deleting until there aren't anymore
		while (end) {
			// Click on "Team Inbox" button to refresh inbox
			driver.findElement(By.xpath("//p[@id='bigrow_team']")).click();
			
			// Delete all emails currently displayed in inbox
			try {
				num = new WebDriverWait(driver, 5)
					.until(ExpectedConditions.visibilityOfAllElementsLocatedBy((By.xpath("//i[@class='fa fa-square-o fa-lg']"))))
					.size();

				for (int i = 0; i < num; i++) {
					// scrapes the page each iteration to avoid the stale element exception
					WebElement checkbox = driver.findElements(By.xpath("//i[@class='fa fa-square-o fa-lg']")).get(i);
					((JavascriptExecutor)driver).executeScript("window.scrollTo(" + checkbox.getLocation().x + "," + (checkbox.getLocation().y - 150) + ")");
					checkbox.click();
					driver.findElement(By.xpath("//i[@class='fa fa-trash fa-stack-1x fa-inverse']")).click(); 
				}

			} catch (TimeoutException t) {
				// No more emails in inbox
				end = false;
			}
		}

		System.out.println(GlobalVariables.getTestID() + " ---CLEANUP: Cleanup complete");
	}
}

