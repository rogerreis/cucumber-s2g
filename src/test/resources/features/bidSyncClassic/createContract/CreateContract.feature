Feature: Integrations > BidSync Classic - CreateBid
	This will create a bid in BidSync Classic

   Scenario:
	Given I navigate my browser to the BidSync classic login page		
	When I enter my username as "kirk1984" and password as "test1234"
	Then I will be on the agency home page
	Given I am logged in as an agency
	Then I will create a new contract	