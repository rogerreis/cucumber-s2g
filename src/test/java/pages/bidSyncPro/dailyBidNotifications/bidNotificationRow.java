package pages.bidSyncPro.dailyBidNotifications;

import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.DateHelpers;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.StringHelpers;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class bidNotificationRow</h1>
 * @author dmidura
 * <p>details: This class defines page Objects and methods to manipulate a single bid notification
 * 	row found in the daily bid notification email
 */
public class bidNotificationRow extends BasePageActions{
	
	/* --------------- Configurable values ----------------------------- */
	/* ---------------------------------------------------------------- */
	// For maintenance purposes, update these values as needed:

	// TODO: update to System Property after QA starts sending notifications
	private final String   expectedDomain = "https://app.bidsync.com/";  

	// Required Static Images
	private final String     expectedStarImage       = "https://supplier.phi-qa.cloud/assets/email/notification-email/star.png";
	private final String     expectedSpyGlassImage   = "https://supplier.phi-qa.cloud/assets/email/notification-email/view.png";
	private final String     expectedClockImage      = "https://supplier.phi-qa.cloud/assets/email/notification-email/history-clock.png";
	private final String[]   expectedBlurImage       = {"https://supplier.phi-qa.cloud/assets/email/notification-email/blur-1.png",
	                                                    "https://supplier.phi-qa.cloud/assets/email/notification-email/blur-2.png",
	                                                    "https://supplier.phi-qa.cloud/assets/email/notification-email/blur-3.png",
	                                                    "https://supplier.phi-qa.cloud/assets/email/notification-email/blur-4.png",
	                                                    "https://supplier.phi-qa.cloud/assets/email/notification-email/blur-5.png"};
	private final String[]   expectedMatchScoreImage = {"https://s3-us-west-2.amazonaws.com/phi-prod-periscopeholdings-cdn-frontend-supplier/assets/images/email/best.jpg",
	                                                    "https://s3-us-west-2.amazonaws.com/phi-prod-periscopeholdings-cdn-frontend-supplier/assets/images/email/good.jpg",
	                                                    "https://s3-us-west-2.amazonaws.com/phi-prod-periscopeholdings-cdn-frontend-supplier/assets/images/email/possible.jpg",
	                                                    "https://s3-us-west-2.amazonaws.com/phi-prod-periscopeholdings-cdn-frontend-supplier/assets/images/email/unlikely.jpg"};

	// Required Static Text
	private final String expectedBidNumberText = "Bid #:";
	private final String expectedViewBidText   = "View Bid";
	private final String expectedDaysLeftText  = "Days Left";
	private final String expectedDueDateText   = "Due:";
	/* ---------------------------------------------------------------- */
	/* ---------------------------------------------------------------- */
	
	// Required Dynamic Elements displayed in each bid notification row
	private String   matchIndicator; // form: <best | possible | unlikely> match (ex: best match)
	private String   bidTitle;
	private String   bidTitleLink;   // links to bid if subscribed     | links to manage-subscriptions if not subscribed
	private String   bidLocation;
	private String   bidDescription; // description text if subscribed | blank if not subscribed 
	private String   readMoreLink;   // Read More link if subscribed   | blurred link if not subscribed
	private String   viewBidLink;    // links to bid if subscribed     | links to manage-subscriptions if not subscribed
	private String   bidNumber;      // can be blank
	private String   dueDate;
	private int      daysLeft;
	
	// Optional Dynamic Elements
	private String   starImage;      // Star Button (image path) displays if subscribed     | null if not subscribed
	private String   starLink;       // Star Button (link component) displays if subscribed | null if not subscribed
	private String   subscribeLink;  // Subscription Button (link component) displays if not subscribed | null if subscribed
	private String   subscribeText;  // Subscription Button (text component) displays if not subscribed | null if subscribed
	
	// Required Static Elements
	private String  viewBidText;     
	private String  bidNumberText;   
	private String  dueDateText;     
	private String  daysLeftText;    
	private String  spyGlassImage;   
	private String  clockImage;      

	// Other
	private DateHelpers   dateHelpers;
	private StringHelpers stringHelpers;
	private String        emailGenerationDate;
	
	// Fix for release OCT 7
	/***
	 * <h1>bidNotificationRow</h1>
	 * <p>purpose: Class constructor. Automatically sets all required and optional bid row elements
	 * @param driver
	 * @param emailGenerationDate = String MM/DD/YYYY (ex: 02/02/2019) is the generated email date, from bottom of notification
	 * @param extractedText = List <String> with 12 elements, corresponding to expected text for bid notification row<br>
	 * 	Note: Text should be extracted from Mailinator email, set in the "text/html" view
	 */
/*	public bidNotificationRow (EventFiringWebDriver driver, String emailGenerationDate, List <String> extractedText) {
		super(driver);
		dateHelpers   = new DateHelpers();
		stringHelpers = new StringHelpers();
		Assert.assertEquals("ERROR: bidNotificationRow expects extractedText with exactly 12 elements", 12, extractedText.size());
		this.emailGenerationDate = emailGenerationDate;

		/*----- Example of expected format for Subscribed Bid: -----*/
		// [best match]
		// 467453 SINGLE PURPOSE EXPLOSIVE DETECTION DOG<https://app.bidsync.com/bid-detail/72f4801b-4692-4a60-9270-3817b8bb0307?email=Y>
		// Detroit
		// The City of Detroit is requesting a one (1) time purchase of single purpose explosive detection bomb dogs. All vendors submitting offers for this bid must submit the bid packet in its entirety including any additional/optional paragraphs, clearances and, when required, affidavits to their BidSync ve... Read More<https://app.bidsync.com/bid-detail/72f4801b-4692-4a60-9270-3817b8bb0307?email=Y>
		// [https://firebasestorage.googleapis.com/v0/b/argo-demo.appspot.com/o/view.png?alt=media&token=704a5fe7-32e7-476a-8aef-5f05d3d5e4ff]
		// View Bid <https://app.bidsync.com/bid-detail/72f4801b-4692-4a60-9270-3817b8bb0307?email=Y>
		// Bid #: 19LC2681
		// Due: 02/06/2019
		// [https://firebasestorage.googleapis.com/v0/b/argo-demo.appspot.com/o/history-clock.png?alt=media&token=bf1116e2-2fb3-4b75-81c3-442f017ca916]
		// 6
		// Days Left
		// [https://firebasestorage.googleapis.com/v0/b/argo-demo.appspot.com/o/star.png?alt=media&token=2ffaad57-143c-45ef-8abf-ce8a3b8c6d01]<https://app.bidsync.com/bid-detail/72f4801b-4692-4a60-9270-3817b8bb0307?save=true>
	
		/* ----- Example of expected format of Unsubscribed Bid: -----*/
		// [best match]
		// KENNELING SERVICE FOR DETECTOR DOG<https://app.bidsync.com/admin/manage-subscriptions?email=Y>
		// [https://firebasestorage.googleapis.com/v0/b/argo-demo.appspot.com/o/blur-4.png?alt=media]
		// [https://firebasestorage.googleapis.com/v0/b/argo-demo.appspot.com/o/blur-4.png?alt=media]
		// [https://firebasestorage.googleapis.com/v0/b/argo-demo.appspot.com/o/view.png?alt=media&token=704a5fe7-32e7-476a-8aef-5f05d3d5e4ff]
		// View Bid <https://app.bidsync.com/admin/manage-subscriptions?email=Y>
		// Bid #:
		// Due: 02/14/2019
		// [https://firebasestorage.googleapis.com/v0/b/argo-demo.appspot.com/o/history-clock.png?alt=media&token=bf1116e2-2fb3-4b75-81c3-442f017ca916]
		// 12
		// Days Left
		// SUBSCRIBE <https://app.bidsync.com/admin/manage-subscriptions>

		
		// Required Dynamic Elements
/*		this.matchIndicator  = extractedText.get(0).replaceFirst("^\\[", "").replaceFirst("\\]$", "");
		this.bidTitle        = extractedText.get(1).replaceFirst("<\\s*\\S*\\d*\\D*>$", "");
		this.bidTitleLink    = extractedText.get(1).replace(this.bidTitle, "").replaceFirst("^<", "").replaceFirst(">$", "");
		this.bidLocation     = extractedText.get(2).replaceFirst("^\\[", "").replaceFirst("\\]$", "");
		this.bidDescription  = extractedText.get(3).replaceFirst("\\s?(Read More){1}<\\s*\\S*\\d*\\D*>$", "").replaceFirst("^\\[", "").replaceFirst("\\]$", "");
		this.readMoreLink    = extractedText.get(3).replace(this.bidDescription, "").replaceFirst("\\s?(\\.\\.\\.)?\\s?(Read More){1}<","").replaceFirst(">$", "").replaceFirst("^\\[", "").replaceFirst("\\]$", "");
		this.viewBidLink     = extractedText.get(5).replaceFirst("^" + this.getExpectedViewBidText()   + "\\s?<", "").replaceFirst(">$", "");
		this.bidNumber       = extractedText.get(6).replaceFirst("^" + this.getExpectedBidNumberText() + "\\s?", "");
		this.dueDate         = extractedText.get(7).replaceFirst("^" + this.getExpectedDueDateText() + "\\s?", "");
		this.daysLeft        = Integer.parseInt(extractedText.get(9));
		
		// Optional Dynamic Elements
		if (!extractedText.get(11).contains("star")) {
			// Unsubscribed bid:
			// Subscribe button displays (both button text and link)
			this.subscribeLink   = extractedText.get(11).replaceFirst("^SUBSCRIBE\\s?<","").replaceFirst(">$", "");
			this.subscribeText   = extractedText.get(11).replace(this.subscribeLink, "").replaceFirst("\\s?<>$", "");
		} else {
			// Subscribed bid:
			// Star button displays (both button image and link)
			this.starLink        = extractedText.get(11).replaceFirst("^\\[{1}(\\S*)(\\]<){1}", "").replaceFirst(">$", ""); 
			this.starImage       = extractedText.get(11).replace(this.starLink, "").replaceFirst("^\\[", "").replaceFirst("\\]<>$", "");
		}
		
		// Required Static Elements
		this.viewBidText     = extractedText.get(5).replace(this.viewBidLink, "").replaceFirst("\\s?<>$", "");
		this.bidNumberText   = extractedText.get(6).replace(this.bidNumber, "").replaceFirst("\\s?$", "");
		this.dueDateText     = extractedText.get(7).replaceFirst("\\s?(\\d{2}/\\d{2}/\\d{4}){1}$", "");
		this.daysLeftText    = extractedText.get(10);
		this.spyGlassImage   = extractedText.get(4).replaceFirst("^\\[","").replaceFirst("\\]$", "");
		this.clockImage      = extractedText.get(8).replaceFirst("^\\[","").replaceFirst("\\]$", "");
	}
*/	
	
	// Fix for release OCT 7
	/***
	 * <h1>bidNotificationRow</h1>
	 * <p>purpose: Class constructor. Automatically sets all required and optional bid row elements
	 * @param driver
	 * @param emailGenerationDate = String MM/DD/YYYY (ex: 02/02/2019) is the generated email date, from bottom of notification
	 * @param extractedText = List <String> with 12 elements, corresponding to expected text for bid notification row<br>
	 * 	Note: Text should be extracted from Mailinator email, set in the "text/html" view
	 */
	public bidNotificationRow (EventFiringWebDriver driver, String emailGenerationDate, List <String> extractedText) {
		super(driver);
		dateHelpers   = new DateHelpers();
		stringHelpers = new StringHelpers();
		Assert.assertEquals("ERROR: bidNotificationRow expects extractedText with exactly 9 elements", 9, extractedText.size());
		this.emailGenerationDate = emailGenerationDate;
		
		// Required Dynamic Elements
		this.bidTitle        = extractedText.get(0).replaceFirst("<\\s*\\S*\\d*\\D*>$", "");
		this.bidTitleLink    = extractedText.get(0).replace(this.bidTitle, "").replaceFirst("^<", "").replaceFirst(">$", "");
		this.bidLocation     = extractedText.get(1).replaceFirst("^\\[", "").replaceFirst("\\]$", "");
		this.bidDescription  = extractedText.get(2).replaceFirst("\\s?(Read More){1}<\\s*\\S*\\d*\\D*>$", "").replaceFirst("^\\[", "").replaceFirst("\\]$", "");
		this.readMoreLink    = extractedText.get(2).replace(this.bidDescription, "").replaceFirst("\\s?(\\.\\.\\.)?\\s?(Read More){1}<","").replaceFirst(">$", "").replaceFirst("^\\[", "").replaceFirst("\\]$", "");
		this.viewBidLink     = extractedText.get(4).replaceFirst("^" + this.getExpectedViewBidText()   + "\\s?<", "").replaceFirst(">$", "");
		this.bidNumber       = extractedText.get(5).replaceFirst("^" + this.getExpectedBidNumberText() + "\\s?", "");
		this.dueDate         = extractedText.get(6).replaceFirst("^" + this.getExpectedDueDateText() + "\\s?", "");
		this.daysLeft        = Integer.parseInt(extractedText.get(7));
		
		// Required Static Elements
		this.viewBidText     = extractedText.get(4).replace(this.viewBidLink, "").replaceFirst("\\s?<>$", "");
		this.bidNumberText   = extractedText.get(5).replace(this.bidNumber, "").replaceFirst("\\s?$", "");
		this.dueDateText     = extractedText.get(6).replaceFirst("\\s?(\\d{2}/\\d{2}/\\d{4}){1}$", "");
		this.daysLeftText    = extractedText.get(8);
		this.spyGlassImage   = extractedText.get(3).replaceFirst("^\\[","").replaceFirst("\\]$", "");
	}
	
	/* ----- getters ----- */
	// Required Dynamic Elements
	public String   getMatchIndicator()     { return this.matchIndicator; }
	public String   getBidTitle()           { return this.bidTitle;       }
	public String   getBidTitleLink()       { return this.bidTitleLink;   }
	public String   getBidLocation()        { return this.bidLocation;    }
	public String   getBidDescription()     { return this.bidDescription; }
	public String   getReadMoreLink()       { return this.readMoreLink;   }
	public String   getViewBidLink()        { return this.viewBidLink;    }
	public String   getBidNumber()          { return this.bidNumber;      }
	public String   getDueDate()            { return this.dueDate;        }
	public int      getDaysLeft()           { return this.daysLeft;       }

	// Optional Dynamic Elements
	private String  getSubscribeLink()      { return this.subscribeLink;  }
	private String  getSubscribeText()      { return this.subscribeText;  }
	private String  getStarLink()           { return this.starLink;       }
	private String  getStarImage()          { return this.starImage;      }
	
	// Static Required Elements
	private String  getViewBidText()        { return this.viewBidText;    }
	private String  getBidNumberText()      { return this.bidNumberText;  }
	private String  getDueDateText()        { return this.dueDateText;    }
	private String  getDaysLeftText()       { return this.daysLeftText;   }
	private String  getSpyGlassImage()      { return this.spyGlassImage;  }
	private String  getClockImage()         { return this.clockImage;     }
	
	// Other
	private String  getEmailGenerationDate()         { return this.emailGenerationDate;      }
	
	// Configurable Values
	private String     getExpectedDomain()           { return this.expectedDomain;           }

	protected String   getExpectedClockImage()       { return this.expectedClockImage;       }
	protected String   getExpectedSpyGlassImage()    { return this.expectedSpyGlassImage;    }
	protected String   getExpectedStarImage()        { return this.expectedStarImage;        }
	protected String[] getExpectedBlurTextImage()    { return this.expectedBlurImage;        }
	protected String[] getExpectedMatchScoreImage()  { return this.expectedMatchScoreImage;  }

	private String     getExpectedViewBidText()      { return this.expectedViewBidText;      }
	private String     getExpectedBidNumberText()    { return this.expectedBidNumberText;    }
	private String     getExpectedDueDateText()      { return this.expectedDueDateText;      }
	private String     getExpectedDaysLeftText()     { return this.expectedDaysLeftText;     }
	
	/* ----- helpers ----- */
	
	/***
	 * <h1>getDueDateAsCalendar</h1>
	 * @return due date as Calendar object
	 */
	private Calendar getDueDateAsCalendar() { return dateHelpers.getStringDateAsCalendar(this.getDueDate().trim()); }
	
	// Link Matchers
	/***
	 * <h1>matchManageSubscriptionsLink</h1>
	 * <p>purpose: Pattern matcher for Soft Assertions to verify link matches manage-subscriptions link
	 * <p>manage-subscriptions link (or iterations of): [supplierBaseURL]admin/manage-subscriptions?email=Y 
	 * @return Pattern
	 */
	private Pattern matchManageSubscriptionsLink() {
		// manage-subscriptions link ex: https://app.bidsync.com/admin/manage-subscriptions?email=Y 
		String domain = stringHelpers.formatStringForPattern(this.getExpectedDomain());
		String manageSubscriptionLink = String.format("^%sadmin/manage-subscriptions(\\?email=Y)?$", domain);
		return Pattern.compile(manageSubscriptionLink);
	}
	
	/***
	 * <h1>matchBidLink</h1>
	 * <p>purpose: Pattern matcher for Soft Assertions to verify bid link matches expected form
	 * <p>bid link form (or iterations of): [supplierBaseURL]bid-detail/[bidID]?email=Y
	 * @return Pattern
	 */
	private Pattern matchBidLink() {
		// bid link ex: https://app.bidsync.com/bid-detail/f89ef0ee-f5d4-4ee4-845c-cdb63fe2e2db?email=Y
		String bidId   = "[a-z0-9]{8}(\\-[a-z0-9]{4}){3}\\-[a-z0-9]{12}";
		String domain  = stringHelpers.formatStringForPattern(this.getExpectedDomain());
		String bidLink = String.format("^(%sbid-detail/){1}%s(\\?email=Y)?$", domain, bidId);
		return Pattern.compile(bidLink);
	}
	
	/***
	 * <h1>matchStarLink</h1>
	 * <p>purpose: Pattern matcher for Soft Assertions to verify star link matches expected form
	 * <p>star link form (or iterations of): [supplierBaseURL]bid-detail/[bidID]?save=true
	 * @return Pattern
	 */
	private Pattern matchStarLink() {
		// star link ex: https://app.bidsync.com/bid-detail/72f4801b-4692-4a60-9270-3817b8bb0307?save=true
		String bidId    = "[a-z0-9]{8}(\\-[a-z0-9]{4}){3}\\-[a-z0-9]{12}";
		String domain   = stringHelpers.formatStringForPattern(this.getExpectedDomain());
		String starLink = String.format("^(%sbid-detail/){1}%s(\\?save=true){1}$", domain, bidId);
		return Pattern.compile(starLink);
	}

	/***
	 * <h1>matchMatchScore</h1>
	 * <p>purpose: Pattern matcher for Soft Assertions to verify match score matches expected form
	 * <p>match score form: [matchScore] match
	 * @return Pattern
	 */
	private Pattern matchMatchScore() {
		// match ex: best match
		return Pattern.compile("^(best|good|possible|unlikely|poor){1}\\s{1}match{1}$");
	}
	
	/* ----- methods ----- */
	
	/***
	 * <h1>printRow</h1>
	 * <p>purpose: Print info about this bid notification row, including:<br>
	 *  1.  Match Indicator Score<br>
	 *  2.  Bid Title<br>
	 *  3.  Bid Title link  (or manage-subscriptions link if obfuscated bid)<br>
	 *  4.  Bid Location    (or blurred link if obfuscated bid)<br>
	 *  5.  Bid Description (or blurred link if obfuscated bid)<br>
	 *  6.  Read More Link  (or blank if obfuscated bid)<br>
	 *  7.  View Bid Link   (or manage-subscription link if obfuscated bid)<br>
	 *  8.  Bid Number      (this can be blank)<br>
	 *  9.  Due Date<br>
	 *  10. Days Left<br>
	 *  If Subscribed:<br>
	 *  11. Star Link<br>
	 *  12. Star Button image<br>
	 * @return bidNotificationRow
	 */
	public bidNotificationRow printRow() {
		System.out.println(GlobalVariables.getTestID() + " --------- Bid Notification ------------");
		System.out.println(GlobalVariables.getTestID() + " Match Indicator: " + this.getMatchIndicator());
		System.out.println(GlobalVariables.getTestID() + " Bid Title:       " + this.getBidTitle());
		System.out.println(GlobalVariables.getTestID() + " Bid Title Link:  " + this.getBidTitleLink());
		System.out.println(GlobalVariables.getTestID() + " Bid Location:    " + this.getBidLocation());
		System.out.println(GlobalVariables.getTestID() + " Bid Description: " + this.getBidDescription());
		System.out.println(GlobalVariables.getTestID() + " Read More Link:  " + this.getReadMoreLink());
		System.out.println(GlobalVariables.getTestID() + " View Bid Link:   " + this.getViewBidLink());
		System.out.println(GlobalVariables.getTestID() + " Bid Number:      " + this.getBidNumber());
		System.out.println(GlobalVariables.getTestID() + " Due Date:        " + this.getDueDate());
		System.out.println(GlobalVariables.getTestID() + " Days Left:       " + this.getDaysLeft());

		// For Subscribed bids:
		if (this.getStarImage()!= null) {
		System.out.println(GlobalVariables.getTestID() + " Star Link:       " + this.getStarLink());
		System.out.println(GlobalVariables.getTestID() + " Star Image:      " + this.getStarImage());
		}
		return this;
	}
	
	/* ----- verifications ----- */

	/***
	 * <h1>verifySubscribed</h1>
	 * <p>purpose: Verify that the bid notification row is displayed per:<br>
	 *  1. Bid Title link should link to an actual bid<br>
	 *  2. Read More link should link to an actual bid<br>
	 *  3. View Bid link should link to an actual bid<br>
	 *  4. View Bid link, Read More link, Bid Title link should link to the same bid<br>
	 *  5. Bid Description is displaying the blurred image, and shows text<br>
	 *  6. Star button image and link are displayed<br>
	 *  7. Star button link is for the same bid as Bid Title bid<br>
	 *  8. Subscribe button and text are not displayed<br>
	 * 	Fail test if any of the above are not met</p>
	 * @return bidNotificationsRow
	 */	
	public bidNotificationRow verifySubscribed() {
		SoftAssertions softly = new SoftAssertions();

		// Bid Title Link should link to the actual document
		// Read More Link should link to actual document
		// View Bid Link should link to actual document
		softly.assertThat(this.getBidTitleLink())  .withFailMessage("Bid [%s]: Expected Bid Title to link to bid, but got <%s>", this.getBidTitle(), this.getBidTitleLink()).matches(this.matchBidLink());
		softly.assertThat(this.getReadMoreLink())  .withFailMessage("Bid [%s]: Expected Read More to link to bid, but got <%s>", this.getBidTitle(), this.getReadMoreLink()).matches(this.matchBidLink());
		softly.assertThat(this.getViewBidLink())   .withFailMessage("Bid [%s]: Expected View Bid  to link to bid, but got <%s>", this.getBidTitle(), this.getViewBidLink()) .matches(this.matchBidLink());

		// Bid Title Link == Read More Link == View Bid Link
		String errMsg = String.format("Bid [%s]: Bid Link is inconsistent. Bid Title <%s>, Read More <%s>, View Bid<%s>", this.getBidTitle(), this.getBidTitleLink(), this.getReadMoreLink(), this.getViewBidLink());
		softly.assertThat(this.getBidTitleLink())  .withFailMessage(errMsg).isEqualTo(this.getReadMoreLink()).isEqualTo(this.getViewBidLink());

		// Bid Description is not blurred
		softly.assertThat(this.getBidDescription()).withFailMessage("Bid [%s]: Expected Bid Description to contain text but, got <%s>", this.getBidTitle(), this.getBidDescription()).isNotIn((Object[])this.getExpectedBlurTextImage()).isNotEmpty();

		// Star Button image is displayed 
		// Star Button link is of correct form
		// Star Button link to bid is for the same bid linked to by the Bid Title Link
		softly.assertThat(this.getStarImage())     .withFailMessage("Bid [%s]: Expected Star Button to display, but got image path <%s>",   this.getBidTitle(), this.getStarImage()).isEqualTo(this.getExpectedStarImage());
		softly.assertThat(this.getStarLink())      .withFailMessage("Bid [%s]: Expected Star Link to display correct form, but got <%s>",   this.getBidTitle(), this.getStarLink()) .matches(this.matchStarLink());
		if (this.getStarLink() != null) {
		softly.assertThat(this.getStarLink().replaceFirst("(\\?save=true){1}$", "")).withFailMessage("Bid [%s]: Expected Star Link to link to Bid Title <%s>, but got <%s>", this.getBidTitle(), this.getBidTitleLink(), this.getStarLink()).isEqualTo(this.getBidTitleLink().replaceFirst("(\\?email=Y)?$", ""));
		}


		// Subscribe Button link is not displayed
		// Subscribe Button text is not displayed
		softly.assertThat(this.getSubscribeLink()) .withFailMessage("Bid [%s]: Expected SUBSCRIBE button to not be visible, but instead it is displaying <%s>", this.getBidTitle(), this.getSubscribeLink()).isNullOrEmpty();
		softly.assertThat(this.getSubscribeText()) .withFailMessage("Bid [%s]: Expected SUBSCRIBE button to not be visible, but instead it is displaying <%s>", this.getBidTitle(), this.getSubscribeText()).isNullOrEmpty();

		softly.assertAll();
		return this;
	}

	/***
	 * <h1>verifyNotSubscribed</h1>
	 * <p>purpose: Verify that the bid notification row is obfuscated per:<br>
	 * 	1. Bid Title links to manage-subscriptions<br>
	 *	2. View Bid links to manage-subscriptions<br>
	 * 	3. Read More link does not display (is blank)<br>
	 * 	4. Bid Description is blurred<br>
	 * 	5. Bid Location is blurred<br>
	 * 	6. Star button does not display<br>
	 * 	7. Subscribe button is displaying and links to manage-subscriptions<br>
	 * 	Fail test if any of the above are not met</p>
	 * @return bidNotificationsRow
	 */
	public bidNotificationRow verifyNotSubscribed() {
		SoftAssertions softly = new SoftAssertions();

		// Bid Title Link should link to manage-subscriptions
		// View Bid Link should link to manage-subscriptions
		// Subscribe Button should link to manage-subscriptions
		softly.assertThat(this.getBidTitleLink())  .withFailMessage("Bid [%s]: Expected Bid Title link to navigate to manage subscriptions but, got <%s>", this.getBidTitle(), this.getBidTitleLink())   .matches(this.matchManageSubscriptionsLink());
		softly.assertThat(this.getViewBidLink())   .withFailMessage("Bid [%s]: Expected View More link to navigate to manage subscriptions but, got <%s>", this.getBidTitle(), this.getViewBidLink())    .matches(this.matchManageSubscriptionsLink());
		softly.assertThat(this.getSubscribeLink()) .withFailMessage("Bid [%s]: Expected Subscribe link to navigate to manage subscriptions but, got <%s>", this.getBidTitle(), this.getSubscribeLink())  .matches(this.matchManageSubscriptionsLink());

		// Read More Link should be blank
		softly.assertThat(this.getReadMoreLink())  .withFailMessage("Bid [%s]: Expected Read More link to not be visible, but instead got <%s>",           this.getBidTitle(), this.getReadMoreLink())   .isNullOrEmpty();

		// Bid Description is blurred
		// Bid Location is blurred
		softly.assertThat(this.getBidDescription()).withFailMessage("Bid [%s]: Expected Bid Description to display blurred image but, got <%s>",           this.getBidTitle(), this.getBidDescription()) .isIn((Object[])this.getExpectedBlurTextImage());
		softly.assertThat(this.getBidLocation())   .withFailMessage("Bid [%s]: Expected Bid Location to display blurred image but, got <%s>",              this.getBidTitle(), this.getBidLocation())    .isIn((Object[])this.getExpectedBlurTextImage());
		
		// Star Button is not displayed
		softly.assertThat(this.getStarLink())      .withFailMessage("Bid [%s]: Expected Star image to not be visible, but instead it is displaying <%s>",  this.getBidTitle(), this.getStarLink())       .isNullOrEmpty();
		softly.assertThat(this.getStarImage())     .withFailMessage("Bid [%s]: Expected Star image to not be visible, but instead it is displaying <%s>",  this.getBidTitle(), this.getStarImage())      .isNullOrEmpty();

		// Subscribe button is displayed with text "SUBSCRIBE"
		softly.assertThat(this.getSubscribeText()) .withFailMessage("Bid [%s]: Expected Subscribe button to display \"SUBSCRIBE\" text, but got <%s>",     this.getBidTitle(), this.getSubscribeText())  .isEqualTo("SUBSCRIBE");
		
		softly.assertAll();
		return this;
	}
	
	/***
	 * <h1>verifyGenericNotification</h1>
	 * <p>purpose: Verify that generic objects are displayed for a bid (regardless of
	 * whether the bid is in the user's subscription or not). Verify per:<br>
	 * 1. Match Indicator is of correct form<br>
	 * 2. Bid Title contains some text, with no more than 300 chars long, and if 300 chars, should end with<br>
	 * 3. "Bid #:" text is displayed before the bid number<br>
	 * 4. "View Bid" text is displayed<br>
	 * 5. Spy Glass image is displayed<br>
	 * 6. Clock image is displayed<br>
	 * 7. "Due:" text is displayed before the bid due date<br>
	 * 8. Due date should be a date, and should not be expired (i.e. min expiration date is generation date printed at bottom of notification email)<br>
	 *    As a sanity check, due date should be no more than six months from email generation date<br>
	 * 9. "Days Left" text should display<br>
	 * 10. Days Left value is correct<br>
	 * Fail test if any of the above conditions are not met<br>
	 * TODO: In progress
	 * @return bid NotificationRow
	 */
	public bidNotificationRow verifyGenericNotification() {
		
		SoftAssertions softly = new SoftAssertions();

		// 1. Required Dynamic Objects are of correct form:
		// Match Indicator should be of correct form
		softly.assertThat(this.getMatchIndicator()).withFailMessage("Bid [%s]: Expected match score in correct form, but got <%s>", this.getBidTitle(), this.getMatchIndicator()).matches(this.matchMatchScore());

		// Bid Title contains some text, and should be no more than 300 chars long (will be 303 with the "...") and if 300 chars, should end with "..."
		softly.assertThat(this.getBidTitle().length()).withFailMessage("Bid [%s]: Expected Bid Title < 300 chars, but got", this.getBidTitle(), this.getBidTitle().length()).isBetween(1, 303);
		if (this.getBidTitle().length() == 303) {
			softly.assertThat(this.getBidTitle()).withFailMessage("Bid [%s]: Expected 300 char long Bid Title to end in \"...\", but got <%s>", this.getBidTitle(), this.getBidTitle()).endsWith("...");
		}

		// Due Date should be a date, and should not be expired (i.e. expiration date be day of send or later)
		// As a sanity check, Due Date should be no more than six months from email generation date
		Calendar earliestDateCalendar      = dateHelpers.getStringDateAsCalendar(this.getEmailGenerationDate());
		Calendar sixMonthsLaterCalendar    = dateHelpers.addNumberOfMonthsToCalendarDate(earliestDateCalendar, +6);
		String   sixMonthsLater            = dateHelpers.getCalendarDateAsString(sixMonthsLaterCalendar);
		softly.assertThat(this.getDueDateAsCalendar())
			.withFailMessage("Bid[%s]: Expected due date between [generated date, +6 months] = [<%s>, <%s>], but got <%s>", this.getBidTitle(), this.getEmailGenerationDate(), sixMonthsLater, this.getDueDate())
			.isGreaterThanOrEqualTo(earliestDateCalendar)
			.isLessThanOrEqualTo(sixMonthsLaterCalendar);

		// Days Left is correct
		// We're allowing a one day fudge here since we don't have the exact UTC time
		long diff = Math.abs(this.getDueDateAsCalendar().getTimeInMillis() - earliestDateCalendar.getTimeInMillis());
		final int ONE_DAY = 1000 * 60 * 60 * 24;
		long calculatedDaysRemaining = diff/ONE_DAY;
		
		softly.assertThat(this.getDaysLeft()).withFailMessage("Bid[%s]: Expected days left of <%s>, but calculated value from generated date <%s> to due date <%s> is <%s> days", this.getBidTitle(), this.getDaysLeft(), this.getEmailGenerationDate(), this.getDueDate(), calculatedDaysRemaining)
			.isBetween((int)calculatedDaysRemaining, (int)calculatedDaysRemaining + 1);
			
		
		// 2. Required Static Images display:
		// Spy Glass image is displayed
		// Clock image is displayed
		softly.assertThat(this.getSpyGlassImage()).withFailMessage("Bid [%s]: Expected spy glass image, but got <%s>", this.getBidTitle(), this.getSpyGlassImage()).isEqualTo(this.getExpectedSpyGlassImage());
		softly.assertThat(this.getClockImage())   .withFailMessage("Bid [%s]: Expected clock image, but got <%s>",     this.getBidTitle(), this.getClockImage())   .isEqualTo(this.getExpectedClockImage());

		// 3. Required Static Text displays:
		// "Bid #:" text is displayed (actual bid number is optional)
		// "View Bid" text is displayed for viewBidLink
		// "Due:" text is displayed before bid due date
		// "Days Left" text is displayed
		String errMsg = "Bid [%s]: Expected static text \"%s\" to display, but got <%s>";
		softly.assertThat(this.getBidNumberText()).withFailMessage(errMsg, this.getBidTitle(), this.getExpectedBidNumberText(), this.getBidNumberText()).isEqualTo(this.getExpectedBidNumberText());
		softly.assertThat(this.getViewBidText())  .withFailMessage(errMsg, this.getBidTitle(), this.getExpectedViewBidText(),   this.getViewBidText())  .isEqualTo(this.getExpectedViewBidText());
		softly.assertThat(this.getDueDateText())  .withFailMessage(errMsg, this.getBidTitle(), this.getExpectedDueDateText(),   this.getDueDateText())  .isEqualTo(this.getExpectedDueDateText());
		softly.assertThat(this.getDaysLeftText()) .withFailMessage(errMsg, this.getBidTitle(), this.getExpectedDaysLeftText(),  this.getDaysLeftText()) .isEqualTo(this.getExpectedDaysLeftText());

		softly.assertAll();
		return this;
	}
}
