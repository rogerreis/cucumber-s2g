package pages.bidSyncPro.supplier.account.myInfo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.bidSyncPro.supplier.account.AccountCommon;
import pages.common.helpers.StringHelpers;

/***
 * <h1>Class MyInfo</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects on the My Info tab (admin/my-profile)</p>
 */
public class MyInfo extends AccountCommon {
    
    /*----- xpaths -----*/
	// Labels
	private final String profileInformationLabel_xpath  = "//mat-card-title[@id='matMyProfileTitle']//span[contains(., 'Profile Information')]";
	private final String emailLabel_xpath               = "//strong[contains(text(), 'Email')]";
	private final String phoneLabel_xpath               = "//strong[contains(text(), 'Phone')]";
	private final String companyNameLabel_xpath         = "//strong[contains(text(), 'Company Name')]";
	private final String faxLabel_xpath                 = "//strong[contains(text(), 'Fax')]";
	private final String addressLabel_xpath             = "//strong[contains(text(), 'Address')]";
	private final String localTimeLabel_xpath           = "//strong[contains(text(), 'Local Time')]";
	
	// Buttons
    private final String updateProfileButton_xpath = "//button[contains(span, 'mode_edit')]";

	// User Info
    private final String firstName_xpath           = "//span[@id='firstName']";
    private final String lastName_xpath            = "//span[@id='lastName']";
    private final String jobTitle_xpath            = "//div[@id='jobTitle']";
    private final String email_xpath               = "//span[@id='email']";
    private final String phone_xpath               = "//span[@id='phoneNumber']";
    private final String phoneExt_xpath            = "//span[@id='phoneExt']";
    private final String companyName_xpath         = "//span[@id='companyName']";
    private final String fax_xpath                 = "//span[@id='fax']";
    private final String faxExt_xpath              = "//span[@id='faxExt']";
    private final String addressLine1_xpath        = "//span[@id='addressLine1']";
    private final String addressLine2_xpath        = "//span[@id='addressLine2']";
    private final String city_xpath                = "//span[@id='city']";
    private final String state_xpath               = "//span[@id='state']";
    private final String zip_xpath                 = "//span[@id='zip']";
    private final String localTime_xpath           = "//span[@id='localTime']";
    
    // Other
    private StringHelpers stringHelpers;
    
    public MyInfo(EventFiringWebDriver driver) {
        super(driver);
        stringHelpers = new StringHelpers();
    }
    
    /*----- getters -----*/
    
    // Labels
    private WebElement getProfileInformationLabel() { return findByVisibility(By.xpath(this.profileInformationLabel_xpath)); }
    private WebElement getEmailLabel()              { return findByVisibility(By.xpath(this.emailLabel_xpath));              }
    private WebElement getPhoneLabel()              { return findByVisibility(By.xpath(this.phoneLabel_xpath));              }
    private WebElement getCompanyNameLabel()        { return findByVisibility(By.xpath(this.companyNameLabel_xpath));        }
    private WebElement getFaxLabel()                { return findByVisibility(By.xpath(this.faxLabel_xpath));                }
    private WebElement getAddressLabel()            { return findByVisibility(By.xpath(this.addressLabel_xpath));            }
    private WebElement getLocalTimeLabel()          { return findByVisibility(By.xpath(this.localTimeLabel_xpath));          }

    // Buttons
    private WebElement getUpdateProfileButton() { return findByVisibility(By.xpath(this.updateProfileButton_xpath)); }
    
    // User Info
    private String getFirstName()    { return findByVisibility(By.xpath(this.firstName_xpath)).getText();    }
    private String getLastName()     { return findByVisibility(By.xpath(this.lastName_xpath)).getText();     }
    private String getJobTitle()     { return findByVisibility(By.xpath(this.jobTitle_xpath)).getText();     }
    private String getEmail()        { return findByVisibility(By.xpath(this.email_xpath)).getText();        }
    private String getPhone()        { return findByVisibility(By.xpath(this.phone_xpath)).getText();        }
    private String getPhoneExt()     { return findByVisibility(By.xpath(this.phoneExt_xpath)).getText();     }
    private String getCompanyName()  { return findByVisibility(By.xpath(this.companyName_xpath)).getText();  }
    private String getFax()          { return findByVisibility(By.xpath(this.fax_xpath)).getText();          }
    private String getFaxExt()       { return findByVisibility(By.xpath(this.faxExt_xpath)).getText();       }
    private String getAddressLine1() { return findByVisibility(By.xpath(this.addressLine1_xpath)).getText(); }
    private String getAddressLine2() { return findByVisibility(By.xpath(this.addressLine2_xpath)).getText(); }
    private String getCity()         { return findByVisibility(By.xpath(this.city_xpath)).getText();         }
    private String getState()        { return findByVisibility(By.xpath(this.state_xpath)).getText();        }
    private String getZip()          { return findByVisibility(By.xpath(this.zip_xpath)).getText();          }
    private String getLocalTime()    { return findByVisibility(By.xpath(this.localTime_xpath)).getText();    }
    
    /*----- methods -----*/
    
    /***
     * <h1>openUpdateProfileDialog</h1>
     * <p>purpose: Click on the "edit" button to view the update profile dialog</p>
     * @return UpdateProfileDialog
     */
    public UpdateProfileDialog openUpdateProfileDialog() {
        getUpdateProfileButton().click();
        return new UpdateProfileDialog(driver);
    }
    
    /***
     * <h1>getCurrentProfileElement</h1>
     * <p>purpose: Return a set of all the current profile elements on the My Profile tab.<br>
     * 	These items will be formatted to match inputs into the DataTable</p>
     * @return ProfileElements
     */
    public ProfileElements getCurrentProfileElements() {
    	ProfileElements currentScreen = new ProfileElements();
		currentScreen.setFirstName   (this.getFirstName().trim());
		currentScreen.setLastName    (this.getLastName().trim());
		currentScreen.setPhoneNumber (stringHelpers.removeWhitespaceAndSpecialCharsFromString(this.getPhone()).trim());
		currentScreen.setPhoneExt    (this.getPhoneExt().replaceAll("Ext ", "").trim());
		currentScreen.setFaxNumber   (stringHelpers.removeWhitespaceAndSpecialCharsFromString(this.getFax()).trim());
		currentScreen.setFaxExt      (this.getFaxExt().replace("Ext ", "").trim());
		currentScreen.setAddressLine1(this.getAddressLine1().trim());
		currentScreen.setAddressLine2(this.getAddressLine2().trim());
		currentScreen.setJobTitle    (this.getJobTitle().trim());
		currentScreen.setCity        (this.getCity().replace(",", "").trim());
		currentScreen.setState       (this.getState().trim());
		currentScreen.setZip         (this.getZip().trim());
		currentScreen.setCompanyName (this.getCompanyName().trim());
		
		return currentScreen;
    }
    
    /***
     * <h1>verifyPageInitialization</h1>
     * <p>purpose: Verify that each element on the "Profile Information" is has initialized</p>
     * @return MyInfo
     */
    public MyInfo verifyPageInitialization() {
    	
    	// Labels
    	this.getProfileInformationLabel();
    	this.getEmailLabel();
    	this.getPhoneLabel();
    	this.getCompanyNameLabel();
    	this.getFaxLabel();
    	this.getAddressLabel();
    	this.getLocalTimeLabel();
    	
    	// Buttons
    	this.getUpdateProfileButton();
    	
    	// User Info
    	this.getEmail();
    	this.getPhone();
    	this.getPhoneExt();
    	this.getCompanyName();
    	this.getFax();
    	this.getFaxExt();
    	this.getAddressLine1();
    	this.getAddressLine2();
    	this.getLocalTime();

    	return this;
    }
    
    public MyInfo verifyCompanyName(RandomChargebeeUser user) {
    	new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Mismatch in company name")
    		.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(companyName_xpath), user.getCompany().getName()));
    	return this;
    }
}