package pages.chargebeeSite.subscriptions.subscriptionDetails;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.springframework.util.Assert;

import data.bidSyncPro.registration.RandomChargebeeUser;
import data.bidSyncPro.registration.SubscriptionPlan;
import pages.chargebeeSite.common.CommonNavSliderMenu;
import pages.chargebeeSite.subscriptions.subscriptionDetails.cancelSubscriptionFlow.CancelSubscriptionImmediatelyPopup;
import pages.chargebeeSite.subscriptions.subscriptionDetails.cancelSubscriptionFlow.CancelSubscriptionPopup;
import pages.chargebeeSite.subscriptions.subscriptionDetails.changeSubscriptionFlow.ChangeSubscription;
import pages.common.helpers.HelperMethods;

/***
 * <h1>SubscriptionDetails</h1>
 * @author dmidura
 * <p>details: This class houses methods and PageElements for objects on the Subscriptions->Subscription Details page<br>
 * 			   on the Chargebee test site. This class will replace the old ChargebeeSubscriptionsDetails class</p>
 */
public class SubscriptionDetails extends CommonNavSliderMenu {
	
	// xpaths -- add as needed
	
	// Subscription Info
	private final String planName_xpath               = "//a[@id='plans.details']";
	private final String genericAddOns_xpath          = "//dl[@id='subscription_info']/dd[5]/a";
	
	// Right Side Buttons
	private final String changeSubscriptionBtn_xpath  = "//ul[@class='cb-sidebar-action-list']//a[@id='change_sub_link']";
	private final String cancelSubscriptionBtn_xpath  = "//ul[@class='cb-sidebar-action-list']//span[@id='cancel_subscription'][contains(text(),'Cancel Subscription')]";
	private final String cancelImmediatelyBtn_xpath   = "//ul[@class='cb-sidebar-action-list']//span[@id='cancel_subscription'][contains(text(),'Cancel Immediately')]";
	

	//Other 
	private EventFiringWebDriver driver;

	public SubscriptionDetails (EventFiringWebDriver driver) {
		super(driver);
		this.driver 		= driver;
	}
	
	/* ----- getters ----- */
	// Subscription Info
	private String getPlanName()              {return findByVisibility(By.xpath(this.planName_xpath)).getText();                         }

	// Right Side Buttons
	private WebElement getChangeSubscriptionBtn() {return findByVisibility(By.xpath(this.changeSubscriptionBtn_xpath));                  }
	private WebElement getCancelSubscriptionBtn() {return findByScrollIntoViewBottomOfScreen(By.xpath(this.cancelSubscriptionBtn_xpath));}
	private WebElement getCancelImmediatelyBtn()  {return findByScrollIntoViewBottomOfScreen(By.xpath(this.cancelImmediatelyBtn_xpath)); }
	
	/* ----- helpers ----- */
	
	/***
	 * <h1>getAllAddOns</h1>
	 * <p>purpose: Return a list of all the addOns, as displayed in Chargebee.<br>
	 * 	Note: Format is a little strange<br>
	 *  Ex returned List:<br>
	 * Military Bids - State Plan <br>
	 * Canada Bids - State Plan <br>
	 * Federal Bids - State Plan <br>
	 * Additional States <br>
	 * Additional Users - State Plan </p>
	 * @return List <String> displaying Chargebee text of all addOns for user subscription
	 */
	private List <String> getAllAddOns(){
		List <String> ret = new ArrayList<>();
		
		int numAddOns = this.findAllOrNoElements(driver, By.xpath(this.genericAddOns_xpath)).size();
		
		for(int i=1; i < numAddOns +1; i++) {
			String xpath = String.format(this.genericAddOns_xpath + "[%s]", i);
			ret.add(findByVisibility(By.xpath(xpath)).getText());
		}
		
		return ret;
	}

	/* ----- methods ----- */
	
	/***
	 * <h1>clickOnChangeSubscriptionBtn</h1>
	 * <p>purpose: Click on the "Change Subscription" button on the<br>
	 * 	Subscription->Subscription Details screen<br>
	 * 	("Change Subscription" button is on the right side panel)</p> 
	 * @return Change
	 */
	public ChangeSubscription clickOnChangeSubscriptionBtn() {
		new HelperMethods().addSystemWait(2);
		this.getChangeSubscriptionBtn().click();
		return new ChangeSubscription(driver);
	}

	/***
	 * <h1>clickOnCancelSubscriptionBtn</h1>
	 * <p>purpose: Click on the "Cancel Subscription" button on the<br>
	 * 	Subscription->Subscription Details screen<br>
	 * 	("Cancel Subscription" button is on the right side panel)</p> 
	 * <p>Note: In order to "Cancel Immediately", the subscription<br>
	 * 	must first be scheduled to cancel. To schedule a cancel,<br>
	 * 	click on "Cancel Subscription" and then on the Cancel Subscription popup<br>
	 * 	click on "Schedule Cancelation". After the Cancel Subscription popup dismisses, a<br>
	 * 	a "Cancel Immediately button will become available on the Subscription Details</p>
	 * @return CancelSubscriptionPopup
	 */
	public CancelSubscriptionPopup clickOnCancelSubscriptionBtn() {
		this.getCancelSubscriptionBtn().click();
		return new CancelSubscriptionPopup(driver);
	}
	
	/***
	 * <h1>clickOnCancelImmediatelyBtn</h1>
	 * <p>purpose: Click on the "Cancel Immediately" button on the<br>
	 * 	Subscription->Subscription Details screen<br>
	 * 	("Cancel Immediately" button is on the right side panel)</p> 
	 * <p>Note: In order to "Cancel Immediately", the subscription<br>
	 * 	must first be scheduled to cancel. To schedule a cancel,<br>
	 * 	click on "Cancel Subscription" and then on the Cancel Subscription popup<br>
	 * 	click on "Schedule Cancelation". After the Cancel Subscription popup dismisses, a<br>
	 * 	a "Cancel Immediately button will become available on the Subscription Details</p>
	 * @return CancelSubscriptionImmediatelyPopup
	 */
	public CancelSubscriptionImmediatelyPopup clickOnCancelImmediatelyBtn() {
		this.getCancelImmediatelyBtn().click();
		return new CancelSubscriptionImmediatelyPopup(driver);
	}
	
	/* ----- verifications ----- */
	/***
	 * <h1>verifyPlanName</h1>
	 * <p>purpose: Verify that displayed subscription plan is plan<br>
	 * 	Fail test if displayed plan does not match plan
	 * @param plan = expected subscription plan
	 * @return SubscriptionDetails
	 */
	public SubscriptionDetails verifyPlanName(SubscriptionPlan plan) {
		String actualPlan   = this.getPlanName();
		String expectedPlan = plan.getChargebeeSubscriptionBackEndName();
		String errorMsg     = String.format("Test Failed: Expected [%s] subscription plan, but Chargebee displays [%s]", expectedPlan, actualPlan);
		Assert.isTrue(expectedPlan.contains(actualPlan), errorMsg);
		return this;
	}
	
	/***
	 * <h1>verifyAddOns</h1>
	 * <p>purpose: Verify that the Chargebee test site displays the user subscription addOns and Additional State addOns
	 * 	Fail test if Chargebee test site does not include all expected addOns
	 * @param user = RandomChargebeeUser that contains all user data
	 * @return SubscriptionDetails
	 */
	public SubscriptionDetails verifyAddOns(RandomChargebeeUser user) {
		// format: as displays in Chargebee
		// Military Bids - State Plan 
		// Canada Bids - State Plan
		// Federal Bids - State Plan
		// Additional States
		List <String> actualAddOns   = this.getAllAddOns();

		// Match format in order to compare
		List <String> expectedAddOns = new ArrayList<>();
		expectedAddOns.addAll(user.getSubscriptionAddOnsInChargebeeFormat());
//		if(!user.getAdditionalStateAddOns().isEmpty()) {expectedAddOns.add("Additional States");}

		String errorMsg = String.format("Error: Chargebee expected addOns <%s> != displayed addOns <%s>", expectedAddOns.toString(), actualAddOns.toString());
		Assert.isTrue(actualAddOns.containsAll(expectedAddOns), errorMsg);
		return this;
	}

	/***
	 * <h1>verifySubscriptionDetails</h1>
	 * <p>purpose: Verify the user's details display as expected. Currently,
	 * 	just verifying the subscription plan and addOns
	 * TODO: Verify license count
	 * @param user = RandomChargebeeUser that contains all user data
	 * @return SubscriptionDetails
	 */
	public SubscriptionDetails verifySubscriptionDetails(RandomChargebeeUser user) {
		this.verifyPlanName(user.getSubscriptionPlan());
		this.verifyAddOns(user);
		// TODO: Add license check here 
		return this;
	}
}
