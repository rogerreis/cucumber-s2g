@must @topBar @bidSearch_detail @level3
Feature: Dashboard - Top Bar Search Input Functions Across App - Company Settings
	This feature file verifies that the Top Bar Search input functions across the app
	BIDSYNC-39: Search Criteria clears after looking at a bid

	In all cases after user enters value into Top Bar input and searches:
	1. User is navigated to All Bids list
	2. Top Bar search term is deleted
	3. Filter Results automatically opens and displays the Top Bar search term as a positive keyword.
	   (No other filters should display in Filter Results).
	4. All Bids list displays search results per the Top Bar search term


@BIDSYNC-39
Scenario Outline: User navigates to Company Settings > menuOption. From this location, verify Top Bar search flow.
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to Company Settings > "<companySettingsOption>"
	When I run a search on the Top Bar with keyword "water"
	Then I will verify that the Top Bar search term is blank
	And I will verify the Filter Results section displays a filter with only my Top Bar search term as the keyword
	And I will verify that All Bids is both highlighted and selected, and also displays correct results per the displayed Filter Results search filter
	
	Examples: Company Settings Menu Options
	| companySettingsOption  |
	| Agency Interaction     |
	| Company Profile        |
	| Manage Subscriptions   |
	| Manage Users           |
