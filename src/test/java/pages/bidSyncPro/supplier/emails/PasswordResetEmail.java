package pages.bidSyncPro.supplier.emails;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

public class PasswordResetEmail extends GenericEmail {
	
	public HelperMethods helperMethods;
	
	// PageElements
	public Map<String, WebElement> PageElements;  	// Holds all the PageElements on the
													// Change Password email
				                                    //	form: <name of element, WebElement>
	
	// Other
    private EventFiringWebDriver driver;
	
	public PasswordResetEmail(EventFiringWebDriver driver) {
		super(driver, "PasswordResetEmail.json");
		this.driver 	= driver;
		PageElements 	= new HashMap<>();
	}
	
	/***
	 * <h1>registerPageElements</h1>
	 * <p>purpose: Register all the PageElements on the Registration Complete Email</p>
	 * @return None
	 */
	public void registerPageElements() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Registering PageElements on the Registration Complete Email");
		// dmidura: These page Elements should only be things like logos.
		// Email text is stored in the RegistrationEmail.json.
		
		// BidSync logo
		PageElements.put("BidSyncLogoImg", driver.findElement(By.xpath("//a/img[@class=\"logo\" and @src=\""+
				"https://034bb8c843edee0e2a60-5ff025f7b23adcfa6eee4cf26d636808.ssl.cf2.rackcdn.com/emails/notificaiotns/daily/june2017/bidsyncHorizLogo.png\"]")));
	}
}
