@should @BidSync_CompanyContracts @future_work @integrations @level3
Feature: Company Settings > Agency Interaction - Punchout to Classic Company Contracts Page
  #ARGO-1242
  #This item is on hold for the moment (blocked). application broken
  # dmidura: According to ARGO-1242 ticket, "Company Contracts" is phase 2
  # Turning off test until then. Test is also in need of refactor

  Scenario: Company Contracts Tab/Page Initializes All Fields as Expected
    Given I have logged into the Supplier side as user "National FMC"
    And I have navigated to Company Settings > "Agency Interaction"
    And I click on the Agency Interaction Company Contracts Tab
#    And I will verify that the Company Contracts Page has required criteria fields
#    And I will verify that the Company Contracts Page has required Results table headers
#    And I will verify that the Company Contracts Page Results table can be sorted
    And I will verify that the Company Contracts Page Search returns Results
