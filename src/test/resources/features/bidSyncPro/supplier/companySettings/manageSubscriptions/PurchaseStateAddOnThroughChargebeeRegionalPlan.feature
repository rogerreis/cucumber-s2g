@must @ignore
Feature: Company Settings > Manage Subscriptions > Purchase State AddOn through Chargebee site - Regional Plan
   BIDSYNC-510: Add On Pro Plan - Purchase Additional States
   mandrews: NOTE:  This test is now deprecated via ignore, but keeping it in case they change their mind later.
   Mark Andrews 12:24 PM
	 I want to verify my understanding of this conversation. We are now only allowing vendors to purchase additional 
	 states during sign-up. If they want to purchase additional states later, they need to contact support or something. Is that correct?
	 Henrik Engert 12:24 PM
	 Yes

   
@BIDSYNC-510
Scenario Outline: Selecting "Additional State" addOn from Chargebee test site will allow user access selected states.
	Canceling user subscription reverts user to BASIC plan, removes all addOns, and removes access to selected additional states.
	> Current workaround in place for BIDSYNC-629: No Notification State set after Cancelling Pro Account
	> Current workaround in place for BIDSYNC-406: Re-map entry for the state of Oklahoma in grouping_mapping table

	Given I register a new company owner with a "<subscriptionPlan>" subscription plan, with add-ons and some licenses
	And I logout of BidSync Pro
	When I randomly purchase Additional States addOns from the Chargebee test site
	Then I will verify that the Chargebee test site correctly displays the company owner subscription and addOns
	And I will verify that the ARGO database correctly displays the company owner subscription and addOns
	And I will verify that in BidSync Pro the company owner can view bids in their subscription and addOns
	When the Chargebee admin cancels the subscription for the company owner
	Then I will verify that the Chargebee test site correctly displays the company owner subscription and addOns
	And I will verify that the ARGO database correctly displays the company owner subscription and addOns
	And I will verify that in BidSync Pro the company owner can view bids in their subscription and addOns

	# Note: National plan doesn't need additional States. Basic plan needs to upgrade to paid plan first.	
	Examples: Subscriptions that will purchase Additional State addOn
	| subscriptionPlan           |
	| BidSync Regional - Yearly  |
	| BidSync Regional - Monthly |
	| BidSync Regional - 6 Month |
	
	
	