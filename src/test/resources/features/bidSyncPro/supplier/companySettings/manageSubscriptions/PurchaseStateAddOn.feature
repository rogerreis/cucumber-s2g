@must @subscriptions @level3
Feature: Company Settings > Manage Subscriptions > Purchase State AddOn General
   BIDSYNC-510: Add On Pro Plan - Purchase Additional States
   
@BIDSYNC-510
Scenario: Chargebee test site includes five dropdowns for "Additional States" addOn, populated with 50 USA states

	Given I have logged into the Chargebee test site as the admin user
	When I Change Subscription for the first visible subscription record in the Chargebee test site
	Then I will verify that the five Additional State dropdowns appear with populated values on the Chargeebee test site


#dmidura: Below are not required for BIDSYNC-510, but I want to add them to be thorough

@should
Scenario: Chargebee punchout includes five dropdowns for "Additional States" addOn

	Given I have logged into the Supplier side as user "Basic"
# TODO	Then I will verify that the five Additional State dropdowns appear with populated values in the Chargebee punchout

