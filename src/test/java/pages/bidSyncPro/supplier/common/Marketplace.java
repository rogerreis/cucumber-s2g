package pages.bidSyncPro.supplier.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.BasePageActions;

public class Marketplace  extends BasePageActions {

	
	private final String marketPlaceIcon_xpath = "//p[contains(text(),'Marketplace Catalog Manager')]";
	private final String updateCompanyProfileLink_xpath = "//span[contains(text(),'Update Company Profile')]";
	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;

	public Marketplace(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
		helperMethods = new HelperMethods();
	}
	 /* ----- getters ----- */
	
    public WebElement getMarketPlaceIcon()     { return findByClickability(By.xpath(this.marketPlaceIcon_xpath));     } 
    public WebElement getUpdateCompanyProfileLink()     { return findByVisibility(By.xpath(this.updateCompanyProfileLink_xpath));     } 
    
   /* ------ methods ----- */
   
    public Marketplace clickOnMarketplaceIcon(){
    	getMarketPlaceIcon().click();
    	return this;
    }
    
   /* ------ verifications ------ */
    
    public Marketplace verifyMarketPlaceRedirect(){
    	String winHandleBefore = driver.getWindowHandle();
    	helperMethods.addSystemWait(2);
    	for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
    	helperMethods.addSystemWait(2);
    	new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Error while punching out to marketplace.")
    		.until(ExpectedConditions.urlContains("/pims/catalogs"));
    	driver.close();
    	driver.switchTo().window(winHandleBefore);
    	return this;
    }
}
