package utility.encryption;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Hash's Password via BCrypt
 * https://www.mscharhag.com/software-development/bcrypt-maximum-password-length
 * Copied into this repository to help test passwords
 * @author mpasko
 *
 */
public class CustomPasswordEncoder implements PasswordEncoder {
    
    
    private final String salt = "$2a$10$A/n8fdDDwzppfLexymiyQ.";
    
    @Override
    public String encode(CharSequence rawPassword) {
        if (rawPassword.toString().startsWith(salt)) {
            return rawPassword.toString();
        }
        return BCrypt.hashpw(rawPassword.toString(), salt);
    }
    
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        String passwordToCheck = BCrypt.hashpw(rawPassword.toString(), salt);
        return passwordToCheck.equals(encodedPassword);
    }
    
}
