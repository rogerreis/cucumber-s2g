package pages.bidSyncPro.supplier.common;

import static org.junit.Assert.fail;

/***
 * <h1>enum: AccountDropdownOptions</h1>
 * @author dmidura
 * <p>details: This enum defines the dropdown options available when the top bar "Account" menu is open.<br>
 *  Dropdown options for "Account" menu are: Bid Profile, My Info, Training, Support, Account Settings, Logout</p>
 */
public enum AccountDropdownOptions {
	BID_PROFILE,
	MY_INFO,
	TRAINING,
	SUPPORT,
	ACCOUNT_SETTINGS,
	LOGOUT;
	
	/***
	 * <h1>fromString</h1>
	 * <p>purpose: Given text that matches the Account top bar menu options,<br>
	 *  return the enum value of the AccountDropdownOptions</p>
	 * @param dropDown<br>
	 * 	= "Bid Profile" | "My Info" | "Training" | "Support" | "Account Settings" | "Logout"
	 * @return AccountDropdownOptions
	 */
	public static AccountDropdownOptions fromString(String dropDown) {
		if (dropDown.equalsIgnoreCase("Bid Profile")) {
			return BID_PROFILE;
		} else if (dropDown.equalsIgnoreCase("My Info")) {
			return MY_INFO;
		} else if (dropDown.equalsIgnoreCase("Training")) {
			return AccountDropdownOptions.TRAINING;
		} else if (dropDown.equalsIgnoreCase("Support")) {
			return AccountDropdownOptions.SUPPORT;
		} else if (dropDown.equalsIgnoreCase("Account Settings")) {
			return AccountDropdownOptions.ACCOUNT_SETTINGS;
		} else if (dropDown.equalsIgnoreCase("Logout")) {
			return AccountDropdownOptions.LOGOUT;
		} else {
			fail("ERROR: Dropdown option \"" + dropDown + "\" is an invalid option to AccountDropDownOptions");
			return null;
		}
	}
	
	@Override
	public String toString() {
		if (this.equals(AccountDropdownOptions.BID_PROFILE)) {
			return "Bid Profile";
		} else if (this.equals(AccountDropdownOptions.MY_INFO)) {
			return "My Info";
		} else if (this.equals(AccountDropdownOptions.TRAINING)) {
			return "Training";
		} else if (this.equals(AccountDropdownOptions.SUPPORT)) {
			return "Support";
		} else if (this.equals(AccountDropdownOptions.ACCOUNT_SETTINGS)) {
			return "Account Settings";
		} else if (this.equals(AccountDropdownOptions.LOGOUT)) {
			return "Logout";
		} else {
			fail("ERROR: Account Dropdown value \"" + this + "\" is an invalid option to AccountDropdownOptions");
			return null;
		}
	}
	

}
