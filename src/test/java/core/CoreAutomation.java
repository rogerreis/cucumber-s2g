package core;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import pages.common.helpers.GlobalVariables;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.remote.RemoteWebDriver;

/***
 * <h1>Class CoreAutomation</h1>
 * 
 * @author dmidura
 * <p>details: This class houses methods to define the Cucumber environment and each of our site environments.<br>
 * 	Configurable values are set in config.json.</p>
 */
public class CoreAutomation {
	private CoreConfig config;
	private EventFiringWebDriver driver;

	/***
	 * <h1>CoreAutomation</h1>
	 * <p>purpose: Load environment. </p>
	 */
	public CoreAutomation() {
		// Grab config values from config.json
		config      = new CoreConfig();

		// generate unique test ID so we can trace steps through mixed logs
		String[] rand        = UUID.randomUUID().toString().split("-");
		GlobalVariables.addGlobalVariable("testID", rand[0]);

		// Configure platform URLs, set driver, set browser
		this.driver = loadEnvironment(config.getValue("localDriver"));

		// Set system properties for other third party integrations
		// that are NOT server dependent
		setBuySpeedIntegrationInfo();
		setMailinatorInfo();
		
		// Set system properties for Cucumber 
		setDefaultTimeOut();
		setMinimalDefaultTimeOut();
		setSystemLogging();
		//PrintScreenResolution();
	}

	/**
	 * <h1>config</h1>
	 * <p>purpose: Grab this session's config instance</p>
	 * @return CoreConfig for this session
	 */
	public CoreConfig config() {
		return config;
	}
	
	/***
	 * <h1>setSystemLogging</h1>
	 * <p>purpose: Based on the value of "debug" property in config.json,<br>
	 * 	turn on/off the debug mode during test run.</p>
	 */
	public void setSystemLogging() {

		// Get the debug status
		System.setProperty("debug", config.getValue("debug"));
		
		// mandrews: logging if we need it
		if (System.getProperty("debug") == null) {
			System.setProperty("debug", "false");
		}
		if (System.getProperty("debug").equals("true")) {
			EventLogger logger = new EventLogger();
			driver.register(logger);
		}
	}

	/**
	 * <h1>getDriver</h1>
	 * <p>purpose: Return the EventFiringWebDriver</p>
	 * @return the driver
	 */
	public EventFiringWebDriver getDriver() {
		return driver;
	}

	/***
	 * <h1>getOS</h1>
	 * <p>purpose: Based on the System property "os.name"<br>
	 * 	get the operating system that we're running on</p>
	 * @return "windows" | "mac" | "linux"
	 */
	public String getOS() {
		String os = System.getProperty("os.name").toLowerCase();
		String returnOS = "ERROR";

		if (os.contains("win")) {
			returnOS = "windows";
		} else if (os.contains("mac")) {
			returnOS = "mac";
		} else if (os.contains("nux")) {
			returnOS = "linux";
		}
		return returnOS;

	}

	/***
	 * <h1>loadEnvironment</h1>
	 * <p>purpose: Set browser, selenium grid (if not local), and environment URLs</p>
	 * @param targetDriver
	 * @return EventFiringWebDriver
	 */
	private EventFiringWebDriver loadEnvironment(String targetDriver) {
		EventFiringWebDriver driver = null;
		setDriverPath(getOS());

		// ENVIRONMENT SETUP

		// Are we running locally or on selenium grid?
		// env can be "local" or "grid"
		if (System.getProperty("env") == null) {
			System.setProperty("env", "local");
		}
		System.out.println(GlobalVariables.getTestID() + " Environment set to \"" + System.getProperty("env") + "\"");

		// "browser" property will be read in from mvn build line in jenkins
		// OR when running on someone's local, we'll pull the "localDriver" property from the config.txt
		// browser = "IE11" | "chrome" | "firefox"
		if (System.getProperty("browser") == null) {
			System.setProperty("browser", config.getValue("localDriver"));
		}
		System.out.println(GlobalVariables.getTestID() + " Browser set to " + System.getProperty("browser"));

		// "server" property will be read in from mvn build line in jenkins
		// OR when running on someone's local, we'll pull the "server" property from the config.txt
		// "server" = "dev" | "qa" | "prod"
		if (System.getProperty("server") == null) {

			System.setProperty("server", config.getValue("server")); 

			// If we don't have the server property defined in config.txt
			// then just default to the qa environment
			if (System.getProperty("server") == null) {
				System.setProperty("server", "qa");}
		}
		System.out.println(GlobalVariables.getTestID() + " Server set to \"" + System.getProperty("server") + "\" environment");
		
		// set platform urls based on the server we're running against
		this.setBaseUrls();
		
		// If we're using Selenium Grid
		if (System.getProperty("env").equals("grid")) {
			
			// Selenium Grid with Chrome
			if (System.getProperty("browser").equals("chrome")) {

				ChromeOptions options = new ChromeOptions();
				LoggingPreferences logPrefs = new LoggingPreferences();
			    logPrefs.enable(LogType.BROWSER, Level.WARNING);
			    //logPrefs.enable(LogType.PERFORMANCE, Level.INFO);
			    logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
				options.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
				options.addArguments("--kiosk");   // start maximized
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);

				
				WebDriver rwd = null;
				System.out.println(GlobalVariables.getTestID() + " setting up remote webdriver");
				try {
					String gridLocation = getSeleniumGridURL();
					System.out.println(GlobalVariables.getTestID() + " URL: " + gridLocation);
					rwd = new RemoteWebDriver(new URL(gridLocation), capabilities);
				} catch (IOException e) {
					e.printStackTrace();
				}
				driver = new EventFiringWebDriver(Objects.requireNonNull(rwd));
				System.out.println(GlobalVariables.getTestID() + " webdriver setup complete");

			// Selenium Grid with IE11
			} else if (System.getProperty("browser").equals("IE11")) {
				DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
				WebDriver rwd = null;
				System.out.println(GlobalVariables.getTestID() + " setting up remote webdriver");
				try {
					String gridLocation = "http://10.200.40.83:4444/wd/hub";
					System.out.println(GlobalVariables.getTestID() + " URL: " + gridLocation);
					rwd = new RemoteWebDriver(new URL(gridLocation), capabilities);
				} catch (IOException e) {
					e.printStackTrace();
				}

				driver = new EventFiringWebDriver(Objects.requireNonNull(rwd));
				System.out.println(GlobalVariables.getTestID() + " webdriver setup complete");
			}
			
		// If running locally (and not in Selenium Grid)
		} else {
			
			// Get the base page URL and set as system property
			System.setProperty("server", config.getValue("server"));
			
			switch (targetDriver.toLowerCase()) {
			case "firefox":
				DesiredCapabilities capabilities = DesiredCapabilities.firefox();
				capabilities.setCapability("marionette", true);
				driver = new EventFiringWebDriver(new FirefoxDriver(new FirefoxOptions(capabilities)));
				break;
			case "chrome":
				ChromeOptions options = new ChromeOptions();
				LoggingPreferences logPrefs = new LoggingPreferences();
			    logPrefs.enable(LogType.BROWSER, Level.WARNING);
			    //logPrefs.enable(LogType.PERFORMANCE, Level.INFO);
			    logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
				options.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
				driver = new EventFiringWebDriver(new ChromeDriver(options));
				break;
			case "ie11":
				driver = new EventFiringWebDriver(new InternetExplorerDriver());
				break;
			default:
				System.out.println(GlobalVariables.getTestID() + " ERROR: Valid driver not requested! Look at the config.json file to see what is listed.  Should be Chrome or Firefox");
			}
		}
		
		Objects.requireNonNull(driver).manage().window().maximize();

		return driver;
	}

	/***
	 * <h1>getSeleniumGridURL</h1>
	 * <p>purpose: Set up the Selenium grid, and then return URL for the instance </p>
	 * @return URL for instance of Selenium grid just created
	 * @throws IOException
	 */
	private String getSeleniumGridURL() throws IOException {
		/*
	    curl -X GET -H "Content-Type: application/json" -H "Authorization: Bearer eb2e3c4eb9c3599d20fb6f79643dd18f49aab6ad76365c00fafa50d1c0b0f4e5"
	    "https://api.digitalocean.com/v2/droplets?tag_name=oldCucumber"
		 */
		
		// 'server' is supplied by the Maven command in Jenkins and will be 'dev', 'qa', or 'stage'
		String strServer = System.getProperty("server");  
		// 'gridName' is supplied by the Maven command in Jenkins and will be some identifier for the job
		// like 'BidSyncPro' or 'reconciler'
		String strGridName = System.getProperty("gridName");
		String url          = "https://api.digitalocean.com/v2/droplets?tag_name=" + strGridName + "_" + strServer;
		String authHeader   = "Bearer 3a6f04a0905cb9fa068f4435ba784f36fe7bf44e965ed544f1cf18080665785b";
		String contentType  = "application/json";
		OkHttpClient client = new OkHttpClient.Builder()
		        .connectTimeout(20, TimeUnit.SECONDS)
		        .writeTimeout(20, TimeUnit.SECONDS)
		        .readTimeout(60, TimeUnit.SECONDS)
		        .build();
        Request request = new Request.Builder()
                .url(url)
                .addHeader("Content-Type", contentType)
                .addHeader("Authorization", authHeader)
                .build();
		Response response    = client.newCall(request).execute();
		String rawJson       = Objects.requireNonNull(response.body()).string();
		JsonElement jsonTree = new JsonParser().parse(rawJson);
		try {
	        String ipAddress = jsonTree
	                .getAsJsonObject().get("droplets")
	                .getAsJsonArray().get(0)
	                .getAsJsonObject().get("networks")
	                .getAsJsonObject().get("v4")
	                .getAsJsonArray().get(0)
	                .getAsJsonObject().get("ip_address").getAsString();
	        return "http://" + ipAddress + ":4444/wd/hub";
		}
		catch(Exception e) {  
			System.out.printf(GlobalVariables.getTestID() + " ERROR: getSeleniumGridUrl = %s\n", rawJson);
			return "problem getting grid IP address from DigitalOcean";
			}
	}

	/***
	 * <h1>setDriverPath</h1>
	 * <p>purpose:Set the driver based on the platform</p>
	 * @param os = "mac" | "windows" | "linux"
	 */
	private void setDriverPath(String os) {

		System.setProperty("webdriver.firefox.logfile", "drivers/logs/firefox/fire.log");
		switch (os) {
		case "mac":
			System.setProperty("webdriver.chrome.driver", config.getValue("chromeDriverLocation_mac"));
			System.setProperty("webdriver.gecko.driver",  config.getValue("firefoxDriverLocation_mac"));
			break;
		case "windows":
			System.setProperty("webdriver.chrome.driver", config.getValue("chromeDriverLocation_windows"));
			System.setProperty("webdriver.gecko.driver",  config.getValue("firefoxDriverLocation_windows"));
			System.setProperty("webdriver.ie.driver",     config.getValue("IE11DriverLocation_windows"));
			break;
		case "linux":
			System.setProperty("webdriver.chrome.driver", config.getValue("chromeDriverLocation_linux"));
			System.setProperty("webdriver.gecko.driver",  config.getValue("firefoxDriverLocation_linux"));
			break;
		default:
			System.out.println(GlobalVariables.getTestID() + " ERROR: The OS, '" + os + "' is unkown.");
		}
	}


	/***
	 * <h1>setDefaultTimeOut</h1>
	 * <p>purpose: Set the System Timeout as specified in the config.json by defaultTimeOut field.
	 * If value is not listed in config.json, then default to 15 second. If defaultTimeOut
	 * is provided in the mvn build statement, then use that
	 * @return None
	 * @throws Throwable
	 */
	private void setDefaultTimeOut() {
		if (System.getProperty("defaultTimeOut") == null) {
			// Get defaultTimeOut from config.json or default it
			String timeOut = config.getValue("defaultTimeOut") == null ? "25" : config.getValue("defaultTimeOut");
			System.setProperty("defaultTimeOut", timeOut); 
			driver.manage().timeouts().implicitlyWait(Integer.parseInt(timeOut), TimeUnit.SECONDS);
		} else {
			// Provided defaultTimeOut as parameter in mvn command line
			String timeOut = System.getProperty("defaultTimeOut");
			driver.manage().timeouts().implicitlyWait(Integer.parseInt(timeOut), TimeUnit.SECONDS);
		}

		System.out.println(GlobalVariables.getTestID() + " defaultTimeOut        = " + System.getProperty("defaultTimeOut") + " seconds");
	}

	/***
	 * <h1>setMinimalDefaultTimeOut</h1>
	 * <p>purpose: Set the System Timeout as specified in the config.json by minimalDefaultTimeOut field.
	 * If value is not listed in config.json, then default to 15 second. If minimalDefaultTimeOut
	 * is provided in the mvn build statement, then use that
	 * @return None
	 * @throws Throwable
	 */
	private void setMinimalDefaultTimeOut() {
		if (System.getProperty("minimalTimeOut") == null) {
			// Get defaultTimeOut from config.json or default it
			String timeOut = config.getValue("minimalTimeOut") == null ? "2" : config.getValue("minimalTimeOut");
			System.setProperty("minimalTimeOut", timeOut);
			driver.manage().timeouts().implicitlyWait(Integer.parseInt(timeOut), TimeUnit.SECONDS);
		} else {
			// Provided defaultTimeOut as parameter in mvn command line
			String timeOut = System.getProperty("minimalTimeOut");
			driver.manage().timeouts().implicitlyWait(Integer.parseInt(timeOut), TimeUnit.SECONDS);
		}

		System.out.println(GlobalVariables.getTestID() + " minimalTimeOut        = " + System.getProperty("minimalTimeOut") + " seconds");
	}
	
	/***
	 * <h1>setMailinatorInfo</h1>
	 * <p>purpose: Set all System info required to access/use of Mailinator</p>
	 * @return None
	 */
	private void setMailinatorInfo() {
		// Get the base page URL and set as system property
		System.setProperty("mailinatorURL", config.getValue("mailinatorURL"));
		System.out.println(GlobalVariables.getTestID() + " mailinatorURL         = " + System.getProperty("mailinatorURL"));
	}

	/***
	 * <h1>setBuySpeedIntegrationInfo</h1>
	 * <p>purpose: Set all System info required to access/use of BuySpeed</p>
	 * @return None
	 */
	private void setBuySpeedIntegrationInfo() {
		// Get the base page URL and set as system property
		if (System.getProperty("buyspeedURL") == null) {
			System.setProperty("buyspeedURL", config.getValue("buyspeedURL"));
		}
		System.out.println(GlobalVariables.getTestID() + " buyspeedURL           = " + System.getProperty("buyspeedURL"));
	}
	
	/***
	 * <h1>setBaseUrls</h1>
	 * <p>purpose: Based on the environment, as defined by the "server" property in config.json, set:<br>
	 * 	1. supplierBaseURL       = supplier platform site<br>
	 * 	2. agencyBaseURL         = agency platform site<br>
	 * 	3. adminBaseURL          = admin platform site<br>
	 * 	4. bidSyncSiteURL        = bidsync pro registration site <br>
	 * 	5. bidSyncClassicSiteURL = bidsync classic site <br>
	 * 	6. chargebeeTestSiteURL  = chargebee site (admin) <br>
	 *	   a. chargebeeTestSiteLogin    = admin login name for chargebee <br>
	 *	   b. chargebeeTestSitePassword = admin password for chargebee <br>
	 *	7. databaseURL           =  postgres database URL<br>
	 *	   a. databaseLogin             = db login name<br> 
	 *	   b. databasePassword          = db password <br>
	 */
	private void setBaseUrls () {
		
		switch (System.getProperty("server")) {

        // QA         
		case "qa":
			String base = "phi-qa.cloud";
        	System.setProperty("supplierBaseURL",           "https://supplier." + base + "/");
        	System.setProperty("agencyBaseURL",             "https://agency."   + base + "/");
        	System.setProperty("adminBaseURL",              "https://admin."    + base + "/");
        	System.setProperty("bidSyncSiteURL",            "https://bidsync-qa.builtbymasonry.com/");
        	System.setProperty("salesforceURL",             "https://test.salesforce.com/");
        	System.setProperty("bidSyncClassicBaseURL",     "https://classic.phi-stage.cloud/");
        	System.setProperty("buyspeedURL", 				"https://qa.buyspeed.com/bsoheadauto/");

//        	System.setProperty("chargebeeTestSiteURL",      "https://bidsync-test.chargebee.com/");
        	System.setProperty("chargebeeTestSiteURL",      "https://www.chargebee.com/");
        	System.setProperty("chargebeeTestSiteLogin",    "mandrews@periscopeholdings.com");
        	System.setProperty("chargebeeTestSitePassword", "3ZMb%zRt8C3*");

        	// ChargeBee API Properties
        	System.setProperty("chargeBeeSite",      "bidsync-test");
        	System.setProperty("chargeBeeApiKey",      "test_jn49YP7tc0lCARuZql2cuLC5cRf5OSQBu:3ZMb%zRt8C3*");
        	System.setProperty("chargeBeeUsername",    "mandrews@periscopeholdings.com");
        	System.setProperty("chargeBeePassword", "3ZMb%zRt8C3*");
        	
        	System.setProperty("databaseURL",               "phi-qa-cluster-1.cluster-cvaagqb2nefs.us-west-2.rds.amazonaws.com");
        	System.setProperty("databaseLogin",             "phi_rds_admin");
        	System.setProperty("databasePassword",          "apnWmmjjrjTFCzfM");
        	
            break;
        
		//  Dev
		case "dev":
			base = "phi-dev.cloud";
        	System.setProperty("supplierBaseURL",           "https://supplier." + base + "/");
        	System.setProperty("agencyBaseURL",             "https://agency."   + base + "/");
        	System.setProperty("adminBaseURL",              "https://admin."    + base + "/");
        	System.setProperty("bidSyncSiteURL",            "https://bidsync-dev.builtbymasonry.com/");
        	System.setProperty("salesforceURL",             "https://test.salesforce.com/");
        	System.setProperty("bidSyncClassicBaseURL",     "https://classic.phi-dev.cloud/");
        	System.setProperty("buyspeedURL", 				"https://qa.buyspeed.com/bsoautodev/");

        	System.setProperty("chargebeeTestSiteURL",      "https://phieng-test.chargebee.com/");
        	System.setProperty("chargebeeTestSiteLogin",    "mandrews@periscopeholdings.com");
        	System.setProperty("chargebeeTestSitePassword", "3ZMb%zRt8C3*");
        	
        	// ChargeBee API Properties
        	System.setProperty("chargeBeeSite",      "https://phieng-test.chargebee.com");
        	System.setProperty("chargeBeeApiKey",      "test_hC0N1Wmm3K2EXOZ46JOadb0Uk9pM39gS");
        	System.setProperty("chargeBeeUsername",    "mandrews@periscopeholdings.com");
        	System.setProperty("chargeBeePassword", "3ZMb%zRt8C3*");

        	System.setProperty("databaseURL",               "phi-dev-cluster-1.cluster-cievzltlsczv.us-west-2.rds.amazonaws.com");
        	System.setProperty("databaseLogin",             "phi_rds_admin");
        	System.setProperty("databasePassword",          "apnWmmjjrjTFCzfM");
            break;
            
		//  stage
		case "stage":
			base = "phi-stage.cloud";
        	System.setProperty("supplierBaseURL",           "https://supplier." + base + "/");
        	System.setProperty("agencyBaseURL",             "https://agency."   + base + "/");
        	System.setProperty("adminBaseURL",              "https://admin."    + base + "/");
        	System.setProperty("bidSyncSiteURL",            "https://bidsync-stage.builtbymasonry.com/");
        	System.setProperty("salesforceURL",             "https://test.salesforce.com/");
        	System.setProperty("bidSyncClassicBaseURL",     "https://classic.phi-stage.cloud/");
        	System.setProperty("buyspeedURL", 				"https://qa.buyspeed.com/bsoautostage/");
        	
        	System.setProperty("chargebeeTestSiteURL",      "https://qastage-test.chargebee.com/");
        	System.setProperty("chargebeeTestSiteLogin",    "mandrews@periscopeholdings.com");
        	System.setProperty("chargebeeTestSitePassword", "Gm1GnchFLkc4");
        	
        	// ChargeBee API Properties
        	System.setProperty("chargeBeeSite",      "qastage-test");
        	System.setProperty("chargeBeeApiKey",      "test_hC0N1Wmm3K2EXOZ46JOadb0Uk9pM39gS:3ZMb%zRt8C3*");
        	System.setProperty("chargeBeeUsername",    "mandrews@periscopeholdings.com");
        	System.setProperty("chargeBeePassword", "3ZMb%zRt8C3*");

        	System.setProperty("databaseURL",               "phi-stage-cluster-1.cluster-cetqjelxdyry.us-west-2.rds.amazonaws.com");
        	System.setProperty("databaseLogin",             "phi_rds_admin");
        	System.setProperty("databasePassword",          "apnWmmjjrjTFCzfM");
            break;        

        // Prod
		case "prod":
			base = "app.bidsync.com";
        	System.setProperty("supplierBaseURL",           "https://" + base + "/");
        	System.setProperty("agencyBaseURL",             "https://" + base + "/");
        	System.setProperty("adminBaseURL",              "https://admin.phi-production.cloud/");
        	System.setProperty("bidSyncSiteURL",            "https://bidsync.com/");
        	System.setProperty("salesforceURL",             "https://salesforce.com/");

        	// dmidura: Logins are wrong
        	System.setProperty("chargebeeTestSiteURL",      "https://bidsync.chargebee.com/");
        	System.setProperty("chargebeeTestSiteLogin",    "mandrews@periscopeholdings.com");
        	System.setProperty("chargebeeTestSitePassword", "Gm1GnchFLkc4");

        	// dmidura: These are probably wrong
        	System.setProperty("databaseURL",               "phi-prod-cluster-1.cluster-c7khqvzqopsx.us-west-2.rds.amazonaws.com");
        	System.setProperty("databaseLogin",             "phi_rds_admin");
        	System.setProperty("databasePassword",          "apnWmmjjrjTFCzfM");
        	
        	// dmidura: Need to update to correct value
        	System.setProperty("bidSyncClassicBaseURL",     "https://stage.bidsync.com/");
 			break;
		}
		
		System.out.println(GlobalVariables.getTestID() + "  ---- Environment Properties ----- ");
		System.out.println(GlobalVariables.getTestID() + " supplierBaseURL       = " + System.getProperty("supplierBaseURL"));
		System.out.println(GlobalVariables.getTestID() + " agencyBaseURL         = " + System.getProperty("agencyBaseURL"));
		System.out.println(GlobalVariables.getTestID() + " adminBaseURL          = " + System.getProperty("adminBaseURL"));
		System.out.println(GlobalVariables.getTestID() + " bidSyncSiteURL        = " + System.getProperty("bidSyncSiteURL"));
		System.out.println(GlobalVariables.getTestID() + " bidSyncClassicSiteURL = " + System.getProperty("bidSyncClassicBaseURL"));
		System.out.println(GlobalVariables.getTestID() + " chargebeeTestSiteURL  = " + System.getProperty("chargebeeTestSiteURL"));
		System.out.println(GlobalVariables.getTestID() + " chargebeeBidSyncSiteURL  = " + System.getProperty("chargeBeeSite"));
		System.out.println(GlobalVariables.getTestID() + " salesforceURL         = " + System.getProperty("salesforceURL"));
		System.out.println(GlobalVariables.getTestID() + " databaseURL           = " + System.getProperty("databaseURL"));
	}
	
	
	/***
	 * <h1>PrintScreenResolution</h1>
	 * <p>purpose: Print screen's height and width to console</p>
	 */
	public void PrintScreenResolution() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize(); //get the dimension of screen
        System.out.println(GlobalVariables.getTestID() + " Screen Width: "  + screenSize.getWidth());
        System.out.println(GlobalVariables.getTestID() + " Screen Height: " + screenSize.getHeight());
	}
	
	
	/***
	 * <h1>waitForNumberOfWindowsToEqual</h1>
	 * <p>purpose: Wait for a new browser window to open or existing to close</p>
	 * @return None
	 * @deprecated - remove this from CoreAutomation
	 */
	public void waitForNumberOfWindowsToEqual(final int numberOfWindows) {
		new WebDriverWait(driver, 10) {
		}.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				return (driver.getWindowHandles().size() == numberOfWindows);
			}
		});
	}
}
