/**
 * 
 */
package pages.common.pageObjects.toBeDeprecated;

/**
 * List of types. each one represents a element locating mechanism. See org.openqa.selenium.By
 * @author jlara
 *
 */
@Deprecated
public enum ByTypes {
	ClassName, 
    CssSelector, 
    Id, 
    LinkText, 
    Name, 
    PartialLinkText, 
    TagName, 
    XPath
} // end of enum ByTypes
