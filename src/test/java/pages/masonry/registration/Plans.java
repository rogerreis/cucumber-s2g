package pages.masonry.registration;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.masonry.registration.bidSyncBasicRegistration.BidSyncBasicRegistration;

/***
 * <h1>Class Plans</h1>
 * <p>details: This class houses page objects and methods for the Plans page</p>
 */
public class Plans extends CommonTopBar {
	
    // xpaths
    private final String tryFreeButton_xpath = "//a[contains(.,'Try Free')][@href='/bidsync-basic']";
    
	// Other
    private final String thisPagesURL        = System.getProperty("bidSyncSiteURL") + "plans";

    public Plans(EventFiringWebDriver driver) {
        super(driver);
    }
    
    /* ----- getters ----- */
    private WebElement getTryFreeButton() {
    	int time = DEFAULT_TIMEOUT; 

    	// Button displays differently depending on screen size
    	List <WebElement> tryFreeButtons =
    		new WebDriverWait(driver, time)
    		.withMessage("ERROR: Cannot locate \"TRY FREE\" button either for small screen size or large screen size after \"" + time + "\" seconds")
    		.until( ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(this.tryFreeButton_xpath)));
    	
    	// So return the button based on the size
    	for (WebElement ele : tryFreeButtons) {
    		if (ele.isDisplayed()) {
    			return ele; }
    	}
    	return null;
    } 
    
    /* ----- methods ----- */
    /***
     * <h1>clickTryFreeBtn</h1>
     * <p>purpose: Click on the "TRY FREE" button to navigate to the BidSync Basic registration screen</p>
     * @return BidSyncBasicRegistration
     */
    public BidSyncBasicRegistration clickTryFreeBtn() {
        getTryFreeButton().click();
        return new BidSyncBasicRegistration(driver);
    }
    
    /***
     * <h1>toHere</h1>
     * <p>purpose: Navigate browser directly to the Plan page
     * @return Plan
     */
    public Plans toHere() {
    	driver.navigate().to(this.thisPagesURL);
    	return this;
    }
}
