package pages.masonry.registration.bidSyncProRegistration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>AddAccountDetailsModal</h1>
 * <p>details: This class houses page objects and methods for the "Add Account Details" modal (1st modal in Chargebee<br>
 * 	punchout registration flow) that is part of the BidSync Pro registration off of the Masonry site</p>
 */
public class AddAccountDetailsModal extends BasePageActions {
    
    // ids
    private final String modalWindowFrame_id = "cb-frame";
    private final String firstNameInput_id   = "first_name";
    private final String lastNameInput_id    = "last_name";
    private final String companyInput_id     = "company";
    private final String phoneNumberInput_id = "phone";
    private final String industry_id = "cf_industry";
    private final String companySize_id ="cf_company_size";
    private final String companyRevenue_id ="cf_company_revenue";
    private final String reasonForRegistration_id ="cf_why_are_you_registering_with_bidsync";
    
    // xpaths
    private final String nextButton_xpath    = "//button[contains(@class, 'cb-button')]";

    // Other
    private EventFiringWebDriver driver;
    private HelperMethods helperMethods;
    
    public AddAccountDetailsModal(EventFiringWebDriver driver) {
    	super(driver);
        this.driver        = driver;
        this.helperMethods = new HelperMethods();
        // TODO: this frame switch takes several seconds. find out why
        this.driver.switchTo().frame(this.modalWindowFrame_id);
    }
    
    /* ----- getters ------ */
    private WebElement getFirstNameInput()   { return findByVisibility(By.id(this.firstNameInput_id));   } 
    private WebElement getLastNameInput()    { return findByVisibility(By.id(this.lastNameInput_id));    } 
    private WebElement getCompanyInput()     { return findByVisibility(By.id(this.companyInput_id));     } 
    private WebElement getPhoneNumberInput() { return findByVisibility(By.id(this.phoneNumberInput_id)); } 
    private WebElement getNextButton()       { return findByVisibility(By.xpath(this.nextButton_xpath)); } 
    
    private Select getIndustry()           	   	   { return new Select( findByVisibility(By.id(this.industry_id)));             } 
    private Select getCompanySize()            	   { return new Select( findByVisibility(By.id(this.companySize_id)));          } 
    private Select getCompanyRevenue()         	   { return new Select(findByVisibility(By.id(this.companyRevenue_id)));        } 
    private Select getReasonForRegistration()  	   { return new Select( findByVisibility(By.id(this.reasonForRegistration_id)));} 

    /* ----- methods ----- */
    /***
     * <h1>fillOutUserDataAndSubmit</h1>
     * <p>purpose: Enter user's registration info into the "Add Accound Details" modal (1st Chargebee punchout registration modal) <br>
     * 	and then click "NEXT" to submit</p>
     * @param user = user registration info
     * @return AddBillingAddressModal
     */
    public AddBillingAddressModal fillOutUserDataAndSubmit(RandomChargebeeUser user) {
    	System.out.println(GlobalVariables.getTestID() + " Fills user data.");
        getFirstNameInput()  .sendKeys(user.getFirstName());
        getLastNameInput()   .sendKeys(user.getLastName());
        getCompanyInput()    .sendKeys(user.getCompany().getName());
        getPhoneNumberInput().sendKeys(user.getPhoneNumber());
        // leaving out the email field because it is auto-filled

        System.out.println(GlobalVariables.getTestID() + " Industry : "+ user.getIndustry());
        System.out.println(GlobalVariables.getTestID() + " Company Size : "+ user.getCompanySize());
        System.out.println(GlobalVariables.getTestID() + " Company Revenue : "+ user.getCompanyRevenue());
        System.out.println(GlobalVariables.getTestID() + " Reason For Registration : "+ user.getReasonForRegistration());
        getIndustry().selectByVisibleText(user.getIndustry());
        getCompanySize().selectByVisibleText(user.getCompanySize());
//        getCompanyRevenue().selectByVisibleText(user.getCompanyRevenueForMasonry());
//        getReasonForRegistration().selectByVisibleText(user.getReasonForRegistrationForMasonry());
        getCompanyRevenue().selectByVisibleText(user.getCompanyRevenue());
        getReasonForRegistration().selectByVisibleText(user.getReasonForRegistration());
        
        getNextButton().click();
        helperMethods.addSystemWait(1);
        return new AddBillingAddressModal(driver);
    }
    
}
