package pages.bidSyncPro.supplier.common;

import static org.junit.Assert.fail;

/***
 * <h1>enum: CompanySettingsDropdownOptions</h1>
 * @author dmidura
 * <p>details: This enum defines the dropdown options available when the top bar "Company Settings" menu is open.<br>
 *  Dropdown options for "Company Settings" menu are: Company Profile, Manage Users, Manage Subscriptions, Agency Interaction</p>
 */
public enum CompanySettingsDropdownOptions {
	COMPANY_PROFILE,
	MANAGE_USERS,
	MANAGE_SUBSCRIPTIONS,
	AGENCY_INTERACTION;
	
	/***
	 * <h1>fromString</h1>
	 * <p>purpose: Given text that matches the Company Settings top bar menu options,<br>
	 *  return the enum value of the CompanySettingsDropdownOptions</p>
	 * @param dropDown<br>
	 * 	= "Company Profile" | "Manage Users" | "Manage Subscriptions" | "Agency Interaction"</p>
	 * @return CompanySettingsDropdownOptions
	 */
	public static CompanySettingsDropdownOptions fromString(String dropDown) {
		if (dropDown.equalsIgnoreCase("Company Profile")) {
			return COMPANY_PROFILE;
		} else if (dropDown.equalsIgnoreCase("Manage Users")) {
			return MANAGE_USERS;
		} else if (dropDown.equalsIgnoreCase("Manage Subscriptions")) {
			return CompanySettingsDropdownOptions.MANAGE_SUBSCRIPTIONS;
		} else if (dropDown.equalsIgnoreCase("Agency Interaction")) {
			return CompanySettingsDropdownOptions.AGENCY_INTERACTION;
		} else {
			fail("ERROR: Dropdown option \"" + dropDown + "\" is an invalid option to CompanySettingsDropDownOptions");
			return null;
		}
	}
	
	@Override
	public String toString() {
		if (this.equals(CompanySettingsDropdownOptions.COMPANY_PROFILE)) {
			return "Company Profile";
		} else if (this.equals(CompanySettingsDropdownOptions.MANAGE_SUBSCRIPTIONS)) {
			return "Manage Subscriptions";
		} else if (this.equals(CompanySettingsDropdownOptions.MANAGE_USERS)) {
			return "Manage Users";
		} else if (this.equals(CompanySettingsDropdownOptions.AGENCY_INTERACTION)) {
			return "Agency Interaction";
		} else {
			fail("ERROR: Company Dropdown value \"" + this + "\" is an invalid option to CompanySettingsDropdownOptions");
			return null;
		}
	}
	

}
