@must @registration @level2
Feature: Supplier Registration - Set Password Screen
ARGO-375: Security: Minimum password requirements
ARGO-879: User Registration - Set your password
ARGO-2580: Expand coverage of Set Password page in supplier registration
ARGO-2591: Page Initialization


Background: Test steps to get to Set Your Password Screen
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to the Set Your Password screen 
	

@ARGO-2580	@ARGO-879 @ARGO-2591
Scenario: Validate SetPassword Screen - page initializations, ability to show/hide password and confirm password fields
	When I landed on Set password page
	Then I verify that the expected page objects are present in Set Password page
	And the new password and confirm password field is ready to accepts input or enabled.
	And the next button is disabled
	Then I verify that the new password is hidden by default.
	And the confirm password is hidden by default


@ARGO-2580 @ARGO-879
Scenario: Ability to show/hide password and confirm password fields	
	When user select show password option for new password, the new password entered should be visible
	When user select hide password option for new password, the new password entered should not be visible
	When user select show password option for confirm password, the confirm password entered should be visible
	When user select hide password option for confirm password, the confirm password entered should not be visible
	
	
@ARGO-2580 @ARGO-879
Scenario Outline:Validate if new and confirm password input values must match
	When I enter invalid <newPassword> and <confirmPassword>
	Then I should see the error message on the screen
	And the next button is disabled

Examples:
	|newPassword    | confirmPassword |
	|Password       |	Password1     |
	|Password1      |                 |
	|               |                 |
	|Pass123        |	pass123       |
	|Pass@12        |   Pass@123      |


@ARGO-2580 @ARGO-879
Scenario Outline:Validate after setting password, user navigates to the setup/company page
	When I use valid newPassword <new> and confirmPassword <confirm> to set my password
	Then I should redirect to company setup page

Examples:
	|new            |  confirm         |
	|Pass1234       |  Pass1234        |
	|Password1      |  Password1       |


# ARGO-2589: "Password Validations"
# 1.  Password < 8 chars
# 2.  Password > 128 chars

@ARGO-2589 @ARGO-375
Scenario Outline:Validate password length check - less than 8 and greater than 128
	When I enter new password as <password>
	Then I should see invalid password error message
	And the password criteria between 8 and 128 characters should not be highlighted
	And the next button is disabled

Examples:
	|password    	|
	|Pass           |
	|This is not a valid password. This is used to test the invalid password use case. This password has more than 128 characters which makes it invalid.|
			

# ARGO-2589: "Password Validations"
# 3.  Password contains only lower case
# 4.  Password contains only upper case
# 5.  Password contains only digits
# 6.  Password contains only special chars
# 7.  Password contains lower case and upper case
# 8.  Password contains lower case and digits
# 9.  Password contains lower case and special chars
# 10. Password contains upper case and digits
# 11. Password contains upper case and special chars
# 12. Password contains digits and special chars
# 13. Password contains lower case, upper case, and digit
# 14. Password contains lower case, upper case, and special char
# 15. Password contains lower case,  special char, and digit
# 16. Password contains upper case,  special char, and digit
# 17. Password contains upper case,  lower case, special char, and digit

@ARGO-2589 @ARGO-375
Scenario Outline: Password validation
	When I enter new password as <password>
	Then I should get the password requirement <requirement1> to be highlighted
	And I should get the password requirement <requirement2> to be highlighted
	And I should get the password requirement <requirement3> to be highlighted
	And I should get the password requirement <requirement4> to be highlighted
	And the next button is disabled
	
	Examples:
		|password      |requirement1                 |requirement2                 |requirement3                 |requirement4         |
		|myNewPassword |between 8 and 128 characters |                             |                             |                     |
		|mynewpassword |At least 1 lowercase         |                             |                             |                     |
		|MYNEWPASSWORD |At least 1 uppercase         |                             |                             |                     |
		|1542365487    |At least 1 digit             |                             |                             |                     |
		|$#$%^&**^     |At least 1 special character |                             |                             |                     |
		|Password      |At least 1 lowercase         |At least 1 uppercase         |                             |                     |
		|password1     |At least 1 lowercase         |At least 1 digit             |							 |                     |
		|password@     |At least 1 lowercase         |At least 1 special character |                             |                     |
		|Password1     |At least 1 uppercase         |At least 1 digit             |                             |                     |
		|Password@     |At least 1 uppercase         |At least 1 special character |                             |                     |
		|12345678@     |At least 1 digit             |At least 1 special character |                             |                     |
		|Password1     |At least 1 lowercase         |At least 1 uppercase         |At least 1 digit             |                     |
		|Password@     |At least 1 lowercase         |At least 1 uppercase         |At least 1 special character |                     |
		|password@1    |At least 1 lowercase         |At least 1 digit             |At least 1 special character |                     |
		|Password@1    |At least 1 uppercase         |At least 1 digit             |At least 1 special character |                     |
		|Password@1    |At least 1 uppercase         |At least 1 digit             |At least 1 special character |At least 1 lowercase |
