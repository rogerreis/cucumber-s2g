package pages.bidSyncPro.supplier.dashboard.bidLists;

import static org.junit.Assert.fail;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;

import io.cucumber.datatable.DataTable;
import pages.common.helpers.DateHelpers;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import utility.database.SQLConnector;

/***
 * <h1>Class BidCriteria</h1>
 * @author dmidura
 * <p>details: This class holds the expected data elements that comprise a bid.<br>
 * 	For right now, the bid elements are limited to what appears on the bid list,<br>
 * 	however, this may expand to include bid details fields if needed</p>
 */
public class BidCriteria {
	
	// Elements of a bid that displays in the bid list
    private String bidId     = "";
    private String bidTitle  = "";
    private String bidNumber = "";
    private String agency    = "";
    private String state     = "";
    private String endDate   = "";
    private String addedDate = "";
    
    public BidCriteria() { }
    /***
     * <h1>BidCriteria</h1>
     * <p>purpose: class constructor</p>
     * @param bidId     = id
     * @param bidTitle  = title
     * @param bidNumber = number
     * @param agency    = agency
     * @param state     = state (abbreviation)
     * @param endDate   = MM/dd/yyyy
     * @param addedDate = MM/dd/yyyy
     */
    public BidCriteria(String bidId, String bidTitle, String bidNumber, String agency, String state, String endDate, String addedDate) { 
    	this.setBidId(bidId);
    	this.setBidTitle(bidTitle);
    	this.setBidNumber(bidNumber);
    	this.setAgency(agency);
    	this.setState(state);
    	this.setEndDate(endDate);
    	this.setAddedDate(addedDate);
    }
    
    /***
     * <h1>BidCriteria</h1>
     * <p>purpose: class constructor</p>
     * @param dTable of form: <br>
     * | Bid Title  | title                  |<br>
     * | Bid Number | number                 |<br>
     * | Bid Id     | id                     |<br>
     * | Agency     | agency                 |<br>
     * | State      | state (abbreviation)   |<br>
     * | End Date   | MM/dd/yyyy             |<br> 
     * | Added Date | MM/dd/yyyy             |<br>
     * Note: Added Date will be automatically converted to text display<br>
     * 	to match how BidSync Pro would display this date ("Yesterday" ,"1 Day Ago", etc)</p>
     */
    public BidCriteria(DataTable dTable) {
    	Map <String, List <String>> inputs = new HelperMethods().putDataTableIntoMap(dTable);
    	System.out.println(inputs.toString());
    	this.setBidId(inputs.get("Bid Id").get(0));
    	this.setBidTitle(inputs.get("Bid Title").get(0));
    	this.setBidNumber(inputs.get("Bid Number").get(0));
    	this.setAgency(inputs.get("Agency").get(0));
    	this.setState(inputs.get("State").get(0));
    	this.setEndDate(inputs.get("End Date").get(0));
    	this.setAddedDate(inputs.get("Added Date").get(0));
    	
    	// Update the Added Date to match how our system is currently displaying added date
    	this.setAddedDate(this.getAddedDateAsText(this.getAddedDate()));
    }
    
    // getters
	public String getBidId()                      { return bidId;               }
	public String getBidTitle()                   { return bidTitle;            }
	public String getBidNumber()                  { return bidNumber;           }
	public String getAgency()                     { return agency;              }
	public String getState()                      { return state;               }
	public String getEndDate()                    { return endDate;             }
	public String getAddedDate()                  { return addedDate;           }

	// setters
	protected void setBidId(String bidId)         { this.bidId     = bidId;     }
	protected void setBidTitle(String bidTitle)   { this.bidTitle  = bidTitle;  }
	protected void setBidNumber(String bidNumber) { this.bidNumber = bidNumber; }
	protected void setAgency(String agency)       { this.agency    = agency;    }
	protected void setState(String state)         { this.state     = state;     }
	protected void setEndDate(String endDate)     { this.endDate   = endDate;   }
	protected void setAddedDate(String addedDate) { this.addedDate = addedDate; }

	/* ----- overrides ----- */

	/***
     * <h1>equals</h1>
     * <p>purpose: Necessary override so this class functions correctly with comparators</p>
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof BidCriteria)) {
            return false;
        }
        
        // For a bid to be equal to another bid, all attributes should match
        BidCriteria other = (BidCriteria) o;
        if( this.getBidTitle()  .equals(other.getBidTitle())   &&
            this.getBidNumber() .equals(other.getBidNumber())  &&
            this.getBidId()     .equals(other.getBidId())      &&
            this.getAgency()    .equals(other.getAgency())     &&
            this.getState()     .equals(other.getState())      && 
            this.getEndDate()   .equals(other.getEndDate())    &&
            this.getAddedDate().trim().equals(other.getAddedDate().trim())) {
        	return true;
        } else {
        	return false;
        }
        		
    }
    
    /***
     * <h1>hashCode</h1>
     * <p>purpose: Necessary override so this class functions correctly with mapping objects</p>
     * @return int
     */
    @Override
    public int hashCode() {
        return this.getBidId().hashCode();
    }

     /***
     * <h1>getAddedDateAsText</h1>
     * <p>purpose: Helper for converting a bid added date from<br>
     * 	an exact date to a displayed date.</p>
     * @param bidAddedDate = String of format MM/dd/yyy HH:mm:ss<br>
     *        or of format MM/dd/yyyy
     * <p>Ex:<br>
     * 	Added Date   = 11/29/2018<br>
     * 	Today's Date = 12/06/2018<br>
     * 	return will be \"7 Days Ago\"</p>
     * @return String = added date, formatted per BidSync Pro display
     */
    private String getAddedDateAsText(String bidAddedDate) {
    	String addedDateText   = "";
    	DateHelpers dateHelper = new DateHelpers();

    	// Determine the time between the added date and today's date
    	Date addedDate  = dateHelper.getStringDateAsCalendar(bidAddedDate).getTime();
    	Date today      = dateHelper.getCurrentTimeAsCalendar().getTime();
    	long hours      = TimeUnit.HOURS.convert((today.getTime() - addedDate.getTime()), TimeUnit.MILLISECONDS);
    	
    	// Determine text that should display in BidSync Pro
    	// Note: Here, a month is given as 30 days
    	if (hours < 24) {
    		// Hours Ago
    		addedDateText = String.format("%s %s Ago", hours, hours == 1 ? "Hour" : "Hours"); 
    	} else if (hours/24 < 2)  { 
    		// Yesterday
    		addedDateText = "Yesterday";
    	} else if (hours/24 < 30) {
    		// Days Ago
    		int days = (int) hours/24;
    		addedDateText = String.format("%s %s Ago", days, days == 1 ? "Day" : "Days");
    	} else if (hours/24 < 365) {
    		// Months Ago
    		int months = (int) hours/(24*30);
    		addedDateText = String.format("%s %s", months, months == 1 ? "Month Ago" : "Months");
    	} else {
    		// Years Ago
    		int years = (int) hours/(24*365);
    		addedDateText = String.format("%s %s Ago", years, years == 1 ? "Year" : "Years");
    	}
    	
    	System.out.println(GlobalVariables.getTestID() + " INFO: Determined that bid added date of \"" + bidAddedDate + "\" occurred \"" + addedDateText + "\"" );
    	return addedDateText; 
    }   

    /* ----- methods ----- */
    
	/***
     * <h1>getEndDateAsCalendar</h1>
     * <p>purpose: Returns the bid's due date as a Calendar object.  Useful for comparing due dates.</p>
     * @return Calendar
     */
    public Calendar getEndDateAsCalendar() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
        try {
            calendar.setTime(sdf.parse(this.getEndDate()));
        } catch (ParseException e) {
            Assert.fail(String.format("ERROR: Invalid due date '%s'.\n", this.getEndDate()));
        }
        return calendar;
    }
    
    /***
     * <h1>getEndDateInUTC</h1>
     * <p>purpose: Returns the bid's due date as in UTC.  Useful for comparing due dates.</p>
     * @return ZoneDateTime
     */
    public ZonedDateTime getEndDateInUTC() {
        String[] fields = getEndDate().split("/");
        String endDateAsString = String.format("%s-%s-%sT00:00:00", fields[2], fields[0], fields[1]);
        return LocalDateTime.parse(endDateAsString).atZone(ZoneOffset.UTC);
    }
    
    /***
     * <h1>getAddedDateInHours</h1>
     * <p>purpose: Returns the bid's Added value as a number of hours.  Useful for comparing Added values.</p>
     * @return Calendar
     */
    public Integer getAddedDateInHours() {
    	
        if(getAddedDate().equals("Yesterday")) {
            return 24;
        }
        Matcher matcher = Pattern.compile("\\d+").matcher(getAddedDate());
        if(!matcher.find()) {
            System.out.println(String.format("WARN: No integer found in Added value '%s'", getAddedDate()));
        }
        Integer numeral = Integer.parseInt(matcher.group());
        if(getAddedDate().contains("Day")) {
            numeral *= 24;
        } else if (getAddedDate().contains("Month")) {
            numeral *= 720;
        } else if (getAddedDate().contains("Year")) {
            numeral *= 8640;
        }
        return numeral;
    }
    
    /***
     * <h1>isExpired</h1>
     * <p>purpose: Determine if the bid is expired by comparing the current time<br>
     * 	(per GMT time zone) with the value of the bid as stored in the database.<br>
     * 	Note: If the bid is in "Upgrade Plan" mode, then this test will method will fail.<br>
     * 	If this is a bid in the bid list row, pre-empt with isUpgradePlan()</p>	
     * @return
     */
    public boolean isExpired() {
        Calendar now = new DateHelpers().getCurrentTimeAsCalendar();

    	// Note: If a bid is in "Upgrade Plan" mode, then we can't retrieve its 
    	// date from the database b/c we've obscured the solicitation_central_id that 
    	// we use to lookup the date
        return now.compareTo(this.getPreciseEndDate()) > 0;
    }
    
    /***
     * <h1>getPreciseEndDate</h1>
     * <p>purpose: Grab the bid end date from the database<br>
     * 	Note: This method should only be used for a bid that is not in<br>
     *  "Upgrade Plan" mode b/c we use the solicition_central_id as the <br>
     * 	for the database lookup, and this is obscured in "Upgrade Plan" mode</p>
     * @return Calendar of the bid's end date
     */
    public Calendar getPreciseEndDate() {
        String query = String.format("select end_date_epoch_milli from solicitation_central where solicitation_central_id = '%s'", this.getBidId());
        List<Long> results = new ArrayList<>();
        try (ResultSet rs = SQLConnector.executeQuery("survivor", query)) {
            while (rs.next()) {
                results.add(rs.getLong("end_date_epoch_milli"));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            Assert.fail(String.format("Errors when running query '%s'", query));
        }
    
        if (results.isEmpty()) {
            Assert.fail(String.format("No results found for query '%s' for bid title: '%s', number: '%s'", query, this.getBidTitle(), this.getBidNumber()));
        }
        Calendar result = new GregorianCalendar();
        Long timestamp = results.get(0);
        if(timestamp < 9999999999L) {
            // standard epoch is a 13 digit timestamp, but some bids have a truncated 10 digit.
            // we need to make those consistent with the rest
            timestamp *= 1000;
        }
        result.setTimeInMillis(timestamp);
        return result;
    }
    
    
    /***
     * <h1>printBidInfo</h1>
     * <p>purpose: Use this method to print out all bid row info to screen</p>
     * @return BidCriteria
     */
    public BidCriteria printBidInfo() {
    	System.out.println(GlobalVariables.getTestID() + " ------ Bid Info -------"     );
    	System.out.println(GlobalVariables.getTestID() + " Title:  "   + getBidTitle()  );
    	System.out.println(GlobalVariables.getTestID() + " Number: "   + getBidNumber() );
    	System.out.println(GlobalVariables.getTestID() + " Id: "       + getBidId()     );
    	System.out.println(GlobalVariables.getTestID() + " Agency: "   + getAgency()    );
    	System.out.println(GlobalVariables.getTestID() + " State: "    + getState()     );
    	System.out.println(GlobalVariables.getTestID() + " End Date: " + getEndDate()   );
    	System.out.println(GlobalVariables.getTestID() + " Added: "    + getAddedDate() );
    	System.out.println(GlobalVariables.getTestID() + " -----------------------"     );
    	return this;
    }
	
        /* ----- verifications ----- */
    /***
     * <h1>verifyEndDateIsAsExpected</h1>
     * <p>purpose: Verify that a bid's End Date is displaying<br>
     * 	as expected. This method grabs the currently displayed String value<br>
     * 	(MM/DD/YYYY format) and compares to the expected date.<br>
     * 	Fail test if the expected date doesn't match the displayed date</p>
     * @param endDate = Calendar for the expected end date
     * @return BidList
     */
    public BidCriteria verifyEndDateIsAsExpected(Calendar endDate) {
    	// End Date displays as MM/DD/YYYY
    	String endDateDisplayed = this.getEndDate();
    	String endDateExpected = new DateHelpers().getCalendarDateAsString(endDate);
    	
    	if (!endDateDisplayed.equals(endDateExpected)) {
    		fail("Test failed: Expected bid end date of \"" + endDateExpected + "\" but got \"" + endDateDisplayed + "\"."); }
    	
    	return this;
    }
}
