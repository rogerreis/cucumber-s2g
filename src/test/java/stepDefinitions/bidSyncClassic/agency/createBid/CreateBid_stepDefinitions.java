package stepDefinitions.bidSyncClassic.agency.createBid;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;


public class CreateBid_stepDefinitions {

	public CoreAutomation automation;
	public EventFiringWebDriver driver;	

	public CreateBid_stepDefinitions(CoreAutomation automation) throws Throwable {
		this.automation = automation;
		this.driver = automation.getDriver();		
	}
	
	@Given("^I navigate my browser to the BidSync classic login page$")
	public void iNavigateToTheBidSyncLoginPage() throws Throwable {
		try {
			driver.get("https://classic.phi-stage.cloud/DPXLogin");
			//automation.maximizeBrowser();
			//	System.out.print("Navigating to login page\n");
		} catch (Exception e) {
			System.out.println("Unable to navigate to login page.\n");
			}
	}

	//Log in to system
	@When("^I enter my username as \"([^\"]*)\" and password as \"([^\\\"]*)\"$")
	public void I_enter_my_Email_as_and_Password_as (String arg1, String arg2) throws Throwable {
		try {
			System.out.println(GlobalVariables.getTestID() + " Logging into BidSync.\n");
			driver.findElement(By.id("username")).sendKeys(arg1);
			driver.findElement(By.id("password")).sendKeys(arg2);
			driver.findElement(By.id("btnSubmit")).click();
		} catch (Exception e) {
			System.out.println("Login failed.\n");
		}
	}
	
	// Successful login result. I will be on the agency home page
	@Then("^I will be on the agency home page$")
	public void i_will_be_on_the_supplier_home_page() throws Throwable {		
		try {
			TimeUnit.SECONDS.sleep(2);
			String actual_url = driver.getCurrentUrl();
			if(actual_url.contains("classic.phi-stage.cloud/DPX")) { 
				System.out.println(GlobalVariables.getTestID() + " Test passed: Login was successful!\n");
			} else {
				System.out.println(GlobalVariables.getTestID() + " Test failed: Login was unsuccessful!\n");
				}
		} catch (Exception e) {
			System.out.println("Failed. We are not on the landing page.\n");
		}
	}
	
	//We can now create a new bid. For now, we are going to cheat and copy from an existing bid
	@Given("^I am logged in as an agency$")
	public void i_am_logged_in() {
		System.out.println(GlobalVariables.getTestID() + " We are logged in.\n");
	}
	
	@Then("^I will create a new bid$")
	public void I_will_create_a_new_bid() {
		try {
			int count = 1;
			while (count <= 10) {
				System.out.println(GlobalVariables.getTestID() + " Ready to create bid #"+count+". Changing URL for Alpine SD. OID 178.\n");
				driver.get("https://classic.phi-stage.cloud/DPX?atmnav=myauctions.viewauctions.current&ac=agencyview&show=curr&headoid=178");
				new HelperMethods().waitForVisibilityOfId(driver, "search", Integer.parseInt(System.getProperty("defaultTimeOut")));
				driver.findElement(By.id("search")).sendKeys("Basic coating products test");
				driver.findElement(By.linkText("Submit")).click();
				driver.findElement(By.xpath("//*[contains(@src,'images/auction/copy_auction.gif')]")).click();
				driver.findElement(By.id("budgetedamount")).clear();
				driver.findElement(By.id("budgetedamount")).sendKeys("15800.00");
				driver.findElement(By.linkText("Next Page")).click();		
				
				//Generate a random value for naming the bid
				Random rand = new Random();
				int value = rand.nextInt(10000);
				String bid_title = "Alpine SD test Bid #"+value;
				//Store it off for future use		
				GlobalVariables.storeObject("bid_title", bid_title);
				driver.findElement(By.id("bidtitle")).clear();
				driver.findElement(By.id("bidtitle")).sendKeys(bid_title);
				driver.findElement(By.linkText("Save Bid")).click();
				driver.findElement(By.linkText("Return to Bid List")).click();
				driver.findElement(By.id("search")).sendKeys(bid_title);
				driver.findElement(By.linkText("Submit")).click();
				driver.findElement(By.xpath("(//input[@type='checkbox'])[last()]")).click();
				driver.findElement(By.partialLinkText("Release")).click();		
				driver.findElement(By.xpath("//input[@name='pwd']")).sendKeys("test1234");
				driver.findElement(By.partialLinkText("Release")).click();
				count++;
			}
				
		} catch(Exception e) {
			System.out.println("Error: Was unable to create bids.\n");
			}
			
			
        //Bid created successfully. Logging out
        System.out.println(GlobalVariables.getTestID() + " All bids have been successfully created. Thanks for your support.\n");
        driver.findElement(By.xpath("//a[@href='javascript:loggingOut();']")).click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
    }
	
}