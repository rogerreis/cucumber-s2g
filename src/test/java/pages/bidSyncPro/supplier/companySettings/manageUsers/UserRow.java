package pages.bidSyncPro.supplier.companySettings.manageUsers;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import data.bidSyncPro.registration.SubscriptionAddOns;
import data.bidSyncPro.registration.SubscriptionGrouping;
import data.bidSyncPro.registration.SubscriptionPlan;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.BasePageActions;

public class UserRow extends BasePageActions {
    
    private EventFiringWebDriver driver;
    private HelperMethods helperMethods;
    private String thisRowsXpath;
    
    private String firstName;
    private String lastName;
    private String city;
    private String state;
    
    // xpaths
    private final String firstName_xpath                    = "//span[contains(@class, 'manage-user-first-name')]/strong";
    private final String lastName_xpath                     = "//span[contains(@class, 'manage-user-first-last')]/strong";
    private final String city_xpath                         = "//span[contains(@class, 'manage-user-city')]";
    private final String state_xpath                        = "//span[contains(@class, 'manage-user-state')]";
    private final String subscriptionNameGeneric_xpath      = "//span[contains(@class, 'manage-user-subscription-name')]";
    private final String subscriptionGroupingGeneric_xpath  = "//span[contains(@class, 'manage-user-grouping-name')]";
    private final String subscriptionAddonGeneric_xpath     = "//span[contains(@class, 'manage-user-addon')]";
    private final String subscriptionGenericLocator_xpath   = "//span[contains(@class,'mangage-user-user-subscription')]";
    private final String contextMenu_xpath                  = "//button[contains(@class, 'user-select-menu-trigger')]";
    private final String assignLicense_xpath                = "//button[contains(span, 'Assign License')]";
    private final String removeUser_xpath                   = "//button[contains(text(), 'Remove User')]";
    private final String revokeLicense_xpath                = "//button[contains(span, 'Revoke License')]";
    
    
    UserRow(EventFiringWebDriver driver, String thisRowsXpath) {
    	super(driver);
        this.driver        = driver;
        this.thisRowsXpath = thisRowsXpath;
        this.firstName     = this.driver.findElement(By.xpath(thisRowsXpath + firstName_xpath)).getText();
        this.lastName      = this.driver.findElement(By.xpath(thisRowsXpath + lastName_xpath)).getText();
        this.city          = this.driver.findElement(By.xpath(thisRowsXpath + city_xpath)).getText();
        this.state         = this.driver.findElement(By.xpath(thisRowsXpath + state_xpath)).getText();
        helperMethods 	   = new HelperMethods();
    }
    
    /* ----- getters ----- */
    private WebElement contextMenuButton()   { return findByPresence(By.xpath(this.thisRowsXpath + this.contextMenu_xpath)); }
    private WebElement assignLicenseOption() { return findByVisibility(By.xpath(this.assignLicense_xpath));                    }
    private WebElement removeUserOption()    { return findByPresence(By.xpath(this.removeUser_xpath));                       }
    private WebElement revokeLicenseOption() { return findByPresence(By.xpath(this.revokeLicense_xpath));                    }
    
    private List <String> getAllSubscriptionPlans(){
    	List <String> names = new ArrayList<>();
    	List <WebElement> ele = this.findAllOrNoElements(driver, By.xpath(this.thisRowsXpath + this.subscriptionGenericLocator_xpath + this.subscriptionNameGeneric_xpath));
    	
    	for (int i = 1; i < ele.size() + 1; i++) {
    		names.add(driver.findElement(By.xpath(
    				this.thisRowsXpath + this.subscriptionGenericLocator_xpath 
    				+ "[" + i + "]" + this.subscriptionNameGeneric_xpath)).getText());
    	}
    	
    	return names;
    }
    
    /***
     * <h1>getSubscriptionAddOns</h1>
     * <p>purpose: Get this row's subscription add-ons (if they exist)</p>
     * @return List <String> contains up to three add-ons ("Federal", "Canada", "Military")<br>
     * 	or will be empty if no addons</p>
     */
    private List <String> getSubscriptionAddOns() { 
    	int numAddons = driver.findElements(By.xpath(this.thisRowsXpath + this.subscriptionAddonGeneric_xpath)).size();
    	List <String> addOns = new ArrayList<String>();
    	if (numAddons > 0){
			for (int i = 1; i < numAddons + 1 ; i++) {
				addOns.add(driver.findElement(By.xpath(this.thisRowsXpath + this.subscriptionAddonGeneric_xpath + "[" + i + "]")).getText());
			}
    	}
    	return addOns;}
    
    public String getFirstName() {
        return this.firstName;
    }
    
    public String getLastName() {
        return this.lastName;
    }
    
    public String getCity() {
        return this.city;
    }
    
    public String getState() {
        return this.state;
    }
    
    
    /* ----- methods ----- */
    
    /**
     * <h1>assignLicense</h1>
     * <p>purpose: Click to open the content menu for the user<br>
     * 	Then click on the "Assign License" option<br>
     * 	Fail test if "Assign License" option is not visible</p>
     * @return ManageUsers
     */
    public ManageUsers assignLicense() {
    	System.out.println(GlobalVariables.getTestID() + " INFO: Assigning subscription license to user \"" + this.getFirstName() + " " + this.getLastName() + "\"" );
    	findByScrollIntoViewBottomOfScreen(By.xpath(this.thisRowsXpath));
        contextMenuButton().click();
        helperMethods.addSystemWait(3);
        waitForVisibility(By.xpath(this.assignLicense_xpath), 10);
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.assignLicenseOption());
        return new ManageUsers(driver);
    }
    
    public ManageUsers removeUser() {
        contextMenuButton().click();
        waitForVisibility(By.xpath(this.removeUser_xpath), 10);
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.removeUserOption());
        return new ManageUsers(driver);
    }
    
    /**
     * <h1>revokeLicense</h1>
     * <p>purpose: Click to open the content menu for the user<br>
     * 	Then click on the "Revoke License" option<br>
     *  When the confirmation popup appears, click "Confirm"<br>
     *  Fail test if "Revoke License" option isn't availble or<br>
     *  if popup doesn't appear</p>
     * @return ManageUsers
     */
    public ManageUsers revokeLicense() {
    	System.out.println(GlobalVariables.getTestID() + " INFO: Revoking subscription license from user \"" + this.getFirstName() + " " + this.getLastName() + "\"" );
    	findByScrollIntoViewBottomOfScreen(By.xpath(this.thisRowsXpath));
        contextMenuButton().click();
        waitForVisibility(By.xpath(this.revokeLicense_xpath), 10);
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.revokeLicenseOption());
        new ManageUsersConfirmRevokeLicense(driver)
        	.clickConfirmBtn();
    	return new ManageUsers(driver);
    }
    
    /* ----- verifications ----- */

//    /***
//     * <h1>verifySubscriptionPlanCorrect</h1>
//     * <p>purpose: Verify that the user's subscription plan name is displaying as expected for user plan<br>
//     * 	Fail test if the subscription plan name does not appear as expected</p>
//     * @param SubscriptionPlan
//     * @return UserRow
//     */
//    public UserRow verifySubscriptionPlanCorrect(SubscriptionPlan subscriptionPlan) {
//    	if (!helperMethods.doesPageObjectExist(driver, this.subscriptionName_xpath)) {
//    		fail("Test Failed: The Subscription Name is not appearing for user"); }
//    	
//    	String expectedPlan = subscriptionPlan.getChargebeeSubscriptionName();
//
//    	// There can be multiple subscriptions for each user, so check them all
//    	for (String name : this.getSubscriptionName()) {
//    		if (name.contains(expectedPlan)) {
//    			System.out.println(GlobalVariables.getTestID() + " INFO: Verified Subscription Name on Manage Users screen");
//    			return this; }
//    	}
//
//    	fail("Test Failed: The Subscription Name did not match expected plan name of \"" + expectedPlan + "\"");
//    	return null;
//    }

//    /***
//     * <h1>verifyGroupingCorrect</h1>
//     * <p>purpose: Verify that the user's subscription grouping is displaying as strGrouping<br>
//     * 	Fail test if the subscription grouping name does not match strGrouping or <br>
//     *  Fail test if no subscription grouping name is appearing and user does not have a BidSync National plan</p>
//     * @param subscriptionGrouping
//     * @return UserRow
//     */
//    public UserRow verifyGroupingCorrect(SubscriptionGrouping subscriptionGrouping) {
//    	// BidSync National Plan doesn't have a grouping
//    	// so skip verification if using a National Plan
//    	// TODO dmidura: this might now be true anymore. I'm seeing USA in that spot
//    	if (!subscriptionGrouping.equals(SubscriptionGrouping.NONE)) {
//			if (!helperMethods.doesPageObjectExist(driver, this.subscriptionGrouping_xpath) &
//				!this.getSubscriptionGrouping().contains("BidSync National")) {
//				fail("Test Failed: The Subscription Grouping is not appearing for user"); }
//			
//			// Grouping displays as "<Grouping> -", so remove the punctuation
//			this.getSubscriptionGrouping().forEach(grouping->
//				{
//					if(!grouping.replace(" -", "").equals(subscriptionGrouping.getChargebeeGroupingName())) { 
//						fail("Test Failed: The Subscription Grouping did not match expected grouping of " 
//							+ subscriptionGrouping.getChargebeeGroupingName());
//					}
//				});
//    	}
//    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified Subscription Grouping on Manage Users screen");
//    	return this;
//    }
//    
    /***
     * <h1>verifySubscriptionAndGroupingCorrect</h1>
     * <p>purpose: Verify that the User Row displays "subscriptionPlan - subscriptionGrouping" <br>
     * 	Fail test if User Row does not display "subscriptionPlan - subscriptionGrouping"</p>
     * @param subscriptionPlan = subscription plan you're expecting to display on User Row
     * @param subscriptionGrouping = associated grouping to go with the subscription plan
     * @return UserRow
     */
    public UserRow verifySubscriptionAndGroupingCorrect(SubscriptionPlan subscriptionPlan, SubscriptionGrouping subscriptionGrouping) {

    	// Make sure there's actually a subscription plan displaying
    	if (!this.doesPageObjectExist(driver, By.xpath(this.subscriptionNameGeneric_xpath))) {
    		fail("Test Failed: The Subscription Name is not appearing for user"); }
    	
    	String expectedPlan     = subscriptionPlan.getChargebeeSubscriptionName();
    	String expectedGrouping = subscriptionGrouping.getChargebeeGroupingName();

    	// There can be multiple subscriptions for each user, so check them all
    	int i= 1;
    	for (String name : this.getAllSubscriptionPlans()) {
    		if (name.contains(expectedPlan)) {
    			System.out.println(GlobalVariables.getTestID() + " INFO: Located match for Subscription Name \"" + expectedPlan + "\" on Manage Users screen. Determing if Grouping is \"" + expectedGrouping + "\"");

    			// The current subscription plan matches the name, but does its grouping correspond to what we're looking for?
    			String grouping = driver.findElement(By.xpath(this.thisRowsXpath 
    					+ this.subscriptionGenericLocator_xpath + "[" + i + "]" + this.subscriptionGroupingGeneric_xpath)).getText();
    			if (grouping.contains(expectedGrouping)) {
    				System.out.println(GlobalVariables.getTestID() + " INFO: Located match for Subscription Name \"" + expectedPlan + "\" with Grouping \"" + expectedGrouping + "\" on Manage Users screen");
    				return this;
    			}
    		}
    		i++;
    	}

    	fail("Test Failed: The Subscription Name did not match expected plan name of \"" + expectedPlan + "\" with grouping \"" + expectedGrouping + "\"");
    	return null;
    }

    /***
     * <h1>verifyAddOnCorrect</h1>
     * <p>purpose: Verify that the user's subscription add-ons are displaying as expected<br>
     * 	Fail test if the subscription add-ons are not displaying as expected</p>
     * @param List<SubscriptionAddOns> = up to three addOns
     * @return UserRow
     */
	public UserRow verifyAddOnCorrect(List<SubscriptionAddOns> addOns) {
		
		// Skip verification if we don't have any addOns to test for
		if (!addOns.isEmpty()) {
			if (addOns.size()>3) {fail("ERROR: There are more than three addOns");}
			
			List <String> subscriptionAddOns = this.getSubscriptionAddOns();
			
			for (SubscriptionAddOns addOn : addOns) {
				if (!subscriptionAddOns.contains(addOn.getChargebeeAddOnName())) {
					fail("Test Failed: The Subscription Add-Ons did not include expected add-on of " + addOn.getChargebeeAddOnName()); }
			}
		}
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified Subscription Add-ons on Manage Users screen");
		return this;
	}
	
	/***
	 * <h1>verifyOnlyBidSyncBasicSubscription</h1>
	 * <p>purpose: Verify that the user is displaying only a bidsync basic subscription<br>
	 * 	because the subscription plan is "BidSync Basic", a state exists for the grouping, and add-on is blank in the user row<br>
	 * 	Note: This method assumes that only one subscription is displayed and it is a BidSync Basic plan</p>
	 * @return UserRow
	 */
	public UserRow verifyOnlyBidSyncBasicSubscription() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Verifying user has BidSync Basic account");
		if (this.doesPageObjectExist(driver, By.xpath(this.thisRowsXpath + this.subscriptionAddonGeneric_xpath))) {
			fail ("Test Failed: User subscription information displays Add-On for BidSync Basic plan");
		} else if (!this.doesPageObjectExist(driver, By.xpath(this.thisRowsXpath + this.subscriptionGroupingGeneric_xpath))) {
			fail ("Test Failed: User subscription information does not display a state for BidSync Basic plan");
		} else if ( this.getAllSubscriptionPlans().size() != 1 ) {
			fail ("Test Failed: Expected just one subscription plan for user, but seeing \"" + this.getAllSubscriptionPlans().size() + "\"");
		} else if (!this.getAllSubscriptionPlans().get(0).contains("BidSync Basic")) {
			fail ("Test Failed: User subscription information should display \"BidSync Basic\", but is displaying \"" + this.getAllSubscriptionPlans().get(0) + "\"");}
	
		return this;
	}
	
	/***
	 * <h1>verifyCannotManageSubscription</h1>
	 * <p>purpose: Only the company owner can manage a subscription.<br>
	 * 	verify the current user cannot manage the subscription by opening<br>
	 * 	the content menu and verifying that the "Assign License" and<br>
	 * 	"Revoke License" options are not visible.<br>
	 * 	Fail test if either "Assign License" or "Revoke License" options<br>
	 * 	are visible</p>
	 *  Note: Currently, my user does not have a content menu available<br>
	 *  (i.e. they should not manage themselves).</p>
	 * @param String myUsersFirstName = my user's first name
	 * @param String myUsersLastName = my user's last name
	 * @return ManageUsers
	 */
	public ManageUsers verifyCannotManageSubscription(String myUsersFirstName, String myUsersLastName) {
		System.out.println(GlobalVariables.getTestID() + " INFO: Verifying cannot manage subscription");
		
		// User should not be able to view the "Assign License" or "Revoke License" buttons 
		// if the user cannot manage company subscriptions
		findByScrollIntoViewBottomOfScreen(By.xpath(this.thisRowsXpath));
		if (!this.getFirstName().equals(myUsersFirstName) & !this.getLastName().equals(myUsersLastName)) {
			contextMenuButton().click();
			if( this.doesPageObjectExist(driver, By.xpath(this.assignLicense_xpath)) |
				this.doesPageObjectExist(driver, By.xpath(this.revokeLicense_xpath)) ) {
				fail ("Test Failed: Non-subscription owner can view \"Assign License\" or \"Revoke License\" buttons for other users in Manage Users"); }
			
			// close the menu
			clickOffMouseFocus();
			waitForInvisibility(By.xpath(this.removeUser_xpath), 3);
		} 

		return new ManageUsers(driver);
	}

	private void clickOffMouseFocus() {driver.findElement(By.xpath("//div[@class=\"cdk-overlay-container\"]")).click();}
}
