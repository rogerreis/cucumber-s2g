package pages.common.pageObjects.toBeDeprecated;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.GlobalVariables;

/***
 * <h1>class SeleniumTweaks</h1>
 * <p>This class is an interface off of WebElements. Its primary purpose is to tweak the WebElement actions when needed</p>
 * @author dmidura
 * @deprecated - This class is a bit redundant with HelperMethods.  Moving copies of these methods into HelperMethods
 * and deprecating these until we can remove this class.
 *
 */
public class SeleniumTweaks {

	EventFiringWebDriver driver;

	public SeleniumTweaks(EventFiringWebDriver automation) {
		driver = automation;
	}
	
	/***
	* <h1>makeElementVisible</h1>
	* <p>purpose: When a WebElement is offscreen, use this method to scroll the screen</p> 
	* @param eleTarget = WebElement we want to see
	* @return WebElement = the WebElement
	*/
	@Deprecated
	public WebElement makeElementVisible(WebElement eleTarget)
	{
		if ( !isElementVisible(eleTarget)) {
			System.out.println(GlobalVariables.getTestID() + " INFO: WebElement is not visible. Moving it into view port.");
			
			// Our element's properties
			Point      ourEle = eleTarget.getLocation();
			Dimension ourSize = eleTarget.getSize();

			// Page offsets
			int heightOffset = 0;   // general offset for fine tuning
			int topBarHeight;   	// offset for static top bar element

			if (driver.getCurrentUrl().contains("supplier")) {
				topBarHeight = driver.findElement(By.id("bidsyncHeader")).getSize().height; // top static header bar 
				
				// Make sure we're not underneath the bottom environment static bar
				WebElement bottomEnvironmentBar = driver.findElement(By.xpath("//div[@class=\"environment ng-tns-c0-0 ng-star-inserted\"]"
						+ "[contains(text(), \"Environment\")]"));
				int environmentBarHeight	 = bottomEnvironmentBar.getSize().height;
				int rightEndOfEnvironmentBar = bottomEnvironmentBar.getLocation().x + bottomEnvironmentBar.getSize().width;
				
				int topOfEnvironmentBar    	 = bottomEnvironmentBar.getLocation().y;

//				System.out.println(GlobalVariables.getTestID() + " >>> ourEle.y                 = " + ourEle.y);
//				System.out.println(GlobalVariables.getTestID() + " >>> ourSize.height           = " + ourSize.height);
//				System.out.println(GlobalVariables.getTestID() + " >>> ourEle.x                 = " + ourEle.x);
//				System.out.println(GlobalVariables.getTestID() + " >>> ourSize.width            = " + ourSize.width);
//				System.out.println(GlobalVariables.getTestID() + " >>> bottomOfEnvironmentBar   = " + bottomOfEnvironmentBar);
//				System.out.println(GlobalVariables.getTestID() + " >>> environmentBarHeight     = " + environmentBarHeight);
//				System.out.println(GlobalVariables.getTestID() + " >>> topBarHeight             = " + topBarHeight);
//				System.out.println(GlobalVariables.getTestID() + " >>> rightEndOfEnvironmentBar = " + rightEndOfEnvironmentBar);
				
				if ( (ourEle.x + ourSize.width)  < rightEndOfEnvironmentBar &
					 (ourEle.y + ourSize.height) > topOfEnvironmentBar ) {
					System.out.println(GlobalVariables.getTestID() + " INFO: Object is covered by Environment Bar. Adding offset");
					heightOffset = environmentBarHeight;
				} 
				
				

			} else if (driver.getCurrentUrl().contains("shopify")) {
				//topBarHeight = driver.findElement(By.xpath("//nav[@class=\"navbar navbar-expand-md fixed-top\"]")).getSize().height; // top static header bar 
				topBarHeight = driver.findElement(By.xpath("//div[@id=\"shopify-section-header\"]/nav")).getSize().getHeight();
				heightOffset = 0;
			} else {
				topBarHeight = 0; 
				heightOffset = 0; }
			
			int XCoordinate = ourEle.x;
			//int YCoordinate = ourEle.y + ourSize.height/2 - topBarHeight - heightOffset;
			int YCoordinate = ourEle.y - topBarHeight + heightOffset;

			// Comment out for debug purposes
//			System.out.printf(GlobalVariables.getTestID() + " XCoordinate >>> %s\n", XCoordinate);
//			System.out.printf(GlobalVariables.getTestID() + " YCoordinate >>> %s\n", YCoordinate);

			// Scroll to object (actual scroll is to the bottom left corner of the object with optional offset included)
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollTo(" + XCoordinate + "," + YCoordinate  + ");");
			
//			System.out.println(GlobalVariables.getTestID() + " >>> New Y = " + eleTarget.getLocation().y);
//			System.out.println(GlobalVariables.getTestID() + " >>> New X = " + eleTarget.getLocation().x);
		} else {
			System.out.println(GlobalVariables.getTestID() + " INFO: WebElement is already visible.");
		}
		
		return eleTarget;
	}
	
	/***
	 * <h1>isElementVisible</h1>
	 * <p>purpose: Determine if a WebElement is offscreen obscured by another object (say, the org bar) </p>
	 * @param element = WebElement to search for
	 * @return true if visible, false if not visible
	 */
	@Deprecated
	private Boolean isElementVisible(WebElement element) {
		System.out.println(GlobalVariables.getTestID() + " INFO: Checking if WebElement is in view port.");
		return (Boolean)((JavascriptExecutor)driver).executeScript(
		  "var elem = arguments[0],                 " +
		  "  box = elem.getBoundingClientRect(),    " +
		  "  cx = box.left + box.width / 2,         " +
		  "  cy = box.top + box.height / 2,         " +
		  "  e = document.elementFromPoint(cx, cy); " +
		  "for (; e; e = e.parentElement) {         " +
		  "  if (e === elem)                        " +
		  "    return true;                         " +
		  "}                                        " +
		  "return false;                            "
		  , element);
	} 

}

