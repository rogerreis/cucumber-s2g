package pages.chargebeeSite.customers;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.common.helpers.GlobalVariables;

/***
 * <h1>CustomersRow</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects for a single row in the Customers table<br>
 * 			   on the Chargebee test site</p>
 */
public class CustomersRow extends CustomersTable {
	// Elements in the table - add more as needed
	// Column 1
	private final String customerId_xpath        = "//dl[@data-cb-col-index='1']/dd[1]//span";

	// Column 2
	private final String name_xpath              = "//dl[@data-cb-col-index='2']/dd[1]/span";
	private final String email_xpath             = "//dl[@data-cb-col-index='2']/dd[2]/span";
	private final String phoneNbr_xpath          = "//dl[@data-cb-col-index='2']/dd[4]/span";
	
	// Other
	private String strRootPath = "";
	
	public CustomersRow (EventFiringWebDriver driver, String strXPathToRow) {
		super(driver);
		this.strRootPath = strXPathToRow;
	}
	
	/* ----- getters ----- */
	private WebElement getCustomerId()      {return findByVisibility(By.xpath(this.strRootPath + this.customerId_xpath));      }
	private WebElement getName()            {return findByVisibility(By.xpath(this.strRootPath + this.name_xpath));            }
	private WebElement getEmail()           {return findByVisibility(By.xpath(this.strRootPath + this.email_xpath));           }
	private WebElement getPhoneNbr()        {return findByVisibility(By.xpath(this.strRootPath + this.phoneNbr_xpath));        }
	
	/* ----- helpers ----- */

	/***
	 * <h1>printCustomerInfo</h1>
	 * <p>purpose: Print customer info to screen<br>
	 * 	For debug purposes</p>
	 */
	private void printCustomerInfo() {
		System.out.println(GlobalVariables.getTestID() + " Name         = " + this.getFirstNameText() + " " + this.getLastNameText());
		System.out.println(GlobalVariables.getTestID() + " Email        = " + this.getEmailText());
		System.out.println(GlobalVariables.getTestID() + " Phone Nbr    = " + this.getPhoneNbrText());
		System.out.println(GlobalVariables.getTestID() + " Customer Id  = " + this.getCustomerIdText());
	}

	/***
	 * <h1>getFirstNameText</h1>
	 * <p>purpose: Grab the first name displaying in the Customer Row and return</p>
	 * @return first name
	 */
	private String getFirstNameText() { return this.getName().getText().replaceFirst("[\\s]{1}\\d*\\D*\\w*\\W*$","").trim(); }
	
	/***
	 * <h1>getLastNameText</h1>
	 * <p>purpose: Grab the last name displaying in the Customer Row and return</p>
	 * @return last name
	 */
	private String getLastNameText() { return this.getName().getText().replaceFirst("^\\d*\\D*\\w*\\W*[\\s]{1}","").trim(); }
	
	/***
	 * <h1>getEmailText</h1>
	 * <p>purpose: Grab the email displaying in the Customer Row and return</p>
	 * @return email
	 */
	private String getEmailText() { return this.getEmail().getText().trim(); }
	
	/***
	 * <h1>getPhoneNbrText</h1>
	 * <p>purpose: Grab the phone number displaying in the Customer Row and return</p>
	 * @return phone number
	 */
	// dmidura: Sometimes Chargebee will store the +1 before the 10 digit phone number.
	private String getPhoneNbrText() { return this.getPhoneNbr().getText().replaceFirst("\\+1", ""); }

	/* ----- methods ----- */

	/***
	 * <h1>getCustomerIdText</h1>
	 * <p>purpose: Grab the customer id displaying in the Customer Row and return</p>
	 * @return the customer id
	 */
	public String getCustomerIdText() { return this.getCustomerId().getText().trim(); }
	
	/* ----- verifications ----- */
	/***
	 * <h1>verifyFirstName</h1>
	 * <p>purpose: Verify first name listed in the Customer Row is firstName.
	 * 	Fail test if not</p>
	 * @param firstName = String containing customer's first name
	 * @return CustomersRow
	 */
	public CustomersRow verifyFirstName (String firstName) {
		Assert.assertEquals("Test Failed: Chargebee customer row displays wrong first name", firstName, this.getFirstNameText());
		return this;
	}
	
	/***
	 * <h1>verifyLastName</h1>
	 * <p>purpose: Verify last name listed in the Customer Row is lastName.
	 * 	Fail test if not</p>
	 * @param lastName = String containing customer's last name
	 * @return CustomersRow
	 */
	public CustomersRow verifyLastName (String lastName) {
		Assert.assertEquals("Test Failed: Chargebee customer row displays wrong last name", lastName, this.getLastNameText());
		return this;
	}
	
	/***
	 * <h1>verifyEmail</h1>
	 * <p>purpose: Verify email listed in the Customer Row is email.
	 * 	Fail test if not</p>
	 * @param email = String containing user's registration email
	 * @return CustomersRow
	 */
	public CustomersRow verifyEmail (String email) {
		Assert.assertEquals("Test Failed: Chargebee customer row displays wrong email", email, this.getEmailText());
		return this;
	}

	/***
	 * <h1>verifyPhoneNumber</h1>
	 * <p>purpose: Verify phone number listed in the Customer Row is phoneNumber.
	 * 	Fail test if not</p>
	 * @param phoneNumber = String containing 10 digit phone number
	 * @return CustomersRow
	 */
	public CustomersRow verifyPhoneNumber (String phoneNumber) {
		Assert.assertEquals("Test Failed: Chargebee customer row displays wrong phone number", phoneNumber, this.getPhoneNbrText());
		return this;
	}

	/***
	 * <h1>verifyCustomerRecord</h1>
	 * <p>purpose: Verify that the Chargebee customer row data matches the
	 * 	RandomChargebeeUser data based on first name, last name, phone number, email</p>
	 * @param user = RandomChargebeeUser registration data
	 * @return CustomersRow
	 */
	public CustomersRow verifyCustomerRecord (RandomChargebeeUser user) {
		this
			.verifyFirstName(user.getFirstName())
			.verifyLastName(user.getLastName())
			.verifyPhoneNumber(user.getPhoneNumber())
			.verifyEmail(user.getEmailAddress());
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified Customer Info is correct");
		this.printCustomerInfo();
		return this;
	}
	
	
}
