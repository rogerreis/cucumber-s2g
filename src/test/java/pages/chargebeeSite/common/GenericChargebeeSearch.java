package pages.chargebeeSite.common;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>GenericChargebeeSearch</h1>
 * @author dmidura
 * <p>details: This class houses methods and page objects for a generic search bar that appears above almost<br>
 *  every dashboard table in the Chargebee test site<br>
 * 	Includes: input, search button</p>
 */
public class GenericChargebeeSearch extends BasePageActions {
	
	//Other
	private EventFiringWebDriver driver;

	public GenericChargebeeSearch(EventFiringWebDriver driver) {
		super(driver);
		this.driver 	= driver;
	}
	
	/* ----- getters ----- */
    private WebElement getSearchByInput() { return findByVisibility(By.xpath(this.getSearchByInput_xpath()));                      }
    private WebElement getSearchBtn()     { return findByVisibility(By.xpath(this.getSearchBtn_xpath()));                          }
    private String getCurrentPage()       { return driver.getCurrentUrl().replace(System.getProperty("chargebeeTestSiteURL"), ""); }
    
    
    // constructed xpaths
    private String getSearchByInput_xpath() {
        return "//form[@action='/" + this.getCurrentPage() + "']//input[@data-cb-table-option-element='search']"; }
    
    private String getSearchBtn_xpath() {
        return "//form[@action='/" + this.getCurrentPage() + "']//input[@type='submit']"; }
		
	/* ----- methods ----- */

	/***
	 * <h1>SearchTable</h1>
	 * <p>purpose: Enter the strSearch into the search input and then search</p>
	 * @param strSearch = value to search table for
	 * @return GenericChargebeeTable
	 */
	public GenericChargebeeSearch SearchTable(String strSearch) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Searching for %s\n", strSearch);

		String currentURL = driver.getCurrentUrl();
		int time          = DEFAULT_TIMEOUT;
		
		// Enter search term and click on Search. Give app time to navigate
		this.getSearchByInput().sendKeys(strSearch);
		this.getSearchBtn().click();
		new WebDriverWait(driver, time)
			.withMessage("Test Failed: After \"" + time + "\" seconds, search results did not appear")
			.until(ExpectedConditions.not(ExpectedConditions.urlToBe(currentURL)));
		return this;
	}
}
