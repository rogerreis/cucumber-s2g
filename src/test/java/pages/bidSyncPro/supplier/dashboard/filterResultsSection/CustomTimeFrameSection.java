package pages.bidSyncPro.supplier.dashboard.filterResultsSection;

import static org.junit.Assert.fail;

import java.util.Calendar;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.DateHelpers;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

/***
 * <h1>Class CustomTimeFrameSection</h1>
 * @author dmidura
 * <p>details: This class houses the objects and methods for the Custom Time Frame section on the Filter Results<br>
 * 	This section becomes visible after the user has selected "Custom Time Frame" from the "Time Frame Selection" of the<br>
 * 	Bid End Date section.</p>
 */
public class CustomTimeFrameSection extends SelectTimeFrameDropdown {
	// Other
	private DateHelpers dateHelpers;
	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;
	
	// xpaths
	private final String showBidsFromInput_xpath = "//input[@id='txtStartDate'][@placeholder='Show Bids From']";
	private final String showBidsToInput_xpath   = "//input[@id='txtEndDate'][@placeholder='Show Bids to']";

	public CustomTimeFrameSection(EventFiringWebDriver driver) {
		super(driver);
		this.driver   = driver;
		dateHelpers   = new DateHelpers();
		helperMethods = new HelperMethods();
	}
	
	/* ----- getters ----- */
	private WebElement getShowBidsFromInput() { return findByVisibility(By.xpath(this.showBidsFromInput_xpath)); }
	private WebElement getShowBidsToInput()   { return findByVisibility(By.xpath(this.showBidsToInput_xpath));   }

	/* ----- setters ----- */
	
	/***
	 * <h1>setShowBidsFromInput</h1>
	 * <p>purpose: When using a Custom Time Frame, set the "Show Bids From" date on the Bid End Date </p>
	 * @param Calendar date
	 * @return CustomTimeFrameSection
	 */
	public CustomTimeFrameSection setShowBidsFromInput (Calendar date) {
		String strDate = dateHelpers.getCalendarDateAsString(date);
		((JavascriptExecutor)driver).executeScript("arguments[0].value = '';", this.getShowBidsFromInput());
		this.getShowBidsFromInput().sendKeys(strDate);
		return this;
	}

	/***
	 * <h1>setShowBidsFromInput</h1>
	 * <p>purpose: When using a Custom Time Frame, set the "Show Bids From" date on the Bid End Date </p>
	 * @param strDate = MM/DD/YYYY
	 * @return CustomTimeFrameSection
	 */
	public CustomTimeFrameSection setShowBidsFromInput (String strDate) {
//		((JavascriptExecutor)driver).executeScript("arguments[0].value = '';", this.getShowBidsFromInput());
		this.getShowBidsFromInput().sendKeys(Keys.chord(Keys.CONTROL, "a"));
		this.getShowBidsFromInput().sendKeys(strDate);
		return this;
	}

	/***
	 * <h1>setShowBidsToInput</h1>
	 * <p>purpose: When using a Custom Time Frame, set the "Show Bids To" date on the Bid End Date </p>
	 * @param Calendar date
	 * @return CustomTimeFrameSection
	 */
	public CustomTimeFrameSection setShowBidsToInput (Calendar date) {
		String strDate = dateHelpers.getCalendarDateAsString(date);
		((JavascriptExecutor)driver).executeScript("arguments[0].value = '';", this.getShowBidsToInput());
		this.getShowBidsToInput().sendKeys(strDate);
		return this;
	}

	/***
	 * <h1>setShowBidsToInput</h1>
	 * <p>purpose: When using a Custom Time Frame, set the "Show Bids To" date on the Bid End Date </p>
	 * @param strDate = MM/DD/YYYY
	 * @return CustomTimeFrameSection
	 */
	public CustomTimeFrameSection setShowBidsToInput (String strDate) {
		((JavascriptExecutor)driver).executeScript("arguments[0].value = '';", this.getShowBidsToInput());
		this.getShowBidsToInput().sendKeys(strDate);
		return this;
	}
	
	/***
	 * <h1>getShowBidsToInputText</h1>
	 * <p>purpose: Return the currently displayed value of the<br>
	 * 	"Show Bids to" field. (View be formatted as MM/DD/YYYY)</p>
	 * @return MM/DD/YYYY value displayed for "Show Bids To"
	 */
	public String getShowBidsToInputText() {
		return dateHelpers.formatDateWithWrongLength(this.getShowBidsToInput().getAttribute("value"), '/');
	}

	/***
	 * <h1>getShowBidsFromInputText</h1>
	 * <p>purpose: Return the currently displayed value of the<br>
	 * 	"Show Bids From" field. (View be formatted as MM/DD/YYYY)</p>
	 * @return MM/DD/YYYY value displayed for "Show Bids From"
	 */
	public String getShowBidsFromInputText() {
		return dateHelpers.formatDateWithWrongLength(this.getShowBidsFromInput().getAttribute("value"), '/');
	}
	
	/* ----- Verifications ----- */
	
	/***
	 * <h1>verifyShowBidsFromDisplaysDate</h1>
	 * <p>purpose: Verify that "Show Bids From" field is currently displaying<br>
	 * 	expectedDate. Fail test if "Show Bids From" is not displaying expectedDate</p>
	 * @param expectedDate = String of expected "Show Bids From" date (MM/DD/YYYY)
	 * @return
	 */
	public CustomTimeFrameSection verifyShowBidsFromDisplaysDate(String expectedDate) {
		
		// We will string compare the expectedDate to the displayedDate
		if (!expectedDate.equals(this.getShowBidsFromInputText())){
			fail("Test Failed: Expected \"Show Bids From\" input to display \"" + 
					expectedDate + "\" but instead, \"" + this.getShowBidsFromInputText() + "\" is displayed"); }
		
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"Show Bids From\" displays expected date " + "\"" + expectedDate + "\"");
		return this;
	}

	/***
	 * <h1>verifyShowBidsToDisplaysDate</h1>
	 * <p>purpose: Verify that "Show Bids to" field is currently displaying<br>
	 * 	expectedDate. Fail test if "Show Bids to" is not displaying expectedDate</p>
	 * @param expectedDate = String of expected "Show Bids to" date (MM/DD/YYYY)
	 * @return
	 */
	public CustomTimeFrameSection verifyShowBidsToDisplaysDate(String expectedDate) {
		
		// We will string compare the expectedDate to the displayedDate
		// (since the displayedDate is returned as a String and we don't have greater
		// precision than MM/DD/YYYY)
		if (!expectedDate.equals(this.getShowBidsToInputText())){
			fail("Test Failed: Expected \"Show Bids To\" input to display \"" + 
					expectedDate + "\" but instead, \"" + this.getShowBidsToInputText() + "\" is displayed"); }
		
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"Show Bids to\" displays expected date " + "\"" + expectedDate + "\"");
		return this;
	}

	/***
	 * <h1>verifyShowBidsToValidationWasTriggered</h1>
	 * <p>purpose: Verify that the "Show Bids to" input field has currently triggered validation<br>
	 * 	Fail test if validation is not currently triggered.</p>
	 * @return CustomTimeFrame
	 */
	public CustomTimeFrameSection verifyShowBidsToValidationWasTriggered() {
		if (!this.getShowBidsToInput().getAttribute("aria-invalid").equals("true")) {
			fail ("Test Failed: \"Show Bids to\" input did not trigger validation"); }
		return this;
	}
	
	/***
	 * <h1>verifyCustomTimeFrameDateSelectorsAreVisible</h1>
	 * <p>purpose: Verify that "Show Bids From" and "Show Bids to"<br>
	 * 	Custom Time Frame date selectors are currently visible.<br>
	 * 	Fail test if either is not visible</p>
	 * @return CustomTimeFrameSection
	 */
	public CustomTimeFrameSection verifyCustomTimeFrameDateSelectorsAreVisible() {
		if (!this.doesPageObjectExist(driver, By.xpath(this.showBidsFromInput_xpath))) {
			fail("Test Failed: \"Show Bids From\" input is not visible"); }

		if (!this.doesPageObjectExist(driver, By.xpath(this.showBidsToInput_xpath))) {
			fail("Test Failed: \"Show Bids To\" input is not visible"); }
		
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that Custom Time Frame date selectors are currently visible");
		return this;
	}

	/***
	 * <h1>verifyCustomTimeFrameDateSelectorsAreNotVisible</h1>
	 * <p>purpose: Verify that "Show Bids From" and "Show Bids to"<br>
	 * 	Custom Time Frame date selectors are currently not visible.<br>
	 * 	Fail test if either is visible</p>
	 * @return CustomTimeFrameSection
	 */
	public CustomTimeFrameSection verifyCustomTimeFrameDateSelectorsAreNotVisible() {
		if (this.doesPageObjectExist(driver, By.xpath(this.showBidsFromInput_xpath))) {
			fail("Test Failed: \"Show Bids From\" input is visible"); }

		if (this.doesPageObjectExist(driver, By.xpath(this.showBidsToInput_xpath))) {
			fail("Test Failed: \"Show Bids To\" input is visible"); }
		
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that Custom Time Frame date selectors are not currently visible");
		return this;
	}
	
	/***
	 * <h1>verifyShowBidsFromDisplaysDefault</h1>
	 * <p>purpose: Verify that "Show Bids From" date selector<br>
	 * 	will default to "1/1/2016" when a "Custom Time Frame" is selected<br>
	 * 	from "SELECT TIME FRAME" dropdown</p>
	 * @return CustomTimeFrameSection
	 */
	public CustomTimeFrameSection verifyShowBidsFromDisplaysDefault() {
		return this.verifyShowBidsFromDisplaysDate("01/01/2016");
	}
}
