package stepDefinitions.bidSyncPro.supplier.login;

import core.CoreAutomation;

import static org.junit.Assert.fail;

import org.junit.Assert;

import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.bidSyncPro.registration.RandomChargebeeUser;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class LoginPage_stepDefinitions {

	public EventFiringWebDriver driver;
	SupplierLogin supplierLogin;

	public LoginPage_stepDefinitions(CoreAutomation automation) {
		this.driver       = automation.getDriver();
		supplierLogin = new SupplierLogin(driver);
	}
	
	/* ----- helpers ----- */
	
	/***
	 * <h1>sizeBrowserPerUser</h1>
	 * <p>purpose: Resize browser window depending on user is local or selenium grid</p>
	 */
	private void sizeBrowserPerUser() {
		if (System.getProperty("env").equals("grid")) {
			if (System.getProperty("browser").equals("chrome")) {
				// can't just maximize chrome, you have to set specific resolution
				//driver.manage().window().setSize(new Dimension(1366, 768));
				driver.manage().window().setSize(new Dimension(1440, 1440));
			}
		}
		// don't let IE11 systems resize the browser smaller 
		if (System.getProperty("browser").equals("IE11")) {
			driver.manage().window().maximize();
		}

		System.out.printf(GlobalVariables.getTestID() + " INFO: Browser resolution = %s\n", driver.manage().window().getSize());
	}
	
	/***
	 * <h1>handleThreeLevelAccess</h1>
	 * <p>purpose: Three Level Access can sometimes login from previous test. If so, logout</p>
	 * @throws InterruptedException 
	 */
	private void handleThreeLevelAccess() throws InterruptedException {

		// if the 3 level access has us login as the user from the last test, logout
		if(driver.getCurrentUrl().contains("dashboard/new-bids")) {
            System.out.print("INFO: Previous test is still logged in.  Logging out...\n");
			// wait for the page to finish loading before trying to log out.
			new BidList(driver)
				.waitForPageLoad()
				.logout();
		}
	}
	
	/***
	 * <h1>loginWithCredentials</h1>
	 * <p>purpose: Login with credentials and wait for the page to load
	 * @param user = RandomChargebeeUser with user login credentials
	 * @return CommonTopBar
	 */
	private CommonTopBar loginWithCredentials(RandomChargebeeUser user) {
		return new SupplierLogin(driver)
			.loginWithCredentials(user.getEmailAddress(), user.getPassword())
			.waitForPageLoad();
	}
	
	/***
	 * <h1>loginWithReference</h1>
	 * <p>purpose: Login by user reference and wait for the page to load
	 * @param userReference = user reference associated with user
	 * @return CommonTopBar
	 */
	private CommonTopBar loginWithReference (String userReference) {
		return new SupplierLogin(driver)
			.loginAsUser(userReference)
			.waitForPageLoad();
	}
    
	/***
	 * <h1>logout</h1>
	 * <p>purpose: Log out of BidSync Pro</p>
	 * @return SupplierLogin
	 * @throws InterruptedException 
	 */
	private SupplierLogin logout() throws InterruptedException {
		return new BidList(driver)
			.logout();
	}
	
	/* ----- test steps ----- */

	@Given("^I navigate my browser to the supplier login page$")
	public void iNavigateToTheSupplierLoginPage() throws InterruptedException {
		new SupplierLogin(driver)
			.navigateToHere();
		this.sizeBrowserPerUser();
		this.handleThreeLevelAccess();;
	}

	// System Logins
	/***
	 * <h1>I_have_logged_into_the_Supplier_side_as_user</h1>
	 * </p>purpose: Use this for a quick login</p>
	 * @param userReference = the reference name for the user you want to login as<br>
	 *        Note: The reference name should match a reference name given in useAccounts_ARGO.json
	 * @return None
	 * @throws InterruptedException 
	 */
	@Given ("^I have logged into the Supplier side as user \"(.*)\"$")
	public void I_have_logged_into_the_Supplier_side_as_user (String userReference) throws InterruptedException {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Logging into the supplier side as user %s\n", userReference);

		new SupplierLogin(driver).navigateToHere();
		this.sizeBrowserPerUser();
		this.handleThreeLevelAccess();
		this.loginWithReference(userReference).waitForSpinner(driver);
	}

	@Given("^I log into BidSync Pro as my new BidSync Basic user$")
	public void iEnterMyEmailFromTheGlobalVariableAndPassword() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Logging in as new user");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		this.loginWithCredentials(user);
	}

	@Given("^I log into BidSync Pro as the company owner$")
	public void I_log_into_BidSync_Pro_as_the_company_owner() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Logging in as new user");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		this.loginWithCredentials(user);
	} 
	
	@When ("^I log out and then back into BidSync Pro$")
	public void I_log_out_and_then_back_into_BidSync_Pro() throws InterruptedException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Logging out and then back in with current user");
		String userReference = SupplierLogin.thisUser.getAccountReference();
		this.logout();
		this.loginWithReference(userReference);
	}

	@When("^I enter my Email as \"([^\"]*)\" and Password as \"([^\"]*)\"$")
	public void I_enter_my_Email_as_and_Password_as (String arg1, String arg2) {
		new SupplierLogin(driver)
			.loginWithCredentials(arg1, arg2);
	}
	
	// Login Page 

	@When("^I click Try for free$")
	public void i_click_try_for_free() {
		// TODO dmidura: update typo in window title after Masonry fixes per ARGO-2459
		new SupplierLogin(driver)
			.clickTryForFree()
			.switchToWindow("Bidsync Basic | BidSync");
	}

	@Then("^I will be on the supplier Home page$")
	public void i_will_be_on_the_supplier_home_page() throws Throwable {		
		// Successful login result.
		// Let the page load to verify that login has occurred
		new CommonTopBar(driver)
			.waitForPageLoad();
		String expected_url = new BidList(driver).getPageURL();
		String actual_url   = driver.getCurrentUrl();
		String message      = "Test failed: Login was unsuccessful!";
		Assert.assertEquals(message, expected_url, actual_url);
	}
			
	@Then("^I will be on the supplier registration page$")
	public void iWillBeOnTheSupplierRegistrationPage() {
		String actual_url = driver.getCurrentUrl();
        System.out.println(GlobalVariables.getTestID() + " Actual URL Returned: "+actual_url+"\n");
        String message = "Test failed: Registration page was NOT loaded.";
        Assert.assertTrue(message, actual_url.contains("builtbymasonry.com"));
	}
	
	@Then("^I will see a login error$")
	public void i_Will_see_a_login_Error() {
		String login_error = "Email/Password combination does not match.";
		String actual_error = driver.findElement(By.id("loginErrorMessage")).getText();
		System.out.println(actual_error+"\n");
		String message = "Test failed. Login correct error was NOT displayed.";
		Assert.assertEquals(message, login_error, actual_error);
	}
	
	@Then("^I will be locked out for 20 min")
	public void i_will_be_locked_out_for_20_min() {
		new HelperMethods().addSystemWait(5);
		String acct_locked_msg = "You have entered an incorrect Login ID and/or Password too many times,"
				+"and will be locked out for 20 minutes. During this time you will be unable to log in.";
		String actual_locked_msg = driver.findElement(By.id("loginErrorMessage")).getText();
		System.out.println(acct_locked_msg+"\n");
		String message = "Test failed. Locked error was NOT displayed.";
		Assert.assertTrue(message, actual_locked_msg.contains("20 minutes"));
	}
		
	// Logout
	
	@And("^I logout of BidSync Pro$")
	public void logoutPro() throws InterruptedException {
		this.logout();
	}	
	
	@And("^I will punchout to Marketplace")
	public void iWillPunchoutToMarketplace() throws Throwable {		
		driver.findElement(By.xpath("//(mat-icon[contains(text(),'library_books')]")).click();
	}
	
	@And("^I log out and login back into BidSync Pro as my new BidSync Basic user$")
	public void logoutProAndLoginBackAsNewBasicUser() throws InterruptedException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Logout and Logging in as new user");
		this.logout();
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		this.loginWithCredentials(user);
	}
	
	@Given("^attempt 6 plus times to login to lock the account$")
	public void attempt6PlusTimesToLoginToLockTheAccount() {
		try {
			System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Login as user whose statue is R(Registered; not verified)");
			RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
			for(int i=0;i<=6;i++) {
				supplierLogin.getUserNameField().clear();
				supplierLogin.getUserNameField().sendKeys(user.getEmailAddress());
				supplierLogin.getPasswordField().clear();
				supplierLogin.getPasswordField().sendKeys(user.getPassword());
				supplierLogin.getLoginButton().click();
				new HelperMethods().addSystemWait(1);
			}
		}catch(Exception exp) {
			fail("FAILED : Attempt 6 times to login to lock the account" + exp);
		}
	}
	
	
}
