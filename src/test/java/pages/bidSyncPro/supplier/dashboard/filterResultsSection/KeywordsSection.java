package pages.bidSyncPro.supplier.dashboard.filterResultsSection;

import static org.junit.Assert.fail;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.common.ChipList;
import pages.common.helpers.HelperMethods;

/***
 * <h1>Class KeywordsSection</h1>
 * @author dmidura
 * <p>details: This class houses the objects and methods for the Keywords section on the Filter Results</p>
 */
public class KeywordsSection extends FilterResultsSectionNewClass {

	// Other
	private EventFiringWebDriver driver;
	HelperMethods helperMethods = new HelperMethods();

	private final String PlaceholderMessage = "Enter Keywords";

	// xpaths
	private final String keywordsInput_xpath       = "//input[@id='txtAddKeyword']";
	private final String keywordsInputBox_xpath    = "//mat-form-field[@id='matAddKeywordFormField']//input[@id='txtAddKeyword']";
	private final String keywordsUnderline_xpath   = "//mat-form-field[@id='matAddKeywordFormField']//div[contains(@class, 'mat-form-field-underline')]";
	private final String keywordsAddBtn_xpath      = "//button[@id='btnAddKeyword']";

	//#Fix for release OCT 7
//	private final String keywordsChipList_xpath    = "//mat-chip-list[@id='matKeywordChipList']";
	private final String keywordsChipList_xpath    = "//mat-expansion-panel[@id='matExpKeywordPanel']//mat-chip-list";
	private final String keywordsChipListAfterBackToBid_xpath    = "//mat-chip-list[@id='mat-chip-list-3']";

	private final String addedKeyword_xpath    = "//mat-chip/span[contains(text(),'#keyword#')]";

	public KeywordsSection(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	/* ----- getters ----- */
	private WebElement getKeywordsAddBtn()    { return findByScrollIntoViewBottomOfScreen(By.xpath(this.keywordsAddBtn_xpath)); }
	private WebElement getKeywordsInput()     { return findByVisibility(By.xpath(this.keywordsInput_xpath));                    }
	private WebElement getKeywordsInputBox()  { return findByVisibility(By.xpath(this.keywordsInputBox_xpath));                 }
	private WebElement getKeywordsUnderline() { return findByVisibility(By.xpath(this.keywordsUnderline_xpath));                }
	private WebElement getAddedKeyword(String keyword) { return findByVisibility(By.xpath(this.addedKeyword_xpath.replace("#keyword#", keyword)));                }

	/* ----- methods ----- */

	/***
	 * <h1>clickAddBtn</h1>
	 * <p>purpose: Click on the add (+) button on the Keyword Input</p>
	 * @return KeywordsSection
	 */
	private KeywordsSection clickAddBtn() { 
		this.getKeywordsAddBtn().click(); 
		return this;
	}
	
	/***
	 * <h1>addKeyword</h1>
	 * <p>purpose: Add a keyword into the input and then click on "+"</p>
	 * @param strKeyword = your keyword to add
	 * @return KeywordsSection
	 */
	public KeywordsSection addKeyword (String strKeyword) {
		this.getKeywordsInput().sendKeys(strKeyword);
		this.clickAddBtn();
		return new KeywordsSection(driver);
	}
	
	/***
	 * <h1>addKeywords<h1>
	 * <p>purpose: Add a list of keywords into the input, clicking on "+" after each addition</p>
	 * @param keywordsList = list of all your keywords
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass addKeywords (List <String> keywordsList){
		for (String strKeyword : keywordsList) {
			helperMethods.addSystemWait(1,TimeUnit.SECONDS);
			this.getKeywordsInput().sendKeys(strKeyword);
			this.clickAddBtn();
			
	        // if search term is over 25 characters, then truncate like the UI does
	        if (strKeyword.length() > 25) {
	        	strKeyword = strKeyword.substring(0, 25) + "..."; 
	        }
			
			getAddedKeyword(strKeyword);
		}
		return new FilterResultsSectionNewClass(driver);
	}

	/***
	 * <h1>getPositiveKeywordsChipList</h1>
	 * <p>purpose: Access the Positive Keywords ChipList</p>
	 * @return ChipList
	 */
	public ChipList getPositiveKeywordsChipList(){
		return new ChipList(driver, this.keywordsChipList_xpath);
	}

//#Fix for release OCT 7
	/***
	 * <h1>getPositiveKeywordsChipList</h1>
	 * <p>purpose: Access the Positive Keywords ChipList after click back to bid</p>
	 * @return ChipList
	 */
	public ChipList getPositiveKeywordsChipListAfterBackToBid(){
		return new ChipList(driver, this.keywordsChipListAfterBackToBid_xpath);
	}

	/* ----- verifications ----- */

	/***
	 * <h1>verifySectionInitalization</h1>
	 * <p>purpose: Verify that the Keywords section has initialized per:<br>
	 * 	1. input bar with "+" button<br>
	 * 	2. Default placeholder message in input of "Enter Keywords"<br>
	 * 	3. Chip list is empty<br>
	 *	4. Section header opens/closes<br>
	 * 	Fail test if any of the above conditions are not met</p>
	 * @return KeywordsSection
	 */
	public KeywordsSection verifySectionInitialization() {
	
		// 1. Verify that input with "+" button is visible
		if(!this.getKeywordsInput().isDisplayed()) {
			fail("Test Failed: \"Keywords\" input is not displayed when initializing \"Keywords\" section"); }
		
		if(!this.getKeywordsUnderline().isDisplayed()) {
			fail("Test Failed: \"Keywords\" input underline is not displayed when initializing \"Keywords\" section"); }

		if(!this.getKeywordsAddBtn().isDisplayed()) {
			fail("Test Failed: \"Keywords\" add button (\"+\") is not displayed when initializing \"Keywords\" section"); }
		
		// And the correct placeholder message is displaying
		if (!this.getKeywordsInputBox().getAttribute("placeholder").equals(this.PlaceholderMessage)) {
			fail("Test Failed: Keywords input did not initialize with expected placeholder \"" + this.PlaceholderMessage + "\""); }
		
		// 2. Chip list should be >= 3 chips (max number of default registration chips is 3)
//		commenting out this test as we do not have 3 keywords for this login
//		if (this.getPositiveKeywordsChipList().getAllChipsInChipList().size() < 3 ) {
//			fail("Test Failed: \"Keywords\" chip list is smaller than 3 default registration chips");}
		
		// 3. Close/Open "Keywords" section header and verify section is closed/open
		this.closeKeywordsSection()
			.verifyKeywordsSectionIsClosed()
			.openKeywordsSection()
			.verifyKeywordsSectionIsOpen();

		return this;
	}
}
