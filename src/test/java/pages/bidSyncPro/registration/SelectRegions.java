package pages.bidSyncPro.registration;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import data.bidSyncPro.registration.SalesRegion;
import pages.bidSyncPro.common.BidSyncProCommon;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidListRow;
import pages.common.helpers.GeographyHelpers;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.helpers.RandomHelpers;

/***
 * <h1>Class SelectRegions</h1>
 * @author ssomaraj
 * <p>details: This class houses methods and objects related to the objects on the "Select Regions" screen that is part of supplier registration</p>
 */
public class SelectRegions extends BidSyncProCommon{

	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;
	
	private final String usAndCanada_xpath 			= "//div[@class='both-image']";
	private final String usRegion_xpath				= "//div[@class='us-image']";
	private final String statesOrProvinces_xpath	= "//div[@class='map-image']";
	private final String regionsPageHead_xpath 		= "//mat-card-title[@id='matRegionsTitle']";
	private final String continueButton_xpath 		= "//span[contains(text(),'CONTINUE')]";
	private final String backButton_xpath		 	= "//span[contains(text(),'BACK')]";
	// Footer
	private final String footer_xpath 				= "//footer[@id='fxFooterLayout']";
    private final String footerPhone_xpath        	= "//footer[@id='fxFooterLayout']//span[contains(.,'(800) 990-9339')]";
    private final String footerEmail_xpath        	= "//footer[@id='fxFooterLayout']//span[contains(.,'s2g-support@periscopeholdings.com')]";
    
    
    //States and province
    private final String stateProvinceTitle_xpath 	= "//mat-card-title[@id='matStateProvincesTitle']";
    private final String unitedStatesTab_xpath 		= "//div[contains(text(),'UNITED STATES')]";
    private final String canadaTab_xpath 			= "//div[contains(text(),'CANADA')]";
    private final String listViewTab_xpath 			= "//div[contains(text(),'LIST VIEW')]";
//    private final String allNonSelectedState_xpath	= "//*[@id='text-abb']/*[@class='ng-star-inserted']";
    private final String allSelectedState_xpath		= "//*[@id='text-abb']/*[@class='ng-star-inserted selected']";
//    private final String listViewStates_xpath		="//mat-chip-list[@id='mat-chip-list-3']//div[@class='mat-chip-list-wrapper']//mat-chip";
    
//    #Fix for release OCT 7
    private final String listViewStates_xpath		="//div[@class='mat-chip-list-wrapper']//mat-chip";

    private final String clearAllUSButton_id 		= "select_all_states";
    private final String selectAllUSButton_id 		= "clear_all_states";
    private final String clearAllCanadaButton_id 	= "select_all_ca";
    private final String selectAllCanadaButton_id 	= "clear_all_ca";
    private final String selectAllListView_id		= "clear_all_chips";
    private final String clearAllListView_id		= "select_all_chips";
    
    private final String searchState_id				= "state";
    private final String stateOptionList_xpath		="//div[@class='mat-autocomplete-panel mat-autocomplete-visible ng-star-inserted']//mat-option";
    private final String statesOnFilterSection_xpath= "//div[@class='searchState chipSearch ng-star-inserted']//div[@class='chip-container']//mat-chip";
    
    private final int totalNumberOfUSStates				=51; //we support this much so far: Included DC
    private final int totalNumberOfCanadianProvince	  	=13;
    
	public SelectRegions(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
		helperMethods = new HelperMethods();
	}
	
	/* ----- getters ----- */
	
	public WebElement getRegionsPageHead() 			{	return findByVisibility(By.xpath(regionsPageHead_xpath));	}
	public WebElement getUsRegion() 				{	return findByVisibility(By.xpath(usRegion_xpath));			}
	public WebElement getUsAndCanada() 				{	return findByVisibility(By.xpath(usAndCanada_xpath));		}
	public WebElement getStatesOrProvinces()		{	return findByVisibility(By.xpath(statesOrProvinces_xpath));	}
	public WebElement getContinueButton() 			{	return findByVisibility(By.xpath(continueButton_xpath));	}
	public WebElement getBackButton() 				{	return findByClickability(By.xpath(backButton_xpath));		}
	// Footer
	public WebElement getFooter()					{	return findByVisibility(By.xpath(footer_xpath));			}
	public WebElement getFooterPhone() 				{ 	return findByVisibility(By.xpath(this.footerPhone_xpath));	}
	public WebElement getFooterEmail() 				{ 	return findByVisibility(By.xpath(this.footerEmail_xpath)); 	}
	
	public WebElement getStateProvinceTitle()		{	return findByVisibility(By.xpath(stateProvinceTitle_xpath));}
	public WebElement getUnitedStatesTab()			{	return findByVisibility(By.xpath(unitedStatesTab_xpath));	}
	public WebElement getCanadaTab()				{	return findByVisibility(By.xpath(canadaTab_xpath));			}
	public WebElement getListViewTab()				{	return findByVisibility(By.xpath(listViewTab_xpath));		}
	public WebElement getClearAllUSButton()   		{	return findByVisibility(By.id(clearAllUSButton_id));		}
	public WebElement getSelectAllUSButton()  		{	return findByVisibility(By.id(selectAllUSButton_id));		}
	public WebElement getClearAllCanadaButton()   	{	return findByVisibility(By.id(clearAllCanadaButton_id));	}
	public WebElement getSelectAllCanadaButton()  	{	return findByVisibility(By.id(selectAllCanadaButton_id));	}
	public WebElement getClearAllListViewButton()  	{	return findByVisibility(By.id(clearAllListView_id));		}
	public WebElement getSelectAllListViewButton()  {	return findByVisibility(By.id(selectAllListView_id));		}
	
	public WebElement getSearchState()  			{	return findByVisibility(By.id(searchState_id));				}

	
	/* ----- methods ----- */

	/**
	 * <h1>selectRegion</h1>
	 * <p>
	 * purpose: Select the supplier's region while registration process<br>
	 * Fail test if invalid region is selected
	 * </p>
	 * 
	 * @param region
	 * @return SelectRegions
	 * @throws InterruptedException
	 */
	public SelectRegions selectRegion(SalesRegion region) {
		System.out.println(GlobalVariables.getTestID() + " INFO: Selecting region and Clicking on \"CONTINUE\"");
		helperMethods.addSystemWait(2);
		switch (region) {
		case US:
			getUsRegion().click();
			GlobalVariables.storeObject("salesRegion", SalesRegion.US);
			break;
		case US_CANADA:
			getUsAndCanada().click();
			GlobalVariables.storeObject("salesRegion", SalesRegion.US_CANADA);
			break;
		case STATES_PROVINCE:
			getStatesOrProvinces().click();
			GlobalVariables.storeObject("salesRegion", SalesRegion.STATES_PROVINCE);
			break;
		default:
			fail("Selected region is invalid");
		}
		return this;
	}

	/**
	 * <h1>selectContinueButton</h1>
	 * <p>
	 * purpose: Select the continue button after selecting supplier's region during
	 * registration process<br>
	 * Fail test if not redirected to expected page
	 * </p>
	 * 
	 * @return SelectRegions
	 */
	public SelectRegions selectContinueButton() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Clicking on \"CONTINUE\"");
		getContinueButton().click();
		// Give the app a second to complete region selection before selecting keywords
		// Waiting for URL to display the keywords/state selection modal depending on
		// region selection
		int timeout = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, timeout).withMessage("Test Timed Out: After clicking on \"CONTINUE\", "
				+ "the user was not able to reach the keyword or states selection within " + timeout + " seconds")
				.until(ExpectedConditions.or(ExpectedConditions.urlContains("setup/keywords"),
						ExpectedConditions.urlContains("setup/location")));

		return this;
	}

	/***
	 * <h1>selectRandomStates</h1>
	 * <p>
	 * purpose: Method to select random states as sales region from the map
	 * 
	 */
	public void selectRandomStates(int statesTobeSelected) {
		System.out.println(GlobalVariables.getTestID() + " INFO : Selecting random states");
		List<String> stateList = new ArrayList<>();
		if (GlobalVariables.containsObject("selectedStateList")) {
			stateList.addAll(GlobalVariables.getStoredObject("selectedStateList"));
		}
		for (int i = 0; i < statesTobeSelected; i++) {
			String state = new RandomHelpers().getRandomUSState();
			String stateName = new GeographyHelpers().getStateOrProvinceAbbreviationFromStateName(state);
			//Fix for release OCT 7
			if(!stateList.contains(stateName)) {
				stateList.add(stateName);
				String xpath = "//div[@id='usmapwrapper']//*[contains(text(),'" + stateName + "')]";
				helperMethods.addSystemWait(2);
				findByVisibility(By.xpath(xpath)).click();
			}
		}
		GlobalVariables.storeObject("selectedStateList", stateList);
	}
	
	/***
	 * <h1>selectRandomselectGivenStatesAndProvinceStates</h1>
	 * <p>
	 * purpose: Method to select given states and province from the map
	 * 
	 */
	public void selectGivenStatesAndProvince(List<String> states, List<String> province) {
		System.out.println(GlobalVariables.getTestID() + " INFO : Selecting given states and province");
		List<String> stateList = new ArrayList<>();
		if (GlobalVariables.containsObject("selectedStateList")) {
			stateList.addAll(GlobalVariables.getStoredObject("selectedStateList"));
		}
		for(String state : states) {
			String xpath = "//div[@id='usmapwrapper']//*[contains(text(),'" + state + "')]";
			helperMethods.addSystemWait(1);
			findByVisibility(By.xpath(xpath)).click();
		}
		stateList.addAll(states);
		helperMethods.addSystemWait(2);
		this.getCanadaTab().click();
		
		for(String pro : province) {
			String xpath = "//div[@id='map_base']//*[contains(text(),'" + pro + "')]";
			helperMethods.addSystemWait(2);
			findByVisibility(By.xpath(xpath)).click();
		}
		stateList.addAll(province);
		GlobalVariables.storeObject("selectedStateList", stateList);
//    	getContinueButton().click();
	}

	/**
	 * <h1>clickOnUSTab</h1>
	 * <p>
	 * Selects US Tab
	 * 
	 * @return SelectRegions
	 */
	public SelectRegions clickOnUSTab() {
		this.getUnitedStatesTab().click();
		return this;
	}

	/**
	 * <h1>clickOnCanadaTab</h1>
	 * <p>
	 * Selects Canada Tab
	 * 
	 * @return SelectRegions
	 */
	public SelectRegions clickOnCanadaTab() {
		this.getCanadaTab().click();
		return this;
	}

	/**
	 * <h1>clickOnListViewTab</h1>
	 * <p>
	 * Selects List View Tab
	 * 
	 * @return SelectRegions
	 */
	public SelectRegions clickOnListViewTab() {
		this.getListViewTab().click();
		return this;
	}

	/**
	 * <h1>clickOnSelectAllUSButton</h1>
	 * <p>
	 * Selects Select All Button - US
	 * 
	 * @return SelectRegions
	 */
	public SelectRegions clickOnSelectAllUSButton() {
		this.getSelectAllUSButton().click();
		return this;
	}

	/**
	 * <h1>clickOnClearAllUSButton</h1>
	 * <p>
	 * Selects Clear All Button - US
	 * 
	 * @return SelectRegions
	 */
	public SelectRegions clickOnClearAllUSButton() {
		this.getClearAllUSButton().click();
		return this;
	}

	/**
	 * <h1>clickOnSelectAllCanadaButton</h1>
	 * <p>
	 * Selects Select All Button - Canada
	 * 
	 * @return SelectRegions
	 */
	public SelectRegions clickOnSelectAllCanadaButton() {
		this.getSelectAllCanadaButton().click();
		return this;
	}

	/**
	 * <h1>clickOnClearAllCanadaButton</h1>
	 * <p>
	 * Selects Clear All Button - Canada
	 * 
	 * @return SelectRegions
	 */
	public SelectRegions clickOnClearAllCanadaButton() {
		this.getClearAllCanadaButton().click();
		return this;
	}
	
	/**
	 * <h1>clickOnSelectAllListViewButton</h1>
	 * <p>
	 * Selects Select All Button - List View
	 * 
	 * @return SelectRegions
	 */
	public SelectRegions clickOnSelectAllListViewButton() {
		this.getSelectAllListViewButton().click();
		return this;
	}

	/**
	 * <h1>clickOnClearAllListViewButton</h1>
	 * <p>
	 * Selects Clear All Button - List View
	 * 
	 * @return SelectRegions
	 */
	public SelectRegions clickOnClearAllListViewButton() {
		this.getClearAllListViewButton().click();
		return this;
	}

	

	/* ----- verifications ----- */

	/***
	 * <h1>verifyPageInitialization</h1>
	 * <p>
	 * purpose: Verify that the following are present when page initializes:<br>
	 * 1. Header 2. All 3 regions<br>
	 * 3. Buttons 4. Footer
	 * </p>
	 * 
	 * @return SelectRegions
	 */
	public SelectRegions verifyPageInitialization() {
		// Header
		this.getRegionsPageHead();

		// All three regions
		this.getUsRegion();
		this.getUsAndCanada();
		this.getStatesOrProvinces();

		// Buttons
		this.getContinueButton();
		this.getBackButton();

		// Footer
		this.getFooter();
		this.getFooterPhone();
		this.getFooterEmail();

		return this;
	}

	/***
	 * <h1>verifySelectStatesAndProvincesPageInitialization</h1>
	 * <p>
	 * purpose: Verify that the following are present when State and Province page
	 * initializes:<br>
	 * 1. Header 2. All 3 tabs<br>
	 * 3. Buttons(ClearAll, SelectAll, Back, Continue) 4. Footer
	 * </p>
	 * 
	 * @return SelectRegions
	 */
	public SelectRegions verifySelectStatesAndProvincesPageInitialization() {
		this.getStateProvinceTitle();

		this.getUnitedStatesTab();
		this.getCanadaTab();
		this.getListViewTab();

		this.getClearAllUSButton();
		this.getSelectAllUSButton();// TODO : Can addCanada and list page objects

		this.getBackButton();
		this.getContinueButton();

		// Footer
		this.getFooter();
		this.getFooterPhone();
		this.getFooterEmail();

		return this;
	}

	/**
	 * <h1>verifySelectAllSelectsAllStatesOrProvince</h1>
	 * <p>
	 * Purpose : Verification method for select all button - flag says its US or
	 * Canada tab
	 */
	public SelectRegions verifySelectAllSelectsAllStatesOrProvince(boolean isUs) {
		System.out.println(GlobalVariables.getTestID() + " INFO : Verifying Select All functionality");
		if (isUs)
			new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Select All is not selecting all  states / province")
					.until(ExpectedConditions.numberOfElementsToBe(By.xpath(allSelectedState_xpath),
							totalNumberOfUSStates));
		else
			new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Select All is not selecting all states / province")
					.until(ExpectedConditions.numberOfElementsToBe(By.xpath(allSelectedState_xpath),
							totalNumberOfCanadianProvince));
		return this;
	}

	/**
	 * <h1>verifyClearAllClearsAllUSStates</h1>
	 * <p>
	 * Purpose : Verification method for clear all button
	 */

	public SelectRegions verifyClearAllClearsAllStatesOrProvince() {
		System.out.println(GlobalVariables.getTestID() + " INFO : Verifying Clear All functionality");
		new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Clear All is not selecting all states / province")
				.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(allSelectedState_xpath)));
		return this;
	}

    
	/**
	 * <h1>verifyListView</h1>
	 * <p>
	 * Purpose : Verifies the number of selected states on map and list view are
	 * same
	 * <p>
	 * Accepts boolean true if select all is selected; else the flag will be false
	 */
	public SelectRegions verifyListView(boolean selectAll, boolean isUs) {
		System.out.print("INFO : Verifying List View tab");
		int totalSelection = 0;
		if (isUs)
			totalSelection = totalNumberOfUSStates;
		else
			totalSelection = totalNumberOfCanadianProvince;
		if (selectAll)
			new WebDriverWait(driver, DEFAULT_TIMEOUT)
					.withMessage("There is a miss match in state selection on \"Select All\" : isUs = " + isUs)
					.until(ExpectedConditions.numberOfElementsToBe(By.xpath(listViewStates_xpath), totalSelection));
		else
			new WebDriverWait(driver, DEFAULT_TIMEOUT)
					.withMessage("There is a miss match in state selection on \"Clear All\": isUs = " + isUs)
					.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(listViewStates_xpath)));
		return this;
	}
	/**
	 * <h1>verifyListView()</h1>
	 * <p>Verifies the list view when both US and Canada is the sales region
	 * 
	 * @return SelectRegions
	 */
	public SelectRegions verifyListView(boolean selectAll) {
		System.out.print("INFO : Verifying List View tab");
		new WebDriverWait(driver, DEFAULT_TIMEOUT)
				.withMessage("There is a miss match in state selection when both US and Canada is the sales region")
				.until(ExpectedConditions.numberOfElementsToBe(By.xpath(listViewStates_xpath),
						totalNumberOfUSStates + totalNumberOfCanadianProvince));

		return this;
	}

	/**
	 * <h1>verifyStateDropDownListOptions()</h1>
	 * <p>Verifies the total number of state drop down options
	 * 
	 * @return SelectRegions
	 */
	public SelectRegions verifyStateDropDownListOptions() {
		System.out.print("INFO : Verifying State drop down list option in list view");
		new WebDriverWait(driver, DEFAULT_TIMEOUT)
				.withMessage("Unable to load all states in list view ")
				.until(ExpectedConditions.numberOfElementsToBe(By.xpath(stateOptionList_xpath),
						totalNumberOfUSStates + totalNumberOfCanadianProvince));
	
		
		return this;
	}
	
	public SelectRegions verifySalesRegionOnFilter() {
		System.out.printf(GlobalVariables.getTestID() + " INFO : Verifying Sales region on filter. ");
		List<String> stateList = GlobalVariables.getStoredObject("selectedStateList");
		System.out.println(GlobalVariables.getTestID() + " Selected States :  "  + stateList.size());
		if(stateList.isEmpty())
			stateList=new ArrayList<>();
		new WebDriverWait(driver, DEFAULT_TIMEOUT)
		.withMessage("There is a miss match in state or province in filter section with the one in profile section")
		.until(ExpectedConditions.numberOfElementsToBe(By.xpath(statesOnFilterSection_xpath),stateList.size()));

		return this;
	}
	/**
	 * <h1> verifyStateforBidList </h1>
	 * <p> Check the listed bids in the page and verify the state of each bids
	 * 
	 * @return SelectRegions
	 */
	public SelectRegions verifyStateforBidList(boolean isNewForYouTab) {
		System.out.println(GlobalVariables.getTestID() + " INFO : Verifying states on bidlist");
		SalesRegion selectedRegion = (SalesRegion)GlobalVariables.getStoredObject("salesRegion");
		GeographyHelpers geographyHelpers = new GeographyHelpers();
		
		switch (selectedRegion) {
		case US:
			for (BidListRow bidResult : new BidList(this.driver).allVisibleBids()) {
				if(!RandomHelpers.usStatesTerritoryAbr.contains(bidResult.bidInfo.getState())) {
					fail("Not All bids are from the supplier sales region ");
				}
			}
			
			break;
		case US_CANADA:
			for (BidListRow bidResult : new BidList(this.driver).allVisibleBids()) {
				if(geographyHelpers.getStateOrProvinceAbbreviationFromStateName(bidResult.bidInfo.getState()).equals("ERROR")) {
					fail("Not All bids are from the supplier sales region ");
				}
			}
			break;
		case STATES_PROVINCE:
			List<String> stateList = GlobalVariables.getStoredObject("selectedStateList");
			List<BidListRow> bidListRowList = new ArrayList<>();
			if(isNewForYouTab) {
				bidListRowList = new BidList(this.driver).allVisibleBidsInNewForYouTab();
			}else {
				bidListRowList = new BidList(this.driver).allVisibleBids();
			}
			for (BidListRow bidResult : bidListRowList) {
				if(!stateList.contains(bidResult.bidInfo.getState())){
					fail("Not All bids are from the supplier sales region ");
				}
			}
			break;
		default:
			fail("Bid list includes the bids other than the profile region");
			break;
		}
		return this;
	}

//  #Fix for release OCT 7
	public SelectRegions verifyContinueButtonDisabled() {
			System.out.printf(GlobalVariables.getTestID() + " INFO : Verifying Save&Continue button. ");
			new WebDriverWait(driver, DEFAULT_TIMEOUT)
			.withMessage("Save & continue Button is active")
			.until(ExpectedConditions.attributeToBe(By.id("continueButtonFirst"), "disabled", "true"));
		return this;
	}
}
