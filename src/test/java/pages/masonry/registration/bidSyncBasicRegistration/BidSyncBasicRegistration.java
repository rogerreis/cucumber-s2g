package pages.masonry.registration.bidSyncBasicRegistration;


import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.javascript.host.Window;

import data.bidSyncPro.registration.CompanyRevenueEnum;
import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.masonry.registration.CommonTopBar;

/***
 * <h1>Class BidSyncBasicRegistration</h1>
 * <p>details: This class houses page objects and methods for registering a BidSync Basic user off of the Masonry site.<br>
 * 	Unlike, Pro registration, the Basic user does not punch out to Chargebee (i.e. no Chargebee registration modals).<br>
 *	Instead, all registration for the BidSync Basic user is done on this Masonry page</p>
 */
public class BidSyncBasicRegistration extends CommonTopBar {
    
    // xpaths
    private final String stateDropdown_xpath                  = "//select[@id='to_subscribe_to']";
    private final String verifyEmailButton_xpath              = "//div[@class='check-arrow smol flex align-center']";
    private final String emailSuccessfullyVerifiedLabel_xpath = "//p[contains(., 'Email SUCCESSFULLY verified.')]";
    private final String firstNameInput_xpath                 = "//input[@name='first_name']";
    private final String lastNameInput_xpath                  = "//input[@name='last_name']";
    private final String phoneInput_xpath                     = "//input[@name='phone']";
    private final String phoneExtInput_xpath                  = "//input[@name='extension']";
    private final String addressInput_xpath                   = "//input[@name='address']";
    private final String cityInput_xpath                      = "//input[@name='city']";
    private final String zipCodeInput_xpath                   = "//input[@name='zip']";
    private final String stateCodeDropdown_xpath              = "//select[@name='state_code']";
    private final String termsOfServiceCheckbox_xpath         = "//input[@type='checkbox']";
    private final String submitButton_xpath                   = "//input[@value='submit']";
    private final String accountExistsErrorMessage_xpath 	  = "//*[@id='modal1']//h2";
    private final String companyRevenue_id					  = "company_revenue";
    private final String companySize_id						  = "company_size";
    private final String Industry_id	 					  = "selected_industry";
    private final String reasonForRegistration_id			  = "why_are_you_registering";
    
    
    // ids
    private final String emailInput_id                        = "email";
    private final String verifyEmailInput_id                  = "verify-email";
    
    
    private int timeout = DEFAULT_TIMEOUT;
    private final String accountExistsErrorMsg 					  = "We see that you already have an existing account on BidSync.";
    private final String termsAndConditionsLink_xpath			="//a[contains(text(),'BidSync Terms and Conditions')]";
    
    public BidSyncBasicRegistration(EventFiringWebDriver driver) {
        super(driver);
    }
    
    /* ----- getters ----- */
    private Select     getStateDropdown()     { return new Select(findByVisibility(By.xpath(this.stateDropdown_xpath))); } 
    private WebElement getEmailInput()        { return findByVisibility(By.id(this.emailInput_id));                      } 
    private WebElement getVerifyEmailInput()  { return findByVisibility(By.id(this.verifyEmailInput_id));                } 
    private WebElement getVerifyEmailButton() { return findByVisibility(By.xpath(this.verifyEmailButton_xpath));         } 
    private WebElement getFirstNameInput()    { return findByVisibility(By.xpath(this.firstNameInput_xpath));            } 
    private WebElement getLastNameInput()     { return findByVisibility(By.xpath(this.lastNameInput_xpath));             } 
    private WebElement getPhoneInput()        { return findByVisibility(By.xpath(this.phoneInput_xpath));                } 
    private WebElement getPhoneExtInput()     { return findByVisibility(By.xpath(this.phoneExtInput_xpath));             }
    
    private WebElement getAddressInput()           { return findByVisibility(By.xpath(this.addressInput_xpath));                   } 
    private WebElement getCityInput()              { return findByVisibility(By.xpath(this.cityInput_xpath));                      } 
    private WebElement getZipCodeInput()           { return findByVisibility(By.xpath(this.zipCodeInput_xpath));                   } 
    private Select     getStateCodeDropdown()      { return new Select (findByVisibility(By.xpath(this.stateCodeDropdown_xpath))); } 
    private WebElement getTermsOfServiceCheckbox() { return findByVisibility(By.xpath(this.termsOfServiceCheckbox_xpath));         } 
    private WebElement getSubmitButton()           { return findByClickability(By.xpath(this.submitButton_xpath));                 } 
    
    private Select getCompanyRevenue()         	   { return new Select(findByVisibility(By.id(this.companyRevenue_id)));        } 
    private Select getCompanySize()            	   { return new Select( findByVisibility(By.id(this.companySize_id)));          } 
    private Select getIndustry()           	   	   { return new Select( findByVisibility(By.id(this.Industry_id)));             } 
    private Select getReasonForRegistration()  	   { return new Select( findByVisibility(By.id(this.reasonForRegistration_id)));} 
    
    public WebElement getTermaAndConditionLink()	{ return findByScrollIntoViewBottomOfScreen(By.xpath(termsAndConditionsLink_xpath));}

    /* ----- helpers ----- */

    /**
     * <h1>verifyEmailFroNewUserRegistration</h1>
     * <p> purpose:  Verify emailId while user registration.<br>
     * Note: The system supports unique emailId for user registration
     * @param RandomChargebeeUser with user's registration data
     * @return BidSyncBasicRegistration
     */
    private BidSyncBasicRegistration verifyEmailForNewUserRegistration(RandomChargebeeUser user) {
        System.out.printf(GlobalVariables.getTestID() + " INFO: userEmail = %s\n", user.getEmailAddress());
        getEmailInput()       .sendKeys(user.getEmailAddress());
        getVerifyEmailInput() .sendKeys(user.getEmailAddress());
        getVerifyEmailButton().click();
        return this;
    }

    /* ----- methods ----- */

   /**
   * <h1>initiateBasicRegistration</h1>
   * <p>purpose: Select Basic notification state, enter user email, and verify email uniqueness</p>
   * @param user: RandomChargebeeUser is the user's registration data
   * @return BidSyncBasicRegistration
   */
	public BidSyncBasicRegistration initiateBasicRegistration(RandomChargebeeUser user) {
    	// Select user's state
//		getStateDropdown().selectByVisibleText(user.getState());

        // Verify email is unique
        // Note: If email is not unique, test fails
		return this.verifyEmailForNewUserRegistration(user);
	}
	
	/***
	 * <h1>completeBasircRegistration</h1>
	 * <p>purpose: Assuming user has entered registration notification state and verified email uniqueness,
	 * enter all user registration data, check T&C
	 * @param user = RandomChargebeeUser registration data
	 * @return BidSyncBasicRegistration
	 */
	public BidSyncBasicRegistration completeBasicRegistration(RandomChargebeeUser user) {
		// Make sure that email uniqueness was verified
        waitForVisibility(By.xpath(this.emailSuccessfullyVerifiedLabel_xpath), 20);

	    // Enter user registration data
        getFirstNameInput().sendKeys(user.getFirstName());
        getLastNameInput() .sendKeys(user.getLastName());
        getPhoneInput()    .sendKeys(user.getPhoneNumber());
        getPhoneExtInput() .sendKeys(user.getPhoneExtension());
        getAddressInput()  .sendKeys(user.getAddress());
        getCityInput()     .sendKeys(user.getCity());
        getZipCodeInput()  .sendKeys(user.getZipCode());
        getStateCodeDropdown().selectByVisibleText(user.getState());
        
        getCompanyRevenue().selectByVisibleText(user.getCompanyRevenue());
        getCompanySize().selectByVisibleText(user.getCompanySize());
        getIndustry().selectByVisibleText(user.getIndustry());
        getReasonForRegistration().selectByVisibleText(user.getReasonForRegistration());
        
        // Accept T&C
        getTermsOfServiceCheckbox().click();
	
        return this;
	}
	
	
	
	/***
	 * <h1>clickSubmitButton</h1>
	 * <p>purpose: Assuming Submit button is active, click on it to submit the Basic registration
	 * @return BidSyncBasicRegistration
	 */
	public BidSyncBasicRegistration clickSubmitButton() {
		this.scrollIntoViewBottomOfScreen(By.xpath(this.submitButton_xpath));
        // Submit registration
        getSubmitButton().click();
		return this;
	}
    
    /***
     * <h1>registerUser</h1>
     * <p>purpose: For a BidSync Basic user registration:<br>
     *  1. Select user's registration state as Basic subscription state<br>
     *  2. Verify the user email is unique<br>
     *  3. Enter required registration information<br>
     *  4. Accept T&C<br>
     *  5. Submit registration</p>
     * @param user = user registration information
     */
    public void registerUser(RandomChargebeeUser user) {
    	this.initiateBasicRegistration(user)
        	.completeBasicRegistration(user)
        	.clickSubmitButton();
    }
    
    /* ----- verifications ----- */

    /**
     * <h1>verifyAccountExistsErrorMessage</h1>
     * <p>purpose: Method to verify error message.
     * The system throws error message when a user tries to register with an email which is already in our system </p>
     * <p>Message : We see that you already have an existing account on BidSync.</p>
     */
    public BidSyncBasicRegistration verifyAccountExistsErrorMessage() {
    	new WebDriverWait(driver, timeout).withMessage("Could not find the error message")
    	.until(ExpectedConditions.textToBePresentInElementLocated(By.xpath(accountExistsErrorMessage_xpath), accountExistsErrorMsg));
    	return this;
    }
    
    /***
     * <h1>verifyPhoneExtensionContainsOnlyNumber</h1>
     * <p>purpose: Grab the currently displayed value in the Phone Ext input.
     * Verify the value only contains numbers (Note: may include an exponential). Fail test if not
     * @return BidSyncBasicRegistration
     */
    public BidSyncBasicRegistration verifyPhoneExtensionContainsOnlyNumbers() {
    	String actualPhoneExt = this.getPhoneExtInput().getAttribute("value");
    	String errMsg = String.format("ERROR: Expected Masonry Basic Phone Ext input to allow only numbers, but got <%s>", actualPhoneExt);
    	Assert.assertTrue(errMsg, actualPhoneExt.matches("\\d+(e|E)?\\d+"));
    	return this;
    }
    
    /***
     * <h1>verifyPhoneExtensionAttributesForValidation</h1>
     * <p>purpose: Verify that the phone number attributes for the Phone Ext are:<Br>
     *  1. Placeholder = Extension<br>
     *  2. Type = number<br>
     *  3. min = 0<br>
     *  4. max = 999999999<br>
     *  Fail test if any of these is incorrect
     * @return BidSyncBasicRegistration
     */
    public BidSyncBasicRegistration verifyPhoneExtensionAttributesForValidation() {
    	//<input id="phoneExt" class="input" placeholder="Extension" type="number" min="0" max="999999999" name="extension">
    	String actualPlaceholder = this.getPhoneExtInput().getAttribute("placeholder");
    	String actualType        = this.getPhoneExtInput().getAttribute("type");
    	String actualMin         = this.getPhoneExtInput().getAttribute("min");
    	String actualMax         = this.getPhoneExtInput().getAttribute("max");
    	
    	String expectedPlaceholder = "Extension";
    	String expectedType        = "number";
    	String expectedMin         = "0";
    	String expectedMax         = "999999999";
    	
    	SoftAssertions softly = new SoftAssertions();
    	String errMsg = "Error: Expected Phone Ext attribute %s to be <%s>, but got <%s>";
    	softly.assertThat(actualPlaceholder).withFailMessage(errMsg, "placeholder", expectedPlaceholder, actualPlaceholder).isEqualTo(expectedPlaceholder);
    	softly.assertThat(actualType)       .withFailMessage(errMsg, "Type", expectedType, actualType)                     .isEqualTo(expectedType);
    	softly.assertThat(actualMin)        .withFailMessage(errMsg, "Min", expectedMin, actualMin)                        .isEqualTo(expectedMin);
    	softly.assertThat(actualMax)        .withFailMessage(errMsg, "Max", expectedMax, actualMax)                        .isEqualTo(expectedMax);
    	softly.assertAll();

    	return this;
    }
    
    public BidSyncBasicRegistration verifyTermsAndConditionOpensInNewWindow(){
    	String winHandleBefore = driver.getWindowHandle();
    	for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
    	new HelperMethods().addSystemWait(2);
    	new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Terms & Condition link is not opening in new window")
    	.until(ExpectedConditions.urlContains("/terms-and-conditions"));
    	driver.close();
    	driver.switchTo().window(winHandleBefore);
    	new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Terms & Condition link is not opening in new window")
    	.until(ExpectedConditions.urlContains("/bidsync-basic"));
    	return this;
    }
}
