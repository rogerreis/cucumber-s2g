@must @registration @level3
Feature: Supplier Registration - BidSync Website Registration > Validate Basic Phone Ext
	BIDSYNC-649: BidSync.com Extension Validation


@BIDSYNC-649
Scenario: Verify Basic Phone Number Extension Validation Attributes to Trigger Validation are Correct
	Given I initiate registration for a BidSync Basic user on the Masonry website
	Then I will verify that the validation attributes are correct for the Phone Number Extension for a Masonry Basic Registration

	
@BIDSYNC-649
Scenario Outline: Verify Valid Phone Extensions do not Trigger Validation
	Given I initiate registration for a BidSync Basic user on the Masonry website
	When I input "<phoneExt>" phone extension along with random registration data into the Masonry Basic registration
	And I submit the Masonry Basic registration
	Then I will verify that Masonry Basic registration displays a Basic Registration Confirmation screen

	Examples: Valid Phone Registrations
	| phoneExt    |
	| 123456789   |
	| 1           |
	|             |
	
	# 9 numeric digits
	# single numeric digit
	# blank


@BIDSYNC-649
Scenario Outline: Verify Special Chars and Alpha Chars are not Allowed in Phone Extension Input
	Given I initiate registration for a BidSync Basic user on the Masonry website
	When I input "<phoneExt>" phone extension along with random registration data into the Masonry Basic registration
	Then I will verify that Masonry Basic registration will automatically update the phone extension to exclude the invalid inputs
	When I submit the Masonry Basic registration
	Then I will verify that Masonry Basic registration displays a Basic Registration Confirmation screen

	Examples: Inputs Masonry will automatically update
	| phoneExt                     |
	| ab12De1                      |
	#| !@#$%^&*()133_=[]{|}\:;<>,/? |
	
	# alpha chars (note: e is interpreted as an exponential)
	# special chars