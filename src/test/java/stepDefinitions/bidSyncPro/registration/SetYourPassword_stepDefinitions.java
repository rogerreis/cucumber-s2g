package stepDefinitions.bidSyncPro.registration;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.bidSyncPro.registration.SetYourPassword;
import pages.common.helpers.GlobalVariables;

public class SetYourPassword_stepDefinitions {

	private EventFiringWebDriver driver;

	public SetYourPassword_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Test on \"Set Password\" screen ");
	}
	
	@When("^I landed on Set password page$")
	public void iLandedOnSetPasswordPage() {
	    try {
	    	new SetYourPassword(driver)
	    		.verifySetPasswordUrl();
	    	
	    } catch (ElementNotFoundException exp) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Set Password\" screen : iLandedOnSetPasswordPage()\n" + exp);
		}
	}

	@Then("^I verify that the expected page objects are present in Set Password page$")
	public void i_verify_that_the_expected_page_objects_are_present_in_Set_Password_page() {
		
		try {
			new SetYourPassword(driver)
				.verifyPageInitialization();
			
		} catch (ElementNotFoundException exp) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Set Password\" screen did not initialize per: i_verify_that_the_expected_page_objects_are_present_in_Set_Password_page\n" + exp);
		}
	}
	
	@And("^the new password and confirm password field is ready to accepts input or enabled.$")
	public void the_user_can_type_password_in_New_Password_and_Confirm_Password_field() {
		try {
		new SetYourPassword(driver)
			.verifyNewPasswordEnabled()
			.verifyConfirmPasswordEnabled();
		
		} catch (ElementNotFoundException exp) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Set Password\" screen - : the_user_can_type_password_in_New_Password_and_Confirm_Password_field()\n" + exp);
		}
	
	}
	
	@And("^the next button is disabled$")
	public void the_next_button_is_disabled() {
		try {
			new SetYourPassword(driver)
				.verifyNextButtonDisabled();
			
		} catch (ElementNotFoundException exp) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Set Password\" screen did not initialize per: the_next_button_is_disabled()\n" + exp);
		}
	}

	@Then("^I verify that the new password is hidden by default.$")
	public void I_verify_that_the_new_password_is_hidden_by_default() {
		try {
			new SetYourPassword(driver)
				.verifyPasswordIsHiddenOnPageLoad();
			
		} catch (ElementNotFoundException exp) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Set Password\" screen : I_verify_that_the_new_password_is_hidden_by_default() method:\n" + exp);
		}
	}
	
	@And("^the confirm password is hidden by default$")
	public void the_confirm_password_is_hidden_by_default() {
		try {
			new SetYourPassword(driver)
				.verifyConfirmPasswordIsHiddenOnPageLoad();
			
		} catch (ElementNotFoundException exp) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Set Password\" screen : the_confirm_password_is_hidden_by_default() method:\n" + exp);
		}
	}
	
	@When("^user select show password option for new password, the new password entered should be visible$")
	public void user_select_show_password_option_for_new_password_the_new_password_entered_should_be_visible () {
		try {
			new SetYourPassword(driver)
				.verifyViewNewPassword();
			
		} catch (ElementNotFoundException exp) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Set Password\" screen : user_select_show_password_option_for_new_password_the_new_password_entered_should_be_visible() method:\n" + exp);
		}
	}
	
	@When("^user select hide password option for new password, the new password entered should not be visible$")
	public void user_select_hide_password_option_for_new_password_the_new_password_entered_should_not_be_visible () {
		try {
			new SetYourPassword(driver)
				.verifyHideNewPassword();
			
		} catch (ElementNotFoundException exp) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Set Password\" screen : user_select_hide_password_option_for_new_password_the_new_password_entered_should_not_be_visible() method:\n" + exp);
		}
	}
	
	@When("^user select show password option for confirm password, the confirm password entered should be visible$")
	public void user_select_show_password_option_for_confirm_password_the_confirm_password_entered_should_be_visible () {
		try {
			new SetYourPassword(driver)
				.verifyViewConfirmPassword();
			
		} catch (ElementNotFoundException exp) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Set Password\" screen : user_select_show_password_option_for_confirm_password_the_confirm_password_entered_should_be_visible() method:\n" + exp);
		}
	}
	
	@When("^user select hide password option for confirm password, the confirm password entered should not be visible$")
	public void user_select_hide_password_option_for_confirm_password_the_confirm_password_entered_should_not_be_visible () {
		try {
			new SetYourPassword(driver)
				.verifyHideConfirmPassword();
			
		} catch (ElementNotFoundException exp) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Set Password\" screen : user_select_hide_password_option_for_confirm_password_the_confirm_password_entered_should_not_be_visible() method:\n" + exp);
		}
	}
	

	@When("^I enter invalid (.*?) and (.*?)$")
	public void i_enter_new_and_confirm_password(String newPassword, String confirmPassword) {
		try {
			new SetYourPassword(driver)
			.inputPasswords(newPassword, confirmPassword);
		} catch (ElementNotFoundException exp) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Set Password\" screen - : i_enter_new_and_confirm_password()\n" + exp);
		}
	}
	
	@Then("^I should see invalid password error message$")
	public void i_should_see_invalid_password_as_error_meaage(){
		try {
			new SetYourPassword(driver)
			.verifyInvalidPasswordErrorMessage();
		} catch (ElementNotFoundException exp) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Set Password\" screen - : i_should_see_invalid_password_as_error_meaage()\n" + exp);
		}
	}
	
	@Then("^I should see the error message on the screen$")
	public void I_should_see_the_error_message_on_the_screen() {
		try {
			new SetYourPassword(driver)
			.verifyPasswordDoesNotMatchErrorMessage();
		} catch (ElementNotFoundException exp) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Set Password\" screen - : I_should_see_the_error_message_on_the_screen()\n" + exp);
		}
	}
	
	
	@When("^I use valid newPassword (.*?) and confirmPassword (.*?) to set my password$")
	public void i_use_valid_new_and_confirm_password_to_set_my_password(String newPassword, String confirmPassword) {
		try {
			new SetYourPassword(driver)
			.inputPasswords(newPassword,confirmPassword);
		} catch (ElementNotFoundException exp) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Set Password\" screen - i_use_valid_new_and_confirm_password_to_set_my_password() :\n" + exp);
		}
	}
	
	@Then("^I should redirect to company setup page$")
	public void i_should_redirect_to_company_setup_page(){
		try {
			new SetYourPassword(driver)
			.verifyCompanySetUpUrl();
		} catch (ElementNotFoundException exp) {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: \"Set Password\" screen - i_should_redirect_to_company_setup_page() :\n" + exp);
		}
	}
	
	@When("^I enter new password as (.*?)$")
	public void i_enter_new_password(String password) {
		try {
			new SetYourPassword(driver)
				.enterPassword(password);
		} catch (ElementNotFoundException exp) {
			System.out.println(
					"Test Failed: \"Set Password\" screen - i_enter_new_password() :\n" + exp);
		}
	}
	
	

	@Then("^I should get the password requirement (.*?) to be highlighted$")
	public void i_should_get_passwordRequirement_ToBeHighlighted(String message){
		
		try {
			if (message == null || message.isEmpty()) {
				return;
			}
			new SetYourPassword(driver)
				.verifyMessageIsHighlighted(message);
		} catch (ElementNotFoundException exp) {
			System.out.println(
					"Test Failed: \"Set Password\" screen - i_should_get_passwordRequirement_ToBeHighlighted() :\n" + exp);

		}
	}
	
	
	@And("^the password criteria (.*?) should not be highlighted$")
	public void password_length_criteria_should_not_be_highlighted(String message) {
		try {
			
			new SetYourPassword(driver)
				.verifyMessageIsNotHighlighted(message);
		} catch (ElementNotFoundException exp) {
			System.out.println(
					"Test Failed: \"Set Password\" screen - password_length_criteria_should_not_be_highlighted() :\n" + exp);

		}
	}
	


}
