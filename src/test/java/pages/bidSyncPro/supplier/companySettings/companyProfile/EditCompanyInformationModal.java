package pages.bidSyncPro.supplier.companySettings.companyProfile;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import data.bidSyncPro.registration.RandomCompanyInformation;
import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.BasePageActions;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

/***
 * <h1>EditCompanyInformationModal</h1>
 * <p>details: This class houses page Objects and methods to manipulate the Edit Company Information modal
 * accessed at Company Settings > Company Profile > Edit
 */
public class EditCompanyInformationModal extends BasePageActions {
    
    private EventFiringWebDriver driver;
    private HelperMethods helperMethods;
    
    // ids
    private final String cardId               = "matCompanyProfileCard";
    private final String companyName_id       = "companyname";
    private final String industryType_id      = "companytype";
    private final String addressLine1_id      = "addressLine1";
    private final String addressLine2_id      = "addressLine2";
    private final String city_id              = "city";
    private final String annualRevenue_id     = "annualrevenue";
    private final String numberOfEmployees_id = "employees";
    private final String yearFounded_id       = "founded";
    private final String zipCode_id           = "zip";
    private final String fein_id              = "fedIdNbr";
    private final String ssn_id               = "ssn";
    
    private final String saveButton_id        = "saveButton";
    private final String cancelButton_id      = "cancelButton";
    
    public EditCompanyInformationModal(EventFiringWebDriver driver) {
    	super(driver);
        this.driver = driver;
        this.helperMethods = new HelperMethods();
    }
    
    /* ----- getters ----- */

    
    private WebElement getCompanyName()       { return findByScrollIntoViewBottomOfScreen(By.id(this.companyName_id));       }
    private WebElement getIndustryType()      { return findByScrollIntoViewBottomOfScreen(By.id(this.industryType_id));      }
    private WebElement getAddressLine1()      { return findByScrollIntoViewBottomOfScreen(By.id(this.addressLine1_id));      }
    private WebElement getAddressLine2()      { return findByScrollIntoViewBottomOfScreen(By.id(this.addressLine2_id));      }
    private WebElement getCity()              { return findByScrollIntoViewBottomOfScreen(By.id(this.city_id));              }
    private WebElement getAnnualRevenue()     { return findByScrollIntoViewBottomOfScreen(By.id(this.annualRevenue_id));     }
    private WebElement getNumberOfEmployees() { return findByScrollIntoViewBottomOfScreen(By.id(this.numberOfEmployees_id)); }
    private WebElement getYearFounded()       { return findByScrollIntoViewBottomOfScreen(By.id(this.yearFounded_id));       }
    private WebElement getZipCode()           { return findByScrollIntoViewBottomOfScreen(By.id(this.zipCode_id));           }
    private WebElement getFein()              { return findByScrollIntoViewBottomOfScreen(By.id(this.fein_id));              }
    private WebElement getSsn()               { return findByScrollIntoViewBottomOfScreen(By.id(this.ssn_id));               }
    private WebElement getSaveButton()        { return findByScrollIntoViewBottomOfScreen(By.id(this.saveButton_id));        } 
    private WebElement getCancelButton()      { return findByScrollIntoViewBottomOfScreen(By.id(this.cancelButton_id));      } 

    /* ----- methods ----- */
    public EditCompanyInformationModal fillInInformation(RandomCompanyInformation companyInformation) {
    	this.verifyModalIsVisible();

        helperMethods.addSystemWait(500, TimeUnit.MILLISECONDS);
        getCompanyName().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        getCompanyName().sendKeys(companyInformation.getName());
        
        getIndustryType().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        getIndustryType().sendKeys(companyInformation.getTypeOfIndustry());
        
        getAddressLine1().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        getAddressLine1().sendKeys(companyInformation.getAddressLine1());
        
        getAddressLine2().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        getAddressLine2().sendKeys(companyInformation.getAddressLine2());
        
        getCity().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        getCity().sendKeys(companyInformation.getCity());
        
        getAnnualRevenue().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        getAnnualRevenue().sendKeys(String.valueOf(companyInformation.getAnnualRevenue()));
        
        getNumberOfEmployees().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        getNumberOfEmployees().sendKeys(String.valueOf(companyInformation.getNumberOfEmployees()));
        
        getYearFounded().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        getYearFounded().sendKeys(String.valueOf(companyInformation.getYearEstablished()));
        
        getFein().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        getFein().sendKeys(companyInformation.getFEIN());
        
        getSsn().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        getSsn().sendKeys(companyInformation.getSSN());
        
        getZipCode().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        getZipCode().sendKeys(companyInformation.getZipCode());
        
        return this;
    }
    
    public CompanyProfile acceptChanges() {
        getSaveButton().click();
        return new CompanyProfile(driver);
    }
    
    public CompanyProfile cancelChanges() {
        getCancelButton().click();
        return new CompanyProfile(driver);
    }
    
    /***
     * <h1>verifyModalIsVisible</h1>
     * <p>purpose: Verify that the Edit Company Information Modal is visible
     * @return EditCompanyInformationModal
     */
    public EditCompanyInformationModal verifyModalIsVisible() {
        try {
        	findByVisibility(By.id(this.cardId));
        } catch (TimeoutException t) {
        	fail("ERROR: Company Profile dialog is not visible");
        }
        
        return this;
    }
}

