package stepDefinitions.bidSyncPro.supplier.companySettings.agencyInteraction;
 
import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import pages.bidSyncPro.supplier.companySettings.agencyInteractions.AgencyInteraction;
import pages.common.helpers.GlobalVariables;

public class AgencyInteraction_stepDefinitions {
  
	private EventFiringWebDriver driver;
	
	public AgencyInteraction_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
	}

		
	@And("^I click on the Agency Interaction Manage Qualifications Tab$")
	public void I_click_on_the_Agency_Interaction_Manage_Qualifications_Tab() throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on the Agency Interaction Manage Qualifications Tab");
		new AgencyInteraction(driver).clickManageQualificationsTab();
	}

	@And("^I click on the Agency Interaction Company Orders Tab$")
	public void I_click_on_the_Agency_Interaction_Company_Orders_Tab() throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on the Agency Interaction Company Orders Tab");
		new AgencyInteraction(driver).clickCompanyOrdersTab();
	}
	
	@And("^I click on the Agency Interaction Company Contracts Tab$")
	public void I_click_on_the_Agency_Interaction_Company_Contracts_Tab() throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on the Agency Interaction Company Contracts Tab");
		new AgencyInteraction(driver).clickCompanyContractsTab();
	}
	
	@And("^I click on the Agency Interaction Search Public Contracts Tab$")
	public void I_click_on_the_Agency_Interaction_Search_Public_Contracts_Tab() throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on the Agency Interaction Search Public Contracts Tab");
		new AgencyInteraction(driver).clickSearchPublicContractsTab();
	}
    
	
}

