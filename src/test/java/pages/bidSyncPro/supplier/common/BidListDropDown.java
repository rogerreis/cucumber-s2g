package pages.bidSyncPro.supplier.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;

public class BidListDropDown extends BasePageActions {
	
	// xpaths
  	private final String newForYouBtn_xpath     = "//button[@role=\"menuitem\" and contains(span, \"New For You\")]";
	private final String newForYouImg_xpath     = "//button[@role=\"menuitem\"and contains(span, \"New For You\")]/mat-icon[@role=\"img\" and contains(text(), \"flash_on\")]";
//	private final String yourSavedBidsBtn_xpath = "//button[@role=\"menuitem\" and contains(span, \"Your Saved Bids\")]";
	
//	#Fix for release OCT 7
	private final String yourSavedBidsBtn_xpath = "//button[@role=\"menuitem\" and contains(span, \"Your Bids\")]";
//	private final String yourSavedBidsImg_xpath = "//button[@role=\"menuitem\"and contains(span, \"Your Saved Bids\")]/mat-icon[@role=\"img\" and contains(text(), \"star\")]";
	
//	#Fix for release OCT 7
	private final String yourSavedBidsImg_xpath = "//button[@role=\"menuitem\"and contains(span, \"Your Bids\")]/mat-icon[@role=\"img\" and contains(text(), \"star\")]";
	private final String allBidsBtn_xpath       = "//button[@role=\"menuitem\" and contains(span, \"All Bids\")]";
	private final String allBidsImg_xpath       = "//button[@role=\"menuitem\"and contains(span, \"All Bids\")]/mat-icon[@role=\"img\" and contains(text(), \"list\")]";
    
    
    public BidListDropDown(EventFiringWebDriver driver) {
    	super(driver);
    }
    
    /* ----- getters ----- */
    private WebElement getNewForYouBtn()     { return findByVisibility(By.xpath(this.newForYouBtn_xpath));     } 
    private WebElement getYourSavedBidsBtn() { return findByVisibility(By.xpath(this.yourSavedBidsBtn_xpath)); } 
    private WebElement getAllBidsBtn()       { return findByVisibility(By.xpath(this.allBidsBtn_xpath));       } 
    private WebElement getNewForYouImg()     { return findByVisibility(By.xpath(this.newForYouImg_xpath));     } 
    private WebElement getYourSavedBidsImg() { return findByVisibility(By.xpath(this.yourSavedBidsImg_xpath)); } 
    private WebElement getAllBidsImg()       { return findByVisibility(By.xpath(this.allBidsImg_xpath));       } 

   /* ------ methods ----- */
   
   /* ------ verifications ------ */
   
   /***
    * <h1>verifyAllOptionsInBidListDropdown</h1>
    * <p>purpose: Verify that all the options (buttons + images) in the bid list dropdown appear</p>
    * @return BidListDropDown
    */
   public BidListDropDown verifyAllOptionsInBidListDropdown() {
	   this.getAllBidsBtn();
	   this.getAllBidsImg();
	   this.getNewForYouBtn();
	   this.getNewForYouImg();
	   this.getYourSavedBidsBtn();
	   this.getYourSavedBidsImg();
	   
	 	System.out.println(GlobalVariables.getTestID() + " INFO: Verified all options and images in the \"Company Settings\" menu have correctly loaded");
        return this;
   }
}
