package stepDefinitions.bidSyncPro.api;

import java.io.IOException;
import java.io.StringReader;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import core.CoreAutomation;
import cucumber.api.java.en.Given;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pages.common.helpers.GlobalVariables;
import org.apache.commons.codec.binary.Base64;

public class TriggerNightlyEmail_stepDefinitions {

	private EventFiringWebDriver driver;
	private CoreAutomation coreAutomation;

	public TriggerNightlyEmail_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
		coreAutomation = automation;
	}

	@Given("I trigger nightly email for user \"(.*)\"$")
	public void triggerNightlyEmailForUser(String strUser) {

		System.out.printf(GlobalVariables.getTestID() + " INFO: strUser = %s\n", strUser);
		// We should be logged in now, so get the token from the local storage.
		new GetToken_stepDefinitions(coreAutomation).GetUserToken(strUser);
		String token = GlobalVariables.getGlobalVariable("userToken");

		
		// Decode the token and get the user ID out of the token.
		// I commented out the parts we don't need, but still useful to understand what's happening.
		String[] split_string = token.split("\\.");
        //String base64EncodedHeader = split_string[0];
        String base64EncodedBody = split_string[1];
        //System.out.println(GlobalVariables.getTestID() + " ~~~~~~~~~ JWT Header ~~~~~~~");
        Base64 base64Url = new Base64(true);
        //String header = new String(base64Url.decode(base64EncodedHeader));
        //System.out.println(GlobalVariables.getTestID() + " JWT Header : " + header);
        //System.out.println(GlobalVariables.getTestID() + " ~~~~~~~~~ JWT Body ~~~~~~~");
        String body = new String(base64Url.decode(base64EncodedBody));
        System.out.println(GlobalVariables.getTestID() + " JWT Body : "+body);

        // The body of the token is json.  Get the userID out of it and load into variable.  
        JsonReader reader = new JsonReader(new StringReader(body));
		reader.setLenient(true);
		JsonElement jsonTree = new JsonParser().parse(reader);
		String strUserID = jsonTree.getAsJsonObject().get("userId").getAsString();
		System.out.printf(GlobalVariables.getTestID() + " INFO: strUserID = %s\n", strUserID);
		
		// Make a new json to be used to trigger nightly email for the user.
		String jsnBody = "{\"userIds\": [\"" + strUserID + "\"]}";
		
		// Setup to trigger nightly email
		OkHttpClient client = new OkHttpClient();
		MediaType JSON = MediaType.parse("application/json; charset=utf-8");
		RequestBody rbody = RequestBody.create(JSON, jsnBody);
		Request request = new Request.Builder()
				//.url(System.getProperty("buyspeedURL") + "api/admin/v1/onetimeprocess/startNewPublisherBatchWithUsers")
				.url("https://admingateway.phi-qa.cloud/api/admin/v1/onetimeprocess/startNewPublisherBatchWithUsers")
				.header("Authorization", "Bearer " + token).post(rbody).build();
		Response response = null;
		try {
			// Trigger nightly email
			response = client.newCall(request).execute();
			
			// The response is empty, just HTTP 200 status code, so verify.
			int responseCode = response.code();
			System.out.printf(GlobalVariables.getTestID() + " INFO: response code = %s\n", responseCode);
			assert responseCode == 200;
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return;
	}
}
