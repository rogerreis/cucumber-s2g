package pages.bidSyncPro.registration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.common.BidSyncProCommon;

/***
 * <h1>Class JoinCompany</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects related to the objects on the "Join Company" screen that is part of supplier registration<br>
 * 	"Join Company" will appear when the user selected an existing company out of the company lookup that already has a company owner<br>
 * 	and is given the option to request to the join company via emailing the company owner</p>
 * 
 */
public class JoinCompany extends BidSyncProCommon {

    // ids
    private final String sendRequestButton_id = "sendRequestButton";
    private final String nextButton_id        = "nextButton";
	
	// Other
	private EventFiringWebDriver driver;
	
	public JoinCompany (EventFiringWebDriver driver) {
		super(driver);
		this.driver   = driver;
	}
	
	/* ----- getters ----- */
	
	private WebElement getSendRequestButton() { return findByVisibility(By.id(this.sendRequestButton_id)); }
    private WebElement getNextButton()        { return findByVisibility(By.id(this.nextButton_id));        }

	/* ----- methods ----- */
    
    /****
     * <h1>sendRequest</h1>
     * <p>purpose: User has just joined an existing company that already has a company owner.<br>
     * 	The user is viewing a "Join Company" screen indicating that they need to send a request to<br>
     * 	the company owner to join this company. This method clicks on "Send Request" and then "Next"</p>
     * @return EnterKeywords
     */
	public EnterKeywords sendRequest() {
	    getSendRequestButton().click();
	    getNextButton().click();
        return new EnterKeywords(driver);
    }
}
