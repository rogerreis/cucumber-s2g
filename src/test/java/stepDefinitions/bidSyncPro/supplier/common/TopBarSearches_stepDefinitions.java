package stepDefinitions.bidSyncPro.supplier.common;

import core.CoreAutomation;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.bidSyncPro.supplier.dashboard.BidListTabNames;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidListRow;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterCriteria;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterResultsSectionNewClass;
import pages.common.helpers.GlobalVariables;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.support.events.EventFiringWebDriver;

public class TopBarSearches_stepDefinitions {
    
    private EventFiringWebDriver driver;
    
	public TopBarSearches_stepDefinitions(CoreAutomation automation) {
		this.driver     = automation.getDriver();
	}
	
	/* ----- helpers -----*/
	
	/***
	 * <h1>runTopBarSearch</h1>
	 * <p>purpose: Helper to run the top bar search</p>
	 * @param searchTerm = input to the top bar search 
	 */
	private void runTopBarSearch(String searchTerm) {
		
		new CommonTopBar(driver)
				.waitForPageLoad()
				.searchFor(searchTerm);
	}

    /* ------------ Test Steps ---------------- */

	@Then( "^I will verify that All Bids is both highlighted and selected, and also displays correct results per Top Bar keyword \"(.*)\"$")
	public void I_will_verify_that_All_Bids_is_both_highlighted_and_selected_and_also_displays_correct_results_per_Top_Bar_keyword (String strKeyword) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that All Bids tab is selected and highlighted, and bid results appear per Top Bar search");

		new FilterResultsSectionNewClass(driver)
				.verifyTabIsHighlightedAndSelected(BidListTabNames.ALL_BIDS)
				.verifyTopBarSearchDisplaysCorrectResultsInAllBidsTab(strKeyword);

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Each Bid Result matches the keyword \"" + strKeyword + "\" entered into the Top Bar search");
	}	

	@When("^I run a search on the Top Bar with keyword \"(.*)\"$")
	public void I_run_a_search_on_the_Top_Bar_with_keyword (String strKeyword) {
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Searching Top Bar with keyword \"%s\"\n", strKeyword);

		FilterCriteria filter = new FilterCriteria();
		List<String> keywords = new ArrayList<>();
		keywords.add(strKeyword);
		filter.setKeywords(keywords);

		this.runTopBarSearch(strKeyword);
		
		// Set as filter for test cases that use filter
		GlobalVariables.storeObject("filter", filter);
	}	
	
	@Then ("^I will verify that the Top Bar search term is blank$")
	public void I_will_verify_that_the_Top_Bar_search_term_is_blank() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying Top Bar search term is blank");
		new CommonTopBar(driver).verifyTopBarSearchTermIsTerm("");
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Top Bar search term is blank");
	}

	
	@When ("^I run a Top Bar search using the bid number as the keyword$")
	public void I_run_a_Top_Bar_search_using_the_bid_number_as_the_keyword() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Searching Top Bar with bid number as keyword");

		BidListRow bid = GlobalVariables.getStoredObject("randomBid");
		this.runTopBarSearch(bid.bidInfo.getBidNumber());
	}
	
	@When ("^I run a Top Bar search using the NIGP code \"(.*)\" as the keyword$")
	public void I_run_a_Top_Bar_search_using_the_NIGP_code__as_the_keyword(String NIGP_code){
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Searching Top Bar with NIGP code as keyword");
		this.runTopBarSearch(NIGP_code);
	}
	
	
}
