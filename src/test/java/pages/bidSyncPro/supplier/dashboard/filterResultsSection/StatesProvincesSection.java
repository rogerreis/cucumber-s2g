package pages.bidSyncPro.supplier.dashboard.filterResultsSection;

import static org.junit.Assert.fail;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.common.ChipList;
import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.common.helpers.GeographyHelpers;
import pages.common.helpers.HelperMethods;

/***
 * <h1>Class StatesProvincesSection</h1>
 * @author dmidura
 * <p>details: This class houses the objects and methods for the States/Provinces section on the Filter Results</p>
 */
import pages.common.helpers.HelperMethods;
public class StatesProvincesSection extends FilterResultsSectionNewClass {
	
	// Other
	private EventFiringWebDriver driver;
	HelperMethods helperMethods;
	private final String PlaceholderMessage = "States/Provinces";
	
	// xpaths
	// checkbox
	private final String searchAllStatesProvincesTxt_xpath               = "//mat-slide-toggle[@id='searchAllStates']//span[contains(text(), 'Search all states/provinces')]";
	private final String searchAllStatesProvincesCheckbox_xpath          = "//mat-slide-toggle[@id='searchAllStates']";
//	private final String searchAllStatesProvincesCheckboxIndicator_xpath = "//input[@id='searchAllStates-input']//following-sibling::div[contains(@class,'mat-slide-toggle-thumb-container')]";
	private final String searchAllStatesProvincesCheckboxIndicator_xpath = "//input[@id='searchAllStates-input']";
	// input
	private final String statesProvincesInput_xpath                      = "//mat-form-field[@id='matSelectStatesFormField']";
	private final String statesProvincesInputBox_xpath                   = "//mat-form-field[@id='matSelectStatesFormField']//input[@placeholder='States/Provinces']";
	private final String statesProvincesInputUnderline_xpath             = "//mat-form-field[@id='matSelectStatesFormField']//div[contains(@class, 'mat-form-field-underline')]";
	private final String statesProvincesAddBtn_xpath                     = "//button[@id='btnAddState']";
	//private final String statesProvincesChipList_xpath                   = "//mat-chip-list[@id = 'matStateChipList']";
	
	//Fix for release OCT 7
	private final String statesProvincesChipListAfterBacktoBid_xpath     =  "//mat-chip-list[@id = 'mat-chip-list-5']";
	private final String statesProvincesChipList_xpath                   =  "//mat-expansion-panel[@id='matStateExpPanel']//mat-chip-list";
//	private final String allUSStateToggle_xpath			 				 = "//mat-slide-toggle[@id='searchUSStates']//div[@class='mat-slide-toggle-bar']";
	private final String allUSStateToggle_xpath			 				 = "//mat-slide-toggle[@id='searchUSStates']";
	
	public StatesProvincesSection(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
		this.helperMethods = new HelperMethods();
	}
	
	/* ----- getters ----- */
	// checkbox
	private WebElement getSearchAllStatesProvincesTxt()               { return findByVisibility(By.xpath(this.searchAllStatesProvincesTxt_xpath));               }
	public WebElement getSearchAllStatesProvincesCheckbox()          { return findByVisibility(By.xpath(this.searchAllStatesProvincesCheckbox_xpath));          }
	private WebElement getSearchAllStatesProvincesCheckboxIndicator() { helperMethods.addSystemWait(1); return findByVisibility(By.xpath(this.searchAllStatesProvincesCheckboxIndicator_xpath)); }
	// input
	private WebElement getStatesProvincesInput()          { return findByVisibility(By.xpath(this.statesProvincesInput_xpath));          }
	private WebElement getStatesProvincesInputBox()       { return findByVisibility(By.xpath(this.statesProvincesInputBox_xpath));       }
	private WebElement getStatesProvincesInputUnderline() { return findByVisibility(By.xpath(this.statesProvincesInputUnderline_xpath)); }
	private WebElement getStatesProvincesAddBtn()         { return findByVisibility(By.xpath(this.statesProvincesAddBtn_xpath));         }
	private WebElement getAllUSStateToggleIcon()  			  { return findByVisibility(By.xpath(this.allUSStateToggle_xpath));           }
	/* ----- methods ----- */
	
	/***
	 * <h1>isCheckboxChecked</h1>
	 * <p>purpose: Test if "Search all states/provinces" checkbox is checked
	 * @return Boolean = true if checked | false if not checked
	 */
	private Boolean isCheckboxChecked(){
		return this.getSearchAllStatesProvincesCheckboxIndicator().isSelected();
		
	}
	
	/***
	 * <h1>checkSearchAllStatesProvincesCheckbox</h1>
	 * <p>purpose: Check the "Search all states/provinces" checkbox if it is not already checked</p>
	 * @return BidEndDateSection
	 */
	public StatesProvincesSection checkSearchAllStatesProvincesCheckbox() {
		if (!isCheckboxChecked()) { this.getSearchAllStatesProvincesCheckbox().click();}
		return this;
	}

	/***
	 * <h1>uncheckSearchAllStatesProvincesCheckbox</h1>
	 * <p>purpose: Uncheck the "Search all states/provinces" checkbox if it is not already checked</p>
	 * @return BidEndDateSection
	 */
	public StatesProvincesSection uncheckSearchAllStatesProvincesCheckbox() {
		if (isCheckboxChecked()) {
			helperMethods.addSystemWait(3);
//			this.getSearchAllStatesProvincesCheckboxIndicator().click();
			((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getSearchAllStatesProvincesCheckboxIndicator());
		}
		return this;
	}
	
	public StatesProvincesSection uncheckAllUsStateCheckbox() {
		//Fix for release OCT 7
		//if (!getAllUSStateToggleIcon().isSelected()) {
			this.getAllUSStateToggleIcon().click();
		//}
		return this;
	}
	
	public StatesProvincesSection checkAllUsStateCheckbox() {
		//Fix for release OCT 7
		if (!getAllUSStateToggleIcon().isSelected()) {
			this.getAllUSStateToggleIcon().click();
		}
		return this;
	}
	
	/***
	 * <h1>getStatesProvincesChipList</h1>
	 * <p>purpose: Access the States/Provinces ChipList</p>
	 * @return ChipList
	 */
	public ChipList getStatesProvincesChipList(){
		return new ChipList(driver, this.statesProvincesChipList_xpath);
	}

//#Fix for release OCT 7
	/***
	 * <h1>getPositiveKeywordsChipList</h1>
	 * <p>purpose: Access the Positive Keywords ChipList after click back to bid</p>
	 * @return ChipList
	 */
	public ChipList getStatesProvincesChipListAfterBackToBid(){
		return new ChipList(driver, this.statesProvincesChipListAfterBacktoBid_xpath);
	}
	/***
	 * <h1>clickAddBtn</h1>
	 * <p>purpose: Click on the add (+) button on the States/Provinces Input</p>
	 * @return KeywordsSection
	 */
	private StatesProvincesSection clickAddBtn() { 
		this.getStatesProvincesAddBtn().click(); 
		return this;
	}
	
	/***
	 * <h1>addStateProvince</h1>
	 * <p>purpose: Add a State/Province into the input and then click on "+"</p>
	 * @param strStateProvince = state | province to add<br>
	 * 	Can be either full text or the abbreviation<br>
	 * 	(i.e. "North Carolina" | "NC")</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass addStateProvince (String strKeyword) {
		this.openStatesProvincesDropdown();
		this.getStatesProvincesInput().sendKeys(strKeyword);
		this.clickAddBtn();
		return this;
	}
	
	/***
	 * <h1>addStatesProvinces<h1>
	 * <p>purpose: Add a list of States/Provinces into the input, clicking on "+" after each addition</p>
	 * @param List <String> statesProvincesList = list of your states and provinces
	 * 	Items in list should be abbreviations of the states/provinces</p>
	 * @return FilterResultsSectionNewClass
	 */
//	public FilterResultsSectionNewClass addStatesProvinces(List <String> statesProvincesList){
//		for (String strStateProvince : statesProvincesList) {
//			if (strStateProvince.length() != 2) { fail("Test Failed: Expected state/province abbreviation into addStatesProvinces, but got " + strStateProvince);}
//			this.openStatesProvincesDropdown()
//			.clickOnStateProvinceFromStatesProvincesDropdown(strStateProvince)
//			.clickAddBtn();
//		}
//		return this;
//	}
	
	public FilterResultsSectionNewClass addStatesProvinces (List <String> statesProvincesList){
		GeographyHelpers geographyHelpers =new GeographyHelpers();
		for (String strStateProvince : statesProvincesList) {
			if (strStateProvince.length() != 2) { fail("Test Failed: Expected state/province abbreviation into addStatesProvinces, but got " + strStateProvince);}
			getStatesProvincesInputBox().sendKeys(geographyHelpers.getStateOrProvinceAbbreviationFromStateName(strStateProvince),Keys.ARROW_DOWN,Keys.ENTER);
		}
		return this;
	}
	
	/***
	 * <h1>openStatesProvincesDropdown</h1>
	 * <p>purpose: Click on the "States/Provinces" input to open the dropdown.<br>
	 * 	You can then select a State/Province value from the dropdown.<br>
	 * @return StatesProvincesDropdown
	 */
	public StatesProvincesDropdown openStatesProvincesDropdown () {
		this.getStatesProvincesInput().click();
		return new StatesProvincesDropdown(driver);
	}
	
	/* ----- verifications ----- */
	
	/***
	 * <h1>verifySearchAllStatesProvincesCheckboxChecked</h1>
	 * <p>purpose: Verify that the "Search all states/provinces" checkbox is checked.<br>
	 * 	Fail test if the checkbox is not checked</p>
	 * @return StatesProvincesSection
	 */
	public StatesProvincesSection verifySearchAllStatesProvincesCheckboxChecked() {
		if (!this.isCheckboxChecked()) {fail("Test Failed: \"Search all states/provinces checkbox is not checked");}
		return this;
	}

	/***
	 * <h1>verifySearchAllStatesProvincesCheckboxUnchecked</h1>
	 * <p>purpose: Verify that the "Search all states/provinces" checkbox is not checked.<br>
	 * 	Fail test if the checkbox is checked</p>
	 * @return StatesProvincesSection
	 */
	public StatesProvincesSection verifySearchAllStatesProvincesCheckboxUnchecked() {
		if (this.isCheckboxChecked()) {fail("Test Failed: \"Search all states/provinces checkbox is checked");}
		return this;
	}
	
	
	/***
	 * <h1>verifySectionInitalization</h1>
	 * <p>purpose: Verify that the States/Provinces section has initialized per:<br>
	 * 	1. "Search all states/provinces" checkbox checked.<br>
	 * 	After unchecking, verify:<br>
	 * 	1. input bar with "+" button<br>
	 * 	2. Default placeholder message in input of "Enter Keywords"<br>
	 * 	3. Chip list is empty<br>
	 * 	Fail test if any of the above conditions are not met</p>
	 * @return StatesProvincesSection
	 */
	public StatesProvincesSection verifySectionInitialization() {

		// 1. Verify "Search all states/provinces" checkbox initializes checked
		this.verifySearchAllStatesProvincesCheckboxChecked();
		
		// Checkbox should be displayed with text
		if (!this.getSearchAllStatesProvincesCheckbox().isDisplayed() | !this.getSearchAllStatesProvincesTxt().isDisplayed()) {
			fail("Test Failed: \"Search all states/provinces\" checkbox is not displaying with text"); }
		
		// 2. Now check uncheckbox
		this.uncheckSearchAllStatesProvincesCheckbox()
			.verifySearchAllStatesProvincesCheckboxUnchecked();

		// And verify that input with "+" button is visible
		if (!this.getStatesProvincesInput().isDisplayed() | !this.getStatesProvincesInputBox().isDisplayed()) {
			fail("Test Failed: States/Provinces input is not currently displayed on screen"); }

		if (!this.getStatesProvincesInputUnderline().isDisplayed()) {
			fail("Test Failed: States/Provinces input underline is not currently displayed on screen"); }

		if (!this.getStatesProvincesAddBtn().isDisplayed()) {
			fail("Test Failed: States/Provinces input add button (\"+\") is not currently displayed on screen"); }

		// And displays correct default placeholder message
		if (!this.getStatesProvincesInputBox().getAttribute("placeholder").equals(this.PlaceholderMessage)) {
			fail("Test Failed: States/Provinces input did not initialize with expected placeholder \"" + this.PlaceholderMessage + "\""); }
		
		// 3. Now check checkbox again and verify input with "+" is hidden
		this.checkSearchAllStatesProvincesCheckbox()
			.verifySearchAllStatesProvincesCheckboxChecked();
		
		if (this.doesPageObjectExist(driver, By.xpath(this.statesProvincesInput_xpath)) |
		    this.doesPageObjectExist(driver, By.xpath(this.statesProvincesInputBox_xpath))) {
			fail("Test Failed: States/Provinces input is currently displayed on screen after checking \"Search all states/provinces\""); }

		if (this.doesPageObjectExist(driver, By.xpath(this.statesProvincesInputUnderline_xpath))) {
			fail("Test Failed: States/Provinces input underline is currently displayed on screen after checking \"Search all states/provinces\""); }

		if (this.doesPageObjectExist(driver, By.xpath(this.statesProvincesAddBtn_xpath))){
			fail("Test Failed: States/Provinces input add button (\"+\") is currently displayed on screen after checking \"Search all states/provinces\""); }
		
		// 4. Close/Open the "States/Provinces" section header, and verify the section is closed/open
		this.closeStatesProvincesSection()
			.verifyStatesProvincesSectionIsClosed()
			.openStatesProvincesSection()
			.verifyStatesProvincesSectionIsOpen();

		return this;
	}
}
