package stepDefinitions.bidSyncPro.supplier.account.accountSettings;
 
import org.junit.Assert;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.bidSyncPro.registration.RandomChargebeeUser;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.account.accountSettings.ResetPasswordModal;
import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.bidSyncPro.supplier.common.LocalBrowserStorageBidSyncPro;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.RandomHelpers;
import pages.mailinator.Mailinator;

public class ChangePassword_stepDefinitions {
  
	private EventFiringWebDriver driver;
	
	public ChangePassword_stepDefinitions(CoreAutomation automation) {
	    this.driver = automation.getDriver();
	} 
	
	/* ----- helpers ----- */
	
	/***
	 * <h1>issuePasswordReset</h1>
	 * <p>purpose: Navigate to the My Account screen, click on Password Reset,<br>
	 * 	and navigate through all the popups so that the Change Password email is sent</p>
	 */
	private void issuePasswordReset() {
		new CommonTopBar(driver)
		.waitForPageLoad()
		.openAccountDropDown()
		.clickAccountSettings()
		.changePassword();
	}
	
	/***
	 * <h1>retrieveChangePasswordEmail</h1>
	 * <p>purpose: Navigate to inbox, retrieve "Change Password" email and verify content, click on URL and navigate</p>
	 * @param email = String with user email
	 */
	private void retrieveChangePasswordEmail(String email) {

		// Navigate to the correct Mailinator inbox
		new Mailinator(driver)
				.navigateToMailinatorInbox(email)
				.openChangePasswordEmail()
				.verifyBodyTextIsCorrect()
				.verifyFooterTextIsCorrect()
				.clickOnTokenURLInEmail()
				.switchToWindow("S2G");
	}

	
		
	/* ------------ Test Steps ---------------- */

	@When ("^I try to change my password from the My Account page$")
	public void I_try_to_change_my_password_from_the_My_Account_page() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Navigating to my account page, issuing password reset, and retrieving email");

		// 1. Navigate to My Account and issue password reset
		RandomChargebeeUser storedUser = GlobalVariables.getStoredObject("StoredUser");
		this.issuePasswordReset();

		// 2. Get the JWT Token for this session
		String token = new LocalBrowserStorageBidSyncPro(driver).getToken();
		Assert.assertNotNull("Test Failed: JWT token is null or blank when logged in", token);
		GlobalVariables.storeObject("JWTToken", token);
		
		// 3. Retrieve the Change Password email
		this.retrieveChangePasswordEmail(storedUser.getEmailAddress());
	}
	
	@Then ("^I will verify that my original JWT token is deleted in local browser storage and a new JWT token is issued$")
	public void I_will_verify_that_my_original_JWT_token_is_deleted_in_local_browser_storage_and_a_new_JWT_token_is_issued() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying JWT token deleted and reissued in local browser storage");
		
		// 1. Verify user is now viewing Reset Password modal
		ResetPasswordModal resetPassword = new ResetPasswordModal(driver);
		resetPassword.verifyPageInitialization();
		
		// 2. Verify that the JWT token was deleted
		Assert.assertNull( "Test Failed: JWT token was not deleted when Reset Password.", new LocalBrowserStorageBidSyncPro(driver).getToken());
		
		// 3. Now reset the password
		resetPassword.resetPassword("P@ssw0rdReset");
		// and save out new password
		RandomChargebeeUser storedUser = GlobalVariables.getStoredObject("StoredUser");
		storedUser.setPassword("P@ssw0rdReset");
		GlobalVariables.storeObject("StoredUser", storedUser);
		
		// 4. Verify that a new JWT token was issued
		String newToken = new LocalBrowserStorageBidSyncPro(driver).getToken();
		String oldToken = GlobalVariables.getStoredObject("JWTToken");
		Assert.assertNotNull  ("Test Failed: No JWT token was generated after resetting password.", newToken);
		Assert.assertNotEquals("Test Failed: New JWT token was not issued after password reset. Old token is still in use.", newToken, oldToken);
		
	}
	
	@When ("^I submit a password that does not match the confirmation password into the Reset Your Password dialog$")
	public void I_submit_a_password_that_does_not_match_the_confirmation_password_into_the_Reset_Your_Password_dialog(){
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Entering password that doesn't match confirmation password and then clicking \"Reset Password\" button");

		// Password is generated randomly. Confirm password is just a junk value
		String password = new RandomHelpers().getRandomPasswordForBidSync();

		new ResetPasswordModal(driver)
			.verifyPageInitialization()
			.setPasswordInput(password)
			.setConfirmPasswordInput("P@ssw0rd")
			.clickResetPasswordButton();
		
		GlobalVariables.storeObject("randomPassword", password);
	}

	@Then ("^I will see an error message for the mismatched password and confirm password fields on the Reset Your Password dialog$")
	public void I_will_see_an_error_message_for_the_mismatched_password_and_confimr_password_fields_on_the_Reset_Your_Password_dialog() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that \"Passwords do not match\" message is displaying");

		new ResetPasswordModal(driver)
			.verifyPasswordMismatchErrorDisplays();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: \"Passwords do not match\" message is displaying");
	}

	@When("^I update the confirm password field to match the password field on the Reset Your Password dialog$")
	public void I_update_the_confirm_password_field_to_match_the_password_field_on_the_Reset_Your_Password_dialog() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Updating confirmation password to match password");

		// Update the confirmation password to match what is already inputted into the password field
		String password = GlobalVariables.getStoredObject("randomPassword");
		new ResetPasswordModal(driver)
			.clearConfirmPasswordInput()
			.setConfirmPasswordInput(password);  
	}

	@Then ("^I will verify that the Reset Your Password dialog does not display any error message$")
	public void I_will_verify_that_the_Reset_Your_Password_dialog_does_not_display_any_error_message() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that no error messages are displayed on Reset Your Password modal");

		new ResetPasswordModal(driver)
			.verifyPasswordMismatchErrorDoesNotDisplay()
			.verifyPasswordValidationErrorDoesNotDisplay();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: No error messages are displayed on the Reset Your Password modal");
	}

	@And ("^I can successfully submit my new password through the Reset Your Password dialog and log in with this new password$")
	public void I_can_successfully_submit_my_new_password_through_the_Reset_Your_Password_dialog_and__log_in_with_this_new_password() throws InterruptedException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that password has successfully reset");

		// 1. Complete the password reset by clicking "Reset Password" on modal
		// 2. Now verify that user can log in with this new password
		new ResetPasswordModal(driver)
			.clickResetPasswordButton()
			.verifyUserIsOnMyAccountTab()
			.logout()
			.loginWithCredentials(SupplierLogin.thisUser.getEmail(), SupplierLogin.thisUser.getPassword())
			.waitForPageLoad();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Password Reset was successful");
	}
	
	@When ("^I try to update my account with \"(.*)\" that does not meet validation$")
	public void I_try_to_update_my_account_with_password_that_does_not_meet_validation(String password){
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Updating account with password = \""+ password + "\" that does not meet validation");

		new ResetPasswordModal(driver)
			.setPasswordInput(password)
			.setConfirmPasswordInput(password);
	}

	@Then ("^I will see an error message to update my password per validation specification on the Reset Your Password dialog$")
	public void I_will_see_an_error_message_to_update_my_password_per_validation_specification_on_the_Reset_Your_Password_dialog() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that password validation error message is displayed");

		new ResetPasswordModal(driver)
			.verifyPasswordValidationErrorDisplays();
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Error message about password validation displayed");
	}

	@And ("^I will see the appropriate password validation criteria highlighted$")
	public void I_will_see_the_appropriate_password_validation_criteria_highlighted() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that password validation error message is displayed");

		new ResetPasswordModal(driver)
			.verifyCorrectPasswordCriteriaIsHighlighted();
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Correct password validation criteria are highlighted");
	}

	@And ("^I will see that the Reset Password button is disabled on the Reset Your Password dialog$")
	public void I_will_see_that_the_Reset_Password_button_is_disabled_on_the_Reset_Your_Password_dialog() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that \"Reset Password\" button is disabled");
		
		new ResetPasswordModal(driver)
			.verifyResetPasswordButtonIsDisabled();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: \"Reset Password\" button is disabled");
	}

	@When ("^I update the password and confirm password fields to match and also to meet password validation on the Reset Your Password dialog$")
	public void I_update_the_password_and_confirm_password_fields_to_match_and_also_to_meet_validation_on_the_Reset_Your_Password_dialog() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clearing values in password and confirm password fields and then replacing with proper password");

		String password = new RandomHelpers().getRandomPasswordForBidSync();

		new ResetPasswordModal(driver)
			.clearPasswordInput()
			.clearConfirmPasswordInput()
			.setPasswordInput(password)
			.setConfirmPasswordInput(password);
	}
	
	@And ("^I enter and confirm a new \"(.*)\" in the Reset Your Password dialog$")
	public void I_enter_and_confirm_a_new_password_in_the_Reset_Your_Password_dialog(String password) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Setting new password = \"" + password + "\" in password and confirm password fields");
		
		new ResetPasswordModal(driver)
			.setPasswordInput(password)
			.setConfirmPasswordInput(password);
	}
}



