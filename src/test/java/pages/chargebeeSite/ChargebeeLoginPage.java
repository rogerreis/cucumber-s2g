package pages.chargebeeSite;

import javax.swing.JOptionPane;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.chargebeeSite.common.CommonNavSliderMenu;
import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.BasePageActions;
/***
 * <h1>ChargebeeLoginPage</h1>
 * @author dmidura
 * <p>details: This class houses methods and page objects for the login screen of the Chargebee test site</p>
 */
public class ChargebeeLoginPage extends BasePageActions{
	
	// xpaths
	private final String nameInput_xpath 		= "//form[@id='users_login_submit']//input[@id='email']";
//	private final String nameInput_xpath 		= "//input[@id=\"email\"]";
	private final String passwordInput_xpath 	= "//form[@id='users_login_submit']//input[@id=\"password\"]";
	private final String signInBtn_xpath 		= "// input[@type=\"submit\" and @value=\"Sign in\"]";
	private final String loginBtn_xpath			= "//header[@id='ch-js-header']//a[contains(text(),'Log in')]";
	private final String testSite_xpath			= "//div[@class='cn-ss__list']//div[1]//a[@data-form-id='select-bidsync-test']";
	
	//Other 
	private EventFiringWebDriver driver;
	private final String strPagePath            = System.getProperty("chargebeeTestSiteURL");
	
	// Admin Login Credentials
	private String login                        = System.getProperty("chargebeeTestSiteLogin");
	private String password                     = System.getProperty("chargebeeTestSitePassword");

	public ChargebeeLoginPage (EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	/* ----- getters ----- */
	private WebElement getNameInput()     {return findByVisibility(By.xpath(this.nameInput_xpath));    }
	private WebElement getPasswordInput() {return findByVisibility(By.xpath(this.passwordInput_xpath));}
	private WebElement getSignInBnt()     {return findByVisibility(By.xpath(this.signInBtn_xpath));    }
	private WebElement getLoginBtn()      {return findByVisibility(By.xpath(this.loginBtn_xpath));    }
	private WebElement getTestSite()      {return findByVisibility(By.xpath(this.testSite_xpath));    }
	/* ----- methods ----- */
	/***
	 * <h1>loginAsChargebeeAdmin</h1>
	 * <p>purpose: Navigate to the chargebee test site.<br>
	 * 	Log into the Chargebee test site as the admin user<br>
	 * 	After login, user is navigated to a dashboard (and verifies common nav menu loads).<br>
	 *  Use the CommonNavSliderMenu options to navigate to your correct dashboard</p>
	 * 	Note: Admin login credentials are set in CoreAutomation.java<br>
	 * @return CommonNavSliderMenu
	 */
	public CommonNavSliderMenu loginAsChargebeeAdmin() {
		driver.navigate().to(this.strPagePath);
		this.getLoginBtn().click();
		this.getNameInput().sendKeys(this.login);
		this.getPasswordInput().sendKeys(this.password);
		this.getSignInBnt().click();
		// uncomment this to handle captcha manually
		// COMMENT THIS BACK OUT BEFORE CHECKING IT IN!!
		//JOptionPane.showMessageDialog(null,"Click OK after you've dealt with captcha");
		this.getTestSite().click();
		return new CommonNavSliderMenu(driver).verifyMenuLoad();
	}
}
