@must @ManageUsers @subscriptions @level3
Feature: Company Settings > Management Users - Company Owner
	ARGO-1258: "Manage Additional Subscription Licenses (assign/remove purchased licenses)"

dmidura: Per 12.10.2018 discussion with jbarlow, during registration,  Federal and Military addOn packages are correlated to
the user's subscription plan (i.e. Federal addOn to National sees all states, but Federal addOn to KS State plan will only see the KS Federal bids)
Canada addOn is for all Canadian provinces. This behavior may change in the future, in which case the BidListRow.verifyBidAccessIsCorrect()
method should be updated.
  

@ARGO-1258
Scenario Outline: User that purchases plan is assigned Subscription Owner Role

	Given I register a new company owner with a "<subscriptionType>" subscription plan, with add-ons and some licenses
	When I have navigated to Company Settings > "Manage Users"
	Then I will verify that the user plan plus add-ons will be displayed in the subscriptions column
	
	Examples:
	| subscriptionType           |
	| BidSync National - Yearly  |
	| BidSync National - Monthly |
	| BidSync National - 6 Month |
	| BidSync Regional - Yearly  |
	| BidSync Regional - Monthly |
	| BidSync Regional - 6 Month |
	| BidSync State - Yearly     |
	| BidSync State - Monthly    |
	| BidSync State - 6 Month    |
 