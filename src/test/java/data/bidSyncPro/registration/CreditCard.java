package data.bidSyncPro.registration;


/***
 * <h1>Class CreditCard</h1>
 * <p>details: This data class defines a credit card used for Chargebee registration.<br>
 * 	 Note: Class constructor defines QA canned card -- use setters in this class if you want to create a different card </p>
 */
public class CreditCard {
    
    private String number;
    private String expiryMonth;
    private String expiryYear;
    private String cvv;
    
    /***
     * <h1>CreditCard</h1>
     * <p>purpose: Set the canned credit card per:<br>
     * 	1. number        = "4111-1111-1111-1111"<br>
     * 	2. expiraryMonth = "10"<br>
     * 	3. expiryYear    = "2020"<br>
     *  4. cvv           = "111"</p>
     */
    public CreditCard() {
    	// Canned QA credit card
        this.number                 = "4111-1111-1111-1111";
        this.expiryMonth            = "10";
        this.expiryYear             = "2020";
        this.cvv                    = "111";
    }
    
    /* ----- getters ----- */
    public String getCardNumber () { return number;      }
    public String getExpiryMonth() { return expiryMonth; }
    public String getExpiryYear () { return expiryYear;  }
    public String getCvv        () { return cvv;         }
    
    /* ----- setters ----- */
    public void setCardNumber (String number     ) { this.number      = number;     }
    public void setExpiryMonth(String expiryMonth) { this.expiryMonth = expiryMonth;}
    public void setExpiryYear (String expiryYear ) { this.expiryYear  = expiryYear; }
    public void setCvv        (String cvv        ) { this.cvv         = cvv;        }
}
