package stepDefinitions.bidSyncClassic.agency.createContract;

//import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;


public class CreateContract_stepDefinitions {

	public CoreAutomation automation;
	public EventFiringWebDriver driver;	

	public CreateContract_stepDefinitions(CoreAutomation automation) throws Throwable {
		this.automation = automation;
		this.driver = automation.getDriver();		
	}

	@Then("^I will create a new contract$")
	public void I_will_create_a_new_contract() {
		try {
			/*int count = 1;
			while (count <= 10) {*/
				new HelperMethods().waitForVisibilityOfId(driver, "search", Integer.parseInt(System.getProperty("defaultTimeOut")));
						
				
				//Generate a random value for naming the bid
				/*Random rand = new Random();
				int value = rand.nextInt(10000);*/				
				//Store it off for future use				
				driver.findElement(By.xpath("//span/ul/li/a[contains(text(),'Contracts')]")).click();				
				driver.findElement(By.xpath("//a[contains(text(),'New Contract')]")).click();
				driver.findElement(By.xpath("//table[1]//tbody[1]//tr[3]//td[2]//input[1]")).sendKeys("test");
				/*driver.findElement(By.id("bidtitle")).clear();
				driver.findElement(By.id("bidtitle")).sendKeys(bid_title);
				driver.findElement(By.linkText("Save Bid")).click();.
				driver.findElement(By.linkText("Return to Bid List")).click();
				driver.findElement(By.id("search")).sendKeys(bid_title);
				driver.findElement(By.linkText("Submit")).click();
				driver.findElement(By.xpath("(//input[@type='checkbox'])[last()]")).click();
				driver.findElement(By.partialLinkText("Release")).click();		
				driver.findElement(By.xpath("//input[@name='pwd']")).sendKeys("test1234");
				driver.findElement(By.partialLinkText("Release")).click();
				count++;*/
			
				
		} catch(Exception e) {
			System.out.println("Error: Was unable to create a contract.\n");
			}



}
}