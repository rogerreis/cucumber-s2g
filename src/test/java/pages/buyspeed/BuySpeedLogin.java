package pages.buyspeed;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import pages.buyspeed.agency.AgencyHome;
import pages.buyspeed.seller.SellerHome;
import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class BuySpeedLogin</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects related to the BuySpeed login screen</p>
 */
public class BuySpeedLogin extends BasePageActions{

	// Login Accounts
	private Map<String, UserLoginAccountBuySpeed> LoginAccounts; 	// Holds all the login accounts 
			                                                		//	form: <reference name for account, UserLoginAccountBuySpeed>
																	//  Note: Login Accounts should be set in the file userAccounts_BuySpeed.json
																	//        The file will then be automatically read in via the method
																	// 		  loadUserAccounts() in this class' constructor.
	// xpaths
	private final String signInBtn_xpath     = "//button[@id=\"home-sign-in-btn\" and contains(span, \"Sign In\")]";
	private final String loginIDInput_xpath  = "//form[@id=\"homeLoginForm\"]//input[@id=\"homeLoginForm:loginId\"]";
	private final String passwordInput_xpath = "//form[@id=\"homeLoginForm\"]//input[@id=\"homeLoginForm:password\"]";
	private final String signInFormBtn_xpath = "//form[@id=\"homeLoginForm\"]//button[@id=\"homeLoginForm:sign-in-dialog-btn\"]";

	// Other 
	private EventFiringWebDriver driver;
	private String strPagePath = System.getProperty("buyspeedURL");
	private String strUserAccountFileName = "userAccounts_BuySpeed.json";

	public BuySpeedLogin (EventFiringWebDriver driver) {
		super(driver);
		this.driver		= driver;
		LoginAccounts	= new HashMap <String, UserLoginAccountBuySpeed>();
		loadUserAccounts();
	}
	
	/* ----- getters ----- */

	private WebElement getSignInBtn()     { return findByPresence(By.xpath(this.signInBtn_xpath));     }
	private WebElement getLoginIDInput()  { return findByPresence(By.xpath(this.loginIDInput_xpath));  }
	private WebElement getPasswordInput() { return findByPresence(By.xpath(this.passwordInput_xpath)); }
	private WebElement getSignInFormBtn() { return findByPresence(By.xpath(this.signInFormBtn_xpath)); }
	
	/* --------------- Methods --------------- */
	
	/**
	 * <h1>getBuySpeedLoginPageURL</h1>
	 * <p>purpose: Get the URL for the BuySpeed login page (where bso Supplier login and registration occurs)</p>
	 * @param None
	 * @return String = URL of the BuySpeed login Page
	 */
	public String getBuySpeedLoginPageURL() {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Getting the Page Path = %s\n", strPagePath);
		return strPagePath;
	}
	
	/***
	 * <h1>Login</h1>
	 * <p>purpose: Log into buyspeed. Hook into another method<br>
	 * 	(ex: LoginBPUser) to return the correct home screen after<br>
	 *  successfull login</p>
	 */
	private void Login(String strUserReference) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Logging is as user %s\n", strUserReference);
		
		driver.navigate().to(this.getBuySpeedLoginPageURL());
		
		// First verify that our user account was defined in userAccounts_BuySpeed.json
		if (LoginAccounts.containsKey(strUserReference)) {
			UserLoginAccountBuySpeed myUser = LoginAccounts.get(strUserReference);
			System.out.printf(GlobalVariables.getTestID() + " INFO: Logging in with User Name     = %s\n", myUser.getName());
			System.out.printf(GlobalVariables.getTestID() + " INFO: Logging in with User Password = %s\n", myUser.getPassword());
			
			// Click on the sign in button
			this.getSignInBtn().click();

			// Next, grab the user info and login
			this.getLoginIDInput().sendKeys(myUser.getName());
			this.getPasswordInput().sendKeys(myUser.getPassword());
			
			// And click login
			this.getSignInFormBtn().click();
		} else {
			fail("Test Failed: Login user does not exist in user definitions.");
		}
			
	}
	
	/***
	 * <h1>loginAsAgencyUser</h1>
	 * <p>purpose: Login into BuySpeed as the specified agency user (DA, AP, BP)</p>
	 * @param strUserReference = the name to reference your user by, as defined in userAccounts_BuySpeed.json
	 * @return BPHome
	 */
	public AgencyHome loginAsAgencyUser(String strUserReference) {
		this.Login(strUserReference);
		return new AgencyHome(driver);
	}
	
	/***
	 * <h1>loginAsSellerUser</h1>
	 * <p>purpose: Login into BuySpeed as the specified user</p>
	 * @param strUserReference = the name to reference your user by, as defined in userAccounts_BuySpeed.json
	 * @return SellerHome
	 */
	public SellerHome loginAsSellerUser(String strUserReference) {
		this.Login(strUserReference);
		return new SellerHome(driver).waitForPageLoad();
	}


	/***
	 * <h1>loadUserAccounts</h1>
	 * <p>purpose: User accounts are defined in the file strUserAccountFileName<br>
	 *             This method reads in that file and then sets the data for access. </p>
	 * @param None
	 * @return None
	 */
	private void loadUserAccounts() {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Loading users from %s\n", strUserAccountFileName);

		// Open strUserAccountFileName file for reading
		Gson gson             = new Gson();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(strUserAccountFileName));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
				
		// Grab the account info for all accounts stored in userAccount.json
		Type type = new TypeToken<List<UserLoginAccountBuySpeed>>(){}.getType();

		List<UserLoginAccountBuySpeed> allAccounts = gson.fromJson(reader, type);
		
		// And now save out that info for each login account, so that we can access this info
		for (UserLoginAccountBuySpeed infoForOneAccount : allAccounts){
			LoginAccounts.put(infoForOneAccount.getAccountReference(), infoForOneAccount);
		}
	}
}
