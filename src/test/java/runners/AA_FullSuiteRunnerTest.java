package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

// This runner is used by jenkins to run the full test suite.
// Tests should be tagged with @must to be included in the jenkins nightly run.
// Tests tagged ~@ignore will be skipped from the jenkins nightly run.
@RunWith(Cucumber.class)
@CucumberOptions(
		plugin ={"pretty" , "json:target/cucumber/cucumber.json", "html:target/cucumber"},
        features = "src/test/resources/features",
        glue = {"stepDefinitions"},
        tags = {"~@ignore", "@must"},

        dryRun = false,
        snippets=SnippetType.CAMELCASE
)

public class AA_FullSuiteRunnerTest {

}
