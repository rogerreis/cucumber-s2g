package pages.bidSyncPro.supplier.account.accountSettings;

import java.time.Duration;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.BasePageActions;

/**
 * <h1>Class ResetPasswordModal</h1>
 * <p>details: This class contains page objects and methods for the "Reset Your Password" dialog that appears when a user is resetting their password<br>
 * 	The ResetPasswordModal will display after the user has either:<br>
 * 	1. Clicked on "Forgot Password" from the login screen and processed the Forgot Password screens (classes: SupplierLogin, ForgotYourPassword),<br>
 * 	   retrieved their email, and activated the reset token<br>
 * 	2. Navigated to My Account->Account Settings->Change Password and processed through the Change Password screens<br>
 * 	   (classes: ChangePasswordModal_Step1, ChangePasswordModal_Step2, ChangePasswordModal_Step3),<br>
 * 	   retrieved their email, and activated the reset token</p>
 */
public class ResetPasswordModal extends BasePageActions {

	// ids
	private final String headerLabel_id            = "resetPasswordText";       //text: Reset your password
	private final String recoveryError_id          = "errorRecoverMessage";     //contains text: Error!  Server was unable to reset your password
	private final String passwordLabel_id          = "matPasswordFormField";    //contains text: Password
	private final String passwordInput_id          = "password2";
	private final String passwordError_id          = "passwordSecurityRule";    //text: Password must be between 8 and 128 characters long, and must contain at least 3 of the following criteria: at least 1 uppercase, at least 1 lowercase, at least 1 digit, at least 1 special character.
	private final String confirmPasswordLabel_id   = "confirmPassword";         //contains text: Confirm Password
	private final String confirmPasswordInput_id   = "confirmPassword";
	private final String confirmPasswordError_id   = "matConfirmPasswordError"; //confirms text: Passwords do not match
	private final String cancelButton_id           = "cancelButton";            //contains text: Cancel
	private final String resetPasswordButton_id    = "resetPasswordButton";     //contains text: Reset Password 
	
	// xpath 
	// Error Messages
	private final String passwordValidationError_xpath    = "//div[@id='matPasswordError'][contains(.,'Password must be between 8 and 128 characters long, and must contain at least 3 of the following criteria: at least 1 uppercase, at least 1 lowercase, at least 1 digit, at least 1 special character.')]";
	private final String passwordMismatchError_xpath      = "//mat-error[@id='matConfirmPasswordError'][contains(.,'Passwords do not match')]";
	
	// Validation Criteria
	private final String passwordLengthCriteria_xpath      = "//li[contains(., 'between 8 and 128 characters')]";
	private final String passwordRequire3Criteria_xpath    = "//p[contains(@class, 'followCriteria')][contains(., 'Any 3 of the following criteria:')]";
	private final String passwordRequire1Uppercase_xpath   = "//li[contains(text(), 'At least 1 uppercase ')]";
	private final String passwordRequire1Lowercase_xpath   = "//li[contains(text(), 'At least 1 lowercase ')]";
	private final String passwordRequire1Digit_xpath       = "//li[contains(text(), 'At least 1 digit ')]";
	private final String passwordRequire1SpecialChar_xpath = "//li[contains(text(), 'At least 1 special character ')]";
	
	// Other
	private EventFiringWebDriver driver;
	private HelperMethods helper;
	
	public ResetPasswordModal(EventFiringWebDriver driver) {
		super(driver);
	    this.driver = driver;
	    helper      = new HelperMethods();
	}
	
	/* ----- getters ----- */
	public WebElement getHeaderLabel()                 { return findByVisibility(By.id(this.headerLabel_id));                      }
	public WebElement getRecoveryError()               { return findByVisibility(By.id(this.recoveryError_id));                    }
	public WebElement getPasswordLabel()               { return findByVisibility(By.id(this.passwordLabel_id));                    }
	public WebElement getPasswordInput()               { return findByVisibility(By.id(this.passwordInput_id));                    }
	public WebElement getPasswordError()               { return findByVisibility(By.id(this.passwordError_id));                    }
	public WebElement getConfirmPasswordLabel()        { return findByVisibility(By.id(this.confirmPasswordLabel_id));             }
	public WebElement getConfirmPasswordInput()        { return findByVisibility(By.id(this.confirmPasswordInput_id));             }
	public WebElement getConfirmPasswordError()        { return findByVisibility(By.id(this.confirmPasswordError_id));             }
	public WebElement getCancelButton()                { return findByClickability(By.id(this.cancelButton_id));                   }
	public WebElement getResetPasswordButton()         { return findByVisibility(By.id(this.resetPasswordButton_id));              }

	// Error Messages
	public WebElement getPasswordValidationError()     { return findByVisibility(By.xpath(this.passwordValidationError_xpath));    }
	public WebElement getPasswordMismatchError()       { return findByVisibility(By.xpath(this.passwordMismatchError_xpath));      }
	
	// Validation Criteria
	public WebElement getPasswordLengthCriteria()      { return findByVisibility(By.xpath(this.passwordLengthCriteria_xpath));     }
	public WebElement getPasswordRequire3Criteria()    { return findByVisibility(By.xpath(this.passwordRequire3Criteria_xpath));   }
	public WebElement getPasswordRequire1Uppercase()   { return findByVisibility(By.xpath(this.passwordRequire1Uppercase_xpath));  }
	public WebElement getPasswordRequire1Lowercase()   { return findByVisibility(By.xpath(this.passwordRequire1Lowercase_xpath));  }
	public WebElement getPasswordRequire1Digit()       { return findByVisibility(By.xpath(this.passwordRequire1Digit_xpath));      }
	public WebElement getPasswordRequire1SpecialChar() { return findByVisibility(By.xpath(this.passwordRequire1SpecialChar_xpath));}
	
	/* ----- helpers ----- */

	/***
	 * <h1>getValueOfPasswordInput</h1>
	 * @return String = value currently displayed in the Password field
	 */
	private String getValueOfPasswordInput() { return this.getPasswordInput().getAttribute("value"); }
	
	/***
	 * <h1>isValidationMessageHighlighted</h1>
	 * <p>purpose:  Determine if a validation criteria message is currently displayed as highlighted
	 * @param ele = WebElement for the validation message<br>
	 * 	Note: This must be located by an xpath defined to the level of the object with class = "success"
	 * @return true = message is highlighted | false = message is not highlighted
	 */
	private Boolean isValidationMessageHighlighted(WebElement ele) {
		return ele.getAttribute("class").contains("success");
	}

	/* ----- methods ----- */

	/***
	 * <h1>clickCancelButton</h1>
	 * <p>purpose: Click "Cancel" on the Reset Password dialog
	 * @return AccountSettings
	 */
	public AccountSettings clickCancelButton() {
		getCancelButton().click();
		return new AccountSettings(driver);
	}

	/***
	 * <h1>clickResetPasswordButton</h1>
	 * <p>purpose: Click "Reset Password"  on the Reset Password dialog<br>
	 * 	Update the static SupplierLogin.thisUser for the password component.<br>
	 * 	Note: password will be pulled from currently visible value in password<br>
	 * 	input field. <br>
	 * 	Note: Reset Password button should be enabled before running this method</p>
	 * @return AccountSettings
	 */
	public AccountSettings clickResetPasswordButton() {
		getResetPasswordButton().click();

		// Set the password into static thisUser 
		SupplierLogin.thisUser.setPassword(this.getValueOfPasswordInput());

		return new AccountSettings(driver);
	}
	
	/***
	 * <h1>setPasswordInput</h1> 
	 * <p>purpose: Set the "Password" input to text</p>
	 * @param password = text to enter into the "Password"
	 * @return ResetPasswordModal
	 */
	public ResetPasswordModal setPasswordInput(String password) {
		// Enter one char at a time to mimic typing
		for( int i = 0; i <  password.length(); i++) {
			char c = password.charAt(i);
			String s = new StringBuilder().append(c).toString();
			this.getPasswordInput().sendKeys(s); }

		// Allow validation to trigger and complete.
		// TODO: Update with wait for "Checking Password" rather than explicit wait
		helper.addSystemWait(1);  
		this.getPasswordInput().sendKeys(Keys.TAB);
		return this;
	}

	/***
	 * <h1>setConfirmPasswordInput</h1> 
	 * <p>purpose: Set the "Confirm Password" input to text</p>
	 * @param confirmPassword = text to enter into the "Confirm Password" input
	 * @return ResetPasswordModal
	 */
	public ResetPasswordModal setConfirmPasswordInput(String confirmPassword) {
		// Enter one char at a time to mimic typing
		for( int i = 0; i <  confirmPassword.length(); i++) {
			char c = confirmPassword.charAt(i);
			String s = new StringBuilder().append(c).toString();
			this.getConfirmPasswordInput().sendKeys(s); }

		// Allow validation to trigger and complete.
		// TODO: Update with wait for "Checking Password" rather than explicit wait
		helper.addSystemWait(1);
		this.getConfirmPasswordInput().sendKeys(Keys.TAB);
		return this;
	}
	
	/***
	 * <h1>clearConfirmPasswordInput</h1> 
	 * <p>purpose: Clear the "Confirm Password" input of text</p>
	 * @return ResetPasswordModal
	 */
	public ResetPasswordModal clearConfirmPasswordInput() {
		this.getConfirmPasswordInput().clear();
		this.setConfirmPasswordInput(""); // Send a blank space to re-trigger validation and clear out any previous validations
		int seconds = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, seconds)
			.withMessage("ERROR: After \"" + seconds + "\" seconds, the confirm password input field has not cleared")
			.pollingEvery(Duration.ofMillis(200))
			.until(ExpectedConditions.attributeToBe(By.id(this.confirmPasswordInput_id), "value", ""));
		return this;
	}

	/***
	 * <h1>clearPasswordInput</h1> 
	 * <p>purpose: Clear the "Password" input of text</p>
	 * @return ResetPasswordModal
	 */
	public ResetPasswordModal clearPasswordInput() {
		this.getPasswordInput().clear();
		this.setPasswordInput(""); // Send a blank space to re-trigger validation and clear out any previous validations
		int seconds = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, seconds)
			.withMessage("ERROR: After \"" + seconds + "\" seconds, the password input field has not cleared")
			.pollingEvery(Duration.ofMillis(200))
			.until(ExpectedConditions.attributeToBe(By.id(this.passwordInput_id), "value", ""));
		return this;
	}

	/***
	 * <h1>resetPassword</h1>
	 * <p>purpose: Enter the new password into the Reset Password modal.<br>
	 * 	 Click "Confirm Password" button. Wait for modal to hide <br>
	 * 	 Update the SupplierLogin.thisUser static for the password component</p>
	 * @param newPassword = new password
	 * @return AccountSettings
	 */
	public AccountSettings resetPassword(String newPassword) {
		this.setPasswordInput(newPassword)
				.setConfirmPasswordInput(newPassword)
				.clickResetPasswordButton();

		// Wait for the modal to close
		this.waitForInvisibility(By.id(this.passwordInput_id));

		return new AccountSettings(driver);
	}
	
	/* ----- verifications ----- */
	
	/***
	 * <h1>verifyPageInitialization</h1>
	 * <p>purpose: Verify that the "Reset Password" modal has correctly loaded</p>
	 * @return ResetPasswordModal
	 */
	public ResetPasswordModal verifyPageInitialization() {
		
		// Page Header
		this.getHeaderLabel();
		
		// Inputs and their labels
		this.getPasswordLabel();
		this.getPasswordInput();
		this.getConfirmPasswordLabel();
		this.getConfirmPasswordInput();
		
		// Bottom Buttons
		this.getCancelButton();
		this.getResetPasswordButton();
		
		return this;
	}
	
	/***
	 * <h1>verifyResetPasswordButtonIsDisabled</h1>
	 * <p>purpose: Verify that the "Reset Password" button is disabled to user click<br>	
	 * 	(This should happen when the user has not inputted password and confirm password that<br>
	 * 	meets validation, do not match each other, or are blanks). Fail test if the "Reset Password"<br>
	 * 	button is still enabled</p>
	 * @return ResetPasswordModal
	 */
	public ResetPasswordModal verifyResetPasswordButtonIsDisabled() {
		int seconds = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, seconds)
			.withMessage("Test Failed: After \"" + seconds + "\" seconds, the \"Reset Password\" button has not disabled")
			.until(ExpectedConditions.not(ExpectedConditions.elementToBeClickable(this.getResetPasswordButton())));
		return this;	
	}

	/***
	 * <h1>verifyPasswordMismatchErrorDoesNotDisplay</h1>
	 * <p>purpose: Verify that "Passwords do not match" message is not<br>
	 * 	displaying. Fail test if this message is displayed</p>
	 * @return ResetPasswordModal
	 */
	public ResetPasswordModal verifyPasswordMismatchErrorDoesNotDisplay() {
		waitForInvisibility(By.xpath(this.passwordMismatchError_xpath), 2);
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that password mismatch error is not displayed");
		return this;
	}
	
	/***
	 * <h1>verifyPasswordMismatchErrorDisplays</h1>
	 * <p>purpose: When enters password + confirm password that	do not match<br>
	 * 	and then clicks on "Reset Password", then an error message should<br>
	 * 	display to "Password do not match".  This method verifies that the<br>
	 * 	error message displays or fails the test if the error message does not display</p>
	 * @return ResetPasswordModal
	 */
	public ResetPasswordModal verifyPasswordMismatchErrorDisplays() {
		this.getPasswordMismatchError();
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that password mismatch error is displayed");
		return this;
	}

	/***
	 * <h1>verifyPasswordValidationErrorDoesNotDisplay</h1>
	 * <p>purpose: Verify that password validation error message is not<br>
	 * 	displaying. Fail test if this message is displayed</p>
	 * @return ResetPasswordModal
	 */
	public ResetPasswordModal verifyPasswordValidationErrorDoesNotDisplay() {
		waitForInvisibility(By.xpath(this.passwordValidationError_xpath), 2);
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that password validation error is not displayed");
		return this;
	}
	
	/***
	 * <h1>verifyPasswordValidationErrorDisplays</h1>
	 * <p>purpose: When enters password + confirm password that does not meet password validation<br>
	 * 	and then clicks on "Reset Password", then an error message should<br>
	 * 	displays about the bad password validation.  This method verifies that the<br>
	 * 	error message displays or fails the test if the error message does not display</p>
	 * @return ResetPasswordModal
	 */
	public ResetPasswordModal verifyPasswordValidationErrorDisplays() {
		this.getPasswordValidationError();
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that password validation error is displayed");
		return this;
	}
	
	/***
	 * <h1>verifyCorrrectPasswordCriteriaIsHighlighted</h1>
	 * <p>purpose: Password criteria should highlight depending on the value of the password.<br>	
	 * 	Depending on the value of the password (as currently displayed in the password input field),<br> 
	 *	verify that the following selections are properly highlighted:<br>
	 * 	1. between 8 and 128 characters<br>
	 * 	2. Any 3 of the following criteria:<br>
	 * 	3. At least 1 uppercase<br>
	 * 	4. At least 1 lowercase<br> 
	 *  5. At least 1 digit<br>
	 *  6. At least 1 special character<br>
	 *  Fail test if the proper values are not highlighted for the value of the password currently<br>
	 *  stored in the password input</p>
	 * @return
	 */
	public ResetPasswordModal verifyCorrectPasswordCriteriaIsHighlighted() {
		
		// Grab the String inputted into the password input field
		String password = this.getValueOfPasswordInput();
		int count       = 0;

		System.out.println(GlobalVariables.getTestID() + " INFO: For password = \"" + password + "\", password criteria validation is correctly highlighted for:");
		
		// Verify highlighted criteria is displayed per password
		// 1. between 8 and 128 chars long
		if (password.length() > 8 & password.length() < 128) { 
			String message = "between 8 and 128 chars long";
			Assert.assertTrue("Test Failed: message is not highlighted -\"" + message + "\"", this.isValidationMessageHighlighted(this.getPasswordLengthCriteria())); 
			System.out.println(GlobalVariables.getTestID() + " \t" + message);
		}

	  	// 2. At least 1 uppercase
		if (password.matches("(\\W*\\w*\\D*\\d*)([A-Z]{1})(\\W*\\w*\\D*\\d*)")) {
			String message = "At least 1 uppercase";
			Assert.assertTrue("Test Failed: message is not highlighted - \"" + message + "\"", this.isValidationMessageHighlighted(this.getPasswordRequire1Uppercase())); 
			count++; 
			System.out.println(GlobalVariables.getTestID() + " \t" + message);
		}

	  	// 3. At least 1 lowercase
		if (password.matches("(\\W*\\w*\\D*\\d*)([a-z]{1})(\\W*\\w*\\D*\\d*)")) {
			String message = "At least 1 lowercase";
			Assert.assertTrue("Test Failed: message is not highlighted - \""+ message + "\"", this.isValidationMessageHighlighted(this.getPasswordRequire1Lowercase())); 
			count++; 
			System.out.println(GlobalVariables.getTestID() + " \t" + message);
		}

	    // 4. At least 1 digit
		if (password.matches("(\\W*\\w*\\D*\\d*)([0-9]{1})(\\W*\\w*\\D*\\d*)")) {
			String message = "At least 1 digit";
			Assert.assertTrue("Test Failed: message is not highlighted - \"" + message + "\"", this.isValidationMessageHighlighted(this.getPasswordRequire1Digit())); 
			count++; 
			System.out.println(GlobalVariables.getTestID() + " \t" + message);
		}

	    // 5. At least 1 special character
		if (password.matches("(\\W*\\w*\\D*\\d*)(\\W{1})(\\W*\\w*\\D*\\d*)")) {
			String message = "At least 1 special character";
			Assert.assertTrue("Test Failed: message is not highlighted - \"" + message + "\"", this.isValidationMessageHighlighted(this.getPasswordRequire1SpecialChar())); 
			count++; 
			System.out.println(GlobalVariables.getTestID() + " \t" + message);
		}

		// 6. Any 3 of the following criteria
		if (count >= 3) {
			String message = "Any 3 of the following criteria";
			Assert.assertTrue("Test Failed: message is not highlighted - \"" + message + "\"", this.isValidationMessageHighlighted(this.getPasswordRequire3Criteria())); 
			System.out.println(GlobalVariables.getTestID() + " \t" + message);
		}

		return this;
	}
	
}
