@must @BuySpeed_Integration @administration @level2
Feature: Integrations > Buyspeed - BuySpeed to ARGO Registration
	This feature file exercises steps required to link BuySpeed users to ARGO
  
Scenario: BuySpeed user links to new BidSync
	Given I have logged into BuySpeed as seller "Generic BuySpeed Vendor"
	When I click on the Register With BidSync button on the BuySpeed Seller Home Screen
	Then I will see the Register With BidSync popup