@must @filterResults @search @bidSearch_detail @level4
Feature: Dashboard - Filter Results Section - Search Tests, Part 1
  This feature file exercises the objects related to the search ability of the Filter Results collapsible section on the Supplier Dashboard
  	ARGO-1233: Filter bids


@ARGO-1233
Scenario Outline: If user searches via Filter Results:
  1. Tab switches to All Bids and highlights
  2. Results display in All Bids based on search
  
  Given I have logged into the Supplier side as user "Basic"
  When I run a search on the Filter Results section with filters:
      | Filter Name       | Bogus Name         |
      | Keywords          | <+Keyword1>        | 
      | Negative Keywords | <-Keyword1>        |
      | States/Provinces  | <states/provinces> |
      | Time Frame        | <timeFrame>        |
      | Show Bids From    | <startDate>        |
      | Show Bids to      | <endDate>          |
  Then I will verify that All Bids tab is both highlighted and selected, and also displays correct results per selected filters: 
      | Filter Name       | Bogus Name         |
      | Keywords          | <+Keyword1>        |
      | Negative Keywords | <-Keyword1>        |
      | States/Provinces  | <states/provinces> |
      | Time Frame        | <timeFrame>        |
      | Show Bids From    | <startDate>        |
      | Show Bids to      | <endDate>          |

  Examples:
      | +Keyword1| -Keyword1| states/provinces  | timeFrame         | startDate  | endDate    |
      | Road     | Redar    | Texas             | Past 6 Months     |            |            |
      | Road     | Redar    | Texas             | Past Year         |            |            |
      | Road     | Redar    | Texas             | Past 3 Years      |            |            |
      | Road     | Redar    | Texas             | Custom Time Frame | 03/22/2018 | 12/20/2019 |