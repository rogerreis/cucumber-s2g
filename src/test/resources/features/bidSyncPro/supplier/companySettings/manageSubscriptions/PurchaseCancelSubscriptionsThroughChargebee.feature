@must @subscriptions @level2
Feature: Company Settings > Manage Subscriptions - Purchase and Cancel Subscriptions through Chargebee test site
BIDSYNC-408: "Vendors unable to access Pro Subscription Bids"
ARGO-1398: "When Pro Plans are Cancelled or Expire, Need to Create Basic Plan"


@ARGO-1398 @BIDSYNC-408
Scenario Outline: If user subscription is cancelled in Chargebee, then Basic account is created
	Given I have logged into the Chargebee test site as the admin user
	When I create a user "<ChargebeeSubscription>" subscription in the Chargebee test site for my user account "UserForChargebeeCancelSubscriptionsTests"
	Then I will verify that the "<ProSubscription>" subscription is updated in BidSync Pro
	When I cancel the user subscription in the Chargebee test site for my user account
	Then I will verify that the user subscription is cancelled and that only a BidSync Basic subscription is created in BidSync Pro
	And I will verify that my user only has a "BidSync Basic" subscription plan listed in the ARGO database

	Examples:
	| ProSubscription            |ChargebeeSubscription			   |
	| BidSync National - Yearly  |BidSync National - Yearly Online |
	| BidSync National - Monthly |BidSync National - Monthly Online|
	| BidSync National - 6 Month |BidSync National - 6 Month Online|
	| BidSync Regional - Yearly  |BidSync Regional - Yearly Online |
	| BidSync Regional - Monthly |BidSync Regional - Monthly Online|
	| BidSync Regional - 6 Month |BidSync Regional - 6 Month Online|
	| BidSync State - Yearly     |BidSync State - Yearly Online    |
	| BidSync State - Monthly    |BidSync State - Monthly Online   |
	| BidSync State - 6 Month    |BidSync State - 6 Month Online   |
 