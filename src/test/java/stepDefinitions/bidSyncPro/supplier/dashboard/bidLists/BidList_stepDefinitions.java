package stepDefinitions.bidSyncPro.supplier.dashboard.bidLists;

import com.google.common.collect.Lists;
import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import data.bidSyncPro.registration.RandomChargebeeUser;
import data.salesforceIntegration.Click;
import pages.bidSyncPro.supplier.account.bidProfile.SalesTerritories;
import pages.bidSyncPro.supplier.common.AccountDropDown;
import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.bidSyncPro.supplier.dashboard.BidListTabNames;
import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidCriteria;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidListRow;
import pages.bidSyncPro.supplier.dashboard.bids.BidDetails;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.IterableUtils;
import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class BidList_stepDefinitions {
    
    private EventFiringWebDriver driver;
    private BidList bidList;
    private HelperMethods helperMethods;
    
    private List<String> persistedBidIds = new ArrayList<>(); // used to keep state between step definitions
    
    public BidList_stepDefinitions(CoreAutomation automation) {
        this.driver     = automation.getDriver();
        this.bidList  = new BidList(driver);
        this.helperMethods = new HelperMethods();
    }

    // for new for you and saved bids tab
	@When ("^I view the last bid currently displaying in the bid list in \"([^\"]*)\" bid tab$")
	public void I_view_the_last_bid_currently_displaying_in_the_bid_list(String tab) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Viewing last bid currently visible in bid list");
		if (tab.equalsIgnoreCase("New For You")) {
            bidList.getLastVisibleBidInNewForYouTab();
        }
		else
        {
            bidList.getLastVisibleBid();
        }
	}

	@When ("^I click on load more, if it is available$")
	public void I_click_on_load_more_if_it_is_available(){
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on \"Load More\" button, if available");
		bidList.loadMoreBidsIfAvailable();
	}

    @Then("^I will verify that the List columns are ([^\"]*)$")
    public void iWillVerifyThatTheList(String columns) {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify List columns are correct");
        List<WebElement> headerElements = bidList.columnHeaders();
        
        List<String> actualHeaders = new ArrayList<>(Lists.transform(headerElements, header -> header.getText()));
        actualHeaders.replaceAll(s -> s.replace("Sorted Descending", "").trim());
        actualHeaders = actualHeaders.stream().filter(x -> !x.isEmpty()).collect(Collectors.toList());
        
        List<String> expectedHeaders = Arrays.asList(columns.split(", "));
        System.out.printf(GlobalVariables.getTestID() + " INFO: Expected column headers: %s\n", expectedHeaders);
        System.out.printf(GlobalVariables.getTestID() + " INFO: Actual column headers: %s\n", actualHeaders);
        String message = "ERROR: Expected headers and actual headers are not equal.";
        assertTrue(message, CollectionUtils.isEqualCollection(expectedHeaders, actualHeaders));
    }
    
	@Then("^I verify that the List columns are displayed")
	public void iVerifyColumnsDisplayed() {
		try {
			bidList.verifyColumnHeadersLoaded();
		}
		catch(Exception e){
			Assert.fail(
					"Test Failed: iVerifyColumnsDisplayed() :\n" + e);		
		}
		
	}
    
	@Then("^I verify the List columns are displayed in your saved bids")
	public void iVerifyColumnsDisplayedinYourSavedBids() {
		try {
			bidList.verifyYourSavedBidsColumnHeadersLoaded();
		}
		catch(Exception e){
			Assert.fail(
					"Test Failed: iVerifyColumnsDisplayed() :\n" + e);		
		}
		
	}
	
    @When("^I select (\\d+) bids by clicking their stars$")
    public void iSelectBidsByClickingTheStar(int numBids) {
        // This function breaks if you try to select more bids than appear on the screen
        System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Select %d bids by clicking their stars\n", numBids);
        int numDisplayedBids = bidList.getNumberOfCurrentlyDisplayedBids();
        Assert.assertTrue("ERROR: Not enough bids in list (only \"" + numDisplayedBids + "\") to select \"" + numBids + "\" bids", numBids <= numDisplayedBids);

        List<BidListRow> bids = bidList.firstNumberOfVisibleBidsForNewForYouBids(numBids);
        persistedBidIds = new ArrayList<>(Lists.transform(bids, bid -> bid.bidInfo.getBidId()));
        IterableUtils.forEach(IterableUtils.reversedIterable(bids), 
        	bid -> {
        		int i = 1;
        		if(!bid.isFavorited()) {
        			bid.clickStar();}
        	});
        helperMethods.addSystemWait(1);
    }
    
    
    @When("^I deselect (\\d+) bids by clicking their stars$")
    public void iDeSelectBidsByClickingTheStar(int numBids) {
        // This function breaks if you try to select more bids than appear on the screen
        System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: DeSelect %d bids by clicking their stars\n", numBids);
        int numDisplayedBids = bidList.getNumberOfCurrentlyDisplayedBids();
        Assert.assertTrue("ERROR: Not enough bids in list (only \"" + numDisplayedBids + "\") to select \"" + numBids + "\" bids", numBids <= numDisplayedBids);

        List<BidListRow> bids = bidList.firstNumberOfVisibleBids(numBids);
        persistedBidIds = new ArrayList<>(Lists.transform(bids, bid -> bid.bidInfo.getBidId()));
        IterableUtils.forEach(IterableUtils.reversedIterable(bids), 
        	bid -> {
        		int i = 1;
        		if(bid.isFavorited()) {
        			bid.clickStar();}
        	});
        helperMethods.addSystemWait(1);
    }
    
    @Then("^those bids stars will show as checked$")
    public void thoseBidsStarsWillShowAsChecked() {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify bids stars are checked");
        List<BidListRow> bids = bidList.allVisibleBids();
        List<BidListRow> selectedBids = (List<BidListRow>) CollectionUtils.select(bids, bid -> persistedBidIds.contains(bid.bidInfo.getBidId()));
        List<BidListRow> favoritedBids = (List<BidListRow>) CollectionUtils.select(bids, bid -> bid.isFavorited());
        String message = "Selected bids do not equal favorited bids.";
        assertTrue(message, CollectionUtils.isEqualCollection(selectedBids, favoritedBids));
    }
    
    @And ("^those saved bids are displayed in the Your Saved Bids list$")
    public void those_saved_bids_are_displayed_in_the_Your_Saved_Bids_list() {
    	try {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying saved bids are displayed in \"Your Saved Bids\" list");
        List <String> ids = new ArrayList<String>();
        
        // dmidura: Workaround for BIDSYNC 546. Remove when fixed.
        bidList.waitForBidsInYourSavedBids(persistedBidIds);

        // persistedBidIds are for bids that were saved out
        // ids are for the persistedBidIds that are currently displayed in the "Your Saved Bids" list
        bidList.allVisibleBids().stream()
        	.filter(bid->persistedBidIds.contains(bid.bidInfo.getBidId()))
        	.forEach(bid->ids.add(bid.bidInfo.getBidId()));

            // verify that all the persistedBidIds are currently displayed in "Your Saved Bids" list
        assertTrue("ERROR: Not all saved bids are displaying in the \"Your Saved Bids\" list", ids.containsAll(persistedBidIds));
    }catch(Exception exp){
		fail("TEST STEP FAILED : thoseBidsStarsWillShowAsUnchecked()" + exp);
	}
    }

    @Then("^those bids stars will show as unchecked$")
    public void thoseBidsStarsWillShowAsUnchecked() {
    	try {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify bids stars are unchecked");
        List<BidListRow> bids = bidList.allVisibleBids();
        List<BidListRow> selectedBids = (List<BidListRow>) CollectionUtils.select(bids, bid -> persistedBidIds.contains(bid.bidInfo.getBidId()));
        List<BidListRow> favoritedBids = (List<BidListRow>) CollectionUtils.select(bids, bid -> bid.isFavorited());
        String message = "Some or all of the selected bids are favorited.";
        assertFalse(message, CollectionUtils.containsAny(selectedBids, favoritedBids));
    	}catch(Exception exp){
    		fail("TEST STEP FAILED : thoseBidsStarsWillShowAsUnchecked()" + exp);
    	}
    }
    
    @Then("^those bids will be shown in the bid List$")
    public void thoseBidsWillBeShownInTheBidList() {
    	try {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify bids are shown in the bid List");
        List<String> visibleBidIds = new ArrayList<>(Lists.transform(bidList.allVisibleBids(), bid -> bid.bidInfo.getBidId()));
        String message = "Not all of the selected bids are visible in the bid List.";
        assertTrue(message, CollectionUtils.containsAll(visibleBidIds, persistedBidIds));
    	}catch(Exception exp){
    		fail("TEST STEP FAILED : thoseBidsWillBeShownInTheBidList()" + exp);
    	}
    }
    
    @Then("^those bids will not be shown in the bid List$")
    public void thoseBidsWillNotBeShownInTheBidList() {
    	try {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify bids are not shown in the bid List");
        List<String> visibleBidIds = new ArrayList<>(Lists.transform(bidList.allVisibleBids(), bid -> bid.bidInfo.getBidId()));
        String message = "At least one of the selected bids are visible in the bid List.";
        assertTrue(message, CollectionUtils.containsAny(visibleBidIds, persistedBidIds));
    	}catch(Exception exp){
    		fail("TEST STEP FAILED : thoseBidsWillNotBeShownInTheBidList()" + exp);
    	}
    }
    
    @And("^I have saved (\\d+) bids from the \"([^\"]*)\" tab$")
    public void iHaveSavedBidsFromTheTab(int numBids, String tabName) {
        System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Save %d bids from the '%s' tab\n", numBids, tabName);
        new DashboardCommon(driver)
        	.openBidTab(BidListTabNames.fromString(tabName))
        	.verifyTabIsHighlightedAndSelected(BidListTabNames.fromString(tabName));
        
		// Workaround to display New For You bids by enabling to more states
        if (bidList.getNumberOfCurrentlyDisplayedBids() < numBids) {
			new CommonTopBar(driver)
				.waitForPageLoad()
				.openAccountDropDown();
			((JavascriptExecutor) driver).executeScript("arguments[0].click();",
					new AccountDropDown(driver)
						.clickBidProfile());

			new SalesTerritories(driver)
				.verifyPageElements()
				.clickOnEditSalesTerritories()
				.verifyRegionPageElements()
				.clickOnUSSalesTerritories()
				.clickOnSaveButton();
			helperMethods.addSystemWait(2);
			new BidDetails(driver).goBackToBids();
		}
        
        this.iSelectBidsByClickingTheStar(numBids);
    }
    
    @When("^I sort by \"([^\"]*)\"$")
    public void iSortBy(String column) {
        System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: sort by %s\n", column);
        bidList.sortByColumn(column);
    }
    
    @Then("^the List is sorted in ([^\"]*) order by \"([^\"]*)\"$")
    public void theListIsSortedInOrderBy(String sortOrder, String column) {
        System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: verify the List is sorted by %s in %s order.\n", column, sortOrder);
        String message = String.format("Bid List is not sorted by %s in %s order.\n", column, sortOrder);
        assertTrue(message, bidList.isSortedBy(column, sortOrder));
    }

    @Then("^the List is sorted in ([^\"]*) order by \"([^\"]*)\" in New for you Bid tab$")
    public void NewTAbBidtheListIsSortedInOrderBy(String sortOrder, String column) {
        System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: verify the List is sorted by %s in %s order.\n", column, sortOrder);
        String message = String.format("Bid List is not sorted by %s in %s order.\n", column, sortOrder);
        assertTrue(message, bidList.isSortedByInNewForYouTab(column, sortOrder));
    }

    @And("^I will unsave all bids$")
    public void iWillUnsaveAllBids() {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: unsave all saved bids.");
//    #Fix for release OCT 7
        List<BidListRow> bids = bidList.allVisibleBids();
        List<BidListRow> favoritedBids = (List<BidListRow>) CollectionUtils.select(bids, bid -> bid.isFavorited());
        IterableUtils.forEach(IterableUtils.reversedIterable(favoritedBids), bid -> bid.clickStar());
    }

    //    #Fix for release OCT 7
    @And("^I unsave all bids$")
    public void iUnsaveAllBids() {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: unsave all saved bids.");
        List<BidListRow> bids = bidList.allVisibleBids();
        List<BidListRow> favoritedBids = (List<BidListRow>) CollectionUtils.select(bids, bid -> bid.isFavorited());
        IterableUtils.forEach(IterableUtils.reversedIterable(favoritedBids), bid -> bid.clickStarTemp());
    }
    
    @When("^I load more bids$")
    public void iLoadMoreBids() {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: load more bids.");
        bidList.loadMoreBids();
    }
    
    @Then("^I will verify that the List can load more bids$")
    public void iWillVerifyThatTheListCanLoadMoreBids() {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: verify the List can load more bids");
        int numberOldVisibleBids = bidList.getNumberOfCurrentlyDisplayedBids();
        bidList.loadMoreBids();
        int numberNewVisibleBids = bidList.getNumberOfCurrentlyDisplayedBids();
        System.out.printf(GlobalVariables.getTestID() + " INFO: Visible bids before loading more: %d\n", numberOldVisibleBids);
        System.out.printf(GlobalVariables.getTestID() + " INFO: Visible bids after loading more: %d\n", numberNewVisibleBids);
        String message = "ERROR: Clicking the Load More Bids button does not result in more bids shown.";
        assertTrue(message, numberNewVisibleBids > numberOldVisibleBids);
    }
    
    @Then("^all bids in the table are from the last (\\d+) days$")
    public void allBidsInTheTableAreFromTheLastDays(int numDays) {
        System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: verify all bids in the table are from the last %d days.\n", numDays);
        //#Fix for release OCT 7 - Only for new for you Bid  Tab
        List<BidListRow> allBids = bidList.allVisibleBidsInNewForYouTab();
        final int acceptableHours = 24 * numDays;
        String message = "At least one bid in the New For You tab is from over ten days ago.";
        assertTrue(message, IterableUtils.matchesAll(allBids, bid -> bid.bidInfo.getAddedDateInHours() <= acceptableHours));
    }
    
    @Then("^no bids in the table are expired$")
    public void noBidsInTheTableAreExpired() {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: verify no bids in the table are expired.");
        //#Fix for release OCT 7 - Only for new for you Bid  Tab
        List<BidListRow> allBids = bidList.allVisibleBidsInNewForYouTab();
        String message = "At least one bid is expired.";
        assertTrue(message, IterableUtils.matchesAll(allBids, bid -> !bid.bidInfo.isExpired()));
    }
    
    @Then("I verify whether the table displays expired bid")
    public void tableShouldNotDisplayExpiredBid() {
    	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: I verify whether the table displays expired bid");
		bidList.verifyBidExpired();   	
    }
    
    @Then("^the table is sorted first by \"([^\"]*)\" in \"([^\"]*)\" order, second by \"([^\"]*)\" in \"([^\"]*)\" order$")
    public void theTableIsSortedFirstByInOrderSecondByInOrder(String column1, String sortOrder1, String column2, String sortOrder2) {
        System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: verify the bid table is sorted first by '%s' and second by '%s'.\n", column1, column2);
        String result = bidList.isSortedBy(column1, sortOrder1, column2, sortOrder2);
        if (result.equals("primary failed")) {
            Assert.fail(String.format("Bid list is not sorted primarily by '%s' in '%s' order", column1, sortOrder1));
        } else if (result.equals("secondary failed")) {
            Assert.fail(String.format("Bid list is not sorted secondarily by '%s' in '%s' order", column2, sortOrder2));
        } else if (!result.equals("sorted correctly")) {
            Assert.fail(String.format("Invalid result: '%s'", result));
        }
    }

    @When("^I view details of (\\d+) bids from the \"([^\"]*)\" tab$")
    public void iViewDetailsOfBidsFromTheTab(int numBids, String tabName) {
        System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: View details of %d bids from the %s tab", numBids, tabName);
        new DashboardCommon(driver)
        	.openBidTab(BidListTabNames.fromString(tabName))
        	.verifyTabIsHighlightedAndSelected(BidListTabNames.fromString(tabName));

        List<BidListRow> bids = bidList.allVisibleBids().subList(0, numBids);
        if(!GlobalVariables.containsObject("clickLog")) {
            GlobalVariables.storeObject("clickLog", new ArrayList<Click>());
        }
        persistedBidIds = new ArrayList<>(Lists.transform(bids, bid -> bid.bidInfo.getBidId()));
        RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
        IterableUtils.forEach(bids, bid -> {
            Click click = new Click(user.getId(), bid.bidInfo.getBidId(), tabName);
            GlobalVariables.<ArrayList<Click>>getStoredObject("clickLog").add(click);
            bid.viewDetails().goBackToBids();
        });

    }

    @Then("^the bid list should have a bid with \"([^\"]*)\" in the title$")
    public void theBidListShouldHaveABidWithInTheTitle(String keyword) {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that bid list contains bid with \"" + keyword + "\" in the title");
        List<BidListRow> bids = bidList.allVisibleBids();
        List<BidListRow> bidsWithKeyword = (List<BidListRow>) CollectionUtils.select(bids, bid ->
                bid.bidInfo.getBidTitle().toLowerCase().contains(keyword.toLowerCase())
        );
        String message = String.format("No bids found containing '%s' in the title", keyword);
        Assert.assertTrue(message, bidsWithKeyword.size() > 0);

        System.out.println(GlobalVariables.getTestID() + " Test Passed: bid list contains bid with \"" + keyword + "\" in the title");
    }

    @When("^I randomly select a bid from the currently displayed bid list$")
    public void I_randomly_select_a_bid_from_the_currently_displayed_bid_list() {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on a random bid in the currently displayed bid list");

        BidListRow bid = new BidList(driver).getRandomBidInVisibleList();
        bid.bidInfo.printBidInfo();
        bid.viewDetails();
    }

// Fix for release OCT 7
    @When("^I randomly select a bid from the currently displayed bid list from All BIDS tab$")
    public void I_randomly_select_a_bid_from_the_currently_displayed_bid_list_All_Bids_tab() {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on a random bid in the currently displayed bid list in All bids");

        BidListRow bid = new BidList(driver).getRandomBidInVisibleListInAllBidsTab();
        bid.bidInfo.printBidInfo();
        bid.viewDetails();
    }

    @When("^I click on the Load More button in the All Bids list$")
    public void I_click_on_the_Load_More_button_in_the_All_Bids_list() {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on the \"Load More\" button on the All Bids tab");
        BidList allBids = new BidList(driver);
        allBids.loadMoreBids();
    }

	@When ("^I am viewing all the bids on the \"(.*)\" tab$")
    public void I_am_viewing_all_the_bids_on_the_bid_tab(String strBidTabName) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Viewing all bids");
		new DashboardCommon(driver).verifyTabIsHighlightedAndSelected(BidListTabNames.fromString(strBidTabName));
		new BidList(driver).paginateThroughAllBids();
	}
	
	@Then ("^I will not see a load more bids button$")
	public void I_will_not_see_a_load_more_bids_button() {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that \"Load More\" button is not visible");
        BidList displayedBids = new BidList(driver);
        displayedBids.verifyLoadMoreBidsDoesNotDisplay();
	}
    
    @Then("^I will verify that the bids in the list have the correct dates$")
    public void iWillVerifyThatTheBidsInTheListHaveTheCorrectDates() {
        List<BidListRow> bids = new BidList(driver).allVisibleBids();
        SoftAssertions softly = new SoftAssertions();
        for(BidListRow bid : bids) {
        	
        	// dmidura: If a bid is in "Upgrade Plan" mode, then we can't retrieve its 
        	// date from the database b/c we've obscured the solicitation_central_id that 
        	// we use to lookup the date
        	if (!bid.isUpgradePlan()) {
				Calendar actualEndDate   = bid.bidInfo.getEndDateAsCalendar();
				Calendar expectedEndDate = bid.bidInfo.getPreciseEndDate();
				
				String actualEndDateStr = String.format("%s-%s-%s",
						actualEndDate.get(Calendar.YEAR),
						actualEndDate.get(Calendar.MONTH),
						actualEndDate.get(Calendar.DAY_OF_MONTH));
				String expectedEndDateStr = String.format("%s-%s-%s",
						expectedEndDate.get(Calendar.YEAR),
						expectedEndDate.get(Calendar.MONTH),
						expectedEndDate.get(Calendar.DAY_OF_MONTH));
				softly.assertThat(actualEndDateStr)
						.as(String.format("End Date for bid '%s'", bid.bidInfo.getBidId()))
						.isEqualTo(expectedEndDateStr);
        	}
        }
        softly.assertAll();
    }
    
    @And("^I view the details of the first bid in the list$")
    public void iViewTheDetailsOfTheFirstBidInTheList() {
    	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Viewing details of first bid my user has perms to view in bid list");

    	// Grab the first bid in the list that the user has
    	// permissions to view
    	BidList bidList = new BidList(driver);
        for (int i=0; i < bidList.getNumberOfCurrentlyDisplayedBids(); i++ ) {
        	BidListRow bid = bidList.getBidByIndex(i);
        	if (bid.isUpgradePlan()) {
        		continue;
        	} else {
        		GlobalVariables.storeObject("StoredBid", bid);
        		bid.bidInfo.printBidInfo();
        		bid.viewDetails();
        		break;
        	}
        }
    }
    
    @And ("^I randomly select a bid with spaces in the bid number from the \"(.*)\" bid list$")
    public void I_randomly_select_a_bid_with_spaces_in_the_bid_number_from_the_selected_bid_list(String bidList) {
    	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Selecting bid with spaces in bid number from \"" + bidList +"\" bid list");

    	// Randomly grab a bid from one of the bid lists
    	BidListRow randomBid = 
    	new DashboardCommon(driver)
    		.openBidTab(BidListTabNames.fromString(bidList))
    		.verifyTabIsHighlightedAndSelected(BidListTabNames.fromString(bidList))
    		.allVisibleBids().stream().filter(bid->bid.bidInfo.getBidNumber().trim().contains(" "))
    		.findAny().orElseThrow(IllegalStateException::new);
    	
    	GlobalVariables.storeObject("randomBid", randomBid);
    }
    
    @And ("^I randomly select a bid with dashes in the bid number from the \"(.*)\" bid list$")
    public void I_randomly_select_a_bid_with_dashes_in_the_bid_number_from_the_selected_bid_list(String bidList) {
    	try{
    		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Selecting bid with dashes in bid number from \"" + bidList +"\" bid list");
    	

    	// Randomly grab a bid from one of the bid lists
    	BidListRow randomBid = 
    	new DashboardCommon(driver)
    		.openBidTab(BidListTabNames.fromString(bidList))
    		.verifyTabIsHighlightedAndSelected(BidListTabNames.fromString(bidList))
    		.allVisibleBids().stream().filter(bid->bid.bidInfo.getBidNumber().trim().contains("-"))
    		.findAny().orElseThrow(IllegalStateException::new);
    	
    	GlobalVariables.storeObject("randomBid", randomBid);
    	randomBid.bidInfo.printBidInfo();
    	}catch(Exception exp) {
    		fail("TEST FAILED: Selecting bid with dashes in bid number from \\\"\" + bidList +\"\\\" bid list" + exp);
    	}
	}
    
    @Then ("^I will verify that the correct bid is returned per the search for the bid number$")
    public void I_will_verify_that_the_correct_bid_is_returned_per_the_search_for_the_bid_number() {
    	BidListRow expectedBid = GlobalVariables.getStoredObject("randomBid");
    	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that bid with bid number \"" + expectedBid.bidInfo.getBidNumber() + "\" was returned from search");
    	
    	// Only one bid should have returned from the search
    	BidListRow returnedBid = 
    	new BidList(driver)
    		//.verifyMaximumExpectedNumberOfBidResults(1)   // dmidura: per discussion with dev, current logic could allow > 1 bid return.
    		.allVisibleBids().stream()                      //          Uncomment if search term logic becomes more specific.
    		.findFirst().orElseThrow(IllegalStateException::new);
    	
    	// Verify that it is our expected bid
        //    #Fix for release OCT 7
    	Assert.assertTrue("Test Failed: Incorrect bid was returned from a search result for bid number = \"" + expectedBid.bidInfo.getBidNumber() + "\"", 
    			expectedBid.bidInfo.getBidTitle().equals(returnedBid.bidInfo.getBidTitle()));
        Assert.assertTrue("Test Failed: Incorrect bid was returned from a search result for bid number = \"" + expectedBid.bidInfo.getBidNumber() + "\"",
                expectedBid.bidInfo.getBidNumber().equals(returnedBid.bidInfo.getBidNumber()));

    	System.out.println(GlobalVariables.getTestID() + " Test Passed: Searching by bid number returns correct bid");
    }

    //    #Fix for release OCT 7
    // Whenever searching keyword, which displays more than one bids (>1) as a result,
    // if expected one bid then following function will find that one among all (get bid details by bid title)
    @Then ("^I verify that the correct bid is returned per the search for the bid number$")
    public void I_verify_that_the_correct_bid_is_returned_per_the_search_for_the_bid_number() {
        BidListRow expectedBid = GlobalVariables.getStoredObject("randomBid");
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that bid with bid number \"" + expectedBid.bidInfo.getBidNumber() + "\" was returned from search");

        // Only one bid should have returned from the search
        BidListRow returnedBid =
                new BidList(driver)
                        //.verifyMaximumExpectedNumberOfBidResults(1)   // dmidura: per discussion with dev, current logic could allow > 1 bid return.
                        .allVisibleBids().stream().filter(bid->bid.bidInfo.getBidTitle().trim().contains(expectedBid.bidInfo.getBidTitle()))
                .findAny().orElseThrow(IllegalStateException::new);                 //          Uncomment if search term logic becomes more specific

        // Verify that it is our expected bid
        //    #Fix for release OCT 7
        Assert.assertTrue("Test Failed: Incorrect bid was returned from a search result for bid number = \"" + expectedBid.bidInfo.getBidNumber() + "\"",
                expectedBid.bidInfo.getBidTitle().equals(returnedBid.bidInfo.getBidTitle()));
        Assert.assertTrue("Test Failed: Incorrect bid was returned from a search result for bid number = \"" + expectedBid.bidInfo.getBidNumber() + "\"",
                expectedBid.bidInfo.getBidNumber().equals(returnedBid.bidInfo.getBidNumber()));

        System.out.println(GlobalVariables.getTestID() + " Test Passed: Searching by bid number returns correct bid");
    }

    
    @Then ("^I will verify that the following bid is displayed in the bid list:$")
	public void I_will_verify_that_the_following_bid_is_returned(DataTable dTable) {
    	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that expected bid is displayed in bid list");

    	BidCriteria expectedBid = new BidCriteria(dTable);

    	new BidList(driver)
    		.allVisibleBids().stream()
    		.filter(bid->bid.bidInfo.equals(expectedBid))
    		.findFirst().orElseThrow(IllegalStateException::new);

    	System.out.println(GlobalVariables.getTestID() + " Test Passed: Expected bid is displayed in bid list");
	}


}
