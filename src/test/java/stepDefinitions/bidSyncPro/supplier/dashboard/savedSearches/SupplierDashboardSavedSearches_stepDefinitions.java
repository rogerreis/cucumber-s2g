package stepDefinitions.bidSyncPro.supplier.dashboard.savedSearches;

import core.CoreAutomation;

import static org.junit.Assert.fail;

import java.sql.SQLException;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import pages.bidSyncPro.supplier.dashboard.BidListTabNames;
import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterCriteria;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterResultsSavePopupNewClass;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterResultsSectionNewClass;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.TimeFrameOptions;
import pages.common.helpers.DateHelpers;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.RandomHelpers;


public class SupplierDashboardSavedSearches_stepDefinitions {

	private EventFiringWebDriver driver;
	private DateHelpers dateHelpers;
	private RandomHelpers randomHelpers;
	
	public SupplierDashboardSavedSearches_stepDefinitions(CoreAutomation automation) {
		this.driver     = automation.getDriver();
		dateHelpers     = new DateHelpers();
		randomHelpers   = new RandomHelpers();
	}
	
	/* ----- Helpers ----- */
	
	/***
	 * <h1>createSavedSearchNameFromTimeFrame</h1>
	 * <p>purpose: Helper to generate a unique Saved Search name based on<br>
	 * 	date and selected TimeFrameOption</p>
	 * @param timeFrame = TimeFrameOptions
	 * @return String with a unique Saved Search name
	 */
	private String createSavedSearchNameFromTimeFrame(TimeFrameOptions timeFrame) {
		// Create a filter with the specified time frame
		// name = <timeFrame_abbrev>_<today's date as MMDD>
		// ex: P6M_0821
		String abbrev = "";
		switch (timeFrame) {
			case PAST_6_MONTHS:
				abbrev = "P6M";
				break;
			case PAST_YEAR:
				abbrev = "PY";
				break;
			case PAST_3_YEARS:
				abbrev = "P3Y";
				break;
			case CUSTOM_TIME_FRAME:
				abbrev = "CTF";
				break;
			default:
				fail("ERROR: \"" + timeFrame + "\" is an invalid input"); }
		
		//String date = dateHelpers.getCalendarDateAsString(dateHelpers.getCurrentTimeAsCalendar()).substring(0, 5).replace("/", "");
		String filterName = abbrev + "_" + randomHelpers.getRandomIntStringOfLength(6);

		System.out.println(GlobalVariables.getTestID() + " INFO: Created filter name \"" + filterName + "\"");
		return filterName;
	}
	
	/**
	 * <h1>enterFilterClickSave</h1>
	 * <p>purpose: Assuming Filter Results section is open, click "Clear All".
	 * 	Set Filter Results per filter and then click "Save". Filter save popup will appear.
	 * @param filter = FilterCriteria to set
	 * @return FilterResultsSavePopupNewClass.
	 */
	private FilterResultsSavePopupNewClass enterFilterClickSave(FilterCriteria filter) {
		return new FilterResultsSectionNewClass(driver)
				.waitForFilterResultsSectionToLoad()
				.closeKeywordsSection()
				.clickClearAllBtn()
				.setFilter(filter)
				.clickSaveSearchBtn();
	}
	
	/**
	 * <h1>saveSearch</h1>
	 * <p>purpose: Assuming Filter Results section is open, click "Clear All".
	 * 	Set Filter Results per filter and then click "Save". Filter save popup will appear.
	 * 	On popup, enter filter name and click OK.
	 * @param filter = FilterCriteria to set
	 */
	private void saveSearch(FilterCriteria filter) {
		this.enterFilterClickSave(filter)
			.saveSearch(filter.getFilterName());
	}
	
	/***
	 * <h1>createGenericFilterWithRandomName</h1>
	 * <p>purpose: Create a generic FilterCriteria with a random name</p>
	 * @return FilterCriteria
	 */
	private FilterCriteria createGenericFilterWithRandomFilter() {
		// Create a filter with a random name
		FilterCriteria filter = new FilterCriteria();
		return filter.setGenericFilter()
				.setRandomFilterName();
	}
	
	/***
	 * <h1>createFilterFromSpecifiedTimeFrame</h1>
	 * <p>purpose: Give a timeFrame, create a filter. Use createFilterFromSpecifiedCustomTimeFrame
	 * 	for custom time frame</p>
	 * @param timeFrame = PAST_6_MONTHS, PAST_YEAR, PAST_3_YEARS
	 * @return
	 */
	private FilterCriteria createFilterFromSpecifiedTimeFrame(TimeFrameOptions timeFrame) {
		String filterName = this.createSavedSearchNameFromTimeFrame(timeFrame);
		FilterCriteria filter = new FilterCriteria();

		filter.setGenericFilter()
				.setFilterName(filterName);
		
		switch (timeFrame) {
		case CUSTOM_TIME_FRAME:
			return filter.setSelectTimeFrame(TimeFrameOptions.CUSTOM_TIME_FRAME)
						.setBidStartDateToToday()
						.setBidEndDateToOneMonthFromToday();
		default:
			return filter.setSelectTimeFrame(timeFrame)
						.setBidDatesFromPreselectedTimeFrame(); // dmidura: workaround to account for "9999" in bid end date
		}
	}
	
	/***
	 * <h1>getSearchFromSavedSearchesDropdown</h1>
	 * <p>purpose: Open the Saved Searches dropdown. Verify searchName is present in list and click on searchName.
	 * @param searchName = name of search to retrieve from Saved Searches dropdown
	 */
	private void getSearchFromSavedSearchesDropdown(String searchName) {
		// 1. Open Saved Searches dropdown
		// 2. Click on the search name for the custom search
		new DashboardCommon(driver)
    			.openSavedSearchesDropdown()
    			.verifySearchInSavedSearchList(searchName)
    			.clickOnSavedSearch(searchName);	
	}
	
	/***
	 * <h1>getFilterFromDatabase</h1>
	 * <p>purpose: Given the name of a filter in the database, retrieve it.
	 * @param filterName = name of filter to retrieve from ARGO database
	 * @return FilterCriteria representing stored filter
	 */
	private FilterCriteria getFilterFromDatabase(String filterName) {
		return new DashboardCommon(driver)
				.getFilterResultsSectionNewClass()
				.getSearchFromDatabase(filterName);
	}
	
	/* ----- Definitions ----- */

	/* Each DataTable that is used to input a filter has format as below:
	
	 * @param dTable = DataTable formatted as:
	 *    | Keywords               | [keyword1]                       | [keyword2]                      | ... | [keywordx]                      | 
	 *    | Negative Keywords      | [keyword1]                       | [keyword2]                      | ... | [keywordx]                      | 
	 *    | States/Provinces       | [state/province1_from_dropdown]  | [state/province2_from_dropdown> | ... | [state/provincex_from_dropdown] | 
	 *    | Time Frame             | [time_frame_from_dropdown]       |                                 | ... |                                 | 
	 *    | [Show Bids From]       | [start_date]                     |                                 | ... |                                 | 
	 *    | [Show Bids to]         | [end_date]                       |                                 | ... |                                 | 
	 *    
	 *    (start_date and end_date should be in MM/DD/YYYY format)
	 *    Here, you can add as many keywords and states as you want, BUT the table must be balanced (i.e. if you
	 *    add 3 states, then there should be 4 columns in the datatable). Filters that can only be set to one value should
	 *    then be input with blanks.
	 *    Note: if any field in the datatable is blank, then that filter will be skipped.
	 *    "Show Bids From" and "Show Bids to" fields should be provided only when the Time Frame = "Custom Time Frame"
	 *
	 *    Ex:  
	 *    | Keywords               | test                |      |         | 
	 *    | Negative Keywords      |                     |      |         | 
	 *    | States/Provinces       | Texas               | Utah | Alabama | 
	 *    | Time Frame             | Past 6 months       |      |         | 
	 *    
	 *    Ex: Time Frame = "Custom Time Frame"
	 *    | Keywords               | test                |      |         | 
	 *    | Negative Keywords      | bad                 | no   |         | 
	 *    | States/Provinces       | Texas               | Utah | Alabama | 
	 *    | Time Frame             | Custom Time Frame 	 |      |         | 
	 *    | Show Bids From         | 12/12/2016          |      |         | 
	 *    | Show Bids to           | 02/02/2018          |      |         | 
	 *    
	 */
	
	@And ("^I have saved a generic search with a random name$")
	public void I_have_saved_a_generic_search_with_a_random_name() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Saving search with random name");
		
		FilterCriteria filter = this.createGenericFilterWithRandomFilter();
		this.saveSearch(filter);
		GlobalVariables.storeObject("filter", filter);
	}

	@When ("^I try to save another search with the same name$")
	public void I_try_to_save_another_search_with_the_same_name() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Attempting to save another search with the same name");

		FilterCriteria filter = GlobalVariables.getStoredObject("filter");
		this.enterFilterClickSave(filter)
			.enterSearchName(filter.getFilterName())
			.clickOkBtn();
	}

	@When("^I delete the search from the Saved Searches list$")
	public void I_delete_the_search_from_the_Saved_Searches_list() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Deleting the search from the Saved Searches List");
		FilterCriteria filter = GlobalVariables.getStoredObject("filter");
		
		new DashboardCommon(driver)
				.openSavedSearchesDropdown()
				.deleteSavedSearchFromSavedSearchesDropdown(filter.getFilterName());
	}
	
	@When("^I retrieve the first search in the Saved Searches dropdown$")
	public void I_retrieve_the_first_search_in_the_Saved_Searches_dropdown() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Retrieving the first search in the Saved Search dropdown");

		DashboardCommon dashboard = new DashboardCommon(driver);
		String filterName = dashboard.openSavedSearchesDropdown()
				.getFirstSavedSearch()
				.getSavedSearchName();
		
		dashboard.getsSavedSearchesDropdown()
				.getFirstSavedSearch()
				.clickOnSearchInSavedSearches();
		
		// Retrieve filter from database
		FilterCriteria filter = this.getFilterFromDatabase(filterName);
		GlobalVariables.storeObject("filter", filter);
	}

	@When ("^I create a saved search with a random name$")
	public void I_create_a_saved_search_with_a_random_name() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Creating saved search with random name");

		FilterCriteria filter = this.createGenericFilterWithRandomFilter();
		this.saveSearch(filter);
		GlobalVariables.storeObject("filter", filter);
	}

    @When ("^I create a search but then cancel its save$")
    public void I_create_a_search_but_then_cancel_its_save() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Creating search, but cancelling save in popup");
		FilterCriteria filter = this.createGenericFilterWithRandomFilter()
							.setBidDatesFromPreselectedTimeFrame(); //dmidura: workaround to account for "9999" in bid end date
		
		// Save out filter and then cancel
		this.enterFilterClickSave(filter)
				.enterSearchName(filter.getFilterName())
				.clickNoThanksBtn();
		
		GlobalVariables.storeObject("filter", filter);
    }
 
    @When ("^I resave the search with a random name$")
    public void I_resave_the_search_with_a_random_name() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that saved search appears in the Saved Searches dropdown");
		FilterCriteria filter = GlobalVariables.getStoredObject("filter");
		
		// 1. Verify filter is still displaying in the Filter Results section
		// 2. Click "Save Search"
		// 3. Enter search name into popup and save
		new FilterResultsSectionNewClass(driver)
				.verifyFilterResultsDisplaysUnsavedFilter(filter) 
				.clickSaveSearchBtn()
				.saveSearch(filter.getFilterName());
    }

    /***
     * @param timeFrame = "Past 6 Months" | "Past Year" | "Past 3 Years" 
     */
    @When ("^I save out a search with specified time frame \"(.*)\"$")
    public void I_save_out_a_search_with_specified_time_frame(String timeFrame) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Creating and saving search with time frame = \"" + timeFrame + "\"");
		
		FilterCriteria filter = this.createFilterFromSpecifiedTimeFrame(TimeFrameOptions.fromString(timeFrame));
		this.saveSearch(filter);
		GlobalVariables.storeObject("filter", filter);
    }
    
    @When ("^I save out a search with a custom time frame$")
    public void I_save_out_a_search_with_a_custom_time_frame() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Creating and saving new search with custom time frame");

		FilterCriteria filter = this.createFilterFromSpecifiedTimeFrame(TimeFrameOptions.CUSTOM_TIME_FRAME);
		this.saveSearch(filter);
		GlobalVariables.storeObject("filter", filter);
    }
    
    @When ("^I select a saved search with specified time frame \"(.*)\" from the Saved Searches dropdown$")
    public void I_select_a_saved_search_with_specified_time_frame_from_the_Saved_Searches_dropdown(String timeFrame) {
 		// Create the same filter with timeFrame as
    	// "Filter Results Section Allows for Creation of Search with Specified Time Frame" test
    	//Fix for release OCT 7
//		FilterCriteria filter = this.createFilterFromSpecifiedTimeFrame(TimeFrameOptions.fromString(timeFrame));
//		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Selecting search \"" + filter.getFilterName() + "\" from Saved Searches");

    	FilterCriteria filter = GlobalVariables.getStoredObject("filter");
		this.getSearchFromSavedSearchesDropdown(filter.getFilterName());
		GlobalVariables.storeObject("filter", filter);
   	
    }
    
    @When ("^I select my search saved with a custom time frame from the Saved Searches dropdown$")
	public void I_select_my_search_saved_with_a_custom_time_frame_from_the_Saved_Searches_dropdown() {

 		// Create the same filter with custom time frame as per the 
    	// "Filter Results Allows for Creation of Search with a Custom Time Frame" test
    	//FilterCriteria filter = this.createFilterFromSpecifiedTimeFrame(TimeFrameOptions.CUSTOM_TIME_FRAME);
    	//Fix for release OCT 7
    	FilterCriteria filter = GlobalVariables.getStoredObject("filter");
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Selecting search \"" + filter.getFilterName() + "\" from Saved Searches");
		this.getSearchFromSavedSearchesDropdown(filter.getFilterName());
		GlobalVariables.storeObject("filter", filter);
    }

    @When ("^I view the Save Search Popup$")
    public void I_view_the_Save_Search_Popup() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking Save Search button in order to view Saved Search popup");
		
//		new DashboardCommon(driver)
//				.openFilterResultsSection()
		new FilterResultsSavePopupNewClass(driver).clickSaveSearchBtn();
    }
    
    @When ("^I try to save a search with a blank name$")
    public void I_try_to_save_a_search_with_a_blank_name() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Creating search and attempting to save without name");
		
		// 1. Open Filter Results
		// 2. Just click on "Save Search" button to launch Save Search popup
		// 3. Don't enter a name
//		new DashboardCommon(driver)
//				.openFilterResultsSection()
		new FilterResultsSavePopupNewClass(driver).clickSaveSearchBtn()
				.enterSearchName("");
    }

    @Then ("^I will verify that validation will reject the blank name in the Saved Search popup$")
    public void I_will_verify_that_validation_will_reject_the_blank_name_in_the_Saved_Search_popup() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Saved Search popup rejects blank name");
		
		new FilterResultsSavePopupNewClass(driver)
				.verifyCannotSaveSearchDueToBlankName();
    	
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Saved Search popup rejects missing name when trying to save search");
    }
    

    @Then ("^I will verify that the Save Search Popup has correctly initialized$")
    public void I_will_verify_that_the_Save_Search_Popup_has_correctly_initialized() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Saved Search popup properly initializes");
    
		new FilterResultsSavePopupNewClass(driver)
				.verifyFilterResultsSaveSearchPopupCorrectlyLoaded();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Saved Search popup has correctly loaded");
    }
    
    @Then ("^I will verify that my saved search displays filters in the Filter Results section$")
    public void I_will_verify_that_my_saved_search_displays_filters_in_the_Filter_Results_section() {
    	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Filter Results section updates to display retrieved saved search withcustom time frame");

    	FilterCriteria filter = GlobalVariables.getStoredObject("filter");

    	new DashboardCommon(driver)
    		.verifyFilterResultsSectionOpen()
    		.getFilterResultsSectionNewClass()
    		.verifyFilterResultsDisplaysSavedFilter(filter);
     
        System.out.println(GlobalVariables.getTestID() + " Test Passed: Filter Results section has updated to reflect the retrieved saved search with custom time frame");
    }


	@Then("^I will verify that the All Bids list will display results per my retrieved search$")
	public void I_will_verify_that_the_All_Bids_list_will_display_results_per_my_retrieved_search() throws ClassNotFoundException, SQLException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that retrieved search is displaying results in the All Bids list");
		
		// Get the name of the selected search
		// And then pull all its info from the database
		// and store as a filter criteria
		FilterCriteria filter         = GlobalVariables.getStoredObject("filter");
		FilterCriteria databaseFilter = this.getFilterFromDatabase(filter.getFilterName());
		
		// And now verify that the results displaying in the All Bids list 
		// match the filter criteria
		new FilterResultsSectionNewClass(driver)
				.verifyTabIsHighlightedAndSelected(BidListTabNames.ALL_BIDS)
				.getFilterResultsSectionNewClass()
				.verifyFilterSearchResultsDisplayCorrectlyInAllBidsTab(databaseFilter);
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Each bid in All Bids list is displaying per selected saved search");
	}
	
	 @Then("^I will verify that I cannot save the search with duplicated name$")
	 public void I_will_verify_that_I_cannot_save_the_search_with_duplicated_name() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that validation triggers for duplicate search name, and Save Search popup is still visible");

		 new FilterResultsSavePopupNewClass(driver)
		 		.verifyCannotSaveSearchDueToDuplicateName();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: User cannot save searches with duplicated names");
	 }

	@Then("^I will verify that the search has been deleted from the Saved Searches list$")
	public void I_will_verify_that_the_search_has_been_deleted_from_the_Saved_Searches_list(){
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that search was deleted from the Saved Searches dropdown");

		FilterCriteria filter = GlobalVariables.getStoredObject("filter");
		
		new DashboardCommon(driver)
				.openSavedSearchesDropdown()
				.verifySearchNotInSavedSearchList(filter.getFilterName());

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Search was successfully deleted from the Saved Searches dropdown");
	}

	@Then("^I will verify that the Saved Searches dropdown is not visible on the dashboard$")
	public void I_will_verify_that_the_Saved_Searches_dropdown_is_not_visible_on_the_dashboard() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Saved Searches dropdown is not visible on the dashboard");

		new DashboardCommon(driver)
				.verifySavedSearchDropdownIsHidden();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Saved Searches dropdown is not visible on the dashboard");
	}

	@Then("^I will verify that the Saved Searches dropdown is visible on the dashboard$")
	public void I_will_verify_that_the_Saved_Searches_dropdown_is_visible_on_the_dashboard() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Saved Searches dropdown is visible on the dashboard");
		FilterCriteria filter = GlobalVariables.getStoredObject("filter");
		
		// Saved Search should be visible and should display the name of the just-saved search
		new DashboardCommon(driver)
				.verifySavedSearchDropdownIsVisible()
				.verifySavedSearchesNameMatchesMySearchName(filter.getFilterName());

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Saved Searches dropdown is visible on the dashboard");
	}
	
	@Then ("^I will verify that my saved search appears in the Saved Searches dropdown$")
	public void I_will_verify_that_my_saved_search_appears_in_the_Saved_Searches_dropdown() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that saved search appears in the Saved Searches dropdown");
		FilterCriteria filter = GlobalVariables.getStoredObject("filter");

		new DashboardCommon(driver)
				.openSavedSearchesDropdown()
				.verifySearchInSavedSearchList(filter.getFilterName());

		System.out.println(GlobalVariables.getTestID() + " Test Passed: My saved search is listed in the Saved Searches dropdown and dropdown name has updated to display my search");
	}

	
   @Then ("^I will verify that the cancelled saved search does not appear in the Saved Searches dropdown$")
    public void I_will_verify_that_the_cancelled_saved_search_does_not_appear_in_the_Saved_Searches_dropdown() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that saved search appears in the Saved Searches dropdown");
		FilterCriteria filter = GlobalVariables.getStoredObject("filter");
		
		// 1. Verify that Saved Searches is showing "SAVED SEARCHES" for its title and not the cancelled search name
		// 2. Open Saved Searches dropdown
		// 3. Verify the cancelled search name is not listed in the saved searches list
		new DashboardCommon(driver)
				.verifySavedSearchesNameIsSavedSearches()
				.openSavedSearchesDropdown()
				.verifySearchNotInSavedSearchList(filter.getFilterName())
				.clickOffMouseFocus();
    	
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Saved Searches dropdown is visible on the dashboard");
    }

    @Then ("^I will verify that Saved Search name is saved in the Saved Searches dashboard$")
    public void I_will_verify_that_Saved_Search_name_is_saved_in_the_Saved_Searches_dashboard() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that saved search appears in the Saved Searches dashboard");
		
		// Verify that Saved Searches is showing "SAVED SEARCHES" for its title and not the name of previously saved search name
		new DashboardCommon(driver)
				.verifySavedSearchesNameIsSavedSearches();
   	
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Saved Searches name is visible on the dashboard");
    }
   
    @Then ("^I will verify that my search did not save out to the database$")
    public void I_will_verify_that_my_search_did_not_save_out_to_the_database() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that saved search appears in the Saved Searches dropdown");
		FilterCriteria filter = GlobalVariables.getStoredObject("filter");

		new FilterResultsSectionNewClass(driver)
				.verifyFilterDidNotSaveOutToDatabase(filter);
    	
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Saved Searches dropdown is visible on the dashboard");
    }

	@Then ("^I will verify that my search has saved out to the database$")
	public void I_will_verify_that_my_search_has_saved_out_to_the_database() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying search saved out to database");
		FilterCriteria filter = GlobalVariables.getStoredObject("filter");

		new FilterResultsSectionNewClass(driver)
				.verifyFilterSavedOutToDatabase(filter);
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Saved Searches dropdown is visible on the dashboard");
	}

}

