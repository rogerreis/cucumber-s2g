@must @registration @level3
Feature: Supplier Registration - Get Invited to Bids Screen (i.e. Company Lookup screen)

@ARGO-946
Scenario: Supplier Registers for a BidSync Basic Account, Verifies Their Email, and Changes Password.
	"Get Invited to Bids" screen will then correctly initalize per:
	1. All page objects appear
	2. Placeholder messages are correct
	3. Navigation buttons are functional
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to the Get Invited to Bids screen
	Then I will verify that the Company Lookup screen correctly initializes


@ARGO-946
Scenario Outline: Company Lookup Autocompletes With Multiple Companies Based on User Input as User Types, and User can Select Result
	Where Results of Each Lookup to the Company Lookup are Validated to Make Sure that:
	1. A Company Name Displays
	2. A Company Image Displays
	3. A Company URL Displays and is of valid form, or Company Adddress Displays and is of valid form
	4. The Company Name or Company URL Matches Some Part of the Input into the Company Lookup
	Note: To locate the company correctly, company name is case sensitive
	dmidura: Validation is currently failing per BIDSYNC-42, so workaround is in place to ignore

	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to the Get Invited to Bids screen
	And I have entered "<searchTerm>" into the Company Lookup one letter at a time and receive valid results
	When I select the top returned result from the Company Lookup autocomplete results
	Then I will verify that I am now the owner of the company

Examples: Companies to Test With
	| searchTerm       					|
	| This Old House Ventures   |
	| FedEx					   					|
	
	# Fedex          = verifies basic functionality
	# This Old House = verifies search string works

@ARGO-946
Scenario: Company Lookup Autocompletes to a Single Company Based on User Input of New Company not in Lookup
	Where Results of Each Lookup to the Company Lookup are Validated to Make Sure that:
	1. A Company Name Displays
	2. A Company Image Displays
	3. A Company URL Displays and is of valid form, or Company Address displays and is of valid form
	4. The Company Name or Company URL Matches Some Part of the Input into the Company Lookup
	dmidura: Validation is currently failing per BIDSYNC-42, so workaround is in place to ignore
	
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to the Get Invited to Bids screen
	When I have entered a random string into the Company Lookup one letter at a time and receive valid results
	Then I will verify that after completing input of my company name no result is returned by the company lookup
	And I will verify that I am now the owner of the company
