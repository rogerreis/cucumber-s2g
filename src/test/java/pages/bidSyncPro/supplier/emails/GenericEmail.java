package pages.bidSyncPro.supplier.emails;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;

import org.apache.commons.text.StringEscapeUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;
import pages.mailinator.Mailinator;

public class GenericEmail extends BasePageActions {
	

	// Variables
	protected String strEmailFileName;	 // The name of the json file containing the email elements
	                                     // ex: "RegistrationEmail.json"
	                                     // Should be located in the same dir as this class
	protected String strGreetingTxt;     // Contains the email greeting text extracted from strEmailFileName
	protected String [] strBodyTxt;      // Contains the email body text extracted from strEmailFileName
	protected String [] strFooterTxt;    // Contains the email footer text extracted from strEmailFileName

	// Other
	private EventFiringWebDriver driver;
	private final String strEmailPath = "src/test/java/pages/bidSyncPro/supplier/emails/";

	/***
	 * <h1>GenericEmail</h1>
	 * @param driver = EventFiringWebDriver
	 * @param strEmailFileName = the name of the json file containing the email elements<br>
	 * 		  ex: "RegistrationEmail.json" <br>
	 * 		  This file should be located in the same dir as this GenericEmail class.</p>
	 * @throws Throwable
	 */
	public GenericEmail(EventFiringWebDriver driver, String strEmailFileName) {
		super(driver);
		this.driver 			= driver;
		this.strEmailFileName 	= strEmailFileName;
		readInJsonEmailFile(strEmailFileName);
	}	
	
    /* ------------ Helper Methods ---------------- */
	
	/***
	 * <h1>readInJsonEmailFile</h1> 
	 * <p>purpose: Read in the supplied email</p>
	 * @param strEmailName = name of the email<br>
	 * 	ex: "RegistrationEmail.json"</p>
	 * @return None
	 */
	private void readInJsonEmailFile(String strEmailName) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Reading in email from \"%s\"\n", strEmailPath + strEmailName);

		// Open strUserAccountFileName file for reading
		Gson gson             = new Gson();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(strEmailPath + strEmailName));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
				
		// Grab the info about the email stored in strEmailName
		//Type type = new TypeToken<List<ExpectedEmailFormat>>(){}.getTypeOfIndustry();
		Type t = new TypeToken<ExpectedEmailFormat>(){}.getType();

		// And save out
		ExpectedEmailFormat Email = gson.fromJson(reader, t);
		strGreetingTxt            = Email.getGreeting();
		strBodyTxt                = Email.getBody();
		strFooterTxt              = Email.getFooter();
	}

	/***
	 * <h1>formatTextToMatchEmailOutput</h1>
	 * <p>purpose: Many of the json parameters are sent as String arrays, where a blank entry indicates a newline.<br>
	 *    This method grabs the String array and returns a single string that has replaced all blank entries with \n</p>
	 * @return String = single string containing the text
	 */
	protected String formatTextToMatchEmailOutput(String [] strText) {
		System.out.println(GlobalVariables.getTestID() + " INFO: Formatting text to match email output.");
		String strReturn = "";
		for (String s : strText) { 
			if (s.isEmpty()) { 
				strReturn += s.replace("", "\\n"); 
			} else {
				strReturn += s; }
		}
		return strReturn;
	}
	
	/**
	 * <h1>findSearchString</h1> 
	 * <p>purpose: We want to use a string in our input strArray to search, but it must not be empty.<br>
	 *    Run through the strArray and return the first non empty string. If the string contains a<br>
	 *    variable tag, then return a non-variable part of the string</p>
	 * @param strArray = String [] of inputs
	 * @return String = a string that is not empty | empty if no matching string
	 */
	private String findSearchString (String[] strArray) {
		for (String i : strArray) {
			// Since we can have newlines in our body text, make sure we're not using a newline as our search string
			if (!i.isEmpty()) { 

				// Make sure we're not using a search string with a variable token
				// if we are, then we'll split the string and use the non-variable part of the string
				if (i.contains("<TOKEN>")) {
					String [] strSplit  = i.split("<TOKEN>");
					String [] strSplit2 = strSplit[0].replace("<BASEURL>", System.getProperty("supplierBaseURL")).split("\\."); 
					
					// And we'll return only the first sentence
					return strSplit2[0];}

				// And we'll return only the first sentence
				String [] strSplit = i.replace("<BASEURL>", System.getProperty("supplierBaseURL")).split("\\."); 
				return strSplit[0];}}

		return "";
	}

	/* ------------ Public Methods ---------------- */


	/***
	 * <h1>isGreetingTextWithVariableUserNameCorrect</p>
	 * <p>purpose: Verify that the greeting text matches the greeting in the email.json <br>
	 * 	Where the greeting name should match strUserName. (Note a < NAME> tag should be included in email.json for the "Greeting")</p>
	 * @param strUserName = the name of your user
	 * @return true = email greeting displays as expected | false = greeting body does not display as expected
	 */
	public Boolean isGreetingTextWithVariableUserNameCorrect(String strUserName) {
		String strExpectedGreeting = strGreetingTxt.replace("<NAME>", strUserName);
		Boolean bReturn = false;
		System.out.printf(GlobalVariables.getTestID() + " INFO: Determining if the email greeting is addressed to user \"%s\" and matches greeting in \"%s\" email\n", strUserName, strEmailFileName);
		
		System.out.printf(GlobalVariables.getTestID() + " \n--------- Expected Email Greeting ---------\n");
		System.out.printf(GlobalVariables.getTestID() + " %s\n", strExpectedGreeting); 
		System.out.printf(GlobalVariables.getTestID() + " ----------------------------------------\n");
		
		// Basically we just need to locate the greeting as an element. If we find it, then we've verified the correctness.
		if (!this.findAllOrNoElements(driver, By.xpath("//tbody//td[contains(.,\"" + strExpectedGreeting + "\")]")).isEmpty()){ bReturn = true; }
		
		return bReturn;
	}
	
	/***
	 * <h1>isBodyTextCorrect</h1>
	 * <p>purpose: Determine if the registration email's body text is displaying as expected.<br>
	 * dmidura: This method assumes our body text is contained in a single paragraph onscreen<br>
	 * This method will need to updated, if that changes</p>
	 * @return true = email body displays as expected | false = email body does not display as expected
	 */
	public Boolean isBodyTextCorrect() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Determining if body text in the email is correct");
		Boolean bReturn = false;

		// First we'll locate a WebElement based on searching on a string from our expected body text to define the displayed body text
		String strSearchPath = "//tbody//td[contains(.,'" + findSearchString(strBodyTxt).trim() + "')]";

		if (!driver.findElements(By.xpath(strSearchPath)).isEmpty()) {

			// Now we'll verify the body text
			String strExpectedText      = formatTextToMatchEmailOutput(strBodyTxt);
			strExpectedText             = strExpectedText.replace("<BASEURL>", System.getProperty("supplierBaseURL"));
			String strDisplayedBodyText = StringEscapeUtils.escapeJava(driver.findElement(By.xpath(strSearchPath)).getText().toString());

			System.out.printf(GlobalVariables.getTestID() + " \n--------- Expected Email Body ---------\n");
			System.out.println(strExpectedText);
			System.out.printf(GlobalVariables.getTestID() + " ----------------------------------------\n");

			System.out.printf(GlobalVariables.getTestID() + " \n--------- Displayed Email Body ---------\n");
			System.out.println(strDisplayedBodyText);
			System.out.printf(GlobalVariables.getTestID() + " ----------------------------------------\n");
			
			// Does our body text include a <TOKEN> tag to indicate a registration or password reset token? 
			String [] Locators1 = strExpectedText.split("<TOKEN>");
			if (Locators1.length != 0){
				// We did locate a <TOKEN>, so we'll need to strip out the tag and then verify
				// three strings: body text before token, token path, body text after token
				String [] Locators2 = Locators1[0].split("http");

				String strBodyTextBefore 	= Locators2[0].trim();
				String strTokenPath 		= "http" + Locators2[1].trim();
				String strBodyTextAfter 	= Locators1[1].trim();
				
				if (strDisplayedBodyText.contains(strBodyTextBefore) &
					strDisplayedBodyText.contains(strTokenPath) &
					strDisplayedBodyText.contains(strBodyTextAfter)) { bReturn = true; }
				
			} else {
				// No token present, so we'll just use the text as is. 
				if (strDisplayedBodyText.contains(strExpectedText) ){ bReturn = true; }
			}
		} else { fail("ERROR: Unable to locate a WebElement to define the body text based on search string \"" + findSearchString(strBodyTxt).trim() + "\""); }

		return bReturn;
	}

	/***
	 * <h1>isFooterTextCorrect:</h1>
	 * <p>purpose: Determine if the registration email's footer text is displaying as expected.<br>
	 * TODO dmidura: complete method. need to wait until Shane sends us a complete footer to use</p>
	 * @return true = email footer displays as expected | false = footer body does not display as expected
	 */
	public Boolean isFooterTextCorrect() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Determining if footer text in the email is correct");
		Boolean bReturn = false;
        
        System.out.print("\n--------- Expected Email Footer ---------\n");
		for (String i : strFooterTxt) { System.out.printf(GlobalVariables.getTestID() + " %s\n", i); }
        System.out.print("----------------------------------------\n");

		if (this.findAllOrNoElements(driver, By.xpath("//tbody//p[contains(.,\"" + strFooterTxt[1] + "\")]")).isEmpty()) { bReturn = true; }

		return bReturn;
	}
	
	/***
	 * <h1>clickOnTokenURLInEmail</h1> 
	 * <p>purpose: Windows: Locate the token URL from the email and then click on the link.
	 * 	Mac: Locate the token, extract, and then navigate directly to link</p>
	 * @return None
	 */
	public GenericEmail clickOnTokenURLInEmail () {
		System.out.println(GlobalVariables.getTestID() + " INFO: Locating and then clicking on the token URL in the email.");
        
        String linkXpath = "//a[contains(., '" + System.getProperty("supplierBaseURL") + "')]";
        
		if(!System.getProperty("os.name").toLowerCase().startsWith("mac os x")){
			// Windows -- click the token
			driver.executeScript("window.scrollBy(0,200);");
			findByVisibility(By.xpath(linkXpath), 10).click();
		} else {
			// Mac -- will not open a new tab for the link, so we'll just navigate directly
			String tokenLink = findByVisibility(By.xpath(linkXpath)).getAttribute("href");
			this.driver.get(tokenLink);	
		}
        
        return this;
	}
	
	public Boolean isInvalidDateDisplayed() {
		driver.switchTo().defaultContent();
		Boolean bReturn = false;
		String invalidDateXpath = "//td[contains(text(),'Received:')]/following-sibling::td/b";
		if(findByVisibility(By.xpath(invalidDateXpath)).getText().contains("Invalid Date")) {
	    	 bReturn = true; 
	      }

		return bReturn;
	}
	
	public GenericEmail checkInvalidDate(String strEmail) {
	    
		if(isInvalidDateDisplayed()) {
	    	 System.out.println(" INFO: Invalid date text is displayed");
	    	 
	    	 new Mailinator(driver)
     		.navigateToMailinatorInbox(strEmail)
            .openRegistrationCompleteEmail()
	    	.clickOnTokenURLInEmail();
	    	 
	     } else {
	    	 driver.switchTo().frame("msg_body");
	    	 System.out.println(" INFO: Invalid date text is not displayed");	
	     }
		 
	     return this;
	}
	
	/* ----- verifications ----- */

	/***
	 * <h1>verifyGreetingTextIsCorrect</h1>
	 * <p>purpose: Verify that the email's greeting text matches expected format.<br>
	 * 	Note: Expected format is defined in json email file</p>
	 * @return GenericEmail
	 */
	public GenericEmail verifyGreetingIsCorrect(String strUserName) {
		if (!this.isGreetingTextWithVariableUserNameCorrect(strUserName)) {
			fail("Test Failed: Email greeting is incorrect"); }
		System.out.println(GlobalVariables.getTestID() + " Info: Verified email greeting matches expected format");
		return this;
	}
	
	/***
	 * <h1>verifyBodyTextIsCorrect</h1>
	 * <p>purpose: Verify that the email's body text matches expected format.<br>
	 * 	Note: Expected format is defined in json email file</p>
	 * @return GenericEmail
	 */
	public GenericEmail verifyBodyTextIsCorrect() {
		if(!this.isBodyTextCorrect()) {
			fail("Test Failed: Body text is incorrect"); }
		System.out.println(GlobalVariables.getTestID() + " Info: Verified body text matches expected format");
		return this;
	}

	/***
	 * <h1>verifyFooterTextIsCorrect</h1>
	 * <p>purpose: Verify that the email's footer text matches expected format.<br>
	 * 	Note: Expected format is defined in json email file</p>
	 * @return GenericEmail
	 */
	public GenericEmail verifyFooterTextIsCorrect() {
		if(!this.isFooterTextCorrect()) {
			fail("Test Failed: Footer text is incorrect"); }
		System.out.println(GlobalVariables.getTestID() + " Info: Verified footer text matches expected format");
		return this;
	}

}
