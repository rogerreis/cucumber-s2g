package data.bidSyncPro.BidCard;

public class UnSubscribedBidCard {

	private String bidName;
	private String city;
	private String description;
	private String viewBidLink;
	private String bidNumber;
	private String dueDate;
	private String noOfDays;
	private String noOfDaysLeft;
	private String matchType;
	private String subscribeButtonExists;
	
	public String getbidName() {
        return bidName;
    }
 
    public void setBidTitle(String bidName) {
        this.bidName = bidName;
    }
    
	public String getcity() {
        return city;
    }
 
    public void setBidLocation(String city) {
        this.city = city;
    }
    
	public String getdescription() {
        return description;
    }
 
    public void setBidDescription(String description) {
        this.description = description;
    }

	public String getviewBidLink() {
        return viewBidLink;
    }
 
    public void setViewBidLink(String viewBidLink) {
        this.viewBidLink = viewBidLink;
    }

        
	public String getbidNumber() {
        return bidNumber;
    }
 
    public void setBidNumber(String bidNumber) {
        this.bidNumber = bidNumber;
    }
    
	public String getdueDate() {
        return dueDate;
    }
 
    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }
    
	public String getnoOfDays() {
        return noOfDays;
    }
 
    public void setNoOfDaysLeft(String noOfDays) {
        this.noOfDays = noOfDays;
    }
    
	public String getnoOfDaysLeft() {
        return noOfDaysLeft;
    }
 
    public void setDaysLeftText(String noOfDaysLeft) {
        this.noOfDaysLeft = noOfDaysLeft;
    }
    
	public String getmatchType() {
        return matchType;
    }
 
    public void setmatchType(String matchType) {
        this.matchType = matchType;
    }
    
	public String getSubscribeButtonExists() {
        return subscribeButtonExists;
    }
 
    public void setSubscribeButtonExists(String subscribeButtonExists) {
        this.subscribeButtonExists = subscribeButtonExists;
    }

}
