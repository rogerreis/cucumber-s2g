package pages.bidSyncPro.supplier.dashboard;

import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterResultsSectionNewClass;
import pages.bidSyncPro.supplier.dashboard.savedSearches.SavedSearchesDropdown;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

import java.util.concurrent.TimeUnit;

/***
 * <h1>Class SupplierHome</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects related to the common objects on all Dashboard pages.<br>
 * 	Included Objects: Filter open/close button, Bid Tabs ("New For You", "Your Saved Bids", "All Bids"), Saved Searches</p>
 */
public class DashboardCommon extends CommonTopBar {

	// Other
	private HelperMethods helperMethods;

	// URLs
	private String strNewForYouPagePath      = System.getProperty("supplierBaseURL") + "dashboard/new-bids";
	private String strYourSavedBidsPagePath  = System.getProperty("supplierBaseURL") + "dashboard/my-bids";
	private String strAllBidsPagePath        = System.getProperty("supplierBaseURL") + "dashboard/all-bids";
	private String strInitializationPagePath = strNewForYouPagePath;
	
	// xpaths
	// Filter Results Section
    private final String filterResultsBtnImg_xpath               = "//button[@id='filterResultsButton']//mat-icon[@role='img' and contains(.,'filter_list')]";
    private final String filterResultsBtn_id                     = "filterResultsButton";
    // Filter Section Indicator -- used to indicate if the Filter Section is "hidden" or "visible" 
    private final String filterSectionIndicator_xpath            = "//mat-drawer//bidsync-dashboard-sidepanel";
    
    // Bid Tabs
    private final String newForYouBtnImg_xpath                   = "//a[@id='aNewBids_0']/mat-icon[@role='img' and contains(.,'flash_on')]";
    private final String newForYouBtn_id                         = "aNewBids_0";
    private final String yourSavedBidsBtnImg_xpath               = "//a[@id='aNewBids_1']/mat-icon[@role='img' and contains(.,'star')]";
    private final String yourSavedBidsBtn_id                     = "aNewBids_1";
    private final String allBidsBtnImg_id                        = "aNewBids_2";
    private final String allBidsBtn_xpath                        = "//a[@id='aNewBids_2' and contains(p, 'All Bids')]";
    private final String tabBtnUnderline_xpath                   = "//nav[@id='navTabBar']//mat-ink-bar";
    
    // Saved Searches
    private final String savedSearchesBtnArrowImg_xpath          = "//bidsync-sub-header//button[@id='saveSearchButtonHeader']//mat-icon[contains(text(), 'youtube_searched_for') ]";
    private final String savedSearchesDropdownBtn_xpath          = "//bidsync-sub-header//button[@id='saveSearchButtonHeader']";
    private final String savedSearchesDropdownName_xpath         = "//bidsync-sub-header//button[@id='saveSearchButtonHeader']//span[@class='ng-star-inserted']";

	public DashboardCommon (EventFiringWebDriver driver) {
		super(driver);
		helperMethods   = new HelperMethods();
	}

	/* ----- getters ----- */
	
	// Filter Results
    private WebElement getFilterResultsBtnImg() { return findByVisibility(By.xpath(this.filterResultsBtnImg_xpath)); }
    private WebElement getFilterResultsBtn   () { return findByVisibility(By.id(this.filterResultsBtn_id));          }

    // Bid Tabs
    private WebElement getNewForYouBtnImg()     { return findByVisibility(By.xpath(this.newForYouBtnImg_xpath));     }
    private WebElement getNewForYouBtn()        { return findByVisibility(By.id(this.newForYouBtn_id));              }
    private WebElement getYourSavedBidsBtnImg() { return findByVisibility(By.xpath(this.yourSavedBidsBtnImg_xpath)); }
    private WebElement getYourSavedBidsBtn()    { return findByVisibility(By.id(this.yourSavedBidsBtn_id));          }
    private WebElement getAllBidsBtnImg()       { return findByVisibility(By.id(this.allBidsBtnImg_id));             }
    private WebElement getAllBidsBtn()          { return findByVisibility(By.xpath(this.allBidsBtn_xpath));          }
    private WebElement getTabBtnUnderline()     { return findByVisibility(By.xpath(this.tabBtnUnderline_xpath));     }
    
    // Nav Menus
    
    // Saved Searches
    private WebElement getSavedSearchesDropdownBtn()    { return findByVisibility(By.xpath(this.savedSearchesDropdownBtn_xpath));  }
    private WebElement getSavedSearchesDropdownName()   { return findByVisibility(By.xpath(this.savedSearchesDropdownName_xpath)); }
    private WebElement getSavedSearchesArrowImg()   { return findByVisibility(By.xpath(this.savedSearchesBtnArrowImg_xpath)); }
    
	/* ----- methods ----- */
    
    /* ----- general dashboard methods ----- */
	/**
	 * <h1>getPageURL</h1>
	 * <p>purpose: Get the URL for the initialized Supplier Dashboard Screen<br>
	 * 	(Currently "New For You" tab)</p>
	 * @return String = URL of the initialized Supplier Dashboard Screen
	 */
	public String getPageURL() {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Getting the Page Path = \"%s\"\n", strInitializationPagePath);
		return strInitializationPagePath;
	}

	/***
	 * <h1>clickOffMouseFocus</p>
	 * <p>purpose: When a popup or element has taken MouseFocus, click off to release the focus and (if applicable) hide the popup or element</p>
	 * @return None
	 */
	public BidList clickOffMouseFocus () {
		System.out.println(GlobalVariables.getTestID() + " INFO: Clicking to remove MouseFocus.");
		driver.findElement(By.xpath("//div[@class=\"cdk-overlay-container\"]")).click();
		return new BidList(driver);
	}
	
	/***
	 * <h1>verifyDashboardCommonInitialization</h1>
	 * <p>purpose: Verify Dashboard Common initialization for the user's first login:<br>
	 * 	1. Bid Tabs visible with their images<br>
	 * 	2. Filter Results Button visible with its image<br>
	 * 	3. Saved Searches button is hidden<br>
	 * 	4. ALL BIDS tab is highlighted and selected</p>
	 * @return DashboardCommon
	 */
	public DashboardCommon verifyDashboardCommonInitializationForFirstLogin() {

		// Bid Tabs should each initialize
		this.getAllBidsBtn();
		this.getAllBidsBtnImg();
		this.getNewForYouBtn();
		this.getNewForYouBtnImg();
		this.getYourSavedBidsBtn();
		this.getYourSavedBidsBtnImg();

		// Filter Results button should initialize
		this.getFilterResultsBtn();
		this.getFilterResultsBtnImg();
		
		// Saved Searches should not be visible on first login
		this.verifySavedSearchDropdownIsHidden();
		
		// New For You tab is highlighted and selected
		this.verifyTabIsHighlightedAndSelected(BidListTabNames.NEW_FOR_YOU);
		
		return this;
		
		
	}
	
	/* ---- Saved Searches ----- */

	/**
	 * <h1>getSavedSearchesDropdownName</h1>
	 * <p>purpose: Assuming that saved searches dropdown is visible,<br>
	 *  get the current display of the Saved Searches dropdown <br>
	 * 	(this should either be "SAVED SEARCHES" | "\<searchName\>") <br>
	 *	Test fails if the Saved Searches dropdown is not displaying</p>
	 * @return String = currently displayed name on the Saved Searches dropdown
	 */
	private String getSavedSearchesCurrentlyDisplayedDropdownName() {
		if (!this.isSavedSearchDropdownVisible()) {
			fail("Test Failed: Saved Searches dropdown is not visible"); }

		return this.getSavedSearchesDropdownName().getText();
	}

	/***
	 * <h1>isSavedSearchDropdownVisible</h1>
	 * <p>purpose: Determine if the SAVED SEARCHES dropdown on the supplier home screen is currently visible or hidden
	 * @return true if visible | false if hidden
	 */
	private Boolean isSavedSearchDropdownVisible () {
		if (this.doesPageObjectExist(driver, By.xpath(this.savedSearchesDropdownBtn_xpath)) |
		    this.doesPageObjectExist(driver, By.xpath(this.savedSearchesDropdownName_xpath))|
		    this.doesPageObjectExist(driver, By.xpath(this.savedSearchesBtnArrowImg_xpath))) {
			return true; }
		return false;
	}

 	/***
	 * <h1>openSavedSearchesDropdown</h1>
	 * <p>purpose: Click on the Saved Searches button to view the Saved Searches dropdown</p>
	 * @return SavedSearchesDropdown
	 */
	public SavedSearchesDropdown openSavedSearchesDropdown () {
		System.out.println(GlobalVariables.getTestID() + " INFO: Opening the Saved Searches dropdown.");
		findByScrollIntoViewBottomOfScreen(By.xpath(this.savedSearchesDropdownBtn_xpath));
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getSavedSearchesDropdownBtn());
//		this.getSavedSearchesDropdownBtn().click();
		
		return new SavedSearchesDropdown(driver);
	}
	
	/***
	 *<h1>getSavedSearchesDropdown</h1> 
	 *<p>purpose: Return access to the Saved Searches class</p>
	 *@return SavedSearchesDropdown 
	 */
	public SavedSearchesDropdown getsSavedSearchesDropdown() {
		return new SavedSearchesDropdown(driver);
	}
	
	/***
	 * <h1>verifySavedSearchesDropdownIsVisible</h1>
	 * <p>purpose: Verify that the Saved Searches dropdown (button)<br>
	 * 	is currently displayed on the dashboard. This button should be<br>
	 * 	available whenever the user has at least one search saved.<br>
	 * 	Fail is dropdown is not visible</p>
	 * @return DashboardCommon
	 */
	public DashboardCommon verifySavedSearchDropdownIsVisible() {
		if (!this.isSavedSearchDropdownVisible()) {
			waitForVisibility(By.xpath(this.savedSearchesDropdownBtn_xpath));}
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that Saved Searches dropdown is visible on dashboard");
		return this;
	}

	/***
	 * <h1>verifySavedSearchesDropdownIsHidden</h1>
	 * <p>purpose: Verify that the Saved Searches dropdown (button)<br>
	 * 	is not currently displayed on the dashboard. This button should not be<br>
	 * 	available until the user has at least one search saved.<br>
	 * 	Fail is dropdown is visible</p>
	 * @return DashboardCommon
	 */
	public DashboardCommon verifySavedSearchDropdownIsHidden() {
		if (this.isSavedSearchDropdownVisible()) { fail("Test Failed: Saved Searches dropdown is visible on dashboard"); }
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that Saved Searches dropdown is not visible on dashboard");
		return this;
	}
	
	/***
	 * <h1>verifySavedSearchesNameMatchesMySearchName</h1>
	 * <p>purpose: When a search is selected off of or saved to the <br>
	 * 	Saved Searches dropdown, the dropdown name should update from<br>
	 * 	"Saved Searches" to the selected/saved search name. This method<br>
	 * 	will verify that the search name on Saved Searches currently displays mySearchName<br>
	 * 	Test will fail if the search name does not match mySearchName</p>
	 * @param mySearchName = what you're expecting the Saved Searches dropdown text to currently display
	 * @return DashboardCommon
	 */
	public DashboardCommon verifySavedSearchesNameMatchesMySearchName(String mySearchName) {
		String currentlyDisplayedText = this.getSavedSearchesCurrentlyDisplayedDropdownName();

		if (!currentlyDisplayedText.trim().equalsIgnoreCase(mySearchName.trim())) {
			fail("Test Failed: Expected \"" + mySearchName + "\" to display on Saved Searches dropdown, but instead got \"" + currentlyDisplayedText + "\""); }
		
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that Saved Searches dropdown currently displays text \"" + mySearchName + "\"");
		return this;
	}

	/***
	 * <h1>verifySaverdSearchesNameIsSavedSearches</h1>
	 * <p>purpose: When user logs into ARGO, then if the user has previously saved out searches,<br>
	 *  "SAVED SEARCHES" will display as the Saved Search dropdown name (and will continue<br>
	 *  to display in the current session as long as the user does not save out a new search<br>
	 *  or select an existing search from the Saved Searches dropdown)<br>
	 *  Use this method to verify that Saved Searches dropdown is displaying "SAVED SEARCHES"<br>
	 *  as the dropdown title. Fail if the Saved Searches dropdown title is not displaying<br>
	 *  "SAVED SEARCHES" or if no Saved Search dropdown is present</p>
	 * @return DashboardCommon
	 */
	public DashboardCommon verifySavedSearchesNameIsSavedSearches() {
		this.verifySavedSearchDropdownIsVisible();
		
		String currentlyDisplayedText = this.getSavedSearchesCurrentlyDisplayedDropdownName();

		if (!currentlyDisplayedText.trim().equals("SAVED SEARCHES")){
			fail("Test Failed: Expected \"SAVED SEARCHES\" to display on Saved Searches dropdown, but instead got \"" + currentlyDisplayedText + "\""); }
		
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that Saved Searches dropdown currently displays text \"SAVED SEARCHES\"");
		return this;
	}
	
	/* ----- Filter Results ----- */
    /***
     * <h1>getFilterResultsSection</h1>
     * <p>purpose: Access to the Filter Results class from the dashboard</p>
     */
    public FilterResultsSectionNewClass getFilterResultsSectionNewClass() {
	    return new FilterResultsSectionNewClass(driver);
    }
    
    /***
     * <h1>openFilterResultsSection</h1>
     * <p>purpose: Click on the Filter button on the dashboard<br>
     * 	to open the filter results section</p>
     * @return FilterResultsSectionNewClass
     */
    public FilterResultsSectionNewClass openFilterResultsSection() {
    	this.getFilterResultsBtn().click();
    	
    	// Need to let the filter results section load before
    	// any search criteria should be run (otherwise we have issues. 
    	// with the default keywords overriding filter selections). 
    	// Id we're waiting for corresponds to the filter results "keyword" section
    	waitForPresence(By.id("matKeywordPanel"));
    	helperMethods.addSystemWait(1);
    	
		return new FilterResultsSectionNewClass(driver);
    }

    /***
     * <h1>closeFilterResultsSection</h1>
     * <p>purpose: Click on the Filter button on the dashboard<br>
     * 	to close the filter results section</p>
     * @return DashboardCommon
     */
    public DashboardCommon closeFilterResultsSection() {
    	this.getFilterResultsBtn().click();
    	return this;
    }
 	
	/***
	 * <h1>verifyFilterResultsSectionClosed</h1>
	 * <p>purpose: Verify that the Filter Results collapsible section is currently<br>
	 * 	displayed as closed on the dashboard. Fail test if Filter Results collapsible section<br>
	 * 	is currently displayed as open.</p>
	 * @return DashboardCommon
	 */
	public DashboardCommon verifyFilterResultsSectionClosed() {
		try {
			waitForInvisibility(By.xpath(this.filterSectionIndicator_xpath));
		} catch (TimeoutException t) {
			fail("ERROR: Filter Results section should be closed, but is instead still open");
		}

		System.out.println(GlobalVariables.getTestID() + " INFO: Verified Filter Results collapsible section is closed on dashboard");
		return this;
	}

	/***
	 * <h1>verifyFilterResultsSectionOpen</h1>
	 * <p>purpose: Verify that the Filter Results collapsible section is currently<br>
	 * 	displayed as open on the dashboard. Fail test if Filter Results collapsible section<br>
	 * 	is currently displayed as closed.</p>
	 * @return DashboardCommon
	 */
	public DashboardCommon verifyFilterResultsSectionOpen() {
		try {
//			if(doesPageObjectExist(driver, By.xpath(this.filterSectionIndicator_xpath)))
			waitForVisibility(By.xpath(this.filterSectionIndicator_xpath));
		} catch (TimeoutException t){
			fail("ERROR: Filter Results section should be open, but is instead closed.");
		}

		System.out.println(GlobalVariables.getTestID() + " INFO: Verified Filter Results collapsible section is open on dashboard");
		return this;
	}
		
	/* ----- Bid Tabs ----- */
	
	/***
	* <h1>verifyTabIsHighlightedAndSelected</h1>
	* <p>purpose: Determine if a tab is currently displayed as underlined and<br>
	* 	also if the page URL is displaying per the correct tab</p>
	* @param strTabName = name of the tab to check<br>
	*    = "New For You" | "Your Saved Bids" | "All Bids"
	* @return BidList for the tab | will fail if not selected and highlighted
	*/
	public BidList verifyTabIsHighlightedAndSelected(BidListTabNames bidTab) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Determining if \"%s\" tab is currently selected and highlighted \n", bidTab.toString());
		
		// TODO dmidura: currently only checking that the underline is visible, but need to get with Shane about how to check that it's
		//	underlining the correct tab (only indicator currently is the value of the left pixel)
		Boolean bTabUnderlineState = this.getTabBtnUnderline().getCssValue("visibility").equals("visible");
		String strExpectedPageURL = "";
		
		switch (bidTab) {
			case ALL_BIDS:
				strExpectedPageURL = strAllBidsPagePath;
				break;
			case NEW_FOR_YOU:
				strExpectedPageURL = strNewForYouPagePath;
				break;
			case YOUR_SAVED_BIDS:
				strExpectedPageURL = strYourSavedBidsPagePath;
				break;
			default:
				fail("ERROR: \"" + bidTab.toString() + "\" is not a valid tab on the supplier dashboard");
				break;

		};
		waitForSpinner(driver);
		// We should both be on the page URL and displaying the underlined tab
		if (!driver.getCurrentUrl().equals(strExpectedPageURL) | !bTabUnderlineState ) { fail("Test Failed: Selected tab " + bidTab.toString() + " is not highlighted and selected."); }
		
		return new BidList(driver);
	}
	
	/***
	 * <h1>openBidTab</h1>
	 * <p>purpose: Click on the appropriate bid tab</p>
	 * @param bidTab = BidListNames for the tab to open
	 * @return BidList for the bidTab you want to open
	 */
	public BidList openBidTab (BidListTabNames bidTab ) {
		switch (bidTab) {
		case ALL_BIDS:
			return this.openAllBidsTab();
		case YOUR_SAVED_BIDS:
			return this.openYourSavedBidsTab();
		case NEW_FOR_YOU:
			return this.openNewForYouTab();
		default:
				return null;
		}
	}
	
	/**
	 * <h1>openYourSavedBidsTab</h1>
	 * <p>purpose: click on the "Your Saved Bids" tab to view that bid list</p>
	 * @return BidList
	 */
    public BidList openYourSavedBidsTab() {
	    System.out.println(GlobalVariables.getTestID() + " INFO: Opening the Your Saved Bids Tab.");
        this.getYourSavedBidsBtn().click();
        return new BidList(driver);
    }
	
	/**
	 * <h1>openAllBidsTab</h1>
	 * <p>purpose: click on the "All Bids" tab to view that bid list</p>
	 * @return BidList
	 */
    public BidList openAllBidsTab() {
        System.out.println(GlobalVariables.getTestID() + " INFO: Opening the All Bids Tab.");
        this.getAllBidsBtn().click();
        return new BidList(driver);
    }
    
	/**
	 * <h1>openNewForYouTab</h1>
	 * <p>purpose: click on the "New For You" tab to view that bid list</p>
	 * @return BidList
	 */
    public BidList openNewForYouTab() {
        System.out.println(GlobalVariables.getTestID() + " INFO: Opening the New For You Tab.");
        helperMethods.addSystemWait(300, TimeUnit.MILLISECONDS);
        this.getNewForYouBtn().click();
        return new BidList(driver);
    }
    
    /***
     * <h1>getCurrentBidList</h1>
     * <p>purpose: Access to the currently displayed bid list
     * @return BidList
     */
    public BidList getCurrentBidList() {
    	return new BidList(driver);
    }
    
}
