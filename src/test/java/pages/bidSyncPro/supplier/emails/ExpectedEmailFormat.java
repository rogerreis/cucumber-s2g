package pages.bidSyncPro.supplier.emails;

/***
 * <h1>Class ExpectedEmailFormat</h1>
 * @author dmidura
 * <p>details: This class holds info about generic emails sent from ARGO<br>
 *             Note: To save an email, create and name a file as <email_name>.json<br>
 *                   Then, use methods in GenericEmail to access/verify the email</p>
 *
 */
public class ExpectedEmailFormat {

		
		// User login credentials
		private String Greeting;		// The email greeting. Include <NAME> if the name is variable
		private String[] Body;			// The body text. Each entry in the array should designate a newline
										// in the body email. User <TOKEN> for a token variable and <BASEURL> for the domain
										// ex: https://supplier.periscopeholdings.cloud/register/v1/verify/xfJrjDvh0LIoeXwC2j2tEBxte7Iuz2zLQ 
										// should be stored as
										// <BASEURL>register/v1/verify/<TOKEN>
		private String[] Footer;		// The footer text. Each entry in the array should designate a newline in the footer section.

		public ExpectedEmailFormat () {
		}
		
		/* --------------- Methods --------------- */
		
		public void setGreeting (String strGreeting) {
			this.Greeting = strGreeting;
		}
		
		public String getGreeting () {
			return this.Greeting;
		}
		
		public void setBody (String [] strBody) {
			this.Body = strBody;
		}
		
		public String [] getBody () {
			return this.Body;
		}	
		
		public void setFooter (String [] strFooter) {
			this.Footer = strFooter;
		}
		
		public String [] getFooter () {
			return this.Footer;
		}
}
