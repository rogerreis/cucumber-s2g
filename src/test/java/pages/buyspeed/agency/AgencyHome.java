package pages.buyspeed.agency;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;


/***
 * <h1>Class AgencyHome</h1>
 * @author dmidura
 * <p>purpose: This class houses the agency home that any agency user will encounter after login
 */
public class AgencyHome extends AgencyTopAndSideBars {
	
	// xpaths
	
	// other
	private final String pageLoadId = "news";
	
	public AgencyHome(EventFiringWebDriver driver) {
		super(driver);
	}
	
	/***
	 * <h1>waitForDashboardLoad</h1>
	 * <p>purpose: Wait up to 10 seconds for the BP's dashboard to load<br>
	 * 	(specifically, the "News" section). Fail test if the "News" section <br>
	 * 	has not loaded after 10 seconds</p>
	 * @return BPHome
	 */
	public AgencyHome waitForDashboardToLoad() {
		waitForVisibility(By.id(this.pageLoadId), 10);
		return this;
	}

}
