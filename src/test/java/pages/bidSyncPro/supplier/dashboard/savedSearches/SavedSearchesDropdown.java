package pages.bidSyncPro.supplier.dashboard.savedSearches;

import static org.junit.Assert.fail;

import java.util.ArrayList;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.dashboard.DashboardCommon;

/***
 * <h1>Class SavedSearchesDropdown</h1>
 * @author dmidura
 * <p>details: This class houses the objects and methods for the Saved Search dropdown that appears on the dashboard (top left)</p>
 */
public class SavedSearchesDropdown extends DashboardCommon {
	
	// xpath
	private final String savedSearchItemRoot_xpath = "//div[contains(@class, 'mat-menu-panel')]//mat-list//mat-list-item";
	private final String firstRowInDropdown_xpath  = savedSearchItemRoot_xpath + "[1]";
	
	// Other
	private EventFiringWebDriver driver;

	public SavedSearchesDropdown(EventFiringWebDriver driver) {
		super (driver);
		this.driver = driver;
		this.waitForDropdownLoad();
	}
	

	/* ----- methods ----- */
	
	/***
	 * <h1>waitForDropdownLoad</h1>
	 * <p>purpose: Wait for the Saved Searches dropdown to load</p>
	 * @return SavedSearchesDropdown
	 */
	private SavedSearchesDropdown waitForDropdownLoad() {
		if(!doesPageObjectExist(driver, By.xpath(this.firstRowInDropdown_xpath)))
			openSavedSearchesDropdown();
		waitForVisibility(By.xpath(this.firstRowInDropdown_xpath));
		return this;
	}
	

	/***
	 *<h1>getFirstRowInSavedSearchesDropdown</h1>
	 * <p>purpose: Get the first row in the Saved Searches dropdown<br>
	 * 	(assuming there is one). Test fails if no object</p>
	 * @return SavedSearchRow 
	 */
	private SavedSearchRow getFirstRowInSavedSearchesDropdown() {
		return new SavedSearchRow(driver, this.firstRowInDropdown_xpath);
	}
	
	/***
	 * <h1>getAllSavedSearchesInSavedSearchesDropdown</h1>
	 * <p>purpose: Get all the Saved Search rows in the Saved Search dropdown</p>
	 * @return List <SavedSearchRow>
	 */
	private List <SavedSearchRow> getAllSavedSearchesInSavedSearchesDropdown() {

		// Get WebElement for each saved search row
		List <WebElement> allSavedSearchRowElements = this.findAllOrNoElements(driver, By.xpath(this.savedSearchItemRoot_xpath));
		
		int intNumberOfSearches = allSavedSearchRowElements.isEmpty()? 1 : allSavedSearchRowElements.size();

		List <SavedSearchRow>allSavedSearchRows = new ArrayList<SavedSearchRow>();
		for(int i=1; i <intNumberOfSearches + 1; i++) {
			allSavedSearchRows.add(new SavedSearchRow(driver, savedSearchItemRoot_xpath + "[" + i + "]" ));}
		
		/* Uncomment for debug
		System.out.println(GlobalVariables.getTestID() + " Retrieving searches from Saved Searches dropdown");
		allSavedSearchRows.forEach(search->System.out.println(search.getSavedSearchName()));
		*/

		return allSavedSearchRows;
	}

	/***
	 * <h1>doesSavedSearchesDropdownContainMySearch</h1> 
	 * <p>purpose: Check if the Saved Searches dropdown contains a specific saved search</p>
	 * @param strSearchName = the saved search to look for
	 * @return Boolean = true if strSearchName is listed in the Saved Searches dropdown | false if strSearchName is not listed in the Saved Search dropdown
	 */
	private Boolean doesSavedSearchesDropdownContainMySearch (String strSearchName) {
		List <SavedSearchRow> allSearches = this.getAllSavedSearchesInSavedSearchesDropdown();
		for (SavedSearchRow search: allSearches) {
			if(search.getSavedSearchName().equals(strSearchName)) { return true; }
		}

		return false;
	}

	 /***
	 * <h1>deleteSavedSearchFromSavedSearchesDropdown</h1> 
	 * <p>purpose: Click on the "X" to delete a search from the Saved Searches dropdown<br>
	 * 	After clicking "X", click "CONFIRM" on the "Confirm Delete Search".<br>
	 * 	Then wait for the "<searchName> deleted!" status message to appear and disappear.<br>
	 * 	If search isn't in dropdown, then fail test. If confirm dialog doesn't appear, fail test.<br>
	 *  If black status message doesn't appear and disappear in a timely manner, fail test.</p>
	 * @param strSearchName = the saved search to select
	 * @return DashboardCommon
	 */
	public DashboardCommon deleteSavedSearchFromSavedSearchesDropdown (String strSearchName) {
		// The search should be in the list
		this.verifySearchInSavedSearchList(strSearchName);

		List <SavedSearchRow> allSearches = this.getAllSavedSearchesInSavedSearchesDropdown();
		for (SavedSearchRow search: allSearches) {
			if(search.getSavedSearchName().equals(strSearchName)) { 
				// So find the search term and delete it
				search.deleteSearchFromSavedSearches()
					.clickConfirmBtn()
					.waitForStatusMessageToAppear()
					.waitForStatusMessageToDisappear();
				return search;
				}
		}

		// We shouldn't get here -- test should have failed above
		return this;
	}

 	/***
	 * <h1>clickOnSavedSearch</h1> 
	 * <p>purpose: Select a search from the Saved Searches dropdown<br>
	 * 	If search isn't in dropdown, then fail test</p>
	 * @param strSearchName = the saved search to select
	 * @return DashboardCommon
	 */
	public DashboardCommon clickOnSavedSearch (String strSearchName) {
		// The search should be in the list
		this.verifySearchInSavedSearchList(strSearchName);

		List <SavedSearchRow> allSearches = this.getAllSavedSearchesInSavedSearchesDropdown();
		for (SavedSearchRow search: allSearches) {
			if(search.getSavedSearchName().equals(strSearchName)) { 
				// So find the search term and click on it
				return search.clickOnSearchInSavedSearches(); }
		}
		
		// We shouldn't get here -- test should have failed above
		return this;
	}
	
	/***
	 * <h1>getFirstSavedSearch</h1>
	 * <p>purpose: Return the first saved search in the Saved Searches dropdown</p>
	 * @return SavedSearchRow
	 */
	public SavedSearchRow getFirstSavedSearch() {
		return this.getFirstRowInSavedSearchesDropdown();
	}
	
	/* ----- Verifications ----- */
	/***
	 * <h1>verifySearchNotInSavedSearchList</h1>
	 * <p>purpose: Verify that a search term is not in the Saved Searches dropdown<br>
	 * 	Fail test if search term is in the Saved Searchesdropdown. <br>
	 * 	(Useful to verify a deletion from the dropdown)</p>
	 * @param strSearchName = search term to verify
	 * @return SavedSearchesDropdown
	 */
	public SavedSearchesDropdown verifySearchNotInSavedSearchList(String strSearchName) {
		if (this.doesSavedSearchesDropdownContainMySearch(strSearchName)) {
			fail("Test Failed: Saved Searches dropdown contains search " + strSearchName); }
		
		return this;
	}

	/***
	 * <h1>verifySearchInSavedSearchList</h1>
	 * <p>purpose: Verify that a search term is in the Saved Searches dropdown<br>
	 * 	Fail test if search term is not in the Saved Searches dropdown after DEFAULT_TIMEOUT
	 * @param searchName = search term to verify
	 * @return SavedSearchesDropdown
	 */	
	public SavedSearchesDropdown verifySearchInSavedSearchList(String searchName) {
		String searchNameXPath = this.savedSearchItemRoot_xpath + "[contains(.,'" + searchName + "')]";
		try {
			this.waitForVisibility(By.xpath(searchNameXPath));
		} catch (TimeoutException t) {
			fail("Test Failed: Saved search did not appear in dropdown");
		}
		return this;
	}
}
