package pages.chargebeeSite.customers;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.GlobalVariables;

/***
 * <h1>CustomersTable</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects for objects on the subscriptions in the table on the "Customers" tab<br>
 * 			   on the Chargebee test site</p>
 */
public class CustomersTable extends ChargebeeCustomersDashboard {
	
	// xpath
	private final String genericRow_xpath = "//div[@class='cb-bootstrap-table-row']/div[@class='cb-bootstrap-table-col'][not(@id='bulk_chkbox')]";
	
	// Other 
	private EventFiringWebDriver driver;
	

	public CustomersTable(EventFiringWebDriver driver) {
		super(driver);
		this.driver   = driver;
	}
	
	/***
	 * <h1>getAllTableRows</h1>
	 * <p>purpose: Get all the results in the Customers table</p>
	 * @return List <CustomersRow>
	 */
	public List <CustomersRow> getAllTableRows() {
		List <CustomersRow> allRows  = new ArrayList <CustomersRow>();

		List <WebElement> numberOfRows = this.findAllOrNoElements(driver, By.xpath(this.genericRow_xpath));

		for (int i =1; i < numberOfRows.size()+1; i++) {
			allRows.add(new CustomersRow(driver, this.genericRow_xpath + "[" + i + "]" )); }

		return allRows;
	}
	
	/***
	 * <h1>getFirstResult</h1>
	 * <p>purpose: Get the first result in the Customers table<br>
	 * 	Fail test if there are no results</p>
	 * @return CustomersRow
	 */
	public CustomersRow getFirstResult() {
		return this.getAllTableRows().stream().findFirst().orElseThrow(NotFoundException::new);
	}
	
	/* ----- verifications ----- */
	/***
	 * <h1>verifyNumberOfRows</h1>
	 * <p>purpose: verify that Customers table contains the expected number of rows</p>
	 * @param expectedNumberOfRows = number of expected rows in Customers table must be >= 0
	 * @return CustomersTable
	 */
	public CustomersTable verifyNumberOfRows(int expectedNumberOfRows) {
		Assert.assertEquals("Test Failed: Wrong number of rows in Customers table", expectedNumberOfRows, this.getAllTableRows().size());
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that Customers table only contains \"" + expectedNumberOfRows + "\" number of rows");
		return this;
	}
}
