@must @registration @level2
Feature: Supplier Registration - Sales Region Screen
BIDSYNC-727: Automation Test for BIDSYNC-180 Sales Regions - Registration Process & Edit


@BIDSYNC-180
Scenario Outline: Test steps to verify Sales Region Screen - Registration flow
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to sales region screen
	Then Select "<region>" and continue registration 
	And verify filter section is populated with Sales regions in user's bid profile
	And my sales region should be populated with edit functionality
	Then select 'Back To Bids' and verify filter section is populated with Sales regions in user's bid profile
	And verify state for all bids matches the profile region
	
Examples:
 	|region			|
 	|US				|
 	|US_CANADA	 	|
 	|STATES_PROVINCE|


 	
@BIDSYNC-180
Scenario: Test steps to verify edit functionality of sales territories
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I complete Supplier Registration
	Then my sales region should be populated with edit functionality
	When I change my sales territories to US and Canada
	And I log out and login back into BidSync Pro as my new BidSync Basic user
	Then verify that my updates persist on my page
	When I change my sales territories to All US States
	And I log out and login back into BidSync Pro as my new BidSync Basic user
	Then verify that my updates persist on my page
	When I change my sales territories to States or Province
	And I log out and login back into BidSync Pro as my new BidSync Basic user
	Then verify that my updates persist on my page
	 	
@BIDSYNC-180
Scenario: Test steps to verify Select All, Clear All, Back & Continue button - Registration flow
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to sales region screen
	When Select "STATES_PROVINCE" as sales Region
	Then verify 'Select All' will select all the states and 'Clear All' will clear them all in Unites States tab
	And verify 'Select All' will select all the province and 'Clear All' will clear them all in Canada tab
	And verify 'Select All' US states and 'Select All' Canada province will add up list view
	And verify 'Select All' will select both us states and canada province and 'Clear All' will clear them off in list view
	And verify state List options in list view and can add states
	
	
@BIDSYNC-180
Scenario: Test steps to verify bid list
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to sales region screen
	Then select given states and province and complete registration
	When I have navigated to the "All Bids" bid tab
	And verify sales region on filter section and bid selection
	
	
@BIDSYNC-180
Scenario: Test steps to verify edit functionality of sales territories-List view Add states
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I complete Supplier Registration
	Then my sales region should be populated with edit functionality
	And I verify list view can add states from the drop down list	
	And I verify if states can be removed in list view

@BIDS-340
Scenario: Test to verify save & continue will not be active without selecting any sales region	
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to none selected as sales region
	Then verify save & continue button is not active
		