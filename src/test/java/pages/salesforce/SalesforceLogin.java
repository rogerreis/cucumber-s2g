package pages.salesforce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import pages.common.pageObjects.BasePageActions;

public class SalesforceLogin extends BasePageActions{

	// xpaths
	private final String username_xpath     = "//input[@id='username']";
	private final String password_xpath  = "//input[@id='password']";
	private final String loginButton_xpath = "//input[@id='Login']";


	public SalesforceLogin (EventFiringWebDriver driver) {
		super(driver);
	}
	
	/* ----- getters ----- */

	
	private WebElement getUsernameInput()  { return findByPresence(By.xpath(this.username_xpath));  }
	private WebElement getPasswordInput() { return findByPresence(By.xpath(this.password_xpath)); }
	private WebElement getLoginBtn()     { return findByPresence(By.xpath(this.loginButton_xpath));     }
	
	/* --------------- Methods --------------- */
	
	/***
	 * <h1>Login</h1>
	 * <p>
	 * purpose: Login
	 * </p>
	 * 
	 * @return BuySpeedLogin
	 */
	public SalesforceLogin Login(String username, String password) {
		this.getUsernameInput().sendKeys(username);
		this.getPasswordInput().sendKeys(password);
		this.getLoginBtn().click();
		return this;
	}
}
