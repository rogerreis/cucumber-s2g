@must @topBar @bidSearch_detail @level3
Feature: Dashboard - Top Bar Search Input Functions Across App - Supplier Dashboard
	This feature file verifies that the Top Bar Search input functions across the app
	BIDSYNC-39: Search Criteria clears after looking at a bid

	In all cases after user enters value into Top Bar input and searches:
	1. User is navigated to All Bids list
	2. Top Bar search term is deleted
	3. Filter Results automatically opens and displays the Top Bar search term as a positive keyword.
	   (No other filters should display in Filter Results).
	4. All Bids list displays search results per the Top Bar search term

	
@BIDSYNC-39
Scenario Outline: User navigates to bid list. From this location, verify Top Bar search flow.
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to the "<bidList>" bid tab
	When I run a search on the Top Bar with keyword "water"
	Then I will verify that the Top Bar search term is blank
	And I will verify the Filter Results section displays a filter with only my Top Bar search term as the keyword
	And I will verify that All Bids is both highlighted and selected, and also displays correct results per the displayed Filter Results search filter
	
	Examples: Bid lists
	| bidList         |
	| New For You     |
	| Your Saved Bids |
	| All Bids        |

@BIDSYNC-39 @BIDSYNC-567
Scenario: User navigates to Bid Details. From this location, verify Top Bar search flow.
	Given I have logged into the Supplier side as user "National FMC"
	And I randomly select a bid from the currently displayed bid list
	And I will verify that all of the objects on the bid details screen have correctly populated
	When I run a search on the Top Bar with keyword "water"
	Then I will verify that the Top Bar search term is blank
	And I will verify the Filter Results section displays a filter with only my Top Bar search term as the keyword
	And I will verify that All Bids is both highlighted and selected, and also displays correct results per the displayed Filter Results search filter

@BIDSYNC-39 @BIDSYNC-577
Scenario: User navigates to All Bids list and closes Filter Results. With this setup, verify Top Bar search flow.
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to the "All Bids" bid tab
	And I close the Filter Results Section
	When I run a search on the Top Bar with keyword "water"
	Then I will verify that the Top Bar search term is blank
	And I will verify that the Filter Results collapsible section is open
	And I will verify the Filter Results section displays a filter with only my Top Bar search term as the keyword
	And I will verify that All Bids is both highlighted and selected, and also displays correct results per the displayed Filter Results search filter
