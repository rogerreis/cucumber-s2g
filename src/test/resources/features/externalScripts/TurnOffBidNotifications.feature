Feature: Turn off bid notifications

@Cleanup @ignore
Scenario: Turn off bid notifications for all users. Then turn on notifications for select accounts
	Given I turn off bid notifications for all users
	Then I turn on bid notifications for user "userName"