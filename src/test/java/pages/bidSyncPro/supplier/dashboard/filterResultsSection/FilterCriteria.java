package pages.bidSyncPro.supplier.dashboard.filterResultsSection;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import pages.common.helpers.DateHelpers;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.RandomHelpers;

/***
 * <h1>Class FilterCriteria</h1>
 * @author dmidura
 * <p>purpose: Use this class to store a search filter.</p>
 */
public class FilterCriteria {
	
	// Elements of a search filter
	private String filterName = "";			// Name of the filter 				
	private List <String> keywords;  		// Any number of keywords			| blank
	private List <String> negativeKeywords; // Any number of negative keywords	| blank
	private List <String> statesProvinces;  // Any number of states/provinces	| blank
											// Each item in list should the state/province abbreviation
											// Ex: "TX"
	private String bidEndDate   = "";		// MM/DD/YYYY 						| blank
	private String bidStartDate = ""; 		// MM/DD/YYYY 						| blank
	private TimeFrameOptions timeFrame;		// NONE | PAST_6_MONTHS | PAST_YEAR | PAST_3_YEARS |  CUSTOM_TIME_FRAME
											// will default to NONE
											// Note: If you set a bidEndDate or bidStartDate, then you shoudl
											// set the timeFrame to CUSTOM_TIME_FRAME
	
	// Other
	private DateHelpers dateHelpers;
	private RandomHelpers randomHelpers;

	public FilterCriteria() {
		this.keywords           = new ArrayList<String>();
		this.negativeKeywords   = new ArrayList<String>();
		this.statesProvinces    = new ArrayList<String>();
		dateHelpers             = new DateHelpers();
		randomHelpers           = new RandomHelpers();
		this.setSelectTimeFrame(TimeFrameOptions.NONE);	// Select Time Frame should default to NONE
	}

	/* ------ getters ------*/
	public String getFilterName()                { return filterName;      }
	public List<String> getKeywords()            { return keywords;        }
	public List<String> getNegativeKeywords()    { return negativeKeywords;}
	public List<String> getStatesProvinces()     { return statesProvinces; } 
	public String getBidStartDate()              { return bidStartDate;    }
	public String getBidEndDate()                { return bidEndDate;      }
	public TimeFrameOptions getSelectTimeFrame() { return this.timeFrame;  }

	/* ------ setters ------*/
	/***
	 * <h1>setFilterName</p>
	 * @param filterName = name you're associating with this filter<br>
	 * 	Should not be blank
	 * @return FilterCriteria
	 */
	public FilterCriteria setFilterName(String filterName) { this.filterName = filterName; return this;}
	/***
	 * <h1>setKeywords</p>
	 * @param keywords = a list of keywords to include in your filter<br>
	 * 	Can be blank
	 * @return FilterCriteria
	 */
	public FilterCriteria setKeywords(List<String> positiveKeywords) {
		if(!keywords.isEmpty())
			keywords.clear();
		this.keywords.addAll(positiveKeywords); 
		return this;}
	/***
	 * <h1>setFilterName</p>
	 * @param negativeKeywords = a list of negative keywords to include in your filter<br>
	 * 	Can be blank
	 * @return FilterCriteria
	 */
	public FilterCriteria setNegativeKeywords(List<String> negativeKeywords) { this.negativeKeywords = negativeKeywords; return this;} 
	/***
	 * <h1>setStatesProvinces</p>
	 * @param statesProvinces = a list of states/provinces to include in your filter<br>
	 * 	format: abbrevation of each state | province<br>
	 * 	ex: "TX"<br>
	 * 	Can be blank
	 * @return FilterCriteria
	 */
	public FilterCriteria setStatesProvinces(List<String> statesProvinces) { this.statesProvinces = statesProvinces; return this;}
	/***
	 * <h1>setBidStartDate</h1>
	 * @param bidStartDate = day to start searching for bids<br>
	 * 	format: MM/DD/YYYY<br>
	 * 	ex: "12/12/2012<br>
	 * 	Can be blank. If set, must set SelectTimeFrame to "CUSTOM_TIME_FRAME"
	 * @return FilterCriteria
	 */
	public FilterCriteria setBidStartDate(String bidStartDate){ this.bidStartDate = bidStartDate; return this;}
	/***
	 * <h1>setBidEndDate</h1>
	 * @param bidEndate = day to end searching for bids<br>
	 * 	format: MM/DD/YYYY<br>
	 * 	ex: "12/12/2012<br>
	 * 	Can be blank. If set, must set SelectTimeFrame to "CUSTOM_TIME_FRAME"
	 * @return FilterCriteria
	 */
	public FilterCriteria setBidEndDate(String bidEndDate){this.bidEndDate = bidEndDate; return this;}
	/***
	 * <h1>setSelectTimeFrame</p>
	 * @param selectTimeFrame = TimeFrameOptions (enum) value to set the time frame to<br>
	 * 	= NONE | CUSTOM_TIME_FRAME | PAST_6_MONTHS | PAST_YEAR | PAST_3_YEARS
	 * @return FilterCriteria
	 */
	public FilterCriteria setSelectTimeFrame(TimeFrameOptions selectTimeFrame) { this.timeFrame = selectTimeFrame; return this;}
	
	/* ----- methods ----- */
	
	/***
	 * <h1>setBidEndDatesFromPreselectedTimeFrame</h1>
	 * <p>purpose: If a preselected time frame was set (Past 6 Months | Past Year | Past 3 Years)<br>
	 * 	then set the bid start and bid end date that would correspond to this value, assuming that<br>
	 * 	the preselection is based on today's date.<br>
	 * 	Note: Workaround currently sets bid end date to year "9999"<br>
	 * 	Ex: time frame = PAST_YEAR and today is 9/13/2018<br>
	 * 	Bid Start = 9/13/2017<br>
	 * 	Bid End = 9/13/9999</p>
	 * @return FilterCriteria
	 */
	public FilterCriteria setBidDatesFromPreselectedTimeFrame() {
		
		// Determine start date based on today's date
		Calendar start = dateHelpers.getCurrentTimeAsCalendar();
		
		switch (this.getSelectTimeFrame()) {
			case PAST_6_MONTHS:
				start.add(Calendar.MONTH, -6);
				break;
			case PAST_YEAR:
				start.add(Calendar.YEAR, -1);
				break;
			case PAST_3_YEARS:
				start.add(Calendar.YEAR, -3);
				break;
			default:
				fail("ERROR: Invalid time frame option");
		} 
		
		String strStart = dateHelpers.getCalendarDateAsString(start);
		
		// Determine end date based on today's MM/DD and with year "9999"
		String strEnd = dateHelpers.getCalendarDateAsString(dateHelpers.getCurrentTimeAsCalendar());
//		smitha : commented to fix Scenario :  Filter Results Collapsible Section Allows for Cancel and Save of Search Names on Save Search Popup
//		strEnd        = strEnd.replace(strEnd.subSequence(6, 10), "9999");

		// And set the dates
		System.out.println(GlobalVariables.getTestID() + " INFO: Based on time frame of \"" + this.getSelectTimeFrame().toString() + "\"");
		System.out.println(GlobalVariables.getTestID() + " Bid Start Date = \"" + strStart + "\"");
		System.out.println(GlobalVariables.getTestID() + " Bid End Date   = \"" + strEnd + "\"");
		this.setBidEndDate(strEnd);
		this.setBidStartDate(strStart);

		return this;
	}
	
	/***
	 * <h1>setRandomFilterName</h1>
	 * <p>purpose: Set a random name for the filter.<br>
	 * 	The name will be a random string of 10 chars</p>
	 * @return FilterCriteria
	 */
	public FilterCriteria setRandomFilterName() {
		this.setFilterName(randomHelpers.getRandomString(10));
		return this;
	}
	 
	/***
	 * <h1>setGenericFilter</h1>
	 * <p>purpose: Set a generic filter (fills out all filter criteria)<br>
	 * 	Generic Filter:<br>
	 *	Keywords = "water", "sewer"<br> 
	 * 	Negative Keywords = "irrigation", "plumbing"<br>
	 * 	States/Provinces = "AL", "TX", "UT"<br>
	 *  Time Frame = "Past Year"<br>
	 *  Filter Name = "Generic Filter"</p>
	 */
	public FilterCriteria setGenericFilter () {
		this.setFilterName("Generic Filter")
			.setGenericKeywords()
			.setGenericNegativeKeywords()
			.setGenericStatesProvinces()
			.setSelectTimeFrame(TimeFrameOptions.PAST_YEAR);
		return this;
	}

	
	/**
	 * <h1>setGenericKeywords</h1>
	 * <p>purpose: Set a list of generic keywords for the filter<br>
	 *	Keywords = "water", "sewer"</p> 
	 * @return FilterCriteria
	 */
	public FilterCriteria setGenericKeywords() {
		List <String> genKeywords =  new ArrayList<String>();
		genKeywords.add("sewer");
		genKeywords.add("water");
		genKeywords.add("test");
		this.setKeywords(genKeywords);		
		return this;
	}

	/**
	 * <h1>setGenericNegativeKeywords</h1>
	 * <p>purpose: Set a list of generic negative keywords for the filter<br>
	 * 	Negative Keywords = "irrigation", "plumbing"</p>
	 * @return FilterCriteria
	 */
	public FilterCriteria setGenericNegativeKeywords() {
		List <String> negKeywords =  new ArrayList<String>();
		negKeywords.add("irrigation");
		negKeywords.add("plumbing");
		this.setNegativeKeywords(negKeywords);		
		return this;
	}
	
	/**
	 * <h1>setGenericNegativeKeywords</h1>
	 * <p>purpose: Set a list of generic states/provinces for the filter<br>
	 * 	States/Provinces = "AL", "TX", "UT"</p>
	 * @return FilterCriteria
	 */
	public FilterCriteria setGenericStatesProvinces() {
		List <String> statesProvinces =  new ArrayList<String>();
		statesProvinces.add("AL");
		statesProvinces.add("TX");
		statesProvinces.add("UT");
		this.setStatesProvinces(statesProvinces);
		return this;
	
	}

	/**
	 * <h1>setBidStartDateToToday()</h1> 
	 * <p>purpose: Set the Bid Start Date to Today (MM/DD/YYYY)</p>
	 * @return FilterCriteria
	 */
	public FilterCriteria setBidStartDateToToday() {
		Calendar startDate = Calendar.getInstance();
		this.setBidStartDate(dateHelpers.getCalendarDateAsString(startDate));
		return this;
	}
	
	/**
	 * <h1>setBidEndDateToToday()</h1> 
	 * <p>purpose: Set the Bid End Date to Today (MM/DD/YYYY)</p>
	 * @return FilterCriteria
	 */
	public FilterCriteria setBidEndDateToToday() {
		Calendar endDate = Calendar.getInstance();
		this.setBidEndDate(dateHelpers.getCalendarDateAsString(endDate));
		return this;
	}

	/**
	 * <h1>setBidStartDateToOneMonthFromToday()</h1> 
	 * <p>purpose: Set the Bid Start Date to one month from today (MM/DD/YYYY)</p>
	 * @return FilterCriteria
	 */
	public FilterCriteria setBidStartDateToOneMonthFromToday() {
		Calendar startDate = Calendar.getInstance();
		startDate.add(Calendar.MONTH, +1);
		this.setBidStartDate(dateHelpers.getCalendarDateAsString(startDate));
		return this;
	}

	/**
	 * <h1>setBidEndDateToOneMonthFromToday()</h1> 
	 * <p>purpose: Set the Bid End Date to one month from today (MM/DD/YYYY)</p>
	 * @return FilterCriteria
	 */
	public FilterCriteria setBidEndDateToOneMonthFromToday() {
		Calendar endDate = Calendar.getInstance();
		endDate.add(Calendar.MONTH, +1);
		this.setBidEndDate(dateHelpers.getCalendarDateAsString(endDate));
		return this;
	}
	
	/***
	 * <h1>clearBidStartDate</h1>
	 * <p>purpose: Clear out the value of bid start date, and set to ""</p>
	 * @return FilterCriteria
	 */
	public FilterCriteria clearBidStartDate() {
		this.bidStartDate = "";
		return this;
	}

	/***
	 * <h1>clearBidEndDate</h1>
	 * <p>purpose: Clear out the value of bid end date, and set to ""</p>
	 * @return FilterCriteria
	 */
	public FilterCriteria clearBidEndDate() {
		this.bidEndDate = "";
		return this;
	}

	/***
	 * <h1>clearKeywords</h1>
	 * <p>purpose: Delete all values in keywords. List will now be empty</p>
	 * @return FilterCriteria
	 */
	public FilterCriteria clearKeywords() {
		this.getKeywords().clear();
		return this;
	}
	
	/***
	 * <h1>clearNegativeKeywords</h1>
	 * <p>purpose: Delete all values in negative keywords. List will now be empty</p>
	 * @return FilterCriteria
	 */
	public FilterCriteria clearNegativeKeywords() {
		this.getNegativeKeywords().clear();
		return this;
	}

	/***
	 * <h1>clearStatesProvinces</h1>
	 * <p>purpose: Delete all values in states/provinces. List will now be empty</p>
	 * @return FilterCriteria
	 */
	public FilterCriteria clearStatesProvinces() {
		this.getStatesProvinces().clear();
		return this;
	}

	@Override
    public boolean equals(Object o) {	
		FilterCriteria filter = (FilterCriteria) o;
		
		// Must be equal for each filter item
		if (!this.getFilterName().equals(filter.getFilterName())             | 
			!this.getKeywords().equals(filter.getKeywords())                 |
			!this.getNegativeKeywords().equals(filter.getNegativeKeywords()) |
			!this.getStatesProvinces().equals(filter.getStatesProvinces())   |
			!this.getSelectTimeFrame().equals(filter.getSelectTimeFrame())   |
			!this.getBidEndDate().equals(filter.getBidEndDate())             |
			!this.getBidStartDate().equals(filter.getBidStartDate())          ) {
			return false; }

		return true;
	}
	
	/**
	 * <h1>printFilterCriteriaToScreen</h1>
	 * <p>purpose: Use this method to print out the filter criteria onto the screen</p>
	 * @return FilterCriteria
	 */
	public FilterCriteria printFilterCriteriaToScreen() {
		System.out.println ("INFO: Filter Criteria is:" +
			"\nFilter Name      = " + this.getFilterName() +
			"\nKeywords         = " + this.getKeywords().toString() +
			"\nNeg Keywords     = " + this.getNegativeKeywords().toString() +
			"\nStates/Provinces = " + this.getStatesProvinces().toString() +
			"\nTime Frame       = " + this.getSelectTimeFrame().toString());
		
		if (this.getBidStartDate() != null) {
			System.out.println(GlobalVariables.getTestID() + " \nBid Start Date   = \"" + this.getBidStartDate().toString() + "\"" ); }
		if (this.getBidEndDate() != null) {
			System.out.println(GlobalVariables.getTestID() + " \nBid End Date     = \"" + this.getBidEndDate().toString() + "\"");}
		
		return this;
	}

}
