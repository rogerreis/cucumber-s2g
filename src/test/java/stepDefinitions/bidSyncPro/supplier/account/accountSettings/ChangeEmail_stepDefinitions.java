package stepDefinitions.bidSyncPro.supplier.account.accountSettings;
 
import core.*;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import pages.bidSyncPro.supplier.account.accountSettings.ChangeEmailModal_Step1;
import pages.bidSyncPro.supplier.account.accountSettings.ChangeEmailModal_Step2;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.toBeDeprecated.StepDefinition;
import pages.mailinator.Mailinator;

/***
 * <h1>Class ChangeEmail_stepDefinitions</h1>
 * <p>details: This class houses test steps to test the Change Email Modals</p>
 * @author jlara
 * <p>TODO: Refactor this class</p>
 */
public class ChangeEmail_stepDefinitions extends StepDefinition {
  
	private ChangeEmailModal_Step1 changeEmailModal_Step1;
	private ChangeEmailModal_Step2 changeEmailModal_Step2;
	private Mailinator mailinatorScreen;
	private HelperMethods helperMethods;
	
	public ChangeEmail_stepDefinitions(CoreAutomation automation) throws Throwable {
		super(automation);
		changeEmailModal_Step1 = new ChangeEmailModal_Step1(driver);
		changeEmailModal_Step2 = new ChangeEmailModal_Step2(driver);
		mailinatorScreen = new Mailinator(driver);
		helperMethods = new HelperMethods();
		
	} // end of constructor

	
		
	/* ------------ Test Steps ---------------- */

	@Given("^I will use Email \"(.*)\" to check if the Email Availability Spinner is Visible$")
	public void IwilluseEmailtocheckiftheEmailAvailabilitySpinnerisVisible(String strEmail) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify the that the Email Availability Spinner is visible");
		changeEmailModal_Step1.sendKeysEmailTextBox(strEmail);
		
		changeEmailModal_Step1.getConfirmEmailTextBox().click();
		changeEmailModal_Step1.getCheckEmailAvailabilitySpinner();
		verifyInnerTextExactMatch(changeEmailModal_Step1.getCheckEmailAvailabilityLabel(),"...checking email availability");
		
		changeEmailModal_Step1.getEmailTextBox().clear();
	}
	
	@Given("^I will use Email \"(.*)\" to check the Valid Email Error$")
	public void IwilluseInvalidEmailtochecktheValidEmailError(String strEmail) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify the that Valid Email Error is visible");
		changeEmailModal_Step1.sendKeysEmailTextBox(strEmail);
		
		changeEmailModal_Step1.getConfirmEmailTextBox().click();
		verifyInnerTextExactMatch(changeEmailModal_Step1.getEnterValidEmailErrorLabel(),"Please enter a valid email");
		changeEmailModal_Step1.getEmailTextBox().clear();
	}
	
	
	@Given("^I will use Emails \"(.*)\" and \"(.*)\" to check the Mismatch Email Error$")
	public void IwilluseEmailstochecktheMismatchEmailError(String strEmail1,String strEmail2) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify the that Mismatch Email Error is visible");
		changeEmailModal_Step1.sendKeysEmailTextBox(strEmail1);
		
		changeEmailModal_Step1.sendKeysConfirmEmailTextBox(strEmail2);
		verifyInnerText(changeEmailModal_Step1.getEmailsDoNotMatchErrorLabel(),"Emails do not match");
		changeEmailModal_Step1.getEmailTextBox().clear();
		changeEmailModal_Step1.getConfirmEmailTextBox().clear();
	}
	
	
	
	@Given("^I will use a random email to navigate to Change Email Modal Step 2$")
	public void IwilluseARandomEmailtonavigatetoChangeEmailModalStep2() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify the that Valid path navigates to Change Email Modal Step 2");
        String email = new HelperMethods().generateNewEmail();
        GlobalVariables.storeObject("New Email", email);
        changeEmailModal_Step1.sendKeysEmailTextBox(email);
		
		changeEmailModal_Step1.sendKeysConfirmEmailTextBox(email);
		changeEmailModal_Step1.clickConfirmButton();
	}
	
	/***
	 * <h1>I have Account Settings Change Email Step 2 in Focus</h1>
	 * <p>Purpose: Verify that the Account Settings Change Email Step 2 is in Focus</p>
	 * @return None
	 */
	@Given("^I have clicked the Close Button on Account Settings Change Email Step 2$")
	public void IhaveclickedtheCloseButtononAccountSettingsChangeEmailStep2() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify the Close Button on Account Settings Change Email Step 2 can be clicked");

		changeEmailModal_Step2.clickCloseButton();
	}
	
	@Then("^I have received that Account Settings Change Email Emails the new email in Mailinator Inbox$")
	public void IhavereceivedthatAccountSettingsChangeEmailEmailsTheNewEmailinMailinatorInbox() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify the Account Settings Change Email Emails are in the Mailinator Inbox");
		
		String email = GlobalVariables.getStoredObject("New Email");

		// 1. Navigate to the correct Mailinator inbox
		// 2. Open Change Email
		// 3. Verify Email (note: no greeting for this email)
		// 4. Click on token
		mailinatorScreen.navigateToMailinatorInbox(email)
			.openChangeEmailEmail()
			.verifyBodyTextIsCorrect()
			.verifyFooterTextIsCorrect()
			.clickOnTokenURLInEmail()
			.switchToWindow("S2G");
	}
	

}

