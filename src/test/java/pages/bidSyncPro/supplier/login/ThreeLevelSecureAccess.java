package pages.bidSyncPro.supplier.login;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.bidSyncPro.supplier.common.LocalBrowserStorageBidSyncPro;
import pages.common.helpers.DateHelpers;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.helpers.RandomHelpers;

/***
 * <h1>Class ThreeLevelSecureAccess</h1>
 * @author dmidura
 * <p>details: This class houses methods to test the three levels of security (public, priviledged, secure)<br>
 * 	that BidSync Pro incorporates for user session management.</p>
 */
public class ThreeLevelSecureAccess {
	
	// Page Access
	private List <String> publicPages     = new ArrayList<String>();
	private List <String> privilegedPages = new ArrayList<String>();
	private List <String> securePages     = new ArrayList<String>();
	
	// Other
	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;
	private DateHelpers   dateHelpers;
	private RandomHelpers randomHelpers;
	private LocalBrowserStorageBidSyncPro browserStorage;
	
	// URLs -- for direct navigation tests
	
	// Public Page URLs
	private final String loginURL = System.getProperty("supplierBaseURL") + "login";
	
	// Privileged Page URLs
	private final String newForYouURL           = System.getProperty("supplierBaseURL") + "dashboard/new-bids";
	private final String yourSavedBidsURL       = System.getProperty("supplierBaseURL") + "dashboard/my-bids";
	private final String allBidsURL             = System.getProperty("supplierBaseURL") + "dashboard/all-bids";
	private final String manageSubscriptionsURL = System.getProperty("supplierBaseURL") + "admin/manage-subscriptions";
	
	// Secure Page URLs
	private final String companyProfileURL       = System.getProperty("supplierBaseURL") + "admin/company-profile";
	private final String manageQualificationsURL = System.getProperty("supplierBaseURL") + "admin/agency-interaction";
	private final String manageUsersURL          = System.getProperty("supplierBaseURL") + "admin/users";
	private final String myInfoURL               = System.getProperty("supplierBaseURL") + "admin/my-profile";
		
			
	public ThreeLevelSecureAccess(EventFiringWebDriver driver) {
		this.driver    = driver;
		helperMethods  = new HelperMethods();
		dateHelpers    = new DateHelpers();
		randomHelpers  = new RandomHelpers();
		browserStorage = new LocalBrowserStorageBidSyncPro(driver);
		
		this.setPrivilegedPages();
		this.setPublicPages();
		this.setSecurePages();
	}
	
	/* ----- helpers ------ */
	
    
	// Define access level for each page
	private ThreeLevelSecureAccess setPublicPages() {

		// Bid Details - for public bids
		// dmidura: Phase 2, so not currently included

		return this;
	}
	
	private ThreeLevelSecureAccess setPrivilegedPages() {
		// Bid tabs
		privilegedPages.add(this.newForYouURL);
		privilegedPages.add(this.yourSavedBidsURL);
		privilegedPages.add(this.allBidsURL);

		return this;
	}
	
	/***
	 * <h1>addBidDetailsPrivilegedPages</h1>
	 * <p>purpose: Grab all URLs to Bid Detail pages that my user can <br>
	 * 	access. Add these pages to the privilegedPages list<br>
	 * 	Note: we don't do this inside of setPrivilegedPages b/c this method<br>
	 * 	can really slow down the automation. <br>
	 * 	Additional Note: User must already have ability to view privileged pages<br>
	 * 	in order to grab Bid Details for their bids</p>
	 * @return
	 */
	private ThreeLevelSecureAccess addBidDetailsPrivilegedPages(){
		// Bid Details - for subscription plan bids
		// Test a sampling of the Bid Details pages. These should adhere to both the subscription plan
		// and to the three level of security authentication
		driver.navigate().to(this.allBidsURL);
		new CommonTopBar(driver).searchFor("water")
				.allVisibleBids().forEach(bid-> {
			if(!bid.isUpgradePlan()) {
				this.privilegedPages.add(bid.getBidURL()); 
			}});
		
		return this;
	}

	private ThreeLevelSecureAccess setSecurePages() {
		// Company Profile - parent and children
		securePages.add(this.companyProfileURL);
		
		// Agency Interaction > Manage Qualifications
		securePages.add(this.manageQualificationsURL);
		
		// Manage Users
		securePages.add(this.manageUsersURL);
		
		// My Info - parent and children
		securePages.add(this.myInfoURL);

		// Manage Subscriptions 
		// dmidura: Manage Subscriptions will be subdivided into privileged and secure
		// children pages in Phase 2. For Phase 1.5, parent and all children are secure
		securePages.add(this.manageSubscriptionsURL);
	
		return this;
	}
	
	/***
	 * <h1>navigateToPagesVerifyLogin</h1>
	 * <p>purpose: Navigate to each page in the list of URLs. Fail test
	 * 	if page is re-routed to login</p>
	 * @param pagesToNavigate = list of all page URLs to navigate to
	 * @return ThreeLevelSecureAccess
	 */
	private ThreeLevelSecureAccess navigateToPagesVerifyNoLogin(List <String> pagesToNavigate) {

		// Navigate to each page in the list
		for (String url : pagesToNavigate) {
			System.out.println(GlobalVariables.getTestID() + " INFO: Navigating to " + url);
			driver.navigate().to(url);
			
			// And make sure that screen is not re-routed to login
			// (need to wait here to make sure page isn't redirected)
			helperMethods.addSystemWait(3);
			if (driver.getCurrentUrl().contains(this.loginURL)) {
				fail("Test Failed: Browser navigated to login screeen "); }

		}
		return this;
	}

	/***
	 * <h1>navigateToPagesVerifyLogin</h1>
	 * <p>purpose: Navigate to each page in the list of URLs. Fail test
	 * 	if page is not re-routed to login</p>
	 * @param pagesToNavigate = list of all page URLs to navigate to
	 * @return ThreeLevelSecureAccess
	 */
	private ThreeLevelSecureAccess navigateToPagesVerifyLogin(List <String> pagesToNavigate) {
		// Navigate to each page in the list
		for (String url : pagesToNavigate) {
			driver.get(url);
			
			// And make sure that screen is re-routed to login
			new WebDriverWait(driver, 3).until(ExpectedConditions.urlContains(this.loginURL));
		}
		return this;
	}
	
	/* ----- methods ----- */
	
	/***
	 * <h1>provideLoginCredentials</h1>
	 * <p>purpose: Enter login credentials for your current user<br>
	 * 	(User credentials come from SupplierLogin.thisUser.getAccountReference()<br>
	 * 	static. Make sure the static is set in your test to use this method)</p>
	 * @return
	 */
	public ThreeLevelSecureAccess provideLoginCredentials() {
		new SupplierLogin(driver).loginAsUser(SupplierLogin.thisUser.getAccountReference())
				.waitForPageLoad();
		return this;
	}
	
	/***
	 * <h1>closeBrowserWindow</h1>
	 * <p>purpose: Make driver close the current browser window<br>
	 * 	(In Three Level Secure Access tests, this method is necessary to <br>
	 * 	ensure that "Remember Me" status persists between sessions)<br>
	 * 	After closing the browser, we'll create a new CoreAutomation and grab the new driver.<br>
	 * 	This new driver should be saved at the step definition level (so we can open<br>
	 * 	another browser session)</p>
	 * @return CoreAutomation
	 */
	public ThreeLevelSecureAccess closeBrowserWindow() {
		
		// Let the backend catch up -- waiting for the partialToken to appear
		browserStorage.waitForPartialToken();
		
		// open a new blank browser tab (so we don't kill the current driver session with our local storage)
		JavascriptExecutor js = (JavascriptExecutor) driver;  
		js.executeScript("window.open();");
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs.get(0));
		
		// now close the browser tab that was running our first test (i.e. not the blank tab)
		driver.close();
		return this;
	}

	/***
	 * <h1>waitForTokenToExpireFromLoginScreen</h1>
	 * <p>purpose: Wait 30 minutes for the token to expire</p>
	 * @return ThreeLevelSecureAccess
	 */
	public ThreeLevelSecureAccess waitForTokenToExpire() {

		// Let the backend catch up -- waiting for the partialToken to appear
		browserStorage.waitForPartialToken();

		// Make sure that we see our local storage values
		browserStorage.printLocalStorage();
		if (!browserStorage.isRememberMeSetInLocalStorage()) { fail("ERROR: rememberMe is not in local storage");}

		/* -----  Test hack ----- */
		// Test hack
		// here we delete the TOKEN from local storage
		browserStorage.deleteToken();
		/*------------------------*/
		
		// TODO: comment out Test hack and uncomment below when all the three-level access stuff is ready
		// This is the real test below:
		// We are actually waiting 31 minutes to be safe
		// System.out.println(GlobalVariables.getTestID() + " INFO: Waiting 31 minutes for token to expire");
		// helperMethods.addSystemWait(31, TimeUnit.MINUTES);
		browserStorage.printLocalStorage();

		return this;
	}
	
	/***
	 * <h1>waitForTokenToExpireFromLoginScreen</h1>
	 * <p>purpose: Navigate to the login screen. Wait 30 minutes for the token to expire</p>
	 * @return ThreeLevelSecureAccess
	 */
	public ThreeLevelSecureAccess waitForTokenToExpireFromLoginScreen() {

		// Navigate to the ARGO login, so we can access local storage for ARGO
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs.get(0));

		// Let the backend catch up -- waiting for the partialToken to appear
		driver.navigate().to(this.loginURL);
		browserStorage.waitForPartialToken();

		// Make sure that we see our local storage values
		browserStorage.printLocalStorage();
		if (!browserStorage.isRememberMeSetInLocalStorage()) { fail("ERROR: rememberMe is not in local storage");}

		/* -----  Test hack ----- */
		// here we delete the TOKEN from local storage
		browserStorage.deleteToken();
		/*------------------------*/
		
		// TODO: comment out Test hack and uncomment below when all the three-level access stuff is ready
		// This is the real test below:
		// System.out.println(GlobalVariables.getTestID() + " INFO: Waiting 30 minutes for token to expire");
		// helperMethods.addSystemWait(30, TimeUnit.MINUTES);
		// if (!browserStorage.isTokenExpired()){ fail("Test Failed: Token did not expire after 30 minutes");}
		browserStorage.printLocalStorage();

		return this;
	}


	/***
	 * <h1>randomlySelectSecurePageAndNavigateThere</h1>
	 * <p>purpose: Select a secure page at random and then navigate there</p>
	 * @return ThreeLevelSecureAccess
	 */
	public ThreeLevelSecureAccess randomlySelectSecurePageAndNavigateThere() {
		int index = randomHelpers.getRandomInt(this.securePages.size());
		driver.navigate().to(this.securePages.get(index));
		return this;
	}

	/***
	 * <h1>navigateToDashboardNewForYou</h1>
	 * <p>purpose: Navigate to the dashboard>New For You bid list</p>
	 * @return ThreeLevelSecureAccess 
	 */
	public ThreeLevelSecureAccess navigateToDashboardNewForYou() {
		driver.navigate().to(this.newForYouURL);
		return this;
	}

	/***
	 * <h1>changePartialTokenExpirationInLocalStorageSecondsFromNow</h1>
	 * <p>purpose: Change the partialTokenExpiration in local storage to be seconds from<br>
	 * 	the current time.</p>
	 * @param seconds = seconds from the current time (can be positive or negative)<br>
	 * 	Ex: -1 is 1 second in the past from the current time</p>
	 * 	Ex: 10 is 10 seconds in the future from the current time</p>
	 * @return ThreeLeveSecureAccess
	 */
	public ThreeLevelSecureAccess changePartialTokenExpirationInLocalStorageSecondsFromNow(int seconds) {
		browserStorage.printLocalStorage();
		Calendar partialTokenExp = Calendar.getInstance();
		partialTokenExp.add(Calendar.SECOND, seconds);
		
		browserStorage.setPartialTokenExpiration(partialTokenExp);
		browserStorage.printLocalStorage();
		
		return this;
	}

	
	/* ----- verifications ----- */
	
	/***
	 * <h1>verifyCredentialsNotRequiredForPublicPages</h1>
	 * <p>purpose: Navigate to each public page and verify that user is not re-routed to a login</p>
	 * @return ThreeLevelSecureAccess
	 */
	public ThreeLevelSecureAccess verifyCredentialsNotRequiredForPublicPages() {
		this.navigateToPagesVerifyNoLogin(this.publicPages);

		return this;
	}
	
	/***
	 * <h1>verifyCredentialsRequiredForPrivilegedPages</h1>
	 * <p>purpose: Navigate to each privileged page and verify that user is re-routed to a login</p>
	 * @return ThreeLevelSecureAccess
	 */
	public ThreeLevelSecureAccess verifyCredentialsRequiredForPrivilegedPages() {
		// Note: Can't verify credentials required to navigate to any of the Bid Details pages here
		// because in order to locate bids we need to be logged in
		this.navigateToPagesVerifyLogin(this.privilegedPages);

		return this;
	}

	/***
	 * <h1>verifyCredentialsNotRequiredForPrivilegedPages</h1>
	 * <p>purpose: Navigate to each privileged page and verify that user is not re-routed to a login</p>
	 * @return ThreeLevelSecureAccess
	 */
	public ThreeLevelSecureAccess verifyCredentialsNotRequiredForPrivilegedPages() {
		this.addBidDetailsPrivilegedPages();
		this.navigateToPagesVerifyNoLogin(this.privilegedPages);

		return this;
	}
	
	/***
	 * <h1>verifyCredentialsRequiredForSecurePages</h1>
	 * <p>purpose: Navigate to each secure page and verify that user is re-routed to a login</p>
	 * @return ThreeLevelSecureAccess
	 */
	public ThreeLevelSecureAccess verifyCredentialsRequiredForSecurePages() {
		this.navigateToPagesVerifyLogin(this.securePages);

		return this;
	}
	
	/***
	 * <h1>verifyCredentialsNotRequiredForSecurePages</h1>
	 * <p>purpose: Navigate to each secure page and verify that user is not re-routed to a login</p>
	 * @return ThreeLevelSecureAccess
	 */
	public ThreeLevelSecureAccess verifyCredentialsNotRequiredForSecurePages() {
		this.navigateToPagesVerifyNoLogin(this.securePages);

		return this;
	}

	/***
	 * <h1>verifyUserWhoIsNotRememberMe</h1>
	 * <p>purpose: A user who is not in Remember Me mode should not have any of the following<br>
	 * 	in their browser's local storage:<br>
	 * 	1. rememberMe = true<br>
	 * 	2. any partialToken<br>
	 * 	3. any partialTokenExpiration<br>
	 * 	Fail test if any of these are located in local storage</p>
	 * @return ThreeLevelSecure Access
	 */
	public ThreeLevelSecureAccess verifyUserWhoIsNotRememberMe() {
		// User who is not Remember Me should not see the following in local storage
		// 1. rememberMe = true
		// 2. any partialToken
		// 3. any partialTokenExpiration
		
		if (browserStorage.getRememberMe() != null) {
			fail ("Test Failed: Remember Me is currently \"true\" in local storage"); }
		
		if (browserStorage.getPartialToken() != null) {
			fail ("Test Failed: partialToken exists in local storage as " + browserStorage.getPartialToken()); }

		if (browserStorage.getPartialTokenExpiration() != null) {
			fail ("Test Failed: partialTokenExpiration exists in local storage as " + browserStorage.getPartialTokenExpiration().getTimeInMillis()); }
		
		return this;
	}
	
	
	/***
	 * <h1>verifyRememberMeSetTo30DaysFromNowInBrowserLocalStorage</h1>
	 * <p>purpose: Verify that the partialTokenExpiration was set to 30 days<br>
	 * 	from the current time. (Because our current time may not match the time<br>
	 * 	when the partialExpirationToken was set, we're actually testing that it<br>
	 * 	is within 10 minutes of our calculated time). Fail test if the local storage<br>
	 * 	value of partialTokenExpiration is outside of the 10 minute calculated window<br>
	 * 	of one month from today</p>
	 * @return ThreeLeveSecureAccess
	 */
    public ThreeLevelSecureAccess verifyRememberMeSetTo30DaysFromNowInBrowserLocalStorage() {
    	Calendar oneMonth = Calendar.getInstance();
    	int days = 24 * 30;
    	oneMonth.add(Calendar.HOUR, days);
    	Calendar partialTokenExp = browserStorage.getPartialTokenExpiration();
    	
    	// The partialTokenExpiration should be within 10 minutes of our calculated value
    	// or something wrong happened
    	Calendar lowerBounds = (Calendar) oneMonth.clone();
    	Calendar upperBounds = (Calendar) oneMonth.clone();
    	lowerBounds.add(Calendar.MINUTE, -10);
    	upperBounds.add(Calendar.MINUTE, 10);
    	
    	if (partialTokenExp.before(lowerBounds) | partialTokenExp.after(upperBounds)) {
    		System.out.println(GlobalVariables.getTestID() + " Lower = " + lowerBounds.getTimeInMillis() + " Upper = " + upperBounds.getTimeInMillis());
			String oneMonthFormatted        = dateHelpers.getCalendarDateAsString(oneMonth);
			String partialTokenExpFormatted = dateHelpers.getCalendarDateAsString(partialTokenExp);
    		System.out.println(GlobalVariables.getTestID() + " INFO: partialToken = " + partialTokenExpFormatted + ", and oneMonth = " + oneMonthFormatted);
    		System.out.println(GlobalVariables.getTestID() + " INFO: partialToken = " + partialTokenExp.getTimeInMillis() + ", and oneMonth = " + oneMonth.getTimeInMillis());
    		fail("Test Failed: partialTokenExpiration = \"" + partialTokenExpFormatted + "\", but one month from today is = \"" + oneMonthFormatted + "\"");
    	}

    	
    	return this;
    }	
}
