package stepDefinitions.bidSyncPro.supplier.companySettings.manageUsers;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import data.bidSyncPro.registration.RandomChargebeeUser;

import org.junit.Assert;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.companySettings.companyProfile.CompanyProfile;
//import pages.bidSyncPro.supplier.companySettings.companyProfile.CompanyProfile;
import pages.bidSyncPro.supplier.companySettings.manageUsers.ManageUsers;
import pages.bidSyncPro.supplier.companySettings.manageUsers.WaitingForApproval;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

public class CompanySettings_stepDefinitions {
    
    private EventFiringWebDriver driver;
    
    public CompanySettings_stepDefinitions(CoreAutomation automation) {
        this.driver = automation.getDriver();
    }
    
    @Then("^The user cannot view the company tabs$")
    public void theUserCannotViewTheCompanyTabs() {
        RandomChargebeeUser companyJoiner = GlobalVariables.getStoredObject("CompanyJoiner");
        boolean isCompanyProfileAvailable =  new SupplierLogin(driver)
                .loginWithCredentials(companyJoiner.getEmailAddress(), companyJoiner.getPassword())
                .openCompanySettingsDropDown()
                .openCompanyProfile()
                .isTheUserAllowedToViewThisPage();
        boolean isManageUsersAvailable = new CompanyProfile(driver)
                .openManageUsersTab()
                .isTheUserAllowedToViewThisPage();
        boolean isAgencyInteractionAvailable = new ManageUsers(driver)
                .openAgencyInteractionTab()
                .isTheUserAllowedToViewThisPage();
        Assert.assertTrue("The user can view the Company Profile tab when he shouldn't be able to.", isCompanyProfileAvailable);
        Assert.assertTrue("The user can view the Agency Interaction tab when he shouldn't be able to.", isAgencyInteractionAvailable);
        
        // TODO dmidura: Functionality is currently broken per bug
        // BIDSYNC-425: ""User Management" should not display infinite spinning wheel for user not yet accepted to company
        // Uncomment below after fix for BIDSYNC-425
//        Fixed it as part of BIDSYNC-810
        Assert.assertTrue("The user can view the Manage Users tab when he shouldn't be able to.", isManageUsersAvailable);
    }
    
    @And("^The user can resend a request to join the company$")
    public void theUserCanResendARequestToJoinTheCompany() {
        RandomChargebeeUser companyOwner = GlobalVariables.getStoredObject("StoredUser");
        WaitingForApproval waitingForApproval = new WaitingForApproval(driver)
                .resendRequestToJoin();
        String companyToJoin = companyOwner.getCompany().getName();
        String message = String.format("User does not have a pending request to join company '%s'", companyToJoin);
        Assert.assertEquals(message, waitingForApproval.getCompanyTheUserWantsToJoin(), companyToJoin);
    }
    
    @And("^The user can change to \"([^\"]*)\" company$")
    public void theUserCanChangeToCompany(String companyName) {
        String actualCompany = new WaitingForApproval(driver)
                .changeCompany(companyName)
                .getCompanyTheUserWantsToJoin();
        String message = String.format("User does not have an active request to join company '%s'", companyName);
        Assert.assertEquals(message, companyName, actualCompany);
    }
}
