package pages.chargebeeSite.subscriptions.subscriptionDetails.changeSubscriptionFlow;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.chargebeeSite.subscriptions.subscriptionDetails.SubscriptionDetails;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>PreviewOfChangesVerificationPopup</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects that appears the "Preview of Changes" popup<br>
 * 	that appears under the Subscriptions->Subscription Details->Change Subscription screen when the user clicks on "Change Subscription"</p>
 */
public class PreviewOfChangesVerificationPopup extends BasePageActions {
	// xpaths -- add as needed
	private final String confirmBtn_id = "cb-popup-confirm-submit";
	
	// Other
	private EventFiringWebDriver driver;

	public PreviewOfChangesVerificationPopup (EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	/* ----- getters ----- */
	private WebElement getConfirmBtn () { return findByVisibility(By.id(this.confirmBtn_id)); }
	
	/* ----- methods ----- */
	/***
	 * <h1>clickConfirmBtn</h1>
	 * <p>purpose: Click the confirm button on the "The subscription will be cancelled immediately" popup </p>
	 * @return
	 */
	public SubscriptionDetails clickConfirmBtn() {
		this.getConfirmBtn().click();
		return new SubscriptionDetails(driver);
	}
	
}
