package pages.bidSyncPro.supplier.companySettings.manageUsers;

import pages.bidSyncPro.supplier.companySettings.CompanySettingsCommon;
import pages.common.helpers.HelperMethods;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitingForApproval extends CompanySettingsCommon {
    
    private HelperMethods helperMethods;
    
    // xpaths
    private final String resendRequestButton_xpath          = "//button[contains(@class, 'resendRequest')]";
    private final String changeCompanyButton_xpath          = "//button[contains(@class, 'changeCompany')]";
    private String companyDropdownModalOptionTemplate_xpath = "//span[contains(., '%s')]";
    private final String companyRequestText_xpath           = "//div[contains(@class, 'CompanyRequestText')]";
    private final String requestHeaderLabel_xpath					= "//h3[contains(text(),'Request')]";
    private final String requestSentLabel_xpath					= "//h3[contains(text(),'Request Sent')]";
	
    
    
    // ids
    private final String waitingForApprovalLabel_id         = "matCompanyRequestTitle";
    private final String joinNewCompanyModalButton_id       = "CreateNextButton2";
    private final String changeCompanyModalInput_id         = "company";
    private final String nextModalButton_id                 = "nextButton2";
    private final String sendRequestModalButton_id          = "sendRequestButton";
    private final String closeModalButton_id                = "nextButton";
    
    public WaitingForApproval(EventFiringWebDriver driver) {
        super(driver);
        helperMethods = new HelperMethods();
    }
    
    /* ----- getters ----- */
    private WebElement getResendRequestButton()       { return findByVisibility(By.xpath(resendRequestButton_xpath));      }
    private WebElement getChangeCompanyButton()       { return findByVisibility(By.xpath(this.changeCompanyButton_xpath)); }
    private WebElement getJoinNewCompanyModalButton() { return findByVisibility(By.id(this.joinNewCompanyModalButton_id)); }
    private WebElement getChangeCompanyModalInput()   { return findByVisibility(By.id(this.changeCompanyModalInput_id));   }
    private WebElement getNextModalButton()           { return findByVisibility(By.id(this.nextModalButton_id));           }
    private WebElement getSendRequestModalButton()    { return findByVisibility(By.id(this.sendRequestModalButton_id));    }
    private WebElement getCloseModalButton()          { return findByVisibility(By.id(this.closeModalButton_id));          }
    private WebElement getCompanyRequestText()        { return findByVisibility(By.xpath(this.companyRequestText_xpath));  }
    
    /* ----- methods ----- */

    public boolean areWeHere() {
        try {
        	new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Not able to find 'Waiting for Approval' page")
        	.until(ExpectedConditions.and(ExpectedConditions.visibilityOfElementLocated(By.id(waitingForApprovalLabel_id)),
        									ExpectedConditions.visibilityOfElementLocated(By.xpath(requestHeaderLabel_xpath))));
        	if(doesPageObjectExist(driver, By.xpath(requestSentLabel_xpath)))
        		return true;
        	else
        		return false;
//            driver.findElement(By.id(this.waitingForApprovalLabel_id));
            
        } catch (NoSuchElementException e) {
            return false;
        }
    }
    
    public WaitingForApproval resendRequestToJoin() {
        getResendRequestButton().click();
        return this;
    }
    
    public WaitingForApproval changeCompany(String companyName) {
        getChangeCompanyButton().click();
        
        waitForVisibility(By.id(this.joinNewCompanyModalButton_id), 10);
        getJoinNewCompanyModalButton().click();
        
        waitForVisibility(By.id(this.changeCompanyModalInput_id), 10);
        getChangeCompanyModalInput().sendKeys(companyName);
        
        String companyModalXpath = String.format(this.companyDropdownModalOptionTemplate_xpath, companyName);
        waitForVisibility(By.xpath(companyModalXpath));
        helperMethods.addSystemWait(1);
        driver.findElement(By.xpath(companyModalXpath)).click();
        
        waitForVisibility(By.id(this.nextModalButton_id));
        getNextModalButton().click();
        
        waitForVisibility(By.id(this.sendRequestModalButton_id));
        getSendRequestModalButton().click();
        
        waitForVisibility(By.id(this.closeModalButton_id));
        getCloseModalButton().click();
        
        // wait for name of company changes.  Consider finding a way to explicitly wait for this
        helperMethods.addSystemWait(2);
        
        return this;
    }
    
    public String getCompanyTheUserWantsToJoin() {
        String requestText = getCompanyRequestText().getText();
        if (requestText.contains("REQUEST SENT")) {
            return requestText.split(" ")[8].replace(".", "");
        } else  if (requestText.contains("REQUEST DECLINED")) {
            return requestText.split(" ")[5].replace(".", "");
        } else {
            Assert.fail(String.format("Unfamiliar request text: '%s'", requestText));
            return "";
        }
    }
}
