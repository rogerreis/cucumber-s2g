package pages.bidSyncPro.supplier.dashboard.filterResultsSection;

import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.GlobalVariables;

/***
 * <h1>Class BidEndDateSection</h1>
 * @author dmidura
 * <p>details: This class houses the objects and methods for the Bid End Date section on the Filter Results</p>
 */
public class BidEndDateSection extends FilterResultsSectionNewClass {
	
	// Other 
	private EventFiringWebDriver driver;
	
	// xpaths
	private final String includePastBidsTxt_xpath               = "//mat-slide-toggle[@id='bidEndSlideToggle']//span[contains(text(), 'Include past bids')]";
	private final String includePastBidsCheckboxIndicator_xpath = "//mat-slide-toggle[@id='bidEndSlideToggle']//input[@type='checkbox']";
	private final String includePastBidsCheckbox_xpath          = "//mat-slide-toggle[@id='bidEndSlideToggle']";
	private final String timeFrameDropdown_xpath 	            = "//a[@id='aShowTimeFrame']";
	
	
	public BidEndDateSection(EventFiringWebDriver driver) {
		super(driver);
		this.driver   = driver;
	}

	/* ----- getters ----- */
	private WebElement getIncludePastBidsTxt()               { return findByVisibility(By.xpath(this.includePastBidsTxt_xpath));               }
	private WebElement getIncludePastBidsCheckboxIndicator() { return findByVisibility(By.xpath(this.includePastBidsCheckboxIndicator_xpath)); }
	private WebElement getIncludePastBidsCheckbox()          { return findByVisibility(By.xpath(this.includePastBidsCheckbox_xpath));          }
	private WebElement getSelectTimeFrameDropdown()          { return findByVisibility(By.xpath(this.timeFrameDropdown_xpath));                }

	/* ----- methods ----- */
	
 	/***
	 * <h1>openSelectTimeFrameDropdown</h1>
	 * <p>purpose: Click to open the "SELECT TIMEFRAME" dropdown.<br>
	 * 	A SelectTimeFrame object is then returned, so you can select the dropdown choice.<br>
	 * 	(Dropdown choices are "Past 6 Months", "Past Year", "Past 3 Years", "Custom Time Frame")</p>
	 * @return SelectTimeFrame
	 */
	public SelectTimeFrameDropdown openTimeFrameDropdown() {
		this.getSelectTimeFrameDropdown().click();
		return new SelectTimeFrameDropdown(driver);
	}
	
	/***
	 * <h1>getTimeFrameSelectionText</h1>
	 * <p>purpose: After a selection is made in the "SELECT TIME FRAME" dropdown,<br>
	 * 	that text is displayed. Return that text</p>
	 * @return String
	 */
	private String getTimeFrameSelectionText() {
		return this.getSelectTimeFrameDropdown().getText();
	}

	/***
	 * <h1>isCheckboxChecked</h1>
	 * <p>purpose: Test if "Include past bids" checkbox is checked
	 * @return Boolean = true if checked | false if not checked
	 */
	private Boolean isCheckboxChecked(){
		return this.getIncludePastBidsCheckboxIndicator().isSelected();
	}
	
	/***
	 * <h1>checkIncludePastBidsCheckbox</h1>
	 * <p>purpose: Check the "Include past bids" checkbox if it is not already checked</p>
	 * @return BidEndDateSection
	 */
	public BidEndDateSection checkIncludePastBidsCheckbox() {
		if (!isCheckboxChecked()) { this.getIncludePastBidsCheckbox().click();}
		return this;
	}

	/***
	 * <h1>uncheckIncludePastBidsCheckbox</h1>
	 * <p>purpose: Uncheck the "Include past bids" checkbox if it is not already unchecked</p>
	 * @return BidEndDateSection
	 */
	public BidEndDateSection uncheckIncludePastBidsCheckbox() {
		if (isCheckboxChecked()) { this.getIncludePastBidsCheckbox().click();}
		return this;
	}
	
	/***
	 * <h1>getCustomTimeFrameSection</h1>
	 * <p>purpose: Access to the custom time frame section<br>
	 * 	("Show Bid to" and "Show Bid From" selectors).<br>
	 * 	Note: These selectors should already be visible <br>
	 * 	when using this method</p>
	 * @return CustomTimeFrameSection
	 */
	public CustomTimeFrameSection getCustomTimeFrameSection() {
		return new CustomTimeFrameSection(driver);
	}
	
	/* ----- verifications ----- */
    /***
     * <h1>verifyIncludePastBidsIsUnchecked</h1>
     * <p>purpose: Verify that the Include Past Bids checkbox is unchecked<br>
     * 	Fail test if Include Past Bids checkbox is unchecked</p>
     * @return BidEndDateSection
     */
    public BidEndDateSection verifyIncludePastBidsCheckboxIsUnchecked() {
    	if (this.isCheckboxChecked()) { fail("Test Failed: Include Past Bids checkbox is not unchecked");}
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified \"Include Past Bids\" checkbox is unchecked");
		return this;
    }

    /***
     * <h1>verifyIncludePastBidsIsChecked</h1>
     * <p>purpose: Verify that the Include Past Bids checkbox is checked<br>
     * 	Fail test if Include Past Bids checkbox is checked</p>
     * @return BidEndDateSection
     */
    public BidEndDateSection verifyIncludePastBidsCheckboxIsChecked() {
    	if (!this.isCheckboxChecked()) { 
    		// Fix for release OCT 7
    		this.getIncludePastBidsTxt().click();
    	}
    		//	fail("Test Failed: Include Past Bids checkbox is not checked");}
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified \"Include Past Bids\" checkbox is checked");
		return this;
    }
    
    /***
     * <h1>verifyTimeFrameSelectionDisplaysValue</h1>
     * <p>purpose: Verify that the currently displayed value of the "SELECT TIME FRAME" dropdown is <br>
     * 	expectedTimeFrame. Fail test if "SELECT TIME FRAME" dropdown does not currently display expectedTimeFrame</p>
     * @param expectedTimeFrame = TimeFrameOptions is the expected time frame that should currently be displayed in the<br
     * 	"SELECT TIME FRAME" dropdown
     * @return BidEndDateSection
     */
    public BidEndDateSection verifyTimeFrameSelectionDisplaysValue(TimeFrameOptions expectedTimeFrame) {
    	String displayedText = this.getTimeFrameSelectionText();
    	String expectedText = expectedTimeFrame.toString();
    	
    	if (displayedText.equals(expectedText)) {
    		fail("Test Failed: Expected Time Frame option to be \"" + expectedText + "\" but instead got \"" + displayedText + "\""); }
    	
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified that time frame selection is currently displayed as \"" + displayedText + "\"");
    	return this;
    }

	/***
	 * <h1>verifySectionInitalization</h1>
	 * <p>purpose: Verify that the Bid End Date section has initialized per:<br>
	 * 	1. "Include past bids" checkbox unchecked<br>
	 * 	After checking:<br>
	 *	1. "SELECT TIME FRAME" dropdown becomes visible<br>
	 * 	Fail test if any of the above conditions are not met</p>
	 * @return BidEndDateSection
	 */
	public BidEndDateSection verifySectionInitialization() {

		// 1. Verify checkbox initalizes unchecked
		this.verifyIncludePastBidsCheckboxIsUnchecked();
		
		// And properly displays both checkbox and text
		if (!this.getIncludePastBidsCheckbox().isDisplayed() | !this.getIncludePastBidsCheckboxIndicator().isDisplayed()){
			fail("Test Failed: \"Include past bids\" checkbox is not displaying on screen"); }
		
		if (!this.getIncludePastBidsTxt().isDisplayed()) {
			fail("Test Failed: \"Include past bids\" checkbox text is not displaying on screen");}

		// 2. Verify checkbox can be checked
		this.checkIncludePastBidsCheckbox()
			.verifyIncludePastBidsCheckboxIsChecked();
		
		// And that the "SELECT TIME FRAME" dropdown is visible
		if (!this.getSelectTimeFrameDropdown().isDisplayed()) {
			fail("Test Failed: After checking \"Include past bids\" checkbox, \"SELECT TIME FRAME\" dropdown is not displaying on screen"); }
		
		// 3. Verify that we can uncheck the checkbox
		this.uncheckIncludePastBidsCheckbox()
			.verifyIncludePastBidsCheckboxIsUnchecked();
		
		// And that the "SELECT TIME FRAME" dropdown is now hidden
		if (this.doesPageObjectExist(driver, By.xpath(this.timeFrameDropdown_xpath))) {
			fail("Test Failed: After unchecking \"Include past bids\" checkbox, \"SELECT TIME FRAME\" dropdown is still displaying on screen"); }
		
		// 4. Close/Open the "Bid End Date" section, and verify the section is closed/open
		this.closeBidEndDateSection()
			.verifyBidEndDateSectionIsClosed()
			.openBidEndDateSection()
			.verifyBidEndDateSectionIsOpen();

    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified Bid End Date section initialization");
		return this;
	}
}
