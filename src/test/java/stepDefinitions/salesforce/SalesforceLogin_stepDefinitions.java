package stepDefinitions.salesforce;

import core.CoreAutomation;
import cucumber.api.java.en.Then;
import pages.common.helpers.GlobalVariables;
import cucumber.api.java.en.And;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;


public class SalesforceLogin_stepDefinitions {

	public CoreAutomation automation;
	public EventFiringWebDriver driver;	

	public SalesforceLogin_stepDefinitions(CoreAutomation automation) {
		this.automation = automation;
		this.driver = automation.getDriver();		
	}	
	

	//Search for the vendor by company (Account)
	@And("^I search for my vendor as \"([^\"]*)\"$")	
	public void I_search_for_my_vendor_as_and_my_user_as (String arg0) throws Throwable {		
		System.out.println(GlobalVariables.getTestID() + " Enter the Company, and search.\n");
		driver.findElement(By.xpath("//input[@id='149:0;p']")).click();
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(By.xpath("//input[@id='149:0;p']")).sendKeys(arg0);
		TimeUnit.SECONDS.sleep(5);
		driver.findElement(By.xpath("//input[@id='149:0;p']")).sendKeys(Keys.RETURN);
		TimeUnit.SECONDS.sleep(5);		

		Boolean isvendorthere = driver.findElements(By.xpath("//a[contains(@title,'" +arg0+ "')]")).size()!=0;		
		if(isvendorthere == true) {		
			System.out.println(GlobalVariables.getTestID() + " Test Passed: "+arg0+" was found.\n");
		} else {
			System.out.println(GlobalVariables.getTestID() + " Test Failed: "+arg0+" was NOT found.\n");
		}
	}	

	@Then("^I will logout of Salesforce$")	
	public void I_will_logout_of_Salesforce() {
		System.out.println(GlobalVariables.getTestID() + " Logging out.\n");
		driver.findElement(By.xpath("//button[contains(.,'View profile')]")).click();
		driver.findElement(By.xpath("//a[contains(.,'Log Out')]")).click();		
	}
}