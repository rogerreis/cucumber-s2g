package stepDefinitions.bidSyncPro.supplier.companySettings.agencyInteraction;
 
import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import pages.bidSyncPro.supplier.companySettings.agencyInteractions.CompanyOrders;
import pages.common.helpers.GlobalVariables;

public class CompanyOrders_stepDefinitions {
  
	private EventFiringWebDriver driver;
	
	public CompanyOrders_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
	}

	@Then("^I will verify that the Company Orders Page has correctly initialized$")
	public void I_will_verify_that_the_Company_Orders_Page_correctly_initialized() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify that the Company Orders Page has required criteria fields and table headers");
		new CompanyOrders(driver).verifyPageInitialization();
	}

//	/***
//	 * 
//	 * @param name Name of the button defined by the the page
//	 * @param expectedValue
//	 * @throws InterruptedException
//	 * @throws Throwable
//	 */
//	private void verifySort(String name, String expectedValue) throws InterruptedException, Throwable {
//		companyOrdersPage.click(name);
//		if(!companyOrdersPage.getText(name).contains(expectedValue))
//			fail(String.format("FAILED: Cannot sort %s on %s",expectedValue,companyOrdersPage.getText(name)));
//		
//		System.out.printf(GlobalVariables.getTestID() + " PASSED: Sorted %s on %s%n",expectedValue,companyOrdersPage.getText(name));
//	} // end of verifySort()
		
	@And("^I will verify that the Company Orders Page Results table can be sorted$")
	public void I_will_verify_that_the_Company_Orders_Page_Results_table_can_be_sorted() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify that the Company Orders Page Results table can be sorted");

		// TODO dmidura: Refactor

//		//column headers
//		companyOrdersPage.click("ResultsHdrOrderId");
//		companyOrdersPage.verifyVisibility("ResultsHdrOrderIdDesc");
//		companyOrdersPage.click("ResultsHdrPurchaseDoc");
//		companyOrdersPage.verifyVisibility("ResultsHdrPurchaseDocDesc");
//		companyOrdersPage.click("ResultsHdrDate");
//		companyOrdersPage.verifyVisibility("ResultsHdrDateAsc");
//		companyOrdersPage.click("ResultsHdrDate");
//		companyOrdersPage.verifyVisibility("ResultsHdrDateDesc");
//		companyOrdersPage.click("ResultsHdrAgency");
//		companyOrdersPage.verifyVisibility("ResultsHdrAgencyDesc");
	}
	
	@And("^I will verify that the Company Orders Page Search returns Results$")
	public void I_will_verify_that_the_Company_Orders_Page_Search_returns_Results() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify that the Company Orders Page Search returns Results");
		
		// TODO dmidura: Refactor
	
//		//column headers
//		companyOrdersPage.click("ResultsHdrOrderId");
//		companyOrdersPage.verifyVisibility("ResultsHdrOrderIdDesc");
//		companyOrdersPage.click("ResultsHdrPurchaseDoc");
//		companyOrdersPage.verifyVisibility("ResultsHdrPurchaseDocDesc");
//		companyOrdersPage.click("ResultsHdrDate");
//		companyOrdersPage.verifyVisibility("ResultsHdrDateAsc");
//		companyOrdersPage.click("ResultsHdrDate");
//		companyOrdersPage.verifyVisibility("ResultsHdrDateDesc");
//		companyOrdersPage.click("ResultsHdrAgency");
//		companyOrdersPage.verifyVisibility("ResultsHdrAgencyDesc");
	}
	

}

