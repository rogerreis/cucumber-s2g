package pages.bidSyncPro.supplier.dashboard.bidLists;

import static org.junit.Assert.fail;

import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.common.helpers.GlobalVariables;


/***
 * <h1>Class BidCountBanner</h1>
 * @author dmidura
 * <p>details: This class contains page Objects and methods to manipulate the Bid Count Banner that
 * 	displays above the "All Bids" list.
 */
public class BidCountBanner extends BidList{
	
	// xpaths
    private final String bidCountBannerRoot_xpath     = "//div[contains(@class, 'count-comp ng-star-inserted')]";
	private final String bidCountTxt_xpath            = "//div[contains(@class, 'count-comp ng-star-inserted')]/strong";
	private final String upgradePlanLink_xpath        = "//div[contains(@class, 'count-comp ng-star-inserted')]//a[contains(.,'UPGRADE PLAN TODAY')]";
	private final String bidTotalCountTx_xpath        = "//div[contains(@class, 'count-comp ng-star-inserted')]/span/strong";
	private final String closeBidCountBannerBtn_xpath = "//div[contains(@class, 'count-comp ng-star-inserted')]/button[contains(span/mat-icon, 'close')]";
	
	// Other
	private EventFiringWebDriver driver;
	private final String expectedUpgradePlanLink = System.getProperty("supplierBaseURL") + "admin/manage-subscriptions";

	public BidCountBanner(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
		
	}
	
	// getters
    private WebElement getBidCountBannerRoot()     { return findByVisibility(By.xpath(this.bidCountBannerRoot_xpath));      }
    private WebElement getBidCountTxt()            { return findByVisibility(By.xpath((this.bidCountTxt_xpath)));           }
    private WebElement getUpgradePlanLink()        { return findByVisibility(By.xpath((this.upgradePlanLink_xpath)));       }
    private WebElement getBidTotalCountTxt()       { return findByVisibility(By.xpath(this.bidTotalCountTx_xpath));         }
    private WebElement getCloseBidCountBannerBtn() { return findByClickability(By.xpath(this.closeBidCountBannerBtn_xpath));}
   
   
    /* ----- helpers ----- */
    
    /***
     * <h1>isTotalBidsConsistent</h1>
     * <p>purpose: Total number of bids displays twice in full string message. Assuming Bid Count Banner message is 
     * 	the full string version, verify that the total displays consistently within the message
     * @return true if total is consistent | false if not
     */
    private Boolean isTotalBidsConsistent() {
    	int secondDisplay = Integer.parseInt(this.getBidTotalCountTxt().getText());
    	return secondDisplay == this.getTotalBidsCount();
    }
    
    /***
     * <h1>getRootText</h1>
     * <p>purpose: Return the entire message displayed on the Bid Count Banner<br>
     * 	(i.e. "Currently only able to view [num] of [num] bids / UPGRADE PLAN TODAY to access all [num]"
     * 	or "Currently able to view [num] of [num] bids")
     * @return
     */
    private String getRootText() {
    	return this.getBidCountBannerRoot().getText().replace("close", "").trim();
    }

    /***
     * <h1>isRootTextFullString</h1>
     * <p>purpose: Test if displayed root text is of form:<br>
     * 	"Currently only able to view [num] of [num] bids / UPGRADE PLAN TODAY to access all [num]"
     * @return true if in form | false if not in form
     */
    private Boolean isRootTextFullString() {
    	String displayedText = this.getRootText();
    	return displayedText.matches("Currently only able to view \\d+ of \\d+ bids/UPGRADE PLAN TODAY to access all \\d+");
    }

    /***
     * <h1>isRootTextAbbreviatedString</h1>
     * <p>purpose: Test if displayed root text is of form:<br>
     * 	"Currently able to view [num] of [num] bids"
     * @return true if in form | false if not in form
     */
    private Boolean isRootTextAbbreviatedString() {
    	String displayedText = this.getRootText();
    	return displayedText.matches("Currently able to view \\d+ of \\d+ bids");
    }
    
    /***
     * <h1>getLinkText</h1>
     * <p>purpose: Return the link text in "UPGRADE PLAN TODAY" link
     * @return link text
     */
    private String getLinkText() {
    	return this.getUpgradePlanLink().getAttribute("href").trim();
    }

   /* ----- methods ----- */

   /***
     * <h1>getTotalBidsCount</h1>
     * <p>purpose: Return an int count of the total number of bids<br>
     * 	(ex: "Currently [only] able to view 2 of 724 bids", return 724)
     * @return int of total number of bids
     */
    public int getTotalBidsCount()  {
    	String [] bidCounts = this.getBidCountTxt().getText().replaceFirst("bids","").split("of");
    	return Integer.parseInt(bidCounts[1].trim());
    }

    /***
     * <h1>getAccessBidsCount</h1>
     * <p>purpose: Return an int count of the number of bids the user can access<br>
     * 	(ex: "Currently [only] able to view 2 of 724 bids", return 2)
     * @return int of number of bids user can access
     */
    public int getAccessBidsCount() { 
    	String [] bidCounts = this.getBidCountTxt().getText().replaceFirst("bids","").split("of");
    	return  Integer.parseInt(bidCounts[0].trim());
    }
      
    /***
     * <h1>clickCloseButton</h1>
     * <p>purpose: Click on the "X" button to close the Bid Count Banner
     * @return BidList
     */
    public BidList clickCloseButton() {
    	this.getCloseBidCountBannerBtn().click();
    	return this;
    }
    
    /* ----- verifications ----- */
    /***
     * <h1>verifyBidCountBannerDisplays</h1>
     * <p>purpose: Verify that the Bid Count Banner is visible. Fail test if not</p>
     * @return BidCountBanner
     */
    public BidCountBanner verifyBidCountBannerDisplays() {
    	try {
    		this.getBidCountBannerRoot();
    	} catch (TimeoutException t){
    		fail("Test Failed: Bid Count Banner is not visible");
    	}
    	return this;
    }
    
    /***
     * <h1>verifyBidCountBannerDoesNotDisplay</h1>
     * <p>purpose: Verify that the Bid Count Banner is not visible. Fail test if visible</p>
     * @return BidCountBanner
     */
    public BidCountBanner verifyBidCountBannerDoesNotDisplay() {
    	Assert.assertFalse("Test Failed: Bid Count Banner is visible",
    			this.doesPageObjectExist(driver, By.xpath(this.bidCountBannerRoot_xpath)));
    	return this;
    }
    	
    /***
     * <h1>verifyBidCountBannerInitialization</h1>
     * <p>purpose: Verify that the Bid Count Banner has initialized per:<br>
     * 	1. Close button available
     * 	2. Text in one of these forms: 
     *     a. "Currently only able to view [num1] of [num2] bids / UPGRADE PLAN TODAY to access all [num2]"<br>
     *        where:<br>
     *        i.  num1 < num2 <br>
     *        ii. num2 displays consistently in message (since it displays twice)<br>
     *        iii. "UPGRADE PLAN TODAY" link navigates to manage subscriptions<br>
     *     b. "Currently able to view [num1] of [num2] bids"<br>
     *        where:<br>
     *        i. num1 = num2<br>
     * @return
     */
    public BidCountBanner verifyBidCountBannerInitialization() {
    	// Fail test immediately if no Bid Count Banner
    	this.verifyBidCountBannerDisplays();

    	SoftAssertions softly = new SoftAssertions();

    	// Verify close button is available
    	softly.assertThat(this.getCloseBidCountBannerBtn()).withFailMessage("Expeced close button to be visible on Bid Count Banner").isNotNull();
    	
    	// Verify correct text displays
    	if (this.isRootTextAbbreviatedString()) {
    		softly.assertThat(this.getAccessBidsCount())   .withFailMessage("Expected accessible bids == total bids, but got <%s> == <%s>", this.getAccessBidsCount(), this.getTotalBidsCount()).isEqualTo(this.getTotalBidsCount());
    	} else {
    		softly.assertThat(this.isRootTextFullString()) .withFailMessage("Expected Bid Count Banner text in full string format, but got <%s>", this.getRootText()).isTrue();
    		softly.assertThat(this.getLinkText())          .withFailMessage("Expected UPGRADE PLAN TODAY to link to <%s>, but got <%s>", this.expectedUpgradePlanLink, this.getLinkText()).isEqualTo(this.expectedUpgradePlanLink);
    		softly.assertThat(this.isTotalBidsConsistent()).withFailMessage("Expected consistent number of total bids, but Bid Count Banner message displays as <%s>", this.getRootText()).isTrue();
    		softly.assertThat(this.getAccessBidsCount())   .withFailMessage("Expected accessible bids <= total number of bids, but got <%s> <= <%s>", this.getAccessBidsCount(), this.getTotalBidsCount()).isLessThan(this.getTotalBidsCount());
    	}
    	
    	softly.assertAll();
    	
    	System.out.printf(GlobalVariables.getTestID() + " INFO: Verified Bid Count Banner \"%s\"\n", this.getRootText());
    	return this;
    }
    
	public BidCountBanner verifyBidCount(int bidCount, int totalCount){
		new  WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Bid count is not matching after the sort")
		.until(ExpectedConditions.and(ExpectedConditions.textToBePresentInElement(getBidCountTxt(), Integer.toString(bidCount)),
				ExpectedConditions.textToBePresentInElement(getBidTotalCountTxt(), Integer.toString(totalCount))));
		return this;
	}
}
