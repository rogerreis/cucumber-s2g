package pages.bidSyncPro.supplier.login;

import pages.common.helpers.GlobalVariables;

/***
 * <h1>Class UserLoginAccount</h1>
 * @author dmidura
 * <p>details: This class holds info about user login credentials. <br>
 *             Note: To save a login, enter the login credentials into userAccounts_ARGO.json<br>
 *                   Then, use methods in SupplierLogin to access the login info.</p>
 *
 */
public class UserLoginAccountARGO {
	
	// User login credentials
	private String AccountReference;		// Something to call the account. ex: "Generic Account"
	private String Email;					// The email for the account      ex: "supplier@periscopeholdings.com"
	private String Password;				// The password for the account   ex: "password"

	public UserLoginAccountARGO () {
	}
	
	public UserLoginAccountARGO (String AccountReference, String Email, String Password) {
		this.AccountReference = AccountReference;
		this.Email            = Email;
		this.Password         = Password;
	}

	/* --------------- Methods --------------- */
	/***
	 * <h1>getAccountReference</h1>
	 * <p>purpose: get the currently stored value of the Account Reference</p>
	 * @return String = the Account Reference
	 */
	public String getAccountReference() {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Getting Account Reference = %s\n", AccountReference);
		return AccountReference;
	}

	/***
	 * <h1>setAccountReference</h1>
	 * <p>purpose: Set the user account reference </p>
	 * @param accountReference = Something to call the account. <br>
	 * 								ex: "Generic Account"<br>
	 * 								Note: This is what is used to reference the account across cucumber.
	 * @return None
	 */
	public void setAccountReference(String accountReference) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Setting Account Reference = %s\n", accountReference);
		AccountReference = accountReference;
	}

	/***
	 * <h1>getEmail</h1>
	 * <p>purpose: get the currently stored value of the Email</p>
	 * @return String = the Email
	 */
	public String getEmail() {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Getting Email = %s\n", Email);
		return Email;
	}

	/***
	 * <h1>setEmail</h1>
	 * <p>purpose: Set the user email </p>
	 * @param email = the email for this account
	 * @return None
	 */
	public void setEmail(String email) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Setting Email = %s\n", email);
		Email = email.toLowerCase();
	}

	/***
	 * <h1>getPassword</h1>
	 * <p>purpose: get the currently stored value of the Password</p>
	 * @return String = the Password
	 */
	public String getPassword() {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Getting Password = %s\n", Password);
		return Password;
	}

	/***
	 * <h1>setPassword</h1>
	 * <p>purpose: Set the password </p>
	 * @param password = the password for this account
	 * @return None
	 */
	public void setPassword(String password) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Setting Password = %s\n", password);
		Password = password;
	}
	
}
