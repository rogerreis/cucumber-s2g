@mailinatorCleanup
Feature: Delete Mailinator Emails


@Cleanup
Scenario: Delete all emails from Mailinator team inbox
    When I navigate to Mailinator team inbox
    Then I delete all emails in the Mailinator team inbox
