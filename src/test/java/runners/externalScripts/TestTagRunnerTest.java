package runners.externalScripts;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.junit.BeforeClass;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin ={"pretty" , "json:target/cucumber/cucumber.json", "html:target/cucumber"},
        features = "src/test/resources/features",
        glue = {"stepDefinitions",
        		"stepDefinitions.hooks"},
        dryRun = false,
        snippets=SnippetType.CAMELCASE,
        tags = {"@skip"}
)

public class TestTagRunnerTest {
	@BeforeClass
    public static void before() {
        System.setProperty("debug", "true");
    }

}