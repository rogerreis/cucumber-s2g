package pages.bidSyncPro.supplier.companySettings.manageUsers;

import static junit.framework.TestCase.fail;

import pages.bidSyncPro.common.DatabaseCommonSearches;
import pages.common.helpers.GlobalVariables;

/***
 * <h1>DatabaseUserManagement</h1>
 * @author dmidura
 * <p>details: This class contains SQL searches useful for User Management tests that can be run on the ARGO database</p>
 */
public class DatabaseUserManagement {
	
	public DatabaseUserManagement() {
		
	}
	
    /***
     * <h1>verifyUserHasJoinedCompanyAsUserInDatabase</h1>
     * <p>purpose: Verify that a user has just joined a company.<br>
     * 	Fail test if user is not listed in the database as having joined the company<br>
     * 	or if the user is listed more than once in the database as having joined the company</p>
     * @param companyJoiner     = email for the user who just joined the company
     * @param companyOwnerEmail = email for the company owner
     * @param companyName       = name of the company
	 * @return DatabaseManagement
     */
    public DatabaseUserManagement verifyUserHasJoinedCompanyAsUserInDatabase(String companyJoiner, String companyOwnerEmail, String companyName) {
        
        DatabaseCommonSearches databaseSearches = new DatabaseCommonSearches();
        
        // Locate the supplier_id for the company owner in the supplier table
        String supplier_id_owner = databaseSearches.getSupplierIdInSupplierDatabaseFromCompanyNameAndOwnerEmail(companyName, companyOwnerEmail);
        
        // Now get the supplier_id listed for the user in the auth table
        String supplier_id_user = databaseSearches.getSupplierIdInAuthDatabaseUserEmail(companyJoiner);
        
        // Verify these values match
		if (!supplier_id_owner.equals(supplier_id_user)) {
			fail("Test Failed: supplier_id of user does not match supplier_id of owner, indicating that user did not join owner's company" +
				 "supplier_id_user       = \"" + supplier_id_user + "\"\n" +
				 "supplier_id_owner      = \"" + supplier_id_owner + "\"\n" +
				 "owner email            = \"" + companyOwnerEmail + "\"\n" +
				 "company name           = \"" + companyName + "\""); }
        
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"" + companyJoiner + "\" successfully joined \"" + companyName + "\" as member");
		return this;
    }
	
	/***
	 * <h1>verifyUserHasJoinedCompanyAsOwnerInDatabase</h1>
	 * <p>purpose: Verify that the user has joined the company and is listed as the company owner in ARGO database.<br>
	 * 	Fail test if user is not listed as company owner in the ARGO database</p>
	 * @param companyOwnerEmail = company owner's registration email address
	 * @param companyName = name of the company they joined and should be owner of
	 * @return DatabaseManagement
	 */
	public DatabaseUserManagement verifyUserHasJoinedCompanyAsOwnerInDatabase(String companyOwnerEmail, String companyName) {
		DatabaseCommonSearches databaseSearches = new DatabaseCommonSearches();
        
        // Get the supplier_id stored in the supplier table of the supplier database
		String supplier_id_supplier = databaseSearches.getSupplierIdInSupplierDatabaseFromCompanyNameAndOwnerEmail(companyName, companyOwnerEmail);
		
		// Get the supplier_id stored in the users table of the auth database
		String supplier_id_auth = databaseSearches.getSupplierIdInAuthDatabaseUserEmail(companyOwnerEmail);

		// Verify these values match
		if (!supplier_id_supplier.equals(supplier_id_auth)) {
			fail("Test Failed: supplier_id is stored differently for company owner in the auth and supplier databases, indicating that the was an issue creating/maintaining the company owner\n" +
				 "supplier_id_auth       = \"" + supplier_id_auth + "\"\n" +
				 "supplier_id_supplier   = \"" + supplier_id_supplier + "\"\n" +
				 "owner email            = \"" + companyOwnerEmail + "\"\n" +
				 "company name           = \"" + companyName + "\""); }
		
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"" + companyOwnerEmail + "\" successfully joined \"" + companyName + "\" as company owner");
		return this;
	}

}
