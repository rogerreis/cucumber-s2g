package pages.common.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Class that automates the mat-select element
 *
 */
public class MatSelect {
    
    private WebElement element;
    private String option_xpath_template = "//mat-option[contains(., '%s')]";
    
    public MatSelect(WebElement element) {
        this.element = element;
    }
    
    public void select(String optionText) {
        element.click();
        WebElement option = element.findElement(By.xpath(String.format(this.option_xpath_template, optionText)));
        option.click();
    }
}
