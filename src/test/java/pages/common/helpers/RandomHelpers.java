package pages.common.helpers;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

/***
 * <h1>Class RandomHelpers</h1>
 * @author dmidura
 * <p>details: This class houses methods for randomly generating values</p>
 */
public class RandomHelpers {

	public RandomHelpers() {
		
	}
    
    /***
     * <h1>getRandomIntStringOfLength</h1>
     * <p>purpose: Return a string of random ints of length</p>
     * @param length = how many ints should be in the string
     * @return a String of length made up of random ints
     */
    public String getRandomIntStringOfLength(int length) {
        StringBuilder strBuilder = new StringBuilder();
        for(int i=0; i<length; i++) {
            strBuilder.append(this.getRandomInt(9));
        }
        return strBuilder.toString();
    }
    
    /***
     * <h1>getRandomUSState</h1>
     * <p>public: Return a random US state (full name, not abbreviation)</p>
     * @return US State, full name (includes District of Columbia)
    */
    public String getRandomUSState() {
        List<String> states = Arrays.asList(
                "Alabama",
                "Alaska",
                "Arizona",
                "Arkansas",
                "California",
                "Colorado",
                "Connecticut",
                "Delaware",
                "District Of Columbia",
                "Florida",
                "Georgia",
                "Hawaii",
                "Idaho",
                "Illinois",
                "Indiana",
                "Iowa",
                "Kansas",
                "Kentucky",
                "Louisiana",
                "Maine",
                "Maryland",
                "Massachusetts",
                "Michigan",
                "Minnesota",
                "Mississippi",
                "Missouri",
                "Montana",
                "Nebraska",
                "Nevada",
                "New Hampshire",
                "New Jersey",
                "New Mexico",
                "New York",
                "North Carolina",
                "North Dakota",
                "Ohio",
                "Oklahoma",
                "Oregon",
                "Pennsylvania",
                "Rhode Island",
                "South Carolina",
                "South Dakota",
                "Tennessee",
                "Texas",
                "Utah",
                "Vermont",
                "Virginia",
                "Washington",
                "West Virginia",
                "Wisconsin",
                "Wyoming"
        );
        return states.get(RandomUtils.nextInt(0, states.size()));
    }
    
    /***
     * <h1>getRandomPhoneNumber</h1>
     * <p>purpose: Return a randomly created phone number of form:<br>
     * 	###-###-####</p>
     * @return String containing phone number
     */
    public String getRandomPhoneNumber() {
	     return String.format("%03d-%03d-%04d",
                 RandomUtils.nextInt(111, 999),
                 RandomUtils.nextInt(111, 999),
                 RandomUtils.nextInt(1111, 9999));
    }

	/**
	 * <h1>getRandomString</h1>
	 * <p>purpose: return a random string that is up to intLengthOfString chars long
	 * @param intLengthOfString = how long you want your string to be
	 * @return the random string
	 */
	public String getRandomString(int intLengthOfString) {

		String alphabet  = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String strReturn ="";
		Random rnd       = new Random();
		
		for (int i=0; i<intLengthOfString; i++) {
			char c     = alphabet.charAt(rnd.nextInt(alphabet.length()));
			strReturn += c; }
		
		return strReturn;
	}

	/***
	 * <h1>getRandomString</h1>
	 * <p>purpose: Generate a randomly sized String of random chars that is between 1-10 chars long</p>
	 * @return randomly sized String of random chars
	 */
	public String getRandomString() {
		Random rnd = new Random();
		int intRandomStringSize = rnd.nextInt(10);

		// intRandomStringSize could have been randomly generated as 0, 
		// but we need the returned string to be at least 1 char long
		return getRandomString(Integer.max(intRandomStringSize, 1));
	}

	/***
	 * <h1>getRandomBoolean</h1> 
	 * <p>purpose: Randomly generate a Boolean value</p>
	 * @return true | false
	 */
	public Boolean getRandomBoolean() {
		Random rnd = new Random();
		return rnd.nextBoolean();
	}
	
	/***
	 * <h1>getRandomInt</h1>
	 * <p>purpose: Return a random int between 0 (inclusive) and maxSize (exclusive)</p>
	 * @param maxSize = the maximum number you'd want returned
	 * @return random int between 0 and maxSize
	 */
	public int getRandomInt(int maxSize) {
		Random rnd = new Random();
		return rnd.nextInt(maxSize);
	}

	/***
	 * <h1>getRandomInt</h1>
	 * <p>purpose: Return a random int between minSize (inclusive) and maxSize (inclusive)</p>
	 * @param minSize = the min number you'd want returned
	 * @param maxSize = the maximum number you'd want returned
	 * @return random int between minSize and maxSize
	 */
	public int getRandomInt(int minSize, int maxSize) {
		return ThreadLocalRandom.current().nextInt(minSize, maxSize + 1);
	}
	
	/***
	 * <h1>getRandomEmail</h1>
	 * <p>purpose: Create a randomized-ish new email. </p>
	 * @return String = random email for mailinator<br>
	 * 	Ex: <randomString>@phimail.mailinator.com
	 */
	public String getRandomEmail () {
		String[] rand        = UUID.randomUUID().toString().split("-");
		String strNewEmail   =  rand[1] + rand[2] + "@phimail.mailinator.com";
		System.out.printf(GlobalVariables.getTestID() + " Created random email %s\n", strNewEmail);
		return strNewEmail;
	}	
	
	/**
	 * <h1>Method to generate random password for BidSync App</h1>
	 * <p>getRandomPasswordForBidSync()
	 * @return random password
	 */
	public String getRandomPasswordForBidSync() {
    	StringBuilder strBuilder = new StringBuilder();
    	return strBuilder.append(RandomStringUtils.random(3, true, false).toUpperCase())
    					 .append(RandomStringUtils.random(5, true, false).toLowerCase())
    					 .append(getRandomIntStringOfLength(5))
    					 .append("!")
    					 .toString();
    }
	
	public static List<String> usStatesTerritoryAbr = Arrays.asList(
            "AL",
            "AK",
            "AZ",
            "AR",
            "AE",
            "AA",
            "AP",
            "AS",
            "CA",
            "CO",
            "CT",
            "DE",
            "DC",
            "FL",
            "GA",
            "GU",
            "HI",
            "ID",
            "IL",
            "IN",
            "IA",
            "KS",
            "KY",
            "LA",
            "ME",
            "MD",
            "MA",
            "MI",
            "MN",
            "MS",
            "MO",
            "MT",
            "NE",
            "NV",
            "NH",
            "NJ",
            "NM",
            "NY",
            "NC",
            "ND",
            "OH",
            "OK",
            "OR",
            "PA",
            "PR",
            "RI",
            "SC",
            "SD",
            "TN",
            "TX",
            "UT",
            "VT",
            "VA",
            "VI",
            "WA",
            "WV",
            "WI",
            "WY"
    );
	public static List<String> CanadaProvinceTerritoriesAbr = Arrays.asList(
            "AB",
            "BC",
            "MB",
            "NB",
            "NF",
            "NS",
            "NT",
            "NU",
            "ON",
            "PE",
            "QC",
            "SK",
            "YT");
}
