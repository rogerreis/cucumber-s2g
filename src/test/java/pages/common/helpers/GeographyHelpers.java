package pages.common.helpers;

import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Map.Entry;

/****
 * <h1>Class GeographyHelpers</h1>
 * @author dmidura
 * <p>purpose: This class contains helpers to be used when manipulating states/provinces and countries data.</p>
 */
public class GeographyHelpers {
	
	public GeographyHelpers() {
		
	}

	/***
	 * <h1>getValidZipCodeForState</h1>
	 * <p>purpose: Given a state (full name), return a zip code from within that state</p>
	 * @param state = state name (ex: "Alabama"), includes District of Columbia
	 * @return String zip code valid within that state (ex: "35801")
	 */
    public String getValidZipCodeForState(String state) {
	    switch(state) {
            case "Alabama":
                return "35801";
            case "Alaska":
                return "99501";
            case "Arizona":
                return "85001";
            case "Arkansas":
                return "72201";
            case "California":
                return "90210";
            case "Colorado":
                return "80201";
            case "Connecticut":
                return "06101";
            case "Delaware":
                return "19901";
            case "District Of Columbia":
                return "20001";
            case "Florida":
                return "32501";
            case "Georgia":
                return "30301";
            case "Hawaii":
                return "96801";
            case "Idaho":
                return "83254";
            case "Illinois":
                return "60601";
            case "Indiana":
                return "46201";
            case "Iowa":
                return "52801";
            case "Kansas":
                return "67201";
            case "Kentucky":
                return "404202";
            case "Louisiana":
                return "70112";
            case "Maine":
                return "04032";
            case "Maryland":
                return "21201";
            case "Massachusetts":
                return "02101";
            case "Michigan":
                return "49036";
            case "Minnesota":
                return "55801";
            case "Mississippi":
                return "39530";
            case "Missouri":
                return "63101";
            case "Montana":
                return "59044";
            case "Nebraska":
                return "68901";
            case "Nevada":
                return "89501";
            case "New Hampshire":
                return "03217";
            case "New Jersey":
                return "07039";
            case "New Mexico":
                return "87500";
            case "New York":
                return "10001";
            case "North Carolina":
                return "27612";
            case "North Dakota":
                return "58282";
            case "Ohio":
                return "44101";
            case "Oklahoma":
                return "74101";
            case "Oregon":
                return "97201";
            case "Pennsylvania":
                return "15201";
            case "Rhode Island":
                return "02840";
            case "South Carolina":
                return "29020";
            case "South Dakota":
                return "57401";
            case "Tennessee":
                return "37201";
            case "Texas":
                return "78726";
            case "Utah":
                return "84321";
            case "Vermont":
                return "05751";
            case "Virginia":
                return "24517";
            case "Washington":
                return "98004";
            case "West Virginia":
                return "25813";
            case "Wisconsin":
                return "53201";
            case "Wyoming":
                return "82941";
        }
        return "99999";
    }
   
    
    /***
     * <h1>getTimeZoneForState</h1>
     * <p>purpose: Given the abbreviation for a US state, return its time zone</p>
     * @param state = state abbreviation (ex: "TX")
     * @return time zone abbrevation (ex: "EST")
     */
    public String getTimeZoneForState(String state) {
        switch(state) {
            case "ME":
            case "VT":
            case "NH":
            case "MA":
            case "RI":
            case "CT":
            case "NJ":
            case "DE":
            case "MD":
            case "DC":
            case "NY":
            case "PA":
            case "WV":
            case "VA":
            case "NC":
            case "SC":
            case "GA":
            case "FL":
            case "KY":
            case "OH":
            case "IN":
            case "MI":
                return "EST";
            case "WI":
            case "IL":
            case "TN":
            case "AL":
            case "MS":
            case "LA":
            case "AR":
            case "MO":
            case "IA":
            case "MN":
            case "ND":
            case "SD":
            case "NE":
            case "KS":
            case "OK":
            case "TX":
                return "CST";
            case "MT":
            case "WY":
            case "CO":
            case "NM":
            case "AZ":
            case "UT":
            case "ID":
                return "MT";
            case "WA":
            case "OR":
            case "NV":
            case "CA":
                return "PST";
            case "AK":
                return "AKST";
            case "HI":
                return "HST";
            default:
                return "None";
        }
    }
    
    
   	/***
   	 * <h1>getCountryCodeFromCountryName</h1>
   	 * <p>purpose: Return country code abbreviation, given country name</p>
   	 * @param countryName = "United States" | "Canada"
   	 * @return String = "US", "CA", null
   	 */
   	public String getCountryCodeFromCountryName(String countryName) {
   		switch (countryName) {
   		case "United States": return "US";
   		case "Canada"       : return "CA";
   		default: return null;
   	}
   		
   	}

   	 /***
   	  * <h1>getStateOrProvinceAbbreviationFromStateName</h1>
   	  * <p>purpose: Given a US state or CA Province, return its abbreviation</p>
   	  * <p> or given an abbreviation, get the full name
   	  * @param strStateName = state name (ex: "Texas")
   	  * @return state abbreviation (ex: "TX")
   	  */
   	 public String getStateOrProvinceAbbreviationFromStateName (String strStateName) {
   		 System.out.printf(GlobalVariables.getTestID() + " INFO: Retrieving state abbrevation for \"%s\"\n", strStateName);

   		 Map<String, String> states = new HashMap<>();
   		 states.put("Alabama",              "AL");
   		 states.put("Alaska",               "AK");
   		 states.put("Alberta",              "AB");
   		 states.put("American Samoa",       "AS");
   		 states.put("Arizona",              "AZ");
   		 states.put("Arkansas",             "AR");
   		 states.put("Armed Forces (AE)",    "AE");
   		 states.put("Armed Forces Americas","AA");
   		 states.put("Armed Forces Pacific", "AP");
   		 states.put("British Columbia",     "BC");
   		 states.put("California",           "CA");
   		 states.put("Colorado",             "CO");
   		 states.put("Connecticut",          "CT");
   		 states.put("Delaware",             "DE");
   		 states.put("District Of Columbia", "DC");
   		 states.put("Florida",              "FL");
   		 states.put("Georgia",              "GA");
   		 states.put("Guam",                 "GU");
   		 states.put("Hawaii",               "HI");
   		 states.put("Idaho",                "ID");
   		 states.put("Illinois",             "IL");
   		 states.put("Indiana",              "IN");
   		 states.put("Iowa",                 "IA");
   		 states.put("Kansas",               "KS");
   		 states.put("Kentucky",             "KY");
   		 states.put("Louisiana",            "LA");
   		 states.put("Maine",                "ME");
   		 states.put("Manitoba",             "MB");
   		 states.put("Maryland",             "MD");
   		 states.put("Massachusetts",        "MA");
   		 states.put("Michigan",             "MI");
   		 states.put("Minnesota",            "MN");
   		 states.put("Mississippi",          "MS");
   		 states.put("Missouri",             "MO");
   		 states.put("Montana",              "MT");
   		 states.put("Nebraska",             "NE");
   		 states.put("Nevada",               "NV");
   		 states.put("New Brunswick",        "NB");
   		 states.put("New Hampshire",        "NH");
   		 states.put("New Jersey",           "NJ");
   		 states.put("New Mexico",           "NM");
   		 states.put("New York",             "NY");
   		 states.put("Newfoundland",         "NF");
   		 states.put("North Carolina",       "NC");
   		 states.put("North Dakota",         "ND");
   		 states.put("Northwest Territories","NT");
   		 states.put("Nova Scotia",          "NS");
   		 states.put("Nunavut",              "NU");
   		 states.put("Ohio",                 "OH");
   		 states.put("Oklahoma",             "OK");
   		 states.put("Ontario",              "ON");
   		 states.put("Oregon",               "OR");
   		 states.put("Pennsylvania",         "PA");
   		 states.put("Prince Edward Island", "PE");
   		 states.put("Puerto Rico",          "PR");
   		 states.put("Quebec",               "QC");
   		 states.put("Rhode Island",         "RI");
   		 states.put("Saskatchewan",         "SK");
   		 states.put("South Carolina",       "SC");
   		 states.put("South Dakota",         "SD");
   		 states.put("Tennessee",            "TN");
   		 states.put("Texas",                "TX");
   		 states.put("Utah",                 "UT");
   		 states.put("Vermont",              "VT");
   		 states.put("Virgin Islands",       "VI");
   		 states.put("Virginia",             "VA");
   		 states.put("Washington",           "WA");
   		 states.put("West Virginia",        "WV");
   		 states.put("Wisconsin",            "WI");
   		 states.put("Wyoming",              "WY");
   		 states.put("Yukon Territory",      "YT"); 
   		 
   		 if (states.containsKey(strStateName)) {
   			 return states.get(strStateName);
   		 } else {
   			 
   			 // allow getting the full name if abbreviation is provided
   			 for (Entry<String, String> entry : states.entrySet()) {
   			        if (Objects.equals(strStateName, entry.getValue())) {
   			            return entry.getKey(); } }
   			    fail("ERROR: Unable to return abbrevation for selected State/Province");
   			    return "ERROR"; }
   	 }
}
