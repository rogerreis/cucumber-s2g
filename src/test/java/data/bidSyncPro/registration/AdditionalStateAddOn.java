package data.bidSyncPro.registration;

import pages.common.helpers.GeographyHelpers;

/***
 * <h1>enum: SubscriptionAddOns</h1>
 * @author dmidura
 * <p>details: This enum defines an Additional State addon that the user can purchase
 * 	from the Chargebee test site or from User Management Chargebee punchout.<br>
 * 	Note: User CANNOT purchase an Additional State addOn during registration<br>
 * 	Additional State AddOn values: One of the 50 USA states</p>
 */
public enum AdditionalStateAddOn {
	AL,
	AK,
	AZ,
	AR,
	CA,
	CO,
	CT,
	DC,
	DE,
	FL,
	GA,
	HI,
	ID,
	IL,
	IN,
	IA,
	KS,
	KY,
	LA,
	ME,
	MD,
	MA,
	MI,
	MN,
	MS,
	MO,
	MT,
	NE,
	NV,
	NH,
	NJ,
	NM,
	NY,
	NC,
	ND,
	OH,
	OK,
	OR,
	PA,
	RI,
	SC,
	SD,
	TN,
	TX,
	UT,
	VT,
	VA,
	WA,
	WV,
	WI,
	WY;
	
	
	/***
	 * <h1>getLocationInMyAdditionalStateAddOn</h1>
	 * <p>purpose: Return the value of the selected state for the Additional State AddOn</p>
	 * @return String = state abbreviation
	 */
	public String getLocationInMyAdditionalStateAddOn() {
		return this.toString();
	}
	
	/**
	 * <h1>getStateName</h1>
	 * <p>purpose: Return the full text value of the selected state for the Additional State AddOn<br>
	 * 	Ex: If TX, return "Texas"
	 * @return String with state name in full text
	 */
	public String getStateName() {
		return new GeographyHelpers().getStateOrProvinceAbbreviationFromStateName(this.toString());
	}

}
