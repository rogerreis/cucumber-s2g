@must @ManageUsers @subscriptions @level3
Feature: Company Settings > Management Users - User Management Functionality for Company Owner
	ARGO-1121: "Accept/Reject users requests to join company"
	ARGO-1258: "Manage Additional Subscription Licenses (assign/remove purchased licenses)"
	ARGO-1321: "Joining a Company in Application"
	ARGO-2091: "Bug-Company Owner can add same user multiple times to company"
	BIDSYNC-408: "Vendors unable to access Pro Subscription Bids"
	

@ARGO-1121 
Scenario: Accept user's request to join company

    Given I have registered a random user
      And I register a new user at the same company
     Then The first user should receive an email about the join request
     When The first user accepts the second user's join request
     Then The second user will be removed from the first user's active requests list
      And The second user will be added to the first user's user list
      And The second user will have joined the company
      And The second user will get an email about his acceptance

  
@ARGO-1121 @ARGO-1321 
Scenario: Decline user's request to join company

    Given I have registered a random user
     And I register a new user at the same company
    When The first user declines the second user's join request
    Then The second user will be removed from the first user's active requests list
     And The second user will not have joined the company
     And The user can resend a request to join the company
     And The second user will get an email about his rejection
     
	
@ARGO-2091
Scenario: After clicking Accept to on the Active Request, the Accept button disappears

	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And the new user joins an existing company
	When the Company Owner clicks on the Accept button
	Then I will verify that the Accept button disappears
		

@ARGO-2091
Scenario: Company Owner should only be able to add a user once

	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And the new user joins an existing company
	When the Company Owner clicks on the Accept button multiple times to accept the new user
	Then the new user will only be added to the company one time
	

@ARGO-1258 @BIDSYNC-408
Scenario: Subscription Owner can assign and revoke additional licenses to users in the company.
	User who has license revoked will receive an email about license being revoked.

	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And the new user joins a company with available subscription licenses
	When the new user is assigned a subscription license by the company owner
	And I logout of BidSync Pro
	Then I will verify that the new user has received the additional license with add-ons
	And I will verify that my user only has a "BidSync Basic" subscription plan listed in the ARGO database
	And I will verify that my user only has a "BidSync Basic" subscription plan listed in the Chargebee test site 
	When the company owner revokes the additional license from the new user
	Then I will verify that the license was revoked and the new user receives a revoke email
	And I will verify that my user only has a "BidSync Basic" subscription plan listed in the ARGO database
	And I will verify that my user only has a "BidSync Basic" subscription plan listed in the Chargebee test site 
	
	
@BIDS-134
Scenario: Manage user tab : Paginator Validation
	 Given I have logged into the Supplier side as user "User Management Tests ONLY"
      And I have navigated to Company Settings > "Manage Users"
      Then verify that paginator is displayed as expected
      And verify that the number of records matches the selected page size 
      