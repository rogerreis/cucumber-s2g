package pages.masonry.registration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class CommonTopBar</h1>
 * <p>purpose: This class houses page objects and methods to manipulate the static top bar that is present on all registration pages</p>
 */
public class CommonTopBar extends BasePageActions {
    
    protected EventFiringWebDriver driver;
    
    // xpaths
    private final String homeButton_xpath          = "//a[@href = '/']";
    private final String featuresDropdown_xpath    = "//a[@href = '/features']";
    private final String customersButton_xpath     = "//a[@href = '/customers']";
    private final String pricingButton_xpath       = "//a[@href = '/pricing']";
    private final String whyBidsyncButton_xpath    = "//a[@href = '/why-bidsync']";
    private final String theLabBidsyncButton_xpath = "//a[@href = '/the-lab']";
    private final String blogButton_xpath          = "//a[@href = '/blog']";
    private final String signInButton_xpath        = "//a[@href = '/https://app.bidsync.com']";
    private final String getBidsyncNowButton_xpath = "//div[contains(@class,'mobile-nav-holder')]//a[@href='/pricing'][contains(., 'Get BidSync Now')]";
    
    public CommonTopBar(EventFiringWebDriver driver) {
    	super(driver);
        this.driver = driver;
    }
    
    /* ----- getters ----- */
    private WebElement getHomeButton()          { return findByVisibility(By.xpath(this.homeButton_xpath));          } 
    private WebElement getFeaturesDropdown()    { return findByVisibility(By.xpath(this.featuresDropdown_xpath));    } 
    private WebElement getCustomersButton()     { return findByVisibility(By.xpath(this.customersButton_xpath));     } 
    private WebElement getPricingButton()       { return findByVisibility(By.xpath(this.pricingButton_xpath));       } 
    private WebElement getWhyBidsyncButton()    { return findByVisibility(By.xpath(this.whyBidsyncButton_xpath));    } 
    private WebElement getTheLabBidsyncButton() { return findByVisibility(By.xpath(this.theLabBidsyncButton_xpath)); } 
    private WebElement getBlogButton()          { return findByVisibility(By.xpath(this.blogButton_xpath));          } 
    private WebElement getSignInButton()        { return findByVisibility(By.xpath(this.signInButton_xpath));        } 
    private WebElement getGetBidsyncNowButton() { return findByVisibility(By.xpath(this.getBidsyncNowButton_xpath)); } 
    /* ----- methods ----- */
    /***
     * <h1>clickGetBidSyncNowBtn</h1>
     * <p>purpose: Click on the "GET BIDSYNC NOW" button (orange) on the static top bar<br>
     * 	to navigate to the Pricing page</p>
     * @return Pricing
     */
    public Pricing clickGetBidsyncNowBtn() {
    	findByScrollIntoViewBottomOfScreen(By.xpath(this.getBidsyncNowButton_xpath));
        getGetBidsyncNowButton().click();
        return new Pricing(driver);
    }
    
}
