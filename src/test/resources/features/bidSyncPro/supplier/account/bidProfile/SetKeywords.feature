@should @ARGO-1031 @administration @level3
Feature: Account > Bid Profile - Go to the Keyword Selection Section
	With this scenario, I can go directly to the user setup, after having logged in.
  
  mcarvajal: This test is not functional.  There are no asserts, so it does not test anything, and it is currently broken.
  It is also ignoring several of the acceptance criteria for ARGO-1031.  Revisit this later.  Marking as @should in the meantime.

Scenario: Successful login with valid user.
    Given I navigate my browser to the supplier login page    
    And I enter my Email as "dsevern@phimail.mailinator.com" and Password as "test_1234"    
    Then I will be on the supplier Home page                
    When I navigate to the notifications page
    Then I will be on the notifications page
    When I add keywords
    And I will save my changes    

#The bid relevance portion is not working correctly. It hangs    
    #When I select thumbs up 5 times    
    #Then I can go to the bid list   
    