package pages.bidSyncPro.supplier.login;

import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/***
 * <h1>Class SupplierLogin</h1>
 * @author dmidura
 * <p>details: This class holds all the objects and methods related to supplier user login</p>
 *
 */
public class SupplierLogin extends BasePageActions {

    // xpaths
    private final String loginButton_xpath        = "//button[@id ='loginButton'][contains(span, 'Log In')]";
    private final String rememberMeCheckbox_xpath = "//mat-checkbox[@id='matRememberCheckBox'][contains(., 'Remember me')]";
    private final String forgotPasswordBtn_xpath  = "//button[@id = 'forgotPasswordButton'][contains(span, 'Forgot your password?')]";
	
    // ids
    private final String emailField_id            = "email";
    private final String passwordField_id         = "password";
    private final String tryForFree_id            = "tryForFreeButton";
	
    // login elements
	private String strUserAccountFileName         = "";         
	private Map<String, UserLoginAccountARGO> LoginAccounts; // Holds all the login accounts 
		                                                     //	form: <reference name for account, UserLoginAccountARGO>
														     //  Note: Login Accounts should be set in the file userAccounts_ARGO.json
														     //        The file will then be automatically read in via the method
														     //        loadUserAccounts() in this class' constructor.
														     //        To auto-login as a user, call the method loginAsUser()
	public static UserLoginAccountARGO thisUser = new UserLoginAccountARGO(); 	// Will hold the credentials of the current user's session
	
	// other
	private EventFiringWebDriver driver;
	private String strPagePath = System.getProperty("supplierBaseURL") + "login";

	public SupplierLogin (EventFiringWebDriver driver) {
		super(driver);
		this.driver 		= driver;
	}

	/* ----- getters ----- */
	
	public WebElement getUserNameField()         { return findByVisibility(By.id(this.emailField_id));               }
	public WebElement getPasswordField()         { return findByVisibility(By.id(this.passwordField_id));            }
	public WebElement getLoginButton()           { return findByVisibility(By.xpath(this.loginButton_xpath));        }
	private WebElement getRememberMeCheckbox()    { return findByVisibility(By.xpath(this.rememberMeCheckbox_xpath)); }
	private WebElement getForgotYourPasswordBtn() { return findByVisibility(By.xpath(this.forgotPasswordBtn_xpath));  }
	private WebElement getTryForFreeButton()      { return findByVisibility(By.id(this.tryForFree_id));               }
    
    /* ----- setters -----*/
    /***
     * <h1>setUserAccountFileName</h1>
     * <p>purpose: Grab the appropriate UserAccounts_ARGO.json file to read in, <br>
     * 	based on the server that we're running our tests on. Server is set in the<br>
     * 	config.json file as "qa" | "dev" | "prod"</p>
     */
    private void setUserAccountFileName() {
    	switch (System.getProperty("server")) {
    	case "qa":
    		this.strUserAccountFileName = "userAccountsQA_ARGO.json";
    		break;
    	case "dev":
    		this.strUserAccountFileName = "userAccountsDEV_ARGO.json";
    		break;
    	case "stage":
    		this.strUserAccountFileName = "userAccountsSTAGE_ARGO.json";
    		break;
    	case "prod":
    		this.strUserAccountFileName = "userAccountsPROD_ARGO.json";
    		break;
    	}
    	
    	
    }
    
	/* ----- methods ----- */
    
    // thisUser

    /***
     * <h1>setThisUserFromAccountReference</h1>
     * purpose: Set static thisUser by providing an account reference.<br>
     * 	Account reference should be defined in "userAccounts_ARGO.json"</p>
     * @param strAccountReference = name of the account reference. As defined in "userAccounts_ARGO.json"
     * @return SupplierLogin
     */
    public SupplierLogin setThisUserFromAccountReference(String strReferenceAccount) {
    	LoginAccounts = this.loadUserAccounts();
    	
    	if (!LoginAccounts.containsKey(strReferenceAccount)) { 
    		fail("ERROR: \"" + strReferenceAccount + "\" was not located in file \"" + strUserAccountFileName + "\""); }
    	
    	thisUser = LoginAccounts.get(strReferenceAccount);

    	return this;
    }
    
    
    // Login methods

	/***
	 * <h1>loginAsUser</h1>
	 * <p>purpose: Login into the supplier side as a specified user<br>
	 * 	Static thisUser will also be set in this method</p>
	 * @param strUserReference = the name to reference your user by, as defined in userAccounts_ARGO.json
	 * @return None
	 */
	public BidList loginAsUser(String strUserReference) {
		LoginAccounts = this.loadUserAccounts();

		System.out.printf(GlobalVariables.getTestID() + " INFO: Logging is as user %s\n", strUserReference);
		
		// First verify that our user account was defined in userAccounts.json
		if (!LoginAccounts.containsKey(strUserReference)) {
            fail("Test Failed: Login user does not exist in user definitions.");
        }
        
        // Login
        UserLoginAccountARGO myUser = LoginAccounts.get(strUserReference);
        this.loginWithCredentials(myUser.getEmail(), myUser.getPassword());

        // Save out our current user into the static
        thisUser = new UserLoginAccountARGO (myUser.getAccountReference(), myUser.getEmail(), myUser.getPassword());

        return new BidList(driver);
	}

	/***
	 * <h1>loadUserAccounts</h1>
	 * <p>purpose: User accounts are defined in the file strUserAccountFileName<br>
	 *             This method reads in that file and then sets the data for access. </p>
	 * @return None
	 */
	public Map<String, UserLoginAccountARGO> loadUserAccounts() {
		setUserAccountFileName();
		System.out.printf(GlobalVariables.getTestID() + " INFO: Loading users from %s\n", strUserAccountFileName);
		
	    Map<String, UserLoginAccountARGO> loginAccounts = new HashMap<>();

		// Open strUserAccountFileName file for reading
		Gson gson             = new Gson();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(strUserAccountFileName));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
				
		// Grab the account info for all accounts stored in userAccount.json
		Type type = new TypeToken<List<UserLoginAccountARGO>>(){}.getType();

		List<UserLoginAccountARGO> allAccounts = gson.fromJson(Objects.requireNonNull(reader), type);
		
		// And now save out that info for each login account, so that we can access this info
		for (UserLoginAccountARGO infoForOneAccount : allAccounts){
			loginAccounts.put(infoForOneAccount.getAccountReference(), infoForOneAccount);
		}
		
		return loginAccounts;
	}
    
	/***
	 * <h1>loginWithCredentials</h1>
	 * <p>purpose: Login into bidsync pro with the given user credentials<br>
	 * 	(Useful when creating new users that are not stored in userAccounts_ARGO.json)<br>
	 * 	Static thisUser will also be set in this method</p>
	 * @param emailAddress = registration email for login
	 * @param password = password for login
	 * @return BidList
	 */
    public BidList loginWithCredentials(String emailAddress, String password) {
        System.out.printf(GlobalVariables.getTestID() + " INFO: Logging in with User Email    = %s\n", emailAddress);
        System.out.printf(GlobalVariables.getTestID() + " INFO: Logging in with User Password = %s\n", password);
        
        this.getUserNameField().sendKeys(emailAddress);
        this.getPasswordField().sendKeys(password);
        // Set thisUser static
        thisUser = new UserLoginAccountARGO("Current User", emailAddress, password);
        this.getLoginButton().click();
        return new BidList(driver);
    }

    // navigation 
	/***
	 * <h1>getSupplerPageURL</h1>
	 * <p>purpose: Return the URL to the supplier login screen</p>
	 * @return String = the URL to the Supplier Login screen
	 */
	public String getSupplierPageURL() {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Getting Page Path = %s\n", strPagePath);
		return strPagePath;
	}

    
    /***
     * <h1>navigateToHere</h1>
     * <p>purpose: Navigate driver to supplier login screen</p>
     * @return SupplierLogin
     */
    public SupplierLogin navigateToHere() {
    	System.out.println(GlobalVariables.getTestID() + " INFO: Navigating to URL = " + this.getSupplierPageURL());
	    this.driver.navigate().to(this.getSupplierPageURL());
	    return this;
    }
    
    // Forgot your password
    
    /***
     * <h1>clickForgotYourPassword</h1>
     * <p>purpose: Click on the "Forgot your password?" button</p>
     * @return ForgotYourPassword
     */
    public ForgotYourPassword clickForgotYourPassword() {
    	this.getForgotYourPasswordBtn().click();
    	return new ForgotYourPassword(driver);
    }
    
    
    // Remember Me checkbox

    /***
     * <h1>checkRememberMeCheckbox</h1>
     * <p>purpose: Check "Remember me" checkbox if it is not already checked
     * @return SupplierLogin
     */
    public SupplierLogin checkRememberMeCheckbox() {
    	if (!this.isRememberMeChecked()) {
    		this.getRememberMeCheckbox().click(); }
    	return this;
    }

    /***
     * <h1>uncheckRememberMeCheckbox</h1>
     * <p>purpose: Uncheck "Remember me" checkbox if it is not already unchecked
     * @return SupplierLogin
     */
    public SupplierLogin uncheckRememberMeCheckbox() {
    	if (this.isRememberMeChecked()) {
    		this.getRememberMeCheckbox().click();}
    	return this;
    }
    
    /***
     * <h1>isRememberMeChecked</h1>
     * <p>purpose: Determine if "Remember me" checkbox is checked</p>
     * @return true if checked | false if unchecked
     */
    private Boolean isRememberMeChecked() {
    	Boolean ret =  driver.findElement(By.xpath(this.rememberMeCheckbox_xpath + "//input")).getAttribute("aria-checked").equals("true");
    	return ret;
    }
    
    // Try for Free

    /***
     * <h1>clickTryForFree</h1>
     * <p>purpose: Click on "Try For Free" button to navigate to Masonry registration page
     * @return SupplierLogin
     */
    public SupplierLogin clickTryForFree() {
    	this.getTryForFreeButton().click();
    	return this;
    }

    /* ----- verifications ----- */
    
    /***
     * <h1>verifyRememberMeCheckboxIsChecked</h1>
     * <p>purpose: Verify "Remember me" checkbox is checked<br>
     * 	Fail test if checkbox is not checked</p>
     * @return SupplierLogin
     */
    public SupplierLogin verifyRememberMeCheckboxIsChecked() {
    	if (!this.isRememberMeChecked()) {
    		fail("Test Failed: \"Remember me\" checkbox is not checked"); }
    	
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"Remember Me\" checkbox is checked");
    	return this;
    }
    
    /***
     * <h1>verifyRememberMeCheckboxIsUnchecked</h1>
     * <p>purpose: Verify "Remember me" checkbox is unchecked<br>
     * 	Fail test if checkbox is not unchecked</p>
     * @return SupplierLogin
     */
    public SupplierLogin verifyRememberMeCheckboxIsUnchecked() {
    	if (this.isRememberMeChecked()) {
    		fail("Test Failed: \"Remember me\" checkbox is not unchecked"); }
    	
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"Remember Me\" checkbox is unchecked"); return this; } 
    /**
     * <h1>verifyURLIsLoginScreen</h1>
     * <p>purpose: Wait up to 5 seconds for the current URL to be the login screen and login screen to be visible.<br>
     * 	Fail test if login screen is not visible after 5 seconds</p>
     * @return SupplierLogin
     */
    public SupplierLogin verifyURLIsLoginScreen() {
    	int time = DEFAULT_TIMEOUT;
    	String expectedURL = this.getSupplierPageURL();

    	new WebDriverWait(driver, time)
    		.pollingEvery(Duration.ofSeconds(1))
    		.withMessage("Test Failed: After \"" + time + "\" seconds, url is not \"" + expectedURL + "\"")
    		.until( ExpectedConditions.and(
    				ExpectedConditions.urlContains(expectedURL),
    				ExpectedConditions.visibilityOf(this.getLoginButton()),
    				ExpectedConditions.visibilityOf(this.getUserNameField()),
    				ExpectedConditions.visibilityOf(this.getPasswordField())));
    	
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified that user is on login screen");
    	return this;
    }

}
