@must @subscriptions @level2
Feature: Company Settings > Manage Subscriptions - Change Subscriptions through Chargebee Punch out in BidSync Pro
ARGO-1868: "BUG: Changing a user's subscription in the Manage Subscriptions screen  always results in a State-Yearly subscription"


@ARGO-1868 @BIDSYNC-408
Scenario Outline: If user subscription is changed in Manage Subscriptions, then database subscription table properly updates
	Given I have logged into the Supplier side as user "UserForChargebeeSubscriptionsTest" 
	When I change my user subscription to "<Subscription>" from the Manage Subscriptions screen
	Then I will verify that the subscription has changed and properly updates the ARGO database
	And I will verify that my user only has a "<Subscription>" subscription plan listed in the Chargebee test site 
    
	Examples:
	| Subscription		         |
	| BidSync National - Yearly  |
	| BidSync National - Monthly |
	| BidSync National - 6 Month |
	| BidSync Regional - Yearly  |
	| BidSync Regional - Monthly |
	| BidSync Regional - 6 Month |
	| BidSync State - Yearly     |
	| BidSync State - Monthly    |
	| BidSync State - 6 Month    |
 
@BIDS-477
Scenario: Create User in chargebee only and complete registartion in pro from selecting email link
	Given I created a user in chargebee 
	And I have completed supplier registration up to Company Details screen 
	Then verify state dropdown is editable 
	And I complete supplier registration from Company Details screen 
 