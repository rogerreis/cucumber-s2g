@must @registration @level1 
Feature: Supplier Registration - BidSync Website Registration 

@ARGO-1037 @ARGO-969
Scenario: Supplier Registers for a BidSync Basic Account and Account Details are 
		Correctly Recorded in the ARGO Database 
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	Then I will verify that a new "BidSync Basic" user is created in the ARGO database with not empty customer_id


@ARGO-1507
Scenario Outline: Masonry Billing Address (and all other registration data) appears in ARGO database for Pro Plan registration through Masonry only
	Given I register a new user in Masonry with a "<subscriptionType>" subscription plan
	And I wait "20" seconds for the backend to catch up
	Then That user's data will be stored in the database
  
  	Examples:
	| subscriptionType           |
	| BidSync National - Yearly  |
	| BidSync National - Monthly |
	| BidSync National - 6 Month |
	| BidSync Regional - Yearly  |
	| BidSync Regional - Monthly |
	| BidSync Regional - 6 Month |
	| BidSync State - Yearly     |
	| BidSync State - Monthly    |
	| BidSync State - 6 Month    |
    
    
@ARGO-1507
Scenario: Masonry Billing Address (and all other registration data) appears in ARGO database for Basic registration through Masonry only
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I wait "11" seconds for the backend to catch up
	Then That user's data will be stored in the database

@BIDS-209	
Scenario: 'Terms and Condition' link should open in a new window 
	Given verify terms And condition opens in new window