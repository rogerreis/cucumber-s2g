@ignore
Feature: Tracking Bid Clicks
  Clicks on bids in the All Bids, New For You, and Your Saved Bids tables, as well as bids in the daily notification
  email, should be saved in the survivor.bid_click table.
  
  dmidura: Turning off this test until it can be refactored.
  
  Columns in bid_click table:
  bid click id, user id, solicitation id, source (where it was clicked from), last bid click date, click count
  
  @ARGO-1722
  Scenario: Dashboard - Track clicks from Bid List table
    Given I register a random "National Annual" user with all subscription addOns
      And I have saved 1 bids from the "New For You" tab
     When I view details of 2 bids from the "New For You" tab
     When I view details of 1 bids from the "Your Saved Bids" tab
     When I view details of 3 bids from the "All Bids" tab
     Then I verify the clicks have been correctly tracked in the database

