package stepDefinitions.bidSyncPro.registration;

import core.CoreAutomation;
import cucumber.api.java.en.When;
import pages.bidSyncPro.registration.EnterKeywords;
import pages.common.helpers.GlobalVariables;
import cucumber.api.java.en.Then;

import org.openqa.selenium.support.events.EventFiringWebDriver;

public class Keywords_stepDefinitions {
	
	private EventFiringWebDriver driver;
	
	public Keywords_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
	}

	@When ("^I enter more than two keywords on the Enter Keywords screen$")
	public void I_enter_more_than_two_keywords_on_the_Enter_Keywords_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Adding more than two keywords on the \"Enter Keywords\" screen");
		new EnterKeywords(driver)
                .addPositiveKeywords("water", "plumbing", "sewer", "hydraulic", "drain");
	}

	@When ("^I delete more than two keywords on the Enter Keywords screen$")
	public void I_delete_more_than_two_keywords_on_the_Enter_Keywords_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Deleting more than two keywords on the \"Enter Keywords\" screen");
		new EnterKeywords(driver)
			.deletePositiveKeyword("drain")
			.deletePositiveKeyword("water")
			.deletePositiveKeyword("sewer");
	}

	@Then ("^I will verify that I do not see a profile error on the Enter Keywords screen$")
	public void I_will_verify_that_I_do_not_see_a_profile_error_on_the_Enter_Keywords_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that no profile error displays on the \"Enter Keywords\" screen.");
		new EnterKeywords(driver)
				.verifyProfileErrorNotDisplayed();
		}
}