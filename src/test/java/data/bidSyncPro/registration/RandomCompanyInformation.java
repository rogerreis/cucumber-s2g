package data.bidSyncPro.registration;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import pages.common.helpers.GeographyHelpers;
import pages.common.helpers.RandomHelpers;

/***
 * <h1>Class RandomCompanyInformation</h1>
 * <p>details: This class houses expected data for creating a company</p>
 *
 */
public class RandomCompanyInformation {
    
    private String name;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String state;
    private String zipCode;
    private String country;
    private String FEIN;
    private String SSN;
    private String typeOfIndustry;
    private int    numberOfEmployees;
    private long   annualRevenue;
    private int    yearEstablished;
    
    /***
     * <h1>RandomCompanyInformation</h1>
     * <p>purpose: Create a random company per:<br>
     * 	1.  name              = random String 7-10 chars long<br>
     * 	2.  addressLine1      = random String (form: String space String)<br>
     *  3.  addressLine1      = String composed of random chars, that is between 1-10 chars long<br>
     *  4.  city              = String composed of random chars, that is between 1-10 chars long<br>
     *  5.  state             = random US state (full name, not abbreviation)<br>
     *  6.  zipCode           = valid zip code for the selected state<br>
     *  7.  FEIN              = String composed of 9 random ints, ex: 123456789<br>
     *  8.  SSN               = String composed of 9 random ints, ex: 123456789<br>
     *  9.  typeOfIndustry    = String composed of random chars, that is between 1-10 chars long<br>
     *  10. numberOfEmployees = random int that is between 10-10,000<br>
     *  11. annualRevenue     = random long that is between 100,000 - 1,000,000,000<br>
     *  12. yearEstablished   = random int between 1800 - 2000<br>
     *  13. country           = United States</p>      
     */
    public RandomCompanyInformation() {
        RandomHelpers randomHelpers       = new RandomHelpers();
        GeographyHelpers geographyHelpers = new GeographyHelpers();

        this.name                   = RandomStringUtils.randomAlphanumeric(7,10);
        this.addressLine1           = String.valueOf(RandomUtils.nextInt(111, 9999)) + " " + randomHelpers.getRandomString();
        this.addressLine2           = randomHelpers.getRandomString();
        this.city                   = randomHelpers.getRandomString();
        this.state                  = randomHelpers.getRandomUSState();
        this.zipCode                = geographyHelpers.getValidZipCodeForState(this.state);
        this.FEIN                   = randomHelpers.getRandomIntStringOfLength(9);
        this.SSN                    = randomHelpers.getRandomIntStringOfLength(9);
        this.typeOfIndustry         = randomHelpers.getRandomString();
        this.numberOfEmployees      = RandomUtils.nextInt(10, 10000);
        this.annualRevenue          = RandomUtils.nextLong(100000, 1000000000);
        this.yearEstablished        = RandomUtils.nextInt(1800, 2000);
        this.country                = "United States of America";//was - United States
    }
    
    /* ----- getters ----- */
    public String getName             () { return name;              }
    public String getAddressLine1     () { return addressLine1;      }
    public String getAddressLine2     () { return addressLine2;      }
    public String getCity             () { return city;              }
    public String getState            () { return state;             } 
    public String getZipCode          () { return zipCode;           }
    public String getCountry          () { return country;           }
    public String getFEIN             () { return FEIN;              }
    public String getSSN              () { return SSN;               } 
    public String getTypeOfIndustry   () { return typeOfIndustry;    }
    public int    getNumberOfEmployees() { return numberOfEmployees; }
    public long   getAnnualRevenue    () { return annualRevenue;     }
    public int    getYearEstablished  () { return yearEstablished;   }

    /* ----- setters ----- */
    public void setName             (String name             ) { this.name              = name;              }
    public void setAddressLine1     (String address          ) { this.addressLine1      = address;           }
    public void setAddressLine2     (String address          ) { this.addressLine2      = address;           }
    public void setCity             (String city             ) { this.city              = city;              }
    public void setState            (String state            ) { this.state             = state;             }
    public void setZipCode          (String zipCode          ) { this.zipCode           = zipCode;           }
    public void setCountry          (String country          ) { this.country           = country;           }
    public void setFEIN             (String FEIN             ) { this.FEIN              = FEIN;              }
    public void setSSN              (String SSN              ) { this.SSN               = SSN;               }
    public void setTypeOfIndustry   (String type             ) { this.typeOfIndustry    = type;              }
    public void setAnnualRevenue    (long   annualRevenue    ) { this.annualRevenue     = annualRevenue;     }
    public void setYearEstablished  (int    yearEstablished  ) { this.yearEstablished   = yearEstablished;   }
    public void setNumberOfEmployees(int    numberOfEmployees) { this.numberOfEmployees = numberOfEmployees; }

    
    /* ----- methods ----- */
    /***
     * <h1>getFEINLastFourDigits</h1>
     * <p>purpose: Return the last four digits in the FEIN.<br>
     * 	(This will be returned as a String composed of four ints)</p>
     * @return last four digits of FEIN
     */
    public String getFEINLastFourDigits() {
        int length = FEIN.length();
        return FEIN.substring(length-4, length); }

    /***
     * <h1>getSSNLastFourDigits</h1>
     * <p>purpose: Return the last four digits in the SSN.<br>
     * 	(This will be returned as a String composed of four ints)</p>
     * @return last four digits of SSN
     */
    public String getSSNLastFourDigits() {
        int length = SSN.length();
        return SSN.substring(length-4, length); }
}
