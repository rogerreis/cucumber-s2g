package pages.bidSyncPro.registration;

import static org.junit.Assert.fail;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.Objects;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.bidSyncPro.common.BidSyncProCommon;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.helpers.StringHelpers;


/***
 * <h1>Class GetInvitedToBids</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects related to the objects on the "Get Invited to Bids" screen that is part of supplier registration</p>
 */
public class GetInvitedToBids extends BidSyncProCommon {
  
    private EventFiringWebDriver driver;
    private HelperMethods helperMethods;
    private StringHelpers stringHelpers;
    
    // xpaths and ids
    // Header
    private final String title_xpath         = "//h1[contains(., 'Get Invited to Bids')]";
    private final String summaryTxt_xpath    = "//div[@id='joinOrCreateCompanyProfile']" +
                                               "//span[contains(., 'Join or create a company profile to receive bid invitations from agencies.')]";
    private final String cardTitle_xpath     = "//mat-card-title[@id='matCreateCompanyTitle'][contains(., 'Join or Create a Company')]";
    
    // Card
    private final String inputHint_xpath     = "//mat-hint[contains(., 'Type company name or select company from list')]";
    private final String companyNameInput_id = "company";
    private final String nextButton_id       = "nextButton";
    private final String companyDropdown_id  = "mat-autocomplete-0";
    
    // Footer
    private final String footerPhone_xpath   = "//footer[@id='fxFooterLayout']//span[contains(.,'(800) 990-9339')]";
    private final String footerEmail_xpath   = "//footer[@id='fxFooterLayout']//span[contains(.,'s2g-support@periscopeholdings.com')]";
    
    public GetInvitedToBids(EventFiringWebDriver driver) {
    	super(driver);
        this.driver        = driver;
        this.helperMethods = new HelperMethods();
        this.stringHelpers = new StringHelpers();
    }
    
    /* ------ getters ------ */
    
    // Header
    private WebElement getTitle()      { return findByVisibility(By.xpath(this.title_xpath));      }
    private WebElement getSummaryTxt() { return findByVisibility(By.xpath(this.summaryTxt_xpath)); }
    private WebElement getCardTitle()  { return findByVisibility(By.xpath(this.cardTitle_xpath));  }

    // Card
    private WebElement getCompanyNameInput() { return findByVisibility(By.id(this.companyNameInput_id), Integer.parseInt(System.getProperty("defaultTimeOut"))); }
    private WebElement getNextButton()       { return findByPresence(By.id(this.nextButton_id), Integer.parseInt(System.getProperty("defaultTimeOut")));         }
    private WebElement getHint()             { return findByVisibility(By.xpath(this.inputHint_xpath));      }

    // Footer
    private WebElement getFooterPhone() { return findByVisibility(By.xpath(this.footerPhone_xpath)); }
    private WebElement getFooterEmail() { return findByVisibility(By.xpath(this.footerEmail_xpath)); }

    /* ----- helpers ----- */
    
    /***
     * <h1>getDropDownForCompany</h1>
     * <p>purpose: Company Lookup is loaded and visible. If it exists, locate the companyName in the company lookup</p>
     * @param companyName = name of company to search in lookup
     * @return WebElement for the company in the lookup | null if company not located in lookup
     */
    private WebElement getDropDownOptionForCompany(String companyName) {
        String xpath = getCompanyOptionXpath(companyName, null, null);
        return getDropDownOptionForXpath(xpath);
    }

    /***
     * <h1>getDropDownForCompany</h1>
     * <p>purpose: Company Lookup is loaded and visible. If it exists, locate the companyName in the company lookup</p>
     * @param companyName    = name of company to search in lookup
     * @param companyAddress = company address of the company
     * @param city           = company city
     * @return WebElement for the company in the lookup | null if company not located in lookup
     */
    private WebElement getDropDownOptionForCompany(String companyName, String companyAddress, String city) {
        String xpath = getCompanyOptionXpath(companyName, companyAddress, city);
        return getDropDownOptionForXpath(xpath);
    }

     /***
     * <h1>getCompanyOptionXpath</h1>
     * <p>purpose: Create a generic xpath to try to locate the company within the Company Lookup
     * @param companyName    = name of company to search in lookup
     * @param companyAddress = company address of the company | null (optional)
     * @param city           = company city                   | null (optional)
     * @return the generic xpath for locating the company name within the Company Lookup
     */
    private String getCompanyOptionXpath(String companyName, String companyAddress, String city) {
        if(Objects.nonNull(companyAddress) && Objects.nonNull(city))
            return String.format("//mat-option//div[@class='name-text']/span[contains(., '%s')]" +
                                              "/../../div[@class='domain-text']/span[contains(.,'%s, %s')]",
                                              companyName, companyAddress, city);
        else {
            return String.format("//mat-option//span[contains(., '%s')]", companyName);
        }
    }
    
    /***
     * <h1>getDropDownOptionForXpath</h1>
     * <p>purpose: Create a generic xpath to try to locate the company within the Company Lookup
     * @param xpath = generic xpath to locate a company in the Company Lookup
     * @return WebElement for the company in the Company Lookup | null if the WebElement can't be located based<br>
     * 	on the xpath (b/c the company isn't in the lookup or b/c Company Lookup reloaded while we're trying to locate the WebElement)</p>
     */
    private WebElement getDropDownOptionForXpath(String xpath) {
        System.out.println(GlobalVariables.getTestID() + " INFO: xpath = " + xpath);
        try {
        	// dmidura: We want a refreshed condition here while searching for the WebElement b/c
        	// the dropdown will continuously refresh and often times make our element stale by the time we access it
        	int time = DEFAULT_TIMEOUT;
            WebElement ele =
            	new WebDriverWait(driver, time)
            		.until(ExpectedConditions.refreshed(
            		ExpectedConditions.elementToBeClickable(By.xpath(xpath))));
            return ele;
        } catch(NoSuchElementException | TimeoutException e) {
            return null;
        }
    }
     
    /***
     * <h1>waitForCompanyDropdownToAppear</h1>
     * <p>purpose: Company lookup should appear within 3 seconds of entering a value into the<br>
     * 	lookup input. Fail test if lookup does not appear within 3 seconds</p>
     * @param companyName = name of company user is searching Company Lookup
     */
    private void waitForCompanyDropdownToAppear(String companyName) {
        // Make sure that we've got a value in the input
        // and the company lookup is present before trying to search the company lookup
    	int time = DEFAULT_TIMEOUT;
        new WebDriverWait(driver, time)
            .withMessage("Test Timed Out: After inputting value, company lookup did not appear within \"" + time + "\" seconds")
            .until(
                ExpectedConditions.and(
                    ExpectedConditions.attributeToBe(By.id(this.companyNameInput_id), "value", companyName),
                    ExpectedConditions.visibilityOfElementLocated(By.id(this.companyDropdown_id))
                )
            );
    }
   
    /***
     * <h1>waitForCompanyDropdownToDisappearAfterSelectingCompany</h1>
     * <p>purpose: A value was entered into the input, the Company Lookup was located, and a value<br>
     * 	was then selected from the lookup. Expected behavior is that the Company Lookup will now close.<br>
     * 	Wait up to 3 seconds to verify that lookup has closed. Fail test if Company Lookup is still open<br>
     * 	after 3 seconds.</p>
     * @param companyDropDownOption = the WebElement related to the company selected out of the Company Lookup
     */
    private void waitForCompanyDropdownToDisappearAfterSelectingCompany(WebElement companyDropDownOption) {
        // Give the company lookup a chance to dismiss and verify that "NEXT" button is available
        // before trying to click "NEXT"
    	int time = DEFAULT_TIMEOUT;
        new WebDriverWait(driver, time)
            .withMessage("Test Timed Out: Company Lookup still visible \"" + time + "\" seconds after value was selected")
            .pollingEvery(Duration.ofMillis(100))
            .until(
                ExpectedConditions.and(
                    ExpectedConditions.invisibilityOf(companyDropDownOption),
                    ExpectedConditions.elementToBeClickable(By.id(this.nextButton_id))
                )
            );
    }
    
    /***
     * <h1>handleCompanyDropDown</h1>
     * <p>purpose: Enter the companyName into the input. Company Lookup will then appear.<br>
     * 	If the company is located in the lookup, then click on that value. If the company is<br>
     * 	not located in the lookup, then click <ESACPE> to verify the lookup has dismissed.</p>
     * @param companyName    = name of the company
     * @param companyAddress = company address (line 1) | null (optional)
     * @param city           = company's city           | null (optional)
     * <p>Note: companyAddress and city parameters are optional parameters. They are useful in locating the specific<br>
     * 	company when the Company Lookup locates multiple companies with the same name</p>
     * @return true if company is located in Company Lookup | false if company is not located in Company Lookup (i.e new company)
     */
    private boolean handleCompanyDropDown(String companyName, String companyAddress, String city) {
    	// Enter value into input and wait for Company Lookup to load with results
        waitForCompanyDropdownToAppear(companyName);
        helperMethods.addSystemWait(250, TimeUnit.MILLISECONDS);
        
        // From these results, locate the company (if it is in the lookup)
        // If company is not in lookup, then return null
        WebElement companyDropDownOption = getDropDownOptionForCompany(companyName, companyAddress, city);
        if(Objects.isNull(companyDropDownOption)) {
            // This block is a workaround for 
        	// ARGO-2539: "Company address and state are not populated in the autocomplete dropdown during registration"
            companyDropDownOption = getDropDownOptionForCompany(companyName);
        }

        // If we located the company in the lookup, then click on it
        if(companyDropDownOption != null) {
            try {
                scrollIntoViewTopOfScreen((companyDropDownOption));
                companyDropDownOption.click();

            // Sometimes the element goes stale between locating and clicking b/c the Company Lookup
            // will often times continuously load/reload results. So if this happens, relocate company and click again
            } catch (StaleElementReferenceException e) {
                helperMethods.addSystemWait(250, TimeUnit.MILLISECONDS);
                getDropDownOptionForCompany(companyName, companyAddress, city).click();
            }
            
            System.out.println(GlobalVariables.getTestID() + " Company located in the Company Lookup");
            
            // Give the Company Lookup a second to hide after clicking our company
            waitForCompanyDropdownToDisappearAfterSelectingCompany(companyDropDownOption);
            return true;
        
        // The company isn't in the lookup
        } else {
        	// Send <ESCAPE> to close lookup if it's still open
            System.out.println(GlobalVariables.getTestID() + " Company not located in the Company Lookup");
            getCompanyNameInput().sendKeys(Keys.ESCAPE);
        }
        return false;
    }

	/***
	 * <h1>isCompanyLookupResultValid</h1>
	 * <p>purpose: Assuming the Company Lookup with inputed search strSearch <br>
	 * 		has returned the CompanyLookupResult resultToTest, verify that the return<br>
	 * 		is a valid return. To be valid, return must:<br>
	 * 		1. Contain a company name, company URL or company address, and company image<br>
	 * 		2. strSearch must be matched to either the company name or company URL<br>
	 * 		3. Company logo must be a valid image file</p>
	 * <p>dmidura: Workaround for BIDSYNC-42 is currently in place</p>
	 * @param strSearchString = the search string inputed into the Company Lookup
	 * @param resultToTest = a returned result from the Company Lookup
	 * @return true if result is valid | false if result is invalid
	 */
	private GetInvitedToBids verifyCompanyLookupResultValid(String strSearchString, CompanyLookupResult resultToTest) {

		String strCompanyName    = resultToTest.getCompanyName();
		String strCompanyLogoSRC = resultToTest.getCompanyLogoSRC();
		String strCompanyURL     = resultToTest.getCompanyURL();
		
		// Result must have all components set
		Assert.assertNotNull("Test Failed: Company Name is null for company lookup listing",     strCompanyName);
		Assert.assertNotNull("Test Failed: Company Logo SRC is null for company lookup listing", strCompanyLogoSRC);
		Assert.assertNotNull("Test Failed: Company URL is null for company lookup listing",      strCompanyURL);
		
		System.out.println(GlobalVariables.getTestID() + " ------ Company Lookup Result ------");
		System.out.printf(GlobalVariables.getTestID() + " Company Name     = \"%s\"\n", strCompanyName);
		System.out.printf(GlobalVariables.getTestID() + " Company Logo SRC = \"%s\"\n", strCompanyLogoSRC);
		System.out.printf(GlobalVariables.getTestID() + " Company URL      = \"%s\"\n", strCompanyURL);
		System.out.println(GlobalVariables.getTestID() + " -----------------------------------");

		// Result display something in the name, logo, and url fields
		Assert.assertFalse("Test Failed: Company Lookup listing does not contain a company name", strCompanyName.isEmpty());
		Assert.assertFalse("Test Failed: Company Lookup listing does not contain a company logo", strCompanyLogoSRC.isEmpty());
		//Commented out "strCompanyURL" assertion as we have null data in DB and fails the test
//		Assert.assertFalse("Test Failed: Company Lookup listing does not contain a company url",  strCompanyURL.isEmpty());

		// Either the Company URL or the Company Name should match the search string.
		// Here, Clearbit API ignores matches for numbers and special chars, so we will too
		// Note: converting all strings to lower case here to avoid case sensitivity
		if (!stringHelpers.removeWhitespaceNumbersAndSpecialCharsFromString(strCompanyName).toLowerCase().contains (
				stringHelpers.removeWhitespaceNumbersAndSpecialCharsFromString(strSearchString).replaceAll(" ", "").toLowerCase()) & 
			!stringHelpers.removeWhitespaceNumbersAndSpecialCharsFromString(strCompanyURL).toLowerCase().contains (
				stringHelpers.removeWhitespaceNumbersAndSpecialCharsFromString(strSearchString).replaceAll(" ","" ).toLowerCase())){
			String msg = String.format("Test Failed: Search string \"%s\" is not matched by company name = \"%s\" or company URL = \"%s\"\n", strSearchString, strCompanyName, strCompanyURL);
			fail(msg);}
		
		// Company logo must be a valid image file
		Assert.assertTrue("Test Failed: Company Logo SRC =\"" + strCompanyLogoSRC + "\" is not of valid form in Company Lookup", isCompanyLogoSRCValid(strCompanyLogoSRC, strCompanyURL));
		
		// Company URL should be of valid form
		Assert.assertTrue("Test Failed: Company URL =\"" + strCompanyURL + "\" is not a valid URL or company address in Company Lookup", isCompanyURLValid(strCompanyURL));
		
		return this;
	} 

	/***
	 * <h1>isCompanyLogoSRCValid</h1> 
	 * <p>purpose: Check if the company logo src is valid<br>
	 * Valid image files are of the form: https://logo.clearbit.com/<company_url><br>
	 *  or <supplier_base_URL>/<company_url><br>
	 *  or <supplier_base_URL>/assets/images/company-placeholder.png</p>
	 * @param strCompanyLogoSRC = the company logo SRC
	 * @param strCompanyURL     = the company URL
	 * @return Boolean = true if company logo SRC is of valid form | false if not
	 */
	private Boolean isCompanyLogoSRCValid(String strCompanyLogoSRC, String strCompanyURL) {
		System.out.println(GlobalVariables.getTestID() + " INFO: Determining if company logo SRC is of valid form");
		// Valid image files are of the form: https://logo.clearbit.com/<company_url>
		// or <supplier_base_URL>/<company_url>
		// or <supplier_base_URL>/assets/images/company-placeholder.png
		String strExpectedImagePathClearBit  = "https://logo.clearbit.com/" + strCompanyURL;
		String strExpectedImagePathPeriscope = System.getProperty("supplierBaseURL") + strCompanyURL;
		String strStockImage                 = System.getProperty("supplierBaseURL") + "assets/images/company-placeholder.png";
		return (strExpectedImagePathClearBit.equals(strCompanyLogoSRC)  | 
				strExpectedImagePathPeriscope.equals(strCompanyLogoSRC) | 
				strStockImage.equals(strCompanyLogoSRC));
	}
	
	/***
	 * <h1>isCompanyURLValid</h1>
	 * <p>purpose: Determine if the company URL is valid by verifying that strCompanyURL<br>
	 * 	ends with ".TLD" where TLD = most likely TLDs (ex: "com", "org", "us", etc). <br>
	 * 	If strCompanyURL does not end with ".TLD", then the company URL field should instead be<br>
	 * 	populated by the company address with form "String, String" (indicating this company was added<br>
	 * 	to the Company Lookup). If company address is not of this form or if String == "null", then fail test.</p>
	 * 	Note: We are not using an extensive TLD list b/c that would waste a lot of processing power,<br>
	 * 	and won't likely give us much better results than comparing to a sub-set of most common TLDs</p>
	 * <p>dmidura: Currently workaround in place for BIDSYNC-42 to ignor String == "null"</p>
	 * @param strCompanyURL
	 * @return Boolean
	 */
    private Boolean isCompanyURLValid(String strCompanyURL) {
    	//We had some bad data in elastic search which has empty address
    	if(strCompanyURL.isEmpty()) {
    		return true;
    	}
		System.out.println(GlobalVariables.getTestID() + " INFO: Determining if Company URL contains a valid TLD");

		// This is by no means, an extensive list, but a list of the most likely TLDs to be returned.
		String [] TLD = { "com", "org", "edu", "net", "int", "gov", "global", "mil", "info", "jobs", "pro",
							"au", "ae", "ag", "an", "ar", "as", "au", "az", "be", "br", "ca", "cc", "ch", "cl",
							"cn", "co", "cr", "cz", "de", "dk", "ee", "eg", "es", "eu", "fi", "fr", "gb", "ge", "gg",
							"gh", "gt", "gr", "gt", "hk", "hr", "hu", "id", "ie", "il", "in", "io", "iq", "ir", "it",
							"jo", "jp", "kh", "kr", "kw", "la", "lu", "lv", "ma", "me", "mg", "mx", "my", "ng", "ne", "nl",
							"no", "nz", "pa", "pe", "ph", "pk", "pl", "pr", "pt", "qa", "ro", "ru", "sa", "se", "sg", "si",
							"sk", "sn", "tc", "th", "tn", "tr", "tw", "ua", "ug", "uk", "us", "uz", "vn", "za", "zm", "zw", "one" };
		
		for (String thisTLD : TLD) {
			if (strCompanyURL.endsWith("." + thisTLD)) { return true; }
		}
		
		// If no URL, then strCompanyURL should contain an address (for a company created within BidSync Pro)
		// Form: <string>, <string>
		if (strCompanyURL.contains(",")) {
			String Address[] = strCompanyURL.split(",");
			if (Address[0].length() > 1 & Address[1].length() > 1 & Address.length == 2 ) {
				// TODO dmidura: Workaround for BIDSYNC-42. Remove after that ticket is fixed
				// When ARGO database value is null, then Company Lookup is passed "null" (as a String). Fail test.
				if (Address[0].contains("null") | Address[1].contains("null")) {return true; } // false; }
				return true; }
		}
		
		return false;
	}

	/***
	 * <h1>getCompanyLookupResults</h1> 
	 * <p>purpose: Assuming a search term has been inputed into the Company Lookup, return a list of the <br>
	 * 		displayed results in CompanyLookupResult type</p>
	 * @return List of the returned results in the autocomplete dropdown. Results are of CompanyLookupResult type.
	 */
	private List <CompanyLookupResult> getCompanyLookupResults() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Retrieving information for each return to the Company Lookup");
		int numberOfCompanies = 0;
		List <WebElement> bidSyncCompanies  = new ArrayList<>();
		List <WebElement> clearbitCompanies = new ArrayList<>();
		String genericBidSyncCompanyLookup_xpath  = "//mat-optgroup[1]//mat-option";
		String genericClearbitCompanyLookup_xpath = "//mat-optgroup[2]//mat-option";
		
		// BidSync Companies in Company Lookup
		if (this.doesPageObjectExist(driver, By.xpath("//mat-optgroup[1]"))) {

			// Let companies load
			new WebDriverWait(driver, 10).until(ExpectedConditions.refreshed(
					ExpectedConditions.presenceOfAllElementsLocatedBy(
					(By.xpath(genericBidSyncCompanyLookup_xpath))))); 

			// Locate each object in the Company Lookup
			bidSyncCompanies  = driver.findElements(By.xpath(genericBidSyncCompanyLookup_xpath));
			numberOfCompanies += bidSyncCompanies.size();}
		
		// Clearbit Companies in Company Lookup
		if (this.doesPageObjectExist(driver, By.xpath("//mat-optgroup[2]"))) {
			// Let companies load
			new WebDriverWait(driver, 10).until(ExpectedConditions.refreshed(
					ExpectedConditions.presenceOfAllElementsLocatedBy(
					(By.xpath(genericClearbitCompanyLookup_xpath)))));

			// Locate each object in the Company Lookup
			clearbitCompanies = driver.findElements(By.xpath(genericClearbitCompanyLookup_xpath)); 
			numberOfCompanies += clearbitCompanies.size();}


		System.out.printf(GlobalVariables.getTestID() + " INFO: \"%s\" results were returned by the Company Lookup\n", numberOfCompanies);
		
		// Now grab the pertinent info: name, URL, src
		List <CompanyLookupResult> companyResults = new ArrayList<>();
		
		// For the BidSync Companies
		for (int i=1; i<bidSyncCompanies.size() + 1; i++) {
			String XPath = genericBidSyncCompanyLookup_xpath + "[" + i + "]";
			System.out.println(XPath);
			try {
				companyResults.add(new CompanyLookupResult(driver,XPath));
			} catch (StaleElementReferenceException e) {
				System.out.println(GlobalVariables.getTestID() + " Stale Element: will not check"); }
		}

		// For the Clearbit Companies
		for (int i=1; i<clearbitCompanies.size() + 1 ; i++) {
			String XPath = genericClearbitCompanyLookup_xpath + "[" + i + "]";
			System.out.println(XPath);

			try {
				companyResults.add(new CompanyLookupResult(driver,XPath));
			} catch (StaleElementReferenceException e) {
				System.out.println(GlobalVariables.getTestID() + " Stale Element: will not check"); }
		}

		return companyResults;
	}
	    
	/***
	 * <h1>selectCompanyName</h1>
	 * <p>purpose: Assuming Company name has been entered into lookup.<br>
     * 	- If the company is located in the lookup, then select and click "NEXT" on "Get Invited to Bids".<br>
     * 	  App will navigate to the "Join Company" screen<br>
     * 	- If the company is not located in the lookup, then click "NEXT" on "Get Invited to Bids".<br>
     * 	  App will navigate to the "Create New Company" screen</p>
     * @param companyName    = name of user's company (can be in the company lookup or not)
     * @param companyAddress = company address of the company | null (optional)
     * @param city           = company city                   | null (optional)
	 * @return true if company is in lookup | false if a new company
	 */
    private Boolean selectCompanyName (String companyName, String companyAddress, String city) {
       // Company Lookup dropdown populates with names
        boolean companyExistsAlready = false;
        try {
            companyExistsAlready = handleCompanyDropDown(companyName, companyAddress, city);
        } catch(TimeoutException ignored) {
            // a TimeoutException indicates no company with that name exists in the system yet
        }
    
        // Just click "Next" to accept the company lookup value
        try {
        	this.clickNextBtn();
        } catch(WebDriverException e) {
            // sometimes autocomplete occurs when not expected and hides the next button.  Hit ESC and try again
            getCompanyNameInput().sendKeys(Keys.ESCAPE);
        	this.clickNextBtn();
        }
        
        return companyExistsAlready;
    }

    /* ------ methods ------ */

     /***
     * <h1>clickNextBtn</h1>
     * <p>purpose: Click on the "Next" button on the "Get Invited to Bids Screen"</p>
     * @return GetInvitedToBids
     */
    public GetInvitedToBids clickNextBtn() {
    	this.getNextButton().click();
    	return this;
    }
    
    /***
     * <h1>inputCompanyNameAndContinue</h1>
     * <p>purpose: <br>
     * 	This is a quick method to navigate the user through the company lookup screen to the "Enter Keywords" screen.<br>
     *	Flow sequence:<br>
     *  1. Given companyName, enter the name into the company lookup.<br>
     * 	2. a- If the company is located in the lookup, then select and click "NEXT".<br>
     * 	   b- If the company is not located in the lookup, then click "NEXT".<br>
     *  3. a- If this company is not owned, accept the defaults on the "Create New Company" screen and verify page initialized. <br>
     *  	  Click "NEXT". Verify "Company Added" screen initialized and click "NEXT"<br>
     *     b- If this company is owner, send request to company owner on "Join Company" and click "NEXT".<br>
     *	4. User is now on "Enter Keywords"<br>
     *  Fail test if any of the above does not happen.<br>
     *  NOTE: Option 2a requires the full company name to be given in companyName</p>
     * @param companyName = name of user's company (can be in the company lookup or not)
     * @return EnterKeywords
     */
    public EnterKeywords inputCompanyNameAndContinue(String companyName) {
        return inputCompanyNameAndContinue(companyName, null, null);
    }

    /***
     * <h1>inputCompanyNameAndContinue</h1>
     * <p>purpose: <br>
     * 	This is a quick method to navigate the user through the company lookup screen to the "Enter Keywords" screen.<br>
     *	Flow sequence:<br>
     *  1. Given companyName, enter the name into the company lookup.<br>
     * 	2. a- If the company is located in the lookup, then select and click "NEXT".<br>
     * 	   b- If the company is not located in the lookup, then click "NEXT".<br>
     *  3. a- If this company is not owned, accept the defaults on the "Create New Company" screen and verify page initialized. <br>
     *  	  Click "NEXT". Verify "Company Added" screen initialized and click "NEXT"<br>
     *     b- If this company is owner, send request to company owner on "Join Company" and click "NEXT".<br>
     *	4. User is now on "Enter Keywords"<br>
     *  Fail test if any of the above does not happen.<br>
     *  NOTE: Option 2a requires the full company name, company address (line 1), and city to locate the company</p>
     * @param companyName    = name of user's company (can be in the company lookup or not)
     * @param companyAddress = company address of the company
     * @param city           = company city
     * @return EnterKeywords
     */
    public EnterKeywords inputCompanyNameAndContinue(String companyName, String companyAddress, String city) {

    	// Enter company name into input
    	this.inputCompanyIntoCompanyLookupAsSingleString(companyName);
    
    	// Locate company in Company Lookup (if it exists). Click "NEXT" to continue
     	Boolean companyExistsAlready = this.selectCompanyName(companyName, companyAddress, city);
    
        // If the company was already in the lookup and already has a company owner
        if (companyExistsAlready) {
        	// Then join the company
        	// pages: "Join Company" 
            return new JoinCompany(driver).sendRequest();
        } else {
        	// Otherwise, this user is now the company owner
        	// and the company address will be set per this user's registration address
        	// so send user to "Create New Company" screen. Here, click "Next"
        	// pages: "Create New Company", "Company Added"
            return new CreateNewCompany(driver)
                    .acceptDefaultsAndContinue()
                    .verifyPageInitialization()
                    .clickNextButton();
        } 
    }
    
    public void inputCompanyName(String companyName, String companyAddress, String city, String state) {

    	// Enter company name into input
    	this.inputCompanyIntoCompanyLookupAsSingleString(companyName);
    
    	// Locate company in Company Lookup (if it exists). Click "NEXT" to continue
     	Boolean companyExistsAlready = this.selectCompanyName(companyName, companyAddress, city);
    
        // If the company was already in the lookup and already has a company owner
        if (companyExistsAlready) {
        	// Then join the company
        	// pages: "Join Company" 
           new JoinCompany(driver).sendRequest();
        } else{
        	new CreateNewCompany(driver).inputRandomState(state);
        	
        }
    }
    /***
     * <h1>inputCompanyIntoCompanyLookupAsSingleString</h1>
     * <p>purpose: Enter the companyName into the company input as a single string</p>
     * @param companyName = name of company to search Company Lookup
     * @return GetInvitedToBids
     */
    public GetInvitedToBids inputCompanyIntoCompanyLookupAsSingleString(String companyName) {

     	// Enter company name (as full string) into Company Lookup
        // pages: Company Lookup
        getCompanyNameInput().sendKeys(companyName);
        helperMethods.addSystemWait(1);
   	
    	return this;
    }
   
   /***
     * <h1>inputCompanyIntoCompanyLookupOneCharAtATime</h1>
     * <p>purpose: Enter companyName into the company lookup one char at a time. Each time a char<br>
     * 	is entered, verify that the lookup results are valid per:<br>
	 * 		1. Contain a company name, company URL, and company image<br>
	 * 		2. strSearch must be matched to either the company name or company URL<br>
	 * 		3. Company logo must be a valid image file<br>
	 * 	    4. Company URL is valid</p>
     * @param companyName
     * @return
     */
    public GetInvitedToBids inputCompanyIntoCompanyLookupOneCharAtATime(String companyName) {
		
		// Break into chars
    	String strCurrentInput = "";
		for (char c: companyName.toCharArray()) {
			
			// and simulate typing the strSearchTerm into Company Lookup input
			strCurrentInput += c;
			System.out.printf(GlobalVariables.getTestID() + " ---INFO: Current input into Company Lookup field is \"%s\"\n", strCurrentInput);
			this.getCompanyNameInput().sendKeys(Character.toString(c));
			
			// Give the Company Lookup a chance to refresh 
			// If this is the first time the lookup appears, verify it has initialized
			if (strCurrentInput.length() == 1) { this.waitForCompanyDropdownToAppear(strCurrentInput); }
			helperMethods.addSystemWait(2);
		}
			// Get the results of the Company Lookup
			List <CompanyLookupResult> theseSearchResults = this.getCompanyLookupResults();
			
			// 1. Verifying that we have returned results
			// If we have no returned results (i.e. we are inputting a new company), 
			// that's okay but only if we've previously returned results
			if (theseSearchResults.size() == 0 && strCurrentInput.length() <3) {
				fail("Test Failed: Company Lookup did not return search results for lookup term \"" + strCurrentInput + "\""+
					  " while trying to lookup company \"" + companyName + "\""); }	
			
			// 2. Verifying that our results are valid:
			//  a. Each result has a Company Name
			//  b. Each result has a Company Image
			//  c. Each results has a Company URL and it is of correct form
			//  d. The Company name or Company URL matches some part of the search term (whitespace between words can be ignored)
			for (CompanyLookupResult result: theseSearchResults) {
				this.verifyCompanyLookupResultValid(strCurrentInput, result); }

		

		return this;
    }
    
    /**
     * <h1>addNewCompanyNotInLookup</h1>
     * <p>purpose: Assuming user has entered a new company into the lookup<br>
     * 	verify that the company is not in the Company Lookup and then click "NEXT".<br>
     * 	Fail test if new company was located in the Company Lookup</p>
     * @param newCompanyName = new company name
     * @return CreateNewCompany
     */
    public CreateNewCompany addNewCompanyNotInLookup(String newCompanyName) {
     	Assert.assertFalse("Test Failed: \"" + newCompanyName + "\" was located in company lookup, even though it is a new company",
     							this.selectCompanyName(newCompanyName, null, null));

    	return new CreateNewCompany(driver);
    }
    
     /**
     * <h1>joinCompanyAlreadyInLookup</h1>
     * <p>purpose: Assuming user has entered an existing company into the lookup<br>
     * 	verify that the company is already listed in the Company Lookup and then click "NEXT".<br>
     * 	Fail test if new company was not located in the Company Lookup</p>
     * @param companyName = company name
     * @return JoinCompany
     */
    public JoinCompany joinCompanyAlreadyInLookup(String companyName) {
     	Assert.assertTrue("Test Failed: \"" + companyName + "\" was not located in company lookup, even though it is an existing company",
     							this.selectCompanyName(companyName, null, null));

    	return new JoinCompany(driver);
    }
    
	/* ------ verifications ------ */

	
   /***
     * <h1>verifyPageInitialization</h1>
     * <p>purpose: Verify that the "Get Invited to Bids" screen has initialized per:<br>
     * 	1. All page objects visible<br>
     * 	2. Input placeholder message visible</p>
     * @return GetInvitedToBids
     */
    public GetInvitedToBids verifyPageInitialization() {
    	
    	// Page Objects are visible
    	this.getTitle();
    	this.getSummaryTxt();
    	this.getCardTitle();
    	this.getCompanyNameInput();
    	this.getHint();
    	this.getNextButton();
    	this.getFooterEmail();
    	this.getFooterPhone();

    	// Place holder messages
    	Assert.assertTrue("Expected Placeholder message \"Company Name\" is not displayed in input", this.getCompanyNameInput().getAttribute("placeholder").equals("Company Name"));
    	
    	return this;
    }
    
    /***
     * <h1>verifyCompanyLookupIsClosed</h1>
     * <p>purpose: Verify that the Company Lookup is not visible, and that the "NEXT" button is active.<br>
     * 	Fail test if after 3 seconds, "NEXT" is inactive or Company Lookup is still visible/p>
     * @return GetInvitedToBids
     */
    public GetInvitedToBids verifyCompanyLookupIsClosed() {
        // Give the company lookup a chance to dismiss and verify that "NEXT" button is available
    	int time = DEFAULT_TIMEOUT;
        new WebDriverWait(driver, time)
            .withMessage("Test Timed Out: Company Lookup still visible \"" + time + "\" seconds expected close")
            .pollingEvery(Duration.ofMillis(100))
            .until(
                ExpectedConditions.and(
                    ExpectedConditions.invisibilityOfElementLocated(By.id(this.companyDropdown_id)),
                    ExpectedConditions.elementToBeClickable(By.id(this.nextButton_id))
                )
            );

        System.out.println(GlobalVariables.getTestID() + " INFO: Verified Company Lookup has closed");
        return this;
    }
    

}
