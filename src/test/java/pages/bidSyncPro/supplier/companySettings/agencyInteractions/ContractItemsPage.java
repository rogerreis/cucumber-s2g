package pages.bidSyncPro.supplier.companySettings.agencyInteractions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import pages.common.pageObjects.toBeDeprecated.Document;

/**
 * @author jlara
 * <p>TODO: Refactor this class</p>
 */
public class ContractItemsPage extends Document{

	private final String xpath = "//form[@name='theform']";
	private final String xpathSearchContractItemsHdrLabel = xpath + "//table//th[contains(.,'Search Contract Items')]";
	//labels
    private final String xpathSearchForLabel = xpath +"//table//td[contains(.,'Search For')]";
    
    private final String xpathContractTypeLabel = xpath +"//table//td[contains(.,'Contract Type')]";
    private final String xpathClassificationsLabel = xpath +"//table//td[contains(.,'Classifications')]";

	//inputs, radio buttons, select, submit
    private final String xpathSearchForTextBox = xpath +"//input[@name='srch' and @type='text']";
    
    private final String xpathSearchTypeTitleRadioButton = xpath +"//input[@name='srchtype' and @value='1' and @type='radio']";
    private final String xpathSearchTypeProductCodeRadioButton = xpath +"//input[@name='srchtype' and @value='3' and @type='radio']";
    private final String xpathSearchTypeContractNumberRadioButton = xpath +"//input[@name='srchtype' and @value='4' and @type='radio']";
    
    private final String xpathContractTypeSelect = xpath +"//select[@name='contracttype']";
    private final String xpathClassificationsTextBox = xpath +"//input[@name='catsrch' and @type='text']";
    private final String xpathSearchButton = xpath +"//a[text()='Search']";
	
    
    private final String xpathResults = "//form[@name='cartform']//form[@name='cartform']/table/tbody";
    
    
    private final String xpathProductCodeResultsHdr = xpathResults + "/tr[1]/th[1]/*[contains(.,'Product Code')]";

    private final String xpathTitleResultsHdr = xpathResults + "/tr[1]/th[2]/*[contains(.,'Title')]";

    private final String xpathVendorResultsHdr = xpathResults + "/tr[1]/th[3]/*[contains(.,'Vendor')]";

    private final String xpathContractResultsHdr = xpathResults + "/tr[1]/th[4]/*[contains(.,'Contract')]";

    private final String xpathPriceResultsHdr = xpathResults + "/tr[1]/th[5]/*[contains(.,'Price')]";
    
    private final String xpathResultRow = xpathResults + "/tr[<ROW>]"; 
    
    private final String xpathPaginationPreviousLink = xpathResults + "/div[2]/font/strong[text()='Previous']";
    private final String xpathPaginationNextLink = xpathResults + "/div[2]/a[5]";
    
	public ContractItemsPage(EventFiringWebDriver driver) throws Throwable {
		super(driver);
		
		//this is the url of the parent page. this page and its elements are contained in an iframe
		this.setPagePath(System.getProperty("supplierBaseURL") + "admin/agency-interaction");
        
      //iframe[@src='https://stage.bidsync.com/DPXOrder?ac=supplierordersforpunchout&atmnav=myorders.orders']
	} // end of constructor

	
	public ContractItemsPage sendKeysSearchForTextBox(CharSequence... keys) {
		this.getSearchForTextBox().sendKeys(keys);
		return this;
	}

	public ContractItemsPage clickSearchTypeTitleRadioButton() {	
		getSearchTypeTitleRadioButton().click();
		return this;	
	}

	public ContractItemsPage clickSearchTypeProductCodeRadioButton() {	
		getSearchTypeProductCodeRadioButton().click();
		return this;	
	}

	public ContractItemsPage clickSearchTypeContractNumberRadioButton() {	
		getSearchTypeContractNumberRadioButton().click();
		return this;	
	}
	
	public ContractItemsPage selectContractTypeOptionByValue(String optionValue) {
		getContractTypeSelect().selectByValue(optionValue); //{ "Standard":1 } seems the be the only key/value pair 
		return this;
	} 
	
	public ContractItemsPage selectContractTypeOptionByText(String optionText) {
		getContractTypeSelect().selectByVisibleText(optionText); //{ "Standard":1 } seems the be the only key/value pair
		return this;
	} 
	
	public ContractItemsPage sendKeysClassificationsTextBox(CharSequence... keys) {
		this.getClassificationsTextBox().sendKeys(keys);
		return this;
	}
	
	public ContractItemsPage clickPaginationPreviousLink() {	
		getPaginationPreviousLink().click();
		return this;	
	}
	public ContractItemsPage clickPaginationNextLink() {	
		getPaginationNextLink().click();
		return this;	
	}
	public ContractItemsPage clickSearchButton() {	
		getSearchButton().click();
		return this;	
	}
	
	public WebElement getSearchContractItemsHdrLabel() {	return driver.findElement(By.xpath(xpathSearchContractItemsHdrLabel));	}

	public WebElement getSearchForLabel() {	return driver.findElement(By.xpath(xpathSearchForLabel));	}

	public WebElement getContractTypeLabel() {	return driver.findElement(By.xpath(xpathContractTypeLabel));	}

	public WebElement getClassificationsLabel() {	return driver.findElement(By.xpath(xpathClassificationsLabel));	}

	public WebElement getSearchForTextBox() {	return driver.findElement(By.xpath(xpathSearchForTextBox));	}

	public WebElement getSearchTypeTitleRadioButton() {	return driver.findElement(By.xpath(xpathSearchTypeTitleRadioButton));	}

	public WebElement getSearchTypeProductCodeRadioButton() {	return driver.findElement(By.xpath(xpathSearchTypeProductCodeRadioButton));	}

	public WebElement getSearchTypeContractNumberRadioButton() {	return driver.findElement(By.xpath(xpathSearchTypeContractNumberRadioButton));	}

	public Select getContractTypeSelect() {	return new Select(driver.findElement(By.xpath(xpathContractTypeSelect)));	}

	public WebElement getClassificationsTextBox() {	return driver.findElement(By.xpath(xpathClassificationsTextBox));	}

	public WebElement getSearchButton() {	return driver.findElement(By.xpath(xpathSearchButton));	}

	public WebElement getProductCodeResultsHdr() {	return driver.findElement(By.xpath(xpathProductCodeResultsHdr));	}

	public WebElement getTitleResultsHdr() {	return driver.findElement(By.xpath(xpathTitleResultsHdr));	}
	
	public WebElement getVendorResultsHdr() {	return driver.findElement(By.xpath(xpathVendorResultsHdr));	}

	public WebElement getContractResultsHdr() {	return driver.findElement(By.xpath(xpathContractResultsHdr));	}

	public WebElement getPriceResultsHdr() {	return driver.findElement(By.xpath(xpathPriceResultsHdr));	}

	public WebElement getResultRow(int rowNumber) {	//rowNumber starts at 1
		if( rowNumber < 2 ) rowNumber = 2; //default to 2. first data row's tr is the second one
		return driver.findElement(By.xpath(xpathResultRow.replace("<ROW>", String.valueOf(rowNumber-1))));	
	}

	public WebElement getPaginationPreviousLink() {	return driver.findElement(By.xpath(xpathPaginationPreviousLink));	}

	public WebElement getPaginationNextLink() {	return driver.findElement(By.xpath(xpathPaginationNextLink));	}

}
