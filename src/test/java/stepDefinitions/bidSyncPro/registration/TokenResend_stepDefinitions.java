package stepDefinitions.bidSyncPro.registration;

import static org.junit.Assert.fail;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.bidSyncPro.registration.TokenResend;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.mailinator.Mailinator;

/**
 * Step Definition file related to Token Resend process
 * @author ssomaraj
 *
 */
public class TokenResend_stepDefinitions {

	public EventFiringWebDriver driver;
	TokenResend tokenResend;

	public TokenResend_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
		tokenResend = new TokenResend(driver);
	}

	@And("I should see an incomplete registration popup with resend verification email option$")
	public void iShouldSeeIncompletRegistrationPopupWithResendVerificationEmailOption() {
		try {
			System.out.println(
					"---TEST STEP: I should see incomplete registration popup with resend verification email option");
			tokenResend.verifyIncompleteRegistrationPopup();
			tokenResend.getResendVerificationEmailBtn().click();
		} catch (Exception exp) {
			fail("FAILED : Not able to see incomplete registration popup" + exp);
		}
	}
	
	@And("I should see an incomplete registration popup with resend verification email option on reset password$")
	public void iShouldSeeIncompletRegistrationPopupWithResendVerificationEmailOptionOnResetPassword() {
		try {
			System.out.println(
					"---TEST STEP: I should see incomplete registration popup with resend verification email option on reset password");
			tokenResend.verifyIncompleteRegistrationPopupOnResetPassword();
			tokenResend.getResendVerificationEmailBtnOnResetPassword().click();
		} catch (Exception exp) {
			fail("FAILED : Not able to see incomplete registration popup on reset password" + exp);
		}
	}
	
	@Then("I should see the email sent popup message once I resent the verification email$")
	public void i_should_see_the_email_sent_message_once_I_resent_the_verification_email() {
		try {
			System.out.println(
					"---TEST STEP: I should see the email sent message once I resent the verification email");
			tokenResend.verifyIncompleteRegistrationPopupAfterSentMail();
			tokenResend.getCloseBtn().click();
		} catch (Exception exp) {
			fail("FAILED : Not able tovalidate the email sent message" + exp);
		}
	}
	
	@Then("I should see the email sent popup message once I resent the verification email on reset password$")
	public void i_should_see_the_email_sent_message_once_I_resent_the_verification_email_on_reset_password() {
		try {
			System.out.println(
					"---TEST STEP: I should see the email sent message once I resent the verification email on reset password");
			tokenResend.verifyIncompleteRegistrationPopupAfterSentMailOnResetPassword();
			tokenResend.getCloseBtnOnResetPassword().click();
		} catch (Exception exp) {
			fail("FAILED : Not able to validate the email sent message  on reset password" + exp);
		}
	}
	
	@And("I should be redirected to complete the registration by selecting the link on email$")
	public void i_should_be_redirected_to_complete_the_registration_by_selecting_the_link_on_email() {
		try {
			System.out.println(
					"---TEST STEP: I should be redirected to complete the registration by selecting the link on email$");
			RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
			
		    new Mailinator(driver)
		        		.navigateToMailinatorInbox(user.getEmailAddress())
		                .openRegistrationCompleteEmail()
		                .clickOnTokenURLInEmail()
		                .switchToWindow("S2G");
		    new HelperMethods().addSystemWait(2);
		    tokenResend.verifyRedirectionToRegistrationPage();
		} catch (Exception exp) {
			fail("FAILED : Not able to complete registration from the email" + exp);
		}
	}
	
}
