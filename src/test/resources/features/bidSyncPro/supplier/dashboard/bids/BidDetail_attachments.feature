@ignore
Feature: Dashboard > Bid Detail - Bid Detail Attachments
dmidura: Attachments are opening in a new tab rather than a download window. 
Tests should be updated to account for this. Turning off this feature until these tests can be refactored.
  
  Scenario: Verify Bid Detail attachments - Parsehub
    1) query the database for a recently created bid that has attached documents
    2) search for the bid using Bid Search
    3) Verify the links are present

    Given I have collected the result for query "select solicitation_central_id from solicitation_central where json similar to '%(bidDocuments)%' AND json similar to '%(PARSEHUB_EXTRACT)%' limit 1" against database "survivor" and put it in a global variable named "glbBidID"
    And I have logged into the Supplier side as user "National FMC"
    And I navigate to the bid detail page where the bid ID matches that of global variable "glbBidID"
    When I click the link for the first document
    Then a download should begin

	@ignore
  Scenario: Verify Bid Detail attachments - BuySpeed
  	mandrews: ignoring this for now.  BuySpeed attachments could be many different types with different behaviors
    1) query the database for a recently created bid that has attached documents
    2) search for the bid using Bid Search
    3) Verify the links are present

    Given I have collected the result for query "select solicitation_central_id from solicitation_central where json similar to '%(bidDocuments)%' AND json similar to '%(BSO)%' limit 1" against database "survivor" and put it in a global variable named "glbBidID"
    And I navigate my browser to the supplier login page
    And I enter my Email as "subsNatFedMilCan@phimail.mailinator.com" and Password as "Test_1234"
    Then I will be on the supplier Home page
    Given I navigate to the bid detail page where the bid ID matches that of global variable "glbBidID"
    When I click the link for the first document
    Then a download should begin
