package pages.chargebeeSite.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.chargebeeSite.ChargebeeLoginPage;
import pages.chargebeeSite.customers.ChargebeeCustomersDashboard;
import pages.chargebeeSite.subscriptions.ChargebeeSubscriptionsDashboard;
import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>CommonNavSliderMenu</h1>
 * @author dmidura
 * <p>details: This class houses methods and page objects associated with the left hand nav bar<br>
 * 	that is common on almost all Chargebee test site pages. This class should be inherited.</p>
 */
public class CommonNavSliderMenu extends BasePageActions{
	
	// xpaths -- add as needed
	// Nav buttons
	private final String customersButton_xpath     = "//div[@id='tab-customers']//a";
	private final String subscriptionsButton_xpath = "//div[@id='tab-subscriptions']//a";
	private final String accountButton_xpath       = "//div[@class='cv-user__account']//a";
	
	// For account dropdown - split to new class if start needing more buttons from account
	private final String logoutButton_xpath        = "//div[@class='cv-menu']//a[contains(., 'Sign out')]";

	// Messages
	private final String genericSuccessMsg_xpath              = "//div[@class='cn-toast']";
	private final String successChangeSubscriptionMessage     = "Successfully changed the subscription.";
	private final String successCancellationMessage           = "Successfully cancelled the subscription.";

	// Other
	private EventFiringWebDriver driver;
	
	public CommonNavSliderMenu(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	// getters
	public WebElement getCustomersButton()     { return findByVisibility(By.xpath(this.customersButton_xpath));     }
	public WebElement getSubscriptionsButton() { return findByVisibility(By.xpath(this.subscriptionsButton_xpath)); }
	public WebElement getAccountButton()       { return findByVisibility(By.xpath(this.accountButton_xpath));       }

	public WebElement getLogoutButton()        { return findByVisibility(By.xpath(this.logoutButton_xpath));        }
	
	/*----- methods ----- */
	/***
	 * <h1>clickOnSubscriptionsButton</h1>
	 * <p>purpose: Click on the Subscriptions Button to navigate to the Subscriptions Dashboard</p>
	 * @return ChargebeeSubscriptionsDashboard
	 */
	public ChargebeeSubscriptionsDashboard clickOnSubscriptionsButton() {
		this.getSubscriptionsButton().click();
		return new ChargebeeSubscriptionsDashboard(driver);
	}
	
	/***
	 * <h1>clickOnCustomersButton</h1>
	 * <p>purpose: Click on the Customers Button to navigate to the Customers Dashboard</p>
	 * @return ChargebeeCustomersDashboard
	 */
	public ChargebeeCustomersDashboard clickOnCustomerButton() {
		this.getCustomersButton().click();
		return new ChargebeeCustomersDashboard(driver);
	}
	
	/***
	 * <h1>logout</h1>
	 * <p>purpose: Click on the "Account" button. After account menu appears, click "Sign Out"</p>
	 * @return ChargebeeLoginPage
	 */
	public ChargebeeLoginPage logout() {
		this.getAccountButton().click();
		this.getLogoutButton().click();
		return new ChargebeeLoginPage(driver);
	}

	/*----- verifications ------*/
	
	/***
	 * <h1>verifyMenuLoad</h1>
	 * <p>purpose: Verify that the Common Nav Slider Menu has loaded
	 * 	by searching for each expected page object in the menu (or at
	 * 	least the objects that we care about). Fail test if these objects
	 * 	are not visible</p>
	 * @return CommonNavSliderMenu
	 */
	public CommonNavSliderMenu verifyMenuLoad() {
		this.getCustomersButton();
		this.getAccountButton();
		this.getSubscriptionsButton();
		return this;
	}
	
	/***
	 * <h1>verifySuccessStatusMsg</h1>
	 * <p>purpose: Verify that the success status message appears and then disappears.
	 * 	As this message can obscure the Account button (to logout), this method can be
	 * 	used to verify that a logout can take place.
	 * @return CommonNavSliderMenu
	 */
	public CommonNavSliderMenu verifySuccessChangingSubscriptionMsg() {
		return this.verifyGenericMsg(this.successChangeSubscriptionMessage);
	}
	
	/***
	 * <h1>verifySuccessStatusMsg</h1>
	 * <p>purpose: Verify that the success cancellation status message appears and then disappears.
	 * 	As this message can obscure the Account button (to logout), this method can be
	 * 	used to verify that a logout can take place.
	 * @return CommonNavSliderMenu
	 */
	public CommonNavSliderMenu verifySuccessCancellingSubscriptionMsg() {
		return this.verifyGenericMsg(this.successCancellationMessage);
	}
	
	/**
	 * <h1>verifyGenericMsg</h1>
	 * <p>purpose: Helper to verify Chargebee status messages appear and then clear
	 * @param msg = text message that should display
	 * @return CommonNavSliderMenu
	 */
	private CommonNavSliderMenu verifyGenericMsg(String msg) {
		int time = DEFAULT_TIMEOUT;
		String xpath = String.format("%s[contains(.,'%s')]", this.genericSuccessMsg_xpath, msg);
		System.out.println(GlobalVariables.getTestID() + " INFO: Verifying \"" + msg + "\" status msg displays in Chargebee test site");

		// Display message
		String expected = "display: inline-block;";
		new WebDriverWait(driver, time)
			.withMessage("ERROR: Status message is not visible after \"" + time + "\" seconds")
			.until(ExpectedConditions.attributeToBe(By.xpath(xpath), "style", expected));

		// Verify message hides
		expected = "display: none;";
		new WebDriverWait(driver, time)
			.withMessage("ERROR: Status message is still visible after \"" + time + "\" seconds")
			.until(ExpectedConditions.attributeToBe(By.xpath(xpath), "style", expected));

		return this;
	}

}
