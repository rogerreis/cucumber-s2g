package pages.bidSyncPro.supplier.common;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.common.BidSyncProCommon;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidListRow;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterCriteria;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterResultsSectionNewClass;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

/***
 * <h1>Class CommonTopBar</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects related to the common top bar elements on the Supplier dashboard and Supplier admin pages.<br>
 * 	Included Objects: Menu bar (with all options), Search bar + Search Functionality, Functionality for Notifications, Functionality for Logout, <br>
 * 		BidSync logo</p>
 */
public class CommonTopBar extends BidSyncProCommon {
	
	// xpaths
    private final String bidSyncBtnImg_xpath         = "//a[@id='alogo']//img[@alt='Periscope S2G']";
    
    // Top Bar Search
    private final String searchBidsInput_id          = "searchBids";
    private final String searchBtn_xpath             = "//input[@id='searchBids']/../button[contains(span,'search')]";
    
    // Nav menu
    private final String bidListBtn_id               = "aBidList";
    private final String bidListBtnImg_xpath         = "//button[@id='aBidList']//mat-icon[@role='img' and contains(.,'list')]";
    private final String companySettingsBtn_id       = "aApps";
    private final String companySettingsBtnImg_xpath = "//button[@id='aApps']//mat-icon[@role='img' and contains(.,'domain')]";
    private final String accountBtn_id               = "aUserMenu";
    private final String accountBtnImg_xpath         = "//button[@id='aUserMenu']//mat-icon[@role='img' and contains(.,'person')]";
    
    private final String addedKeyword_xpath 		 = "//mat-chip/span[contains(text(),'#keyword#')]"; 
	// Other
    protected EventFiringWebDriver driver;
	protected HelperMethods helperMethods;

	

	public CommonTopBar(EventFiringWebDriver driver) {
		super(driver);
		this.driver         = driver;
		helperMethods       = new HelperMethods();
	}
	
	/* ----- getters ----- */
    
    // Top Bar Search
    private WebElement getBidSyncBtnImg()         { return findByVisibility(By.xpath(this.bidSyncBtnImg_xpath));        }
    private WebElement getSearchBidsInput()       { return findByVisibility(By.id(this.searchBidsInput_id));            }
    private WebElement getSearchBtn()             { return findByVisibility(By.xpath(this.searchBtn_xpath));            }
    
    // Nav Menu
    private WebElement getBidListBtn()            { return findByVisibility(By.id(this.bidListBtn_id));                 }
    private WebElement getBidListBtnImg()         { return findByVisibility(By.xpath(this.bidListBtnImg_xpath));        }
    private WebElement getCompanySettingsBtn()    { return findByVisibility(By.id(this.companySettingsBtn_id));         }
    private WebElement getCompanySettingsBtnImg() { return findByVisibility(By.xpath(this.companySettingsBtnImg_xpath));}
    private WebElement getAccountBtn()            { return findByVisibility(By.id(this.accountBtn_id));                 }
    private WebElement getAccountBtnImg()         { return findByVisibility(By.xpath(this.accountBtnImg_xpath));        }
    private WebElement getAddedKeyword(String keyword) { return findByVisibility(By.xpath(this.addedKeyword_xpath.replace("#keyword#", keyword)));                }

	/* --------------- Methods --------------- */
    

	/***
	* <h1>getSearchBidsCurrentText</h1>
	* <p>purpose: Get the text that is currently displaying in the Search Bids input</p>
	* @return String = current text displaying in the Search Bids input
	*/
	private String getSearchBidsCurrentText() {
        return this.getSearchBidsInput().getAttribute("value");
	}
	
	/***
	* <h1>getSearchBidsDefaultText</h1>
	* <p>purpose: Return the default text that Search Bids input should initialize with</p>
	* @return String = default text for Search Bids input
	*/
	public String getSearchBidsDefaultText() {
		String strText =  getSearchBidsInput().getAttribute("placeholder");
		System.out.printf(GlobalVariables.getTestID() + " INFO: Retrieved default text for Search Bids input field = \"%s\"\n", strText);
		return strText;
	}
	
	/***
	 * <h1>getBidListButtonToolTip</h1>
	 * <p>purpose: Get value of tool tip for Bid List nav menu</p>
	 * @return String = current tooltip text
	 */
	private String getBidListButtonToolTip() {
	    return this.getBidListBtn().getAttribute("mattooltip"); }
    
	/***
	 * <h1>getCompanySettingsButtonToolTip</h1>
	 * <p>purpose: Get value of tool tip for Company Settings nav menu</p>
	 * @return String = current tooltip text
	 */
    private String getCompanySettingsButtonToolTip() {
        return this.getCompanySettingsBtn().getAttribute("mattooltip"); }
    
	/***
	 * <h1>getAccountButtonToolTip</h1>
	 * <p>purpose: Get value of tool tip for Account nav menu</p>
	 * @return String = current tooltip text
	 */
    private String getAccountButtonToolTip() {
        return this.getAccountBtn().getAttribute("mattooltip"); }
    
     /***
     * <h1>openBidListDropdown</h1>
     * <p>purpose: Open the "Bid List" nav menu</p>
     * @return BidListDropdown
     */
    public BidListDropDown openBidListDropDown() {
        this.getBidListBtn().click();
        return new BidListDropDown(driver);
    }
    
    /***
     * <h1>openCompanySettingsDropdown</h1>
     * <p>purpose: Open the "Company Settings" nav menu</p>
     * @return CompanySettingsDropDown
     */
    public CompanySettingsDropDown openCompanySettingsDropDown() {
        this.getCompanySettingsBtn().click();
        return new CompanySettingsDropDown(driver);
    }
    
    /***
     * <h1>openAccountDropDown</h1>
     * <p>purpose: Open the "Account" nav menu</p>
     * @return AccountDropdown
     */
    public AccountDropDown openAccountDropDown() {
	    this.getAccountBtn().click();
	    return new AccountDropDown(driver);
    }
    
    /***
     * <h1>logout</h1>
     * <p>purpose: Quick step to click on the Account dropdown and select "logout".<br>
     * 	Verify that user is logged out. Fail test if user does not log out</p>
     * @return SupplierLogin
     * @throws InterruptedException 
     */
    public SupplierLogin logout() throws InterruptedException {
//    	#Fix for release OCT 7
//    	Added system wait to avoid intermittent error
    	helperMethods.addSystemWait(5);
    	this.openAccountDropDown()
    	.clickLogout();
    	// We need to verify the logout here to ensure that the JWT token was cleared
    	return new SupplierLogin(driver).verifyURLIsLoginScreen();
    }
    
    /***
     * <h1>searchFor</h1>
     * <p>purpose: Performs a search for the given string.<br>
     * 	Top Bar Search input will be first cleared. searchTerm will be entered into <br>
     * 	Top Bar Search field. Search button will be clicked. This method will then wait<br>
     * 	for the spinner to finish.</p>
     * @param searchTerm - the string to search for
     * @return BidList (for access to the returned bids)
     */
    public BidList searchFor(String searchTerm) {
    	// If we are spinning for some reason, we need to let everything
    	// complete before trying a new search or Pro can wipe out our top bar search results
        waitForSpinner(driver);
        
        // Search
    	this.getSearchBidsInput().clear();
        this.getSearchBidsInput().sendKeys(searchTerm);
        this.getSearchBtn().click();
        
        // if search term is over 25 characters, then truncate like the UI does
        if (searchTerm.length() > 25) {
        	searchTerm = searchTerm.substring(0, 25) + "..."; 
        }
        
        this.getAddedKeyword(searchTerm);
        helperMethods.addSystemWait(1); 
        waitForSpinner(driver);
        return new BidList(driver);
    }
    
    /***
     * <h1>waitForPageLoad</h1>
     * <p>purpose: Wait for the common top bar to load, by waiting for the top search input to be visible</p>
     * @return CommonTopBar
     */
    public CommonTopBar waitForPageLoad() {
    	waitForVisibility(By.id(this.searchBidsInput_id), Integer.parseInt(System.getProperty("defaultTimeOut")));
    	return this;
    }

	/* --------------- Verifications --------------- */
	/***
	* <h1>verifyNavMenuOptions</h1>
	* <p>purpose: Given the name of a nav menu, verify that all the menus options are visible<br>
	*    Note: The menu must be OPENED already in order to run this method</p>
	* @param menuName = CommonTopBarMenuNames
	* return CommonTopBar
	*/
	public CommonTopBar verifyNavMenuOptions (CommonTopBarMenuNames menuName) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Registering the \"%s\" Navigation Menu PageElements.\n", menuName.toString());

        switch(menuName) {
            case COMPANY_SETTINGS:
                new CompanySettingsDropDown(driver).verifyAllOptionsInCompanySettingsDropdown();
                break;
            case ACCOUNT:
                new AccountDropDown(driver).verifyAllOptionsInAccountDropdown();
                break;
            case BID_LIST:
                new BidListDropDown(driver).verifyAllOptionsInBidListDropdown();
                break;
            case MARKET_PLACE:
                new Marketplace(driver).verifyMarketPlaceRedirect();
                break;
            default:
                Assert.fail(String.format("No nav button '%s' found.", menuName.toString()));
        }

        return this;
    }
	
	/**
	 * <h1>verifyToolTipsAppearForNavMenu</h1> 
	 * <p>purpose: Verify that tool tips appear for nav menus.<br>
	 * 	Fail if they don't or if text is incorrect</p>
	 * @return CommonTopBar
	 */
	public CommonTopBar verifyToolTipsAppearForNavMenu() {
		if (!this.getAccountButtonToolTip().equals("Account")) {
			fail("Test Failed: Account nav bar does not display \"Account\" for tool tip"); }

		if (!this.getBidListButtonToolTip().equals("Bid List")) {
			fail("Test Failed: Bid List nav bar does not display \"Bid List\" for tool tip"); }

		if (!this.getCompanySettingsButtonToolTip().equals("Company Settings")) {
			fail("Test Failed: Company Settings nav bar does not display \"Company Settings\" for tool tip"); }
		
		return this;
	}

	 /***
     * <h1>verifyTopBarSearchDisplaysCorrectResultsInAllBidsTab</h1>
     * <p>purpose: Verify that the search keyword entered into the Top Bar input
     * 	returns correct results. Verify also that the Top Bar input is blank (after searching)
     * 	and that the Filter Results section is now visible and displaying the Top Bar search
     * 	term as a positive keyword. The keyword should be the ONLY filter in Filter Results.<b>r
     * 	Fail test if the a bid title does not match the keyword and the bid is not in
     * 	"UPGRADE PLAN" mode. Fail test if no bids are returned. Fail test if Top Bar search is
     * 	not blank. Fail test if Filter Results is closed, or if Filter Results does not show
     * 	the Top Bar search term as a positive keyword with no other filters set</p>
     * @param strKeyword = keyword that was entered into the Top Bar search input
     * @return BidList = the BidList we just searched
     */
    public BidList verifyTopBarSearchDisplaysCorrectResultsInAllBidsTab (String strKeyword) {
    	BidList bidList = new BidList(this.driver);
    	
    	// The Top Bar search term should be deleted
    	this.verifyTopBarSearchTermIsTerm("");
    	
    	// Filter Results section should be open
    	// and should just display the Top Bar search keyword as a positive keyword
    	List<String> keywords = new ArrayList<>();
    	keywords.add(strKeyword);
    	FilterCriteria filter = new FilterCriteria().setKeywords(keywords);
    	
    	new FilterResultsSectionNewClass(driver)
    		.verifyFilterResultsSectionOpen()
    		.getFilterResultsSectionNewClass()
    		.verifyFilterResultsDisplaysUnsavedFilter(filter);

    	// Make sure that we are actually getting bids back. Fail test if not.
    	bidList.verifyMinimumNumberOfBidResults(1);

		// Verify that each result in the bid list somehow relates to the keyword in the Top Bar search
    	for (BidListRow bidResult : bidList.allVisibleBids()) {
			System.out.println(GlobalVariables.getTestID() + " INFO: Verifying that \"" + bidResult.bidInfo.getBidTitle() + "\" bid result matches bid search keyword");
			Boolean bMatchesKeyword = false;

			if(bidResult.bidInfo.getBidTitle().toLowerCase().contains(strKeyword.toLowerCase())) { 
				System.out.println(GlobalVariables.getTestID() + " INFO: Keyword match for " + strKeyword);
				bMatchesKeyword = true; }

			// If the title is cut off for because we're in upgrade plan mode, then we'll assume a match to be safe
			if (!bMatchesKeyword & bidResult.isUpgradePlan()) { 
				System.out.println(GlobalVariables.getTestID() + " INFO: Plan is being upgraded. Assuming match b/c title is cut off.");
				bMatchesKeyword = true; }
			
			if (!bMatchesKeyword) { fail("Test Failed: " + bidResult.bidInfo.getBidTitle() + " does not contain any keyword from Top Bar search"); }
    	}
    	
    	return bidList;
    }
    
    /***
     * <h1>verifyCommonTopBarLoaded</h1>
     * <p>purpose: Verify that each object in the common top bar has loaded. These include:<br>
     * 	1. BidSync Button + image<br>
     * 	2. Top bar search field and button + image<br>
     * 	3. Bid List, Account, Company Settings dropdowns + images</p>
     * @return CommonTopBar
     */
    public CommonTopBar verifyCommonTopBarLoaded() {
    	
    	// BidSync Button
    	this.getBidSyncBtnImg();
    	
    	// Top Bar Search
    	this.getSearchBidsInput();
    	this.getSearchBtn();
    	
    	// Nav Menu
    	this.getBidListBtn();
    	this.getBidListBtnImg();
    	this.getAccountBtn();
    	this.getAccountBtnImg();
    	this.getCompanySettingsBtn();
    	this.getCompanySettingsBtnImg();
    	
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified Common Top Bar objects initialized");
    	return this;
    }
    
    /***
     * <h1>verifyTopBarSearchTermIsTerm</h1>
     * <p>purpose: Verify that the search term in the Top Bar currently displays as expectedSearchTerm</p>
     * @param expectedSearchTerm = term that should be displayed in the Top Bar search (b/c user entered this term)
     * @return CommonTopBar
     */
    public CommonTopBar verifyTopBarSearchTermIsTerm(String expectedSearchTerm) {

    	String displayedSearchTerm = this.getSearchBidsCurrentText();
    	if (!displayedSearchTerm.equals(expectedSearchTerm)) {
    		fail("Test Failed: Expected Top bar search term to be \"" + expectedSearchTerm + "\"," +
    			  "but currently displayed Top Bar Search term is \"" + displayedSearchTerm + "\""); }
    	
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified Top Bar Search term currently displays as \"" + expectedSearchTerm + "\"");
    	return this;
    }
       
}
