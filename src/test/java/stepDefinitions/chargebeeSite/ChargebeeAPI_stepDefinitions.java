package stepDefinitions.chargebeeSite;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.chargebee.Environment;
import com.chargebee.ListResult;
import com.chargebee.ListResult.Entry;
import com.chargebee.Result;
import com.chargebee.models.Addon;
import com.chargebee.models.Addon.Status;
import com.chargebee.models.Card;
import com.chargebee.models.Customer;
import com.chargebee.models.Plan;
import com.chargebee.models.Plan.AddonApplicability;
import com.chargebee.models.Plan.ApplicableAddon;
import com.chargebee.models.Subscription;
import com.chargebee.models.Subscription.UpdateRequest;
import com.chargebee.models.enums.AutoCollection;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.bidSyncPro.registration.RandomChargebeeUser;
import data.bidSyncPro.registration.SubscriptionGrouping;
import data.bidSyncPro.registration.SubscriptionPlan;
import junit.framework.Assert;
import pages.bidSyncPro.registration.DatabaseRegistration;
import pages.bidSyncPro.supplier.companySettings.manageSubscriptions.ManageSubscriptions;
import pages.bidSyncPro.supplier.companySettings.manageSubscriptions.ManageSubscriptionsChargebeePopup;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.helpers.RandomHelpers;

@SuppressWarnings("deprecation")
public class ChargebeeAPI_stepDefinitions {
	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;
	private SupplierLogin supplierLogin;
	HashMap<SubscriptionKeys, String> subscriptionUpdateKeys = new HashMap<SubscriptionKeys, String>();
	int i = 0;
	private RandomHelpers randomHelpers;

	public ChargebeeAPI_stepDefinitions(CoreAutomation automation) {
		// this.automation = automation;
		this.driver = automation.getDriver();
		helperMethods = new HelperMethods();
		supplierLogin = new SupplierLogin(driver);
		randomHelpers = new RandomHelpers();
		Environment.configure(System.getProperty("chargeBeeSite"), System.getProperty("chargeBeeApiKey"));
	}

	/* ----- helpers ----- */

	/***
	 * <h1>login</h1>
	 * <p>
	 * purpose: Log into Chargebee test site with admin credentials. Navigate to
	 * Subscriptions dashboard and verify navigation
	 * </p>
	 * 
	 * @return ChargebeeSubscriptionsDashboard
	 * @throws Exception
	 */
	public void chargeBeeSubscriptionsList(String planId) throws Exception {
		ListResult result = Subscription.list().limit(2).planId().in(planId).request();
		for (ListResult.Entry entry : result) {
			Subscription subscription = entry.subscription();
			Customer customer = entry.customer();
			Card card = entry.card();
			System.out.println(entry);
		}
		System.out.println(GlobalVariables.getTestID() + " ");
	}

	public Subscription getChargeBeeSubscription() throws Exception {
		Entry result = Subscription.list().limit(1).request().listIterator().next();
		return result.subscription();
	}

	public ListResult getSubscriptionListForUserEmail(String email) throws Exception {
		Entry s = Customer.list().email().is(email).request().listIterator().next();
		ListResult result = Subscription.list().limit(10).customerId().is(s.customer().id()).request();
		return result;
	}

	public Result cancelSubscription(Subscription subscription) throws Exception {
		Result result = subscription.cancel(subscription.id()).endOfTerm(false).request();
		return result;
	}

	public Subscription getSubscriptionForUserEmail(String email) throws Exception {
		Entry s = Customer.list().email().is(email).request().listIterator().next();
		ListResult result = Subscription.list().limit(1).customerId().is(s.customer().id()).request();
		return result.listIterator().next().subscription();
	}

	public Customer getCustomerById(String customerId) throws Exception {
		return Customer.list().id().is(customerId).request().listIterator().next().customer();
	}

	public Customer getCustomerByEmail(String customerEmail) throws Exception {
		return Customer.list().email().is(customerEmail).request().listIterator().next().customer();
	}

	public Result updateSubscription(Subscription subscription, Map<SubscriptionKeys, String> map) throws Exception {
		UpdateRequest request = Subscription.update(subscription.id());
		int addonId = 0;
		Iterator<java.util.Map.Entry<SubscriptionKeys, String>> mapIterator = map.entrySet().iterator();

		while (mapIterator.hasNext()) {
			Map.Entry element = (Map.Entry) mapIterator.next();
			switch (element.getKey().toString()) {
			case "planId":
				request = request.planId(element.getValue().toString());
				break;
			case "addonId":
				request = request.addonId(addonId, element.getValue().toString());
				request = request.addonQuantity(addonId, 1);
				addonId++;
				break;
			case "cf_additional_state":
				int i = 1;
				for (String state : element.getValue().toString().split(","))
					request = request.param("cf_additional_state_" + i++, state);
				break;
			case "endOfTerm":
				request = request.endOfTerm(Boolean.valueOf(element.getValue().toString()));
				break;
			case "planQuantity":
				request = request.planQuantity(Integer.parseInt(element.getValue().toString()));
				break;
			default:
				break;
			}
		}

		return request.request();
	}

	/***
	 * <h1>verifyManageSubscriptionsInPro</h1>
	 * <p>
	 * purpose: Assuming user logged into BidSync Pro, navigate to the Manage
	 * Subscriptions > Chargebee punchout. Verify that the subscription is
	 * displayed
	 * 
	 * @param subscription
	 *            = SubscriptionPlan to verify
	 * @return ManageSubscriptions
	 * @throws InterruptedException
	 */
	public ManageSubscriptions verifyManageSubscriptionsInPro(SubscriptionPlan subscription)
			throws InterruptedException {
		return new ManageSubscriptions(driver).openCompanySettingsDropDown().openManageSubscriptions()
				.clickManageSubscriptions().verifyManageSubscriptionsOnlyDisplaysSubscription(subscription)
				.closePopup();
	}

	public ListResult getActiveAddOnList() throws Exception {
		ListResult result = Addon.list().status().is(Status.ACTIVE).request();
		return result;
	}

	public List<ApplicableAddon> getApplicableAddOnList(Subscription subscription) throws Exception {
		List<ApplicableAddon> applicableAddOns = new ArrayList<ApplicableAddon>();
		Plan plan = Plan.list().id().is(subscription.planId()).request().listIterator().next().plan();
		if (plan.addonApplicability().equals(AddonApplicability.RESTRICTED))
			applicableAddOns = plan.applicableAddons();
		else
			applicableAddOns = plan.applicableAddons();
		return applicableAddOns;
	}

	public List<ApplicableAddon> getUpdatableAddOnFromApplicableAddOnList(Subscription subscription) throws Exception {

		List<com.chargebee.models.Subscription.Addon> availableAddOns = subscription.addons();
		List<ApplicableAddon> applicableAddOns = getApplicableAddOnList(subscription);

		// return applicableAddOns.stream().filter(str1->
		// availableAddOns.stream().map(x->x.id()).collect(Collectors.toSet())
		// .contains(str1.id())).collect(Collectors.toList());
		return applicableAddOns.stream().filter(
				str1 -> !(availableAddOns.stream().map(x -> x.id()).collect(Collectors.toSet()).contains(str1.id())))
				.collect(Collectors.toList());
	}

	public void verifySubscriptionDetails(Subscription subscription, RandomChargebeeUser user) throws Exception {
		verifyPlanName(subscription, user.getSubscriptionPlan());
		verifyAddOns(subscription, user);
	}

	private void verifyAddOns(Subscription subscription, RandomChargebeeUser user) throws Exception {
		// format: as displays in Chargebee
		// Military Bids - State Plan
		// Canada Bids - State Plan
		// Federal Bids - State Plan
		// Additional States
		List<com.chargebee.models.Subscription.Addon> subscriptionAddOns = subscription.addons();
		List<String> actualAddOns = new ArrayList<String>();
		for (com.chargebee.models.Subscription.Addon addOn : subscriptionAddOns) {
			actualAddOns.add(Addon.list().id().is(addOn.id()).request().listIterator().next().addon().invoiceName());
		}

		// Match format in order to compare
		List<String> expectedAddOns = new ArrayList<>();
		expectedAddOns.addAll(user.getSubscriptionAddOnsInChargebeeFormat());
		// if(!user.getAdditionalStateAddOns().isEmpty())
		// {expectedAddOns.add("Additional States");}

		String errorMsg = String.format("Error: Chargebee expected addOns <%s> != displayed addOns <%s>",
				expectedAddOns.toString(), actualAddOns.toString());
		Assert.assertTrue(errorMsg, actualAddOns.containsAll(expectedAddOns));
	}

	private void verifyPlanName(Subscription subscription, SubscriptionPlan plan) throws Exception {
		String actualPlanId = subscription.planId();
		Plan actualPlan = null;
		try {
			actualPlan = Plan.list().id().is(actualPlanId).request().listIterator().next().plan();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String expectedPlan = plan.getChargebeeSubscriptionBackEndName();
		String errorMsg = String.format("Test Failed: Expected [%s] subscription plan, but Chargebee displays [%s]",
				expectedPlan, actualPlan.name());

		while (!(actualPlan.name().equalsIgnoreCase(expectedPlan)) && i < 60) {
			System.out.println(GlobalVariables.getTestID() + "  --- Plan Name :- User Id : " + subscription.customerId() + " Email : "
					+ getCustomerById(subscription.customerId()).email() + "; Expected : " + expectedPlan + " Actual : "
					+ actualPlan.invoiceName());
			helperMethods.addSystemWait(10); // Wait time for the Subscription
												// to get updated
			i++;
			actualPlanId = getSubscriptionForUserEmail(getCustomerById(subscription.customerId()).email()).planId();
			actualPlan = Plan.list().id().is(actualPlanId).request().listIterator().next().plan();
		}
		Assert.assertTrue(errorMsg, expectedPlan.contains(actualPlan.name()));
	}

	public Plan getPlanByPlanName(String planName) throws Exception {
		return Plan.list().name().is(planName).request().listIterator().next().plan();
	}

	public void createRandomChargebeeUser() throws Exception {
		RandomChargebeeUser user = new RandomChargebeeUser();
		String firstName = RandomStringUtils.randomAlphabetic(1, 10);
		String lastName = RandomStringUtils.randomAlphabetic(1, 10);
		Result result = Subscription.create().planId("basic").autoCollection(AutoCollection.OFF)
				.customerFirstName(firstName).customerLastName(lastName).customerEmail(randomHelpers.getRandomEmail())
				.customerPhone(randomHelpers.getRandomIntStringOfLength(10)).billingAddressFirstName(firstName)
				.billingAddressLastName(lastName).billingAddressLine1("500 Plaza on the lake")
				.billingAddressCity("Austin").billingAddressState("Texas").billingAddressZip("78613")
				.billingAddressCountry("US").billingCycles(1).request();
		Customer customer = result.customer();
		user.setEmailAddress(customer.email());
		GlobalVariables.storeObject("StoredUser", user);
	}

	/* ----- test steps ----- */

	@Given("^I created a user in chargebee$")
	public void I_get_Chargebee() {
		try{
			createRandomChargebeeUser();
		}catch(Exception exp){
			fail("Failed to create user in Chargebee");
		}
	}

	// Verified
	@Given("^I get ChargeBee subscriptions list$")
	public void I_have_logged_into_the_Chargebee() throws Exception {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Logging into the Chargebee test site with dmidura credentials");
		Subscription userSubscription = this.getSubscriptionForUserEmail("ab7d4f8c@phimail.mailinator.com");
		getActiveAddOnList();
	}

	// Verified
	@Given("^I have logged into the Chargebee test site as the admin user$")
	public void I_have_logged_into_the_Chargebee_test_site_as_the_admin_user() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Logging into the Chargebee test site with dmidura credentials");
		// this.login();
	}

	// verified
	@And("^I will verify that my user only has a \"(.*)\" subscription plan listed in the Chargebee test site$")
	public void I_will_verify_that_my_user_only_has_a_subscription_plan_listed_in_the_Chargebee_test_site(
			String subscription) throws Exception {
		int i = 0;
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Logging into chargebee and verifying that user only has \"" + subscription
				+ "\" subscription");

		String email = SupplierLogin.thisUser.getEmail();
		Subscription userSubscription = this.getSubscriptionForUserEmail(email);
		String planId = userSubscription.planId();
		Entry s = Plan.list().id().is(planId).request().listIterator().next();
		while (!(s.plan().invoiceName().equalsIgnoreCase(subscription)) && i < 12) {
			System.out.println(GlobalVariables.getTestID() + "  --- : User Id : " + userSubscription.customerId() + " Expected : " + subscription
					+ " Actual : " + s.plan().invoiceName());
			helperMethods.addSystemWait(10); // Wait time for the Subscription
												// to get updated
			i++;
			userSubscription = this.getSubscriptionForUserEmail(email);
			planId = userSubscription.planId();
			s = Plan.list().id().is(planId).request().listIterator().next();
		}
		Assert.assertTrue(
				"Subscription plan not matching for the user : " + userSubscription.customerId() + "; Expected : "
						+ subscription + " Actual : " + s.plan().invoiceName(),
				s.plan().invoiceName().equalsIgnoreCase(subscription));

		System.out.println(GlobalVariables.getTestID() + " Test Passed: User only has a one subscription listed in Chargebee");
	}

	@Then("^I will verify that the company owner only has a \"(.*)\" subscription plan listed in the Chargebee test site$")
	public void I_will_verify_that_the_company_owner_only_has_a_subscription_plan_listed_in_the_Chargebee_test_site(
			String subscription) throws Exception {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying in Chargebee that company owner only has \"" + subscription
				+ "\" subscription");

		RandomChargebeeUser owner = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		Subscription userSubscription = this.getSubscriptionForUserEmail(owner.getEmailAddress());
		String planId = userSubscription.planId();

		Entry s = Plan.list().id().is(planId).request().listIterator().next();

		while (!(s.plan().invoiceName().equalsIgnoreCase(subscription)) && i < 12) {
			System.out.println(GlobalVariables.getTestID() + "  --- : User Id : " + userSubscription.customerId() + " Expected : " + subscription
					+ " Actual : " + s.plan().invoiceName());
			helperMethods.addSystemWait(10); // Wait time for the Subscription
												// to get updated
			i++;
			userSubscription = this.getSubscriptionForUserEmail(owner.getEmailAddress());
			planId = userSubscription.planId();
			s = Plan.list().id().is(planId).request().listIterator().next();
		}
		Assert.assertTrue("Subscription plan not matching for the user " + owner.getEmailAddress(),
				s.plan().name().equalsIgnoreCase(subscription));

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Company Owner only has a one subscription listed in Chargebee");
	}

	// verified
	@When("^I cancel the user subscription in the Chargebee test site for my user account$")
	public void I_cancel_the_user_subscription_in_the_Chargebee_test_site_my_user_account() throws Exception {
		String email = SupplierLogin.thisUser.getEmail();
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Locating user \"" + email + "\" in Chargebee test site and then cancelling");

		Subscription subscription = this.getSubscriptionForUserEmail(email);
		Result result = cancelSubscription(subscription);
		Assert.assertTrue(
				"Subscription is expected to be Cancelled for user " + email + " but is in state "
						+ result.subscription().status(),
				result.subscription().status().equals(Subscription.Status.CANCELLED));
	}

	// verified
	@Then("^I will verify that the user subscription is cancelled and that only a BidSync Basic subscription is created in BidSync Pro$")
	public void I_will_verify_that_the_user_subscription_is_cancelled_and_that_only_a_BidSync_Basic_subscription_is_created_in_BidSync_Pro()
			throws Exception {
		System.out.println(
				"---TEST STEP: Verifying that subscription is cancelled and that only a BidSync Basic Subscription was created");
		String email = SupplierLogin.thisUser.getEmail();
		String accountReference = SupplierLogin.thisUser.getAccountReference();

		// Verify BASIC subscription in Chargebee test site
		Subscription subscription = this.getSubscriptionForUserEmail(email);

		int count = 0;
		if (subscription.status().equals(Subscription.Status.CANCELLED)) {

			while (this.getSubscriptionForUserEmail(email).status().equals(Subscription.Status.CANCELLED)
					&& count < 36) {
				helperMethods.addSystemWait(5);
				System.out.println(count);
				count++;
			}
			subscription = this.getSubscriptionForUserEmail(email);
			Assert.assertTrue("Subscription not auto-updated to Basic on cancel of earlier subscription..."
					+ subscription.planId(), subscription.status().equals(Subscription.Status.ACTIVE));
			String planId = subscription.planId();
			Entry s = Plan.list().id().is(planId).request().listIterator().next();
			Assert.assertTrue("",
					s.plan().name().equalsIgnoreCase("Basic") || s.plan().name().equalsIgnoreCase("BidSync Basic"));
			Assert.assertTrue(subscription.status().equals(Subscription.Status.ACTIVE));
		} else if (subscription.status().equals(Subscription.Status.ACTIVE)) {
			String planId = subscription.planId();
			Entry s = Plan.list().id().is(planId).request().listIterator().next();
			Assert.assertTrue("",
					s.plan().name().equalsIgnoreCase("Basic") || s.plan().name().equalsIgnoreCase("BidSync Basic"));
			Assert.assertTrue(subscription.status().equals(Subscription.Status.ACTIVE));
		} else
			Assert.fail("Subscription is neither in Cancelled state nor in Active state");

		// Log into BidSyn Pro and verify "BASIC" in the Chargebee punchout
		supplierLogin.navigateToHere().loginAsUser(accountReference);
		this.verifyManageSubscriptionsInPro(SubscriptionPlan.BASIC).logout();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Subscription cancelled and converted to BidSync Basic Plan");
	}

	// verified
	@When("^I create a user \"(.*)\" subscription in the Chargebee test site for my user account \"(.*)\"$")
	public void I_create_a_user_subscription_in_the_Chargebee_test_site_for_my_user_account(String strSubscriptionPlan,
			String strUserAccount) {
		System.out.println(
				"---TEST STEP: Creating a new \"" + strSubscriptionPlan + "\" subscription for our user in Chargebee");
		try {
			// Get our user account for later user
			supplierLogin.setThisUserFromAccountReference(strUserAccount);
			String email = SupplierLogin.thisUser.getEmail();

			Subscription subscription = this.getSubscriptionForUserEmail(email);
			String plan1 = subscription.planId();
			// Update the user subscription in Chargebee

			subscriptionUpdateKeys.put(SubscriptionKeys.planId, getPlanByPlanName(strSubscriptionPlan).id());
			Result result = updateSubscription(subscription, subscriptionUpdateKeys);

			Assert.assertFalse("Plan not updated for the subscription " + plan1,
					plan1.equals(result.subscription().planId()));
			// Let Chargebee backend catch up to the subscription change
			helperMethods.addSystemWait(2);
		} catch (Exception exp) {
			fail("TEST FAILED :  I_create_a_user_subscription_in_the_Chargebee_test_site_for_my_user_account() " + exp);
		}
	}

	@When("^I change my user subscription to \"(.*)\" from the Manage Subscriptions screen$")
	public void I_change_my_user_subscription_to_Subscription_from_the_Manage_Subscriptions_screen(
			String strSubscriptionPlan) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Changing user subscription to \"" + strSubscriptionPlan
				+ "\" subscription in \"Manage Subscriptions\" screen");
		// 1. Navigate to the Manage Subscriptions.
		// 2. Click on "Manage Subscriptions" button. Manage Subscriptions
		// Chargbee
		// portal appears.
		// 3. Update the subscription
			try {
				new ManageSubscriptions(driver)
					.openCompanySettingsDropDown()
					.openManageSubscriptions();
			} catch (InterruptedException e) {
				fail("Failed to open Manage Subscriptions");
				e.printStackTrace();
			}
			
			new ManageSubscriptions(driver)
					.clickManageSubscriptions()
					.clickOnSubscriptionToUpdate()
					.clickOnEditSubscription()
					.selectSubscriptionPlanFromDropdown(SubscriptionPlan.fromString(strSubscriptionPlan))
					.clickOnUpdateSubscription()
					.clickOnSubscriptionDetailsBackBtn();

			GlobalVariables.storeObject("strSubscriptionPlan", strSubscriptionPlan);
	}

	@Then("^I will verify that the subscription has changed and properly updates the ARGO database$")
	public void I_will_verify_that_the_subscription_has_changed_and_properly_updates_the_ARGO_database() {
		System.out.println(
				"---TEST STEP: Verifying that subscription has changed and properly updates the ARGO database");
		String strSubscriptionPlan = GlobalVariables.getStoredObject("strSubscriptionPlan");

		// Let everything on the ARGO backend catch up before we try to retrieve
		// from
		// the database
		// and yah, it really does take this long for the backend to sync with
		// the
		// front... ewww
		helperMethods.addSystemWait(12);

		// Verify in bidsync pro that the subscription is displaying
		// Verify in ARGO database that subscription has updated
		new ManageSubscriptionsChargebeePopup(driver)
				.verifyManageSubscriptionsOnlyDisplaysSubscription(SubscriptionPlan.fromString(strSubscriptionPlan))
				.verifySubscriptionPlanUpdatesInARGODatabase(SubscriptionPlan.fromString(strSubscriptionPlan),
						SupplierLogin.thisUser.getEmail())
				.closePopup();
	}

	// verified
	@Then("^I will verify that the \"(.*)\" subscription is updated in BidSync Pro$")
	public void I_will_verify_that_the_subscription_is_updated_in_BidSync_Pro(String strSubscriptionPlan)
			throws Exception {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that the \"" + strSubscriptionPlan
				+ "\" subscription is updated in BidSync Pro");

		String email = SupplierLogin.thisUser.getEmail();
		String accountReference = SupplierLogin.thisUser.getAccountReference();

		// Verify the Subscription plan in Chargebee
		Subscription subscription = this.getSubscriptionForUserEmail(email);

		if (subscription.status().equals(Subscription.Status.CANCELLED)) {
			String planId = this.getSubscriptionForUserEmail(email).planId();
			Entry s = Plan.list().id().is(planId).request().listIterator().next();
			Assert.assertTrue("", s.plan().name().equalsIgnoreCase(strSubscriptionPlan));
			Assert.assertTrue(subscription.status().equals(Subscription.Status.ACTIVE));
		}

		// Log into BidSyn Pro and verify subscription plan displays in
		// Chargebee
		// punchout
		supplierLogin.navigateToHere().loginAsUser(accountReference);
		this.verifyManageSubscriptionsInPro(SubscriptionPlan.fromString(strSubscriptionPlan)).logout();
	}

	// verified
	@Then("^I will verify that my user only has a \"(.*)\" subscription plan listed in the ARGO database$")
	public void I_will_verify_that_my_user_only_has_a_subscription_plan_listed_in_the_ARGO_database(
			String subscriptionPlan) {
		System.out.println(
				"---TEST STEP: Verifying that user only has a \"" + subscriptionPlan + "\" listed in ARGO database");

		String email = SupplierLogin.thisUser.getEmail();
		new DatabaseRegistration(email).verifySubscriptionPlan(email, SubscriptionPlan.fromString(subscriptionPlan));

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Only one subscription for this user");
	}

	@And("^I will verify that the company owner only has a \"(.*)\" subscription plan listed in the ARGO database$")
	public void I_will_verify_that_the_company_owner_only_has_a_subscription_plan_listed_in_the_ARGO_database(
			String subscriptionPlan) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that the company owner only has a \"" + subscriptionPlan
				+ "\" listed in ARGO database");

		RandomChargebeeUser owner = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		new DatabaseRegistration(owner.getEmailAddress()).verifySubscriptionPlan(owner.getEmailAddress(),
				SubscriptionPlan.fromString(subscriptionPlan));

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Only one subscription for this the company owner");
	}

	@When("^the Chargebee admin cancels the subscription for the company owner$")
	public void the_Chargebee_admin_cancels_the_subscription_for_the_company_owner() throws Exception {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Canceling the subscription for the company owner via Chargebee");
		RandomChargebeeUser owner = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");

		Subscription subscription = this.getSubscriptionForUserEmail(owner.getEmailAddress());
		Result result = cancelSubscription(subscription);
		Assert.assertTrue(
				"Subscription is expected to be Cancelled for user " + owner.getEmailAddress() + "but is in state "
						+ result.subscription().status(),
				result.subscription().status().equals(Subscription.Status.CANCELLED));

		// Update the stored values to reflect the cancellation
		// Note: This is what we expect to happen
		owner.setSubscriptionPlan(SubscriptionPlan.BASIC);
		owner.setSubscriptionGrouping(SubscriptionGrouping.NONE);
		owner.setAddOns(new ArrayList<>());
		owner.setAdditionalStateAddOns(new ArrayList<>());
		GlobalVariables.storeObject("ownerRandomChargebeeUser", owner);
	}

	// verified
	@Then("^I will verify that only one user customer account exists for my user in the Chargebee test site$")
	public void I_will_verify_that_only_one_user_customer_account_was_created_for_my_user_in_the_Chargebee_test_site()
			throws Exception {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that only one customer account was created for user in Chargebee");
		RandomChargebeeUser newUser = GlobalVariables.getStoredObject("StoredUser");

		// Grab customer id from Chargebee
		ListResult subscription = this.getSubscriptionListForUserEmail(newUser.getEmailAddress());
		Assert.assertFalse("No chargeBee user accounts created for registered email " + newUser.getEmailAddress(),
				subscription.size() < 1);
		Assert.assertFalse(
				"More than one chargeBee user accounts created for registered email " + newUser.getEmailAddress(),
				subscription.size() > 1);

		GlobalVariables.storeObject("customerId", subscription.listIterator().next().subscription().customerId());
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Verified only one Chargebee customer account was created for new user");

	}

	// verified
	@And("^I will verify that my user's customer_id matches in the ARGO database and Chargebee test site$")
	public void I_will_verify_that_my_user_customer_id_matches_in_the_ARGO_database_and_Chargebee_test_site() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that customer id in Chargebee is stored in the ARGO database");
		RandomChargebeeUser newUser = GlobalVariables.getStoredObject("StoredUser");
		String customerId = GlobalVariables.getStoredObject("customerId");

		new DatabaseRegistration(newUser.getEmailAddress()).verifyUserRegistrationCustomerIdAsExpected(customerId);

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Verified that Chargebee customer id is stored in ARGO database");
	}

	// Verified
	@When("^I Change Subscription for the first visible subscription record in the Chargebee test site$")
	public void I_Change_Subscription_for_the_first_visible_subscription_record_in_the_Chargebee_test_site()
			throws Exception {
		System.out.println(
				"---TEST STEP: Selecting first visible subscription record in Chargebee test site, and clicking \"Change Subscription\"");

		updateSubscription(this.getChargeBeeSubscription(), subscriptionUpdateKeys);
	}

	// Verified
	@Then("^I will verify that the five Additional State dropdowns appear with populated values on the Chargeebee test site$")
	public void I_will_verify_that_the_five_Additional_State_dropdowns_appear_with_populated_values_on_the_Chargeebee_test_site() {
		System.out.println(
				"---TEST STEP: Verifying that the five \"Additional State\" dropdowns appear with populated values on the Chargebee test site");

		// new
		// ChangeSubscription(driver).verifyAdditionalStateDropdownsContainAllOptions();

		System.out.println(
				"Test Passed: Five \"Additional State\" dropdowns are visible in Chargebee test site and are correctly populated");
	}

	@When("^I randomly purchase Additional States addOns from the Chargebee test site$")
	public void I_randomly_purchase_Additional_States_addOns_from_the_Chargebee_test_site() throws Exception {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Randomly purchasing Additional States addOns from Chargebee test site");

		RandomChargebeeUser user = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		user.setRandomAdditionalStateAddOns();

		Subscription subscription = this.getSubscriptionForUserEmail(user.getEmailAddress());

		String addOnId = getUpdatableAddOnFromApplicableAddOnList(subscription).listIterator().next().id();
		subscriptionUpdateKeys.put(SubscriptionKeys.addonId, addOnId);
		subscriptionUpdateKeys.put(SubscriptionKeys.cf_additional_state,
				user.getAdditionalStateAddOns().stream().map(String::valueOf).collect(Collectors.joining(",")));

		updateSubscription(subscription, subscriptionUpdateKeys);
		subscription = this.getSubscriptionForUserEmail(user.getEmailAddress());
		Subscription.Addon updatedAddOn = subscription.addons().stream().filter(addon -> addon.id().equals(addOnId))
				.findAny().get();
		Assert.assertTrue("Subscription not updated with Addon", updatedAddOn.id().equals(addOnId));
	}

	@Then("^I will verify that the Chargebee test site correctly displays the company owner subscription and addOns$")
	public void I_will_verify_that_the_Chargebee_test_site_correctly_displays_the_company_owner_subscription_and_addOns()
			throws Exception {
		System.out.println(
				"---TEST STEP: Verifying that Chargebee test site correctly displays company owner's subscription and addOn");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		user.printSubscriptionDetails();

		Subscription subscription = this.getSubscriptionForUserEmail(user.getEmailAddress());

		verifySubscriptionDetails(subscription, user);

		System.out.println(
				"Test Passed: Chargebee test site correctly displays the company owner's subscription and addOns");
	}

}

enum SubscriptionKeys {
	planId, planQuantity, planUnitPrice, setupFee, startDate, trialEnd, billingCycles, addonId, addonQuantity, endOfTerm, cf_additional_state
}
