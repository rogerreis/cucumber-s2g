package pages.bidSyncClassic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncClassic.myRFQ.InformationCenter;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>BSClassicMYRFQLoginPage</h1>
 * @author dmidura
 *<p>details: This class houses methods and page objects for the BidSync Classic Login Page that direct so My RFQ
 *	Note: After a successful login, user will be directed to the "Information Center" screen
 *</p>
 */
public class BSClassicMyRFQLoginPage extends BasePageActions {
	
	// xpaths
	private final String nameInput_xpath     =  "// input[@id='username']";
    private final String passwordInput_xpath =  "// input[@id='password']";
    private final String signInBtn_xpath     =  "// input[@id='btnSubmit']";
	
	// Other
	private EventFiringWebDriver driver;
	private final String strPagePath = System.getProperty("bidSyncClassicBaseURL") + "MyRFQ";

	public BSClassicMyRFQLoginPage(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	/* ----- getters ----- */
	private WebElement getNameInput()     { return findByVisibility(By.xpath(this.nameInput_xpath));     }
	private WebElement getPasswordInput() { return findByVisibility(By.xpath(this.passwordInput_xpath)); }
	private WebElement getSignInBtn()     { return findByVisibility(By.xpath(this.signInBtn_xpath));     }
	
	/* ----- methods ----- */
	
	/***
	 * <h1>loginToMyRFQ</h1>
	 * <p>purpose: Navigate to classic and then sign with credentials. After signin, user is navigated to MY RFQ screen</p>
	 * @param userName = username for login
	 * @param password = password for login
	 * @return InformationCenter
	 */
	public InformationCenter loginToMyRFQ(String userName, String password) {
		this.navigateToHere();
		this.getNameInput().sendKeys(userName);
		this.getPasswordInput().sendKeys(password);
		this.clickSignInBtn();
		return new InformationCenter(driver);
	}
	
	/***
	 * <h1>navigateToHere</h1>
	 * <p>purpose: Navigate to BidSync Classic login screen with redirect to My RFQ upon login
	 * @return BSClassicLoginPage
	 */
	public BSClassicMyRFQLoginPage navigateToHere() {
		driver.navigate().to(this.strPagePath);
		return this;
	}
	
	/***
	 * <h1>clickSignInBtn</h1>
	 * <p>purpose: Click on the "Sign In" button
	 * @return BSClassicLoginPage
	 */
	public BSClassicMyRFQLoginPage clickSignInBtn() {
		this.getSignInBtn().click();
		return this;
	}

} 
