@administration @level4
Feature: Company Settings > Company Profile - Company Profile Screen
  
  @must
  Scenario: Change Company Information with valid data
    Given I have logged into the Supplier side as user "Basic"
      And I have navigated to Company Settings > "Company Profile"
     When I fill in the Company Information dialog with random valid data
     Then the Company Information will reflect the changes made
    
    
  @must @ARGO-2342
  Scenario: Backing out of editing Company Information should not save the changes made
    Given I have logged into the Supplier side as user "Basic"
      And I have navigated to Company Settings > "Company Profile"
     When I fill in the Company Information dialog but cancel the changes
     Then the Company Information data will not have been changed
    
    
  @ignore @ARGO-1991 @release_ignore
  Scenario: Company Owner leaves for new company
    Given I have registered a random user
      And I register 2 new users at the same company
      And the company owner accepts all applicants
     When the company owner joins a random company
     Then the second user to join the company is the new company owner
  
  @must @ARGO-1991
  Scenario: Company Owner leaves for existing company
  dmidura: Workaround commenting out Then step. 
  Per BIDSYNC-247: When a company owner leaves their company, the new company owner is incorrect
    Given I register 3 users at one company and 1 user at another
      And the company owner accepts all applicants
     When the owner of the first company joins the second
     #Then the second user to join the company is the new company owner
    
  @must @ARGO-1990
  Scenario: If a company has no users, it must be deleted
    Given I have registered a random user
      And I logout of BidSync Pro
     When the company owner joins a random company
     Then the old company is deleted

@must @BIDS-284
Scenario: Company name should match on company profile page
	Given I have registered a random user
	Then I have navigated to Account > "My Info"
	And I verify that the company name is displayed on profile information page
	Then I go to company settings > company profile
	And I verify that the company name is populated on company profile page

