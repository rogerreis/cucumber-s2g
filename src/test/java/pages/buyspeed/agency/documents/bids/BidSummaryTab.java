package pages.buyspeed.agency.documents.bids;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.buyspeed.agency.documents.common.ApprovalsScreen;
import pages.buyspeed.agency.documents.common.DocumentStatusPopup;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

/***
 * <h1>Class BidSummaryTab</h1>
 * @author dmidura
 * <p>details: This class holds page objects and methods for the Bid Summary tab (last tab on a bid)
 */
public class BidSummaryTab extends BidNavigation {
	
	// xpaths
	// Top
	private final String genericClonedBidLink_xpath = "//ul[@id='infoMsgs']//li";
	private final String headerLevelStatus_xpath    = "//td[contains(.,'Status:')][@class='sectionheader-right']";
	private final String bidNumber_xpath            = "//td[@class='sectionheader-01']//h1[contains(text(), 'Open Market Bid')]";
	private final String statusBtn_xpath            = "//a[@title='Document History']";

	// Bottom Button Panel
	private final String cloneBidBtn_xpath          = "//input[@value='Clone Bid'][@type='button']";
	private final String submitForApprovalBtn_xpath = "//input[@value='Submit for Approval'][@type='button']";
	private final String sendBidBtn_xpath           = "//input[@value='Send Bid'][@type='button']";
	private final String cancelBidBtn_xpath         = "//input[@value='Cancel Bid'][@type='button']";
	
	// For Approvals
	private final String continueBtn_xpath          = "//input[@value='Continue'][@type='button']";
	

	// other
	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;

	public BidSummaryTab (EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
		helperMethods = new HelperMethods();
	}
	
	/* ----- getters ----- */
	// Top
	private String getHeaderLevelStatus() { return findByPresence(By.xpath(this.headerLevelStatus_xpath)).getText().replace("Status:", "").trim(); }
	public WebElement getBidNumber()      { return findByVisibility(By.xpath(this.bidNumber_xpath)); }
	public WebElement getStatusBtn()      { return findByVisibility(By.xpath(this.statusBtn_xpath)); }

	// Bottom Button Panel
  	private WebElement getCloneBidBtn()          { return findByScrollIntoViewTopOfScreen(By.xpath(this.cloneBidBtn_xpath));          }
	private WebElement getSubmitForApprovalBtn() { return findByScrollIntoViewTopOfScreen(By.xpath(this.submitForApprovalBtn_xpath)); }
	private WebElement getSendBidBtn()           { return findByScrollIntoViewTopOfScreen(By.xpath(this.sendBidBtn_xpath));           }
	private WebElement getCancelBidBtn()         { return findByScrollIntoViewTopOfScreen(By.xpath(this.cancelBidBtn_xpath));         }
		
	// For Approvals
	private WebElement getContinueBtn()          { return findByPresence(By.xpath(this.continueBtn_xpath));                     }
	
	/* ----- methods ----- */

	/**
	 * <h1>waitForHeaderStatus</h1>
	 * <p>purpose: Wait up to 8 seconds for the header status<br>
	 * 	on the Bid Summary to update to status</p>
	 * @param status = a valid bid status
	 * 	"2BR" | "2BS" , etc
	 * @return BidHeader
	 */
	private BidSummaryTab waitForHeaderStatus(String status) {
		waitForPresence(By.xpath(this.headerLevelStatus_xpath + "[contains(.,'" + status + "')]"), 8);
		return this;
	}
		

	/***
	 * <h1>clickCloneBidBtn</h1>
	 * <p>purpose: Click on the "Clone Bid" button<br>
	 * 	at the bottom of the "Summary" tab to clone the bid</p>
	 * @return BidSummaryTab
	 */
	public BidSummaryTab clickCloneBidBtn() {
		this.getCloneBidBtn().click();
		return this;
	}
	
	/***
	 * <h1>openNewestClonedBid</h1>
	 * <p>purpose: Assuming that you're on the Bid Summary of a bid with<br>
	 * 	clones, click on the link of the newest clone in order to view this<br>
	 *  cloned bid. Fail test if there aren't any clones on this Summary tab</p>
	 * @return BidSummaryTab
	 */
	public BidSummaryTab openNewestClonedBid() {
			
		// Figure out how many cloned bid document links we have
		List <WebElement> clonedBidDocLinks = this.findAllOrNoElements(driver, By.xpath(this.genericClonedBidLink_xpath));

		if (clonedBidDocLinks.isEmpty()) {fail("Test Failed: No cloned bids!");}
		
		// Get all the text for each link (expected text: ADSPO17-########)
		List <String> linkText = new ArrayList<String>();
        helperMethods.addSystemWait(2);
		for (int i = 1 ; i < clonedBidDocLinks.size() + 1 ; i ++) {
			linkText.add(driver.findElement(By.xpath(this.genericClonedBidLink_xpath + "[" + i + "]/a")).getText());
		}

		// Sort so can get the link to the newest cloned bid doc 
		// (this is the link with the highest doc number)
		// (i.e. ADSPO17-00005650 is greater than ADSPO17-00005649, so we want ADSPO17-00005650)
		Collections.sort(linkText);
		Collections.reverse(linkText);
			
		// Click on the link with the appropriate text
		String docXPath = this.genericClonedBidLink_xpath + "[contains(a, '" + linkText.get(0) + "')]/a";
		System.out.println(GlobalVariables.getTestID() + " Cloned Doc Link = " + docXPath);
        helperMethods.addSystemWait(1);
		findByScrollIntoViewBottomOfScreen(By.xpath(docXPath));
		helperMethods.addSystemWait(6);
		WebElement clonedDocLink = driver.findElement(By.xpath(docXPath));
		clonedDocLink.click();
		
		// And let the new doc load
		// Here, we're waiting for the section header to display the cloned doc bid number
		//String xpath = "//td[@class='sectionheader-01']//h1[contains(text(), 'Open Market Bid " + linkText.get(0) + "')]";
		String xpath = this.bidNumber_xpath + "[contains(text(), '" + linkText.get(0) + "')]";
		waitForPresence(By.xpath(xpath));
		
		return this;
	}
	
	/***
	 * <h1>clickSubmitForApprovalBtn</h1>
	 * <p>purpose: Click on the "Submit For Approval" button on the Bid Summary tab</p>
	 * @return BidBiddersTab
	 */
	public BidSummaryTab clickSubmitForApproval() {
		this.getSubmitForApprovalBtn().click();
		return this;
	}
	
	/***
	 * <h1>automaticallyApprove</h1>
	 * <p>purpose: Click "Submit for Approval" button<br>
	 * 	and then process through an automatic approval</p>
	 * @return
	 */
	public BidSummaryTab automaticallyApprove() {
		
		// Submit For Approval
		this.clickSubmitForApproval();
		
		// if there's a "Continue" button, click it
		if (this.doesPageObjectExist(driver, By.xpath(this.continueBtn_xpath))) {
			this.getContinueBtn().click();
		}
		
		// Automatically approve via the approvals screen
		new ApprovalsScreen(driver).automaticallyApprove();
		
		// And wait for the bid to move into ready to send
		this.waitForHeaderStatus("2BR");
		
		return this;
	}
	

	/***
	 * <h1>clickSendBidBtn</h1>
	 * <p>purpose: When bid is in "2BR - Ready to Send"<br>
	 * 	status, then "Send Bid" button is available. <br>
	 * 	Use this method to click "Send Bid" (Change bid status to "Sent" and notify vendors)<br>
	 * 	option is defaulted. Then wait for bid to process to sent status. Fail test if <br>
	 * 	status does not process</p>
	 * @return BidSummaryTab
	 */
	public BidSummaryTab clickSendBidBtn() {
		// Click on "Send" button
		this.getSendBidBtn().click();
		
		// And wait for the bid to update to sent
		this.waitForHeaderStatus("2BS");

		return this;
	}

	/***
	 * <h1>clickCancelBidBtn</h1>
	 * <p>purpose: When bid is in "2BS - Sent"<br>
	 * 	status, then "Cancel Bid" button is available. <br>
	 * 	Use this method to click "Cancel Bid" <br>
	 * 	Then wait for bid to process to sent canceled. Fail test if <br>
	 * 	status does not process</p>
	 * @return BidSummaryTab
	 */
	public BidSummaryTab clickCancelBidBtn() {
		// Click on "Cancel Bid" button
		this.getCancelBidBtn().click();
		
		// Click on OK on the popup
		driver.switchTo().alert().accept();
		
		// And wait for the bid to update to cancel
		this.waitForHeaderStatus("2BC");

		return this;
	}
	
	/***
	 * <h1>clickStatusBtn</h1>
	 * <p>purpose: Click on the status button<br>
	 * 	on the top of the bid (RH corner) to view<br>
	 * 	the status popup. Method will automatically switch<br>
	 * 	to the popup window</p>
	 * @return StatusPopup
	 */
	public DocumentStatusPopup clickStatusBtn() {
		// Click on the status indicator to view the popup
		this.getStatusBtn().click();
		
		// Switch windows to the visible popup
		switchToWindow("BuySpeed Online - Document Status History");

		// Return access to the popup
		return new DocumentStatusPopup(driver);
	}
	
	/***
	 * <h1>getBidCanceledDateAndTimeAsCalendar</h1>
	 * <p>purpose: Click on the status button to open the <br>
	 * 	document status popup. From here, grab the last row<br>
	 * 	(which should correspond to the cancellation date/time).<br>
	 * 	Return that date/time as a Calendar object</p>
	 * @return Calendar = date/time of the bid cancellation
	 */
	public Calendar getBidCanceledDateAndTimeAsCalendar() {

		// Click on the status indicator to view the status popup
		// From the status popup, grab the last timestamp
		// which should correspond to our cancellation time
		return this.clickStatusBtn()
				.getLastRowInDocumentStatusTable()
				.getStatusDateAsCalendar();
	}

	/***
	 * <h1>getBidNumberText</h1>
	 * <p>purpose: Return the bid number displaying at the top of the<br>
	 * 	Bid Summary tab</p>
	 * @return String = bid number<br>
	 * 	ex: "ADSPO17-00005667"</p>
	 */
	public String getBidNumberText() {
		return this.getBidNumber().getText().replace("Open Market Bid ", "").trim();
	}

	/* ----- verifications ----- */

	/***
	 * <h1>verifyBidCanceled</h1>
	 * <p>purpose: Verify that the bid header status is<br>
	 * 	"2BC - Canceled". Fail test if not</p>
	 * @return
	 */
	public BidSummaryTab verifyBidCanceled() {
		if (!this.getHeaderLevelStatus().contains("2BC - Canceled")) {
			fail ("Test Failed: Bid is not in Canceled status"); }
		return this;
	}
}
