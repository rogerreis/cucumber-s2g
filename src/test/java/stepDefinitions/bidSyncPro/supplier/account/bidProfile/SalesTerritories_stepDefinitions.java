package stepDefinitions.bidSyncPro.supplier.account.bidProfile;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import pages.bidSyncPro.registration.SelectRegions;
import pages.bidSyncPro.supplier.account.AccountCommon;
import pages.bidSyncPro.supplier.account.bidProfile.SalesTerritories;
import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterResultsSectionNewClass;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

/**
 * This class is to keep the step definitions for Sales Territories on Account
 * >> bid profile
 * 
 * @author ssomaraj
 *
 */
public class SalesTerritories_stepDefinitions {

	private EventFiringWebDriver driver;
	private SalesTerritories salesTerritories;
	private SelectRegions selectRegions;
	private HelperMethods helperMethods;

	public SalesTerritories_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
		salesTerritories = new SalesTerritories(driver);
		selectRegions = new SelectRegions(driver);
		helperMethods = new HelperMethods();
	}

	@Then("my sales region should be populated with edit functionality$")
	public void my_sales_region_should_be_populated_with_edit_functionality() {
			System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Go to account >> Bid Profile . Verify sales region updated");
			new CommonTopBar(driver).waitForPageLoad().openAccountDropDown().clickBidProfile();
			helperMethods.addSystemWait(3);
			salesTerritories.verifyPageElements();
	}
	

	@And("I change my sales territories to US and Canada$")
	public void I_change_my_sales_territorries_to_US_and_Canada() {
			System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Change Sales Region from US to US and Canada");
			salesTerritories.verifyPageElements()
							.clickOnEditSalesTerritories()
							.verifyRegionPageElements()
							.clickOnUsAndCanadaSalesTerritories()
							.clickOnSaveButton();
			helperMethods.addSystemWait(2);
			salesTerritories.verifyPageElements();
	}
	
	
	
	@And("I change my sales territories to All US States$")
	public void I_change_my_sales_territorries_to_All_US_States() {
			System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Change Sales Region to All US state");
			salesTerritories.verifyPageElements()
							.clickOnEditSalesTerritories()
							.verifyRegionPageElements()
							.clickOnUSSalesTerritories()
							.clickOnSaveButton();
			helperMethods.addSystemWait(2);
			salesTerritories.verifyPageElements();

	}
		
	@And("I change my sales territories to States or Province")
	public void I_change_my_sales_territorries_to_StatesOrProvince() {
			System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Change Sales Region to States or Province");
			salesTerritories.verifyPageElements()
							.clickOnEditSalesTerritories()
							.verifyRegionPageElements()
							.clickOnStatesOrProvinceSalesTerritories()
							.clickOnSelectStatesOrProvinceButton()
							.verifyEditSalesTerritoriesPageElements();
			new SelectRegions(driver).selectRandomStates(5);
			salesTerritories.clickOnSaveButton();
			helperMethods.addSystemWait(5);
			salesTerritories.verifyPageElements();
	}	
		
	@And("verify that my updates persist on my page$")
	public void verify_that_my_updates_persist_on_my_page() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: verify that my updates persist on my page");
			salesTerritories.verifyPageElements();
	}
	
	@And("I verify list view can add states from the drop down list$")
	public void verify_list_view_can_add_states_from_the_drop_down_list() {
			salesTerritories.clickOnEditSalesTerritories()
							.clickOnStatesOrProvinceSalesTerritories()
							.clickOnSelectStatesOrProvinceButton();
			selectRegions.clickOnListViewTab();	
			salesTerritories.selectStateFromDropdownList(new ArrayList<String>(Arrays.asList("California","Texas","Arizona","Florida"))) ;
	}
	
	@And("I verify if states can be removed in list view$")
	public void verify_if_states_can_be_removed_in_list_view() {
		
			salesTerritories.deleteStatesChipsFromListView()
							.verifyStatesChipsRemovedFromListView();
	}
	
	
	@And("verify filter section is populated with Sales regions in user's bid profile$")
	public void verify_filter_section_is_populated_with_Sales_regions_in_users_bid_profile() {
			helperMethods.addSystemWait(2);
			new FilterResultsSectionNewClass(driver).openStatesProvincesSection();
			helperMethods.addSystemWait(2);
			salesTerritories.veryfyStateOrProvinceFilterSection();
			helperMethods.addSystemWait(2);
			new DashboardCommon(driver).openAllBidsTab();
			helperMethods.addSystemWait(2);
			salesTerritories.veryfyStateOrProvinceFilterSection();
	}

	
	@And("select 'Back To Bids' and verify filter section is populated with Sales regions in user's bid profile$")
	public void select_BackToBid_and_verify_filter_section_is_populated_with_Sales_regions_in_users_bid_profile() {
			new AccountCommon(driver).clickBackToBids();
			helperMethods.addSystemWait(2);
			new FilterResultsSectionNewClass(driver).openStatesProvincesSection();
			helperMethods.addSystemWait(2);
			salesTerritories.veryfyStateOrProvinceFilterSection();
	}
}
