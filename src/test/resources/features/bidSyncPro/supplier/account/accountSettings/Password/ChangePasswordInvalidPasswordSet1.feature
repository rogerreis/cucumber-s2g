@must @BidSync_AccountChangePassword @administration @level3
Feature: Account > Account Settings - Change Password: Invalid Passwords, Set 1
	BIDSYNC-238: Password Reset Screen, Incorrect Error


@BIDSYNC-238
Scenario Outline: Password reset error message will hide after user corrects password that doesn't meet validation
	Given I have registered a random user
	And I try to change my password from the My Account page
	When I try to update my account with "<password>" that does not meet validation
	Then I will see an error message to update my password per validation specification on the Reset Your Password dialog
	And I will see that the Reset Password button is disabled on the Reset Your Password dialog
	And I will see the appropriate password validation criteria highlighted
	When I update the password and confirm password fields to match and also to meet password validation on the Reset Your Password dialog
	Then I will verify that the Reset Your Password dialog does not display any error message
	And I can successfully submit my new password through the Reset Your Password dialog and log in with this new password
	
	Examples: Passwords that do not meet validation
	|password      |
	|mynewpassword |
	|MYNEWPASSWORD |
	|1542365487    |
	|pass          |
	|123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890P@SSw0rd! |
	
	# Examples:
	# ------ choose 1 ------
	# 1. only lower
	# 2. only upper
	# 3. only digits
	# 4. only special chars
	# ------ wrong number of chars ------
	# 5. < 8 chars
	# 6. < 128 chars
