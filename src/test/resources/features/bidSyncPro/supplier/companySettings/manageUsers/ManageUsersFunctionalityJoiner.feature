@must @ManageUsers @subscriptions @level3
Feature: Company Settings > Management Users - User Management Functionality for Company Joiner
	ARGO-1258: "Manage Additional Subscription Licenses (assign/remove purchased licenses)"
	ARGO-1321: "Joining a Company in Application"
	

@ARGO-1321
Scenario: A user not in a company should not be able to view Company tabs
dmidura: Workaround in place for BIDSYNC-425: "User Management" should not display infinite spinning wheel for user not yet accepted to company

    Given I have registered a random user
      And I register a new user at the same company
     Then The user cannot view the company tabs
      And The user can change to "Airbnb" company

#Need to test it after fixing BIDSYNC-613 . Now no user request can be tracked
#https://periscopeholdings.atlassian.net/browse/BIDS-218 : this is the new jira
@ARGO-1258
Scenario: Only Subscription Owner can manage the subscription

	Given I register a new user with a BidSync Basic Plan from the Masonry website
	When the new user joins a company with available subscription licenses
	Then I will verify that the new user cannot manage subscription licenses
	When the new user is assigned a subscription license by the company owner
	And I logout of BidSync Pro
	Then I will verify that the new user cannot manage subscription licenses
	
