package pages.bidSyncPro.common;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class BidSyncProCommon</h1>
 * @author dmidura
 * <p>details: This class houses methods and pageObjects that are common across BidSync Pro - both within registration and Supplier.<br>
 * 	Note: Methods and pageObjects that are common only to the supplier should be placed in supplier.common,<br>
 * 	similarly, methods and pageObjects that are common only to registration should be placed in the registration.common</p>
 *
 */
public class BidSyncProCommon extends BasePageActions {

    // Status Messages
    private final String statusMessage_xpath         = "//simple-snack-bar[contains(@class, 'mat-simple-snackbar')]";
	
    // Spinner
	private final String spinner_xpath = "//mat-spinner";
	
	// Other
	private EventFiringWebDriver driver;
	
	public BidSyncProCommon(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	 /***
	  * <h1>waitForSpinner</h1>
	  * <p>purpose: Wait up to 15 seconds for the loading spinner to disappear.<br>
	  *	Fail test if spinner is still visible after 2 * DEFAULT_TIMEOUT</p>
	  * @param driver
	  * @return CommonTopBar
	  */
   public BidSyncProCommon waitForSpinner(EventFiringWebDriver driver) {
   	// Check if the spinner is visible before waiting for it
   	if (this.doesPageObjectExist(driver, By.xpath(this.spinner_xpath))) {
   		System.out.println(GlobalVariables.getTestID() + " INFO: Waiting for spinner.");
		int time = DEFAULT_TIMEOUT * 2;
		new WebDriverWait(driver, time)
			.withMessage("Test Timed out: Spinner did not disappear after \"" + time + "\" seconds")
			.pollingEvery(Duration.ofMillis(70))
			.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(this.spinner_xpath)));
   	}

   	return this;
   }
   
   
   /***
    * <h1>waitForStatusMessageToDisappear</h1>
    * <p>purpose: Many times in ARGO, a black status message is displayed
    * after the backend has completed some task. Wait up to DEFAULT_TIMEOUT seconds for this
    * message to disappear. Fail test if status message is still visible after DEFAULT_TIMEOUT
    * @return BidSyncProCommon
    */
   public BidSyncProCommon waitForStatusMessageToDisappear() {
   	int time = DEFAULT_TIMEOUT;
   	new WebDriverWait(driver, time)
   		.withMessage("Test Failed: Status message did not disappear after \"" + time + "\" seconds")
   		.until(ExpectedConditions.refreshed(
   			ExpectedConditions.invisibilityOfElementLocated(By.xpath(this.statusMessage_xpath))));
   	return this;
   }

   /***
    * <h1>waitForStatusMessageToDisappear</h1>
    * <p>purpose: Many times in ARGO, a black status message is displayed
    * after the backend has completed some task. Wait up to DEFAULT_TIMEOUT seconds for this
    * message to appear. Fail test if status message is not visible after DEFAULT_TIMEOUT
    * @return BidSyncProCommon
    */
   public BidSyncProCommon waitForStatusMessageToAppear() {
   	waitForVisibility(By.xpath(this.statusMessage_xpath));
   	return this;
   }


}
