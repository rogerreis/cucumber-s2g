package stepDefinitions.bidSyncPro.registration;

import core.CoreAutomation;
import io.cucumber.datatable.DataTable;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.And;
import data.bidSyncPro.registration.RandomChargebeeUser;
import data.bidSyncPro.registration.RandomCompanyInformation;
import data.bidSyncPro.registration.SalesRegion;
import data.bidSyncPro.registration.SubscriptionAddOns;
import data.bidSyncPro.registration.SubscriptionGrouping;
import data.bidSyncPro.registration.SubscriptionPlan;

import org.junit.Assert;

import pages.bidSyncPro.registration.CreateNewCompany;
import pages.bidSyncPro.registration.DatabaseRegistration;
import pages.bidSyncPro.registration.EnterKeywords;
import pages.bidSyncPro.registration.GetInvitedToBids;
import pages.bidSyncPro.registration.IncreaseBidRelevancy;
import pages.bidSyncPro.registration.JoinCompany;
import pages.bidSyncPro.registration.SelectNIGPCodes;
import pages.bidSyncPro.registration.SelectRegions;
import pages.bidSyncPro.registration.SetYourPassword;
import pages.bidSyncPro.registration.YourBidsAreReady;
import pages.bidSyncPro.supplier.companySettings.manageUsers.ActiveRequestRow;
import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.bidSyncPro.supplier.emails.RegistrationCompleteEmail;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.common.helpers.GeographyHelpers;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.helpers.RandomHelpers;
import pages.mailinator.Mailinator;
import pages.masonry.registration.*;
import pages.masonry.registration.bidSyncBasicRegistration.BasicRegistrationConfirmation;
import pages.masonry.registration.bidSyncBasicRegistration.BidSyncBasicRegistration;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.support.events.EventFiringWebDriver;


public class MasonrySupplierRegistration_stepDefinitions {

	private EventFiringWebDriver driver;
	private HelperMethods        helperMethods;
	private SelectRegions selectRegion;
	private SelectNIGPCodes selectNigpCode;
	private BidSyncBasicRegistration bidSyncBasicRegistration;

	public MasonrySupplierRegistration_stepDefinitions(CoreAutomation automation) {
		this.driver     = automation.getDriver();
		helperMethods   = new HelperMethods();
		selectRegion 	= new SelectRegions(driver);
		selectNigpCode  = new SelectNIGPCodes(driver);
		bidSyncBasicRegistration= new BidSyncBasicRegistration(driver);
	}
    

	/* ------------ Helpers ---------------- */

	/* ----- Masonry Registration Helpers ----- */

	/**
	 * <h1>goToRegisterUserPage</h1>
	 * <p> purpose: Navigate user to the free registration page<br>
	 *  1. Select Compare Plan<br>
	 *  2. Select Try for free button
	 *  @return BidSyncBasicRegistration
	 */
	private BidSyncBasicRegistration goToRegisterUserPage() {
		return new Pricing(driver).navigateToHere().clickComparePlansBtn().clickTryFreeBtn();
	}

	/***
	 * <h1>registerBidsyncBasicUserFromMasonry</h1>
	 * <p> purpose: Register a BidSync Basic User just on the Masonry site </p>
	 * 
	 * @param user = defined basic user
	 */
	private void registerBidSyncBasicUserFromMasonry(RandomChargebeeUser user) {
		goToRegisterUserPage();
		bidSyncBasicRegistration.registerUser(user);
		user.printUser();
	}

	/**
	 * <h1>ReRegisterBidSyncBasicUserFromMasonry</h1>
	 * <p> purpose : Register user with same mailId which is already in our database </p>
	 * <p> Note: Expecting error message</p>	
	 * @param user: RandomChargebeeUser = user's registration data
	 */
	private void ReRegisterBidSyncBasicUserFromMasonry(RandomChargebeeUser user) {
		goToRegisterUserPage();
		bidSyncBasicRegistration.initiateBasicRegistration(user);
	}

    /***
     * <h1>registerBidsyncProUserFromMasonryWithAddOnsAndLicenses</h1>
     * <p>purpose: Register a BidSync Pro User with addons and licenses just on the Masonry site<br>
     * 	Here, the addons are randomly selected (FEDERAL, MILITARY, CANADA)</p>
     * @param user = defined pro user
     * @param numberOfLicenses = number of licenses you want 
     */
    private void registerBidSyncProUserFromMasonryWithAddOnsAndLicenses(RandomChargebeeUser user, int numberOfLicenses) {
        new Pricing(driver)
                .navigateToHere()
                .selectPaidPlan(user.getSubscriptionPlan())
                .addLicenses(numberOfLicenses)
                .selectAddons(user)
                .subscribeUserWithNoExtras(user)
                .fillOutUserDataAndSubmit(user)
                .fillInBillingAddressAndSubmit(user)
                .fillInPaymentDetailsAndSubmit(user)
                .agreeToTermsAndSubscribe();

		user.printUser();
    }

    /***
     * <h1>registerBidsyncProUserFromMasonry</h1>
     * <p>purpose: Register a BidSync Pro User just on the Masonry site<br>
     * @param user = defined pro user
     */
    private void registerBidSyncProUserFromMasonry(RandomChargebeeUser user) {
    	this.registerBidSyncProUserFromMasonryWithAddOnsAndLicenses(user, 0);
    }
    
    /***
     * <h1>registerBidsyncBasicUser</h1>
     * <p>purpose: Register a BidSync Basic User from <br>
     * 	Masonry -> token extraction -> supplier registration</p>
     * @param user = defined basic user
     */
    private void registerBidsyncBasicUser(RandomChargebeeUser user) {
    	this.registerBidSyncBasicUserFromMasonry(user);
        this.completeCommonRegistrationSteps(user);
    }
    
    /***
     * <h1>registerBidsyncProUser</h1>
     * <p>purpose: Register a BidSync Pro User from <br>
     * 	Masonry -> token extraction -> supplier registration</p>
     * @param user = defined pro user
     */
    private void registerBidsyncProUser(RandomChargebeeUser user) {
    	this.registerBidSyncProUserFromMasonry(user);
        this.completeCommonRegistrationSteps(user);
    }
 
    /***
     * <h1>registerBidsyncProUserWithAddOnsAndLicenses</h1>
     * <p>purpose: Register a BidSync Pro User with addons and licenses from<br>
     * 	Masonry -> token extraction -> supplier registration<br>
     * 	Here, the addons are randomly selected (FEDERAL, MILITARY, CANADA)</p>
     * @param user = defined pro user
     * @param numberOfLicenses = number of licenses you want 
     */
    private void registerBidsyncProUserWithAddOnsAndLicenses(RandomChargebeeUser user, int numberOfLicenses) {
    	this.registerBidSyncProUserFromMasonryWithAddOnsAndLicenses(user, numberOfLicenses);
        this.completeCommonRegistrationSteps(user);
    }
   
	/* ----- Supplier Registration Helpers ----- */
     
    /***
     * <h1>retrieveEmail</h1>
     * <p>purpose: Retrieve the registration email from user inbox,<br>
     * 	and then click on the token email</p>
     * @param user = defined user registration
     */
    private void retrieveEmail(RandomChargebeeUser user) {
    	helperMethods.addSystemWait(15);
    	new Mailinator(driver)
        		.navigateToMailinatorInbox(user.getEmailAddress())
                .openRegistrationCompleteEmail()
                .checkInvalidDate(user.getEmailAddress())
                .clickOnTokenURLInEmail()
                .switchToWindow("S2G");
    }
    
    /***
     * <h1>supplierRegistrationToJoinOrCreateCompany</h1>
     * <p>purpose: Extract token and navigate through supplier registration to "Get Invited To Bids" (company lookup screen)</p>
     * @param user = defined user registration
     * @return GetInvitedToBids
     */
    private GetInvitedToBids supplierRegistrationToGetInvitedToBids(RandomChargebeeUser user) {
    	this.retrieveEmail(user);
    	helperMethods.addSystemWait(2);
    		return new SetYourPassword(driver)
        		.verifyNoTandC()
                .setAndConfirmPassword(user.getPassword());
    }
    
    /***
     * <h1>supplierRegistrationToJoinOrCreateCompany</h1>
     * <p>purpose: Extract token and navigate through supplier registration to "select company" (company lookup screen)</p>
     * @param user = defined user registration
     * @return JoinCompany
     */
    public JoinCompany supplierRegistrationSelectCompany(RandomChargebeeUser user) {
    	this.supplierRegistrationToGetInvitedToBids(user)
    		.inputCompanyIntoCompanyLookupOneCharAtATime(user.getCompany().getName());
    	return new  JoinCompany(driver);
    }
    
    /***
     * <h1>supplierRegistrationToEnterKeywords</h1>
     * <p>purpose: Extract token and navigate through supplier registration to "Enter Keywords" screen</p>
     * @param user = defined user registration
     * @return EnterKeywords
     */
    private EnterKeywords supplierRegistrationToEnterKeywords(RandomChargebeeUser user) {
    	return this.supplierRegistrationToGetInvitedToBids(user)
                .inputCompanyNameAndContinue(
                        user.getCompany().getName(),
                        user.getCompany().getAddressLine1(),
                        user.getCompany().getCity());
    }
    
    /***
     * <h1>supplierRegistrationToSalesRegion</h1>
     * <p>purpose: Extract token and navigate through supplier registration to "Enter Keywords" screen</p>
     * @param user = defined user registration
     * @return EnterKeywords
     */
    private void supplierRegistrationToSalesRegion(RandomChargebeeUser user) {
    	this.supplierRegistrationToGetInvitedToBids(user)
                .inputCompanyNameAndContinue(
                        user.getCompany().getName(),
                        user.getCompany().getAddressLine1(),
                        user.getCompany().getCity());
    	selectRegion.verifyPageInitialization();
    }
    
    
    private void supplierRegistrationToNoSalesRegionSelected(RandomChargebeeUser user) {
    	this.supplierRegistrationToGetInvitedToBids(user)
                .inputCompanyNameAndContinue(
                        user.getCompany().getName(),
                        user.getCompany().getAddressLine1(),
                        user.getCompany().getCity());
//    	#Fix for release OCT 7
    	GlobalVariables.storeObject("salesRegion", SalesRegion.None);
    }
    
    private void supplierRegistrationToSelectRegion(SalesRegion region) {
		helperMethods.addSystemWait(3);
		selectRegion.selectRegion(region)
    									.selectContinueButton();
    }
    
    private void supplierRegistrationToNigpScreen(RandomChargebeeUser user) {
   	 this.supplierRegistrationToEnterKeywords(user);
   	 this.supplierRegistrationToSelectRegion(SalesRegion.US);
   	 new EnterKeywords(driver).addPositiveKeywords(user.getPositiveKeywords())
               .addNegativeKeywords(user.getNegativeKeywords()) // dmidura: if null, update
               .clickNextButton();
   }
   
    private void supplierRegistrationToNIGP() {
    	selectNigpCode.clickOnUseKeyWordsInMyProfileButton()
    	.clickOnFirstNigpCodePlusIcon()
    	.getSelectedNigpCodeAndDescription()
    	.clickOnSaveAndContinueButton();
    }
    
    /***
     * <h1>supplierRegistrationToIncreaseBidRelevancy</h1>
     * <p>purpose: Extract token and navigate through supplier registration to "Increase Bid Relevancy" screen</p>
     * @param user = defined user registration
     * @return IncreaseBidRelevancy
     */
    private void supplierRegistrationToIncreaseBidRelevancy(RandomChargebeeUser user) {
    	 this.supplierRegistrationToEnterKeywords(user);
    	 this.supplierRegistrationToSelectRegion(SalesRegion.US);
    	 new EnterKeywords(driver).addPositiveKeywords(user.getPositiveKeywords())
                .addNegativeKeywords(user.getNegativeKeywords()) // dmidura: if null, update
                .clickNextButton();
    	 this.supplierRegistrationToNIGP();
    }
    
    
    /***
     * <h1>supplierRegistrationToIncreaseBidRelevancy</h1>
     * <p>purpose: Extract token and navigate through supplier registration to "Your Bids Are Ready" screen</p>
     * @param user = defined user registration
     * @return IncreaseBidRelevancy
     */
    private YourBidsAreReady supplierRegistrationToYourBidsAreReady(RandomChargebeeUser user) {
    	this.supplierRegistrationToIncreaseBidRelevancy(user);
        new IncreaseBidRelevancy(driver)
        	.verifyNavigatedToFirstBidRelevancyPage()
        	.clickThumbsUpOrThumbsDownAtRandom(5)
        	.waitForSpinner(driver);
        return new YourBidsAreReady(driver);
    }
    
    /**
     * <h1>completeCommonRegistrationSteps</h1>
     * <p>purpose: Assuming a pro or basic user was registered in Masonry, <br>
     * 	retrieve the user email, activate token, and then complete supplier registration.<br>
     * 	After completing registration, user will be logged into the bid list for the first time</p>
     * @param user
     */
    private void completeCommonRegistrationSteps(RandomChargebeeUser user) {

    	this.supplierRegistrationToYourBidsAreReady(user)
                .clickTakeMeToTheBidListBtn()
                .waitForPageLoad();
        //user.getId(); // populate the user's id from the database
        				// dmidura: this is currently killing the tests b/c the
        				// the frontend is running faster than the backend. Since we'd need
        				// between 8-10 seconds for the backend to catch up (and this is test step is now
        				// used by all registrations), commenting out the call. The features that may
        				// see breakage as a result are:
        				// Change Email.feature, Change Password.feature, CompanyProfile.feature, TrackingBidClicks.feature
        				// (most likely are tests in Change Email and Change Password that are currently tagged as @ignore)
        				// These tests should be updated as needed.
    	
    	// Create a login for this new user
    	SupplierLogin.thisUser.setAccountReference("New User");
    	SupplierLogin.thisUser.setEmail(user.getEmailAddress());
    	SupplierLogin.thisUser.setPassword(user.getPassword());
    }
    
    /***
     * <h1>supplierRegistrationToIncreaseBidRelevancy</h1>
     * <p>purpose: Extract token and navigate through supplier registration to "Your Bids Are Ready" screen</p>
     * @param user = defined user registration
     * @return IncreaseBidRelevancy
     */
    private YourBidsAreReady supplierRegistrationFromIncreaseBidRelevancy(RandomChargebeeUser user) {
    	new IncreaseBidRelevancy(driver)
        	.verifyNavigatedToFirstBidRelevancyPage()
        	.clickThumbsUpOrThumbsDownAtRandom(5)
        	.waitForSpinner(driver);
        return new YourBidsAreReady(driver);
    }

    
    /**
     * <h1>completeRegistrationStepsWithDifferentRegionSelection</h1>
     * <p>purpose: Assuming a pro or basic user was registered in Masonry, <br>
     * 	retrieve the user email, activate token, and then complete supplier registration, including selection of sales region.<br>
     * 	After completing registration, user will be logged into the bid list for the first time</p>
     * @param user, region
     */
    private void completeRegistrationStepsWithDifferentRegionSelection(RandomChargebeeUser user, SalesRegion region) {
    	System.out.println(GlobalVariables.getTestID() + " INFO: Select region and continue registration");
    	 this.supplierRegistrationToSelectRegion(region);
    	 GlobalVariables.storeObject("salesRegion", region);
    	 if(region.equals(SalesRegion.STATES_PROVINCE)) {
    		 selectRegion.selectRandomStates(5);
    		 selectRegion.getContinueButton().click();
    	 }
    		 
    	 this.completeRegistrationFromEnterKeyword(user);
    }
    
	private void completeRegistrationFromEnterKeyword(RandomChargebeeUser user) {
		new EnterKeywords(driver).addPositiveKeywords(user.getPositiveKeywords())
				.addNegativeKeywords(user.getNegativeKeywords()) // dmidura: if null, update
				.clickNextButton();
		this.supplierRegistrationToNIGP();
		new IncreaseBidRelevancy(driver).verifyNavigatedToFirstBidRelevancyPage().clickThumbsUpOrThumbsDownAtRandom(5)
				.waitForSpinner(driver);

		new YourBidsAreReady(driver).clickTakeMeToTheBidListBtn();
		new CommonTopBar(driver).waitForPageLoad();

// Create a login for this new user
		SupplierLogin.thisUser.setAccountReference("New User");
		SupplierLogin.thisUser.setEmail(user.getEmailAddress());
		SupplierLogin.thisUser.setPassword(user.getPassword());
	}

    /* ----- Other Helpers ----- */
	
    /***
     * <h1>setCannedAccount</h1>
     * <p>purpose: Set the canned account information. This is to be used 
     * 	only in User Management tests</p>
     */
	private void setCannedAccount() {
		// Canned Company and Owner
		// TODO dmidura: pull these values from a registration setup script
		RandomChargebeeUser      owner   = new RandomChargebeeUser();
		RandomCompanyInformation company = new RandomCompanyInformation();
		company.setName("QA User Manage 5");
		company.setAddressLine1("816 Congress Avenue");
		company.setCity("Austin");
		owner.setEmailAddress("qauser5@phimail.mailinator.com");
		owner.setPassword("P@ssw0rd");
		owner.setCompany(company);
		owner.setAddress("816 Congress Avenue");
		owner.setCity("Austin");
		GlobalVariables.storeObject("ownerRandomChargebeeUser", owner);
		
		// Subscription
		GlobalVariables.storeObject("ownerSubscriptionPlan", SubscriptionPlan.NATIONAL_ANNUAL);
		GlobalVariables.storeObject("ownerSubscriptionGrouping", SubscriptionGrouping.USA);
		List <SubscriptionAddOns> ownerAddOns = new ArrayList<SubscriptionAddOns>();
		ownerAddOns.add(SubscriptionAddOns.CANADA);
		ownerAddOns.add(SubscriptionAddOns.FEDERAL);
		ownerAddOns.add(SubscriptionAddOns.MILITARY);
		GlobalVariables.storeObject("ownerSubscriptionAddOns", ownerAddOns);
	}     
	
	
	/***
	 * <h1>logout</h1>
	 * <p>purpose: Log out of BidSync Pro</p>
	 * @return SupplierLogin
	 * @throws InterruptedException 
	 */
	private SupplierLogin logout() throws InterruptedException {
	        return new BidList(driver)
	        	.logout();
	}
	
	/***
	 * </h1>createRandomProUser</h1>
	 * <p>purpose: Given a Pro plan in Chargebee Format,
	 * 	create a RanomdChargebeeUser. Additionally, set the user's
	 * 	Subscription Plan and Grouping.
	 * @param plan = Chargebee Plan Name
	 * @return RandomChargebeeUser
	 */
	private RandomChargebeeUser createRandomProUser(String plan) {
		RandomChargebeeUser user          = new RandomChargebeeUser();
		SubscriptionPlan subscriptionPlan = SubscriptionPlan.fromString(plan);
		user.setSubscriptionPlan(subscriptionPlan);
		user.setSubscriptionGroupingBasedOnSubscription(subscriptionPlan);
		
		// Set Notification State
		if (!user.getSubscriptionPlan().isStatePlan()) { 
			// No notification state is set during Pro registration for National and Regional Plans
			user.setNotificationState(null); 
		} else {
			// Notification State is the selected state for a State Pro Plan
			user.setNotificationStateFromFullStateName(user.getSubscriptionGrouping().getChargebeeGroupingName());
		}
		
		return user;
	}
	
	
    /*------------- Test Steps -------------------*/

	/* ----- BidSync Basic Registration Test Steps -----*/

	/***
	 * <h1>I_register_a_BidSync_Basic_account_with_the_following_information</h1>
	 * <p>purpose: Register a new BidSync Basic account through the Masonry site with<br>
	 * 	the following values pulled from user dataTable input. Extract token and complete supplier registration.<br>
	 * 	User will then be logged into BidSync Pro for the first time</p>
	 * @param dTable:<br>
	 * | First Name    | value | <br>
	 * | Last Name     | value | <br>
	 * | Phone         | value | <br>
	 * | Address Line1 | value | <br>
	 * | City          | value | <br>
	 * | Zip           | value | <br>
	 * | State         | value | <br>
	 * | Country       | value | </p>
	 */
	@Given ("^I complete registration for a BidSync Basic account with the following information:$")
	public void I_complete_registration_for_a_BidSync_Basic_account_with_the_following_information(DataTable dTable) {
		Map <String, List <String>> registration = helperMethods.putDataTableIntoMap(dTable);

		RandomChargebeeUser user = new RandomChargebeeUser();
		user.setFirstName   (registration.get("First Name"   ).get(0));
		user.setLastName    (registration.get("Last Name"    ).get(0));
		user.setPhoneNumber (registration.get("Phone"        ).get(0));
		user.setAddress     (registration.get("Address Line1").get(0));
		user.setAddressLine2(null);
		user.setCity        (registration.get("City"         ).get(0));
		user.setZipCode     (registration.get("Zip"          ).get(0));
		user.setState       (registration.get("State"        ).get(0));
		user.setCountry     (registration.get("Country"      ).get(0));
		user.setNotificationStateFromFullStateName(user.getState());
		
		this.registerBidsyncBasicUser(user);
        GlobalVariables.storeObject("StoredUser", user);
	}

	@Given ("^I register a new user with a BidSync Basic Plan from the Masonry website$")
	public void I_register_a_new_user_with_a_BidSync_Basic_Plan_from_the_Masonry_website() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Registering a new BidSync Basic Plan from the Masonry website");
        RandomChargebeeUser user = new RandomChargebeeUser();
        this.registerBidSyncBasicUserFromMasonry(user);
        GlobalVariables.storeObject("StoredUser", user);
	}
	
	@Given ("^verify terms And condition opens in new window$")
	public void check_email_validity_to_register_a_new_user_with_a_BidSync_Basic_Plan_from_the_Masonry_website() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: verifying terms And condition opens in new window");
        goToRegisterUserPage();
		bidSyncBasicRegistration.getTermaAndConditionLink().click();
		bidSyncBasicRegistration.verifyTermsAndConditionOpensInNewWindow();
	}

	@Given ("^I register second user with a BidSync Basic Plan from the Masonry website$")
	public void I_register_second_user_with_a_BidSync_Basic_Plan_from_the_Masonry_website() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Registering second user with BidSync Basic Plan from the Masonry website");
		RandomChargebeeUser oldUser = GlobalVariables.getStoredObject("StoredUser");
        RandomChargebeeUser user = new RandomChargebeeUser();
        user.setCompany(oldUser.getCompany());
        user.setAddress(oldUser.getAddress());
        user.setCity(oldUser.getCity());
        GlobalVariables.storeObject("StoredUser", user);
        this.registerBidSyncBasicUserFromMasonry(user);
	}
	
	@Given("^I have registered a random user$")
    public void iHaveRegisteredARandomUser() {
        System.out.println(GlobalVariables.getTestID() + " --- TEST STEP: Register random user");
        RandomChargebeeUser user = new RandomChargebeeUser();
        registerBidsyncBasicUser(user);
        GlobalVariables.storeObject("StoredUser", user);
    }

    @Given("^I have registered a random Pro user with a password with every special character in it$")
    public void iHaveRegisteredARandomProUserWithAPasswordWithEverySpecialCharacterInIt() throws InterruptedException {
        String password = "aA1 !\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
        RandomChargebeeUser user = new RandomChargebeeUser();
        user.setPassword(password);
        GlobalVariables.storeObject("StoredUser", user);
        registerBidsyncBasicUser(user);
        this.logout();
    }
    
	@Given ("^I initiate registration for a BidSync Basic user on the Masonry website$")
	public void I_initiate_registration_for_a_BidSync_Basic_user_on_the_Masonry_website() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Initiating registration for Basic plan off Masonry");
        RandomChargebeeUser user = new RandomChargebeeUser();
        GlobalVariables.storeObject("StoredUser", user);
        this.goToRegisterUserPage()
        	.initiateBasicRegistration(user);
	}

	@When ("^I input \"(.*)\" phone extension along with random registration data into the Masonry Basic registration$")
	public void I_input_phone_extension_along_with_random_registration_data_into_the_Masonry_Basic_registration(String phoneExt) {
		 System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Inputting \"" + phoneExt + "\" along with random registration data into the phone ext field on Masonry Basic Plan");
		 RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		 user.setPhoneExtension(phoneExt);
		 bidSyncBasicRegistration.completeBasicRegistration(user);
		 GlobalVariables.storeObject("StoredUser", user);
	}

	@And ("^I submit the Masonry Basic registration$")
	public void I_submit_the_Masonry_Basic_registration() {
		 System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on submit button to submit Masonry Basic registration");
		 bidSyncBasicRegistration.clickSubmitButton();
	}

	@Then ("^I will verify that the validation attributes are correct for the Phone Number Extension for a Masonry Basic Registration$")
	public void I_will_verify_that_the_validation_attributes_are_correct_for_the_Phone_Number_Extension_for_a_Masonry_Basic_Registration() {
		 System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying Masonry phone extension contains correct validation attributes");
		 bidSyncBasicRegistration.verifyPhoneExtensionAttributesForValidation();
		 System.out.println(GlobalVariables.getTestID() + " Test Passed: Masonry phone extension contains correct validation attributes");
	}

	@Then ("^I will verify that Masonry Basic registration displays a Basic Registration Confirmation screen$")
	public void I_will_verify_that_Masonry_Basic_registration_displays_a_Basic_Registration_Confirmation_screen() {
		 System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying Masonry is displaying a Basic Registration Confirmation screen");
		 RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		 new BasicRegistrationConfirmation(driver).verifyConfirmationMessage(user.getFirstName(), user.getEmailAddress());
		 System.out.println(GlobalVariables.getTestID() + " Test Passed: Masonry displays a Basic Registration Confirmation screen");
	}
	
	@Then ("^I will verify that Masonry Basic registration will automatically update the phone extension to exclude the invalid inputs$")
	public void I_will_verify_that_Masonry_Basic_registration_will_automatically_update_the_phone_extension_to_exclude_the_invalid_inputs() {
		 System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying Masonry Basic registration automatically updates phone extension to exlude invalid inputs");
		 bidSyncBasicRegistration.verifyPhoneExtensionContainsOnlyNumbers();
		 System.out.println(GlobalVariables.getTestID() + " Test Passed: Verified that Masonry Basic phone extension field contains only numbers");
	}

	/*----- BidSync Pro Registration Test Steps ----- */
 
	@Given ("^I register a new user in Masonry with a \"(.*)\" subscription plan")
	public void I_register_a_new_user_in_Masonry_with_a_subscriptionType_plan(String plan) {
		 System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Registering a new user in Masonry with a " + plan + " plan");
		// Create a random user with the subscription
		RandomChargebeeUser user = this.createRandomProUser(plan);
		
		// Register in Masonry
		this.registerBidSyncProUserFromMasonry(user);

        GlobalVariables.storeObject("StoredUser", user);
	}
	
	@Given ("^I register a new company owner with a \"(.*)\" subscription plan, with add-ons and some licenses$")
	public void I_register_a_new_company_owner_with_a_subscriptionType_plan_with_addons_and_some_licenses(String plan) {
		 System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Registering a new company owner with a " + plan + " plan, plus addons and licenses from the Masonry website");
		 
		// Create a random user with specified subscription plan. Set subscription addOns.
		RandomChargebeeUser user = this.createRandomProUser(plan);
		user.setRandomSubscriptionAddOns();
		int intNumberOfLicenses = 4;
		this.registerBidsyncProUserWithAddOnsAndLicenses(user, intNumberOfLicenses);
		 
		// Set registration globals based on registration
		GlobalVariables.storeObject("ownerRandomChargebeeUser",  user);
		GlobalVariables.storeObject("ownerSubscriptionPlan",     user.getSubscriptionPlan());
		GlobalVariables.storeObject("ownerSubscriptionGrouping", user.getSubscriptionGrouping());
		GlobalVariables.storeObject("ownerSubscriptionAddOns",   user.getSubscriptionAddOns());
	}

   @Given("^I register a random \"([^\"]*)\" user$")
    public void iRegisterARandomUser(String plan) {
	   RandomChargebeeUser user = this.createRandomProUser(plan);
	    
	   GlobalVariables.storeObject("StoredUser", user);
	   registerBidsyncProUser(user);
    }

    @Given("^I register a random \"([^\"]*)\" user with all subscription addOns$")
    public void iRegisterARandomUser_with_all_subscription_addOns(String plan) {
	    RandomChargebeeUser user = this.createRandomProUser(plan);
	    List <SubscriptionAddOns> addOns = new ArrayList<SubscriptionAddOns>();
	    addOns.add(SubscriptionAddOns.CANADA);
	    addOns.add(SubscriptionAddOns.FEDERAL);
	    addOns.add(SubscriptionAddOns.MILITARY);
	    user.setAddOns(addOns);
	    
        GlobalVariables.storeObject("StoredUser", user);
        this.registerBidsyncProUserWithAddOnsAndLicenses(user, 0);
    }
 
	
	/* ----- Supplier Registration Test Steps ----- */

	@And("^I activate the email token sent in my registration complete email$")
	public void I_activate_the_email_token_sent_in_my_registration_complete_email() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Activating email token for registration email");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		SupplierLogin.thisUser.setEmail(user.getEmailAddress());
		this.retrieveEmail(user);
	}
	
	@When ("^I retrieve my registration email from Mailinator for my new generic Chargebee registration$")
	public void I_retrieve_my_registration_email_from_Mailinator_for_my_new_generic_Chargebee_registration() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Retrieving email from mailinator");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");

		new Mailinator(driver)
			.navigateToMailinatorInbox(user.getEmailAddress())
			.openRegistrationCompleteEmail();
	}

	@Then ("^I will verify that the registration email contains all expected elements$")
	public void I_will_verify_that_the_registration_email_contains_all_expected_elements() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying email contains all expected elements");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");

		new RegistrationCompleteEmail(driver)
			.verifyGreetingIsCorrect(user.getFirstName())
			.verifyBodyTextIsCorrect()
			.verifyFooterTextIsCorrect();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Registration email contains all expected elements");
	}
	
	@And ("^I have completed supplier registration up to the Set Your Password screen$")
	public void I_have_completed_supplier_registration_up_to_the_Set_Your_Password_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Completing supplier registration up to the \"Set Your Password\" screen");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		this.retrieveEmail(user);
	}
	
	@And ("^I have completed supplier registration up to select company$")
	public void I_have_completed_supplier_registration_up_to_select_company() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Completing supplier registration up to \"Company Settings\" screen");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		
		this.supplierRegistrationSelectCompany(user);
	}
	
	@And ("^I have completed supplier registration up to the Get Invited to Bids screen$")
	public void I_have_completed_supplier_registration_up_to_the_Get_Invited_to_Bids_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Completing supplier registration up to the \"Get Invited to Bids\" screen");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		this.supplierRegistrationToGetInvitedToBids(user);
	}
	
	@And ("^I have completed supplier registration up to sales region screen$")
	public void I_have_completed_supplier_registration_up_to_sales_region_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Completing supplier registration up to the \"Sales Region\" screen");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		this.supplierRegistrationToSalesRegion(user);
	}
	
	@And ("^I have completed supplier registration up to none selected as sales region$")
	public void I_have_completed_supplier_registration_up_to_none_selected_as_sales_region() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Completing supplier registration up to none selected as sales region");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		this.supplierRegistrationToNoSalesRegionSelected(user);
	}
	
	@And ("^I have completed supplier registration up to the Enter Keywords screen$")
	public void I_have_completed_supplier_registration_up_to_the_Enter_Keywords_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Completing supplier registration up to the \"Enter Keywords\" screen");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		this.supplierRegistrationToEnterKeywords(user);
		this.supplierRegistrationToSelectRegion(SalesRegion.US);
	}
	
	@And ("^I have completed supplier registration up to Bid Relevancy$")
	public void I_have_completed_supplier_registration_up_to_Bid_Relevancy() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Completing supplier registration up to the \"Bid Relevancy\" screen");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		this.supplierRegistrationToIncreaseBidRelevancy(user);
	}

	@And ("^I have completed supplier registration up to Your Bids Are Ready$")
	public void I_have_completed_supplier_registration_up_to_Your_Bids_Are_Ready() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Completing supplier registration up to the \"Your Bids Are Ready\" screen");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		this.supplierRegistrationToYourBidsAreReady(user);
	}

	@And ("^I complete supplier registration from Increase Bid Relevancy page$")
	public void I_complete_supplier_registration_from_Bid_Relevancy() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Completing supplier registration up to the \"Bid Relevancy\" screen");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		this.supplierRegistrationFromIncreaseBidRelevancy(user).clickTakeMeToTheBidListBtn().waitForPageLoad();

// Create a login for this new user
SupplierLogin.thisUser.setAccountReference("New User");
SupplierLogin.thisUser.setEmail(user.getEmailAddress());
SupplierLogin.thisUser.setPassword(user.getPassword());;
	}

	
	@And ("^I have completed supplier registration up to NIGP screen$")
	public void I_have_completed_supplier_registration_up_to_nigp_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Completing supplier registration up to the \"NIGP Codes\" screen");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		this.supplierRegistrationToNigpScreen(user);
	}
	
	@When("^I complete Supplier Registration$")
    public void I_complete_Supplier_Registration() {
    	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Completing supplier registration");
    	RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
    	this.completeCommonRegistrationSteps(user);
    }

	@When ("^I complete supplier registration and log in for the first time$")
	public void I_complete_supplier_registration_and_log_in_for_the_first_time() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Completing supplier registration and logging in for the first time");
		this.setCannedAccount();

		RandomChargebeeUser      owner  = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		RandomChargebeeUser      basic  = GlobalVariables.getStoredObject("StoredUser");

		basic.setCompany(owner.getCompany());
//		basic.setPositiveKeywords(Arrays.asList("apples", "oranges", "pineapple"));
		completeCommonRegistrationSteps(basic);
	}
	
	@And ("^I have completed supplier registration up to Company Details screen$")
	public void I_have_completed_supplier_registration_up_to_enter_company_details_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Completing supplier registration up to \"Company Details\" screen");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		 this.supplierRegistrationToGetInvitedToBids(user)
                .inputCompanyName(
                        user.getCompany().getName(),
                        user.getCompany().getAddressLine1(),
                        user.getCompany().getCity(),
                        user.getCompany().getState());
	}

	@And("^I complete supplier registration from Company Details screen$")
	public void I_complete_supplier_registration_from_Company_Details_Pagen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Completing supplier registration from \"Company Details\" screen");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		new CreateNewCompany(driver).clickNextButton();
		helperMethods.addSystemWait(1);
		new CreateNewCompany(driver).clickNextButton2();
		this.supplierRegistrationToSelectRegion(SalesRegion.US);
		new EnterKeywords(driver).addPositiveKeywords(user.getPositiveKeywords())
				.addNegativeKeywords(user.getNegativeKeywords()) 
				.clickNextButton();
		this.supplierRegistrationToNIGP();
		new IncreaseBidRelevancy(driver).verifyNavigatedToFirstBidRelevancyPage().clickThumbsUpOrThumbsDownAtRandom(5)
				.waitForSpinner(driver);

		new YourBidsAreReady(driver).clickTakeMeToTheBidListBtn().waitForPageLoad();
	}
	
    @Then("^I will be logged into BidSync for the first time and verify page initialization$")
    public void I_will_be_logged_into_BidSync_for_the_first_time_and_verify_page_initialization() {
    	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying user has logged into BidSync and page has correctly initialized");
    	
    	// Page initialization includes:
    	// 1. All Dashboard Common objects
    	// 2. All Common Top Bar objects
    	// 3. "New For You" selected and highlighted if pro plan
    	// 4. Each column header in the bid list 
    	new DashboardCommon(driver)
    		.verifyDashboardCommonInitializationForFirstLogin()
    		.getCurrentBidList()
    		.verifyColumnHeadersLoaded()
    		.verifyCommonTopBarLoaded();
    	
    	System.out.println(GlobalVariables.getTestID() + " Test Passed: User has logged into BidSync");
    }
    
    @Then("^That user can log in successfully$")
    public void thatUserCanLogInSuccessfully() {
        RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
        new SupplierLogin(driver)
                .navigateToHere()
                .loginWithCredentials(user.getEmailAddress(), user.getPassword());
        helperMethods.addSystemWait(2);
        String url        = driver.getCurrentUrl();
        String message    = "User was not able to log in and see the Bid List page.";
        String bidListUrl = System.getProperty("supplierBaseURL") + "dashboard/new-bids";
        Assert.assertEquals(message, bidListUrl, url);
    }
    

	/* ----- User Management Registration Test Steps ----- */
	
	@And ("^the new user joins an existing company$")
	public void the_new_user_joins_an_existing_company() throws InterruptedException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Joining existing company");

		// Set the canned account info
		this.setCannedAccount();

		RandomChargebeeUser basic  = GlobalVariables.getStoredObject("StoredUser");
		RandomChargebeeUser owner  = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		
        basic.setCompany(owner.getCompany());
//        basic.setPositiveKeywords(Arrays.asList("apples", "oranges", "pineapple"));
        completeCommonRegistrationSteps(basic);
        this.logout();
	}

	@When("^the new user joins the new company with available subscription licenses$")
	public void the_new_user_joins_the_new_company_with_available_subscription_licenses() {
		 System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Joining company with available subscription licenses");
		 
		 RandomChargebeeUser owner = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		 RandomChargebeeUser basic = GlobalVariables.getStoredObject("StoredUser");
		 String strOwnerEmail      = owner.getEmailAddress();
		 String strOwnerPassword   = owner.getPassword();
		 String strNewUserEmail    = basic.getEmailAddress();
		 
		 basic.setCompany(owner.getCompany());
		 basic.setPositiveKeywords(Arrays.asList("test", "services", "construction"));
        
        // Complete supplier registration and then save out the updates to the basic user
        completeCommonRegistrationSteps(basic);
        GlobalVariables.storeObject("StoredUser", basic);
        try {
			this.logout();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        // The company owner will then accept the user into their company
        // 1. Log into bidsync
        // 2. Locate user request under Manage Users screen
        // 3. Accept request
        
        ActiveRequestRow userRequest = new SupplierLogin(driver)
                .loginWithCredentials(strOwnerEmail, strOwnerPassword)
                .waitForPageLoad()
                .openCompanySettingsDropDown()
                .openManageUsers()
                .getRequestForEmail(strNewUserEmail);
        String message = String.format("No request for user '%s'", strNewUserEmail);
        Assert.assertNotNull(message, userRequest);
        try {
			userRequest.acceptRequest()
			        .logout();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        // Let the backend catch up -- this is necessary or else not all database states are set
        helperMethods.addSystemWait(3);
	}
	
	@When("^the new user joins a company with available subscription licenses$")
	public void the_new_user_joins_a_company_with_available_subscription_licenses() {
		 System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Joining company with available subscription licenses");
		 // Set data for the canned account. 
		 this.setCannedAccount();
		// And now actually the new company
		this.the_new_user_joins_the_new_company_with_available_subscription_licenses();
	}

   
    @And("^I register a new user at the same company$")
    public void iRegisterANewUserAtTheSameCompany() throws InterruptedException {
        System.out.println(GlobalVariables.getTestID() + " --- TEST STEP: Create new user at the previous user's company");
        RandomChargebeeUser user = new RandomChargebeeUser();
        RandomChargebeeUser companyOwner = GlobalVariables.getStoredObject("StoredUser");
        user.setCompany(companyOwner.getCompany());
        GlobalVariables.storeObject("CompanyJoiner", user);
        registerBidsyncBasicUser(user);
        this.logout();
    }

    @And("^I register (\\d+) new users at the same company$")
    public void iRegisterNewUsersAtTheSameCompany(int numUsers) throws InterruptedException {
        for(int i=0; i<numUsers; i++) {
            RandomChargebeeUser user         = new RandomChargebeeUser();
            RandomChargebeeUser companyOwner = GlobalVariables.getStoredObject("StoredUser");
            user.setCompany(companyOwner.getCompany());
            GlobalVariables.storeObject(String.format("CompanyJoiner %d", i), user);
            registerBidsyncBasicUser(user);
        }
        this.logout();
    }
    
    @Given("^I register 3 users at one company and 1 user at another$")
    public void iRegisterUsersAtOneCompanyAndUserAtAnother() throws InterruptedException {
        RandomChargebeeUser companyOwner           = new RandomChargebeeUser();
        RandomChargebeeUser secondUser             = new RandomChargebeeUser();
        RandomChargebeeUser thirdUser              = new RandomChargebeeUser();
        RandomChargebeeUser userFromAnotherCompany = new RandomChargebeeUser();
        
        secondUser.setCompany(companyOwner.getCompany());
        thirdUser .setCompany(companyOwner.getCompany());
        
        GlobalVariables.storeObject("StoredUser",             companyOwner);
        GlobalVariables.storeObject("CompanyJoiner 1",        secondUser);
        GlobalVariables.storeObject("UserFromAnotherCompany", userFromAnotherCompany);
        
        registerBidsyncBasicUser(companyOwner);
        registerBidsyncBasicUser(secondUser);
        registerBidsyncBasicUser(thirdUser);
        registerBidsyncBasicUser(userFromAnotherCompany);
        this.logout();
    
        System.out.println(String.format("Company Owner Id: '%s'", companyOwner.getId()));
        System.out.println(String.format("Second  User Id:  '%s'", secondUser.getId()));
        System.out.println(String.format("Third   User Id:  '%s'", thirdUser.getId()));
        System.out.println(String.format("User From Another Company Id: '%s'", userFromAnotherCompany.getId()));
    }

    /* ----- Database Verification for Registrations Test Steps ----- */
   
    @Then("^I will verify that a new \"(.*)\" user is created in the ARGO database with not empty customer_id$")
	public void I_will_verify_that_a_new_account_user_is_created_in_the_ARGO_database_with_not_empty_customer_id (String strSubscriptionPlanName) {
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that a new \"%s\" user was created in the ARGO database with not empty customer_id\n", strSubscriptionPlanName);
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");

		// 1. Verify that the ARGO database contains correct registration record for the users's registration
		// 2. Verify that the customer_id associated with this record is blank.
		// 3. Verify that the user status associated with this user is registered.
		new DatabaseRegistration(user.getEmailAddress())
			.verifyDatabaseRegistrationMatchesRegistrationValues(user)
			.verifyRegistrationCustomerIdIsNotBlank()
			.verifyRegistrationStatusIsRegistered();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: New user was correctly created in ARGO database with not empty customer_id immediately after Chargebee registration");
	}

    @And("^That user's data will be stored in the database$")
    public void thatUserSDataWillBeStoredInTheDatabase() {
    	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that user registration values were stored in ARGO database");
        RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
        new DatabaseRegistration(user.getEmailAddress())
        	.verifyDatabaseRegistrationMatchesRegistrationValues(user);
    	System.out.println(GlobalVariables.getTestID() + " Test Passed: User registration values were correctly stored in ARGO database");
    }
    
    @Then("^Verify that the users's registration status is 'V' in database$")
    public void verifyThatTheUsersSRegistrationStatusIsVInDatabase() {
        try {
        	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying the users's registration status is 'V' in ARGO database");
        	RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
    		new DatabaseRegistration(user.getEmailAddress())
    			.verifyRegistrationStatusIsVerified();
        }catch(Exception exp) {
        	fail("Test Failed: status field for user record in ARGO database is not displaying as verified immediately after email token activation" + exp);
        }
    }
    
    @Then("^Verify that the users's registration status is 'R' in database$")
    public void verifyThatTheUsersSRegistrationStatusIsRInDatabase() {
        	helperMethods.addSystemWait(5);
        	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying the users's registration status is 'R' in ARGO database");
        	RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
    		new DatabaseRegistration(user.getEmailAddress())
    			.verifyRegistrationStatusIsRegistered();
    }
    
    /* ----- Other Test Steps ----- */

    @And ("^I wait \"(.*)\" seconds for the backend to catch up$")
	public void I_wait_seconds_for_the_backend_to_catch_up(int seconds) {
		 System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Letting the backend catch up for " + seconds + "seconds.");
		 
		 // Let the backend catch up.
		 // This step should only be used when waiting on a third party integration to process an event 
		 // ex: Masonry/Chargebee linking to the database, Mailinator waiting to receive an email
		 helperMethods.addSystemWait(seconds);
	}

   
    @When("^I Navigate back to the Masonry site and enter the same email$")
    public void iNavigateBackToTheMasonrySiteAndEnterTheSameEmail() {
        try {
        	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Navigate to the Masonry site and enter the same email(already in the system)");
        	RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
        	ReRegisterBidSyncBasicUserFromMasonry(user);
        }catch(Exception exp) {
        	fail("Test Failed: status field for user record in ARGO database is not displaying as verified immediately after email token activation" + exp);
        }
    }
    @When("^Navigate back to the Masonry site and enter the same email with all capital letters$")
    public void navigateBackToTheMasonrySiteAndEnterTheSameEmailWithCapsOn() {
        	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Navigate to the Masonry site and enter the same email(already in the system)_with caps on");
        	RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
        	String emailArray[] = user.getEmailAddress().split("@");
        	user.setEmailAddress(emailArray[0].toUpperCase() +"@"+ emailArray[1]);
        	ReRegisterBidSyncBasicUserFromMasonry(user);
    }
    
    @Then("^Verify that the email is rejected with correct rejection message$")
    public void verifyThatTheEmailIsRejectedWithCorrectRejectionMessage(){
        	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify that the email is rejected with correct rejection message");
        	bidSyncBasicRegistration.verifyAccountExistsErrorMessage();
    }
    
    @Then("Select \"(.*)\" and continue registration$")
    public void selectRegionAndContinueRegistration(SalesRegion region) {
        	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Select region and continue registration");
        	RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
    		this.completeRegistrationStepsWithDifferentRegionSelection(user, region);
    }
    
	@And("Select \"(.*)\" as sales Region$")
	public void selectSalesRegion(SalesRegion region) {
			this.supplierRegistrationToSelectRegion(region);
	}
    
	@Then("select given states and province and complete registration$")
	public void selectGivenStatesAndProvinceAndCompleteRegistration() {
			RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
			this.supplierRegistrationToSelectRegion(SalesRegion.STATES_PROVINCE);
			GlobalVariables.storeObject("salesRegion", SalesRegion.STATES_PROVINCE);
			selectRegion.selectGivenStatesAndProvince(Arrays.asList("AZ","UT","CO"), Arrays.asList("AB","NT"));
			selectRegion.getContinueButton().click();
			this.completeRegistrationFromEnterKeyword(user);
	}
	
    
    /* ------------ REFACTOR BELOW ---------------- */
//	/***
//	 * <h1>I_will_verify_that_my_new_account_subscription_is_listed_in_the_ARGO_database_after_activating_the_registration_token</h1>
//	 * <p>purpose: User has registered via Chargebee registration and activated the token. This test step verifies that:<br>
//	 * 	1. The subscription plan in ARGO database matches the Chargebee subscription information<br>
//	 *  2. The subscription status in ARGO database is active<br>
//	 *  3. The subscription state in ARGO database is correct<br>
//	 *	4. The subscription ID in ARGO matches the subscription ID created in the Chargebee test site
//	 * NOTE: This method should be run AFTER setting values of the Chargebee test site subscription data
//	 */
}