package stepDefinitions.salesforce;

import core.CoreAutomation;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.salesforce.SalesforceLogin;
import pages.salesforce.SalesforceSearch;

import java.util.function.Function;

import static org.junit.Assert.fail;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class Salesforce_stepDefinitions {

	private EventFiringWebDriver driver;
	private SalesforceSearch salesforceSearch;
	private HelperMethods helperMethods;
	

	public Salesforce_stepDefinitions(CoreAutomation automation) {
		this.driver     = automation.getDriver();
		this.salesforceSearch = new SalesforceSearch(driver);
		helperMethods = new HelperMethods();
	}
	
	
    /* ------------ Helper Methods ---------------- */


    /* ------------ Test Steps ---------------- */

	@Given("^I log into to Salesforce$")
	public void iNavigateToSalesforce() throws Throwable {
		//driver.get("https://test.salesforce.com/");
		driver.get(System.getProperty("salesforceURL"));
		new SalesforceLogin(driver).Login("developers@periscopeholdings.com.partialdev", "bidsync");
	}

	@Given("^I lookup the random company in Salesforce$")
	public void lookupTheRandomCompany() throws Throwable {
		Thread.sleep(15000);  // wait for the information to make it into salesforce and be processed
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		String strCompany        = user.getCompany().getName();
		System.out.printf(GlobalVariables.getTestID() + " INFO: Company Name is %s\n", strCompany);
		salesforceSearch.HeaderSearch(strCompany);
		Thread.sleep(2000);
		
		// Waiting 30 seconds for an element to be present on the page, checking
		// for its presence once every 5 seconds.
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(Duration.ofSeconds(30))
				.pollingEvery(Duration.ofSeconds(5))
				.ignoring(NoSuchElementException.class);

		WebElement waitingElement = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				driver.navigate().refresh();
				return driver.findElement(By.xpath("//a[contains(text(),'" + strCompany + "')]"));
				}});
		
		waitingElement.click();
	}

	@Then("^the address in Salesforce will match the address given above$")
	public void theAddressWillMatchTheAddressGivenAbove() throws Throwable {
		driver.findElement(By.xpath("//div[contains(text(),'123 Congress')]"));
	//	driver.findElement(By.xpath("//div[contains(text(),'Austin, TX 78701')]"));
	}
	
	@Then("^the address in Salesforce will match the new address$")
	public void theAddressInSalesforceWillMatchTheNewAddress() throws Throwable {
		driver.findElement(By.xpath("//div[contains(text(),'321 Elm St')]"));
	//	driver.findElement(By.xpath("//div[contains(text(),'Sandy, UT 84094')]"));
	}
	
	@Then("^the Industry in Salesforce will show \"([^\"]*)\"$")
	public void theIndustryInSalesforceWillShow(String arg1) throws Throwable {
		driver.findElement(By.xpath("//span[contains(.,'Construction')]"));
	}


	@Then("verify the company data during registration persists in salesforce")
	public void verifyTheCompanyDataDuringRegistrationPersistsInSalesforce() {
	    try {
	    	RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
			String email       = user.getEmailAddress();
			salesforceSearch.HeaderSearch(email);
			helperMethods.addSystemWait(2);
			String userName = user.getFirstName()+" "+user.getLastName();
			salesforceSearch.clickOnGivenUserName(userName);
			salesforceSearch.verifyBidSyncProData(user);
	    }catch(Exception exp) {
	    	fail("TEST FAILED : verifyTheDataPersistsInSalesforce() : "+ exp);
	    }
	}
	
}

