package pages.bidSyncPro.supplier.account.myInfo;


import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class CurrentCompanyDialog</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects on the Current Company Dialog, that is part of the Update User Profile Dialog<br>
 * 	flows off of the My Profile (admin/my-profile) page</p>
 */
public class CurrentCompanyDialog extends BasePageActions{
	
	/*------ xpaths ------*/
	
	// top bar
	private final String closeBtn_xpath     = "//mat-card[@id='matUpdateUserProfileCard']//button";
	
	// mid section
	private final String companyLogo_xpath  = "//img[@id='currentCompanyLogo']";
	private final String companyName_xpath  = "//div[@id='currentCompanyName']/strong";
	private final String addressLine1_xpath = "//div[@id='currentCompanyAddress1']";
	private final String addressLine2_xpath = "//div[@id='currentCompanyAddress2']";
	private final String cityStateZip_xpath = "//div[@id='currentCompanyCityAddress']";
	
	// bottom bar
	private final String cancelBtn_xpath         = "//button[@id='CancelOnCompany'][contains(span, 'CANCEL')]";
	private final String joinNewCompanyBtn_xpath = "//button[@id='CreateNextButton2'][contains(span, 'JOIN NEW COMPANY')]";
	
	// Other
	private EventFiringWebDriver driver;

	public CurrentCompanyDialog(EventFiringWebDriver driver) {
		super(driver);
	    this.driver = driver;
	}
	
	/*------ getters -----*/
	
	// top bar
	private WebElement getCloseBtn()     { return findByVisibility(By.xpath(this.closeBtn_xpath));               }
	
	// mid section
	private WebElement getCompanyLogo()  { return findByVisibility(By.xpath(this.companyLogo_xpath));            }
	private String     getCompanyName()  { return findByVisibility(By.xpath(this.companyName_xpath)).getText();  }
	private String     getAddressLine1() { return findByVisibility(By.xpath(this.addressLine1_xpath)).getText(); }
	private String     getAddressLine2() { return findByVisibility(By.xpath(this.addressLine2_xpath)).getText(); }
	private String     getCityStateZip() { return findByVisibility(By.xpath(this.cityStateZip_xpath)).getText(); }

	
	// bottom bar
	private WebElement getCancelBtn()         { return findByVisibility(By.xpath(this.cancelBtn_xpath));         }
	private WebElement getJoinNewCompanyBtn() { return findByVisibility(By.xpath(this.joinNewCompanyBtn_xpath)); }
	
	/*----- methods -----*/
	
	/***
	 * <h1>closeCurrentCompanyDialog</h1>
	 * <p>purpose: Click on the "X" to close the Current Company Dialog</p>
	 * @return MyProfile
	 */
	public MyInfo closeCurrentCompanyDialog() {
		this.getCloseBtn().click();
		return new MyInfo(driver);
	}
	
	/***
	 * <h1>clickOnCancelBtn</h1>
	 * <p>purpose: Click on the "Cancel" button to close the Current Company Dialog</p>
	 * @return MyProfile
	 */
	public MyInfo clickOnCancelBnt() {
		this.getCancelBtn().click();
		return new MyInfo(driver);
	}

	/***
	 * <h1>clickOnJoinNewCompany</h1>
	 * <p>purpose: Click on the "Join New Company" button on the Current Company Dialog<br>
	 * 	OnClick, dialog should navigate forward to the Change Company (Company Lookup) screen</p>
	 * @return CompanyLookup
	 */
	public CompanyLookupDialog clickOnJoinNewCompanyBtn() {
		this.getJoinNewCompanyBtn().click();
		return new CompanyLookupDialog(driver);
	}
	
	/* ----- verifications ----- */
	
	/***
	 * <h1>verifyCurrentCompanyIsNotBlank</h1>
	 * <p>purpose: Verify that the Current Company displays the
	 * 	company name or company name + company address. Fail test if it doesn't.</p>
	 * @return CurrentCompanyDialog
	 */
	public CurrentCompanyDialog verifyCurrentCompanyIsNotBlank() {
		// We just want to make sure that something was set here
		// Address Line 2 is optional, so it is not included in this verification
		SoftAssertions softly = new SoftAssertions();
		softly.assertThat(this.getCompanyLogo()) .withFailMessage("Company Logo is null").isNotNull();
		softly.assertThat(this.getCityStateZip()).withFailMessage("Incorrrect Form of <city>, <state> <zip>").containsPattern("\\w*\\W*[,]{1}\\s[A-Z]{2}\\s\\d{5}");
		softly.assertThat(this.getAddressLine1()).withFailMessage("Address Line 1 is blank or null").isNotBlank();
		softly.assertThat(this.getCompanyName()) .withFailMessage("Incorrect form of company name").containsPattern("\\w*\\W*\\d*\\D*");
		softly.assertAll();
		
		return this;
	}
}
