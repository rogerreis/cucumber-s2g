@must @topBar @search @bidSearch_detail @level2
Feature: Dashboard - Top Bar Searches
  This feature file exercises the top bar search on the Supplier Dashboard
  	ARGO-1233: Filter bids
  	ARGO-1965: Stemming isn't working on bid relevance elasticseach index
  	ARGO-2225: Keywords filtering is having issues with special chars
  	BIDSYNC-50: Ability to search for bids by bid numbers
  	BIDSYNC-320: Include NIGP code and description in Elastic Search


@ARGO-1233
Scenario Outline: User searches via Top Bar Search:
	1. Tab switches to All Bids and highlights
	2. Results display in All Bids based on search

	Given I have logged into the Supplier side as user "Basic"
    #Fix for release OCT 7
	And I have navigated to the "All Bids" bid tab
	When I run a search on the Top Bar with keyword "<keyword>"
	Then I will verify that All Bids is both highlighted and selected, and also displays correct results per Top Bar keyword "<keyword>"
  
	Examples:
	| keyword |
	| water   |

  
@ARGO-2225
Scenario Outline: Keywords with special chars will return correct bid results

	Given I have logged into the Supplier side as user "Basic"
	#Fix for release OCT 7
	And I have navigated to the "All Bids" bid tab
	When I run a search on the Top Bar with keyword "<keyword>"
	Then I will verify that All Bids is both highlighted and selected, and also displays correct results per Top Bar keyword "<keyword>"
  
	Examples: some special chars
	| keyword        |
	|  Pre-qualified |
	|  R&D           |
 
 
@BIDSYNC-50
Scenario: Top Bar Search will return results for a search by bid number that contains spaces in the bid number
	Given I have logged into the Supplier side as user "National FMC"
	And I randomly select a bid with spaces in the bid number from the "All Bids" bid list
	When I run a Top Bar search using the bid number as the keyword
	Then I will verify that the correct bid is returned per the search for the bid number

	
@BIDSYNC-50
Scenario: Top Bar Search will return results for a search by bid number that contains dashes in the bid number
	Given I have logged into the Supplier side as user "National FMC"
	And I randomly select a bid with dashes in the bid number from the "All Bids" bid list
	When I run a Top Bar search using the bid number as the keyword
	Then I verify that the correct bid is returned per the search for the bid number

	
@ARGO-1965
Scenario: Stemming should work when searching the Bid List
	Given I have logged into the Supplier side as user "National FMC"
	#Fix for release OCT 7
	And I have navigated to the "All Bids" bid tab
	When I run a search on the Top Bar with keyword "pre"
	Then the bid list should have a bid with "pre-" in the title


@BIDSYNC-320 @ignore @release_ignore
Scenario: Top Bar Search will return results for a search by NIGP code
dmidura: This test assumes that we have a canned bid present to return results.
In the future, we may create the canned bid at the beginning of the test.
	Given I have logged into the Supplier side as user "National FMC"
	When I run a Top Bar search using the NIGP code "60-37" as the keyword
	Then I will verify that the following bid is displayed in the bid list:
	| Bid Title  | TEST                                 |
	| Bid Number | 02LGO-S302                          |
	| Bid Id     | 6c0284fd-c515-4f05-baae-d72c07ea44f4 |
	| Agency     | State of Nevada                      |
	| State      | NV                                   |
	| End Date   | 09/19/2019                           |
	| Added Date | 10/17/2018                           |
	