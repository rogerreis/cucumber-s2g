package pages.buyspeed.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import pages.buyspeed.agency.documents.bids.BidSummaryTab;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class TopSearchBar</h1>
 * @author dmidura
 * <p>details: This class houses methods and page objects for the top bar search common to ageny users.
 */
public class TopSearchBar extends BasePageActions {
	// other
		private EventFiringWebDriver driver;
		private final String bidResultsId = "bidSearchResultsForm";
		
		// xpaths
		private final String topSearchField_xpath        = "//input[@id='search_input']";
		private final String topSearchTypeDropdown_xpath = "//select[@id='console']";
		private final String topSearchBtn_xpath          = "//a[@id='searchBtn']";
		
		public TopSearchBar (EventFiringWebDriver driver){
			super(driver);
			this.driver = driver;
		}
		
		/* ----- getters ----- */
		private WebElement getTopSearchField()    { return findByVisibility(By.xpath(this.topSearchField_xpath));                     }
		private WebElement getTopSearchBtn()      { return findByVisibility(By.xpath(this.topSearchBtn_xpath));                       }
		private Select getTopSearchTypeDropdown() { return new Select (findByVisibility(By.xpath(this.topSearchTypeDropdown_xpath))); }
		
		/* ----- helpers ----- */
		
		/***
		 * <h1>enterSearchTermAndSearch</h1>
		 * @param searchTerm = term to enter into search input
		 * @param searchType = selection from dropdown for type of search
		 * @return TopSearchBar
		 */
		private TopSearchBar enterSearchTermAndSearch(String searchTerm, TopBarSearchType searchType) {
			// Put search string into top bar, select search type, and click search
			this.getTopSearchField().sendKeys(searchTerm);
			this.getTopSearchTypeDropdown().selectByVisibleText(searchTerm.toString());
			this.getTopSearchBtn().click();
			return this;
		}
		
		/* ----- methods ----- */
		
		/***
		 * <h1>searchForBidInTopBar</h1> 
		 * <p>purpose: Enter searchString into the top search bar,<br>
		 * 	select "Bid Solicitations" from the search type dropdown,<br>
		 * 	and then click on the spyglass (quick search) button<br>
		 * 	Assuming that the bid is located, then you'll be immediately<br>
		 * 	navigated to the Bid Summary tab.</p>
		 * @param searchTerm = the search string you want to enter
		 * @return BidSummaryTab
		 */
		public BidSummaryTab searchForBid(String searchTerm) {
			this.enterSearchTermAndSearch(searchTerm, TopBarSearchType.BID_SOLICITATIONS);
			waitForPresence(By.id(bidResultsId), 9);
			return new BidSummaryTab(driver);
		}
		
		// TODO: Add other searches
}
