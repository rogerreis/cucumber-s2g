package pages.buyspeed.agency.documents.common;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class DocumentStatusPopup</h1>
 * @author dmidura
 * <p>details: This class holds page objects and methods to access a generic document status popup, that is
 * located in the upper right hand corner of each document.
 */
public class DocumentStatusPopup extends BasePageActions {
	
	// xpaths
	private final String genericRow_xpath = "//tr[contains(@class, 'tableStripe-01') or contains(@class, 'tableStripe-02')]";
	private final String closeBtn_xpath   = "//input[@type='button'][@value='Close Window']";
	
	// Other
	private EventFiringWebDriver driver;

	public DocumentStatusPopup(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	/* ----- getters ----- */
	private WebElement getCloseBtn() { return findByVisibility(By.xpath(this.closeBtn_xpath)); }		

	/* ----- methods ----- */
	
	/***
	 * <h1>getAllStatusFromDocumentStatusTable</h1>
	 * <p>purpose: Get each row out of the Document Status table</p>
	 * @return List <DocumentStatusRow>
	 */
	public List <DocumentStatusRow> getAllStatusFromDocumentStatusTable() {
		List <WebElement> ele = this.findAllOrNoElements(driver, By.xpath(this.genericRow_xpath));
		
		List <DocumentStatusRow> allRows = new ArrayList<>();
		for (int i = 1; i < ele.size() + 1 ; i ++ ) {
			allRows.add(new DocumentStatusRow(driver, this.genericRow_xpath + "[" + i + "]")); }

		return allRows;
	}
	
	/**
	 * <h1>getRowByIndexFromDocumentStatusTable</h1>
	 * <p>purpose: get a row from the Document Status table by index</p>
	 * @param index = 0 is first row 
	 * @return DocumentStatusRow
	 */
	public DocumentStatusRow getRowByIndexFromDocumentStatusTable(int index) {
		return this.getAllStatusFromDocumentStatusTable().get(index);
	}

	/**
	 * <h1>getLastRowInDocumentStatusTable</h1>
	 * <p>purpose: get the last row from the Document Status table by index</p>
	 * @return DocumentStatusRow
	 */
	public DocumentStatusRow getLastRowInDocumentStatusTable(){
		List <DocumentStatusRow> allRows = this.getAllStatusFromDocumentStatusTable();
		return allRows.get(allRows.size() - 1);
	}
	
	/***
	 * <h1>closeDocumentStatusPopup</h1>
	 * <p>purpose: Click "Close" button to close popup,<br>
	 * 	and switch window back to main</p>
	 */
	public void closeDocumentStatusPopup() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Closing Document Status Popup");

		// Click close
		this.getCloseBtn().click();
		
		// And return to the main screen
		switchToWindow("QA Environment");
	}
	
}
