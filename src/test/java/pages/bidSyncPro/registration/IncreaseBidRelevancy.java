package pages.bidSyncPro.registration;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.bidSyncPro.common.BidSyncProCommon;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.RandomHelpers;

/***
 * <h1>Class IncreaseBidRelevancy</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects related to the objects on the "Increase Bid Relevancy" screen that is part of supplier registration</p>
 */
public class IncreaseBidRelevancy extends BidSyncProCommon {

    // ids
    private final String thumbsUpButton_id      = "btnVoteUp";
    private final String thumbsDownButton_id    = "btnVoteDown";
    private final String bidDescription_id      = "matCardBidContent";
    private final String bidTitle_id            = "matCardBidTitle";
    
    // xpaths
    private final String thumbsUpButton_xpath   = "//*[@id='btnVoteUp']";
    private final String thumbsDownButton_xpath = "//*[@id='btnVoteDown']";
	
	// Other
    private EventFiringWebDriver driver;
    private RandomHelpers        randomHelpers;
    private String strFirstPagePath = System.getProperty("supplierBaseURL") +  "setup/advanced/1/5"; // First page in Bid Relevancy
	
	public IncreaseBidRelevancy (EventFiringWebDriver driver) {
		super (driver);
	    this.driver        = driver;
	    this.randomHelpers = new RandomHelpers();
	}
	
	/* ----- getters ----- */
	
	private WebElement getThumbsUpButton()      { return findByVisibility(By.id(this.thumbsUpButton_id));   }
    private WebElement getThumbsDownButton()    { return findByVisibility(By.id(this.thumbsDownButton_id)); }
    private WebElement getBidDescriptionLabel() { return findByVisibility(By.id(this.bidDescription_id));   }
    private WebElement getBidTitleLabel()       { return findByVisibility(By.id(this.bidTitle_id));         }
    
    /* ----- methods ----- */

	/***
	 * <h1>clickThumbsDown</h1>
	 * <p>purpose: Click on "Thumbs Down" button</p>
	 * @return IncreaseBidRelevancy
	 */
	public IncreaseBidRelevancy clickThumbsDown() {
        findByScrollIntoViewTopOfScreen(By.xpath(this.thumbsDownButton_xpath));
		getThumbsDownButton().click();
		return this;
	}

	/***
	 * <h1>clickThumbsDown</h1>
	 * <p>purpose: Click on "Thumbs Down" button numberOfTimes in a row (advancing Bid Relevancy screen each time)</p>
	 * @param numberOfTimes = how many times to click Thumbs Up
	 * @return YourBidsAreReady
	 */    
    public YourBidsAreReady clickThumbsDown(int numberOfTimes) {
	    for(int i=0; i<numberOfTimes; ++i) {
           this.clickThumbsDown(); }
        return new YourBidsAreReady(driver);
    }

	/***
	 * <h1>clickThumbsUp</h1>
	 * <p>purpose: Click on "Thumbs Up" button</p>
	 * @return IncreaseBidRelevancy
	 */
	public IncreaseBidRelevancy clickThumbsUp() {
        findByScrollIntoViewTopOfScreen(By.xpath(this.thumbsUpButton_xpath));
		getThumbsUpButton().click();
		return this;
	}

	/***
	 * <h1>clickThumbsUp</h1>
	 * <p>purpose: Click on "Thumbs Up" button numberOfTimes in a row (advancing Bid Relevancy screen each time)</p>
	 * @param numberOfTimes = how many times to click Thumbs Up
	 * @return YourBidsAreReady
	 */    
    public YourBidsAreReady clickThumbsUp(int numberOfTimes) {
	    for(int i=0; i<numberOfTimes; ++i) {
           this.clickThumbsUp(); }
    	return new YourBidsAreReady(driver);
    }
    
	/***
	 * <h1>clickThumbsUpOrThumbsDownAtRandom</h1>
	 * <p>purpose: Randomly click on "Thumbs Up"  or "Thumbs Down" button numberOfTimes in a row (advancing Bid Relevancy screen each time)</p>
	 * @param numberOfTimes = how many times to randomly click Thumbs Up | Thumbs Down
	 * @return YourBidsAreReady
	 */    
    public YourBidsAreReady clickThumbsUpOrThumbsDownAtRandom(int numberOfTimes) {
	    for(int i=0; i<numberOfTimes; ++i) {
	    	if (randomHelpers.getRandomBoolean()) {
	    		this.clickThumbsUp();
	    	} else {
	    		this.clickThumbsDown(); } }
    	return new YourBidsAreReady(driver);
    }
	
	/***
	 * <h1>isBidValid</h1>
	 * <p>purpose: Determine if the bid is valid. To be valid:<br>
	 * 	1. Both Bid Title and Bid Description must return values<br>
	 * 	2. Either Bid Title or Bid Description must contain at least one keyword</p>
	 * @param positiveKeywords = list of positive keywords entered into bid relevancy
	 * @param negativeKeywords = list of negative keywords entered into bid relevancy
	 * @return true if bid is valid | false if bid is not valid
	 */
    @SuppressWarnings("unused")
	private Boolean isBidValid(List <String> positiveKeywords, List<String> negativeKeywords) {
		System.out.println(GlobalVariables.getTestID() + " INFO: Determining if Bid is Valid");
		Boolean bReturn = false;

		// Grab our bid info
		String strBidTitleText = getBidTitle();
		String strBidDescriptionText = getBidDescription();
		
		// Verify we've retrieved the bid info
		if (strBidTitleText == null | strBidDescriptionText == null) {
			System.out.println(GlobalVariables.getTestID() + " ERROR: Bid Title or Bid Description is null");
			return false; }

		System.out.println(GlobalVariables.getTestID() + " ------ Bid Information ------");	
		System.out.printf(GlobalVariables.getTestID() + " Bid Title      = \"%s\"\n", strBidTitleText);
		System.out.printf(GlobalVariables.getTestID() + " Bid Description = \"%s\"\n", strBidDescriptionText);
		System.out.println(GlobalVariables.getTestID() + " -----------------------------");	

		// Bid Info can't be blank
		if (strBidTitleText.isEmpty() | strBidDescriptionText.isEmpty()) { 
			System.out.println(GlobalVariables.getTestID() + " ERROR: Bid Title or Bid Description is blank");
			return false; }

		strBidTitleText 		= strBidTitleText.trim().toUpperCase();
		strBidDescriptionText 	= strBidDescriptionText.trim().toUpperCase();

		// Bid Title or Bid Description must contain at least one of the keywords
		for (String positiveKeyword : positiveKeywords ) {
			String positiveKeywordTest = positiveKeyword.trim().toUpperCase();
			if (strBidTitleText.contains(positiveKeywordTest) |
				strBidDescriptionText.contains(positiveKeywordTest)) {
				bReturn = true;
				System.out.printf(GlobalVariables.getTestID() + " SUCCESS: \"%s\" positive keyword is contained in Bid Description or Bid Title\n", positiveKeyword);
				break;
			}
		}
		
		if (!bReturn) { System.out.println(GlobalVariables.getTestID() + " WARNING: Neither Bid Description nor Bid Title contains any of the positive keywords!"); }

		// Bid Title and Bid Description should not contain the negative keywords
		for (String negativeKeyword : negativeKeywords ) {
			String negativeKeywordTest = negativeKeyword.trim().toUpperCase();

			if (strBidTitleText.contains(negativeKeywordTest) |
				strBidDescriptionText.contains(negativeKeywordTest)) {
				bReturn = false;
				System.out.printf(GlobalVariables.getTestID() + " WARNING: \"%s\" negative keyword is incorrectly contained in Bid Description or Bid Title\n", negativeKeyword);
				break;
			}
		}
		
		return bReturn;
	}
	
	/***
	 * <h1>getBidTitle</h1>
	 * <p>purpose: Return the Bid Title text displaying on the current Bid Relevancy page</p>
	 * @return String = title text currently displaying
	 */
	private String getBidTitle() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Returning the Bid Title");
		return getBidTitleLabel().getText();
	}
	
	/***
	 * <h1>getBidDescription</h1>
	 * <p>purpose: Return the Bid Description text displaying on the current Bid Relevancy page</p>
	 * @return String = description text currently displaying
	 */
	private String getBidDescription() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Returning the Bid Description");
		return getBidDescriptionLabel().getText();
	}
	
	/***
	 * <h1>verifyNavigatedToFirstBidRelevancyPage</h1>
	 * <p>purpose: Verify app is on the first page in the Bid Relevancy screens (since there are five pages total)<br>
	 * 	Fail test if url is not <supplierBaseURL>/setup/advanced/1/5</p>
	 * @return IncreaseBidRelevancy
	 */
    public IncreaseBidRelevancy verifyNavigatedToFirstBidRelevancyPage() {
    	waitForSpinner(driver);
    	int time = DEFAULT_TIMEOUT;
    	new WebDriverWait(driver, time)
    		.withMessage("Test Failed: Bid Relevancy is not on first page after \"" + time + "\" seconds")
    		.until(ExpectedConditions.urlToBe(this.strFirstPagePath));
    	return this;
    }

	
}
