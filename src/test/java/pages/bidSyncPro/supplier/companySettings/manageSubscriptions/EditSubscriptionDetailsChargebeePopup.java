package pages.bidSyncPro.supplier.companySettings.manageSubscriptions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import data.bidSyncPro.registration.SubscriptionPlan;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>EditSubscriptionDetailsChargebeePopup</h1>
 * @author dmidura
 * <p>details: This class contains objects and methods to manipulate the <br>
 * 	"Edit Subscription Details" Chargebee Popup that appears after the user <br>
 *  clicks on "Edit Subscription" on the "Subscription Details" Chargebee popup</p>
 */
public class EditSubscriptionDetailsChargebeePopup extends BasePageActions {
	
	// xpaths
	private final String subscriptionDetailsBackBtn_xpath ="//div[contains(span, 'Edit Subscription Details')]";
	private final String subscriptionPlanDropdown_xpath   = "//select[@type='text'][@class='cb-payment__select']";
	private final String updateSubscriptionBtn_xpath      = "//button[contains(@class, 'cb-button')][contains(span, 'Update Subscription')]";
	
	// Other
    private EventFiringWebDriver driver;
	
	public EditSubscriptionDetailsChargebeePopup(EventFiringWebDriver driver) {
		super(driver);
		this.driver   = driver;
	}
	
	/* ----- getters ------ */
	private Select     getSubscriptionPlanDropdown()   { return new Select (findByVisibility( By.xpath(this.subscriptionPlanDropdown_xpath)));}
	private WebElement getUpdateSubscriptionPlanBtn()  { return findByVisibility(By.xpath(this.updateSubscriptionBtn_xpath));      }
	private WebElement getSubscriptionDetailsBackBtn() { return findByVisibility(By.xpath(this.subscriptionDetailsBackBtn_xpath)); }
	
	/* ----- methods ------ */

	/***
	 * <h1>selectSubscriptionPlanFromDropdown</h1>
	 * <p>purpose: Select a subscription plan from the dropdown update subscription plan</p>
	 * @param SubscriptionPlan
	 * @return EditSubscriptionDetailsChargebeePopup 
	 */
	public EditSubscriptionDetailsChargebeePopup selectSubscriptionPlanFromDropdown(SubscriptionPlan subscriptionPlan) {
		this.getSubscriptionPlanDropdown().selectByVisibleText(subscriptionPlan.getChargebeeSubscriptionDropdownName());
		return this;
	}

	/***
	 * <h1>clickOnUpdateSubscription</h1>
	 * <p>purpose: Click on the "Update Subscription" button. Fail if button is<br>
	 * 	not visible after 5 seconds (button initializes on page as inactive <br>
	 *  and becomes active after new subscription plan is selected from the<br>
	 *  subscription plan dropdown. After clicking on "Update Subscriptions"<br>
	 *  user is automatically navigated to the "Subscription Details" screen</p>
	 * @return SubscriptionDetailsChargebeePopup 
	 */
	public SubscriptionDetailsChargebeePopup clickOnUpdateSubscription() {
		waitForClickability(By.xpath(this.updateSubscriptionBtn_xpath));
		this.getUpdateSubscriptionPlanBtn().click();
		return new SubscriptionDetailsChargebeePopup(driver);
	}
	
	/***
	 * <h1>clickOnSubscriptionDetailsBackBtn</h1>
	 * <p>purpose: Click on the "Subscription Details" back button.<br>
	 *  to return to the "Subscription Details" screen</p>
	 * @return SubscriptionDetailsChargebeePopup   
	 */
	public SubscriptionDetailsChargebeePopup  clickOnSubscriptionDetailsBackBtn() {
		this.getSubscriptionDetailsBackBtn().click();
		return new SubscriptionDetailsChargebeePopup(driver);
	}
}
