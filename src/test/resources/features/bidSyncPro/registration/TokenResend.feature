@must @registration @level3
Feature: Supplier Registration - Token Resend Screen
BIDSYNC-762: Unable to verify token - user in locked status
	
@BIDSYNC-762
Scenario: Unable to verify token - user in locked status
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	Then Verify that the users's registration status is 'R' in database	
	When I navigate my browser to the supplier login page
	And attempt 6 plus times to login to lock the account
	Then I will be locked out for 20 min
	When I click Forgot Your Password
	And I enter mail id to reset my password as directed and press enterKey
	Then I should see an incomplete registration popup with resend verification email option
	And I should see the email sent popup message once I resent the verification email
	And I should be redirected to complete the registration by selecting the link on email 

# TODO:  Scenario: Initialization of Token Resend Page

# TODO: ARGO-2582

@BIDS_290
Scenario: Test Email address is case sensitive when resending verification email
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	Then Verify that the users's registration status is 'R' in database	
	Then Navigate back to the Masonry site and enter the same email with all capital letters
	Then I should see an incomplete registration popup with resend verification email option on reset password
	Then I should see the email sent popup message once I resent the verification email on reset password
	And I have completed supplier registration up to Your Bids Are Ready
	
	