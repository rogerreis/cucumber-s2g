@must @administration @level2
Feature: Integrations > SalesForce - Push Company Data to Salesforce 

@ARGO-1712 
Scenario: Integrations > SalesForce - New Pro Registration creates Classic account 
	Given I complete registration for a BidSync Basic account with the following information: 
		| First Name    | Test          |
		| Last Name     | User          |
		| Phone         | 5121231234    |
		| Address Line1 | 123 Congress  |
		| City          | Austin        |
		| Zip           | 78701         |
		| State         | Texas         |
		| Country       | United States |
	And I log into to Salesforce 
	When I lookup the random company in Salesforce 
	Then the address in Salesforce will match the address given above 
	Given I navigate my browser to the supplier login page 
	And I log into BidSync Pro as my new BidSync Basic user 
	Then I will be on the supplier Home page 
	Given I have navigated to Company Settings > "Company Profile" 
	And I click the Edit Company Name icon 
	And I change the supplier Company Name to random chars 
	And I change the supplier Industry to "construction" 
	And I click the Save button on the supplier Company Profile page 
	And I click the Edit Company Information icon 
	And I change the supplier address to the address below 
		| Address Line1 | 321 Elm St    |
		| City          | Sandy         |
		| Zip           | 84094         |
		| State         | Utah          |
		| Country       | United States |
	And I click the Save button on the supplier Company Profile page 
	When I log into to Salesforce 
	And I lookup the random company in Salesforce 
	Then the address in Salesforce will match the new address 
	# mandrews:  this field doesn't exist anymore in salesforce?  Checking with Jason
	#And the Industry in Salesforce will show "construction"
	

@BIDS-43
Scenario: Push company data on registration to salesforce 
	Given I have registered a random user 
	Then I log into to Salesforce 
	And verify the company data during registration persists in salesforce 
	
	
