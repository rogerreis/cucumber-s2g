package pages.bidSyncPro.supplier.emails;

import static org.junit.Assert.fail;

import org.apache.commons.text.StringEscapeUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import data.bidSyncPro.registration.SubscriptionPlan;
import pages.common.helpers.GlobalVariables;

public class RevokeSubscriptionLicenseEmail extends GenericEmail {
	
	
	// Other
    private EventFiringWebDriver driver;
	
	public RevokeSubscriptionLicenseEmail(EventFiringWebDriver driver) {
		super(driver, "RevokeSubscriptionEmail.json"); 
		this.driver 	= driver;
	}
	
	/* ----- methods ----- */
	
	/**
	 * <h1>findSearchString</h1> 
	 * <p>purpose: We want to use a string in our input strArray to search, but it must not be empty.<b>
	 *    Run through the strArray and return the first non empty string. If the string contains a
	 *    variable tag, then return a non-variable part of the string</p>
	 * @param strArray = String [] of inputs
	 * @return String = a string that is not empty | empty if no matching string
	 */
	private String findSearchString (String[] strArray) {
		for (String i : strArray) {
			// Since we can have newlines in our body text, make sure we're not using a newline as our search string
			if (!i.isEmpty()) { 

				// Make sure we're not using a search string with a variable token
				// if we are, then we'll split the string and use the non-variable part of the string
				if (i.contains("<SUBSCRIPTION>")) {
					String [] strSplit = i.split("<SUBSCRIPTION>");
					return strSplit[1]; }
				return i; }}

		return "";
	}
	
	public Boolean isBodyTextCorrect(String strPlanName) {
		// Extract the subscription plan from the email
		String subscriptionPlan = extractSubscriptionPlan();
		
		// Verify that the plan matches
		if(!subscriptionPlan.equals(strPlanName)) {
			fail ("Test Failed: Subscription plan listed in body of subscription removed email is not displaying as \"" + strPlanName + "\"" ); }
		
		return true;
	}
	
	private String extractSubscriptionPlan() {
		String strSearchPath = "//tbody//td[contains(.,\"" + findSearchString(this.strBodyTxt).trim() + "\")]";
		
		// Now we'll verify the body text
		String strExpectedText = formatTextToMatchEmailOutput(strBodyTxt);
		String strDisplayedBodyText = StringEscapeUtils.escapeJava(driver.findElement(By.xpath(strSearchPath)).getText().toString());

		System.out.printf(GlobalVariables.getTestID() + " \n--------- Expected Email Body ---------\n");
		System.out.println(strExpectedText);
		System.out.printf(GlobalVariables.getTestID() + " ----------------------------------------\n");

		System.out.printf(GlobalVariables.getTestID() + " \n--------- Displayed Email Body ---------\n");
		System.out.println(strDisplayedBodyText);
		System.out.printf(GlobalVariables.getTestID() + " ----------------------------------------\n");

		// Create different locators from the expected and displayed text
		String [] Locator1 = strExpectedText.split("<SUBSCRIPTION>");
		String [] Locator2 = strDisplayedBodyText.split("-");
		
		
		// body text form: <text1> <subscriptionName> - <timePeriod> text2
		// subscriptionName = Locator1[0] - Locator2[0]
		String subscriptionName = Locator2[0].replace("\\n","").replace(Locator1[0], "").trim();
		
		// timePeriod = Locator1[1] - Locator2[1]
		String timePeriod = Locator2[1].replace("\\n","").replace(Locator1[1], "").trim();
		
		System.out.println(GlobalVariables.getTestID() + " INFO: Extracted subscription plan as \"" + subscriptionName + " - " + timePeriod + "\"");
		return subscriptionName + " - " + timePeriod;
	}
	
	/* ----- verifications ----- */
	
	/***
	 * <h1>verifyEmailIsCorrect</h1>
	 * <p>purpose: Verify that the revoke subscription email is appearing as expected<br>
	 * 	(per the RevokeSubscriptionEmail.json template)</p>
	 * @param SubscriptionPlan = the expected subscription plan for the user
	 */
	public void verifyEmailIsCorrect(SubscriptionPlan planName) {
		System.out.println(GlobalVariables.getTestID() + " INFO: Verifying that the Revoke Subscription email contains all expected elements");

		// Greeting
		if (!this.strGreetingTxt.isEmpty()) { fail("Test Failed: The revoke subscription email contains an unexpected greeting");}
		
		// Body
		if (!this.isBodyTextCorrect(planName.getChargebeeSubscriptionName())) { fail("Test Failed: The revoke subscription email body text is incorrect");}
		
		// Footer
		try {
			if (!this.isFooterTextCorrect()) { fail("Test Failed: Footer text in the revoke subscription email is incorrect");}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
}
