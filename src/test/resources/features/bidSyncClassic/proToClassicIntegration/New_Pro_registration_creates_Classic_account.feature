@must @administration @level2
Feature: Integrations > Pro/Classic BidSync - New Pro Registration Creates Classic Account
	ARGO-476: Create BidSync Classic Account (if not one)

@ARGO-476
Scenario: New Pro Registration creates Classic account
	Given I have registered a random user
	When I verify that my user was created in Classic
		And I click the Users link at the bottom of the Supplier Edit page
		And I type the random email into the Email field in the User Search pane
		And I click the Find button in the User Search pane
		And I click the Edit button for the vendor user in the User Search pane
		And the vendor user edit pane should appear without error
	Then I will validate the Company and User fields of the Supplier Edit page hold the correct information
		And I will verify that the Pro and Classic accounts are linked
