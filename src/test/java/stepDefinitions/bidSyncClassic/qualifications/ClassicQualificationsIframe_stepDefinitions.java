package stepDefinitions.bidSyncClassic.qualifications;

import core.CoreAutomation;
import cucumber.api.java.en.Then;
import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.common.helpers.HelperMethods;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

public class ClassicQualificationsIframe_stepDefinitions {

	public CoreAutomation automation;
	public EventFiringWebDriver driver;
	public DashboardCommon dashboardCommon;
	public CommonTopBar dashboardTopBar;
	public HelperMethods helperMethods;

	public ClassicQualificationsIframe_stepDefinitions(CoreAutomation automation) throws Throwable {
		this.automation = automation;
		this.driver = automation.getDriver();
		dashboardCommon = new DashboardCommon(driver);
		dashboardTopBar = new CommonTopBar(driver);
		helperMethods = new HelperMethods();
	}

	/* ------------ Test Steps ---------------- */
	@Then("^I am on the classic Qualifications iFrame$")
	public void iAmOnTheClassicQualificationsIFrame() throws Throwable {
		// wait for iframe to appear and there's a weird refresh that happens
		helperMethods.addSystemWait(10);
		String strServer = System.getProperty("server");
		driver.switchTo().frame(driver.findElement(By.xpath(".//iframe[@src='https://classic.phi-" + strServer + ".cloud/bidsync-app-web/vendor/admin/CompanyProfileMini.xhtml']")));
		driver.findElement(By.xpath("//a[contains(.,'My agencies')]")).click();
	}
	
	@Then("^I can exercise the objects inside the frame$")
	public void iCanExerciseTheObjectsInsideTheFrame() throws Throwable {
		driver.findElement(By.xpath("//a[contains(.,'My agencies')]")).click();
		Select dropdown = new Select (driver.findElement(By.xpath("//select[contains(@id,'vendorAdminTabView:vendorQualificationsForm:j_idt')]")));
		dropdown.selectByVisibleText("Galactic Empire");
		driver.findElement(By.xpath("//span[contains(.,'TESTME')]")).click();
	}
}
	