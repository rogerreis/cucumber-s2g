@must @filterResults @bidSearch_detail @level3
Feature: Dashboard - Filter Results Section - Functionality Tests
	This feature file exercises the objects related to the functionality of the Filter Results collapsible section on the Supplier Dashboard
  	ARGO-419: Collapsible Filter Section
  	ARGO-1233: Filter bids
  	ARGO-1539: Load More & Date Range Issues
  	ARGO-2142: unable to turn off "Include Past Bids"
  	ARGO-2392: Automation update tests for - Front end - Modify search time frame available options
  	BIDSYNC-39: Search Criteria clears after looking at a bid
  	BIDSYNC-304:  Filter Results section initializing as open rather than closed on login


@ARGO-419 @ARGO-1233 
Scenario: Filter Results Collapsible Section Can Open and Close

    Given I have logged into the Supplier side as user "Basic"
    Then I will verify that the Filter Results collapsible section is open
    When I close the Filter Results Section
    Then I will verify that the Filter Results collapsible section is closed
    
@ARGO-419 @ARGO-1233 @BIDSYNC-304
Scenario: Filter Results section initializes all expected objects 
    1. Sections headers initialize as correctly opened/closed,
    2. All bottom buttons appear and are properly labeled
    3. Section titles are properly labled and show a dropdown icon

    Given I have logged into the Supplier side as user "Basic"
    Then I will verify that the Filter Results Section initializes correctly
    
@ARGO-419 @ARGO-1233
Scenario: Keywords section displays all expected objects
	1. Default placeholder message
	2. Input field and "+" button
	3. Chip list with >= 3 chips
	Also verify section header can open/close

    Given I have logged into the Supplier side as user "User to Verify Default Keywords"
    When I view the Filter Results Keywords Section
    Then I will verify that the Filter Results Keywords Section initializes correctly

@ARGO-419 @ARGO-1233 
Scenario: Negative Keywords section displays all expected objects
	1. Default placeholder message
	2. Input field and "+" button
	3. Chip list with >= 0 chips 
	4. Text "Hides bids with any of these words"
	Also verify section header can open/close

    Given I have logged into the Supplier side as user "User to Verify Default Keywords"
    When I view the Filter Results Negative Keywords Section
    Then I will verify that the Filter Results Negative Keywords Section initializes correctly

@ARGO-419 @ARGO-1233
Scenario: States/Provinces section displays all expected objects
	1. "Search all states/provinces" checkbox appears with text, and is defaulted on
	After checking, expected objects appear per:
	   1. Default placeholder message
	   2. Input field and "+" button
	   3. Empty chip list
	Also verify section header can open/close

    Given I have logged into the Supplier side as user "Basic"
    When I view the Filter Results States/Provinces Section
    Then I will verify that the Filter Results States/Provinces Section initializes correctly

@ARGO-419 @ARGO-1233
Scenario: Bid End Date section displays all expected objects
	1. "Include past bids" checkbox appears with text, and is defaulted off
	After unchecking, expected objects appear per:
	   1. "SELECT TIME FRAME" dropdown
	Finally, when checking again:
	   1. "SELECT TIME FRAME" dropdown hides
	Also verify section header can open/close

    Given I have logged into the Supplier side as user "Basic"
    When I view the Filter Results Bid End Date Section
    Then I will verify that the Filter Results Bid End Date Section initializes correctly
    
    
@ARGO-1233
Scenario Outline: "Custom Time Frame" date selectors do not appear when user selects any value except "Custom Time Frame" from "SELECT TIME FRAME"
    Given I have logged into the Supplier side as user "Basic"
    When I select "<selectedValue>" from the SELECT TIME FRAME dropdown
    Then I will verify I will not see the Custom Time Frame date selectors

    Examples: SELECT TIME FRAME values that are not "Custom Time Frame"
      | selectedValue     |
      | Past 6 Months     |
      | Past Year         |
      | Past 3 Years      |


@ARGO-1233 @ARGO-2392 
Scenario: "Custom Time Frame" date selectors appear when user selects "Custom Time Frame" from "SELECT TIME FRAME"

    Given I have logged into the Supplier side as user "Basic"
    When I select "Custom Time Frame" from the SELECT TIME FRAME dropdown
    Then I will verify I will see the Custom Time Frame date selectors
    And I will verify that Show Bids From date selector is defaulted to 1/1/2016


@ARGO-1233
Scenario: Keyword Section Allows Keyword Addition and Deletion

    Given I have logged into the Supplier side as user "Basic"
    When I enter and save generic keywords for the Filter Results Keywords section
    Then I will verify that the generic keywords display in the Filter Results Keywords section chip list 
    And I will verify that I can delete each chip from the Filter Results Keywords chip list

@ARGO-1233
Scenario: Negative Keyword Section Allows Keyword Addition and Deletion

    Given I have logged into the Supplier side as user "Basic"
    When I enter and save generic keywords for the Filter Results Negative Keywords section
    Then I will verify that the generic keywords display in the Filter Results Negative Keywords section chip list 
    And I will verify that I can delete each chip from the Filter Results Negative Keywords chip list

@ARGO-1233
Scenario: States/Provinces Section Allows Keyword Addition and Deletion

    Given I have logged into the Supplier side as user "Basic"
    When I enter and save generic keywords for the Filter Results States/Provinces section
    Then I will verify that the generic keywords display in the Filter Results States/Provinces section chip list 
    And I will verify that I can delete each chip from the Filter Results States/Provinces chip list

@ARGO-1233 @BIDS-462
Scenario: Clicking on Clear All Button will Remove Displayed Filters

    Given I have logged into the Supplier side as user "Basic"
	And I have entered generic filters into the Filter Results
    When I click on the Clear All button on the Filter Results Section
    Then I will verify that no search filters are currently displaying on the Filter Results Section


@ARGO-2142
Scenario: "Include Past Bids" checkbox can be turned off from the Filter Results, and will not uncheck

	Given I have logged into the Supplier side as user "Basic"
	And I run a search on the Filter Results section with a Custom Timeframe
	When I uncheck the Include past bids checkbox and click Search
	Then I will verify that the Include past bids checkbox does not uncheck


@ARGO-1233 @retrytest
Scenario: OnClick of Load Default Filters will display the keywords from the user's profile

  Given I have logged into the Supplier side as user "User to Verify Default Keywords"
  When I click on the Load Default button on the Filter Results Collapsible section
  Then I will verify that the filters set from my user profile are displaying on the Filter Results 
  

@ARGO-1233
Scenario: OnClick of Edit Default Filters will take user to their profile to edit/update keywords
  Given I have logged into the Supplier side as user "User to Verify Default Keywords"
  When I click on the Edit Default Filters button on the Filter Results Collapsible section
  Then I will verify that I can edit my default filters from the User Profile page
  

@ARGO-1539
Scenario: Custom Time Frame: When "Show Bids to" is set to before "Show Bids From" then:
	1. Validation should be displayed on the Filter Results
	2. No bids should return on All Bids

  Given I have logged into the Supplier side as user "Basic"
  When I manually enter a Show Bids to date that is before the Show Bids From date
  Then validation will trigger on Show Bids From and I will not be returned any bids 


@BIDSYNC-39
Scenario: Filter results displays search filters and bid list displays search results after user views bid
	
	Given I have logged into the Supplier side as user "National FMC"
	And I run a generic search from the Filter Results section
	And I randomly select a bid from the currently displayed bid list
	And I will verify that all of the objects on the bid details screen have correctly populated
	When I click on the BACK TO BIDS button to return to the bid list
	Then I will verify that my unsaved search correctly displays filters in the Filter Results section
	And I will verify that All Bids is both highlighted and selected, and also displays correct results per the displayed Filter Results search filter
	
@BIDS-452
Scenario Outline: Delete single keyword when it's longer than 28 characters 
	Given I have logged into the Supplier side as user "Basic" 
	When I enter and save keyword "<keyword>" for the Filter Results Keywords section 
	Then I will verify that the keyword "<keyword>" display in the Filter Results Keywords section chip list 
	And I will verify that I can delete keyword "<keyword>" from the Filter Results Keywords chip list 
	
	Examples: 
		|keyword					   |
		|123456789012345678901234567890|
		|123456789012345678901234567890123456789012345678901234567890|
		|123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890|
 



