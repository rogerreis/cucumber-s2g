package pages.masonry.registration.bidSyncProRegistration;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import data.bidSyncPro.registration.RandomChargebeeUser;
import data.bidSyncPro.registration.SubscriptionAddOns;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.BasePageActions;

import java.util.concurrent.TimeUnit;

/***
 * <h1>Class BidSyncProCheckout</h1>
 * <p>details: This class houses page objects and methods for the BidSync Pro checkout page<br>
 * 	that is part of the BidSync Pro registration off the Masonry site. On this screen, the BidSync Pro registration user<br>
 * 	will select which plan to register with, which addOns to check, how many licenses to purchase, and verify that their email is unique.<br>
 *	Note: This page initializes differently depending on the selections made on the Pricing page. As such, this class should be called<br>
 *	      after running methods on the Pricing page, so that BidSyncProCheckout page initializes correctly</p>
 * </p>
 */
public class BidSyncProCheckout extends BasePageActions{
    
    // ids
    private final String stateDropdown_id           = "state-pro";
    private final String regionDropdown_id          = "regional-pro";
    private final String emailInput_id              = "email";
    
    // xpaths
    private final String verifyEmailButton_xpath    = "//input[contains(@value, 'Verify')]";
    private final String federalBidsCheckbox_xpath  = "//div[contains(@class, 'show-addon')]//div[contains(@data-addon, 'federal')]//div[contains(@class, 'checkmark')]";
    private final String militaryBidsCheckbox_xpath = "//div[contains(@class, 'show-addon')]//div[contains(@data-addon, 'military')]//div[contains(@class, 'checkmark')]";
    private final String canadaBidsCheckbox_xpath   = "//div[contains(@class, 'show-addon')]//div[contains(@data-addon, 'canada')]//div[contains(@class, 'checkmark')]";
    private final String addLicenseBtn_xpath        = "//div[contains(@class, 'show-addon')]//div[contains(@class, 'up-counter')][contains(@class, 'increment')]";
    
    // Other
    private EventFiringWebDriver driver;
    private HelperMethods helperMethods;

    public BidSyncProCheckout(EventFiringWebDriver driver) {
    	super(driver);
        this.driver   = driver;
        helperMethods = new HelperMethods();
    }
    
    /* ----- getters ----- */
    private WebElement getFederalBidsCheckbox()  { return findByVisibility(By.xpath(this.federalBidsCheckbox_xpath));     } 
    private WebElement getMilitaryBidsCheckbox() { return findByVisibility(By.xpath(this.militaryBidsCheckbox_xpath));    } 
    private WebElement getCanadaBidsCheckbox()   { return findByVisibility(By.xpath(this.canadaBidsCheckbox_xpath));      } 
    private WebElement getAddLicenseBtn()        { return findByVisibility(By.xpath(this.addLicenseBtn_xpath));           } 
    private Select getStateDropdown()            { return new Select(findByVisibility(By.id(this.stateDropdown_id)));     } 
    private Select getRegionDropdown()           { return new Select(findByVisibility(By.id(this.regionDropdown_id)));    } 
    private WebElement getEmailInput()           { return findByVisibility(By.id(this.emailInput_id));                    } 
    private WebElement getVerifyEmailButton()    { return findByClickability(By.xpath(this.verifyEmailButton_xpath), Integer.parseInt(System.getProperty("defaultTimeOut"))); }
    
    /**
     * @param areaCode - '123' for state, '124' for regional, '125' for national
     * @param durationCode - 'm' for monthly, 's' for 6 month, 'y' for annual
     */
    private WebElement getCheckoutButton(String areaCode, String durationCode) {
    	int time = DEFAULT_TIMEOUT;
        String buttonId = String.format("%s-%s", areaCode, durationCode);
        findByScrollIntoViewBottomOfScreen(By.id(buttonId));
        waitForClickability(By.id(buttonId), time);
        new WebDriverWait(driver, time)
                .until(ExpectedConditions.attributeContains(By.id(buttonId), "class", "selected" ));
        helperMethods.addSystemWait(2000, TimeUnit.MILLISECONDS);
        return driver.findElement(By.id(buttonId));
    }
    
    /* ----- helpers ----- */

    /***
     * <h1>waitForDisappearanceOfLoadingModal</h1>
     * <p>purpose: modal needs time to load</p>
     */
    private void waitForDisappearanceOfLoadingModal() {
        helperMethods.addSystemWait(4); }

    /* ----- methods ----- */
    
    /***
     * <h1>selectAddOns</h1>
     * <p>purpose: Based on the addOns stored in the RandomChargebeeUser object, select appropriate addOns<br>
     * 	during Pro registration. Note: This method should be run before the subscribeUserWithNoExtras method</p>
     * @param user - RandomChargebeeUser
     * @return BidSyncProCheckout
     */
    public BidSyncProCheckout selectAddons(RandomChargebeeUser user) {
    	findByScrollIntoViewBottomOfScreen(By.xpath(this.canadaBidsCheckbox_xpath));
    	System.out.println(GlobalVariables.getTestID() + " INFO: Registering for addOns " + user.getSubscriptionAddOns().toString());
    	for (SubscriptionAddOns addOn :user.getSubscriptionAddOns()) {
    		switch (addOn) {
    		case FEDERAL:
    			this.getFederalBidsCheckbox().click();
    			break;
    		case MILITARY:
    			this.getMilitaryBidsCheckbox().click();
    			break;
    		case CANADA:
    			this.getCanadaBidsCheckbox().click();
    			break;
    		}
    	}
    	return this;
    }
    
    /***
     * <h1>addLicenses</h1>
     * <p>purpose: Add licenses during pro registration<br>
     * 	Note: This method should be run before the subscribeUserWithNoExtras method</p>
     * @param intNumberOfLicenses = number of licenses your user wants
     * @return BidSyncProCheckout
     */
    public BidSyncProCheckout addLicenses(int intNumberOfLicenses) {
    	System.out.println(GlobalVariables.getTestID() + " INFO: Registering for \"" + intNumberOfLicenses + "\" licenses");
    	findByScrollIntoViewBottomOfScreen(By.xpath(this.addLicenseBtn_xpath));
    	for (int i = 0; i < intNumberOfLicenses ; i ++) {
    		this.getAddLicenseBtn().click(); }
    	return this;
    }
    
    /***
     * <h1>subscribeUserWithNoExtras</h1>
     * <p>purpose: From the BidSync Pro Checkout page:<br>
     * 	1. Select appropriate state | region from dropdown -- if user is subscribing for State or Regional plan<br>
     * 	2. Enter email and verify email uniqueness within BidSync Pro system<br>
     *	3. Click "CONTINUE" button to initiate Chargebee punchout registration<br>
     *	Chargebee punchout registration (via Chargebee modals) will commence to complete Pro user's registration.</p>
     * @param user = user registration data
     * @return AddAccountDetailsModal
     */
    public AddAccountDetailsModal subscribeUserWithNoExtras(RandomChargebeeUser user) {
    	
    	// Select appropriate dropdown based on subscription plan
    	// National plan does not update with dropdown
    	if(user.getSubscriptionPlan().isStatePlan()) {
    		// State plan - pick state from state dropdown
    		getStateDropdown().selectByVisibleText(user.getState());
    	} else if (user.getSubscriptionPlan().isRegionalPlan()) {
    		// Region plan - select region from region dropdown
    		String grouping = user.getSubscriptionGrouping().getChargebeeGroupingName();
    		getRegionDropdown().selectByVisibleText(grouping);}
        
    	// Enter email and verify uniqueness 
    	// Note: Test fails if email is not unique
        getEmailInput().sendKeys(user.getEmailAddress());
        getVerifyEmailButton().click();
        waitForDisappearanceOfLoadingModal();
        
        // Click on "Continue" 
        // Note: Continue button locator is linked to user subscription plan (subscription plan and duration)
        // so we have to specify locator based on subscription
        switch(user.getSubscriptionPlan()) {
            case STATE_MONTHLY:
                getCheckoutButton("123", "m").click();
                break;
            case STATE_SIX_MONTH:
                getCheckoutButton("123", "s").click();
                break;
            case STATE_ANNUAL:
                getCheckoutButton("123", "y").click();
                break;
            case REGIONAL_MONTHLY:
                getCheckoutButton("124", "m").click();
                break;
            case REGIONAL_SIX_MONTH:
                getCheckoutButton("124", "s").click();
                break;
            case REGIONAL_ANNUAL:
                getCheckoutButton("124", "y").click();
                break;
            case NATIONAL_MONTHLY:
                getCheckoutButton("125", "m").click();
                break;
            case NATIONAL_SIX_MONTH:
                getCheckoutButton("125", "s").click();
                break;
            case NATIONAL_ANNUAL:
                getCheckoutButton("125", "y").click();
                break;
            default:
                Assert.fail(String.format("Invalid subscription plan '%s'", user.getSubscriptionPlan().toString()));
                return null;
        }
        return new AddAccountDetailsModal(driver);
    }
    
    
}
