package pages.bidSyncPro.supplier.dashboard.bidLists;

import pages.bidSyncPro.supplier.dashboard.bids.BidDetails;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.BasePageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import data.bidSyncPro.registration.SubscriptionAddOns;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/***
 * <h1>Class BidListRow</h1>
 * <p>details: This class defines all the page objects and methods for each row in the bid list.<br>
 */
public class BidListRow extends BasePageActions{
    
    private EventFiringWebDriver driver;

    private String  row_xpath;              // xpath to this row in the bid list
    public  BidCriteria bidInfo;            // Holds all the required data elements of the bid
    private String  relevancyBarStyle;      // for use when determining the relevancy bar indicator percentage
    private String  bidURL;                 // bid URL

    // xpaths
    // stars
    private final String starButton_xpath 	= "//div[contains(@class, 'results-save-bid')]";
    private final String starIcon_xpath 	= "//mat-icon";
    
    // bid info
    private final String bidTitle_xpath 	= "//div[contains(@class, 'result-title')]";
    private final String bidTitleBtn_xpath 	= "//div[contains(@class, 'result-title')]/a";

//    #Fix for release OCT 7
//    private final String bidNumber_xpath 	= "//div[@class='lightest-gray result-bid-number']";
    private final String bidNumber_xpath 	= "//div[contains(@class, 'result-bid-number')]/span"; 
    private final String bidNumber_NewForYou_xpath 	= "//div[contains(@aria-label, 'bid number') and @fxshow.lt-xl]";//commented to fix "I randomly select a bid from the currently displayed bid list"
//    private final String bidNumber_NewForYou_xpath 	= "//div[contains(@aria-label, 'bid number')]";
private final String newForYouActive = "//a[@id='aNewBids_0' and contains(@class,'mat-tab-label-active')]";

    private final String agency_xpath 		= "//div[contains(@class, 'result-agency')]";
    private final String state_xpath 		= "//div[contains(@class, 'result-state')]";
    private final String dueDate_xpath 		= "//span[contains(@class, 'result-bid-end-date')]";
    private final String added_xpath 		= "//span[contains(@class, 'result-bid-added-date')]";

    // relevance indicator
    private final String relevancyBar_xpath = "//bidsync-relevancy-meter//div[contains(@class, 'bar match') or contains(@class, 'bar not') "
    										+ "or contains(@class, 'bar high') or contains(@class, 'bar low') or contains(@class, 'bar medium-match')]";
    private final String matchContainer_xpath = "//bidsync-relevancy-meter";
    private final String matchContainerText_xpath = "//bidsync-relevancy-meter/div[contains(@class, 'relavancy-meter')]/span";

    // for plan upgrades
    private final String upgradePlan_xpath 	= "//div[@class='result-agency']//a[contains(@aria-label,'Upgrade Plan')]";
    private final String strUpgradePlanText = "UPGRADE PLAN";  // text that displays instead of the agency name



    public BidListRow(EventFiringWebDriver driver, String row_xpath ) {
        super(driver);
        this.driver = driver;
        bidInfo = new BidCriteria();

        // Make sure that the bid row is visible before recording its attributes
        findByScrollIntoViewBottomOfScreen(By.xpath(row_xpath));
        waitForVisibility(By.xpath(row_xpath), Integer.parseInt(System.getProperty("defaultTimeOut")));

        /* ----- Required Bid Attributes: ----- */

        this.row_xpath = row_xpath;
        this.bidURL = findByVisibility(By.xpath(row_xpath + this.bidTitle_xpath + "//a")).getAttribute("href");
        String[] partitions = this.bidURL.split("/");
        String bidId = partitions[partitions.length - 1];

        bidInfo.setBidId(bidId);
        bidInfo.setBidTitle(findByVisibility(By.xpath(row_xpath + this.bidTitle_xpath)).getText());
        bidInfo.setState(findByPresence(By.xpath(row_xpath + this.state_xpath)).getAttribute("innerText"));
        bidInfo.setEndDate(findByVisibility(By.xpath(row_xpath + this.dueDate_xpath)).getText());
        bidInfo.setAddedDate(findByVisibility(By.xpath(row_xpath + this.added_xpath)).getText());

        // Upgrade Plan Mode
        if (!this.doesPageObjectExist(driver, By.xpath(row_xpath + agency_xpath))) {
            // When user needs to upgrade plan, Agency shows as "UPGRADE PLAN"
            bidInfo.setAgency(findByVisibility(By.xpath(row_xpath + upgradePlan_xpath)).getText());
        } else {
            // When user does not need to upgrade plan, Agency shows agency text
            bidInfo.setAgency(findByVisibility(By.xpath(row_xpath + agency_xpath)).getText());
        }

		if (this.doesPageObjectExist(driver, By.xpath(newForYouActive))) {
			bidInfo.setBidNumber(findByVisibility(By.xpath(row_xpath + bidNumber_NewForYou_xpath)).getText());
		} else {
			if (!this.doesPageObjectExist(driver, By.xpath(row_xpath + bidNumber_xpath))
					& bidInfo.getAgency().equals(strUpgradePlanText)) {
				// When user needs to upgrade plan, bid number is blanked out
				bidInfo.setBidNumber("");
			} else {
				// When user does not need to upgrade plan, bid number is displayed
				
		    	// Screen Size problem - Bid Id values displayed in two different places when minimize and maximize the screen.
				// small screen
				String bidNumber_xpath1 = row_xpath + this.bidNumber_xpath;
			   	boolean isBidNumberVisible = driver.findElement(By.xpath(bidNumber_xpath1)).isDisplayed();
			    // big screen
			   	if( !isBidNumberVisible) {
			   		bidNumber_xpath1 = "("+bidNumber_xpath1+")[2]";
			   	}
				bidInfo.setBidNumber(findByVisibility(By.xpath(bidNumber_xpath1)).getText());

			}
		}
        /* -----  Optional Bid Attributes: ----- */
        
        // Relevance Indicator 
        // will only be present for bid in "All Bids" or "New For You" bid lists
        if (this.doesPageObjectExist(driver, By.xpath(row_xpath + this.relevancyBar_xpath))) {
            this.relevancyBarStyle = findByVisibility(By.xpath(row_xpath + relevancyBar_xpath)).getAttribute("style");
        }
	}
        

//    #Fix for release OCT 7
//    added one extra parameter boolean forNewForYouTab -  true for that tab(div), having xpath differ from all Bids Tab(span)

    public BidListRow(EventFiringWebDriver driver, String row_xpath, boolean forNewForYouTab ) {
    	super(driver);
        this.driver        = driver;
        bidInfo            = new BidCriteria();
        
        // Make sure that the bid row is visible before recording its attributes
        findByScrollIntoViewBottomOfScreen(By.xpath(row_xpath));
        waitForVisibility(By.xpath(row_xpath), Integer.parseInt(System.getProperty("defaultTimeOut")));

        /* ----- Required Bid Attributes: ----- */

        this.row_xpath      = row_xpath;
        this.bidURL         = findByVisibility(By.xpath(row_xpath + this.bidTitle_xpath + "//a")).getAttribute("href");
        String[] partitions = this.bidURL.split("/");
        String bidId        = partitions[partitions.length-1];

        bidInfo.setBidId(bidId);
        bidInfo.setBidTitle (findByVisibility(By.xpath(row_xpath + this.bidTitle_xpath)).getText());
        bidInfo.setState    (findByPresence(By.xpath(row_xpath   + this.state_xpath)).getAttribute("innerText"));
        bidInfo.setEndDate  (findByVisibility(By.xpath(row_xpath + this.dueDate_xpath)).getText());
        bidInfo.setAddedDate(findByVisibility(By.xpath(row_xpath + this.added_xpath)).getText());
   
        // Upgrade Plan Mode
        if (!this.doesPageObjectExist(driver, By.xpath(row_xpath + agency_xpath))) {
        	// When user needs to upgrade plan, Agency shows as "UPGRADE PLAN"
        	bidInfo.setAgency(findByVisibility(By.xpath(row_xpath + upgradePlan_xpath)).getText());
        } else {
        	// When user does not need to upgrade plan, Agency shows agency text
            bidInfo.setAgency(findByVisibility(By.xpath(row_xpath + agency_xpath)).getText()); }
    
        
        	// When user does not need to upgrade plan, bid number is displayed
            //    #Fix for release OCT 7different xpath for new for you and all bids tab

        	if(forNewForYouTab) {
        		 bidInfo.setBidNumber(findByVisibility(By.xpath(row_xpath + bidNumber_NewForYou_xpath)).getText());
        	}else {
                if (!this.doesPageObjectExist(driver, By.xpath(row_xpath + bidNumber_xpath)) & bidInfo.getAgency().equals(strUpgradePlanText)) {
                	// When user needs to upgrade plan, bid number is blanked out
                	bidInfo.setBidNumber("");
        		bidInfo.setBidNumber(findByVisibility(By.xpath(row_xpath + bidNumber_xpath)).getText());
        	}
        }


        /* -----  Optional Bid Attributes: ----- */
        
        // Relevance Indicator 
        // will only be present for bid in "All Bids" or "New For You" bid lists
        if (this.doesPageObjectExist(driver, By.xpath(row_xpath + this.relevancyBar_xpath))) {
            this.relevancyBarStyle = findByVisibility(By.xpath(row_xpath + relevancyBar_xpath)).getAttribute("style");
        }

    }

    /* ----- getters ----- */

    private WebElement getBidTitleBtn()           { return findByVisibility(By.xpath(this.row_xpath + this.bidTitleBtn_xpath)); }
    private WebElement getScoreElement()          { return findByVisibility(By.xpath(row_xpath + matchContainer_xpath));        }
    private WebElement getRelevancyBarElement()   { return findByVisibility(By.xpath(row_xpath + relevancyBar_xpath));          }
    private WebElement getMatchContainerElement() { return findByVisibility(By.xpath(row_xpath + matchContainerText_xpath));    }
    private WebElement getThisRow()               { return findByPresence(By.xpath(this.row_xpath));                          }


    public String getXpath()                      { return this.row_xpath; }
    public String getBidURL()                     { return this.bidURL;    }
    
    /* ----- helpers ----- */
    
    /***
     * <h1>shouldBidBeAccessible</h1>
     * <p>purpose: Is the bid supposed to be accessible for this user?</p> 
     * @return true if bid is in subscription or addOn or is Basic bid | false if bid is not in subscription or addOn<br>
     * 	Note: Regardless of subscription, a Basic bid is always in the user's subscription</p>
     */
    private Boolean shouldBidBeAccessible() {
        // User's viewing perms on this bid
        // Note: We are using the teaser-bid property to determine if bid is supposed to be accessible.
    	//       If the bid contains the "teaser-bid" attribute, then bid should not be accessible.
    	//       When the "teaser-bid" attribute does not appear (getAttribute will return null), 
    	//       then the user should have access to the bid.
        return this.getThisRow().getAttribute("teaser-bid") == null;
    }
    
    /***
     * <h1>isBidFederalBid</h1>
     * <p>purpose: Is this bid a federal bid?</p>
     * @return true if federal bid | false if not federal bid
     */
    private Boolean isBidFederalBid() {
    	return this.getThisRow().getAttribute("agency-type").equals("Federal");
    }

    /***
     * <h1>isBidMilitaryBid</h1>
     * <p>purpose: Is this bid a military bid?</p>
     * @return true if military bid | false if not military bid
     */
    private Boolean isBidMilitaryBid() {
    	return this.getThisRow().getAttribute("agency-type").equals("Military");
    }
    
    /***
     * <h1>isBidCanadianBid</h1>
     * <p>purpose: Is this bid a Canadian bid?</p>
     * @return true if Canadian bid | false if not Canadian bid
     */
    private Boolean isBidCanadianBid() {
    	return SubscriptionAddOns.CANADA.getCanadaProvinces().contains(this.bidInfo.getState());
    }

    /* ----- methods ----- */

    /***
     * <h1>clickStar</h1>
     * <p>purpose: Clicks the bid's star, favoriting or unfavoriting it.</p>
     * @return void
     */
    public void clickStar() {
    	findByScrollIntoViewBottomOfScreen(By.xpath(row_xpath));

    	// Get star's current state: selected | deselected
    	Boolean currentState = this.isFavorited();
    	// Now click star
    	this.findByVisibility(By.xpath(row_xpath + starButton_xpath)).click();
        
    	// dmidura: Workaround for BIDSYNC-241: "QA Environment: Weird selection lag when starring bids"
    	// Entire if/else statement should be removed after ticket is complete.
    	// Wait for the current state to update to the new state
    	String xpath = row_xpath + starIcon_xpath;
    	if (currentState) {
    		// star was selected, waiting for it to deselect
    		// a deselected star on the "New For You" list will be removed from the list, not show deselected,
    		// so many sure that the star is even available before waiting for it to deselect
    		if (this.doesPageObjectExist(driver, By.xpath(xpath))) {
    			new WebDriverWait(driver, DEFAULT_TIMEOUT)
    			.withMessage("Error: After deselecting selected star, value never changed to deselected")
    			.until(ExpectedConditions.textToBe(By.xpath(xpath), "star_border"));
    		}
    	} else {
    		// star was deselected, waiting for it to select
    		new WebDriverWait(driver, DEFAULT_TIMEOUT)
    			.withMessage("Error: After selecting deselected star, value never changed to selected")
    			.until(ExpectedConditions.textToBe(By.xpath(xpath), "star"));
    	}
    }

//#Fix for release OCT 7
    /***
     * <h1>clickStar</h1>
     * <p>purpose: Clicks the bid's star, favoriting or unfavoriting it.</p>
     * @return void
     */
    public void clickStarTemp() {
        findByScrollIntoViewBottomOfScreen(By.xpath(row_xpath));

        // Get star's current state: selected | deselected
        Boolean currentState = this.isFavorited();
        // Now click star
        this.findByVisibility(By.xpath(row_xpath + starButton_xpath)).click();
        new HelperMethods().addSystemWait(3);
    }

    /***
     * <h1>isFavorited</h1>
     * <p>purpose: Returns true if the bid's star is filled in, false otherwise</p>
     * @return boolean
     */
    public boolean isFavorited() {
        WebElement element = findByVisibility(By.xpath(row_xpath + starIcon_xpath));
        return element.getText().equals("star");
    }
    
    /***
     * <h1>isUpgradePlan</h1>
     * <p>purpose: When user should upgrade plan, agency displays as "UPGRADE PLAN",<br>
     * 	bid title includes "...", bid id is blank</p>
     * @return true if meets upgrade plan criteria | false if doesn't
     */
    public boolean isUpgradePlan() {
    	if (bidInfo.getBidTitle().length() > 25 ) {
    		// title needs to be long enough to have a "..."
    		return (bidInfo.getBidTitle().contains("...") & bidInfo.getAgency().equals(strUpgradePlanText) & bidInfo.getBidNumber().isEmpty());
    	} else {
    		return (bidInfo.getAgency().equals(strUpgradePlanText) & bidInfo.getBidNumber().isEmpty());}
    }
    
   
    /***
     * <h1>getRelevancyPercentage</h1>
     * <p>purpose: Return the relevancy percentage (should be and integer [0,100])
     * @return Integer
     */
    public Integer getRelevancyPercentage() {
        Matcher matcher = Pattern.compile("\\d+").matcher(this.relevancyBarStyle);
        if(!matcher.find()) {
            System.out.println(String.format("WARN: No integer found in relevancy style value '%s'", this.relevancyBarStyle));
        }
        return Integer.parseInt(matcher.group());
    }
    
    /***
     * <h1>columnsMatch</h1>
     * <p>purpose: Returns true if this bid and the given bid have the same value for the given column.</p>
     * @param bid - bid to compare to.
     * @param column - column to compare.
     * @return Calendar
     */
    public boolean columnsMatch(BidListRow bid, String column) {
        switch (column) {
            case "Bid Title":
                return bidInfo.getBidTitle().equals(bid.bidInfo.getBidTitle());
            case "Bid ID":
                return bidInfo.getBidNumber().equals(bid.bidInfo.getBidNumber());
            case "Agency":
                return bidInfo.getAgency().equals(bid.bidInfo.getAgency());
            case "State":
                return bidInfo.getState().equals(bid.bidInfo.getState());
            case "End Date":
                return bidInfo.getEndDateAsCalendar().equals(bid.bidInfo.getEndDateAsCalendar());
            case "Added":
                return bidInfo.getAddedDateInHours().equals(bid.bidInfo.getAddedDateInHours());
            case "Match":
                return this.getRelevancyPercentage().equals(bid.getRelevancyPercentage());
            default:
                Assert.fail(String.format("Unknown column name '%s'.\n", column));
                return false;
        }
    }

    /***
     * <h1>viewDetails</h1>
     * <p>purpose: Click on the bid title to view the bid's Bid Details screen<br>
     * 	Note: If bid displays "UPGRADE PLAN" in the Agency field, then ARGO will<br>
     * 	navigate to an upgrade subscriptions page instead of the Bid Details</p>
     * @return BidDetails
     */
    public BidDetails viewDetails() {
        findByScrollIntoViewBottomOfScreen(By.xpath(this.row_xpath + this.bidTitleBtn_xpath));
        this.getBidTitleBtn().click();
        return new BidDetails(driver);
    }
    
    /***
     * <h1>isBidWithinMatchScoreBounds</h1>
     * <p>purpose: Verify match score is within desired range<br>
     * 	Return false if not within range</p>
     * 	@param lowerBounds integer (between 0-100)
     * 	@param upperBounds integer (between 0-100)
     *  <p>lowerBounds must be less than upperBounds<br>
     *  range is exclusive</p>
     *
     * @return Boolean
     */
    public Boolean isBidWithinMatchScoreBounds(int lowerBounds, int upperBounds) {
        Float percentage = Float.parseFloat(this.getScoreElement().getAttribute("percentage"));

        //MAKE SURE VALUES ARE SANE
        Assert.assertTrue("Match Indicator value lower value less than higher", lowerBounds < upperBounds);
        Assert.assertTrue("Match indicator range is not equal", lowerBounds != upperBounds);
        Assert.assertTrue("Match Indicator Range Values within Range", lowerBounds >= 0 && lowerBounds <=100 && upperBounds >= 0 && upperBounds <= 100);

        return percentage  <= upperBounds && percentage >= lowerBounds;
    }

    /***
     * <h1>verifyMatchScoreColor</h1>
     * <p>purpose: Verify correct color of indicator</p>
     * @return BidListRow
     * @param color String RGBA color only
     * fails if colors are not equal
     */
    public BidListRow verifyMatchScoreColor(String color) {
        String bgColor = this.getRelevancyBarElement().getCssValue("backgroundColor");
        Assert.assertEquals("Colors not equal in match indicator", color, bgColor);
        System.out.println(GlobalVariables.getTestID() + " INFO: Verified color is " + color + " for bid " + bidInfo.getBidTitle() + "bid");

        return this;
    }

    /***
     * <h1>isBidWithinMatchScoreBounds</h1>
     * <p>purpose: Verify correct text of indicator</p>
     * @param text String of expected text for range
     * @return BidListRow
     */
    public BidListRow verifyMatchScoreText(String text) {
        String relevancyText = this.getMatchContainerElement().getAttribute("innerText");
    	Assert.assertEquals("Text not correct on match score", text.toLowerCase(), relevancyText.toLowerCase());
        System.out.println(GlobalVariables.getTestID() + " INFO: Verified text is " + text + " for bid " + bidInfo.getBidTitle() + " bid");
        return this;
    }

    /***
     * <h1>verifyNoMatchScoreIndicator</h1>
     * <p>purpose: Verify there is no indicator on bid<br>
     * 	fails if there is a match indicator</p>
     * @return BidListRow
     */
    public BidListRow verifyNoMatchScoreIndicator() {
    	waitForInvisibility(By.xpath(this.matchContainer_xpath), 1);
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified no match score for \"" + bidInfo.getBidTitle() + "\" bid");
    	return this;
    }
    
    /* ----- verifications ----- */
    /***
     * <h1>verifyIsUpgradePlan</h1>
     * <p>purpose: Verify that the current bid row is in "Upgrade Plan" mode<br>
     * 	Fail test if bid is not in "Upgrade Plan" mode</p>
     * @return BidList
     */
    public BidList verifyIsUpgradePlan() {
    	if (!this.isUpgradePlan()) {
    		fail("Test Failed: Bid \"" + bidInfo.getBidTitle() + "\" is not in \"Upgrade Plan\" mode");}
    	return new BidList(driver);
    }
    
    /***
     * <h1>verifyBidAccessIsCorrect</h1>
     * <p>purpose: Verify that the current bid row is visible per the user's subscription and addOns<br>
     * 	(ex: If the user has a National Plan, then they should be able to view bids from each state)<br>
     * 	Fail test if bid does not meet subscription plan or addOns</p>
     * @param locationsUserBelongsTo
     * 	This is a list of all the places that our user can receive bids from. It includes<br>
     * 	states, provinces, military, federal. States and provinces should be abbreviated<br>
     * 	ex: "TX", "BC", etc<br>
     * 	Federal and military bids should be indicated by including the words "federal" and "military"</p>
     * <p>dmidura: Per 12.10.2018 discussion with jbarlow, during registration,  Federal and Military addOn
     *  packages are correlated to the user's subscription plan (i.e. Federal addOn to National sees all states,
     *  but Federal addOn to KS State plan will only see the KS Federal bids). Canada addOn is for all
     *  Canadian provinces. This behavior may change in the future, in which case this method should be updated.</p>
     * @return BidList
     */ 
    public BidList verifyBidAccessIsCorrect(List <String> locationsUserBelongsTo) {
    	String bidLocation = bidInfo.getState();
    	String bidTitle = bidInfo.getBidTitle();
    	
    	// Get the list of states/provinces and addOns that our user can receive bids from
    	List <String> locationsOurUserShouldReceiveBidsFrom = new ArrayList<>();
    	locationsOurUserShouldReceiveBidsFrom.addAll(locationsUserBelongsTo);
    	
    	// Output message
     	String output;
    	if      (isBidCanadianBid()) { output = "Canada   addOn for province \"" + bidLocation + "\""; }
    	else if (isBidFederalBid())  { output = "Federal  addOn to  state    \"" + bidLocation + "\""; } 
    	else if (isBidMilitaryBid()) { output = "Military addOn to  state    \"" + bidLocation + "\""; }
    	else                         { output = "Subscription   to  state    \"" + bidLocation + "\""; }
   	
    	// Should the user have access to this bid?
    	if (this.shouldBidBeAccessible()) {
    		// If the bid should be accessible to the user, then verify that: 
    		// 1. the bid is not in UPGRADE PLAN mode
    		// 2. if the bid is an addOn, then the user is subscribed to that addOn package 
    		// Note: Testing if a bid's state is in the user's subscription is meaningless here, b/c a Basic bid can be from any state
    		//       and will always be accessible to the user...
    		Assert.assertTrue("User sees \"UPGRADE PLAN\" even though bid = \"" + bidTitle + "\" should be visible to user", !this.isUpgradePlan());
    		Assert.assertFalse("User has access to Military bid = \"" + bidTitle + "\", even though user does not have a Military addOn", isBidMilitaryBid() && !(locationsOurUserShouldReceiveBidsFrom.contains("military")) && !(locationsOurUserShouldReceiveBidsFrom.contains(bidLocation)) );
    		Assert.assertFalse("User has access to Federal bid = \""  + bidTitle + "\", even though user does not have a Federal addOn",  isBidFederalBid()  && !(locationsOurUserShouldReceiveBidsFrom.contains("federal"))  && !(locationsOurUserShouldReceiveBidsFrom.contains(bidLocation)) );
    		Assert.assertFalse("User has access to Canadian bid = \"" + bidTitle + "\", even though user does not have a Canada addOn",       isBidCanadianBid() && !(locationsOurUserShouldReceiveBidsFrom.contains(bidLocation)) );
    		
    		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that with    " + output + " user has access to bid \"" + bidTitle + "\"");
    	} else {
    		// If the bid should not be accessible to the user, then verify that: 
    		// 1. the bid is in UPGRADE PLAN mode
    		// 2. the bid is not included in an addOn package that the user is subscribed to
    	    // 3. the bid state/province is not a state/province that the user is subscribed to
    		Assert.assertTrue("User does not see \"UPGRADE PLAN\" even though bid = \"" + bidTitle + "\" should not be visible to user", this.isUpgradePlan());
    		Assert.assertFalse("User cannot access bid = \"" + bidTitle + "\", even though bid is a Military bid and user has Military addOn.", isBidMilitaryBid() && locationsOurUserShouldReceiveBidsFrom.contains("military") && locationsOurUserShouldReceiveBidsFrom.contains(bidLocation) );
    		Assert.assertFalse("User cannot access bid = \"" + bidTitle + "\", even though bid is a Federal bid and user has Federal addOn.",   isBidFederalBid()  && locationsOurUserShouldReceiveBidsFrom.contains("federal")  && locationsOurUserShouldReceiveBidsFrom.contains(bidLocation) );
    		Assert.assertFalse("User cannot access bid = \"" + bidTitle + "\", even though bid is a Canadian bid and user has Canada addOn",    isBidCanadianBid() && locationsOurUserShouldReceiveBidsFrom.contains(bidLocation) );
    		Assert.assertFalse("User cannot access bid = \""  + bidTitle + "\", even though \"" + bidLocation + "\" is included in user's subscription.", (isBidFederalBid() | isBidMilitaryBid()) && locationsOurUserShouldReceiveBidsFrom.contains(bidLocation));

    		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that without " + output + " user cannot access bid \"" + bidTitle + "\"");
    	}
    	
    	return new BidList(driver);
    }
}
