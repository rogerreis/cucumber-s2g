package pages.common.helpers;

import org.junit.Assert;

import data.bidSyncPro.registration.RandomChargebeeUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * <h1>Class GlobalVariables</h1>
 * <p>details: This class houses methods to store and extract variables used globally by tests. <br>
 * 	NOTE 1: Globals should be used sparingly, since they involve carrying overhead <br>
 * 	NOTE 2: Only set/use global variables in the stepDefinitions. Do not set/use global variables
 * 	in the page level classes (it makes it very difficult to keep track of all the globals set in each test, otherwise).
 */
public class GlobalVariables {

	private static Map<String, String> map = new HashMap<>();
	private static Map<String, Object> objectMap = new HashMap<>();

	// IMPORTANT NOTE:
	// mandrews:  since we're using docker containers with a single thread each 
	// we don't need to worry about thread safety and parallel tests stomping each others variables  
	
	// This method adds or updates existing global variable
	// Usage example:
	// GlobalVariables.addGlobalVariable("myVariable", "hello");
	public static void addGlobalVariable(String variableName, String variableValue) {
		if(map.containsKey(variableName))
			map.replace(variableName, variableValue);
		else
			map.put(variableName, variableValue);
	}
	
	// This method retrieves existing global variable
	// Usage example:
	// System.out.printf(GlobalVariables.getTestID() + " INFO: Global map variable = %s\n", GlobalVariables.getGlobalVariable("myVariable"));
	public static String getGlobalVariable(String variableName) {
		return map.get(variableName);
	}
	
    /***
     * <h1>storeObject</h1>
     * <p>purpose: Store an object into the GlobalVariables for later retrieval</p>
     * <p>NOTE 1: Globals should be used sparingly, since they involve carrying overhead <br>
     * NOTE 2: Only set/use global variables in the stepDefinitions. Do not set/use global variables
     * in the page level classes (it makes it very difficult to keep track of all the globals set in each test, otherwise).
     * @param objectName = String key to store object as (will also be used to retrieve object)
     * @param objectToStore = any type of object you want to store in GlobalVariables
     */
    public static void storeObject(String objectName, Object objectToStore) {
	    objectMap.put(objectName, objectToStore);
    }
    
    /***
     * <h1>getStoredObject</h1>
     * <p>purpose: Retrieve an object stored in the GlobalVariables<br>
     * Fail test if object is not in GlobalVariables<br>
     * (use containsObject() method if you are trying to determine if a global is in GlobalVariables) 
     * @param objectName = String key to search for (should match key set in storeObject() method)
     * @return the global associated with objectName key
     */
    @SuppressWarnings("unchecked")
	public static <T> T getStoredObject(String objectName) {
	    if(!containsObject(objectName)) {
            Assert.fail(String.format("No object stored under name '%s'", objectName));
        }
        //noinspection unchecked -- there's no avoiding an unchecked cast here
        return (T) objectMap.get(objectName);
    }
    
    /***
     * <h1>containsObject</h1>
     * <p>purpose: Determine if an object is currently housed in the GlobalVariables
     * @param objectName = String key for locating object (should match key set in storeObject() method)
     * @return true if object in GlobalVariables | false if not
     */
    public static boolean containsObject(String objectName) {
        return objectMap.containsKey(objectName);
    }
    
    /***
     * <h1>getUsers</h1>
     * <p>purpose: Search globals for stored users. Return the emails of these users<br>
     * 	Note: This method is used mostly in the Hooks after a test completes to print the users registered by that test
     * @return List <String> is a list of each user email used by the test
     */
	public static List <String> getUsers() {
    	List <String> testUsers = new ArrayList<String>();
    	// null check in case no users were created
    	if (objectMap.isEmpty()) {
    		testUsers.add("none");
    	} else {
    		objectMap.values().forEach(object->{
    			if (object.getClass().equals(RandomChargebeeUser.class)) {
    				testUsers.add(((RandomChargebeeUser) object).getEmailAddress());
    				}});
    	}
    	return testUsers;
    }
	
	/***
     * <h1>getTestID</h1>
     * <p>purpose: Write testID out to logs if running in grid.  Otherwise not.<br>
     * @return String testID
     */
	public static String getTestID() {
    	String strTestID = "";
    	
    	if (System.getProperty("env").equals("grid")) {
    		strTestID = getGlobalVariable("testID");
    	}
    	
    	return strTestID;
    }
}
