package data.bidSyncPro.registration;

/**
 * <h1>enum: ReasonForRegistrationEnum</h1>
 * @author ssomaraj
 *<p>details: This enum state the reason the supplier is registration to bidsync
 */
public enum ReasonForRegistrationEnum {
	ONE_SPECIFIC_SOLICITATION,
	GOVERNMENT_AGENCY,
	NEW_TO_GOVERNMENT_SELLING,
	GOVERNMENT_SELLING_IS_STRATEGIC_TO_MY_BUSINESS_GROWTH;
}
