package pages.bidSyncPro.supplier.dashboard.savedSearches;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class ConfirmDeleteSearchPopup</h1>
 * @author dmidura
 * <p>details: This class houses the objects and methods for the "Confirm Delete Search" popup that appears<br>
 * 	after a user has clicked on an "x" to delete a saved search from the Saved Search dropdown.<br>
 * 	(The Saved Searches dropdown appears on the dashboard (top left))</p>
 */
public class ConfirmDeleteSearchPopup extends BasePageActions {

	// Elements of the popup
	private String searchName ="";
	
	// xpaths
	private final String rootXPath 							= "//mat-card[@id='matUpdateUserProfileCard']";
	private final String confirmDeleteSearchTitle_xpath 	= rootXPath + "//mat-card-title[contains(text(),'Confirm Delete Search')]";
	private final String areYouSureYouWantToDeleteTxt_xpath = rootXPath +  "//mat-card-content[@id='confirmDeleteDialogBody']" +
			"[contains(p,'Area you sure you want to delete saved search ')]";
	private final String searchNameTxt_xpath 				= "//mat-card-content[@id='confirmDeleteDialogBody']" +
			"contains(strong,'" + this.searchName + "')]";
	private final String cancelBtn_xpath 					= rootXPath + "//button[@id='cancelDeleteSearchButton'][contains(span, 'CANCEL')]";
	private final String confirmBtn_xpath 					= rootXPath + "//button[@id='confirmDeleteSearchButton'][contains(span, 'CONFIRM')]";
	
	// Other
	private EventFiringWebDriver driver;

	/***
	 * <h1>ConfirmDeleteSearchPopup</h1>
	 * @param driver
	 * @param strSavedSearchName = name of the search that you are trying to delete<br>
	 * 	from the Saved Searches dropdown</p>
	 */
	public ConfirmDeleteSearchPopup(EventFiringWebDriver driver, String strSavedSearchName) {
		super(driver);
		this.driver = driver;
		this.setSearchName(strSavedSearchName);
	}
	
	/* ----- getters ----- */
	private WebElement getConfirmDeleteSearchTitle()     { return findByVisibility(By.xpath(this.confirmDeleteSearchTitle_xpath));     }
	private WebElement getAreYouSureYouWantToDeleteTxt() { return findByVisibility(By.xpath(this.areYouSureYouWantToDeleteTxt_xpath)); }
	private WebElement getSearchNameTxt()                { return findByVisibility(By.xpath(this.searchNameTxt_xpath));                }
	private WebElement getCancelBtn()                    { return findByVisibility(By.xpath(this.cancelBtn_xpath));                    }
	private WebElement getConfirmBtn()                   { return findByVisibility(By.xpath(this.confirmBtn_xpath));                   }
	
	/* ----- setters ----- */
	private void setSearchName(String strSavedSearchName) { this.searchName = strSavedSearchName;}
	
	/* ----- methods ----- */
	/***
	 * <h1>clickCancelBtn</h1> 
	 * <p>purpose: Click on the "CANCEL" button on the "Confirm Delete Search" popup.<br>
	 * 	("Confirm Delete Search" popup appears after user clicks on "X" button on a <br>
	 * 	saved search in the Saved Searches dropdown</p>
	 * @return DashboardCommon
	 */
	public DashboardCommon clickCancelBtn() {
		this.getCancelBtn().click();
		this.waitForConfirmationToHide();
		return new DashboardCommon(driver);
	}

	/***
	 * <h1>clickConfirmBtn</h1> 
	 * <p>purpose: Click on the "CANCEL" button on the "Confirm Delete Search" popup.<br>
	 * 	("Confirm Delete Search" popup appears after user clicks on "X" button on a <br>
	 * 	saved search in the Saved Searches dropdown</p>
	 * @return DashboardCommon
	 */
	public DashboardCommon clickConfirmBtn() {
		this.getConfirmBtn().click();
		this.waitForConfirmationToHide();
		return new DashboardCommon(driver);
	}
	
	/***
	 * <h1>waitforConfirmationToHide</h1>
	 * <p>purpose: Wait for the confirmation popup to dismiss before progressing</p>
	 * @return ConfirmatDeleteSearchPopup
	 */
	private ConfirmDeleteSearchPopup waitForConfirmationToHide() {
		waitForInvisibility(By.xpath(searchNameTxt_xpath), 3);
		return this;
	}
	
	/* ----- Verifications ----- */
	/***
	 * <h1>verifyInitialization</h1> 
	 * <p>purpose: Verify the "Confirm Delete Search" popup has correctly initialized per:<br>
	 * 	1. All expected page objects</p>
	 * @return ConfirmDeleteSearchPopup
	 */
	public ConfirmDeleteSearchPopup verifyInitialization() {
		this.getConfirmDeleteSearchTitle();
		this.getAreYouSureYouWantToDeleteTxt();
		this.getSearchNameTxt();
		this.getCancelBtn();
		this.getConfirmBtn();
		return this;
	}
}
