package pages.bidSyncPro.supplier.companySettings.manageUsers;

import static org.junit.Assert.fail;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;

/**
 * <h1>Class ActiveRequestRow</h1>
 * <p>details: This class contains methods and page Objects to manipulate a single row in the
 *  "Active Requests" table on the Manage Users screen, located at Company Settings > Manage Users.
 */
public class ActiveRequestRow extends BasePageActions {
    
    private EventFiringWebDriver driver;
    
    private String firstName;
    private String lastName;
    private String city;
    private String state;
    private String email;
    
    // xpaths
    private final String firstName_xpath          = "//span[contains(@class, 'user-request-first-name')]";
    private final String lastName_xpath           = "//span[contains(@class, 'user-request-last-name')]";
    private final String city_xpath               = "//span[contains(@class, 'userRequestcity')]";
    private final String state_xpath              = "//span[contains(@class, 'userRequestState')]";
    private final String email_xpath              = "//span[contains(@aria-label, 'email')]";
    private final String acceptButton_xpath       = "//button[contains(span, 'Accept')]";
    private final String declineButton_xpath      = "//button[contains(., 'Decline')]";
    private final String modalCloseButton_xpath   = "//button/span[contains(text(), 'CLOSE')]";
    private final String modalConfirmButton_xpath = "//button/span[contains(text(), 'CONFIRM')]";
    
    ActiveRequestRow(EventFiringWebDriver driver, String thisRowsXpath) {
    	super(driver);
        this.driver = driver;
        waitForVisibility(By.xpath(this.firstName_xpath), Integer.parseInt(System.getProperty("defaultTimeOut")));
        this.firstName = driver.findElement(By.xpath(thisRowsXpath + this.firstName_xpath)).getText();
        this.lastName  = driver.findElement(By.xpath(thisRowsXpath + this.lastName_xpath)).getText();
        this.city      = driver.findElement(By.xpath(thisRowsXpath + this.city_xpath)).getText();
        this.state     = driver.findElement(By.xpath(thisRowsXpath + this.state_xpath)).getText();
        this.email     = driver.findElement(By.xpath(thisRowsXpath + this.email_xpath)).getText();
    }
    
    /* ----- getters ----- */
    
    private WebElement getAcceptButton()       { return findByPresence(By.xpath(this.acceptButton_xpath));       }
    private WebElement getDeclineButton()      { return findByPresence(By.xpath(this.declineButton_xpath));      }
    private WebElement getModalCloseButton()   { return findByPresence(By.xpath(this.modalCloseButton_xpath));   }
    private WebElement getModalConfirmButton() { return findByPresence(By.xpath(this.modalConfirmButton_xpath)); }
    
    public String getFirstName() { return this.firstName; }
    public String getLastName()  { return this.lastName;  }
    public String getCity()      { return this.city;      }
    public String getState()     { return this.state;     }
    public String getEmail()     { return this.email;     }
    
    /* ----- methods ----- */
    
    /***
     * <h1>acceptRequest</h1>
     * <p>purpose: Click on the "Accept" button and then click "Close"<br>
     * 	on the subsequent confirmation popup. Wait for modal to close<br>
     * 	Fail test if confirmation popup doesn't appear</p>
     */
    public ManageUsers acceptRequest() {
        getAcceptButton().click();
        waitForVisibility(By.xpath(this.modalCloseButton_xpath), 10);
        getModalCloseButton().click();
        waitForInvisibility(By.xpath(this.modalCloseButton_xpath));
        return new ManageUsers(driver);
    }
    
    /***
     * <h1>declineRequest</h1>
     * <p>purpose: Click on the "Decline" button and then "Confirm"<br>
     * 	the decline on the subsequent confirmation popup. Finally, click
     *  "Close" on the notification popup. Wait for modal to close<br>
     * 	Fail test if confirmation popup or notification popup don't appear</p>
     */
    public ManageUsers declineRequest() {
        getDeclineButton().click();
        waitForVisibility(By.xpath(this.modalConfirmButton_xpath), 10);
        getModalConfirmButton().click();
        waitForVisibility(By.xpath(this.modalCloseButton_xpath), 10);
        getModalCloseButton().click();

        waitForInvisibility(By.xpath(this.modalCloseButton_xpath), 2);
        return new ManageUsers(driver);
    }
    
    /***
     * <h1>clickAcceptRequest</h1>
     * <p>purpose: Click on the "Accept" button to accept a<br>
     * 	user request. Different than acceptRequest() method<br>
     *  in that this method only clicks "Accept" and does not<br>
     *  handle the subsequent confirmation popup</p>
     * @return ActiveRequestRow 
     */
	public ActiveRequestRow clickAcceptRequest() {
		getAcceptButton().click();
		return this;
	}
	
    /***
     * <h1>confirmRequestFromPopup</h1>
     * <p>purpose: Click on the "Close" button on the accept<br>
     *  user confirmation popup that appears after clicking "Accept" for a user request. <br>
     *  Fail test if popup doesn't appear.</p>
     * @return ActiveRequestRow 
     */
	public ActiveRequestRow confirmRequestFromPopup() {
		this.getModalCloseButton().click();
		waitForInvisibility(By.xpath(this.modalCloseButton_xpath));
		return this;
	}
	
	/* ----- verifications ----- */
	
	/***
	 * <h1>verifyConfirmRequestPopupDoesNotAppear</h1>
	 * <p>purpose: Search the screen for 6 seconds for the <br>
	 * confirm request confirmation popup. Fail test if popup appears<br>
	 * during this time</p>
	 * @return ManageUsers 
	 */
	public ManageUsers verifyConfirmRequestPopupDoesNotAppear() {
		// Search for the confirmation popup for 6 seconds
		System.out.println(GlobalVariables.getTestID() + " INFO: Verifying that confirm request popup is not visible");

		List <WebElement> ele = this.findAllOrNoElements(driver, By.xpath(this.modalCloseButton_xpath));
		if (!ele.isEmpty()) { fail("Test Failed: Confirmation popup is visible");}
		
        return new ManageUsers(driver);
	}
	
	/***
	 * <h1>verifyAcceptButtonIsDisabled</h1>
	 * <p>purpose: Verify that the "Accept" button has disappeared<br>
	 * 	This method can be run after clicking on "Accept"<br>
	 *  Fail test if button is still visible 2 seconds after hitting this method</p>
	 */
	public ActiveRequestRow verifyAcceptButtonIsNotVisible() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Verifying that \"Accept\" button has disappeared");
		waitForInvisibility(By.xpath(this.acceptButton_xpath), 2);
		return this;
	}
	
}
