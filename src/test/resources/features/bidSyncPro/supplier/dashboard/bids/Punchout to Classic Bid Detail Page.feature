@must @bidSearch_detail @level2
Feature: Dashboard > Bid Detail - Punchout to Classic Bid Detail Page
  ARGO-607
  
  mcarvajal: there are some broken selectors in here and it needs a refactor
  
  Scenario: Punchout to Classic Bid Detail Page
    Given I navigate my browser to the BidSync classic login page
    And I enter my username as "jwoodcox" and password as "test1234"
    Then I will be on the agency home page
    Given I am logged in as an agency
    Then I will create a new bid
    #And I will wait for the bid to sync with Bidsync Pro
    #Given I have logged into the Supplier side as user "Basic"
    #And I have navigated to the "All Bids" bid tab
    #When I begin a search
    #Then I will see my bid in the results