package pages.bidSyncPro.dailyBidNotifications;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import data.bidSyncPro.VariableHelper;
import data.bidSyncPro.BidCard.SubscribedBidCard;
import data.bidSyncPro.BidCard.UnSubscribedBidCard;
import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class bidNotifications</h1>
 * 
 * @author dmidura
 *         <p>
 *         details: This class houses methods and page Objects to manipulate the
 *         daily bid notifications (email) that is sent directly to the
 *         registered user's inbox. Note: We are accessing this via Mailinator
 *         with the "text/plain" mode set
 */
public class dailyBidNotification extends BasePageActions {

	/* --------------- Configurable values ----------------------------- */
	/* ---------------------------------------------------------------- */
	// For maintenance purposes, update these values as needed:
	private final String expectedBidSyncImage = "https://034bb8c843edee0e2a60-5ff025f7b23adcfa6eee4cf26d636808.ssl.cf2.rackcdn.com/emails/notificaiotns/daily/june2017/bidsyncHorizLogo.png";

	/* ---------------------------------------------------------------- */
	/* ---------------------------------------------------------------- */

	// Message Body Elements
	private String emailGenerationDate; // Date of generation, extracted from "Generated on MM/DD/YYYY"

	private List<String> msgBody; // Holds the entire message body
	private List<String> subscribedMsgBody; // Holds the entire subscribed message body
	private List<String> unSubscribedMsgBody; // Holds the entire unSubscribed message body
	private List<bidNotificationRow> allSubscribedBids; // Holds all subscribed bids
	private List<bidNotificationRow> allUnsubscribedBids; // Holds all unsubscribed bids
	SubscribedBidCard SubscribedBidCard;
	UnSubscribedBidCard unSubscribedBidCard;

	// Other
	private EventFiringWebDriver driver;

	/***
	 * <h1>dailyBidNotification</h1>
	 * <p>
	 * purpose: Class constructor. Automatically extracts subscribed and
	 * non-subscribed bids and sets into allSubscribedBids and allUnSubscribedBids
	 * 
	 * @param driver
	 */
	// Fix for release OCT 7
	/*
	 * public dailyBidNotification(EventFiringWebDriver driver) { super(driver);
	 * this.driver = driver; allSubscribedBids = new
	 * ArrayList<bidNotificationRow>(); allUnsubscribedBids = new
	 * ArrayList<bidNotificationRow>();
	 * 
	 * // Extract the message body and then set all the bids this.msgBody =
	 * Arrays.asList(findByVisibility(By.xpath("//body")).getText().replaceAll(
	 * "\n{1,6}", "\n").trim().split("\n"));
	 * this.setBottomSectionNotificationElements(); }
	 */
	// Fix for release OCT 7
	/***
	 * <h1>dailyBidNotification</h1>
	 * <p>
	 * purpose: Class constructor. Automatically extracts subscribed and
	 * non-subscribed bids and sets into allSubscribedBids and allUnSubscribedBids
	 * 
	 * @param driver
	 */
	public dailyBidNotification(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
		Iterator<String> iterator;
		allSubscribedBids = new ArrayList<bidNotificationRow>();
		allUnsubscribedBids = new ArrayList<bidNotificationRow>();
		
		subscribedMsgBody = new ArrayList<String>();
		unSubscribedMsgBody = new ArrayList<String>();
		
		VariableHelper.subscribedBidCard_List = new ArrayList<SubscribedBidCard>();
		VariableHelper.unSubscribedBidCard_List = new ArrayList<UnSubscribedBidCard>();
		// Extract the message body and then set all the bids
		try {
			driver.switchTo().frame("msg_body");
		} catch (Exception ex) {
			System.out.println(GlobalVariables.getTestID() + " Setting driver class");
		}

		this.msgBody = Arrays.asList(driver.findElement(By.xpath("//body//table[3]")).getText().trim().split("\\n"));
		

		// subscribedBidCards
		int fromIndex = 0;
		int toIndex = 9;
		List<WebElement> SubscribedBidElements = driver.findElements(By.xpath(
				"//td[contains(text(),'Your Bid Notifications')]/../../../following-sibling::table//table[@class='devicewidthinner']/tbody/tr/td/table"));
		for (WebElement ele : SubscribedBidElements)
			this.subscribedMsgBody.addAll(Arrays.asList(ele.getText().trim().split("\\n")));

		if (!this.subscribedMsgBody.isEmpty()) {
			iterator = this.subscribedMsgBody.subList(fromIndex, toIndex).iterator();

			while (iterator.hasNext()) {
				SubscribedBidCard = new SubscribedBidCard();
				SubscribedBidCard.setBidTitle(iterator.next());
				SubscribedBidCard.setBidLocation(iterator.next());
				SubscribedBidCard.setDescription(iterator.next());
				SubscribedBidCard.setSpyGlassImage(iterator.next());
				SubscribedBidCard.setViewBidLink(iterator.next());
				SubscribedBidCard.setBidNumber(iterator.next());
				SubscribedBidCard.setDueDate(iterator.next());
				SubscribedBidCard.setNoOfDaysLeft(iterator.next());
				SubscribedBidCard.setDaysLeftText(iterator.next());
//			SubscribedBidCard.setMatchType(iterator.next());
				VariableHelper.subscribedBidCard_List.add(SubscribedBidCard);
				try {
					iterator = this.subscribedMsgBody.subList(fromIndex += 9, toIndex += 9).iterator();
				} catch (Exception e) {
					break;
				}
			}
		}
		
		// Un Subscribed Bid Cards
		fromIndex = 0;
		toIndex = 9;
		for (WebElement ele : driver.findElements(By.xpath(
				"//td[contains(text(),'Bids Available with Upgraded Subscription')]/../../..//table[@class='devicewidthinner']/tbody/tr/td/table")))
			this.unSubscribedMsgBody.addAll(Arrays.asList(ele.getText().trim().split("\\n")));
		if (!this.unSubscribedMsgBody.isEmpty()) {
			iterator = this.unSubscribedMsgBody.subList(fromIndex, toIndex).iterator();

			while (iterator.hasNext()) {
				unSubscribedBidCard = new UnSubscribedBidCard();
				unSubscribedBidCard.setBidTitle(iterator.next());
				unSubscribedBidCard.setBidLocation(iterator.next());
				unSubscribedBidCard.setBidDescription(iterator.next());
				unSubscribedBidCard.setViewBidLink(iterator.next());
				unSubscribedBidCard.setBidNumber(iterator.next());
				unSubscribedBidCard.setDueDate(iterator.next());
				unSubscribedBidCard.setNoOfDaysLeft(iterator.next());
				unSubscribedBidCard.setDaysLeftText(iterator.next());
				unSubscribedBidCard.setSubscribeButtonExists(iterator.next());
//					unSubscribedBidCard.setmatchType(iterator.next());

				VariableHelper.unSubscribedBidCard_List.add(unSubscribedBidCard);
				try {
					iterator = this.unSubscribedMsgBody.subList(fromIndex += 9, toIndex += 9).iterator();
				} catch (Exception e) {
					break;
				}
			}
		}

		this.setBottomSectionNotificationElements();
	}

	/* ----- getters ----- */
	private String getEmailGenerationDate() {
		return this.emailGenerationDate;
	}

	// Configurable Values
	private String getExpectedBidSyncProImage() {
		return this.expectedBidSyncImage;
	}

	/* ----- helpers ----- */

	/***
	 * <h1>setTopSectionNotificationElements</h1>
	 * <p>
	 * purpose: Set the top elements of the notification email TODO
	 */
	@SuppressWarnings("unused")
	private void setTopSectionNotificationElements() {
		// TODO
	}

	/***
	 * <h1>setMiddleSectionNotificationElements</h1>
	 * <p>
	 * purpose: Set the middle elements of the notification email. Does not include
	 * subscribed and unsubscribed bids TODO
	 */

	@SuppressWarnings("unused")
	private void setMiddleSectionNotificationElements() {
		// TODO
	}

	/***
	 * <h1>setBottomSectionNotificationElements</h1>
	 * <p>
	 * purpose: Set the bottom elements of the notification email
	 * <p>
	 * In Progress
	 */
	private void setBottomSectionNotificationElements() {
		/* ----- Example of expected bottom text ----- */
		// UPGRADE TO SEE MORE BIDS <https://app.bidsync.com/admin/manage-subscriptions>
		// To change which BidSync bids are sent to you,
		// please click the following link: Manage your keyword
		// preferences<https://app.bidsync.com/admin/bid-notifications?hook_ek=1>
		// To unsubscribe completely from BidSync bid notifications,
		// please click the following link: Unsubscribe
		// here<https://app.bidsync.com/admin/bid-notifications?hook_dn=1>
		// Please do not respond directly to this email.
		// ©2018 BidSync. All rights reserved.
		// Generated on 02/09/2019

		List<String> bottomElements = msgBody.subList(
				msgBody.indexOf("To change which BidSync bids are sent to you,"),
				msgBody.indexOf("Please do not respond directly to this email.") + 3);
		this.emailGenerationDate = bottomElements.get(bottomElements.size()-1).replaceFirst("^Generated on\\s?", "");
		// TODO fill in other elements
	}

	/***
	 * <h1>setSubscribedBids</h1>
	 * <p>
	 * purpose: Extract the subscribed bids from msgBody and set into
	 * allSubscribedBids
	 */
	// Fix for release OCT 7
	/*
	 * private void setSubscribedBids() { // Extract subscribed bids text List
	 * <String> allElements = msgBody
	 * .subList(msgBody.indexOf("Your Bid Notifications") + 1,
	 * msgBody.indexOf("Want to see more relevant bids?") - 2);
	 * 
	 * // Quick sanity check that our extracted text is formatted as expected //
	 * (bid notifications contain 12 pieces of information, first is match score,
	 * 2nd to last is "Days Left" text) int numEle = allElements.size();
	 * Assert.assertTrue("Test Failed: Subscribed Bids formatting has changed",
	 * (numEle%12 == 0 && allElements.get(numEle-2).contains("Days Left") &&
	 * allElements.get(0).matches("\\[(best|possible|unlikely)\\smatch\\]")));
	 * 
	 * // Put extracted text into bidNotificationRow objects for (int i=1; i <
	 * (allElements.size()/12) + 1; i++) { allSubscribedBids.add(new
	 * bidNotificationRow(driver, this.getEmailGenerationDate(),
	 * allElements.subList( (i-1)*12, i*12))); } }
	 */
	// Fix for release OCT 7
	/***
	 * <h1>setSubscribedBids</h1>
	 * <p>
	 * purpose: Extract the subscribed bids from msgBody and set into
	 * allSubscribedBids
	 */
	private void setSubscribedBids() {
		// Extract subscribed bids text
//		List<String> allElements = msgBody.subList(msgBody.indexOf("Your Bid Notifications") + 1,
//				msgBody.indexOf("Want to see more relevant bids?") - 1);
		// Quick sanity check that our extracted text is formatted as expected
		// (bid notifications contain 12 pieces of information, first is match score,
		// 2nd to last is "Days Left" text)
//		int numEle = allElements.size();
		
		
		for(SubscribedBidCard subscribedBidCard : VariableHelper.subscribedBidCard_List) {
			Assert.assertTrue("Test Failed: Subscribed Bids name is empty",!subscribedBidCard.getbidName().isEmpty());
			Assert.assertTrue("Test Failed: Subscribed Bids city is empty",!subscribedBidCard.getcity().isEmpty());
			Assert.assertTrue("Test Failed: Subscribed Bids description is empty",!subscribedBidCard.getdescription1().isEmpty());
			Assert.assertTrue("Test Failed: Subscribed Bids due date is empty",!subscribedBidCard.getdueDate().isEmpty());
			Assert.assertTrue("Test Failed: Subscribed Bids days left is empty",!subscribedBidCard.getnoOfDays().isEmpty());
			Assert.assertTrue("Test Failed: Subscribed Bids days left string is empty",!subscribedBidCard.getnoOfDaysLeft().isEmpty());
		}

		// Put extracted text into bidNotificationRow objects
		for (int i = 1; i <= VariableHelper.subscribedBidCard_List.size(); i++) {
			allSubscribedBids.add(new bidNotificationRow(driver, this.getEmailGenerationDate(),
					subscribedMsgBody.subList((i - 1) * 9, i * 9)));
		}
	}
	/***
	 * <h1>setUnsubscribedBids</h1>
	 * <p>
	 * purpose: Extract the subscribed bids from msgBody and set into
	 * allSubscribedBids
	 */
	private void setUnsubscribedBids() {
		// Extract subscribed bids text
//		List <String> allElements = msgBody
//				.subList(msgBody.indexOf("Bids Available with Upgraded Subscription")     + 1, 
//						 msgBody.indexOf("To change which BidSync bids are sent to you,") - 1);
		// msgBody.indexOf("To change which BidSync bids are sent to you,") - 2);

//		List<String> allElements = unSubscribedMsgBody;

		// Quick sanity check that our extracted text is formatted as expected
		// (bid notifications contain 11 pieces of information, first is match score,
		// 2nd to last is "Days Left" text)
//		int numEle = allElements.size();
//		Assert.assertTrue("Test Failed: Unsubscribed Bids formatting has changed",
//				(numEle % 9 == 0 && allElements.get(numEle - 2).contains("Days Left")
//						&& allElements.get(0).matches("\\[(best|possible|unlikely)\\smatch\\]")));

		for(UnSubscribedBidCard unSubscribedBidCard : VariableHelper.unSubscribedBidCard_List) {
			Assert.assertTrue("Test Failed: Subscribed Bids name is empty",!unSubscribedBidCard.getbidName().isEmpty());
			Assert.assertTrue("Test Failed: Subscribed Bids city is not empty",unSubscribedBidCard.getcity().isEmpty());
			Assert.assertTrue("Test Failed: Subscribed Bids description is not empty",unSubscribedBidCard.getdescription().isEmpty());
			Assert.assertTrue("Test Failed: Subscribed Bids due date is empty",!unSubscribedBidCard.getdueDate().isEmpty());
			Assert.assertTrue("Test Failed: Subscribed Bids days left is empty",!unSubscribedBidCard.getnoOfDays().isEmpty());
			Assert.assertTrue("Test Failed: Subscribed Bids days left string is empty",!unSubscribedBidCard.getnoOfDaysLeft().isEmpty());
			Assert.assertTrue("Test Failed: Subscribed Bids Subscribe button not present",!unSubscribedBidCard.getSubscribeButtonExists().isEmpty());
		}
		
		// Put extracted text into bidNotificationRow objects
		for (int i = 1; i <= VariableHelper.unSubscribedBidCard_List.size(); i++) {
			// In the actual daily notification, Bid Location and Bid Description share the
			// blurred link.
			// However, we want to store each with the blurred link in the
			// bidNotificationRow
			// so unsubscribed bids can be compared to subscribed bids
			// So we're manually setting a copy of the blurred link into
			// bidNotificationRow.bidDescription
			List<String> correctedList = new ArrayList<String>();
			correctedList.addAll(unSubscribedMsgBody.subList((i - 1) * 9, i * 9));
			correctedList.add(3, unSubscribedMsgBody.get((i - 1) * 9 + 2)); 
			correctedList.remove(9); // removes 'Subscribe' button text
			allUnsubscribedBids.add(new bidNotificationRow(driver, this.getEmailGenerationDate(), correctedList));
		}
	}
	/* ----- methods ----- */

	/***
	 * <h1>allSubscribedBids</h1>
	 * <p>
	 * purpose: Return all the subscribed bids my user is viewing in the daily bid
	 * notification email
	 * 
	 * @return List<bidNotificationRow>, where each entry is a subscribed bid
	 */
	public List<bidNotificationRow> allSubscribedBids() {
		this.setSubscribedBids();
		return this.allSubscribedBids;
	}

	/***
	 * <h1>allUnsubscribedBids</h1>
	 * <p>
	 * purpose: Return all the unsubscribed bids my user is viewing in the daily bid
	 * notification email
	 * 
	 * @return List<bidNotificationRow>, where each entry is an unsubscribed bid
	 *         (i.e. obfuscated bid)
	 */
	public List<bidNotificationRow> allUnsubscribedBids() {
		this.setUnsubscribedBids();
		return this.allUnsubscribedBids;
	}

	/* ----- verifications ----- */

	/***
	 * <h1>verifyAllNotificationImages</h1>
	 * <p>
	 * purpose: Directly navigate to the hosting location of each notification
	 * image. Verify that each of the bid notification images loads. Checked images
	 * are:<br>
	 * A. General Images<br>
	 * 1. BidSync Pro Image<br>
	 * B. Bid Row Images (only verify for one bid, since image paths are all the
	 * same)<br>
	 * 1. Clock Image<br>
	 * 2. Spy Glass Image<br>
	 * 3. Star Image<br>
	 * 4. All Blurred Text Images<br>
	 * 5. All Match Score Images<br>
	 * 
	 * @return bidNotificationRow
	 */
	public dailyBidNotification verifyAllNotificationImages() {
		bidNotificationRow genericBid = this.allSubscribedBids().stream().findFirst()
				.orElseThrow(NullPointerException::new);

		List<Object> links = new ArrayList<Object>();
		links.add(this.getExpectedBidSyncProImage());
		links.add(genericBid.getExpectedClockImage());
		links.add(genericBid.getExpectedSpyGlassImage());
		links.add(genericBid.getExpectedStarImage());
		links.addAll(Arrays.asList(genericBid.getExpectedBlurTextImage()));
		links.addAll(Arrays.asList(genericBid.getExpectedMatchScoreImage()));

		// Navigate to each img link and verify that an image tag is present
		SoftAssertions softly = new SoftAssertions();
		links.forEach(link -> {
			Boolean found = false;
			String errMsg = "";

			try {
				// Navigate directly to the image and see if it loads
				System.out.println(GlobalVariables.getTestID() + " INFO: Attempting to directly load image \"" + link + "\"");
				driver.get((String) link);
				found = new WebDriverWait(driver, 3)
						.until(ExpectedConditions.attributeToBe(By.tagName("img"), "src", (String) link));

				// We loaded the page, but there's no image
				errMsg = String.format(
						"Expected Bid Notification Image to be present after direct navigation to image link <%s>",
						link);
			} catch (TimeoutException t) {
				// We didn't actually load the page...
				errMsg = String.format(
						"Expected direct navigation to Bid Notification Image <%s>, but screen did not navigate", link);
			}

			softly.assertThat(found).withFailMessage(errMsg).isEqualTo(true);
		});

		softly.assertAll();
		return this;
	}
}
