package stepDefinitions.buyspeed;

import core.CoreAutomation;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import pages.bidSyncPro.supplier.dashboard.BidListTabNames;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterCriteria;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterResultsSectionNewClass;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.TimeFrameOptions;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.buyspeed.BuySpeedLogin;
import pages.buyspeed.agency.documents.bids.BidSummaryTab;
import pages.buyspeed.agency.documents.common.DocumentStatusPopup;
import pages.common.helpers.DateHelpers;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.support.events.EventFiringWebDriver;

/***
 * <h1>Class CancelBuySpeedBids_stepDefinitions</h1>
 * @author dmidura
 * <p>details: This class houses test steps to test that canceling BuySpeed bids correctly updates ARGO</p>
 */
public class CancelBuySpeedBids_stepDefinitions {
	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;
	private DateHelpers   dateHelpers;

	public CancelBuySpeedBids_stepDefinitions(CoreAutomation automation) throws Throwable {
		this.driver     = automation.getDriver();
		
		dateHelpers     = new DateHelpers();
		helperMethods   = new HelperMethods();
	}

	/* ------------ Test Steps ---------------- */
	
	@Given ("^I have canceled a newly cloned bid in BuySpeed$")
	public void I_have_canceled_a_newly_cloned_bid_in_BuySpeed() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Cloning Buyspeed test bid, sending, and then canceling cloned bid");

		// 1. Login as ARGO BP
		// 2. Locate our canned bid (page will automatically navigate to bid's Summary tab)
		// 3. Navigate to Summary tab
		// 4. Fill in Bid End Date, Bid Open Date, and required custom columns
		// 5. Navigate to the Bidders tab
		new BuySpeedLogin(driver).loginAsAgencyUser("ARGO BP")
			.waitForDashboardToLoad()
			.getTopSearchBar()
			.searchForBid("ADSPO17-00005648")
			.clickCloneBidBtn()
			.openNewestClonedBid() 
			.clickOnBidGeneralTab() 
			.setBidAvailableDateToNow()
			.setBidOpeningDateToOneDayFromNow()
			.setCustomColumnsOnBidGeneralTab() 
			.clickSaveAndContinueBtn()
			.clickOnBidBiddersTab()
			.setUnrestrictedBid()
			.clickSaveAndContinueBtn()
			.clickOnBidSummaryTab()
			.automaticallyApprove()
			.clickSendBidBtn()
			.clickCancelBidBtn()
			.verifyBidCanceled();
		
		// Save out the bid number and canceled time
		String strBidNumber = new BidSummaryTab(driver).getBidNumberText();
		Calendar bidCanceledDateAndTime = new BidSummaryTab(driver).getBidCanceledDateAndTimeAsCalendar();
		System.out.println(GlobalVariables.getTestID() + " Test Variables:");
		System.out.println(GlobalVariables.getTestID() + " Bid Number: " + strBidNumber);
		System.out.println(GlobalVariables.getTestID() + " Cancellation Time: " + dateHelpers.getCalendarDateAsString(bidCanceledDateAndTime));

		// Close out errant popup
		new DocumentStatusPopup(driver).closeDocumentStatusPopup();

		// Save out globals for this test
		GlobalVariables.storeObject("strBidNumber", strBidNumber);
		GlobalVariables.storeObject("bidCanceledDateAndTime", bidCanceledDateAndTime);
		
		// And wait a sec for ARGO to grab this canceled bid
		helperMethods.addSystemWait(4);
	}
	
//	@Given ("^I set globals for this test$")
//	public void setThoseGlobals(){
//		Calendar bidDate = Calendar.getInstance();
//		GlobalVariables.addGlobalVariable("strBidNumber", "ADSPO17-00005685");
//		GlobalVariables.storeObject("bidCanceledDateAndTime", bidDate);
//	}


	@When ("^I locate the canceled BuySpeed bid in BidSync Pro$")
	public void I_locate_the_canceled_BuySpeed_bid_in_BidSync_Pro() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Logging into ARGO to locate the canceled BuySpeed bid");
		
		// 1. Log into ARGO
		// 2. Search for our canned canceled bid (bids canceled today will still display in All Bids)
		new SupplierLogin(driver).navigateToHere()
			.loginAsUser("UserForCanceledBuySpeedBids")
			.waitForPageLoad()
			.searchFor("ARGO");
	}
	
	@Then ("^I will verify that the Bid End Date of the canceled BuySpeed bid displays as the time of cancellation$")
	public void I_will_verify_that_the_Bid_End_Date_of_the_canceled_BuySpeed_bid_displays_as_the_time_of_cancellation() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Bid End Date of canceled BSO bid displays per the time of cancellation");

		String strBidNumber = GlobalVariables.getStoredObject("strBidNumber");
		Calendar bidCanceledDateAndTime = GlobalVariables.getStoredObject("bidCanceledDateAndTime");

		// 1. Get the canceled bid in the All Bids list
		// 2. Verify that the Bid End Date is today's date 
		new BidList(driver).getBidByBidNumber(strBidNumber)
				.bidInfo.verifyEndDateIsAsExpected(bidCanceledDateAndTime);

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Bid End Date for canceled BSO bid in bid list displays as today");
	}
	
	@And ("^I will verify that the Bid Details page correctly updates per the canceled BuySpeed bid$")
	public void I_will_verify_that_the_Bid_Details_page_correctly_updates_per_the_canceled_BuySpeed_bid() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Bid Details page correctly updates per canceled BuySpeed bid");
		
		String strBidNumber = GlobalVariables.getStoredObject("strBidNumber");
		Calendar bidCanceledDateAndTime = GlobalVariables.getStoredObject("bidCanceledDateAndTime");

		// 1. Click on the bid to view its Bid Details
		// 2. Verify that the Bid End Date is displaying per the cancellation time
		// 3. Verify that the Bid Title displays a message about cancellation
		// 4. Verify that the Bid is displaying as EXPIRED
		new BidList(driver).getBidByBidNumber(strBidNumber)
				.viewDetails()
				.verifyBidTitleDisplaysCancellationMessage()
				.verifyBidIsEXPIRED()
				.verifyEndDateIsAsExpected(bidCanceledDateAndTime);
		
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Bid Details page has correctly updated per canceled BuySpeed bid");
	}

	@When ("^I run a Filter Results search for canceled BuySpeed bids based on keyword with timeframe \"(.*)\"$")
	public void I_run_a_Filter_Results_search_for_canceled_BuySpeed_bids_based_on_keyword_with_timeframe(String timeFrame) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Running search for canceled BuySpeed bids");
		
		// Set a filter per our time frame.
		// The keyword set here matches the BuySpeed bid title that
		// has been set for all the canceled bids ("ARGO-2148")
		FilterCriteria filter = new FilterCriteria();
		filter.setFilterName("Canceled BuySpeed Bids");
		List <String> keywords = new ArrayList<String>();
		keywords.add("2148");
		filter.setKeywords(keywords);
		filter.setSelectTimeFrame(TimeFrameOptions.fromString(timeFrame));
		
		// Run the search
		FilterResultsSectionNewClass filterResults = new FilterResultsSectionNewClass(driver);
		filterResults.openFilterResultsSection()
				.clickClearAllBtn()
				.setFilter(filter)
				.clickSearchBtn();
		
		// Save out the filter
		GlobalVariables.storeObject("filter", filter);
	}

	@Then ("^I will verify that I see canceled BuySpeed bid results in the All Bids list per my search$")
	public void I_will_verify_that_I_see_canceled_BuySpeed_bid_results_in_the_All_Bids_list_per_my_search() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that canceled BuySpeed bid results appear in All Bids per search criteria");

		FilterCriteria filter = GlobalVariables.getStoredObject("filter");
		
		// 1. Verify that filter results match our filter criteria, and is within the selected time frame
		// (so bid end date can at most be today's date)
		// 2. Verify that we have more than one canceled bid (otherwise we're only seeing today's canceled bid)
		new FilterResultsSectionNewClass(driver).verifyFilterSearchResultsDisplayCorrectlyInAllBidsTab(filter);
		new BidList(driver).verifyMinimumNumberOfBidResults(2);

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Canceled BuySpeed bid results correctly appear in All Bids per search criteria");
	}

	@When ("I view bids in the New For You bid list$")
	public void I_view_bids_in_the_New_For_You_bid_list(){
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Viewing bids in the \"New For You\" bid list");
		
		// After user login, the "New For You" list should be automatically populated with bids
		// Just verify that we're actually on the "New For You" tab for this test step
		new FilterResultsSectionNewClass(driver).verifyTabIsHighlightedAndSelected(BidListTabNames.NEW_FOR_YOU);
	}
	
	
	@Then ("^I will not see any of the canceled BuySpeed bids, excepting BuySpeed bids canceled today in the New For You bid list$")
	public void I_will_not_see_any_of_the_canceled_bids_excepting_bids_canceled_today_in_the_New_For_You_bid_list() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that no BuySpeed bids that were canceled before today appear in \"New For You\" bid list");
		
		// Canceled BuySpeed bids all have the bid title "ARGO-2148" for this test.
		// Of these canceled BuySpeed test bids, the bid end date can only be today 
		// (for a bid that we just canceled). Otherwise, if an "ARGO-2148" bid is located
		// and it's bid end date is not today, something bad has happened.
		new BidList(driver).allVisibleBids().forEach(bid->{
			if (bid.bidInfo.getBidTitle().equals("ARGO-2148")) {
			bid.bidInfo.verifyEndDateIsAsExpected(Calendar.getInstance());
		}});
		
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: BuySpeed bids that were canceled before today do not appear in the \"New For You\" bid list");
	}
}