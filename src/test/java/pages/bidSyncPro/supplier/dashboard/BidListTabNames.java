package pages.bidSyncPro.supplier.dashboard;

import static org.junit.Assert.fail;

/***
 * <h1>enum: BidListTabNames</h1>
 * @author dmidura
 * <p>details: This enum defines the Bid Lists available on the dashboard.<br>
 * 	Note: These are also the options in the "Bid List" top bar menu<br>
 *  Bid Lists are: All Bids, New For You, Your Saved Bids</p>
 */
public enum BidListTabNames {
	ALL_BIDS,
	NEW_FOR_YOU,
	YOUR_SAVED_BIDS;
	
	/***
	 * <h1>fromString</h1>
	 * <p>purpose: Given text that matches the bid tabs,<br>
	 *  return the enum value of the BidListTabNames</p>
	 * @param bidTab<br>
	 * 	= "All Bids" | "New For You" | "Your Saved Bids"</p>
	 * @return BidListNames
	 */
	public static BidListTabNames fromString(String bidTab) {
		if (bidTab.equalsIgnoreCase("All Bids")) {
			return ALL_BIDS;
		} else if (bidTab.equalsIgnoreCase("New For You")) {
			return NEW_FOR_YOU;
		} else if (bidTab.equalsIgnoreCase("Your Saved Bids")) {
			return YOUR_SAVED_BIDS;
		} else {
			fail("ERROR: Bid tab \"" + bidTab + "\" is an invalid option to BidListTabNames");
			return null;
		}
	}
	
	@Override
	public String toString() {
		if (this.equals(BidListTabNames.ALL_BIDS)) {
			return "All Bids";
		} else if (this.equals(BidListTabNames.YOUR_SAVED_BIDS)) {
			return "Your Saved Bids";
		} else if (this.equals(BidListTabNames.NEW_FOR_YOU)) {
			return "New For You";
		} else {
			fail("ERROR: Bid List Name \"" + this + "\" is an invalid option to BidListTabNames");
			return null;
		}
	}
	

}
