package utility.encryption;

import org.apache.commons.codec.binary.Base64;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * This class is copied from com.bidsync.shared.utilities.utils.HashUtil for the purposes of testing bidsync passwords
 */
public final class HashUtil
{
    private static final String SSHA = "{SSHA}";
    
    public static final String KEY_STOPAUTH = "asdfasdffweraasdfasdfagweqrasd234234sdfsdfsdfs";
    
    public static final String SIGNATURE_SALT = "bidsync signature salt";
    
    public enum Type
    {
        Unknown,
        Clear,
        SSHA
    }
    
    public static String generateHash(String clearPwd, Type hashType)
    {
        if (clearPwd == null || clearPwd.length() <= 0) {
            return clearPwd;
        }
        
        switch (hashType) {
            case Unknown:
            case Clear:
            default:
                return clearPwd;
            case SSHA:
                return SSHA + generateSHAHash(clearPwd);
        }
    }
    
    public static Type getType(String hashVal)
    {
        if (hashVal == null || hashVal.length() < 1) {
            return Type.Unknown;
        }
        else if (hashVal.startsWith(SSHA)) {
            return Type.SSHA;
        }
        else {
            return Type.Clear;
        }
    }
    
    /*
     * Returns an SSHA digest for the given string.  The output is
     * the bae64 encoding of the hash and the salt.  The output always
     * is prefixed with {SSHA}
     */
    private static String generateSHAHash(String src)
    {
        String digest;
        
        try {
            byte[] buf = src.getBytes();
            byte[] salt = new byte[4];
            randomFill(salt);
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(buf);
            md.update(salt);
            buf = md.digest();
            
            byte[] hash = new byte[buf.length + salt.length];
            System.arraycopy(buf, 0, hash, 0, buf.length);
            System.arraycopy(salt, 0, hash, buf.length, salt.length);
            
            digest = Base64.encodeBase64String(hash);
        }
        catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Major Error:" + e.getMessage());
        }
        
        return digest;
    }
    
    public static String getDigest(String src)
    {
        String digest;
        try {
            byte[] buf = src.getBytes();
            MessageDigest md = MessageDigest.getInstance("SHA");
            md.update(buf, 0, buf.length);
            buf = md.digest();
            
            buf[0] = 0;   // avoid negative numbers
            BigInteger bi = new BigInteger(buf);
            digest = bi.toString(30);
        }
        catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Major Error:" + e.getMessage());
        }
        return digest;
    }
    
    /**
     * Returns true/false depending on whether the given hash matches the given
     * pwd
     */
    public static boolean isValidPasswordHash(String pwdHash, String clearPwd)
    {
        if (clearPwd == null || clearPwd.length() <= 0) {
            return false;
        }
        
        switch (getType(pwdHash)) {
            case SSHA:
                return isValidSHAHash(pwdHash.substring(SSHA.length()), clearPwd);
            case Clear:
                return pwdHash.equals(clearPwd);
            case Unknown:
            default:
                return false;
        }
    }
    
    
    /**
     * Checks the given hash against the password using SSHA.  This method assumes
     * that the given hash DOES NOT contain the string {SSHA}.  The salt follows
     * the first 20 bytes.
     */
    private static boolean isValidSHAHash(String pwdHash, String clearPwd)
    {
        if (clearPwd == null || pwdHash == null || clearPwd.length() <= 0 || pwdHash.length() <= 0) {
            return false;
        }
        
        byte[] hs;
        hs = Base64.decodeBase64(pwdHash.getBytes());
        
        byte[] hash = new byte[20];
        byte[] salt = new byte[hs.length - 20];
        System.arraycopy(hs, 0, hash, 0, 20);
        System.arraycopy(hs, 20, salt, 0, salt.length);
        
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-1");
        }
        catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Major Error:" + e.getMessage());
        }
        
        md.update(clearPwd.getBytes());
        md.update(salt);
        byte[] checkHash = md.digest();
        return md.isEqual(hash, checkHash);
    }
    
    private static void randomFill(byte[] byteArray)
    {
        Random rnd = new Random();
        rnd.nextBytes(byteArray);
    }
    
    public static String getSignatureHash(String signatureKey){
        return getDigest(signatureKey+SIGNATURE_SALT);
    }
}
