@must @administration @level2
Feature: Login - Login Page
  This feature file exercises the objects related to the login page

Scenario: successful login
    Given I navigate my browser to the supplier login page
    	And I enter my Email as "gwashburn@phimail.mailinator.com" and Password as "bidsync_1234"
    Then I will be on the supplier Home page

Scenario: Login attempt with a bad password.
	Given I navigate my browser to the supplier login page
		And I enter my Email as "gwashburn@phimail.mailinator.com" and Password as "Kirk1234"
	Then I will see a login error 

Scenario: Try for free
    Given I navigate my browser to the supplier login page
    	And I click Try for free
    Then I will be on the supplier registration page

Scenario: successful login with name to use for lockout test
    Given I navigate my browser to the supplier login page
    	And I enter my Email as "kgordon53@phimail.mailinator.com" and Password as "test_1234"
    Then I will be on the supplier Home page
        
Scenario Outline: Login attempt with bad passwords
    Given I navigate my browser to the supplier login page
    	And I enter my Email as "kgordon53@philmail.mailinator.com" and Password as "<password>"
    Then I will see a login error    	 
    
Examples:
	|password|
	|theboss1|
	|theboss2|
	|theboss3|
	|theboss4|
	|theboss5|	
	      
Scenario: Login attempt with bad password. Attempt 6. This should send the 20 min lockout messsage
    Given I navigate my browser to the supplier login page
    And I enter my Email as "kgordon53@phimail.mailinator.com" and Password as "theboss6"
    Then I will be locked out for 20 min
    
Scenario: Login as admin and clear the locked account and successfully login
	Given I navigate to the Admin login page
		And I enter my admin as "supplier@periscopeholdings.com" and Password as "test_1234"
	Then I will be on the admin Home page
	When I search for my locked user as "kgordon53@phimail.mailinator.com"
	Then I will clear the lock for my user
	Given I navigate my browser to the supplier login page
    	And I enter my Email as "kgordon53@philmail.mailinator.com" and Password as "test_1234"
    Then I will be on the supplier Home page
  
@ARGO-2107
Scenario: Login handles special characters
    Given I have registered a random Pro user with a password with every special character in it
    Then That user can log in successfully