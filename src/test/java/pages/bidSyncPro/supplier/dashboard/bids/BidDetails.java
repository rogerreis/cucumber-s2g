package pages.bidSyncPro.supplier.dashboard.bids;

import static org.junit.Assert.fail;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.common.helpers.DateHelpers;
import pages.common.helpers.GlobalVariables;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/***
 * <h1>Class BidDetails</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects related to the Bid Details page</p>
 */
@SuppressWarnings("unused")
public class BidDetails extends DashboardCommon {
	
	public BidDetails(EventFiringWebDriver driver) {
		super(driver);
		this.driver        = driver;
		this.dateHelpers   = new DateHelpers();
	}
   
	// Other 
	private final String badBidURL = System.getProperty("supplierBaseURL")  + "bid-detail/gekji";
	private EventFiringWebDriver driver;
	private DateHelpers   dateHelpers;
	
	
	// xpaths
	private final String backToBidsButton_xpath     = "//button[contains(span, 'Back to Bid Opportunities')]";

	// Cancelled Bid
	// Bid Title Section
	private final String cancellationText_xpath     = "//mat-card-title//h2[contains(text(),'This Bid has been canceled.')]";

	// Bad Bid
	private final String BidNotFoundTxt_xpath       = "//h2[@id='bidDetailBidNotFound'][contains(text(),'Bid not Found')]";
	
	// Generic Bid Details
	// Bid Title Section -- required section
	private final String bidTitleTxt_xpath 			= "//h2[@id='bidTitle']";
	private final String starBtn_xpath 				= "//button[@id='saveBidStar'][contains(span//mat-icon, 'star_border') or contains(span//mat-icon, 'star')]";
	private final String thumbsUpBtn_xpath			= "//button[@id='moreLikeThisTop'][contains(span/mat-icon, 'thumb_up')]";
	private final String thumbsDownBtn_xpath 		= "//button[@id='lessLikeThisTop'][contains(span/mat-icon, 'thumb_down')]";
	private final String bidNumberTxt_xpath 		= "//div[@id='bidNumber']";
	private final String bidNumberLink_xpath 		= "//span[@id='bidDetailFullURL']";
	private final String bidNumberLinkBSBid_xpath   = "//span[@id='bidDetailSourceBid'][contains(text(), 'Go to bid')]";
	private final String seeBidDetailsTopBtn_xpath 	= "//a[@id='seeBidDetailsButton'][contains(span/mat-icon, 'visibility')][contains(span/span, 'See Bid Details')]";
	
		
	// Bid Date Info Section -- required section
	private final String bidEndDateLabel_xpath          = "//span[@id='BidEndDateWrapper'][contains(text(), 'Bid End Date:')]";
	private final String bidEndDateTxt_xpath            = "//strong[@id='BidEndDate']";
	private final String addedLabel_xpath               = "//span[@id='BidAddedOuter'][contains(text(), 'Added:')]";
	private final String addedTxt_xpath                 = "//span[@id='bidAddedDate']";
	private final String postedLabel_xpath              = "//span[@id='BidPostedOuter'][contains(text(), 'Posted:')]";
	private final String postedTxt_xpath                = "//span[@id='bidPostedDate']";
	private final String daysRemainingLabel_xpath       = "//div[@id='DaysRemainingOuter'][contains(span/mat-icon, 'timelapse')][contains(span, 'Days Remaining:')]";
	private final String daysRemainingTxt_xpath         = "//span[@id='bidDaysRemaining']";
	private final String hoursRemainingLabel_xpath      = "//div[@id='HoursRemainingOuter'][contains(span/mat-icon, 'timelapse')][contains(span, 'Hours Remaining:')]";
	private final String hoursRemainingTxt_xpath        = "//span[@id='bidHoursRemaining']";
	private final String expiredLabel_xpath			 	= "//div[@id='bidErrorExpired'][contains(text(), 'EXPIRED')]";
		
	// Summary Section -- required section
	private final String summaryLabel_xpath             = "//div[@id='BidDetailSummarySection']//h3[@id='summaryHeader'][contains(text(), 'Summary:')]";
	private final String summaryTxt_xpath               = "//div[@id='BidDetailSummarySection']//p[@id='bidDescription']";
	private final String shareBtn_xpath                 = "//a[@id='bidDetailShareViaEmailLink'][contains(span/mat-icon, 'share')][contains(span/span, 'Share')]";
	
	// Left to Summary Section -- required section
	private final String agencyImage_xpath              = "//img[@id='bidDetailBidTypeImage']";
	private final String agencyIconImage_xpath          = "//img[@id='iconImage']";
	private final String agencyAddressTxt_xpath         = "//div[@id='bidAgency']";
	private final String agencyContactEmailLink_xpath   = "//a[@id='bidContactEmailLink']";
	private final String agencyContactURL_xpath         = "//a[@id='bidDetailAgencyUrlLink']//span[@id='bidDetailAgencyUrl']";
	private final String agencyTypeTxt_xpath            = "//div[@id='bidDetailAgencyType']";
		
	// Milestones Section -- optional section
	private final String milestonesLabel_xpath          = "//div[@id='milestones']//h3[contains(text(), 'Milestones:')]";
	private final String bidPublishedDateLabel_xpath    = "//div[@class='milestone-label'][contains(text(), 'Bid Published Date:')]";
	private final String bidPublishedDateTxt_xpath      = "//div[@id='bidDetailPublishedDate']";
		
	// Contacts Section -- optional section
	private final String contactsLabel_xpath            = "//h3[contains(text(), 'Contacts:')]";
	private final String contactNameTxt_xpath           = "//strong[@id='bidDetailContactPerson']";
	private final String contactAddressTxt_xpath        = "//div[@id='bidDetailContactAddress']";
	private final String contactPhoneTxt_xpath          = "//div[@id='bidDetailContactPhoneNumber']";
	private final String contactEmail_xpath             = "//a[@id='aContactEmail']";
		
	// Documents Section -- optional section
	private final String documentsLabel_xpath                = "//h3[contains(text(), 'Documents:')]";
	private final String downloadFirstDocumentIcon_xpath     = "//a/i[contains(@class, 'fa-file-o')][1]";
	private final String genericDocumentAttachmentLink_xpath = "//mat-list-item//a[contains(@id, 'bidDetailAttachment_')]";
		
	// Bottom Section --- required section
	private final String seeBidDetailsBottomBtn_xpath   = "//a[@id='bidDetailBottomSeeBidDetail'][contains(i[@class='fa-eye'],'')][contains(span, 'See Bid Details')]";
	private final String moreLikeThisBtn_xpath          = "//button[@id='bidDetailBottomMLTButton'][contains(i[@class='fa-thumbs-o-up'],'')][contains(span, 'More like this')]";
	private final String lessLikeThisBtn_xpath          = "//button[@id='bidDetailBottomLLTButton'][contains(i[@class='fa-thumbs-o-down'],'')][contains(span, 'Less like this')]";
	private final String thisBidIsLocatedLink_xpath     = "//div[contains(text(), 'This bid is located at')]/a[@id='bidDetailBaseURLLink']";

	/*------------- getters ----------------*/
	private WebElement getBackToBidsButton() { return findByVisibility(By.xpath(this.backToBidsButton_xpath)); }

	// Cancelled Bid
	private WebElement getCancellationTxt() { return findByPresence(By.xpath(this.cancellationText_xpath));    }

	// Bad Bid
	private WebElement getBidNotFoundTxt () { return findByPresence(By.xpath(this.BidNotFoundTxt_xpath));      }
 
	// Generic Bid Details
	// Bid Title Section -- required section
	private WebElement getBidTitleTxt()        { return findByPresence(By.xpath(this.bidTitleTxt_xpath));         }
	private WebElement getStarBtn()            { return findByPresence(By.xpath(this.starBtn_xpath));             }
	private WebElement getThumbsUpBtn()        { return findByPresence(By.xpath(this.thumbsUpBtn_xpath));         }
	private WebElement getThumbsDownBtn()      { return findByPresence(By.xpath(this.thumbsDownBtn_xpath));       }
	private WebElement getBidNumberTxt()       { return findByPresence(By.xpath(this.bidNumberTxt_xpath));        }
	private WebElement getBidNumberLink()      { return findByPresence(By.xpath(this.bidNumberLink_xpath));       }
	private WebElement getBidNumberBSBidLink() { return findByPresence(By.xpath(this.bidNumberLinkBSBid_xpath));  }
	private WebElement getSeeBidDetailsTopBtn(){ return findByPresence(By.xpath(this.seeBidDetailsTopBtn_xpath)); }
		
	// Bid Date Info Section -- required section
	private WebElement getBidEndDateLabel()    { return findByPresence(By.xpath(this.bidEndDateLabel_xpath));   }
	private WebElement getBidEndDateTxt()      { return findByPresence(By.xpath(this.bidEndDateTxt_xpath));     }
	private WebElement getAddedLabel()         { return findByPresence(By.xpath(this.addedLabel_xpath));        }
	private WebElement getAddedTxt()           { return findByPresence(By.xpath(this.addedTxt_xpath));          }
	private WebElement getPostedLabel()        { return findByPresence(By.xpath(this.postedLabel_xpath));       }
	private WebElement getPostedTxt()          { return findByPresence(By.xpath(this.postedTxt_xpath));         }
	private WebElement getDaysRemainingLabel() { return findByPresence(By.xpath(this.daysRemainingLabel_xpath));}
	private WebElement getDaysRemainingTxt()   { return findByPresence(By.xpath(this.daysRemainingTxt_xpath));  }
	private WebElement getExpiredLabel()       { return findByPresence(By.xpath(this.expiredLabel_xpath));      }
		
	// Summary Section -- required section
	private WebElement getSummaryLabel()       { return findByPresence(By.xpath(this.summaryLabel_xpath));      }
	private WebElement getSummaryTxt()         { return findByPresence(By.xpath(this.summaryTxt_xpath));        }
	private WebElement getShareBtn()           { return findByPresence(By.xpath(this.shareBtn_xpath));          }
	
	// Left to Summary Section -- required section
	private WebElement getAgencyImage()            { return findByPresence(By.xpath(this.agencyImage_xpath));           }
	private WebElement getAgencyIconImage()        { return findByPresence(By.xpath(this.agencyIconImage_xpath));       }
	private WebElement getAgencyAddressTxt()       { return findByPresence(By.xpath(this.agencyAddressTxt_xpath));      }
	private WebElement getAgencyContactEmailLink() { return findByPresence(By.xpath(this.agencyContactEmailLink_xpath));}
	private WebElement getAgencyContactURL()       { return findByPresence(By.xpath(this.agencyContactURL_xpath));      }
	private WebElement getAgencyTypeTxt()          { return findByPresence(By.xpath(this.agencyTypeTxt_xpath));         }
		
	// Milestones Section -- optional section
	private WebElement getMilestonesLabel()        { return findByPresence(By.xpath(this.milestonesLabel_xpath));       }
	private WebElement getBidPublishedDateLabel()  { return findByPresence(By.xpath(this.bidPublishedDateLabel_xpath)); }
	private WebElement getBidPublishedDateTxt()    { return findByPresence(By.xpath(this.bidPublishedDateTxt_xpath));   }
		
	// Contacts Section -- optional section
	private WebElement getContactsLabel()          { return findByPresence(By.xpath(this.contactsLabel_xpath));         }
	private WebElement getContactNameTxt()         { return findByPresence(By.xpath(this.contactNameTxt_xpath));        }
	private WebElement getContactAddressTxt()      { return findByPresence(By.xpath(this.contactAddressTxt_xpath));     }
	private WebElement getContactPhoneTxt()        { return findByPresence(By.xpath(this.contactPhoneTxt_xpath));       }
	private WebElement getContactEmail()           { return findByPresence(By.xpath(this.contactEmail_xpath));          }
		
	// Documents Section -- optional section
	private WebElement getDocumentsLabel()            { return findByPresence(By.xpath(this.documentsLabel_xpath));           }
	private WebElement getDownloadFirstDocumentIcon() { return findByPresence(By.xpath(this.downloadFirstDocumentIcon_xpath));}

	// Bottom Section --- required section
	private WebElement getSeeBidDetailsBottomBtn() { return findByPresence(By.xpath(this.seeBidDetailsBottomBtn_xpath));}
	private WebElement getMoreLikeThisBtn()        { return findByPresence(By.xpath(this.moreLikeThisBtn_xpath));       }
	private WebElement getLessLikeThisBtn()        { return findByPresence(By.xpath(this.lessLikeThisBtn_xpath));       }
	private WebElement getThisBidIsLocatedLink()   { return findByPresence(By.xpath(this.thisBidIsLocatedLink_xpath));  }

	/*------------- helpers ----------------*/

	/***
	 * <h1>getAllDocumentAttachmentLinks</h1>
	 * <p>purpose: Retrieve a List of WebElements for all the document attachment links.</p>
	 * @return List <WebElement> of all the document attachments | null if no document attachments are present
	 */
	private List <WebElement> getAllDocumentAttachmentLinks()   {
		List <WebElement> allDocumentNames = new ArrayList<WebElement>();
		List <WebElement> allDocs  = this.findAllOrNoElements(driver, By.xpath(this.genericDocumentAttachmentLink_xpath));

		for( int i = 1; i < allDocs.size() + 1; i++ ) {
			// Grab each link
			String xpath = String.format("//mat-list-item[%s]//a[contains(@id, 'bidDetailAttachment_')]", i);
			allDocumentNames.add(findByVisibility(By.xpath(xpath))); }
		return allDocumentNames;
	}
		 
	/*------------- methods ----------------*/

	/***
	 * <h1>navigateToBadBid</h1>
	 * <p>purpose: Navigate to a bad bid (i.e. Details page is not valid)</p>
	 * @return BidDetails
	 */
	public BidDetails navigateToBadBid() {
		driver.navigate().to(this.badBidURL);
		return this;
	}
	
	/***
	 * <h1>clickGoToBid</h1>
	 * <p>purpose: Assuming you're on a BidSync bid, click on "Go to bid" link
	 * 	to punchout to BidSync. Fail test if this isn't a BS bid</p>
	 * @return BSPunchout
	 */
	public BSPunchout clickGoToBids() {
		try {
			this.getBidNumberBSBidLink().click();
		} catch (TimeoutException t) {
			fail("Test Failed: Could not located \"Go to bid\" button. This might not be a BS bid!");
		}
		
		return new BSPunchout(driver);
	}
	
	/***
	 * <h1>goBackToBids</h1>
	 * <p>purpose: Click on the "Back to Bids" button at the top<br>
	 * 	of the Bid Details screen to navigate back to the dashboard</p>
	 * @return BidList
	 */
    public BidList goBackToBids() {
	    getBackToBidsButton().click();
	    return new BidList(driver);
    }
    
    /* ----- verifications ----- */

	/***
	 * <h1>verifyBadBidScreen</h1>
	 * <p>purpose: Verify that a "BID NOT FOUND" message is displayed <br>
	 * 	on the Bid Details screen. Fail test if not</p>
	 * @return BidDetails
	 */
	public BidDetails verifyBadBidScreen() {
		if (!driver.getCurrentUrl().equals(this.badBidURL)){ fail("Test Failed: Did not navigate to \"Bid Not Found\" Bid Details page"); }
		this.getBidNotFoundTxt();
		return this;
	}
	
	/***
	 * <h1>verifyNotDisplayingBidNotFoundScreen</h1>
	 * <p>purpose: Make sure that we aren't displaying "Bid Not Found" instead of Bid Details</p>
	 * @return BidDetails
	 */
	public BidDetails verifyNotDisplayingBidNotFoundScreen() {
		try {
			waitForInvisibility(By.xpath(this.BidNotFoundTxt_xpath));
		} catch (TimeoutException t){ 
			fail("Test Failed: \"BID NOT FOUND\" displaying rather than Bid Details page"); }
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"BID NOT FOUND\" text is not displaying");
		return this;
	}

	/***
	 *<h1>verifyBidDetailsHaveCorrectlyLoaded</h1>
	 *<p>purpose: Verify that all the expected information on the Bid Details<br>
	 *	page is displaying. This includes the static page objects, as well as<br>
	 *	the dynamic bid detail information. Fail if something does not load</p>
	 * <p> There are also checks that:<br>
	 * 	1. EXPIRED tag correctly displays when the bid is expired<br>
	 *  2. Days Remaining value is correctly calculated<br>
	 *  3. Days Remaining only displays when the bid is not expired<br>
	 *  4. Posted Date is always before the Added date<br>
	 *  5. Agency type, icon, and image always display and are one of our
	 *     preset agency types<br>
	 *  6. If BuySpeed bid:<br>
	 *     a. Bid Number is present<br>
	 *  7. If BidSync Source bid:<br>
	 *     a. "This bid is located" text should not display.<br>
	 *     b. "Go to bid" should display instead of Bid Number Text<br>
	 * Fail test if one of these checks does not come back valid</p>
	 * @return BidDetails
	 */
	public BidDetails verifyBidDetailsHaveCorrectlyLoaded() {
		
		/* --- Required Static Objects ----- */
		System.out.println(GlobalVariables.getTestID() + " INFO: Verifying required static page objects appear on Bid Details");

		// Buttons that should always appear: 
		// 1. Star 
		// 2. Thumbs Up 
		// 3. Thumbs Down
		// 4. See Bid Details (top) 
		// 5. Share
		// 6. See Bid Details (bottom)
		// 7. More like this button 
		// 8. Less like this button
		this.getStarBtn();
		this.getThumbsUpBtn();
		this.getThumbsDownBtn();
		this.getSeeBidDetailsTopBtn();
		this.getShareBtn();
		this.getSeeBidDetailsBottomBtn();
		this.getMoreLikeThisBtn();
		this.getLessLikeThisBtn();

		// Labels that should always appear: 
		// 1. Bid End Date 
		// 2. Added 
		// 3. Summary
		// 4. Bid End Date
		// 5. Added
		this.getBidEndDateLabel();
		this.getAddedLabel();
		this.getSummaryLabel();
		this.getBidEndDateLabel();
		this.getAddedLabel();
		
		/* ---- Required Dynamic Data ---- */
		System.out.println(GlobalVariables.getTestID() + " INFO: Verifying required dynamic page objects correctly appear on Bid Details");
		
		// Objects that should always appear:
		// 1. Bid Title should display text
		// 2. Bid Summary should display text
		// 3. Based on Bid End Date (which should display), Either Days Remaining or Expired should display
		//   a. (If displayed) Days Remaining should be calculated from Bid End Date
		//   b. (If displayed) Expired should only display if today > Bid End Date
		// 4. Bid Added date should display
		// 5. Agency, Agency Icon, and Agency Type Text should display
		//    and the types of these three objects should be the same
		// 6. At least one of these: Agency address info, Agency URL, Agency contact info
		// 7. If BidSync Source Bid:
		//    a. "This bid is located" text should not display.
		//    b. "Go to bid" should display instead of Bid Number Text
		// 8. If BuySpeed Bid:
		//    a. Bid Number Text should display
		this.verifyBidTitleTextIsVisible()
			.verifyBidSummaryTextIsVisible()
			.verifyCorrectSetOfDatesDisplay()
			//.verifyDaysRemainingValueIsCorrect() // dmidura: Uncomment after ARGO-2391 is fixed
			.verifySomeAgencyObjectExists()
			.verifyAgencyTypeAndImages()
			.verifyBSBidDisplaysCorrectly()
			.verifyBSOBidDisplaysCorrectly();


		
		/* ----- Optional Dynamic Data ----- */
		System.out.println(GlobalVariables.getTestID() + " INFO: Verifying optional dynamic page objects correctly appear on Bid Details");
		
		// Objects that might appear 
		// (When these appear, then the associated required labels or images should appear)
		//
		// 1. Posted Date - must be on or before Bid Added Date
		//   a. Required label: Posted
		// 2. If any: user contact, user contact address, user contact phones number, user contact email
		//   a. Required labels: CONTACTS
		// 3. Bid Published Date
		//   a. Required labels: MILESTONES, Bid Published Date
		// 4. Documents
		//   a. Required labels: DOCUMENTS
		//   b. Required icon: document
		//   c. (If displayed) document attachment link text should not display encoded special chars
		// 5. Bid URL (top) -- no checks, and looks like we don't care if it appears or not
		// 6. Bid Number (top) -- no checks, except for requirement of BuySpeed bids
		
		this.verifyPostedDateBeforeBidAddedDate()
			.verifyPostedLabelDisplaysIfPostedDate()
			.verifyContactsSectionDisplaysIfUserContactInfo()
			.verifyMilestonesSectionDisplaysIfBidPublishedDate()
			.verifyDocumentsSectionDisplaysIfDocument()
			.verifyDocumentAttachmentLinksText();
		
		return this;
	}
	
	/***
	 * <h1>isBSSourceBid</h1>
	 * <p>purpose: Is this a BidSync source bid?
	 * @return true if yes | no if false
	 */
	private Boolean isBSSourceBid() {
		return (this.doesPageObjectExist(driver, By.xpath(this.bidNumberLinkBSBid_xpath)));
	}
	
	/***
	 * <h1>verifyBSBidDisplaysCorrectly</h1>
	 * <p>purpose: If BidSync Bid then verify:<br>
	 * 	1. "Go to bid" link displays (top of screen)<br>
	 * 	2. "This bid is located at" text does not display (bottom of screen)<br>
	 *  If not a BS bid, then verify:<br>
	 * 	1. Bid Number link displays (top of screen)<br>
	 * 	2. "This bid is located at" text displays (bottom of screen)<br>
	 * Fail test if any combo does not display as expected
	 * @return BidDetails
	 */
	private BidDetails verifyBSBidDisplaysCorrectly() {
		if(this.isBSSourceBid()) {
			// If BS bid then display "Go to bid". Do not display "This bid is located at" text 
			try {
				this.getBidNumberBSBidLink();
				waitForInvisibility(By.xpath(this.thisBidIsLocatedLink_xpath));
			} catch (TimeoutException t) {
				fail("Test Failed: Bid Details do not display correctly for BidSync source bid" + t);
			}
			System.out.println(GlobalVariables.getTestID() + " INFO: Verified BidSync Source Bid Details display");
		} else {
			// If not BS bid, then display Bid Number link. Display "This bid is located at" text
			this.getThisBidIsLocatedLink();
			this.getBidNumberLink();
			System.out.println(GlobalVariables.getTestID() + " INFO: Verified non-BidSync Bid Details display");
		}
		return this;
	}
	
	/***
	 * <h1>verifyBSOBidDisplaysCorrectly</h1>
	 * <p>purpose: If this is a BSO bid, verify it is displaying the Bid Number (top of screen)<br>
	 * 	Note: This method is determing bso bids based on "buyspeed" being in the bid link.
	 * 	If we start pulling in more bso bids than just from headqa, we'll need to readdress
	 * @return BidDetails
	 */
	private BidDetails verifyBSOBidDisplaysCorrectly() {
		if(!this.isBSSourceBid()) {
			if (this.getBidNumberLink().getText().contains("buyspeed")){
				try {
					this.getBidNumberTxt();
				} catch (TimeoutException t) {
					fail("Test Failed: Could not locate bid number for buyspeed bid" + t);
				}
				System.out.println(GlobalVariables.getTestID() + " INFO: Verified Bid Details for BSO bid"); }
		}
		return this;
	}
	
	/***
	 * <h1>verifyBidTitleTextIsVisible</h1>
	 * <p>purpose: There should always be a Bid Title in visible<br>
	 * 	on the Bid Details page. Fail test if there isn't.</p>
	 * @return BidDetails
	 */
	private BidDetails verifyBidTitleTextIsVisible() {
		if (this.getBidTitleTxt().getText().isEmpty()) {
			fail ("Test Failed: Bid Title text is empty"); }
		return this;
	}
	
	/***
	 * <h1>verifyBidSummaryTextIsVisible</h1>
	 * <p>purpose: There should always be a text for the Bid Summary<br>
	 * 	on the Bid Details page. Fail test if there isn't.</p>
	 * @return BidDetails
	 */
	private BidDetails verifyBidSummaryTextIsVisible() {
		if (this.getSummaryTxt().getText().isEmpty()) {
			fail ("Test Failed: Bid Summary text is empty"); }
		return this;
	}
	

	/***
	 * <h1>verifyAgencyTypeAndImages</h1>
	 * <p>purpose: Agency Type, Agency Image, and Agency Image Icon<br>
	 * 	should always appear. In addition, the type of these three<br>
	 * 	objects must be the same. Finally, type must be from our<br>
	 *  set of agency types. Fail test if objects don't appear,<br>
	 * do not match in type, or contain an invalid type.</p>
	 * <p>Ex: Type match<br>
	 * Agency Image src = "/assets/bid-detail/images/Municipality.jpg"<br>
	 * Agency Icon src = "/assets/bid-detail/icons/Municipality.png"<br>
	 * Agency Type = "Municipality"<br>
	 * Here, all there objects match b/c they are of type Municipality</p>
	 * @return BidDetails
	 */
	private BidDetails verifyAgencyTypeAndImages() {
		// Get Agency Type
		String strAgencyType = this.getAgencyTypeTxt().getText().toLowerCase();
		
		// Verify agency type is valid
		if (!this.getValidAgencyTypes().contains(strAgencyType.toLowerCase())) {
			fail ("Test Failed: \"" + strAgencyType + "\" is not a valid agency type"); }
		
		// And now verify icon and image match type
		if (this.getAgencyIconImage().getCssValue("src").toString().toLowerCase().equals(strAgencyType)) {
			fail("Test Failed: Agency Icon Image is does not match Agency Type = '" + strAgencyType + "\""); }

		if (this.getAgencyImage().getCssValue("src").toString().toLowerCase().equals(strAgencyType)) {
			fail("Test Failed: Agency Image is does not match Agency Type = '" + strAgencyType + "\""); }

		return this;
	}
	
	/***
	 * <h1>getValidAgencyTypes</h1>
	 * @return List <String> containing all valid agency types
	 */
	private List <String> getValidAgencyTypes(){
		List <String> allValidAgencyTypes = new ArrayList<String>();
		allValidAgencyTypes.add("school district");
		allValidAgencyTypes.add("university/college");
		allValidAgencyTypes.add("state");
		allValidAgencyTypes.add("county");
		allValidAgencyTypes.add("municipality");
		allValidAgencyTypes.add("community college");
		allValidAgencyTypes.add("port/transit authority");
		allValidAgencyTypes.add("federal");
		allValidAgencyTypes.add("private");
		allValidAgencyTypes.add("military");
		allValidAgencyTypes.add("other");
		allValidAgencyTypes.add("builders exchanges");
		allValidAgencyTypes.add("special district/authority");
		return allValidAgencyTypes;
	}

	/***
	 * <h1>verifyPostedLabelDisplaysIfPostedDate</h1>
	 * <p>purpose: If a bid posted date is displaying, verify that the<br>
	 * 	"Bid Posted:" label also displays. Fail test if label is not displaying</p>
	 * @return BidDetails
	 */
	private BidDetails verifyPostedLabelDisplaysIfPostedDate() {
		if (this.doesPageObjectExist(driver, By.xpath(this.postedTxt_xpath))) {
			this.getPostedLabel(); }
		return this;
	}
	
	/***
	 * <h1>verifySomeAgencyObjectExists</h1>
	 * <p>purpose: At least one agency contact field should be displayed<br>
	 * 	Fail test if no agency contact field displays</p>
	 * <p>Agency Objects to search for:<br>
	 * 	1. Agency Address<br>
	 *  2. Agency Contact URL<br>
	 *  3. Agency Contact Email<br>
	 * @return
	 */
	private BidDetails verifySomeAgencyObjectExists() {
		if (!this.doesPageObjectExist(driver, By.xpath(this.agencyAddressTxt_xpath)) &
		    !this.doesPageObjectExist(driver, By.xpath(this.agencyContactEmailLink_xpath)) &
		    !this.doesPageObjectExist(driver, By.xpath(this.agencyContactURL_xpath)) ) {
			fail ("Test Failed: No Agency Address, Contact Email, or Contact URL provided in Bid Details"); }

		return this;
	}

	/***
	 * <h1>verifyContactsSectionDisplaysIfUserContactInfo</h1>
	 * <p>purpose: If user contact info is displaying, then verify that the <br> 
	 * 	"DOCUMENTS:" section static page objects are also displaying<br>
	 * 	Fail test if they are not displaying</p>
	 * 	<p>Section objects to check for:<br>
	 * 	1. "CONTACTS:" label</p>
	 * 	<p> User contact info to search for:<br>
	 * 	1. user contact address<br>
	 *  2. user contact email<br>
	 *  3. user contact phone number<br>
	 *  4. user contact name</p>
	 * @return BidDetails
	 */
	private BidDetails verifyContactsSectionDisplaysIfUserContactInfo() {
		if (this.doesPageObjectExist(driver, By.xpath(this.contactAddressTxt_xpath)) |
		    this.doesPageObjectExist(driver, By.xpath(this.contactEmail_xpath))      |
		    this.doesPageObjectExist(driver, By.xpath(this.contactNameTxt_xpath))    |
		    this.doesPageObjectExist(driver, By.xpath(this.contactPhoneTxt_xpath))   ) {
			this.getContactsLabel(); }
		return this;
	}

	/***
	 * <h1>verifyMilestonesSectionDisplaysIfBidPublishedDate</h1>
	 * <p>purpose: If a Bid Published Date is displaying, verify that the<br>
	 * 	"MILESTONES:" section static page objects are also displaying<br>
	 * 	Fail test if they are not displaying</p>
	 * 	<p>Section objects to check for:<br>
	 * 	1. "MILESTONS:" label<br>
	 * 	2. "Bid Published Date" label</p>
	 * @return BidDetails
	 */
	private BidDetails verifyMilestonesSectionDisplaysIfBidPublishedDate() {
		if (this.doesPageObjectExist(driver, By.xpath(this.bidPublishedDateTxt_xpath))) {
			this.getMilestonesLabel();
			this.getBidPublishedDateLabel(); }
		return this;
	}

	/***
	 * <h1>verifyDocumentsSectionDisplaysIfDocument</h1>
	 * <p>purpose: If a document is displaying, verify that the<br>
	 * 	"DOCUMENTS:" section static page objects are also displaying<br>
	 * 	Fail test if they are not displaying</p>
	 * 	<p>Section objects to check for:<br>
	 * 	1. "DOCUMENTS:" label</p>
	 * @return BidDetails
	 */
	private BidDetails verifyDocumentsSectionDisplaysIfDocument() {
		if (this.doesPageObjectExist(driver, By.xpath(this.downloadFirstDocumentIcon_xpath))) {
			this.getDocumentsLabel(); }

		return this;
	}
	
	/***
	 * <h1>verifyDocumentAttachmentLinksText</h1>
	 * <p>purpose: Document Attachment links should not display encoded special chars<br>
	 * 	(ex: Document%20Title). Verify there are no encoded special chars in any visible document attachment text.<br>
	 * 	Fail test if encoded special chars is located in document attachment text. <br>
	 * 	Note: If there are no visible document attachments, then this method will do nothing</p>
	 * @return BidDetails
	 */
	private BidDetails verifyDocumentAttachmentLinksText() {
		List <WebElement> allAttachments = this.getAllDocumentAttachmentLinks();
		if (allAttachments != null & !allAttachments.isEmpty()) {
			// Encoded Special Chars are of form: %[A-F,0-9]{2}
			Assert.assertTrue("Test Failed: Document Attachment link is displaying encoded special chars", 
					allAttachments.stream().noneMatch(ele->ele.getText().matches("\\w*\\D*%[A-F,0-9]{2}\\w*\\D*"))); 
			System.out.println(GlobalVariables.getTestID() + " INFO: Verified document attachments are present and do not contain encoded special chars.");}
		return this;
	}
	
	/***
	 * <h1>verifyCorrectSetOfDatesDisplay</h1>
	 * <p>purpose: Days Remaining and EXPIRED should display based on the<br>
	 * 	Bid End Date. Verify that if today is before Bid End Date, then <br>
	 *  we only show Days Remaining/Hours Remaining, and if today is after Bid End Date, then<br>
	 *  we only show EXPIRE. Fail test if not</p>
	 * @return BidDetails
	 */
	private BidDetails verifyCorrectSetOfDatesDisplay() {
		// We should only see the Bid Remaining Date | Expired, but never both
		// Expired if today > Bid End Date
		// Days Remaining if today <= Bid End Date
		Calendar today      = Calendar.getInstance();
		Calendar bidEndDate = this.getBidEndDateAsCalendarDate();

		// If today is before the bid end date, then Days Remaining or Hours Remaining
		// should display and EXPIRED should not display
		if (today.before(bidEndDate)){

			// Days Remaining or Hours Remaining label should display
			if (!this.doesPageObjectExist(driver, By.xpath(this.daysRemainingLabel_xpath))  &
			    !this.doesPageObjectExist(driver, By.xpath(this.hoursRemainingLabel_xpath))) {
				fail("Test Failed: Neither \"Days Remaining\" nor \"Hours Remaining\" label displays"); }
			
			// However, both Days Remaining and Hours Remaining labels should not display
			if (this.doesPageObjectExist(driver, By.xpath(this.daysRemainingLabel_xpath)) &
			    this.doesPageObjectExist(driver, By.xpath(this.hoursRemainingLabel_xpath))) {
				fail("Test Failed: Both \"Days Remaining\" and \"Hours Remaining\" labels are displayed "); }
			
			// Days Remaining text or Hours Remaining text should display
			if (!this.doesPageObjectExist(driver, By.xpath(this.daysRemainingTxt_xpath))  &
			    !this.doesPageObjectExist(driver, By.xpath(this.hoursRemainingTxt_xpath))) {
				fail("Test Failed: Neither \"Days Remaining\" nor \"Hours Remaining\" text displays"); }
	
			// However, both Days Remaining and Hours Remaining texts should not display
			if (this.doesPageObjectExist(driver, By.xpath(this.daysRemainingTxt_xpath)) &
			    this.doesPageObjectExist(driver, By.xpath(this.hoursRemainingTxt_xpath))) {
				fail("Test Failed: Both \"Days Remaining\" and \"Hours Remaining\" text are displayed "); }
			
			// EXPIRED label should not appear
			if (this.doesPageObjectExist(driver, By.xpath(this.expiredLabel_xpath))) {
				fail ("Test Failed: Although bid end date is before today, EXPIRED label displays on Bid Details"); }
			System.out.println(GlobalVariables.getTestID() + " INFO: Verified that Days Remaining/Hours Remaining displays because today is before Bid End Date (\"" + this.getBidEndDateTxt().getText() + "\")");

		// If today is after the bid end date, then EXPIRED
		// should display and Days Remaining should not display
		} else if (today.after(bidEndDate)) {
			this.getExpiredLabel(); 
			if (this.doesPageObjectExist(driver, By.xpath(this.daysRemainingLabel_xpath)) |
			    this.doesPageObjectExist(driver, By.xpath(this.daysRemainingTxt_xpath))) {
				fail ("Test Failed: Although bid end date is after today, Days Remaining date or label displays on Bid Details"); }
			if (this.doesPageObjectExist(driver, By.xpath(this.hoursRemainingLabel_xpath)) |
			    this.doesPageObjectExist(driver, By.xpath(this.hoursRemainingTxt_xpath))) {
				fail ("Test Failed: Although bid end date is after today, Hours Remaining date or label displays on Bid Details"); }
			System.out.println(GlobalVariables.getTestID() + " INFO: Verified that EXPIRED displays because today is after Bid End Date(\"" + this.getBidEndDateTxt().getText() + "\")");
		}

		return this;
	}
	
	/***
	 * <h1>getBidEndDateAsCalendarDate</h1>
	 * @return Bid End Date formatted as Calendar
	 */
	private Calendar getBidEndDateAsCalendarDate() {
		// displayed onscreen as "DECEMBER 12, 2001"
		String bidEndDate = this.getBidEndDateTxt().getText();
		Date date = null;
		try {
			date = new SimpleDateFormat("MMMM dd, yyyy").parse(bidEndDate);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}
	
	/***
	 * <h1>getBidPostedDateAsCalendar</h1>
	 * @return Bid Posted Date formatted as Calendar
	 */
	private Calendar getBidPostedDateAsCalendarDate() {
		// displayed onscreen as "12.12.2001"
		return dateHelpers.getStringDateAsCalendar(this.getPostedTxt().getText().replaceAll("\\.", "/"));
	}

	/***
	 * <h1>getBidPostedDateAsCalendar</h1>
	 * @return Bid Added Date formatted as Calendar
	 */
	private Calendar getBidAddedDateAsCalendarDate() {
		// displayed onscreen as "12.12.2001"
		return dateHelpers.getStringDateAsCalendar(this.getAddedTxt().getText().replaceAll("\\.", "/"));
	}

	/***
	 * <h1>verifyDaysRemainingValueIsCorrect</h1>
	 * <p>purpose: Days Remaining should be set according to:<br>
	 * 	Today's Date - Bid End Date<br>
	 * 	Fail test if the Days Remaining value is incorrect.<br>
	 *  Fail test if Days Remaining is displaying or if bid<br>
	 *  is also displaying as EXPIRED.  	</p>
	 *  <p>dmidura: This method should be updated after ARGO-2391 is fixed<br>
	 * 	Until then, do not use</p>
	 * @return BidDetails
	 */
	private BidDetails verifyDaysRemainingValueIsCorrect() {
		
		// This calculation should only take place if the bid is not expired
		if (!this.doesPageObjectExist(driver, By.xpath(this.expiredLabel_xpath))) {

			// Today -- we will only use the MM/DD/YYYY for this calculation
			// and ignore the HH:MM:SS part of the calendar
			Calendar cal1 = Calendar.getInstance();
			Calendar cal2 = Calendar.getInstance();
			cal2.clear();
			cal2.set(cal1.get(Calendar.YEAR), cal1.get(Calendar.MONTH), cal1.get(Calendar.DAY_OF_MONTH));
			long today = cal2.getTimeInMillis();

			// Bid End Date
			Calendar endDate = this.getBidEndDateAsCalendarDate();

			// Days Remaining = Today's Date - Bid End Date 
			long diff = Math.abs(endDate.getTimeInMillis() - today);
			final int ONE_DAY = 1000 * 60 * 60 * 24;
			long expectedDaysRemaining = diff/ONE_DAY;
			
			// Check if our calculation matches what's displaying for Days Remaining
			String actualDaysRemaining = this.getDaysRemainingTxt().getText();
			if (!actualDaysRemaining.equals(String.valueOf(expectedDaysRemaining))) {
				fail ("Test Failed: Days Remaining is = \"" + actualDaysRemaining + "\" but should be \"" + expectedDaysRemaining + "\""); }
			
			System.out.println(GlobalVariables.getTestID() + " INFO: Verified that there are \"" + expectedDaysRemaining + "\" days remaining");
		} else if (this.doesPageObjectExist(driver, By.xpath(this.daysRemainingTxt_xpath)) | 
		           this.doesPageObjectExist(driver, By.xpath(this.daysRemainingLabel_xpath)) ) {
			fail("Test Failed: Although bid displays as \"EXPIRED\", \"Days Remaining\" is still displaying"); }
		
		return this;
	}
	
	/***
	 * <h1>verifyPostedDateBeforeBidAddedDate</h1>
	 * <p>purpose: If a bid Posted Date is displaying, then the date<br>
	 * 	must be before the bid Added Date (i.e. How can ARGO add<br>
	 *  a bid before the agency posted it?) Fail test if <br>
	 *  Posted Date is not <= Added date. Skip verification is<br>
	 *  Posted Date is not displaying (since Posted Date is optional)</p>
	 * @return BidDetails
	 */
	private BidDetails verifyPostedDateBeforeBidAddedDate() {
		// Posted Date should actually be appearing to run this verification
		// (since posted date is optional)
		if (this.doesPageObjectExist(driver, By.xpath(this.postedTxt_xpath))) {
			Calendar bidPostedDate = this.getBidPostedDateAsCalendarDate();
			Calendar bidAddedDate = this.getBidAddedDateAsCalendarDate();
			
			System.out.println(GlobalVariables.getTestID() + " INFO: Verifying bid posted date (\"" + this.getPostedTxt().getText() +
					"\") is before bid added date (\"" + this.getAddedTxt().getText() + "\")" );

			// Posted Date - must be on or before Bid Added Date
			if (!bidPostedDate.equals(bidAddedDate) &
				!bidPostedDate.before(bidAddedDate)) {
				String strPostedDate = dateHelpers.getCalendarDateAsString(bidPostedDate);
				String strAddedDate  = dateHelpers.getCalendarDateAsString(bidAddedDate);
				fail("Test Failed: Bid Posted Date (\"" + strPostedDate + "\") is showing as after Bid Added Date (\"" + strAddedDate + "\") on Bid Details"); }
		}

		return this;
	}
	
	
	/***
	 * <h1>verifyBidTitleDisplaysCancellationMessage</h1>
	 * <p>purpose: Verify that the "THIS BID HAS BEEN CANCELED."<br>
	 *  message is displaying (to indicate that the bid was cancelled)<br>
	 *  Fail test if the message is not displaying</p>
	 * @return BidDetails
	 */
	public BidDetails verifyBidTitleDisplaysCancellationMessage() {
		this.getCancellationTxt();
		return this;
	}

	/***
	 * <h1>verifyBidIsExpire</h1>
	 * <p>purpose: Try to locate the "EXPIRED" label.
	 * 	Fail test if not located</p>
	 * @return BidDetails
	 */
	public BidDetails verifyBidIsEXPIRED() {
		this.getExpiredLabel();
		this.verifyCorrectSetOfDatesDisplay();
		return this;
	}

	/***
	 * <h1>verifyEndDateIsAsExpected</h1>
	 * <p>purpose: Determine if the currently displayed End Date<br>
	 * 	is displaying as the expected date of Calendar bidEndDate<br>	
	 *  Fail test if the Bid End Date does not display as expected</p>
	 * @param bidEndDate
	 * @return BidDetails
	 */
	public BidDetails verifyEndDateIsAsExpected(Calendar bidEndDate) {
		// Bid End Date displays as a String of MM/DD/YYYY format,
		// so will just compare the bidEndDate as String to displayed value
		String strExpectedDate  = dateHelpers.getCalendarDateAsString(bidEndDate);
		String strDisplayedDate = dateHelpers.getCalendarDateAsString(this.getBidEndDateAsCalendarDate());
		if (!strExpectedDate.equals(strDisplayedDate)){
			fail("Test Failed: Expected Bid End Date is \"" + strExpectedDate + "\", however currently displaying \"" + strDisplayedDate + "\"");}
		return this;
	}
    
    public ZonedDateTime getEndDateInUTC() {
	    Calendar endDate = this.getBidEndDateAsCalendarDate();
        String year = String.valueOf(endDate.get(Calendar.YEAR));
        String month = StringUtils.leftPad(String.valueOf(endDate.get(Calendar.MONTH) + 1), 2, "0");
        String day = StringUtils.leftPad(String.valueOf(endDate.get(Calendar.DAY_OF_MONTH)), 2, "0");
        String endDateAsString = String.format("%s-%s-%sT00:00:00", year, month, day);
        return LocalDateTime.parse(endDateAsString).atZone(ZoneOffset.UTC);
    }
}
