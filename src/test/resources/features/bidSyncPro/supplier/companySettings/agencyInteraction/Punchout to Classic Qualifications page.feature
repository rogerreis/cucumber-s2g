@must @refactor @integrations @level3
Feature: Company Settings > Agency Interaction - Punchout to Classic Qualifications Page
ARGO-608
  
  Scenario: Punchout to Classic Qualifications page
  jlara: I'm keeping this test light because this is existing functionality that's tested in the classic suite.
  The objects inside the frame were tested manually to verify they work inside the frame (pass) 
  and should not be changing through to retirement of classic.
  
    Given I have logged into the Supplier side as user "Basic"
    And I have navigated to Company Settings > "Agency Interaction"
    Given I am on the classic Qualifications iFrame
    Then I can exercise the objects inside the frame
    