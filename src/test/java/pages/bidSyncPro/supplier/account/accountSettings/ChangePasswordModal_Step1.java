package pages.bidSyncPro.supplier.account.accountSettings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.toBeDeprecated.Document;

/**
 * @author jlara
  * <p>TODO: Refactor this class</p>*
 */
public class ChangePasswordModal_Step1 {

    private EventFiringWebDriver driver;
    private HelperMethods helperMethods;

	private final String xpathModal = "//bidsync-change-password/div/mat-card[@id='matChangePasswordComponentCard']";
	//text: Change Password
	private final String xpathHeaderLabel = xpathModal + "//mat-card-title[@id='matChangePasswordTitle']";
	//text: Change your password.
	private final String xpathDescriptionLabel = xpathModal + "//mat-card-content/p";
	//contains text: CANCEL
	private final String xpathCancelButton = xpathModal + "//mat-dialog-actions/button[@id='btnCancel']";
	//contains text: CHANGE PASSWORD
	private final String xpathChangePasswordButton = xpathModal + "//mat-dialog-actions/button[@id='btnChangePassword']";
	
	
	public ChangePasswordModal_Step1(EventFiringWebDriver driver) {
	    this.driver = driver;
	    this.helperMethods = new HelperMethods();
	}
    
    private WebElement getHeaderLabel() {
        helperMethods.waitForVisibilityOfXpath(driver, this.xpathHeaderLabel, Integer.parseInt(System.getProperty("defaultTimeOut")));
        return driver.findElement(By.xpath(xpathHeaderLabel));
    }
    
    private WebElement getDescriptionLabel() {
        helperMethods.waitForVisibilityOfXpath(driver, this.xpathDescriptionLabel, Integer.parseInt(System.getProperty("defaultTimeOut")));
        return driver.findElement(By.xpath(xpathDescriptionLabel));
    }
    
    private WebElement getCancelButton() {
        helperMethods.waitForVisibilityOfXpath(driver, this.xpathCancelButton, Integer.parseInt(System.getProperty("defaultTimeOut")));
        return driver.findElement(By.xpath(xpathCancelButton));
    }
    
    private WebElement getChangePasswordButton() {
        helperMethods.waitForVisibilityOfXpath(driver, this.xpathChangePasswordButton, Integer.parseInt(System.getProperty("defaultTimeOut")));
        return driver.findElement(By.xpath(xpathChangePasswordButton));
    }

    
	public AccountSettings changePasswordTo(String newPassword) {
	    getChangePasswordButton().click();
	    new ChangePasswordModal_Step2(driver)
                .clickConfirmButton();
		return new AccountSettings(driver);
	}
	
	public ChangePasswordModal_Step2 clickChangePasswordButton() {
		getChangePasswordButton().click();
		helperMethods.addSystemWait(1);
		return new ChangePasswordModal_Step2(driver);	
	}

	
}
