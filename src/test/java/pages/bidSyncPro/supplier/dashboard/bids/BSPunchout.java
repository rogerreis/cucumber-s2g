package pages.bidSyncPro.supplier.dashboard.bids;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.common.helpers.GlobalVariables;

/***
 * <h1>Class BSPunchout</h1>
 * @author dmidura
 * <p>details: This class holds page objects and methods for punching
 * 	out to a BidSync agency bid. The punchout is accessed in a bid's "Bid Details"
 * 	screen by clicking on the "Go to bid" button.
 * 	Note: Only BidSync agency bids will have this class.
 */
public class BSPunchout extends DashboardCommon {
	
	// xpaths
	// Above punchout
	private final String backToBidsButton_xpath = "//button[contains(., 'Back to Bid Opportunities')]";
			
	// Common area
	private final String bidTitle_xpath             = "//div[@class='bidTitle']";
	private final String agencyLogo_xpath           = "//div[@class='agencyLogoBox']/a";
	private final String howToPlaceOfferLink_xpath  = "//div[contains(@class, 'howToPlaceAnOffer')]//a";
			                                          //+ "//a[contains(., 'How do I place an offer?')]";
	private final String downloadBidPacketBtn_xpath = "//div[@id='bidDetailButtons']//a[contains(text(), 'Download Bid packet')]";
			
	// Tab stack
	private final String detailsTab_xpath           = "//div[@id='linksBidDetailTabView']//a[contains(., 'Details')]";
	private final String documentsTab_xpath         = "//div[@id='linksBidDetailTabView']//a[contains(., 'Documents')]";
	private final String lineItemTab_xpath          = "//div[@id='linksBidDetailTabView']//a[contains(., 'Line items')]";
	private final String qAndATab_xpath             = "//div[@id='linksBidDetailTabView']//a[contains(., 'Q&A')]";
	private final String preBidConferenceTab_xpath  = "//div[@id='linksBidDetailTabView']//a[contains(., 'Pre-bid conference')]";
	private final String vendorAdsTab_xpath         = "//div[@id='linksBidDetailTabView']//a[contains(., 'Vendor ads')]";
			
	// Bottom Buttons
	private final String placeOfferBtn_xpath        = "//table[@class='bidDetailButtonsDiv']//a[contains(., 'Place offer')]";
	private final String placeNoBidBtn_xpath        = "//table[@class='bidDetailButtonsDiv']//a[contains(., 'Place \"No bid\"')]";

	// Other
	private EventFiringWebDriver driver;
	
	public BSPunchout(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
		
		// BSPunchout opens in new browser tab. Actual content is displayed in an iframe.
		switchToWindow("Agency Bid");
	}
	
	/* ----- getters ----- */

	// Above punchout
	private WebElement getBackToBidsButtons()    { return findByVisibility(By.xpath(this.backToBidsButton_xpath));    }
			
	// Common area
	private WebElement getBidTitle()             { return findByVisibility(By.xpath(this.bidTitle_xpath));            }
	private WebElement getAgencyLogo()           { return findByVisibility(By.xpath(this.agencyLogo_xpath));          }
	private WebElement getHowToPlaceOfferLink()  { return findByVisibility(By.xpath(this.howToPlaceOfferLink_xpath)); }
	private WebElement getDownloadBidPacketBtn() { return findByVisibility(By.xpath(this.downloadBidPacketBtn_xpath));}
			
	// Tab stack
	private WebElement getDetailsTab()           { return findByVisibility(By.xpath(this.detailsTab_xpath));          }
	private WebElement getDocumentsTab()         { return findByVisibility(By.xpath(this.documentsTab_xpath));        }
	private WebElement getLineItemTab()          { return findByVisibility(By.xpath(this.lineItemTab_xpath));         }
	private WebElement getQAndATab()             { return findByVisibility(By.xpath(this.qAndATab_xpath));            }
	private WebElement getPreBidConferenceTab()  { return findByVisibility(By.xpath(this.preBidConferenceTab_xpath)); }
	private WebElement getVendorAdsTab()         { return findByVisibility(By.xpath(this.vendorAdsTab_xpath));        }
			
	// Bottom Buttons
	private WebElement getPlaceOfferBtn()        { return findByVisibility(By.xpath(this.placeOfferBtn_xpath));       }
	private WebElement getPlaceNoBidBtn()        { return findByVisibility(By.xpath(this.placeNoBidBtn_xpath));       }
	
	/* ----- helpers ----- */
	/***
	 * <h1>switchToPunchoutFrame</h1>
	 * <p>purpose: Punchout is inside an iframe. Call this method to access the frame
	 */
	private void switchToPunchoutFrame() { driver.switchTo().frame("bidsyncFrame"); }
	/***
	 * <h1>switchToProFrame</h1>
	 * <p>purpose: Access to Pro menus are not in the punchout iframe. Call this method to access Pro menus
	 */
	private void switchToProFrame()      { driver.switchTo().defaultContent();      }
	
	/* ----- methods ----- */
	
	/***
	 * <h1>clickBackToBids</h1>
	 * <p>purpose: Click on the "BACK TO BIDS" button to return to the bid list
	 * @return BidList
	 */
	public BidList clickBackToBids() {
		this.switchToProFrame();
		this.getBackToBidsButtons().click();
		return new BidList(driver);
	}
	
	/* ----- verifications ----- */
	
	/***
	 * <h1>verifyPageLoad</h1>
	 * <p>purpose: Verify that the BS Punchout has loaded per:<br>
	 * 	1. "BACK TO BIDS" button<br>	
	 * 	2. Bid title<br>
	 * 	3. Agency Logo<br>
	 * 	4. "How to place an offer?" link<br>
	 * 	5. "Download Bid Packet" button<br>
	 * 	6. Tab Stack for tabs ("Details", "Documents", "Line Items", "Q&A", "Pre-bid conference", "Vendor ads")<br>
	 * 	7. "Place offer" button<br>
	 * 	8. "Place No bid" button<br>
	 * Fail test if one of above does not load
	 * @return BSPunchout
	 */
	public BSPunchout verifyPageLoad() {
		// Above punchout
		this.switchToProFrame();
		this.getBackToBidsButtons();
		
		// Common Area
		this.switchToPunchoutFrame();
		this.getBidTitle();
		this.getAgencyLogo();
		this.getHowToPlaceOfferLink();
		this.getDownloadBidPacketBtn();
		
		// Tab stack
		this.getDetailsTab();
		this.getDocumentsTab();
		this.getLineItemTab();
		this.getQAndATab();
		this.getPreBidConferenceTab();
		this.getVendorAdsTab();
		
		// Bottom buttons
		this.getPlaceOfferBtn();
		this.getPlaceNoBidBtn();
		
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that BS bid punchout has properly loaded");
		
		return this;
	}
}
