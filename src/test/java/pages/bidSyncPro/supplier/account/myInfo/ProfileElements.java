package pages.bidSyncPro.supplier.account.myInfo;

import io.cucumber.datatable.DataTable;
import pages.common.helpers.HelperMethods;

import java.util.List;
import java.util.Map;

/***
 * <h1>Class ProfileElements</h1>
 * @author dmidura
 * <p>details: This class holds all expected elements of a profile on the My Profile tab and its children.<br.
 * 	This class can be used to compare profile states between those pages</p>
 */
public class ProfileElements {
	
	private String firstName;
	private String lastName;
	private String companyName;
	private String jobTitle;
	private String addressLine1;
	private String addressLine2;
	private String country;
	private String state;
	private String city;
	private String zip;
	private String phoneNumber;
	private String phoneExt;
	private String faxNumber;
	private String faxExt;
    
    public ProfileElements() { }
    
    public ProfileElements(DataTable dataTable) {
        Map<String, List<String>> inputsMap = new HelperMethods().putDataTableIntoMap(dataTable);
        firstName    = inputsMap.get("First Name"     ).get(0);
        lastName     = inputsMap.get("Last Name"      ).get(0);
        jobTitle     = inputsMap.get("Job Title"      ).get(0);
        addressLine1 = inputsMap.get("Address Line 1" ).get(0);
        addressLine2 = inputsMap.get("Address Line 2" ).get(0);
        country      = inputsMap.get("Country"        ).get(0);
        state        = inputsMap.get("State"          ).get(0);
        city         = inputsMap.get("City"           ).get(0);
        zip          = inputsMap.get("Zip"            ).get(0);
        phoneNumber  = inputsMap.get("Phone Number"   ).get(0);
        phoneExt     = inputsMap.get("Phone Ext"      ).get(0);
        faxNumber    = inputsMap.get("Fax Number"     ).get(0);
        faxExt       = inputsMap.get("Fax Ext"        ).get(0);
    }
	
    // getters
	public String getFirstName()    { return firstName;    }
	public String getLastName()     { return lastName;     }
	public String getAddressLine1() { return addressLine1; }
	public String getAddressLine2() { return addressLine2; }
	public String getCountry()      { return country;      }
	public String getState()        { return state;        }
	public String getZip()          { return zip;          }
	public String getCity()         { return city;         }
	public String getCompanyName()  { return companyName;  }
	public String getJobTitle()     { return jobTitle;     }
	public String getPhoneNumber()  { return phoneNumber;  }
	public String getPhoneExt()     { return phoneExt;     }
	public String getFaxNumber()    { return faxNumber;    }
	public String getFaxExt()       { return faxExt;       }

	// setters
	public void setFirstName   (String firstName)      { this.firstName    = firstName;      }
	public void setLastName    (String lastName)       { this.lastName     = lastName;       }
	public void setAddressLine1(String addressLine1)   { this.addressLine1 = addressLine1;   }
	public void setAddressLine2(String addressLine2)   { this.addressLine2 = addressLine2;   }
	public void setCountry     (String country)        { this.country      = country;        }
	public void setState       (String state)          { this.state        = state;          }
	public void setZip         (String zip)            { this.zip          = zip;            }
	public void setCompanyName (String companyName)    { this.companyName  = companyName;    }
	public void setCity        (String city)           { this.city         = city;           }
	public void setJobTitle    (String jobTitle)       { this.jobTitle     = jobTitle;       }
	public void setPhoneNumber (String phoneNumber)    { this.phoneNumber  = phoneNumber;    }
	public void setPhoneExt    (String phoneNumberExt) { this.phoneExt     = phoneNumberExt; }
	public void setFaxNumber   (String faxNumber)      { this.faxNumber    = faxNumber;      }
	public void setFaxExt      (String faxNumberExt)   { this.faxExt       = faxNumberExt;   }
	
}
