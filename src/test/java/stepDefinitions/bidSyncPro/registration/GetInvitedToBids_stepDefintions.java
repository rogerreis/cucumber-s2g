package stepDefinitions.bidSyncPro.registration;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.bidSyncPro.registration.CreateNewCompany;
import pages.bidSyncPro.registration.GetInvitedToBids;
import pages.common.helpers.GlobalVariables;

public class GetInvitedToBids_stepDefintions {

	private EventFiringWebDriver driver;
	
	public GetInvitedToBids_stepDefintions (CoreAutomation automation) {
		this.driver     = automation.getDriver();
	}
	
	/* ----- helpers ----- */
	/***
	 * <h1>addCompanyIntoLookupOneCharAtATime</h1>
	 * <p>purpose: Enter company name into input one char at a time,<br>
	 * 	validating each addition in the Company Lookup. When complete<br>
	 * 	save out the company name as global var "companyName"</p>
	 * @param companyName = name of company to enter into input
	 * @return GetInvitedToBids
	 */
	private GetInvitedToBids addCompanyIntoLookupOneCharAtATime(String companyName) {
		GlobalVariables.storeObject("companyName", companyName);

		return new GetInvitedToBids(driver)
				.inputCompanyIntoCompanyLookupOneCharAtATime(companyName);
	}
	
	/***
	 * <h1>enterNewCompanyAndContinue</h1>
	 * <p>purpose: Enter the new company into the Company Lookup.<br>
	 * 	Verify no company is located. Click "NEXT" to navigate to "Create New Company"</p>
	 * @param companyName = company name
	 * @return CreateNewCompany
	 */
	private CreateNewCompany enterNewCompanyAndContinue(String companyName) {
		return new GetInvitedToBids(driver)
				.inputCompanyIntoCompanyLookupAsSingleString(companyName)
				.addNewCompanyNotInLookup(companyName);
	}

	
	/* ----- test steps ------ */
	
	@And ("^I have entered \"(.*?)\" into the Company Lookup one letter at a time and receive valid results$")
	public void I_have_entered_searchTerm_into_the_Company_Lookup_one_letter_at_a_time_and_receive_valid_results(String companyName) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Entering \"" + companyName + "\" into the company lookup one char at a time");
		this.addCompanyIntoLookupOneCharAtATime(companyName);
	}
	
	@When ("^I have added a new company into the Company Lookup and clicked NEXT$")
	public void I_have_added_a_new_company_into_the_Company_Lookup_and_clicked_NEXT() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Entering random company into lookup and clicking NEXT");

		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		this.enterNewCompanyAndContinue(user.getCompany().getName());
	}

	@When ("^I have entered a random string into the Company Lookup one letter at a time and receive valid results$")
	public void I_have_entered_a_random_string_into_the_Company_Lookup_one_letter_at_a_time_and_receive_valid_results() {

		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		this.addCompanyIntoLookupOneCharAtATime(user.getCompany().getName());
	}

	@When ("^I select the top returned result from the Company Lookup autocomplete results$")
	public void I_select_the_top_returned_result_from_the_Company_Lookup_autocomplete_results() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Selecting top company result from Company Lookup");
		String companyName = GlobalVariables.getStoredObject("companyName");
		new GetInvitedToBids(driver)
			.joinCompanyAlreadyInLookup(companyName);
	}

	@Then ("^I will verify that I am now the owner of the company$")
	public void I_will_verify_that_I_am_now_the_owner_of_the_company() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying user is on \"Create New Company\"");
		new CreateNewCompany(driver)
			.verifyPageInitialization();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: After entering company one letter at a time into Company Lookup, company was located and user is owner");
	}
	
	@Then ("^I will verify that after completing input of my company name no result is returned by the company lookup$")
	public void I_will_verify_that_after_completing_input_of_my_company_name_no_result_is_returned_by_the_company_lookup() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying no results displayed in Company Lookup");
		
		// Company Lookup closes when no results are returned
		new GetInvitedToBids(driver)
			.verifyCompanyLookupIsClosed()
			.clickNextBtn();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: After entering new company one letter at a time into Company Lookup, Company Lookup closes b/c company cannot be located");
	}

	@Then ("^I will verify that the Company Lookup screen correctly initializes$")
	public void I_will_verify_that_the_Company_Lookup_screen_correctly_initializes() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying initialization of the \"Get Invited to Bids\" page");
		new GetInvitedToBids(driver)
			.verifyPageInitialization();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: \"Get Invited to Bids\" screen properly initialized");
	}
	
}