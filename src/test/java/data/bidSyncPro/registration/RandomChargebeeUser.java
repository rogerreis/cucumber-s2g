package data.bidSyncPro.registration;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.util.Assert;

import pages.bidSyncPro.common.DatabaseCommonSearches;
import pages.common.helpers.GeographyHelpers;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.RandomHelpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.fail;

/***
 * <h1>Class RandomChargebeeUser</h1>
 * @author dmidura
 * <p>details: This class defines all expected data for registering a user with Chargebee (Pro or Basic).<br>
 * 	Note: Class constructor will populate fields with random data, so if you want to specify a user, you'll need to use class setters</p>
 *
 */
public class RandomChargebeeUser {
    
	// Registration Data
    private String id;
    private String state;  // full name
    private String emailAddress;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String phoneExtension;
    private String address;
    private String addressLine2;
    private String city;
    private String zipCode;
    private String country;
    private String password;
    private String notificationState;
    private List<String> positiveKeywords;
    private List<String> negativeKeywords;
    private RandomCompanyInformation company;
    private SubscriptionPlan            plan;
    private SubscriptionGrouping        grouping;
    private List <SubscriptionAddOns>   addOns; 
    private List <AdditionalStateAddOn> stateAddOns;
    private CreditCard                  creditCard;
    private String companyRevenue;
//    private String companyRevenueForMasonry;
    private String companySize;
    private String industry;
    private String reasonForRegistration;
//    private String reasonForRegistrationForMasonry;

    // Other
    private GeographyHelpers geographyHelpers;
    private RandomHelpers    randomHelpers;
    
    /***
     * <h1>RandomChargebeeUser</h1>
     * <p>purpose: Create a new RandomChargebeeUser.<br>
     * 	This method will generate a new user with the following defaults:<br>
     * 	(Use provided setters to update to specific registration data if needed)<br>
     * 	1.  emailAddress     = random_string@phimail.mailinator.com<br>
     *  2.  firstName        = random string<br>
     *  3.  lastName         = random string<br>
     *  4.  phoneNumber      = random phone number    (10 int)<br>
     *  5.  phoneExtension   = random phone extension (1  int)<br>
     *  6.  address          = random string<br>
     *  7.  addressLine2     = random string<br>
     *  8.  city             = random string<br>
     *  9.  state            = random state (full state name)<br>
     *  10. zipCode          = zip corresponding to state<br>
     *  11. country          = "United States"<br>
     *  12. company          = random data generated for RandomCompanyInformation()<br>
     *  13. password         = "test_1234"<br>
     *  14. positiveKeywords = "services", "construction", "test"<br>
     *  15. negativeKeywords = empty<br>
     *  16. plan             = BASIC<br>
     *  17. grouping         = NONE<br>
     *  18. addOns           = empty<br>
     *  19. creditCard       = canned data from CreditCard()<br>
     *  20. stateAddOns      = empty<br>
     *  21. notificationState = abbreviation of value of state variable<br>
     *  22. companyRevenue 	= random string from CompanyRevenueEnum
     *  23. companySize 	= random string from CompanySizeEnum
     *  24. industry 		= random string from IndustryEnum
     *  25. reasonForRegistration = random string from ReasonForRegistrationEnum
     */
    public RandomChargebeeUser() {
        geographyHelpers  = new GeographyHelpers();
        randomHelpers     = new RandomHelpers();
        id                = "";
        emailAddress      = randomHelpers.getRandomEmail();
        firstName         = RandomStringUtils.randomAlphabetic(1, 10);
        lastName          = RandomStringUtils.randomAlphabetic(1, 10);
        phoneNumber       = randomHelpers.getRandomIntStringOfLength(10);
        phoneExtension    = String.valueOf(RandomUtils.nextInt(111, 999));
        address           = String.valueOf(RandomUtils.nextInt(111, 9999)) + " " + randomHelpers.getRandomString();
        addressLine2      = randomHelpers.getRandomString();
        city              = RandomStringUtils.randomAlphabetic(3, 10);
        state             = randomHelpers.getRandomUSState();
        zipCode           = geographyHelpers.getValidZipCodeForState(state);
        country           = "United States"; //Changed from "United States of America" so compare will succeed
        notificationState = geographyHelpers.getStateOrProvinceAbbreviationFromStateName(state);
        company           = new RandomCompanyInformation();
        password          = "test_1234"; // TODO: create a generate valid password method
        positiveKeywords  = Arrays.asList("services", "construction", "test");
        negativeKeywords  = new ArrayList<>();
        plan              = SubscriptionPlan.BASIC;      // default for a basic plan
        grouping          = SubscriptionGrouping.NONE;   // default for a basic plan
        addOns            = new ArrayList<>();           // default empty (no addons)
        creditCard        = new CreditCard();
        stateAddOns       = new ArrayList<>();           // default empty (no additional state addOns)
        companyRevenue	  = toCompanyRevenueValue(getRandomCompanyRevenue());
      //Needs to remove this when we fix both BidSync and Masonry site optional values same
//        companyRevenueForMasonry	  = toCompanyRevenueValueForMasonry(getRandomCompanyRevenue());
//        reasonForRegistrationForMasonry= toReasonForRegistrationValueForMasonry(getRandomReasonForRegistration());
        companySize		  = toCompanySizeValue(getRandomCompanySize());
        industry		  = toIndustryValue(getRandomIndustry());
        reasonForRegistration= toReasonForRegistrationValue(getRandomReasonForRegistration());
    }
    
    /* ----- getters ----- */
    public String                    getState()                { return state;            }
    public String                    getEmailAddress()         { return emailAddress;     }
    public String                    getFirstName()            { return firstName;        }
    public String                    getLastName()             { return lastName;         }
    public String                    getPhoneNumber()          { return phoneNumber;      }
    public String                    getPhoneExtension()       { return phoneExtension;   }
    public String                    getAddress()              { return address;          }
    public String                    getAddressLine2()         { return addressLine2;     }
    public String                    getCity()                 { return city;             }
    public String                    getZipCode()              { return zipCode;          }
    public String                    getCountry()              { return country;          }
    public String                    getNotificationState()    { return notificationState;}
    public String                    getPassword()             { return password;         }
    public RandomCompanyInformation  getCompany()              { return company;          }
    public CreditCard                getCreditCard()           { return creditCard;       }
    public List<String>              getPositiveKeywords()     { return positiveKeywords; }
    public List<String>              getNegativeKeywords()     { return negativeKeywords; }
    public SubscriptionPlan          getSubscriptionPlan()     { return plan;             }
    public SubscriptionGrouping      getSubscriptionGrouping() { return this.grouping;    }
    public List <SubscriptionAddOns> getSubscriptionAddOns()   { return this.addOns;      } 
    public List <AdditionalStateAddOn> getAdditionalStateAddOns() { return this.stateAddOns; }

    public String                    getCompanyRevenue()        { return companyRevenue;   }
//    public String                    getCompanyRevenueForMasonry(){ return companyRevenueForMasonry;}
    public String                    getCompanySize()           { return companySize;      }
    public String                    getIndustry()              { return industry;         }
    public String                    getReasonForRegistration() { return reasonForRegistration;}
//    public String                    getReasonForRegistrationForMasonry() { return reasonForRegistrationForMasonry;}
    /***
     * <h1>getNotificationStateAsFullName</h1>
     * <p>purpose: Return the full name of the Notification State<br>
     * 	Note: If you want the Notification State as an abbreviation, use getNotificationState()</p>
     * @return full state name for the current value of the Notification State. (ex: "Texas")
     */
    public String getNotificationStateAsFullName() {return geographyHelpers.getStateOrProvinceAbbreviationFromStateName(this.getNotificationState());}

    /***
     * <h1>getSubscriptionAddOnsInChargebeeFormat</h1>
     *	<p>purpose: In the Chargebee site, a Subscription addOn appears per:<br>
	 *	[SubscriptionAddOn] Bids - [planLocation] Plan<br>
	 *	Return the list of Subscription AddOns for this RandomChargebeeUser in this Chargebee format<br>
	 * 	Ex:<br>
	 *	Military Bids - State Plan
     * @return List <String> of Subscription AddOns in Chargebee site format
     */
    public List <String> getSubscriptionAddOnsInChargebeeFormat(){
    	List <String> formatted = new ArrayList<>();
		this.addOns.forEach(addOn->formatted.add(addOn.getChargebeeFormattedAddOn(this.plan)));    	
		return formatted;
    }

    /***
     * <h1>getGroupingFromState</h1>
     * <p>purpose: Given the full name of the state, return its<br>
     * 	SubscriptionGrouping value</p>
     * @return SubscriptionGrouping
     */
    private SubscriptionGrouping getGroupingFromState() {
    	// state is full name, so get its abbrevation
    	String strStateAbbrev = geographyHelpers.getStateOrProvinceAbbreviationFromStateName(this.state);
    	return SubscriptionGrouping.valueOf(strStateAbbrev); }

    /***
     * <h1>getId</h1>
     * <p>purpose: Return the user_id from the database for our user</p>
     * @return user_id from database
     */
    public String getId() {
        if(id.equals("")) { id = getIdFromDatabase(); }
        return id; 
    }
    
    /***
     * <h1>getIdFromDatabase</p>
     * <p>purpose: Return the user_id from the database</p>
     * @return user_id from database
     */
    private String getIdFromDatabase() {
    	return new DatabaseCommonSearches().getUserIdFromUserEmail(this.getEmailAddress());
    }
 
    /* ----- setters ----- */
    public void setEmailAddress  (String emailAddress  ) { this.emailAddress   = emailAddress;   }
    public void setFirstName     (String firstName     ) { this.firstName      = firstName;      }
    public void setLastName      (String lastName      ) { this.lastName       = lastName;       }
    public void setPhoneNumber   (String phoneNumber   ) { this.phoneNumber    = phoneNumber;    }
    public void setPhoneExtension(String phoneExtension) { this.phoneExtension = phoneExtension; }
    public void setAddress       (String address       ) { this.address        = address;        }
    public void setAddressLine2  (String addressLine2  ) { this.addressLine2   = addressLine2;   }
    public void setCity          (String city          ) { this.city           = city;           }
    public void setState         (String state         ) { this.state          = state;          }
    public void setZipCode       (String zipCode       ) { this.zipCode        = zipCode;        }
    public void setCountry       (String country       ) { this.country        = country;        }
    public void setPassword      (String password      ) { this.password       = password;       }
    
    public void setCompany             (RandomCompanyInformation company             ) { this.company          = company;          }
    public void setCreditCard          (CreditCard               creditCard          ) { this.creditCard       = creditCard;       }
    public void setPositiveKeywords    (List<String>             positiveKeywords    ) { this.positiveKeywords = positiveKeywords; }
    public void setNegativeKeywords    (List<String>             negativeKeywords    ) { this.negativeKeywords = negativeKeywords; }
    
    public void setSubscriptionPlan    (SubscriptionPlan         plan                ) { this.plan             = plan;                }
    public void setSubscriptionGrouping(SubscriptionGrouping     subscriptionGrouping) { this.grouping         = subscriptionGrouping;}
    public void setNotificationState   (String                   notificationState   ) {this.notificationState = notificationState;   }
    
    
    /***
     * <h1>setNotificationStateFromFullStateName</h1>
     * <p>purpose: Given the full state name (not abbrev), set the Notification State<br>
     * 	Note: If given a state abbrev, then just use setNotificationState()</p>
     * @param notificationState = full state name (ex: "Texas")
     */
    public void setNotificationStateFromFullStateName(String notificationState) {
    	this.notificationState = geographyHelpers.getStateOrProvinceAbbreviationFromStateName(notificationState);}
    
    /***
     * <h1>setSubscriptionGroupingBasedOnSubscription</h1>
     * <p>purpose: set the subscription grouping based on the
     * 	subscription plan<br>
     * 	1. National Plan: set grouping as "USA"<br>
     *  2. Regional Plan: set grouping to a randomly selected region<br>
     *  3. State Plan: set the grouping  to the state used in registration<br>
     *  4. Basic Plan: set the grouping to NONE</p>
     * @param plan
     */
    public void setSubscriptionGroupingBasedOnSubscription(SubscriptionPlan plan) {
    	if (plan.isStatePlan()) {
    		// Grouping is the registration state for a state plan
    		this.grouping = this.getGroupingFromState() ;
    	} else if (plan.isNationalPlan()) {
    		// Grouping is "USA" for a national plan
    		this.grouping = SubscriptionGrouping.USA;
    	} else if (plan.isRegionalPlan()) {
    		// Set any region for the regional plan
    		this.setRandomSubscriptionGroupingRegion();
    	} else {
    		// There is no grouping for a basic plan
    		this.grouping = SubscriptionGrouping.NONE; }
    	
    	System.out.println(GlobalVariables.getTestID() + " INFO: Set subscription grouping to \"" + this.getSubscriptionGrouping().getChargebeeGroupingName() + "\"");
    }
    
    /***
     * <h1>setRandomSubscriptionGroupingRegion</h1>
     * <p>purpose: Randomly select a subscription grouping (REGION_A - REGION_G) for a Regional Pro plan</p>
     */
    private void setRandomSubscriptionGroupingRegion() {
    	switch (randomHelpers.getRandomInt(8)) {
    	case 0:
    		this.grouping = SubscriptionGrouping.REGION_A;
    		break;
    	case 1:
    		this.grouping = SubscriptionGrouping.REGION_B;
    		break;
    	case 2:
    		this.grouping = SubscriptionGrouping.REGION_C;
    		break;
    	case 3:
    		this.grouping = SubscriptionGrouping.REGION_D;
    		break;
    	case 4:
    		this.grouping = SubscriptionGrouping.REGION_E;
    		break;
    	case 5:
    		this.grouping = SubscriptionGrouping.REGION_F;
    		break;
    	case 6:
    		this.grouping = SubscriptionGrouping.REGION_G;
    		break;
    	case 7:
    		this.grouping = SubscriptionGrouping.REGION_H;
    		break;
    	}
    }
    
    /***
     * <h1>setRandomSubscriptionAddOns</h1>
     * <p>purpose: Randomly set subscription addOns as some combination of FEDERAL, MILITARY, CANADA</p> 
     */
    public void setRandomSubscriptionAddOns() {
    	
    	List <SubscriptionAddOns> addOns = new ArrayList <SubscriptionAddOns>();
    	switch (randomHelpers.getRandomInt(6)) {
    	case 0:
    		addOns.add(SubscriptionAddOns.FEDERAL);
    		break;
    	case 1:
    		addOns.add(SubscriptionAddOns.MILITARY);
    		break;
    	case 2:
    		addOns.add(SubscriptionAddOns.CANADA);
    		break;
    	case 3:
    		addOns.add(SubscriptionAddOns.FEDERAL);
    		addOns.add(SubscriptionAddOns.MILITARY);
    		break;
    	case 4:
    		addOns.add(SubscriptionAddOns.MILITARY);
    		addOns.add(SubscriptionAddOns.CANADA);
    		break;
    	case 5:
    		addOns.add(SubscriptionAddOns.FEDERAL);
    		addOns.add(SubscriptionAddOns.MILITARY);
    		addOns.add(SubscriptionAddOns.CANADA);
    		break;
    	}
    	this.addOns = addOns;
    	System.out.println(GlobalVariables.getTestID() + " INFO: Select addOns " + this.addOns.toString());
    }
    
    /***
     * <h1>setSubscriptionAddons</h1>
     * <p>purpose: Manually set subscription addOns</p>
     * @param subscriptionAddOns = up to three addOns
     */
    public void setAddOns(List  <SubscriptionAddOns> subscriptionAddOns) {
    	if (subscriptionAddOns.size() >3) { fail ("ERROR: There can only be up to three subscription addOns"); }
    	this.addOns = subscriptionAddOns;
    }
    
    /***
     * <h1>setRandomSubscriptionAddOns</h1>
     * <p>purpose: Randomly set additional state addOns, for up to five additional states.
     * 	The value of each state will be selected at random, but will not be repeated among the previously
     * 	selected states.
     */
    public void setRandomAdditionalStateAddOns() {
    	List <AdditionalStateAddOn> stateAddOns        = new ArrayList <AdditionalStateAddOn>();
    	List <AdditionalStateAddOn> allAvailableStates = new ArrayList<AdditionalStateAddOn>(Arrays.asList(AdditionalStateAddOn.values()));
    	
    	// Randomly set numStates Additional State addOns (can set up to five)
    	int numStates = randomHelpers.getRandomInt(1, 5);
    	for (int i = 1; i < numStates + 1; i++) {
    		// Randomly pick a state from the available state for the Additional State addOn
    		int randomIndex = randomHelpers.getRandomInt(allAvailableStates.size());
    		AdditionalStateAddOn addedState = allAvailableStates.get(randomIndex);
    		stateAddOns.add(addedState);
    		
    		// Don't reuse states in the list of Additional State addOns
    		allAvailableStates.remove(addedState);
    	}

    	this.stateAddOns = stateAddOns;
    	System.out.println(GlobalVariables.getTestID() + " INFO: Randomly setting Additional State AddOns to " + this.stateAddOns);
    }
    
    /***
     * <h1>setAdditionalStateAddOns</h1>
     * <p>purpose: Manually set Additional State addOns</p>
     * @param additionalStateAddOns = up to five additional state addOns
     */
    public void setAdditionalStateAddOns (List <AdditionalStateAddOn> additionalStateAddOns) {
    	Assert.isTrue(additionalStateAddOns.size() < 6, "ERROR: There can only be up to five additional state addOns");
    	this.stateAddOns = additionalStateAddOns;
    }
    
    /* ----- methods ----- */
   
    /***
     * <h1>printUser</h1>
     * <p>purpose: Print out Chargebee user registration data<br>
     * 	1.  First Name<br>
     * 	2.  Last Name<br>
     * 	3.  Email Address<br>
     * 	4.  Password<br>
     * 	5.  Company<br>
     * 	6.  Address<br>
     * 	7.  City<br>
     * 	8.  State<br>
     * 	9.  Zip Code<br>
     * 	10. Country<br>
     * 	11. Phone Number<br>
     *  12. Phone Extension</p>
     */
    public void printUser() {
        System.out.printf(GlobalVariables.getTestID() + " First Name:    %s\n", getFirstName()          );
        System.out.printf(GlobalVariables.getTestID() + " Last Name:     %s\n", getLastName()           );
        System.out.printf(GlobalVariables.getTestID() + " Email Address: %s\n", getEmailAddress()       );
        System.out.printf(GlobalVariables.getTestID() + " Password:      %s\n", getPassword()           );
        System.out.printf(GlobalVariables.getTestID() + " Company:       %s\n", getCompany().getName()  );
        System.out.printf(GlobalVariables.getTestID() + " Address:       %s\n", getAddress()            );
        System.out.printf(GlobalVariables.getTestID() + " City:          %s\n", getCity()               );
        System.out.printf(GlobalVariables.getTestID() + " State:         %s\n", getState()              );
        System.out.printf(GlobalVariables.getTestID() + " Zip Code:      %s\n", getZipCode()            );
        System.out.printf(GlobalVariables.getTestID() + " Country:       %s\n", getCountry()            );
        System.out.printf(GlobalVariables.getTestID() + " Phone Number:  %s\n", getPhoneNumber()        );
        System.out.printf(GlobalVariables.getTestID() + " Phone Ext:     %s\n", getPhoneExtension()     );
    }
    
    /***
     * <h1>printSubscriptionDetails</h1>
     * <p>purpose: Print to screen info about user's subscription details. <br>
     * 	1. Subscription Plan<br>
     * 	2. Subscription Grouping <br>
     * 	3. Subscription AddOns<br>
     * 	4. Additional State AddOns<br>
     */
    public void printSubscriptionDetails() {
    	System.out.printf(GlobalVariables.getTestID() + " Subscription Plan:     %s\n", getSubscriptionPlan().toString());
    	System.out.printf(GlobalVariables.getTestID() + " Subscription Grouping: %s\n", getSubscriptionGrouping().toString());
    	System.out.printf(GlobalVariables.getTestID() + " Subscription AddOns:   %s\n", getSubscriptionAddOns().toString());
    	System.out.printf(GlobalVariables.getTestID() + " Additional   States:   %s\n", getAdditionalStateAddOns().toString());
    }
    
	public String toCompanyRevenueValue(CompanyRevenueEnum companyRevenue) {
		String selectedRevenue = null;
		switch (companyRevenue) {
		case _0_500K:
			selectedRevenue = "$0-$500K";
			break;
		case _500K_1M:
			selectedRevenue = "$500K-$1M";
			break;
		case _1M_10M:
			selectedRevenue = "$1M-$10M";
			break;
		case _10M_50M:
			selectedRevenue = "$10M-$50M";
			break;
		case _50M_100M:
			selectedRevenue = "$50M-$100M";
			break;
		case _100M_500M:
			selectedRevenue = "$100M-$500M";
			break;
		case _500M_PLUS:
			selectedRevenue = "$500M+";
			break;
		default:
				fail("Not able to find the specified optional value for company revenue");
				break;
		}
		return selectedRevenue;
	}
	
//	public String toCompanyRevenueValueForMasonry(CompanyRevenueEnum companyRevenue) {
//		String selectedRevenue = null;
//		switch (companyRevenue) {
//		case _0_500K:
//			selectedRevenue = "$0-$500K";
//			break;
//		case _500K_1M:
//			selectedRevenue = "$500K-$1M";
//			break;
//		case _1M_10M:
//			selectedRevenue = "$1M-$10M";
//			break;
//		case _10M_50M:
//			selectedRevenue = "$10M-$50M";
//			break;
//		case _50M_100M:
//			selectedRevenue = "$50M-$100M";
//			break;
//		case _100M_500M:
//			selectedRevenue = "$100M-$500M";
//			break;
//		case _500M_PLUS:
//			selectedRevenue = "$500M+";
//			break;
//		default:
//				fail("Not able to find the specified optional value for company revenue");
//				break;
//		}
//		return selectedRevenue;
//	}

	public CompanyRevenueEnum getRandomCompanyRevenue() {
		List<String> companyRevenueOption = Arrays.asList("_0_500K", "_500K_1M", "_1M_10M", "_10M_50M",
				"_50M_100M", "_100M_500M", "_500M_PLUS");
		return CompanyRevenueEnum.valueOf(companyRevenueOption.get(RandomUtils.nextInt(0, companyRevenueOption.size())));
	}
	
	public String toCompanySizeValue(CompanySizeEnum companySize) {
		String selectedSize = null;
		switch (companySize) {
		case SIZE_1_50:
			selectedSize = "1-50";
			break;
		case SIZE_51_100:
			selectedSize = "51-100";
			break;
		case SIZE_101_250:
			selectedSize = "101-250";
			break;
		case SIZE_251_500:
			selectedSize = "251-500";
			break;
		case SIZE_501_1000:
			selectedSize = "501-1,000";
			break;
		case SIZE_1001_5000:
			selectedSize = "1,001-5,000";
			break;
		case SIZE_5001_10000:
			selectedSize = "5,001-10,000";
			break;
		case SIZE_10001_PLUS:
			selectedSize = "10,001+";
			break;
		default:
				fail("Not able to find the specified optional value for company size");
				break;
		}
		return selectedSize;
	}
	
	public CompanySizeEnum getRandomCompanySize() {
		List<String> companySizeOption = Arrays.asList("SIZE_1_50", "SIZE_51_100", "SIZE_101_250", "SIZE_251_500",
				"SIZE_501_1000", "SIZE_1001_5000", "SIZE_5001_10000", "SIZE_10001_PLUS");
		return CompanySizeEnum.valueOf(companySizeOption.get(RandomUtils.nextInt(0, companySizeOption.size())));
	}
	
	
	public String toIndustryValue(IndustryEnum industry) {
		String selectedIndustry = null;
		switch (industry) {
		case AGRICULTURE_FORESTRY_FISHING_AND_HUNTING:
			selectedIndustry = "Agriculture, Forestry, Fishing and Hunting";
			break;
		case MINING:
			selectedIndustry = "Mining";
			break;
		case UTILITIES:
			selectedIndustry = "Utilities";
			break;
		case CONSTRUCTION:
			selectedIndustry = "Construction";
			break;
		case MANUFACTURING:
			selectedIndustry = "Manufacturing";
			break;
		case WHOLESALE_TRADE:
			selectedIndustry = "Wholesale Trade";
			break;
		case RETAIL_TRADE:
			selectedIndustry = "Retail Trade";
			break;
		case TRANSPORTATION_AND_WAREHOUSING:
			selectedIndustry = "Transportation and Warehousing";
			break;
		case INFORMATION:
			selectedIndustry = "Information";
			break;
		case FINANCE_AND_INSURANCE:
			selectedIndustry = "Finance and Insurance";
			break;
		case REAL_ESTATE_RENTAL_AND_LEASING:
			selectedIndustry = "Real Estate Rental and Leasing";
			break;
		case PROFESSIONAL_SCIENTIFIC_AND_TECHNICAL_SERVICES:
			selectedIndustry = "Professional, Scientific, and Technical Services";
			break;
		case MANAGEMENT_OF_COMPANIES_AND_ENTERPRISES:
			selectedIndustry = "Management of Companies and Enterprises";
			break;
		case ADMINISTRATIVE_AND_SUPPORT_AND_WASTE_MANAGEMENT_AND_REMEDIATION_SERVICES:
			selectedIndustry = "Administrative and Support and Waste Management and Remediation Services";
			break;
		case EDUCATIONAL_SERVICES:
			selectedIndustry = "Educational Services";
			break;
		case HEALTH_CARE_AND_SOCIAL_ASSISTANCE:
			selectedIndustry = "Health Care and Social Assistance";
			break;
		case ARTS_ENTERTAINMENT_AND_RECREATION:
			selectedIndustry = "Arts, Entertainment, and Recreation";
			break;
		case ACCOMMODATION_AND_FOOD_SERVICES:
			selectedIndustry = "Accommodation and Food Services";
			break;
		case OTHER_SERVICES_EXCEPT_PUBLIC_ADMINISTRATION:
			selectedIndustry = "Other Services (except Public Administration)";
			break;
		case PUBLIC_ADMINISTRATION:
			selectedIndustry = "Public Administration";
			break;
		default:
				fail("Not able to find the specified optional value for industry");
				break;
		}
		return selectedIndustry;
	}
	
	public IndustryEnum getRandomIndustry() {
		List<String> industryOption = Arrays.asList("AGRICULTURE_FORESTRY_FISHING_AND_HUNTING", 
				"MINING",
				"UTILITIES",
				"CONSTRUCTION",
				"MANUFACTURING", 
				"WHOLESALE_TRADE", 
				"RETAIL_TRADE",
				"TRANSPORTATION_AND_WAREHOUSING", 
				"INFORMATION",
				"FINANCE_AND_INSURANCE",
				"REAL_ESTATE_RENTAL_AND_LEASING", 
				"PROFESSIONAL_SCIENTIFIC_AND_TECHNICAL_SERVICES", 
				"MANAGEMENT_OF_COMPANIES_AND_ENTERPRISES", 
				"ADMINISTRATIVE_AND_SUPPORT_AND_WASTE_MANAGEMENT_AND_REMEDIATION_SERVICES",
				"EDUCATIONAL_SERVICES", 
				"HEALTH_CARE_AND_SOCIAL_ASSISTANCE", 
				"ARTS_ENTERTAINMENT_AND_RECREATION", 
				"ACCOMMODATION_AND_FOOD_SERVICES", 
				"OTHER_SERVICES_EXCEPT_PUBLIC_ADMINISTRATION",
				"PUBLIC_ADMINISTRATION");
		return IndustryEnum.valueOf(industryOption.get(RandomUtils.nextInt(0, industryOption.size())));
	}
	
	
	public String toReasonForRegistrationValue(ReasonForRegistrationEnum reasonForRegistration) {
		String selectedReason = null;
		switch (reasonForRegistration) {
		case ONE_SPECIFIC_SOLICITATION:
			selectedReason = "One Specific Solicitation - Registration is Required by Government Agency to Respond";
			break;
		case GOVERNMENT_AGENCY:
			selectedReason = "Government Agency - Registration is Required to View and Respond to all Solicitations";
			break;
		case NEW_TO_GOVERNMENT_SELLING:
			selectedReason = "New to Government Selling - Just Exploring What Opportunities are Out there for my Business";
			break;
		case GOVERNMENT_SELLING_IS_STRATEGIC_TO_MY_BUSINESS_GROWTH:
			selectedReason = "Government Selling is Strategic to My Business Growth - Looking for Opportunities at a State, Regional or National Level";
			break;
		default:
				fail("Not able to find the specified optional value for ReasonForRegistration");
				break;
		}
		return selectedReason;
	}
	
//	public String toReasonForRegistrationValueForMasonry(ReasonForRegistrationEnum reasonForRegistration) {
//		String selectedReason = null;
//		switch (reasonForRegistration) {
//		case ONE_SPECIFIC_SOLICITATION:
//			selectedReason = "One Specific Solicitation - Registration is Required by Government Agency to Respond";
//			break;
//		case GOVERNMENT_AGENCY:
//			selectedReason = "Government Agency Registration is Required to View and Respond to all Solicitations";
//			break;
//		case NEW_TO_GOVERNMENT_SELLING:
//			selectedReason = "New to Government Selling - Just Exploring What Opportunities are Out there for my Business";
//			break;
//		case GOVERNMENT_SELLING_IS_STRATEGIC_TO_MY_BUSINESS_GROWTH:
//			selectedReason = "Government Selling is Strategic to My Business Growth Looking for Opportunities at a State, Regional or National Level";
//			break;
//		default:
//				fail("Not able to find the specified optional value for ReasonForRegistration");
//				break;
//		}
//		return selectedReason;
//	}
//	
	
	public ReasonForRegistrationEnum getRandomReasonForRegistration() {
		List<String> reasonForRegistrationOption = Arrays.asList("ONE_SPECIFIC_SOLICITATION", 
				"GOVERNMENT_AGENCY",
				"NEW_TO_GOVERNMENT_SELLING",
				"GOVERNMENT_SELLING_IS_STRATEGIC_TO_MY_BUSINESS_GROWTH");
		return ReasonForRegistrationEnum.valueOf(reasonForRegistrationOption.get(RandomUtils.nextInt(0, reasonForRegistrationOption.size())));
	}
	
}

