package pages.bidSyncPro.supplier.dashboard.filterResultsSection;

import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.common.helpers.GlobalVariables;


/***
 * <h1>Class FilterResultsSectionSavePopup</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects related to the <br>
 *             Filter Results section's Save Popup that appears off of the Supplier Home page</p>
 * <p> Note: This is the new class that will replace the Filter Results Save Popup class</p>
 */
public class FilterResultsSavePopupNewClass extends FilterResultsSectionNewClass {
	
	// Other
	private final String PlaceholderMessage         = "Name your search";

	// xpaths
	// popup
	private final String strPopupPath               = "//mat-dialog-container";
	private final String saveSearchTitle_xpath      = strPopupPath + "//mat-card-title[contains(text(), 'Save Search')]";
	private final String searchNameInput_xpath      = strPopupPath + "//input[@name='searchName']";
	private final String underline_xpath            = strPopupPath + "//div[contains(@class,'mat-form-field-underline')]";
	private final String okBtn_xpath                = strPopupPath + "//button[contains(@class,'mat-button') and contains(span, 'Ok')]";
	private final String noThanksBtn_xpath          = strPopupPath + "//button[contains(@class,'mat-button') and contains(span, 'No Thanks')]";
	private final String closeBtn_xpath             = strPopupPath + "//button[contains(.,'close')]";
	
	
	// validation
	private final String chooseUniqueNameTxt_xpath  = strPopupPath + "//mat-error[@id='searchErrorRepeat'][contains(span, 'Please choose a unique name')]";
	private final String incorrectNameTxt_xpath     = strPopupPath + "//mat-error[@id='searchError'][contains(span, 'Search name must be between 2 & 10 letters.')]";
	
	public FilterResultsSavePopupNewClass (EventFiringWebDriver driver){
		super(driver);
	}
	
	/* ----- getters ----- */
	// popup
	private WebElement getSaveSearchTitle() { return findByVisibility(By.xpath(this.saveSearchTitle_xpath)); }
	private WebElement getSearchNameInput() { return findByVisibility(By.xpath(this.searchNameInput_xpath)); }
	private WebElement getUnderline()       { return findByVisibility(By.xpath(this.underline_xpath));       }
	private WebElement getOkBtn()           { return findByVisibility(By.xpath(this.okBtn_xpath));           }
	private WebElement getNoThanksBtn()     { return findByVisibility(By.xpath(this.noThanksBtn_xpath));     }
	private WebElement getCloseBtn()        { return findByVisibility(By.xpath(this.closeBtn_xpath));        }

	// validation
	private WebElement getChooseUniqueNameTxt() { return findByVisibility(By.xpath(this.chooseUniqueNameTxt_xpath)); }
	private WebElement getIncorrectNameTxt()    { return findByVisibility(By.xpath(this.incorrectNameTxt_xpath));    }
	
	/* ----- methods ----- */
	
	/***
	 * <h1>clickOkBtn</h1>
	 * <p>purpose: Click OK on the Saved Searches popup to (hopefully) save out the search</p>
	 * @return FilterResultsSavePopupNewClass
	 */
	public FilterResultsSavePopupNewClass clickOkBtn() {
		this.getOkBtn().click();
		return this;
	}

	/***
	 * <h1>clickNoThanksBtn</h1>
	 * <p>purpose: Click No Thanks button on the Saved Searches popup to cancel saving the search</p>
	 * @return FilterResultsSavePopupNewClass 
	 */
	public FilterResultsSavePopupNewClass clickNoThanksBtn() {
		this.getNoThanksBtn().click();
		return this;
	}
	
	/***
	 * <h1>enterSearchName</h1>
	 * <p>purpose: enter the name of your search into the Save Search popup</p>
	 * @param strSearchName = name of search
	 * @return FilterResultSavePopupNewClass
	 */
	public FilterResultsSavePopupNewClass enterSearchName(String strSearchName) {

		// Simulate typing into the input
		for (char c : strSearchName.toCharArray()) {
			this.getSearchNameInput().sendKeys(String.valueOf(c));
		}

		return this;
	}

	/***
	 * <h1>saveSearch</h1>
	 * <p>purpose: enter the name of your search into the Save Search popup<br>
	 * 	Click Ok to save the search. Wait for confirmation popup to hide, and for<br>
	 * 	the black status message "<searchName> search saved" to appear and then disappear</p>
	 * @param strSearchName = name of search
	 * @return DashboardCommon
	 */
	public DashboardCommon saveSearch(String strSearchName) {

		this.verifyFilterResultsSaveSearchPopupCorrectlyLoaded()
			.enterSearchName(strSearchName)
			.clickOkBtn()
			.waitForStatusMessageToAppear()
			.waitForStatusMessageToDisappear();

		return this; 
	}
	
	/* ----- Verifications ----- */
	
	/***
	 * <h1>verifyCannotSaveSearchDueToBlankName</p>
	 * <p>purpose: Verify that the "Save Search" will not display the "OK" button<br>
	 * 	when the user tries to save a search with without a name. <br>
	 * 	Test will fail, if "OK" button is present"</p>
	 * @return FilterResultsSavePopupNewClass 
	 */	
	public FilterResultsSavePopupNewClass verifyCannotSaveSearchDueToBlankName() {

		// Verify ok button is not enabled
		if (this.getOkBtn().isEnabled()) {
			fail("Test Failed: \"OK\" button is enabled when user tried to save a blank name");
		}
		
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"OK\" button is not enabled when no name is given in popup");
		return this;
	}
	
	/***
	 * <h1>verifyCannotSaveSearchDueToIncorrectNameLength</h>
	 * <p>purpose: Verify that the "Search name must be between 2 & 10 letters."<br>
	 * 	validation text is displayed. Fail test if validation is not displayed</p>
	 * @return FilterResultsSavePopupNewClass
	 */
	public FilterResultsSavePopupNewClass verifyCannotSaveSearchDueToIncorrectNameLength() {
		
		this.getIncorrectNameTxt();

		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that validation was triggered for blank name in Saved Searches popup");
		return this;
		
	}

	/***
	 * <h1>verifyCannotSaveSearchDueToDuplicateName</h1>
	 * <p>purpose: Verify that the "Save Search" displays an error message of<br>
	 * 	"Please choose a unique name" when the user tries to save a search with duplicate name. <br>
	 * 	Also verify that the Save Search popup is still visible with all its page objects.<br>
	 * 	Test will fail, if an expected page object did not load or the error message isn't displayed</p>
	 * @return FilterResultsSavePopupNewClass 
	 */	
	public FilterResultsSavePopupNewClass verifyCannotSaveSearchDueToDuplicateName() {
		// Verify error text is displaying
		this.getChooseUniqueNameTxt();

//		// Make sure that we're not highlighting the "OK" button
//		// or it will fail the verification below
//		Actions builder = new Actions(driver);   
//		builder.moveToElement(this.getSearchNameInput(), 0, 60).click();
//
//		// And that we're still seeing the entire popup
//		this.verifyFilterResultsSaveSearchPopupCorrectlyLoaded();
		
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that validation was triggered for duplicated name in Saved Searches popup");
		return this;
	}
	
	/***
	 * <h1>verifyFilterResulsSaveSearchPopupCorrectlyLoaded</p>
	 * <p>purpose: Verify that the "Save Search" popup has loaded per:<br>
	 *	1. All page objects<br>
	 *	2. Default placeholder messages<br> 
	 * 	Test will fail, if any of the above are not met</p>
	 * @return FilterResultsSavePopupNewClass 
	 */
	public FilterResultsSavePopupNewClass verifyFilterResultsSaveSearchPopupCorrectlyLoaded() {
		// All the elements that should be present with the Popup loads
		this.getSaveSearchTitle();
		this.getSearchNameInput();
		this.getUnderline();
		this.getNoThanksBtn();
		this.getOkBtn();
		this.getCloseBtn();
		
		// Placeholder text should be correct
		if (!this.getSearchNameInput().getAttribute("placeholder").equals(PlaceholderMessage)) {
			fail("Test Failed: Input for Saved Search popup does not display expected placehold message \"" + this.PlaceholderMessage + "\" on initialization"); }

		return this;
	}

}
