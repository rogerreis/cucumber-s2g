@must @notifications @level2
Feature: Daily Bid Notifications > Bid Obfuscation
  BIDSYNC-607: Not obfuscating links to bids my user shouldn't access via notification email

  Background: Open newest bid notification email and view
    Given I have logged into the Supplier side as user "Basic"
    And I trigger nightly email for user "Basic"
    And I have retrieved the newest bid notification email for user "Basic"

  @BIDSYNC-607
  Scenario: Bid in Obfuscated Section (Non-Subscribed Bid) should displays per:
    1. "View Bid" links to manage subscriptions
    2. Bid Title links to manage subscriptions
    3. Bid Description should be blurred out
    4. Star Button should not be visible
    5. Subscribe Button should be visible

    Then I will verify that each non-subscribed bid is correctly obfuscated in the bid notification email

  @BIDSYNC-607
  Scenario: Bid in Non-Obfuscated Section (Subscribed Bid) should displays per:
    1. "View Bid" links to bid
    2. Bid Title links to bid
    3. Bid Description not should be blurred out
    4. Star Button should be visible
    5. Subscribe Button should not be visible

    Then I will verify that each subscribed bid is correctly displayed in the bid notification email
