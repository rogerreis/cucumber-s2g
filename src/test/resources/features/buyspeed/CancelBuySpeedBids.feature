@should @future_test
Feature: Integrations > Buyspeed - Canceling BuySpeed Bids Updates ARGO
  dmidura: Canceled bso bids are broken per ARGO-2336 which will not be completed until phase 2
	ARGO-2148: "Canceled Bids"
	
Scenario: Fields for Canceled BuySpeed Bids should update in BidSync Pro per:
	1. The status of the bid should be changed to canceled
	2. A message should be displayed on the bid detail page indicating that the bid has been canceled.
	3. Bid End Date should be updated to the date that the bid was canceled

	Given I have canceled a newly cloned bid in BuySpeed
	When I locate the canceled BuySpeed bid in BidSync Pro
	Then I will verify that the Bid End Date of the canceled BuySpeed bid displays as the time of cancellation
	And I will verify that the Bid Details page correctly updates per the canceled BuySpeed bid


# Testing Note: The canceled bids the test refers to are bids that were canceled 
# from tests run BEFORE today's run. This means that this test will fail, should we ever clear out our data.
# Testing Note: UserForCanceledBuySpeedBids has default keywords set to match the bid titles of the canceled bso bids
Scenario: Canceled bids should not display as active bids

	Given I have logged into the Supplier side as user "UserForCanceledBuySpeedBids"
	When I view bids in the New For You bid list
	Then I will not see any of the canceled BuySpeed bids, excepting BuySpeed bids canceled today in the New For You bid list


# Testing Note: The canceled BuySpeed bids the test refers to are bids that were canceled 
# from tests run BEFORE today's run. This means that this test will fail, should we ever clear out our data.
Scenario Outline: Canceled bids are searchable when filtering or searching for expired/past bids
	
	Given I have logged into the Supplier side as user "UserForCanceledBuySpeedBids"
	When I run a Filter Results search for canceled BuySpeed bids based on keyword with timeframe "<timeFrame>"
	Then I will verify that I see canceled BuySpeed bid results in the All Bids list per my search
	
	Examples: Time frames
	| timeFrame     |
	| Past 6 Months |
	| Past Year     |
	| Past 3 Years  |