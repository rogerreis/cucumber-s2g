@should @BidSync_PublicContracts @integrations @level3
Feature: Company Settings > Agency Interaction - Punchout to Classic Company Contracts Page
  ARGO-1241
  mcarvajal: This test expects state specific to the jlaraxyz account, and jlaraxyz is defunct.
  This test needs to be reworked to use a working user.  Marking as @should until then.

  Scenario: Public Contracts Tab/Page Initializes All Fields as Expected
    Given I have logged into the Supplier side as user "jlaraxyz@mailinator.com"
    And I have navigated to Company Settings > "Agency Interaction"
    And I click on the Agency Interaction Search Public Contracts Tab
    And I will verify that the Public Contracts Page has required criteria fields
    And I will verify that the Public Contracts Page has required Results table headers
    And I will verify that the Public Contracts Page Search returns Empty Results
    And I will verify that the Public Contracts Page Search returns Results for "Galactic Empire"
    And I click the Public Contracts Page Search Results Contract for "Galactic Empire"
    
    And I have navigated to the Public Contracts Agency Page for "Galactic Empire"
    
    And I will verify that the Public Contracts Agency Page has required criteria fields
    
