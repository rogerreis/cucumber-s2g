@must @ARGO-1110 @administration @level3
Feature: Account > My Info - My Profile Tab and Update User Profile Dialog
	ARGO-1110: My Profile Tab - Profile Information 

# ---------- Update User Profile Dialog ----------------#

Scenario: User can close Update User Profile Dialog

	Given I have logged into the Supplier side as user "Basic"
	When I click on the close button on the Update User Profile Dialog
	Then I will verify that the Update User Profile Dialog has closed


Scenario Outline: Verify phone number can not be empty; from the Update User Profile Dialog
	Given I have logged into the Supplier side as user "Basic"
	When I edit and save my user profile information on the Update User Profile Dialog per:
		| First Name       | <First Name>       |
		| Last Name        | <Last Name>        |
		| Address Line 1   | <Address Line 1>   |
		| Address Line 2   | <Address Line 2>   |
		| Country          | <Country>          |
		| State            | <State>            |
		| Zip              | <Zip>              |
		| City             | <City>             |
		| Job Title        | <Job Title>        |
		| Phone Number     | <Phone Number>     |
		| Phone Ext        | <Phone Ext>        |
		| Fax Number       | <Fax Number>       |
		| Fax Ext          | <Fax Ext>          |
	Then verify phone number validation error on screen
	
Examples: Some Profile Information
	| First Name | Last Name | Address Line 1 | Address Line 2 | Country                   | State    | Zip   | City    | Job Title | Phone Number | Phone Ext | Fax Number | Fax Ext |
	| Test       | User      | 123 Congress   | Suite 1400     | Canada                    | Alberta  | T1Y2M9| Calgary | CEO       |              | 3         | 5122883121 | 3       |
	
				
Scenario Outline: From the Update User Profile Dialog, user can edit profile information per:
	1. Full Name
	2. Occupation
	3. Email
	4. User Address
	5. Phone Number
	6. Fax
	and these updates will display on the My Profile tab

	Given I have logged into the Supplier side as user "Basic"
	When I edit and save my user profile information on the Update User Profile Dialog per:
		| First Name       | <First Name>       |
		| Last Name        | <Last Name>        |
		| Address Line 1   | <Address Line 1>   |
		| Address Line 2   | <Address Line 2>   |
		| Country          | <Country>          |
		| State            | <State>            |
		| Zip              | <Zip>              |
		| City             | <City>             |
		| Job Title        | <Job Title>        |
		| Phone Number     | <Phone Number>     |
		| Phone Ext        | <Phone Ext>        |
		| Fax Number       | <Fax Number>       |
		| Fax Ext          | <Fax Ext>          |
	Then I will verify that the Profile Information tab updates with my profile information per:
		| First Name       | <First Name>       |
		| Last Name        | <Last Name>        |
		| Address Line 1   | <Address Line 1>   |
		| Address Line 2   | <Address Line 2>   |
		| Country          | <Country>          |
		| State            | <State>            |
		| Zip              | <Zip>              |
		| City             | <City>             |
		| Job Title        | <Job Title>        |
		| Phone Number     | <Phone Number>     |
		| Phone Ext        | <Phone Ext>        |
		| Fax Number       | <Fax Number>       |
		| Fax Ext          | <Fax Ext>          |
	
	Examples: Some Profile Information
	| First Name | Last Name | Address Line 1 | Address Line 2 | Country                   | State    | Zip   | City    | Job Title | Phone Number | Phone Ext | Fax Number | Fax Ext |
	| Test       | User      | 123 Congress   | Suite 1400     | Canada                    | Alberta  | T1Y2M9| Calgary | CEO       | 5122883728   | 3         | 5122883121 | 3       |
	| !Test      | !User     | 321 Congress   | Suite 0041     | United States of America  | Texas    | 78701 | Austin  | GI        | 5122888888   | 0         | 5122883333 | 0       |
	

Scenario Outline: From the Update User Profile Dialog, CANCEL will cancel edits to the profile information

	Given I have logged into the Supplier side as user "Basic"
	When I edit and cancel my user profile information on the Update Profile Dialog per:
		| First Name       | <First Name>       |
		| Last Name        | <Last Name>        |
		| Address Line 1   | <Address Line 1>   |
		| Address Line 2   | <Address Line 2>   |
		| Country          | <Country>          |
		| State            | <State>            |
		| Zip              | <Zip>              |
		| City             | <City>             |
		| Job Title        | <Job Title>        |
		| Phone Number     | <Phone Number>     |
		| Phone Ext        | <Phone Ext>        |
		| Fax Number       | <Fax Number>       |
		| Fax Ext          | <Fax Ext>          |
	Then I will verify that the Profile Information tab does not update with the cancelled profile information per:
		| First Name       | <First Name>       |
		| Last Name        | <Last Name>        |
		| Address Line 1   | <Address Line 1>   |
		| Address Line 2   | <Address Line 2>   |
		| Country          | <Country>          |
		| State            | <State>            |
		| Zip              | <Zip>              |
		| City             | <City>             |
		| Job Title        | <Job Title>        |
		| Phone Number     | <Phone Number>     |
		| Phone Ext        | <Phone Ext>        |
		| Fax Number       | <Fax Number>       |
		| Fax Ext          | <Fax Ext>          |

	Examples: Junk Profile Information (should not match any parts of profile that would be currently visible on My Profile tab)
	| First Name | Last Name | Address Line 1 | Address Line 2 | Country | State          | Zip    | City  | Job Title | Phone Number | Phone Ext | Fax Number | Fax Ext |
	| Junk1      | Junk2     | Junk3          | Junk4          | Canada  | Nova Scotia    | B3H3E2 | Junk5 | Junk6     | 5122880000   | 9         | 5122880000 | 9       |

	
Scenario: Clicking on "To change your email visit the my account tab" on the Update User Profile Dialog navigates user to my account tab

	Given I have logged into the Supplier side as user "Basic"
	When I try to edit my email from the Update User Profile Dialog
	Then I will be navigated from the My Profile tab to the My Accounts tab


# ---------- Current Company ----------------#

Scenario: From the User Profile Dialog, user can view Current Company Information per:
	1. Current Company Name
	2. Company Primary Address

	Given I have logged into the Supplier side as user "Basic"
	When I view the Current Company screen on the Update User Profile Dialog
	Then I will verify that Current Company fields are displaying
  
  
Scenario: Cancel of Current Company information will close User Profile Dialog

	Given I have logged into the Supplier side as user "Basic"
	When I click cancel on the Current Company screen on the Update User Profile Dialog
	Then I will verify that the Update User Profile Dialog has closed
  
  
Scenario: Close of Current Company information will close User Profile Dialog

	Given I have logged into the Supplier side as user "Basic"
	When I click close on the Current Company screen on the Update User Profile Dialog
	Then I will verify that the Update User Profile Dialog has closed


# ---------- Company Lookup ----------------#
Scenario: From the User Profile Dialog, user can access the Company Lookup

	Given I have logged into the Supplier side as user "Basic"
	When I try to join a new company from the User Profile Dialog
	Then I will verify that I am accessing the Company Lookup on the Change Company screen
	

