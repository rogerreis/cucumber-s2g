package pages.bidSyncPro.supplier.emails;

//import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.HelperMethods;

public class PwdResetEmail extends GenericEmail {
	
	public HelperMethods helperMethods;
	
	// Page Elements
	public Map<String, WebElement> PageElements;
	
	
	// Other
	EventFiringWebDriver driver;
	
	public PwdResetEmail(EventFiringWebDriver driver) throws Throwable {
		super(driver, "PwdResetEmail.json");
		this.driver = driver;
		PageElements  = new HashMap <String, WebElement>();
	}
	
}