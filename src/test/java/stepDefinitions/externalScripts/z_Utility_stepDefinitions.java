package stepDefinitions.externalScripts;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.Given;
import pages.common.helpers.GlobalVariables;

import org.junit.Assert;
import utility.database.SQLConnector;
import java.sql.ResultSet;
import java.sql.SQLException;

public class z_Utility_stepDefinitions {

	// Depending on the query you enter, you will get a certain number of columns
	// returned.
	// This will search for a term in a specific column
	@Then("^the query \"([^\"]*)\" against database \"([^\"]*)\" will contain \"([^\"]*)\" in column (\\d+)$")
	public void theQueryAgainstDatabaseWillContainInColumn(String strQuery, String strDBname,
			String strExpectedResponse, int nColumn) {
        
        try {
            ResultSet rs = SQLConnector.executeQuery(strDBname, strQuery);
            // While Loop to iterate through all data and print results
            while (rs.next()) {
                String strResultSet = rs.getString(nColumn);
                System.out.printf(GlobalVariables.getTestID() + " INFO: Database results: %s\n", strResultSet);
                Assert.assertTrue(strResultSet.contains(strExpectedResponse));
            }
        } catch (SQLException | ClassNotFoundException e) {
            Assert.fail("Problem accessing the database.");
            e.printStackTrace();
        }
    }

	@Given("^I seed the global variable \"([^\"]*)\" with value \"([^\"]*)\"$")
	public void seedTheGlobalVariableWithValue(String variable_name, String value) {
		GlobalVariables.storeObject(variable_name, value);
	}

	// make sure you format the query to give a single field
	// example: select cleaned_detail from solicitation where detail similar to
	// '%(bid_document)%' AND date_created > current_date - interval '1' day AND
	// process_detail = 'Solicitation is processed and sent to survivor' limit 1
	@Given("^I have collected the result for query \"([^\"]*)\" against database \"([^\"]*)\" and put it in a global variable named \"([^\"]*)\"$")
	public void iHaveCollectedTheResultForQueryAgainstDatabaseAndPutItInAGlobalVariableNamed(String strQuery,
			String strDBname, String globalVariableName) throws Throwable {

		ResultSet rs = SQLConnector.executeQuery(strDBname, strQuery);
		// While Loop to iterate through all data and print results
		while (rs.next()) {
			String strResultSet = rs.getString(1);
			System.out.printf(GlobalVariables.getTestID() + " INFO: Database results: %s\n", strResultSet);
			GlobalVariables.storeObject(globalVariableName, strResultSet);
		}
	}
	
}