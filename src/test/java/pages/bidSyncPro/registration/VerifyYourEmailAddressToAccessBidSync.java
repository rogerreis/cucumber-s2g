package pages.bidSyncPro.registration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.common.BidSyncProCommon;
import pages.common.helpers.GlobalVariables;

/***
 * <h1>Class VerifyYourEmailAddressToAccessBidSync</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects related to the objects on the "Verify your email address to access BidSync" screen<br>
 *             that is part of supplier registration</p>
 */
public class VerifyYourEmailAddressToAccessBidSync extends BidSyncProCommon {

	// xpaths
	private final String verifyYourEmailTxt_xpath   = "h1/span[@id='emailStep3VerifyText'][contains(.,'Verify your email address to access Bidsync')]";
	private final String afterVerificationTxt_xpath = "//h2/span[@id='emailVerifiedRedirectedText'][contains(.,'After verification you will be redirected')]";
	private final String checkingTokenTxt_xpath     = "//h2[contains(span, 'Checking Token')]";

	public VerifyYourEmailAddressToAccessBidSync (EventFiringWebDriver driver) {
		super(driver);
	}
	
	/* ----- getters ----- */
	
	private WebElement getVerifyYourEmailTxt()   { return findByVisibility(By.xpath(this.verifyYourEmailTxt_xpath));   }
	private WebElement getAfterVerificationTxt() { return findByVisibility(By.xpath(this.afterVerificationTxt_xpath)); }
	private WebElement getCheckingTokenTxt()     { return findByPresence(By.xpath(this.checkingTokenTxt_xpath));       }
	
	/* ----- methods ----- */
	
	/***
	 * <h1>verifyPageInitialization</h1>
	 * <p>purpose: Verify that the "Verify Your Email To Access BidSync" screen has correctly initialized per<b>
	 * 	1. Text displayed on screen</p>
	 * @return VerifyYourEmailAddressToAccessBidSync
	 */
	public VerifyYourEmailAddressToAccessBidSync verifyPageInitialization() {
		this.getAfterVerificationTxt();
		this.getCheckingTokenTxt();
		this.getVerifyYourEmailTxt();
		
		System.out.println(GlobalVariables.getTestID() + " Verified page initialization");
		return this;
	}
		
}
