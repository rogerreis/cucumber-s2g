package stepDefinitions.bidSyncPro.registration;

import core.CoreAutomation;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.bidSyncPro.registration.CreateNewCompany;
import pages.common.helpers.GlobalVariables;
import org.junit.Assert;

import org.openqa.selenium.support.events.EventFiringWebDriver;

public class CreateNewCompany_stepDefinitions {
	
	private EventFiringWebDriver driver;
	private CreateNewCompany createNewCompany;
	
	public CreateNewCompany_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
		createNewCompany = new CreateNewCompany(driver);
	}

	@And ("^I will verify that the Create New Company address fields are editable$")
	public void I_will_verify_that_the_Create_New_Company_address_fields_are_editable() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying address fields are editable on \"Create New Company\" screen");
		new CreateNewCompany(driver)
			.verifyAddressFieldsEditable();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Verified address fields are editable on \"Create New Company\" screen");
	}

	@And ("^I will verify that the Create New Company company name field is not editable$")
	public void I_will_verify_that_the_Create_New_Company_company_name_field_is_not_editable() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying Company Name field is not editable on \"Create New Company\" screen");

		// TODO dmidura: Workaround for BIDSYNC-318: Company Name field should be editable when creating new company during registration
		// uncomment when ticket is complete
//		new CreateNewCompany(driver)
//			.verifyCompanyNameNotEditable();
		new CreateNewCompany(driver).clickNextButton();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Verified Company Name field is not editable on \"Create New Company\" screen");
	}

	@And ("^I will verify that the Create New Company screen's company added page$")
	public void I_will_verify_that_the_Create_New_Company_screen_company_added_page() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying Company added page on \"Create New Company\" screen");
		new CreateNewCompany(driver).clickNextButton2();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Verified Company Name field is not editable on \"Create New Company\" screen");
	}
	
	@Then ("I will verify that the Create New Company screen correctly populates with my user registration address and company name$")
	public void I_will_verify_that_the_Create_New_Company_screen_correctly_populates_with_my_user_registration_address_and_company_name() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying user data displays on \"Create New Company\" screen");

		RandomChargebeeUser userData = GlobalVariables.getStoredObject("StoredUser");
		new CreateNewCompany(driver)
			.verifyPageInitialization()
			.verifyUserDataDisplays(userData);

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Verified user registration data is populating fields on \"Create New Company\" screen");
	}
	
	@Then("^verify state dropdown is editable$")
	public void verify_state_dropdown_is_editable(){
		try{
			createNewCompany.verifyStateCanBeEditable();
		}catch(Exception exp){
			Assert.fail("State dropdpwn is not editable");
		}
	}
}