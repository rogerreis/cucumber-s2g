package pages.bidSyncPro.supplier.account.myInfo;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import data.bidSyncPro.registration.RandomCompanyInformation;
import pages.common.pageObjects.BasePageActions;
import pages.common.pageObjects.MatSelect;

class ChangeCompanyDialog extends BasePageActions {
    
    private EventFiringWebDriver driver;
    
    // ids
    private final String companyNameInput_id  = "companyName";
    private final String countryDropdown_id   = "companyCountry";
    private final String stateDropdown_id     = "companyState";
    private final String cityInput_id         = "city";
    private final String zipCodeInput_id      = "postal";
    private final String addressLine1Input_id = "address";
    private final String addressLine2Input_id = "address2";
    private final String backButton_id        = "createBbackButton";
    private final String saveButton_id        = "CreateNextButton";
    
    ChangeCompanyDialog(EventFiringWebDriver driver) {
    	super(driver);
        this.driver = driver;
    }
    
    /* ----- getters ----- */
    private WebElement getCompanyNameInput()  { return findByVisibility(By.id(this.companyNameInput_id));               }
    private MatSelect getCountryDropdown()    { return new MatSelect(findByVisibility(By.id(this.countryDropdown_id))); }
    private MatSelect getStateDropdown()      { return new MatSelect (findByVisibility(By.id(this.stateDropdown_id)));  }
    private WebElement getCityInput()         { return findByVisibility(By.id(this.cityInput_id));                      }
    private WebElement getZipCodeInput()      { return findByVisibility(By.id(this.zipCodeInput_id));                   }
    private WebElement getAddressLine1Input() { return findByVisibility(By.id(this.addressLine1Input_id));              }
    private WebElement getAddressLine2Input() { return findByVisibility(By.id(this.addressLine2Input_id));              }
    private WebElement getBackButton()        { return findByVisibility(By.id(this.backButton_id));                     }
    private WebElement getSaveButton()        { return findByVisibility(By.id(this.saveButton_id));                     }
    
    /* ----- methods ----- */
    public MyInfo saveCompany(RandomCompanyInformation company) {
        getAddressLine1Input().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        getAddressLine1Input().sendKeys(company.getAddressLine1());
    
        getAddressLine2Input().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        getAddressLine2Input().sendKeys(company.getAddressLine2());
    
        new Actions(driver) // necessary to avoid an 'unable to focus element error
            .moveToElement(getCityInput())
            .click()
            .sendKeys(Keys.chord(Keys.CONTROL, "a"))
            .sendKeys(company.getCity())
            .build().perform();
    
        getZipCodeInput().sendKeys(Keys.chord(Keys.CONTROL, "a"));
        getZipCodeInput().sendKeys(company.getZipCode());
        
        getCountryDropdown().select(company.getCountry());
        
//     @Fix for release OCT 7
//        getStateDropdown().select(company.getState());
        
        getSaveButton().click();
        return new MyInfo(driver);
    }
}
