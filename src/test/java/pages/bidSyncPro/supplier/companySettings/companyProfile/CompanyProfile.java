package pages.bidSyncPro.supplier.companySettings.companyProfile;

import pages.bidSyncPro.supplier.companySettings.CompanySettingsCommon;
import pages.bidSyncPro.supplier.companySettings.manageUsers.WaitingForApproval;
import pages.common.helpers.HelperMethods;
import pages.common.helpers.RandomHelpers;

import java.util.regex.Pattern;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import data.bidSyncPro.registration.RandomChargebeeUser;
import data.bidSyncPro.registration.RandomCompanyInformation;

/***
 *	<h1>CompanyProfile</h1>
 *	<p>details: This class contains methods and page Objects to manipulate the Company Profile screen
 *	located at Company Settings > Company Profile
 */
public class CompanyProfile extends CompanySettingsCommon {
    
    // ids
    private final String companyName_id                  = "companyNameHere";
    private final String companyInformationEditButton_id = "updateCompanyProfileEditBtn";
    
    private final String addressLine1_id                 = "companyAddressLine1Content";
    private final String addressLine2_id                 = "companyAddressLine2Content";
    private final String city_id                         = "companyCityHere";
    private final String state_id                        = "companyStateHere";
    private final String typeOfIndustry_id               = "companyIndustryContent";
    private final String numberOfEmployees_id            = "numberEmployeesContent";
    private final String yearEstablished_id              = "yearEstablishedContent";
    private final String annualRevenue_id                = "annualRevenueContent";
    private final String FEIN_id                         = "fedInNbrContent";
    private final String SSN_id                          = "ssnContent";
    private final String zipCode_id                      = "zipContent";
    private final String save_id                      	 = "save";
    private final String companyProfileLink_xpath		 = "//span[contains(text(),'Update Company Profile')]";
    private final String FEIN_CompanyProfile_xpath		 =" //input[@id='fedIdNbr']";
    private final String saveButton_CompanyProfile_id	 ="saveButton";
    public CompanyProfile(EventFiringWebDriver driver) {
        super(driver);
    }
    
    /* ----- getters ----- */
    
    private WebElement getCompanyInformationEditButton() { return findByVisibility(By.id(companyInformationEditButton_id)); }
    private WebElement getAddressLine1()                 { return findByVisibility(By.id(this.addressLine1_id));            }
    private WebElement getAddressLine2()                 { return findByVisibility(By.id(this.addressLine2_id));            }
    private WebElement getCity()                         { return findByVisibility(By.id(this.city_id));                    }
    private WebElement getState()                        { return findByVisibility(By.id(this.state_id));                   }
    private WebElement getTypeOfIndustry()               { return findByVisibility(By.id(this.typeOfIndustry_id));          }
    private WebElement getNumberOfEmployees()            { return findByVisibility(By.id(this.numberOfEmployees_id));       }
    private WebElement getYearEstablished()              { return findByVisibility(By.id(this.yearEstablished_id));         }
    private WebElement getAnnualRevenue()                { return findByVisibility(By.id(this.annualRevenue_id));           }
    private WebElement getFEIN()                         { return findByVisibility(By.id(this.FEIN_id));                    }
    private WebElement getSSN()                          { return findByVisibility(By.id(this.SSN_id));                     }
    private WebElement getZipCode()                      { return findByVisibility(By.id(this.zipCode_id));                 }
    public WebElement getCompanyProfileLink()			 { return findByVisibility(By.xpath(this.companyProfileLink_xpath));}
    public WebElement getFEINOnCompanyProfile()          { return findByVisibility(By.xpath(this.FEIN_CompanyProfile_xpath)); 	}
    public WebElement getSaveButtonOnCompanyProfile()	 { return findByVisibility(By.id(this.saveButton_CompanyProfile_id));}

    public String getCompanyName() {
    	Pattern pattern = Pattern.compile(".");

    	int time = DEFAULT_TIMEOUT;
    		new WebDriverWait(driver, time)
    		.withMessage("Test Failed: Company Name did not contain text after \"" + time + "\" seconds")
    		.until( ExpectedConditions.textMatches(By.id(this.companyName_id), pattern));

        return findByVisibility(By.id(this.companyName_id)).getText(); }
    
    /* ----- helpers ----- */
    
    public boolean isTheUserAllowedToViewThisPage() {
        return new WaitingForApproval(driver).areWeHere();
    }
    /* ----- methods ----- */
    
    public RandomCompanyInformation getCompanyInformation() {
        RandomCompanyInformation result = new RandomCompanyInformation();
        result.setName(getCompanyName());
        result.setAddressLine1(getAddressLine1().getText());
        result.setAddressLine2(getAddressLine2().getText());
        result.setCity(getCity().getText().trim().replace(",", ""));
        result.setFEIN(getFEIN().getText());
        result.setSSN(getSSN().getText());
        result.setState(getState().getText());
        result.setTypeOfIndustry(getTypeOfIndustry().getText());
        result.setYearEstablished(Integer.parseInt(getYearEstablished().getText()));
        result.setAnnualRevenue(Long.parseLong(getAnnualRevenue().getText()));
        result.setNumberOfEmployees(Integer.parseInt(getNumberOfEmployees().getText()));
        result.setZipCode(getZipCode().getText());
        return result;
    }
    
    public EditCompanyInformationModal fillInCompanyInformation(RandomCompanyInformation companyInformation) {
        getCompanyInformationEditButton().click();
        return new EditCompanyInformationModal(driver)
                .fillInInformation(companyInformation);
    }
    
    public CompanyProfile waitForNewInformationToPopulate(RandomCompanyInformation newCompany) {
        String xpath = String.format("//span[contains(., '%s')]", newCompany.getName());
        try {
            waitForVisibility(By.xpath(xpath), 10);
        } catch (TimeoutException e) {
            Assert.fail("Timeout: New company information did not populate.");
        }
        return this;
    }
    public CompanyProfile clickOnCompanyProfileLink(){
    	getCompanyProfileLink().click();
    	return this;
    }
    
    public CompanyProfile addCompanyFEIN(){
    	scrollIntoViewBottomOfScreen(By.id(saveButton_CompanyProfile_id));
    	getFEINOnCompanyProfile().sendKeys(new RandomHelpers().getRandomIntStringOfLength(9));
    	getSaveButtonOnCompanyProfile().click();
    	return this;
    }
    
    /* ------ verifications ------ */
    
    public void verifyCompanyAdressIsPopulated(RandomChargebeeUser user) {
    	String address = user.getAddress() + ", " + user.getCity();
    	String xpath = "//span[contains(text(),'"+ address +"')]";
    	new WebDriverWait(driver, DEFAULT_TIMEOUT)
    			.withMessage("Company address is not populated correctly")
    			.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
    }
    
    
    public void verifyCompanyNameIsPopulated(RandomChargebeeUser user) {
    	String companyName_xpath = "//span[@id='companyNameHere']/strong[contains(text(),'"+user.getCompany().getName()+"')]";
    	new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Company name is not populated correctly")
		.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(companyName_xpath)));
    }
    
    
}
