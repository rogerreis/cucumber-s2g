@must @savedSearches @search @bidSearch_detail @level3
Feature: Dashboard - Saved Searches - Saving and Retrieving Searches
  This feature file exercises the objects related to saving and retrieving searches via Filter Results on the Supplier Dashboard
  	ARGO-887: Creating/Loading Saved Searches
  	ARGO-2340: UI - When loading a saved search with time frame, time frame does not always display 


@ARGO-887
Scenario Outline: Filter Results Section Allows for Creation of Search with Specified Time Frame

    Given I have logged into the Supplier side as user "Basic"
    When I save out a search with specified time frame "<timeFrame>"
	Then I will verify that my saved search appears in the Saved Searches dropdown 
	And I will verify that my search has saved out to the database
	
	Examples: Available Time Frames
	| timeFrame         |
    | Past 6 Months     |
    | Past Year         |
    | Past 3 Years      |


@ARGO-887
Scenario: Filter Results Section Allows for Creation of Search with a Custom Time Frame

    Given I have logged into the Supplier side as user "Basic"
    When I save out a search with a custom time frame
	Then I will verify that my saved search appears in the Saved Searches dropdown 
	And I will verify that my search has saved out to the database


@ARGO-887 @ARGO-2340
Scenario: Saved Searches with Custom Time Frame Can be Selected Off Home Screen and Filter Results Collapsible Section will Correctly Display Those Saved Searches
# Note: Using searches created in "Filter Results Allows for Creation of Search with a Custom Time Frame". 
# so this test must be run AFTER that test.

    Given I have logged into the Supplier side as user "Basic"
    When I save out a search with a custom time frame
    When I select my search saved with a custom time frame from the Saved Searches dropdown
	Then I will verify that my saved search displays filters in the Filter Results section
    And I will verify that the All Bids list will display results per my retrieved search
    And I delete the search from the Saved Searches list
    

@ARGO-887 @ARGO-2340
Scenario Outline: Saved Searches can be Selected Off Home Screen and Filter Results Collapsible Section will Correctly Display Those Saved Searches
# Note: Using searches created in "Filter Results Section Allows for Creation of Search with Specified Time Frame"
# so this test must be run AFTER that test.

    Given I have logged into the Supplier side as user "Basic"
    When I save out a search with specified time frame "<timeFrame>"
    When I select a saved search with specified time frame "<timeFrame>" from the Saved Searches dropdown
	Then I will verify that my saved search displays filters in the Filter Results section
    And I will verify that the All Bids list will display results per my retrieved search
    And I delete the search from the Saved Searches list

	Examples: Available Time Frames
	| timeFrame         |
    | Past 6 Months     |
    | Past Year         |
    | Past 3 Years      |