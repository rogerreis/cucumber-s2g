package pages.bidSyncPro.supplier.dashboard.filterResultsSection;

import static org.junit.Assert.fail;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import utility.database.SQLConnector;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import io.cucumber.datatable.DataTable;
import pages.bidSyncPro.common.DatabaseCommonSearches;
import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidListRow;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.common.helpers.DateHelpers;
import pages.common.helpers.GeographyHelpers;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

/***
 * <h1>Class FilterResultsSectionNewClass</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects related to collapsible<br>
 *             Filter Results section that appears off of the Supplier dashboard pages</p>
 * <p> NOTE: This is the new class that will replace the old Filter Results Section</p>
 */
public class FilterResultsSectionNewClass extends DashboardCommon {
	
 	// xpath 
 	// Keywords Section
 	private String positiveKeywordsSectionToggleIcon_xpath       = "//mat-expansion-panel[@id='matExpKeywordPanel']//span[contains(@class, 'mat-expansion-indicator')]";
 	private final String positiveKeywordsSection_xpath           = "//mat-panel-title[@id='matKeywordPanel'][contains(text(), 'Keywords')]";
 	private final String positiveKeywordsSectionVisibility_xpath = "//mat-expansion-panel[@id='matExpKeywordPanel']//div[@role='region']";
 	
 	// Negative Keywords Section
 	private String negativeKeywordsSectionToggleIcon_xpath       = "//mat-expansion-panel[@id='matNegKeywordExpPanel']//span[contains(@class, 'mat-expansion-indicator')]";
 	private final String negativeKeywordsSection_xpath           = "//mat-panel-title[@id='matNegPanelTitle'][contains(text(), 'Negative Keywords')]";
 	private final String negativeKeywordsSectionVisibility_xpath = "//mat-expansion-panel[@id='matNegKeywordExpPanel']//div[@role='region']";
 	
 	// States/Provinces Section
 	private String statesProvincesSectionToggleIcon_xpath        = "//mat-expansion-panel[@id='matStateExpPanel']//span[contains(@class, 'mat-expansion-indicator')]";
 	private final String statesProvincesSection_xpath            = "//mat-panel-title[@id='matStatePanelTitle'][contains(text(),'States/Provinces')]";
 	private final String statesProvincesSectionVisibility_xpath  = "//mat-expansion-panel[@id='matStateExpPanel']//div[@role='region']";
 	
 	
 	// Bid End Date Section
 	private String bidEndDateSectionToggleIcon_xpath             = "//mat-expansion-panel[@id='matExpBidEndPanel']//span[contains(@class, 'mat-expansion-indicator')]";
 	private final String bidEndDateSection_xpath                 = "//mat-panel-title[@id='matBidEndPanelTitle'][contains(text(), 'Bid End Date')]";
 	private final String bidEndDateSectionVisibility_xpath       = "//mat-expansion-panel[@id='matExpBidEndPanel']//div[@role='region']";

 	// Bottom Buttons 
	private final String searchBtn_xpath      = "//button[@id='btnSearchBids'][contains(span,'SEARCH')]";
	private final String clearAllBtn_xpath    = "//button[@id='clearFiltersButton'][contains(span,'Clear All')]";
	private final String saveSearchBtn_xpath  = "//button[@id='btnSaveSearch'][contains(span,'Save Search')]";
	private final String loadDefaultBtn_xpath = "//button[@id='btnLoadDefaults'][contains(span,'Load Default')]"; 
	private final String editDefaultBtn_xpath = "//a[@id='btnEditDefaultSearch'][contains(span,'Edit Default Filters')]";
	
	// Other
	private final String pageId = "matKeywordPanel";  // id to test if the Filter Results Section has loaded

 	/*----- getters -----*/
 	// Keywords Section
	private WebElement getPositiveKeywordsSection()           { return findByScrollIntoViewBottomOfScreen(By.xpath(this.positiveKeywordsSection_xpath)); }
	private WebElement getPositiveKeywordsSectionToggleIcon() { return findByVisibility(By.xpath(this.positiveKeywordsSectionToggleIcon_xpath));         }

 	// Negative Keywords Section
	private WebElement getNegativeKeywordsSection()           { return findByScrollIntoViewBottomOfScreen(By.xpath(this.negativeKeywordsSection_xpath)); }
	private WebElement getNegativeKeywordsSectionToggleIcon() { return findByVisibility(By.xpath(this.negativeKeywordsSectionToggleIcon_xpath));         }
	
 	// States/Provinces Section
	private WebElement getStatesProvincesSection()            { return findByScrollIntoViewBottomOfScreen(By.xpath(this.statesProvincesSection_xpath));   }
	private WebElement getStatesProvincesSectionToggleIcon()  { return findByVisibility(By.xpath(this.statesProvincesSectionToggleIcon_xpath));           }
	
	
 	// Bid End Date Section
	private WebElement getBidEndDateSection(){
		findByScrollIntoViewBottomOfScreen(By.xpath(this.bidEndDateSection_xpath));
		return findByClickability(By.xpath(this.bidEndDateSection_xpath));         
	}
	private WebElement getBidEndDateSectionToggleIcon()       { return findByVisibility(By.xpath(this.bidEndDateSectionToggleIcon_xpath));                }

 	// Bottom Buttons 
	private WebElement getSearchBtn()      { return findByScrollIntoViewBottomOfScreen(By.xpath(this.searchBtn_xpath));      }
	private WebElement getClearAllBtn()    { return findByScrollIntoViewBottomOfScreen(By.xpath(this.clearAllBtn_xpath));    }
	private WebElement getSaveSearchBtn()  { return findByScrollIntoViewBottomOfScreen(By.xpath(this.saveSearchBtn_xpath));  }
	private WebElement getLoadDefaultBtn() { return findByScrollIntoViewBottomOfScreen(By.xpath(this.loadDefaultBtn_xpath)); }
	private WebElement getEditDefaultBtn() { return findByScrollIntoViewBottomOfScreen(By.xpath(this.editDefaultBtn_xpath)); }
	
	// Other
	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;
	private GeographyHelpers geographyHelpers;
	private DateHelpers dateHelpers;

	
	public FilterResultsSectionNewClass (EventFiringWebDriver driver) {
		super(driver);
		this.driver 	 = driver;
		helperMethods    = new HelperMethods();
		geographyHelpers = new GeographyHelpers();
		dateHelpers      = new DateHelpers();
	}
	
	/*----- methods ----- */
	
	/***
	 * <h1>setFilterFromDataTable</h1>
	 * <p>purpose: Set the Filter Results section via a DataTable</p>
	 * @param dTable
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass setFilterFromDataTable (DataTable dTable) {
		this.setFilter(this.putDataTableIntoFilterCriteria(dTable));
		return this;
	}
	
	/**
	 * <h1>setFilter</h1>
	 * <p>purpose: Use this method to set a filter on the Filter Results Section<br>
	 * 	After setting filter values, all sections header dropdowns will be closed,<br>
	 * 	except for the bid end date section (if bid end date was set)<br>
	 *  <br>
	 * 	Note: This method does not clear out existing values in the Filter Results section.<br>
	 * 	If you need to clear the Filter Results section, call clickClearAllBtn method before<br>
	 * 	calling the setFilter method.</p>
	 * @param filter = FilterCriteria with information about the filter
	 * @return FilterResultSection
	 */
	public FilterResultsSectionNewClass setFilter (FilterCriteria filter) {
		System.out.println(GlobalVariables.getTestID() + " INFO: Setting filter criteria for Filter Results Section");
		waitForSpinner(driver);
		// Add Keywords
		if (!filter.getKeywords().isEmpty()) {
			this.openKeywordsSection()
				.addKeywords(filter.getKeywords())
				.closeKeywordsSection();
		} else { this.closeKeywordsSection(); }

		// Add Negative Keywords
		if (!filter.getNegativeKeywords().isEmpty()) {
			this.openNegativeKeywordsSection()
				.addNegativeKeywords(filter.getNegativeKeywords())
				.closeNegativeKeywordsSection();
		}
		// States/Provinces
		if (!filter.getStatesProvinces().isEmpty()) {
			this.openStatesProvincesSection()
				.uncheckSearchAllStatesProvincesCheckbox()
				.checkAllUsStateCheckbox()
				.uncheckAllUsStateCheckbox()
				.openStatesProvincesDropdown()
				.addStatesProvinces(filter.getStatesProvinces())
				.closeStatesProvincesSection();
		}
		// Bid End Date
		if (!filter.getSelectTimeFrame().equals(TimeFrameOptions.NONE)) {
			switch (filter.getSelectTimeFrame()) {
				case PAST_6_MONTHS:
				case PAST_YEAR:
				case PAST_3_YEARS:
					this.openBidEndDateSection()
					.checkIncludePastBidsCheckbox()
					.openTimeFrameDropdown()
					.selectTimeFrame(filter.getSelectTimeFrame());
					break;
				case CUSTOM_TIME_FRAME:
					this.openBidEndDateSection()
					.checkIncludePastBidsCheckbox()
					.openTimeFrameDropdown()
					.selectCustomTimeFrame()
					.setShowBidsFromInput(filter.getBidStartDate())
					.setShowBidsToInput(filter.getBidEndDate());
					break;
				default:
					fail("ERROR: Invalid selection of " + filter.getSelectTimeFrame() + " into Time Frame dropdown");
					break;
			}
		}
		return this;
	}
	

	/***
	 * <h1>openKeywordsSection</h1>
	 * <p>purpose: Open the Keywords Section if it is not already open</p>
	 * @return KeywordsSection
	 */
	public KeywordsSection openKeywordsSection() {
		if(!this.isPositiveKeywordsSectionOpen()) { ((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getPositiveKeywordsSection()); }
		return new KeywordsSection(driver);
	}
	
	/***
	 * <h1>getKeywordsSectionAccess</h1>
	 * <p>purpose: Access to the Keywords section</p>
	 * @return KeywordsSection
	 */
	public KeywordsSection getKeywordsSectionAccess() {
		return new KeywordsSection(driver);
	}
	
	/***
	 * <h1>closeKeywordsSection</h1>
	 * <p>purpose: Close the Keywords Section if it is not already closed</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass closeKeywordsSection() {
		if(this.isPositiveKeywordsSectionOpen()) { ((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getPositiveKeywordsSection()); }
		return this;
	}

	/***
	 * <h1>openNegativeKeywordsSection</h1>
	 * <p>purpose: Open the Negative Keywords Section if it is not already open</p>
	 * @return NegativeKeywordsSection
	 */
	public NegativeKeywordsSection openNegativeKeywordsSection() {
		if(!this.isNegativeKeywordsSectionOpen()) { ((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getNegativeKeywordsSection());}
		return new NegativeKeywordsSection(driver);
	}

	/***
	 * <h1>getNegativeKeywordsSectionAccess</h1>
	 * <p>purpose: Access to the Negative Keywords section</p>
	 * @return NegativeKeywordsSection
	 */
	public NegativeKeywordsSection getNegativeKeywordsSectionAccess() {
		return new NegativeKeywordsSection(driver);
	}
	
	/***
	 * <h1>closeNegativeKeywordsSection</h1>
	 * <p>purpose: Close the Negative Keywords Section if it is not already closed</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass closeNegativeKeywordsSection() {
		if(this.isNegativeKeywordsSectionOpen()) { ((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getNegativeKeywordsSection());  }
		return this;
	}

	/***
	 * <h1>openStatesProvincesSection</h1>
	 * <p>purpose: Open the States/Provinces Section if it is not already open</p>
	 * @return StatesProvincesSection
	 */
	public StatesProvincesSection openStatesProvincesSection() {
		if(!this.isStatesProvincesSectionOpen()) { ((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getStatesProvincesSection()); }
		return new StatesProvincesSection(driver);
	}

	/***
	 * <h1>getStatesProvincesSectionAccess</h1>
	 * <p>purpose: Access to the States/Provinces section</p>
	 * @return StatesProvincesSection
	 */
	public StatesProvincesSection getStatesProvincesSectionAccess() {
		return new StatesProvincesSection(driver);
	}
	
	/***
	 * <h1>closeStatesProvincesSection</h1>
	 * <p>purpose: Close the Keywords Section if it is not already closed</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass closeStatesProvincesSection() {
		if(this.isStatesProvincesSectionOpen()) { ((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getStatesProvincesSection()); }
		return this;
	}	

	/***
	 * <h1>openBidEndDateSection</h1>
	 * <p>purpose: Open the Bid End Date Section if it is not already open</p>
	 * @return BidEndDateSection
	 */
	public BidEndDateSection openBidEndDateSection() {
		if(!this.isBidEndDateSectionOpen()) { ((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getBidEndDateSection());  }
		return new BidEndDateSection(driver);
	}

	/***
	 * <h1>getBidEndDateSectionAccess</h1>
	 * <p>purpose: Access to the Bid End Date section</p>
	 * @return BidEndDateSection
	 */
	public BidEndDateSection getBidEndDateSectionAccess(){
		return new BidEndDateSection(driver);
	}
	
	/***
	 * <h1>closeBidEndDateSection</h1>
	 * <p>purpose: Close the Bid End Date Section if it is not already closed</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass closeBidEndDateSection() {
		if(this.isBidEndDateSectionOpen()) { ((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getBidEndDateSection()); }
		return this;
	}	
	
	
	/***
	 * <h1>clickSearchBtn</h1>
	 * <p>purpose: Click on the "Search" button, located at the bottom of the Filter Results section</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass clickSearchBtn() {
		this.getSearchBtn().click();
		return this;
	}

	/***
	 * <h1>clickClearAllBtn</h1>
	 * <p>purpose: Click on the "Clear All" button, located at the bottom of the Filter Results section</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass clickClearAllBtn() {
		findByScrollIntoViewBottomOfScreen(By.xpath(this.clearAllBtn_xpath));
		helperMethods.addSystemWait(1);
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", this.getClearAllBtn());
		return this;
	}

	/***
	 * <h1>clickSaveSearchBtn</h1>
	 * <p>purpose: Click on the "Save Search" button, located at the bottom of the Filter Results section</p>
	 * @return FilterResultsSavePopupNewClass
	 */
	public FilterResultsSavePopupNewClass clickSaveSearchBtn() {
		this.getSaveSearchBtn().click();
		return new FilterResultsSavePopupNewClass(driver);
	}

	/***
	 * <h1>clickLoadDefaultBtn</h1>
	 * <p>purpose: Click on the "Load Default" button, located at the bottom of the Filter Results section</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass clickLoadDefaultBtn() {
		this.getLoadDefaultBtn().click();
		return this;
	}

	/***
	 * <h1>clickEditDefaultBtn</h1>
	 * <p>purpose: Click on the "Edit Default" button, located at the bottom of the Filter Results section</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass clickEditDefaultBtn() {
		this.getEditDefaultBtn().click();
		return this; 
	}

	/***
     * <h1>waitForFilterResultsSectionToLoad</p>
     * <p>purpose: Wait for the Filter Results Section to load</p>
     * @return FilterResultsSectionNewClass 
     */
    public FilterResultsSectionNewClass waitForFilterResultsSectionToLoad() {
    	waitForVisibility(By.id(this.pageId));
    	return this;
    }
 	
	/*--------------- Verifications --------------------*/
	
	/***
	 * <h1>verifyFilterResultsInitialization</h1>
	 * <p>purpose: Verify that the Filter Results section initializes as expected:
	 * 	1. Each section ("Keywords", "Negative Keywords", "States/Provinces", "Bid End Date")<br>
	 * 	   has correct title<br>
	 *	2. Each section header has a toggle icon<br> 
	 *	3. Keywords section is open, other sections are closed<br>
	 *	4. Bottom buttons appear and correctly labeled ("SEARCH", "Clear All" ,"Load Default" ,"Save Search", "Edit Default Filters")<br>
	 *	Fail test if any of the above conditions are not met.<br>
	 *	Note: Initialization tests are available for each section in its respective section class<br>
	 *	(see BidEndDateSection, StatesProvincesSection, KeywordsSection, NegativeKeywordsSection)</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass verifyFilterResultsInitialization() {
		
		// Verify that each section is closed, except for Keywords section
		if (!this.isPositiveKeywordsSectionOpen()){ fail("Test Failed: \"Keywords\" section initialized closed");}
		if (this.isNegativeKeywordsSectionOpen()) { fail("Test Failed: \"Negative Keywords\" section initialized open");}
		if (!this.isStatesProvincesSectionOpen())  { fail("Test Failed: \"States/Provinces\" section initialized closed");}
		if (this.isBidEndDateSectionOpen())       { fail("Test Failed: \"Bid End Date\" section initialized open");}
		
		// Verify each section displays a title
		if (!this.getPositiveKeywordsSection().isDisplayed()) {fail("Text Failed: \"Keywords\" section title did not initialize as visible");}
		if (!this.getNegativeKeywordsSection().isDisplayed()) {fail("Text Failed: \"Negative Keywords\" section title did not initialize as visible");}
		if (!this.getStatesProvincesSection().isDisplayed())  {fail("Text Failed: \"States/Provinces\" section title did not initialize as visible");}
		if (!this.getBidEndDateSection().isDisplayed())       {fail("Text Failed: \"Bid End Date\" section title did not initialize as visible");}
		
		// Verify each section displays a toggle button
		if (!this.getPositiveKeywordsSectionToggleIcon().isDisplayed()) {fail("Text Failed: \"Keywords\" section dropdown icon did not initialize as visible");}
		if (!this.getNegativeKeywordsSectionToggleIcon().isDisplayed()) {fail("Text Failed: \"Negative Keywords\" section dropdown icon did not initialize as visible");}
		if (!this.getStatesProvincesSectionToggleIcon().isDisplayed())  {fail("Text Failed: \"States/Provinces\" section dropdown icon did not initialize as visible");}
		if (!this.getBidEndDateSectionToggleIcon().isDisplayed())       {fail("Text Failed: \"Bid End Date\" section dropdown icon did not initialize as visible");}
		
		// Verify that all bottom buttons have loaded
		if (!this.getSaveSearchBtn().isDisplayed())  {fail("Text Failed \"SEARCH\" button did not initialize as visible");}
		if (!this.getClearAllBtn().isDisplayed())    {fail("Text Failed \"Clear All\" button did not initialize as visible");}
		if (!this.getLoadDefaultBtn().isDisplayed()) {fail("Text Failed \"Load Default\" button did not initialize as visible");}
		if (!this.getSaveSearchBtn().isDisplayed())  {fail("Text Failed \"Save Search\" button did not initialize as visible");}
		if (!this.getEditDefaultBtn().isDisplayed()) {fail("Text Failed \"Edit Default Filters\" button did not initialize as visible");}

		return this;
	}
	
	/***
	 * <h1>verifyKeywordsSectionIsClosed</h1>
	 * <p>purpose: Verify that the Keywords section is closed<br>
	 * 	Fail test if Keywords section is open</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass verifyKeywordsSectionIsClosed() {
		if (this.isPositiveKeywordsSectionOpen()) {fail ("Test Failed: Keywords section is open");}
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified Keywords section is closed");
		return this;
	}

	/***
	 * <h1>verifyKeywordsSectionIsOpen</h1>
	 * <p>purpose: Verify that the Keywords section is open<br>
	 * 	Fail test if Keywords section is closed</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass verifyKeywordsSectionIsOpen() {
		if (!this.isPositiveKeywordsSectionOpen()) {fail ("Test Failed: Keywords section is closed");}
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified Keywords section is open");
		return this;
	}

	/***
	 * <h1>verifyNegativeKeywordsSectionIsClosed</h1>
	 * <p>purpose: Verify that the Negative Keywords section is closed<br>
	 * 	Fail test if Negative Keywords section is open</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass verifyNegativeKeywordsSectionIsClosed() {
		if (this.isNegativeKeywordsSectionOpen()) {fail ("Test Failed: Negative Keywords section is open");}
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified Negative Keywords section is closed");
		return this;
	}	

	/***
	 * <h1>verifyNegativeKeywordsSectionIsOpen</h1>
	 * <p>purpose: Verify that the Negative Keywords section is open<br>
	 * 	Fail test if Negative Keywords section is closed</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass verifyNegativeKeywordsSectionIsOpen() {
		if (!this.isNegativeKeywordsSectionOpen()) {fail ("Test Failed: Negative Keywords section is closed");}
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified Negative Keywords section is open");
		return this;
	}	

	/***
	 * <h1>verifyStatesProvincesSectionIsClosed</h1>
	 * <p>purpose: Verify that the States/Provinces section is closed<br>
	 * 	Fail test if StatesProvinces section is open</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass verifyStatesProvincesSectionIsClosed() {
		if (this.isStatesProvincesSectionOpen()) {fail ("Test Failed: StatesProvinces section is open");}
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified StatesProvinces section is closed");
		return this;
	}	

	/***
	 * <h1>verifyStatesProvincesSectionIsOpen</h1>
	 * <p>purpose: Verify that the States/Provinces section is open<br>
	 * 	Fail test if StatesProvinces section is closed</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass verifyStatesProvincesSectionIsOpen() {
		if (!this.isStatesProvincesSectionOpen()) {fail ("Test Failed: StatesProvinces section is closed");}
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified StatesProvinces section is open");
		return this;
	}	

	/***
	 * <h1>verifyBidEndDateSectionIsClosed</h1>
	 * <p>purpose: Verify that the Bid End Date section is closed<br>
	 * 	Fail test if BidEndDate section is open</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass verifyBidEndDateSectionIsClosed() {
		if (this.isBidEndDateSectionOpen()) {fail ("Test Failed: BidEndDate section is open");}
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified BidEndDate section is closed");
		return this;
	}	

	/***
	 * <h1>verifyBidEndDateSectionIsOpen</h1>
	 * <p>purpose: Verify that the Bid End Date section is open<br>
	 * 	Fail test if BidEndDate section is closed</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass verifyBidEndDateSectionIsOpen() {
		if (!this.isBidEndDateSectionOpen()) {
			// Fix for release OCT 7
			this.getBidEndDateSectionToggleIcon().click();				
		}
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified BidEndDate section is open");
		return this;
	}	
	
	/***
	 * <h1>verifyFilterSavedOutToDatabase</h1>
	 * <p>purpose: Verify that the filter saved out to the database.<br>
	 * 	Fail test if what was recorded in database does not match the filter,<br>
	 * 	or if the filter did not write out to the database</p>
	 * @param filter = filter to search database for
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass verifyFilterSavedOutToDatabase(FilterCriteria filter) {
		FilterCriteria databaseFilter = this.getSearchFromDatabase(filter.getFilterName());
		if (databaseFilter == null) {
			fail("Test Failed: Record for \"" + filter.getFilterName() + "\" filter was not located in the database"); }
		
		
		// dmidura: Workaround for setting Bid End Date to year "9999"
		// Because the bid end date is "9999", the database returns a time frame of custom time frame
		// Here, changing the expected filter value so the comparison doesn't bomb out
//		smitha : commented to fix Scenario :  Filter Results Collapsible Section Allows for Cancel and Save of Search Names on Save Search Popup
//		if (!filter.getSelectTimeFrame().equals("NONE")) {
//			filter.setSelectTimeFrame(TimeFrameOptions.CUSTOM_TIME_FRAME); }

		System.out.println(GlobalVariables.getTestID() + " ---- Expected Filter -----");
		filter.printFilterCriteriaToScreen();
		System.out.println(GlobalVariables.getTestID() + " ---- Database Filter -----");
		databaseFilter.printFilterCriteriaToScreen();
	
		// Compare the databaseFilter to the filter to determine if the database has saved out the filter as expected
		if (!databaseFilter.equals(filter) ) {
			fail("Test Failed: Record for \"" + filter.getFilterName() + "\" filter did not save out correctly to the database");}

		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"" + filter.getFilterName() + "\" filter saved out correctly to database");
		return this;
	}
	
	/***
	 * <h1>verifyFilterDidNotSaveOutToDatabase</h1>
	 * <p>purpose: Verify that the filter did not save out to the database.<br>
	 * Fail test if record was located in database for filter</p>
	 * @param filter = filter to search database for
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass verifyFilterDidNotSaveOutToDatabase(FilterCriteria filter) {
		if (this.getSearchFromDatabase(filter.getFilterName()) != null ) {
			fail("Test Failed: Record for \"" + filter.getFilterName() + "\" filter was located in the database"); }

		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"" + filter.getFilterName() + "\" filter did not save out to database");
		return this;
	}

	/***
	 * <h1>verifyFilterResultsSectionIsCleared</h1>
	 * <p>purpose: After clicking "Clear All" on the Filter Results section<br>
	 * 	this method can be run to verify that the Filter Results section was properly cleared per:<br>
	 * 	1. No chips in Keywords, Negative Keywords, States/Provinces chip lists<br>
	 * 	2. "Search all states/provinces" checkbox unchecked for States/Provinces section<br>
	 * 	3. "Include past bids" checkbox unchecked for Bid End Date section<br>
	 * 	Test will fail if any of the above conditions are not met.</p>
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass verifyFilterResultsSectionIsCleared(){
		// 1. Keywords should not display any chips in chip list
		this.openKeywordsSection()
			.getPositiveKeywordsChipList()
			.verifyChipListIsEmpty(); 

		// 2. Negative Keywords should not display any chips in chip list
		this.openNegativeKeywordsSection()
			.getNegativeKeywordsChipList()
			.verifyChipListIsEmpty(); 

		// 3. States/Provinces should display "Search all states/provinces" checkbox as checked
		//    and should not display any chips in chip list
		this.openStatesProvincesSection()
			.verifySearchAllStatesProvincesCheckboxChecked()
			.getStatesProvincesChipList()
			.verifyChipListIsEmpty();

		// 4. Time Frame shows "Include past bids" checkbox is unchecked when section is opened
		this.openBidEndDateSection()
			.verifyIncludePastBidsCheckboxIsUnchecked();
			
		return this;
	}
	

	/***
	 * <h1>verifyFilterResultsDisplaysUnsavedFilter</h1>
	 * <p>purpose: Verify that the Filter Results section is currently displaying<br>
	 * 	your the filter that has not been retrieved from Saved Searches<br>
	 * 	Fail test if what is currently displayed<br>
	 * 	in the Filter Results section does not match the filter<br>
	 * 	Note: Use verifyFilterResulstDisplaysSavedFilter method to <br>
	 * 	test if filter retrieved from Saved Searches is correctly displayed in <br>
	 * 	the Filter Results section</p>
	 * @param filter = FilterCriteria representing your filter
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass verifyFilterResultsDisplaysUnsavedFilter(FilterCriteria filter) {
		filter.printFilterCriteriaToScreen();
		
		// 1. Keywords: chip list should contain all positive keywords
		this.openKeywordsSection()
			.getPositiveKeywordsChipList()
			.verifyChipsInChipList(filter.getKeywords());

		// 2. Negative Keywords: chip list should contain all negative keywords
		this.openNegativeKeywordsSection()
			.getNegativeKeywordsChipList()
			.verifyChipsInChipList(filter.getNegativeKeywords());
		
		// 3. States/Provinces: chip list should contain all States/Provinces
		this.openStatesProvincesSection()
			.uncheckSearchAllStatesProvincesCheckbox()
			.getStatesProvincesChipList()
			.verifyChipsInChipList(filter.getStatesProvinces());
		
		// 4. Bid End Date

		switch (filter.getSelectTimeFrame()) {
			case NONE:
				// If there is no select time frame, then "Include Past Bids" checkbox
				// should be unchecked
				this.openBidEndDateSection()
					.verifyIncludePastBidsCheckboxIsUnchecked();
				break;

			case CUSTOM_TIME_FRAME:
				// If the time frame is a Custom Time Frame
				// "CUSTOM TIME FRAME" should be displayed in "SELECT TIME FRAME"
				// and the "Show Bids to" and "Show Bids From" dates should be set
				this.openBidEndDateSection()
					.verifyIncludePastBidsCheckboxIsChecked()
					.verifyTimeFrameSelectionDisplaysValue(filter.getSelectTimeFrame())
					.getCustomTimeFrameSection()
					.verifyShowBidsFromDisplaysDate(filter.getBidStartDate())
					.verifyShowBidsToDisplaysDate(filter.getBidEndDate());
				break;

			default:
				// All other time frames should display their respective values in the "SELECT TIME FRAME" dropdown
				this.openBidEndDateSection()
					.verifyIncludePastBidsCheckboxIsChecked()
					.verifyTimeFrameSelectionDisplaysValue(filter.getSelectTimeFrame());
				break;
		}
				return this;
	}

// #Fix for release OCT 7
	/***
	 * <h1>verifyKeywordResultsDisplaysUnsavedFilter - Only checks for Positive  keyword section</h1>
	 * <p>purpose: Verify that the Filter Results section is currently displaying<br>
	 * 	your the filter that has not been retrieved from Saved Searches<br>
	 * 	Fail test if what is currently displayed</p>
	 * @param filter = FilterCriteria representing your filter
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass verifyFilterResultsDisplaysUnsavedFilterAfterBackToBid(FilterCriteria filter) {
		filter.printFilterCriteriaToScreen();

		// 1. Keywords: chip list should contain all positive keywords
		this.openKeywordsSection()
				.getPositiveKeywordsChipListAfterBackToBid()
				.verifyChipsInChipList(filter.getKeywords());

		// 2. Negative Keywords: chip list should contain all negative keywords
		this.openNegativeKeywordsSection()
				.getNegativeKeywordsChipList()
				.verifyChipsInChipList(filter.getNegativeKeywords());

		// 3. States/Provinces: chip list should contain all States/Provinces
		this.openStatesProvincesSection()
				.uncheckSearchAllStatesProvincesCheckbox()
				.getStatesProvincesChipListAfterBackToBid()
				.verifyChipsInChipList(filter.getStatesProvinces());

		// 4. Bid End Date

		switch (filter.getSelectTimeFrame()) {
			case NONE:
				// If there is no select time frame, then "Include Past Bids" checkbox
				// should be unchecked
				this.openBidEndDateSection()
						.verifyIncludePastBidsCheckboxIsUnchecked();
				break;

			case CUSTOM_TIME_FRAME:
				// If the time frame is a Custom Time Frame
				// "CUSTOM TIME FRAME" should be displayed in "SELECT TIME FRAME"
				// and the "Show Bids to" and "Show Bids From" dates should be set
				this.openBidEndDateSection()
						.verifyIncludePastBidsCheckboxIsChecked()
						.verifyTimeFrameSelectionDisplaysValue(filter.getSelectTimeFrame())
						.getCustomTimeFrameSection()
						.verifyShowBidsFromDisplaysDate(filter.getBidStartDate())
						.verifyShowBidsToDisplaysDate(filter.getBidEndDate());
				break;

			default:
				// All other time frames should display their respective values in the "SELECT TIME FRAME" dropdown
				this.openBidEndDateSection()
						.verifyIncludePastBidsCheckboxIsChecked()
						.verifyTimeFrameSelectionDisplaysValue(filter.getSelectTimeFrame());
				break;
		}

		return this;
	}

	/***
	 * <h1>verifyFilterResultsDisplaysSavedFilter</h1>
	 * <p>purpose: Verify that the Filter Results section is currently displaying<br>
	 * 	your the filter retrieved from a saved search.<br>
	 * 	Fail test if what is currently displayed<br>
	 * 	in the Filter Results section does not match the retrieved filter</p>
	 * @param filter = FilterCriteria representing your filter (retrieved from Saved Searches)
	 * @return FilterResultsSectionNewClass
	 */
	public FilterResultsSectionNewClass verifyFilterResultsDisplaysSavedFilter(FilterCriteria filter) {
		filter.printFilterCriteriaToScreen();
		
		// 1. Keywords: chip list should contain all positive keywords
		this.openKeywordsSection()
			.getPositiveKeywordsChipList()
			.verifyChipsInChipList(filter.getKeywords());

		// 2. Negative Keywords: chip list should contain all negative keywords
		this.openNegativeKeywordsSection()
			.getNegativeKeywordsChipList()
			.verifyChipsInChipList(filter.getNegativeKeywords());
		
		// 3. States/Provinces: chip list should contain all States/Provinces
		this.openStatesProvincesSection()
			.uncheckSearchAllStatesProvincesCheckbox()
			.getStatesProvincesChipList()
			.verifyChipsInChipList(filter.getStatesProvinces());
		
		// 4. Bid End Date

		switch (filter.getSelectTimeFrame()) {
			case NONE:
				// If there is no select time frame, then "Include Past Bids" checkbox
				// should be unchecked
				this.openBidEndDateSection()
					.verifyIncludePastBidsCheckboxIsUnchecked();
				break;

			case CUSTOM_TIME_FRAME:
				// If the time frame is a Custom Time Frame:
				// 1. Bid End Date section should be open
				// 2. "Include past bids" should be checked
				// 3. "Show Bids to" and "Show Bids from" should be visible
				// 4. "Show Bids From" should match the filter's start date
				// 5. "Show Bids to" should match the filter's end date
				this.verifyBidEndDateSectionIsOpen()
					.getBidEndDateSectionAccess()
					.verifyIncludePastBidsCheckboxIsChecked()
					.getCustomTimeFrameSection()
					.verifyCustomTimeFrameDateSelectorsAreVisible()
					.verifyShowBidsFromDisplaysDate(filter.getBidStartDate())
					.verifyShowBidsToDisplaysDate(filter.getBidEndDate());
				break;

			default:
				// If the time frame is preselected time frame
				// 1. Bid End Date section should be open
				// 2. "Include past bids" should be checked
				// 3. "Show Bids to" and "Show Bids from" should be visible
				this.verifyBidEndDateSectionIsOpen()
					.getBidEndDateSectionAccess()
					.verifyIncludePastBidsCheckboxIsChecked()
					.getCustomTimeFrameSection()
					.verifyCustomTimeFrameDateSelectorsAreVisible();

				// 4. The difference between "Show Bids to" and "Show Bids from" should
				//    equal the filter's timeframe
				String bidStart          = this.getBidEndDateSectionAccess().getCustomTimeFrameSection().getShowBidsFromInputText();
				String bidEnd            = this.getBidEndDateSectionAccess().getCustomTimeFrameSection().getShowBidsToInputText();
				String bidStartFormatted = dateHelpers.flipDateMonthFirst(bidStart.replaceAll("/", "-"));
				String bidEndFormatted   = dateHelpers.flipDateMonthFirst(bidEnd.replaceAll("/", "-"));

				//dmidura: Workaround to account for Bid End date year as "9999"
				// Updating test to match "9999" year so we don't bomb out
				String [] year = dateHelpers.getCalendarDateAsString(dateHelpers.getCurrentTimeAsCalendar()).split("/");
				bidEndFormatted = bidEndFormatted.replace("9999", year[2]);
				
				if (!filter.getSelectTimeFrame().equals(this.getTimeFramePreSelectionFromDates(bidStartFormatted, bidEndFormatted))) {
					fail("Test Failed: Expected value of \"" + filter.getSelectTimeFrame().toString() + "\", but displayed dates are:\n" +
						 "Show Bids From = \"" + bidStart +"\"\n" +
						 "Show Bids to   = \"" + bidEnd   + "\""); 
				} else {
					System.out.println(GlobalVariables.getTestID() + " INFO: Verified that expected time frame of \"" + filter.getSelectTimeFrame() + "\" is currently displayed with dates\n" +
						 "\tShow Bids From = \"" + bidStart +"\"\n" +
						 "\tShow Bids to   = \"" + bidEnd   + "\""); }
				
				break;
		}
				return this;
	}

    /***
     * <h1>verifyFilterSearchResultsDisplayCorrectlyInAllBidsTab</h1>
     * <p>purpose: Verify that the filters set on the Filter Results section<br>
     * 	are reflected in bids that are currently displaying per: <br>
     * 	1. Bid Title contains at least one keyword and contains no negative keywords<br>
     * 	2. State/Provinces matches at least one of the inputed states/provinces<br>
     * 	3. Bid Due Date is contained within the time frame<br>
     *  Fail test if any of the above criteria are not met.<br>
     *  Note: If a Bid Title is cut off b/c the bid is in "UPGRADE PLAN" mode, then we'll<br>
     *  assume that the keywords match. However, if a negative keyword is located within<br>
     *  the cut-off title, then we'll fail the test.</p>
     * @param dTable = DataTable formatted as:
	 *    | Filter Name            | <Filter_Name>                    |                                 | ... |                                 | <br>
	 *    | Keywords               | [keyword1]                       | [keyword2]                      | ... | [keywordx]                      | <br>
	 *    | Negative Keywords      | [keyword1]                       | [keyword2]                      | ... | [keywordx]                      | <br>
	 *    | States/Provinces       | [state/province1_from_dropdown]  | [state/province2_from_dropdown> | ... | [state/provincex_from_dropdown] | <br>
	 *    | Time Frame             | [time_frame_from_dropdown]       |                                 | ... |                                 | <br>
	 *    | [Show Bids From]       | [start_date]                     |                                 | ... |                                 | <br>
	 *    | [Show Bids to]         | [end_date]                       |                                 | ... |                                 | <br>
	 *    (start_date and end_date should be in MM/DD/YYYY format)
	 *    Here, you can add as many keywords and states as you want, BUT the table must be balanced (i.e. if you <br>
	 *    add 3 states, then there should be 4 columns in the datatable). Filters that can only be set to one value should <br>
	 *    then be input with blanks.<br>
	 *    Note: Filter Name is required<br>
	 *    
	 *    Ex:  
	 *    | Filter Name            | filter              |      |         | <br>
	 *    | Keywords               | test                |      |         | <br>
	 *    | Negative Keywords      |                     |      |         | <br>
	 *    | States/Provinces       | Texas               | Utah | Alabama | <br>
	 *    | Time Frame             | Past 6 months       |      |         | <br>
	 *    Ex: Time Frame = "Custom Timeframe"
	 *    | Filter Name            | filter              |      |         | <br>
	 *    | Keywords               | test                |      |         | <br>
	 *    | Negative Keywords      | bad                 | no   |         | <br>
	 *    | States/Provinces       | Texas               | Utah | Alabama | <br>
	 *    | Time Frame             | Custom Timeframe 	 |      |         | <br>
	 *    | Show Bids From         | 12/12/2016          |      |         | <br>
	 *    | Show Bids to           | 02/02/2018          |      |         | <br>
	 * @param dTable
	 */
    public FilterResultsSectionNewClass verifyFilterSearchResultsDisplayCorrectlyInAllBidsTab(DataTable dTable) {
		verifyFilterSearchResultsDisplayCorrectlyInAllBidsTab(putDataTableIntoFilterCriteria(dTable));
		return this;
    }
    
    /***
     * <h1>verifyFilterSearchResultsDisplayCorrectlyInAllBidsTab</h1>
     * <p>purpose: Given a FilterCriteria object representing filter criteria, verify that 
     * 	all the visible bids that are returned in the All Bids list meet the filter criteria</p>
	 * @param filter - representing the filter to test in All Bids
	 * @return FilterResultsSectionNewClass
     */
    public FilterResultsSectionNewClass verifyFilterSearchResultsDisplayCorrectlyInAllBidsTab(FilterCriteria filter) {
		BidList bidList = new BidList(this.driver);
		
		// Let results load
		waitForSpinner(driver);
		
		// Make sure the bid list isn't empty
		bidList.verifyMinimumNumberOfBidResults(1);
		
		filter.printFilterCriteriaToScreen();
		
		// Check each bid, to make sure that the bid follows the filter criteria
		for (BidListRow bidResult : bidList.allVisibleBids()) {
			System.out.println(GlobalVariables.getTestID() + " INFO: Verifying that \"" + bidResult.bidInfo.getBidTitle() + "\" bid result matches filter criteria");
			//helperMethods.findByScrollIntoView(driver, bidResult.getXpath());

			// Vars to test that our results meet our filter requirements:
			// Note: If a certain filter isn't set, then results meet those requirements
			Boolean bMatchesKeywords = filter.getKeywords().isEmpty()        ? true : false;
			Boolean bMatchesState    = filter.getStatesProvinces().isEmpty() ? true : false;
			
			// Title should contain at least one Keyword
			for (String strKeyword: filter.getKeywords()) {
				if(bidResult.bidInfo.getBidTitle().toLowerCase().contains(strKeyword.toLowerCase())) { 
					System.out.println(GlobalVariables.getTestID() + " INFO: Keyword match for \"" + strKeyword + "\"");
					bMatchesKeywords = true;
					break; }
			}
			
			// but if the title is cut off for because we're in upgrade plan mode, then we'll assume a match to be safe
			if (!bMatchesKeywords & bidResult.isUpgradePlan()) { 
				System.out.println(GlobalVariables.getTestID() + " INFO: Plan is being upgraded. Assuming match b/c title is cut off.");
				bMatchesKeywords = true; }
			
			// However, it should not contain any Negative Keywords
			for (String strNegKeyword: filter.getNegativeKeywords()) {
				if(bidResult.bidInfo.getBidTitle().toLowerCase().contains(strNegKeyword.toLowerCase())) { 
					System.out.println(GlobalVariables.getTestID() + " INFO: Negative Keyword match for \"" + strNegKeyword + "\"");
					bMatchesKeywords = false; 
					break; }
			}
			
			// State should contain at least one States/Provinces
			// States/Provinces are expected to be abbreviations
			for (String strState: filter.getStatesProvinces()) {
				if (bidResult.bidInfo.getState().contains(strState)) {
					System.out.println(GlobalVariables.getTestID() + " INFO: State/Province match for \"" + strState + "\""); 
					bMatchesState = true; 
					break; }
			}
			
			if (!bMatchesKeywords) { fail("Test Failed: \"" + bidResult.bidInfo.getBidTitle() + "\" does not contain any keywords or contains a negative keyword for bid \"" + bidResult.bidInfo.getBidTitle() + "\"\n"
										+ "Selected keywords          = \"" + filter.getKeywords().toString()         + "\"\n"
										+ "Selected negative keywords = \"" + filter.getNegativeKeywords().toString() + "\"");}
			if (!bMatchesState)    { fail("Test Failed: \"" + bidResult.bidInfo.getState() + "\" does not match any of the selected states/provices for bid \""           + bidResult.bidInfo.getBidTitle() + "\"\n"
										+ "Selected states/provinces = \"" + filter.getStatesProvinces().toString() + "\"\n\n"
										+ bidResult.bidInfo.printBidInfo());}
			
			// Due Date should lie within the set Time Frame
			if (!filter.getSelectTimeFrame().equals(TimeFrameOptions.NONE)) {
				TimeFrameOptions timeFrame = filter.getSelectTimeFrame();

				// Grab the date that is currently displaying on the bid result
				Calendar bidDate = bidResult.bidInfo.getEndDateAsCalendar();

				String   strStartDate = "";
				String   strEndDate   = "";
				Calendar startDate;
				Calendar endDate;

				// If Time Frame is not Custom, then create start and end dates where the end date is today
				if (!timeFrame.equals(TimeFrameOptions.CUSTOM_TIME_FRAME)) {
					endDate      = Calendar.getInstance();
					startDate    = addTimeFrame(endDate, timeFrame);
					strStartDate = dateHelpers.getCalendarDateAsString(startDate);
					strEndDate   = dateHelpers.getCalendarDateAsString(endDate);
					
					// dmidura: Workaround for "9999" defaulted as the year in "Show Bids to"
					strEndDate   = strEndDate.substring(0, 6) + "9999";
					endDate      = dateHelpers.getStringDateAsCalendar(strEndDate);
							
				// If the Time Frame is Custom, then grab the start date and end date that the user set
				// from the filter
				} else {
					strStartDate = filter.getBidStartDate();
					strEndDate   = filter.getBidEndDate();
					startDate    = dateHelpers.getStringDateAsCalendar(strStartDate);
					endDate      = dateHelpers.getStringDateAsCalendar(strEndDate);
				}
				
					System.out.println(GlobalVariables.getTestID() + " INFO: Determining if " + bidResult.bidInfo.getEndDate() + " lies between Filter Results time frame of "
										+ strStartDate + " - " + strEndDate); 
				
				// And verify that the date is greater than or equal to the start date, but less than or equal to the end date
				if ((!bidDate.after(startDate) & !bidDate.equals(startDate)) | 
					(!bidDate.before(endDate)  & !bidDate.equals(endDate)))  {
					fail ("Test Failed: Bid Date " + bidDate + " does not lie between Filter Results time frame of" + strStartDate + " - " + strEndDate); }
			}
			
		}	

		return this;
    }
    


	/* ----- helpers ----- */
	
	/***
	 * <h1>isPositiveKeywordsSectionOpen</h1>
	 * @return true if section is open | false if section is closed
	 */
	private Boolean isPositiveKeywordsSectionOpen() { helperMethods.addSystemWait(1); return findByPresence(By.xpath(this.positiveKeywordsSectionVisibility_xpath)).getCssValue("visibility").equals("visible");} 
	/***
	 * <h1>isNegativeKeywordsSectionOpen</h1>
	 * @return true if section is open | false if section is closed
	 */
	private Boolean isNegativeKeywordsSectionOpen() { helperMethods.addSystemWait(1); return findByPresence(By.xpath(this.negativeKeywordsSectionVisibility_xpath)).getCssValue("visibility").equals("visible");} 

	/***
	 * <h1>isStatesProvincesSectionOpen</h1>
	 * @return true if section is open | false if section is closed
	 */
	private Boolean isStatesProvincesSectionOpen() { helperMethods.addSystemWait(1); return findByPresence(By.xpath(this.statesProvincesSectionVisibility_xpath)).getCssValue("visibility").equals("visible");}

	/***
	 * <h1>isBidEndDateSectionOpen</h1>
	 * @return true if section is open | false if section is closed
	 */
	private Boolean isBidEndDateSectionOpen() { helperMethods.addSystemWait(1); return findByPresence(By.xpath(this.bidEndDateSectionVisibility_xpath)).getCssValue("visibility").equals("visible"); }

    /***
     * <h1>putDataTableIntoFilterCriteria</h1>
     * <p>purpose: DataTables can be fed into tests to allow for input configuration.<br>
     * 	In this helper, extract the DataTable and put into a FilterCriteria object for ease of manipulation</p>:w
     * @param dTable = DataTable formatted as:
	 *    | Filter Name            | <Filter_Name>                    |                                 | ... |                                 | <br>
	 *    | Keywords               | [keyword1]                       | [keyword2]                      | ... | [keywordx]                      | <br>
	 *    | Negative Keywords      | [keyword1]                       | [keyword2]                      | ... | [keywordx]                      | <br>
	 *    | States/Provinces       | [state/province1_from_dropdown]  | [state/province2_from_dropdown> | ... | [state/provincex_from_dropdown] | <br>
	 *    | Time Frame             | [time_frame_from_dropdown]       |                                 | ... |                                 | <br>
	 *    | [Show Bids From]       | [start_date]                     |                                 | ... |                                 | <br>
	 *    | [Show Bids to]         | [end_date]                       |                                 | ... |                                 | <br>
	 *    (start_date and end_date should be in MM/DD/YYYY format)
	 *    Here, you can add as many keywords and states as you want, BUT the table must be balanced (i.e. if you <br>
	 *    add 3 states, then there should be 4 columns in the datatable). Filters that can only be set to one value should <br>
	 *    then be input with blanks.<br>
	 *    Note: Filter Name is required<br>
	 *    
	 *    Ex:  
	 *    | Filter Name            | filter              |      |         | <br>
	 *    | Keywords               | test                |      |         | <br>
	 *    | Negative Keywords      |                     |      |         | <br>
	 *    | States/Provinces       | Texas               | Utah | Alabama | <br>
	 *    | Time Frame             | Past 6 months       |      |         | <br>
	 *    Ex: Time Frame = "Custom Timeframe"
	 *    | Filter Name            | filter              |      |         | <br>
	 *    | Keywords               | test                |      |         | <br>
	 *    | Negative Keywords      | bad                 | no   |         | <br>
	 *    | States/Provinces       | Texas               | Utah | Alabama | <br>
	 *    | Time Frame             | Custom Timeframe 	 |      |         | <br>
	 *    | Show Bids From         | 12/12/2016          |      |         | <br>
	 *    | Show Bids to           | 02/02/2018          |      |         | <br>
	 * @return FilterCriteria
	 */
    private FilterCriteria putDataTableIntoFilterCriteria(DataTable dTable) {
    	// Throw everything into a hash so then we can extract it by key
    	Map <String, List <String>> inputsMap;
		inputsMap = helperMethods.putDataTableIntoMap(dTable);	
		return putHashMapIntoFilterCriteria(inputsMap);
    }
   
    /***
     * <h1>putHashMapIntoFilterCriteria</h1>
     * <p>purpose: Given a Hash Map mapping filter criteria to values, format as FilterCriteria<br>
     * 	Note: This is a helper method specifically used to help extract data from datatable and to extract<br>
     * 	data from search query. Doesn't have much application besides these scenarios</p>
     * @param inputsMap
     * @return FilterCriteria
     */
    private FilterCriteria putHashMapIntoFilterCriteria(Map <String, List<String>> inputsMap) {
    	FilterCriteria filter = new FilterCriteria();
    	
    	// When pulling from the database, the values can be null
    	// so there are null checks in this method to account for that

    	if(inputsMap.get("Keywords") != null) {
    		filter.setKeywords(inputsMap.get("Keywords"));}
    	
    	if(inputsMap.get("Negative Keywords") != null) {
    		filter.setNegativeKeywords(inputsMap.get("Negative Keywords")); }
    	
    	// States/Provinces should always be the abbreviations
    	if (inputsMap.get("States/Provinces") != null) {
			List <String> temp = new ArrayList<String>();
			for (String stateProvince : inputsMap.get("States/Provinces")) {
				if (stateProvince.length() == 2 ) {
					temp.add(stateProvince);
				} else if (stateProvince.length() != 0) {
					temp.add(geographyHelpers.getStateOrProvinceAbbreviationFromStateName(stateProvince)); } 
			} 
			filter.setStatesProvinces(temp);
    	}

    	if(inputsMap.get("Time Frame") != null) {
			if (!inputsMap.get("Time Frame").get(0).isEmpty()) {
				switch (inputsMap.get("Time Frame").get(0)) {
					case "None":
						filter.setSelectTimeFrame(TimeFrameOptions.NONE);
						break;
					case "Past 6 Months":
						filter.setSelectTimeFrame(TimeFrameOptions.PAST_6_MONTHS);
						break;
					case "Past Year":
						filter.setSelectTimeFrame(TimeFrameOptions.PAST_YEAR);
						break;
					case "Past 3 Years":
						filter.setSelectTimeFrame(TimeFrameOptions.PAST_3_YEARS);
						break;
					case "Custom Time Frame":
						filter.setSelectTimeFrame(TimeFrameOptions.CUSTOM_TIME_FRAME);
						break;
					default:
						fail("ERROR: DataTable \"Time Frame\" =" + inputsMap.get("Time Frame").get(0) + " is an invalid value");
					}
			}
//		smitha : commented to fix Scenario :  Filter Results Collapsible Section Allows for Cancel and Save of Search Names on Save Search Popup	
//			if (filter.getSelectTimeFrame().equals(TimeFrameOptions.CUSTOM_TIME_FRAME)) {
			if(!inputsMap.get("Show Bids From").isEmpty()){
				filter.setBidStartDate(inputsMap.get("Show Bids From").get(0));
			}
			if(!inputsMap.get("Show Bids to").isEmpty()){	
				filter.setBidEndDate(inputsMap.get("Show Bids to").get(0));
			}
//			}
    	} else {
    		filter.setSelectTimeFrame(TimeFrameOptions.NONE); }
    	
    	filter.setFilterName(inputsMap.get("Filter Name").get(0));
    	
    	return filter;
    }

    
    /***
     * <h1>addTimeFrame</h1>
     * <p>purpose: Given an end date and a specified time frame, return
     * 	the start date<br>
     * 	Ex: endDate = 7/17/2018 and timeFrame = PAST_YEAR<br>
     * 	return startDate = 7/17/2017</p>
     * @param endDate Calendar
     * @param timeFrame
     * @return Calendar
     */
    private Calendar addTimeFrame (Calendar endDate, TimeFrameOptions timeFrame) {
    	Calendar startDate = (Calendar) endDate.clone();

    	switch (timeFrame)
    	{
    	case PAST_6_MONTHS:
    		startDate.add(Calendar.MONTH, -6);
    		break;
    	case PAST_YEAR:
    		startDate.add(Calendar.YEAR, -1);
    		break;
    	case PAST_3_YEARS:
    		startDate.add(Calendar.YEAR, -3);
    		break;
    	default:
    		fail ("ERROR: Supplied time frame is not valid");
    		break;
    	}
    		
    	return startDate;
    }
    

	/***
	 * <h1>getTimeFramePreSelectionFromDates</h1>
	 * <p>purpose: Given a start date and end date, determine if the dates correspond to a Time Frame preselection</p>
	 * @param strStartDate = String for the start date (Form: YYYY-MM-DD)
	 * @param strEndDate = String for the end date (Form: YYYY-MM-DD)
	 * @return the preselection as TimeFrameOptions object<br>
	 */
	private TimeFrameOptions getTimeFramePreSelectionFromDates(String strStartDate, String strEndDate) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Determining preselection of time frame from start date \"%s\" and end date \"%s\"\n", strStartDate, strEndDate);
		TimeFrameOptions returnValue = TimeFrameOptions.NONE;
		
		LocalDate start = LocalDate.parse(strStartDate);
		LocalDate end   = LocalDate.parse(strEndDate);
		
		// User can specify "Past 6 Months", "Past Year", "Past 3 Years"
		// or user could have entered a "Custom Time Frame"
		// Note: Calculations in app are based on subtracting time frame from the end date
		// i.e. today = 8/31/2018 and time frame = Past 6 Months
		// gives end date = 8/31/2018, start date = 2/28/2018
		if (end.plus(-6, ChronoUnit.MONTHS).equals(start)) {
			returnValue = TimeFrameOptions.PAST_6_MONTHS;
		} else if (end.plus(-1, ChronoUnit.YEARS).equals(start)) {
			returnValue = TimeFrameOptions.PAST_YEAR;
		} else if (end.plus(-3, ChronoUnit.YEARS).equals(start)) {
			returnValue = TimeFrameOptions.PAST_3_YEARS;
		} else {
			returnValue = TimeFrameOptions.CUSTOM_TIME_FRAME;}
		
		System.out.printf(GlobalVariables.getTestID() + " INFO: Preselection determined to be \"%s\"\n", returnValue.toString());

		return returnValue;
	}

	/*--------------- Database Queries --------------------*/
	
	/***
	 * <h1>getSearchFromDatabase</h1>
	 * <p>purpose: Given specifics about a filter set, query the database and return what is currently stored for that filter.<br>
	 * 	If filter does not exist in database, then return null</p>
	 * @param strSearchName = name you saved your search as
	 * @return FilterCriteria if filter exists in database | null if filter is not located in database
	 */
	public FilterCriteria getSearchFromDatabase(String strSearchName) {
		String strEmail = SupplierLogin.thisUser.getEmail().toLowerCase();
		System.out.printf(GlobalVariables.getTestID() + " INFO: Running query to retrieve database record for search = %s for user = %s\n", strSearchName, strEmail);
		
		// First get the correct user_id for our user's account
		String user_id = new DatabaseCommonSearches().getUserIdFromUserEmail(strEmail);
		
		// Now using the user_id, locate the search criteria
		String strQuery  = "select search_criteria from user_search_criteria where user_id='" + user_id + "'";
		String strDBname = "survivor";
		System.out.printf(GlobalVariables.getTestID() + " INFO: Running query \"%s\" on database \"%s\" \n", strQuery, strDBname);
		ResultSet rs2 = null;

		try {
			rs2 = SQLConnector.executeQuery(strDBname, strQuery);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		List <String> searchCriteria = new ArrayList<>();
		try {
			while (rs2.next()) {
				searchCriteria.add(rs2.getString("search_criteria")); }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		// We have a HashMap of all the searches conducted since strEstimatedLastModifiedDate, ordered by oldest modification.
		// Go through those searches and see if we have our search 
		Iterator <String> it = searchCriteria.iterator();
		String ourSearch = "";
		
		// Grabbing the search by name. NOTE: This assumes name uniqueness
		while (it.hasNext()) {
			String strSearch = it.next();
			if (strSearch.contains(strSearchName)) {
				ourSearch = strSearch;
				break;}
		}
		
		if (ourSearch.isEmpty()) { return null; }

		System.out.printf(GlobalVariables.getTestID() + " INFO: Located search info from database for \"%s\" and it is \n\"%s\"\n", strSearchName, ourSearch); 
		return createFilterCriteriaFromStringForDatabaseQueryReturn(ourSearch);
	}

	/***
	 * <h1>createFilterCriteriaFromStringForDatabaseQueryReturn</h1>
	 * <p>purpose: Given a string containing the database query for <br>
	 *    a filter, return as a FilterCriteria object               <br>
	 * @param strDatabaseQuery = string containing a database query <br>
	 * ex:                                                          <br>
	 * {                                                            <br>
  	 * 	 "searchId" : "0a54cbb5-80e6-4cd8-bf5c-2bc343e76111",       <br>
	 * 	 "showSubscribed" : false,									<br>
	 *   "showOnlyNew" : false,										<br>
	 *   "keywords" : [ "key1", "key2" ],							<br>
	 *   "negKeywords" : [ "!key1", "!key2" ],						<br>
	 *   "bidTypes" : "military",									<br>
	 *   "startDate" : null,										<br>
	 *   "endDate" : null,											<br>
	 *   "states" : [ "AZ", "UT" ],									<br>
	 *   "scopeMin" : 0,											<br>
	 *   "scopeMax" : null,											<br>
	 *   "productAndManufacturer" : null,							<br>
	 *   "classificationType" : 0,									<br>
	 *   "classifications" : null,									<br>
	 *   "distanceZip" : 0,											<br>
	 *   "distanceMiles" : 0,										<br>
	 *   "name" : "search1"											<br>
	 * }"										                    <br>
	 * @return FilterCriteria
	 */
    private FilterCriteria createFilterCriteriaFromStringForDatabaseQueryReturn(String strDatabaseQuery) {
		Map <String, List<String>> dbQuery = new HashMap<>();
		
		if (!strDatabaseQuery.isEmpty()) {
			System.out.println(GlobalVariables.getTestID() + " INFO: Formatting database query into Map");
			// First, parse the string containing the database query

			for ( String strResult : strDatabaseQuery.replace("{","").replace("}","").trim().split("\n  ")) {
				// Each line is of the form: column_name : value
				String [] thisValue = strResult.split(":");
				String key = "";

				// If column_name corresponds to a filter name, then format the column_name 
				// to match our filter names and then save as a key for the HashMap. 
				switch (thisValue[0].trim().replaceAll("\"", "")){
					// keywords
					case "keywords" :
						key = "Keywords";
						break;
					case "negKeywords":
						key = "Negative Keywords";
						break;
					case "states" :
						key = "States/Provinces";
						break;
					case "startDate" :
						key = "Show Bids From";
						break;
					case "endDate" :
						key = "Show Bids to";
						break;
					case "name" :
						key = "Filter Name";
						break;
					default:
						break; }
					
				// If we just saved a key out, then we want to map it to its corresponding value
				if (!key.isEmpty()) {
					List <String> values = new ArrayList<>();
					
					// We are looking at a string of keywords or a single entry
					// form: [ "key1", "key2", .... , "keyx" ] | "entry"
					for (String keyword : thisValue[1].replace("[", "").replace("]","").split(",")) {

						// Although time is recorded out to the decimal, we are only displaying
						// dates to the user in MM/DD/YYYY form, so remove extra precision
						if (key.equals("Show Bids to") | key.equals("Show Bids From")) {
							keyword = keyword.replaceAll(":\\d{2}:\\d{2}\\.\\d{3}Z{1}", "").replaceAll("T{1}\\d{2}", ""); 
							
						}

						String formatKeyword = keyword.replace("\"", "").trim();
						if (!formatKeyword.equals("null") & !formatKeyword.equals(" ") & !formatKeyword.isEmpty() & formatKeyword.length() != 0 ) {
							values.add(formatKeyword);
						} else {
							// No value was retrieved, so we'll just input the empty list
							continue;
						}

					dbQuery.put(key, values); }
				}
			}
			
			// Now determine the Time Frame from the Start Date and End Date
			// If there is no start and end date, then we haven't set a time frame
			List <String> timeFrameType = new ArrayList<>();
			if (dbQuery.get("Show Bids From") != null & dbQuery.get("Show Bids to") != null) {
				if (!dbQuery.get("Show Bids From").get(0).isEmpty() & !dbQuery.get("Show Bids to").get(0).isEmpty()){
					
					// Set the time frame
					String showBidsTo   = dbQuery.get("Show Bids to").get(0);
					String showBidsFrom = dbQuery.get("Show Bids From").get(0);
					timeFrameType.add(getTimeFramePreSelectionFromDates(showBidsFrom, showBidsTo).toString());
					
					// and format the Show Bids to and Show Bids From dates to match how ARGO displays
					// Database returns in form YYYY-MM-DD, but ARGO app displays dates as MM/DD/YYYY
					// Format date to match MM/DD/YYYY
					dbQuery.get("Show Bids to").  set(0, dateHelpers.flipDateYearFirst(showBidsTo.replace("\"", "").trim().replaceAll("-", "/")));
					dbQuery.get("Show Bids From").set(0, dateHelpers.flipDateYearFirst(showBidsFrom.replace("\"", "").trim().replaceAll("-", "/")));
				}

				dbQuery.put("Time Frame", timeFrameType);
				

			} else {
				timeFrameType.add("None");
				dbQuery.put("Time Frame", timeFrameType);
			}
		}
		
		return this.putHashMapIntoFilterCriteria(dbQuery);
    }
    
}