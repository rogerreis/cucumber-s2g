package pages.chargebeeSite.subscriptions.subscriptionDetails.cancelSubscriptionFlow;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.chargebeeSite.subscriptions.subscriptionDetails.SubscriptionDetails;
import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>CancelSubscriptionVerificationPopup</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects that appears the "The subscription will be cancelled immediately" popup<br>
 * 	that appears under the Subscriptions->Subscription Details screen when the user clicks on "Cancel Immediately"</p>
 * <p>Note: Class is currently under refactor</p>
 */
public class CancelSubscriptionVerificationPopup extends BasePageActions {
	// xpaths -- add as needed
	private final String confirmBtn_xpath = "//input[@value='Confirm'][@type='submit']";
	
	// Other
	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;

	public CancelSubscriptionVerificationPopup (EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
		helperMethods = new HelperMethods();
	}
	
	/* ----- getters ----- */
	private WebElement getConfirmBtn () {return driver.findElement(By.xpath(this.confirmBtn_xpath));}
	
	/* ----- methods ----- */
	/***
	 * <h1>clickConfirmBtn</h1>
	 * <p>purpose: Click the confirm button on the "The subscription will be cancelled immediately" popup </p>
	 * @return
	 */
	public SubscriptionDetails clickConfirmBtn() {
		waitForVisibility(By.xpath(this.confirmBtn_xpath));
		helperMethods.addSystemWait(1);
		this.getConfirmBtn().click();
		waitForInvisibility(By.xpath(this.confirmBtn_xpath), 5); // this can take a REALLY long time
		return new SubscriptionDetails(driver);
	}

}
