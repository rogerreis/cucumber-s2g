package pages.buyspeed.agency.documents.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.buyspeed.agency.AgencyTopAndSideBars;

/***
 * <h1>Class ApprovalsScreen</h1>
 * @author dmidura
 * <p>details: This class holds page objects and methods for a generic approvals page
 */
public class ApprovalsScreen extends AgencyTopAndSideBars {
	
	// xpaths
	private final String automaticallyApproveRadioButton_xpath = "//input[@type='radio'][@value='AutoApprove']";
	private final String saveAndContinueBtn_xpath              = "//input[@type='button'][@value='Save & Continue']";	

	public ApprovalsScreen(EventFiringWebDriver driver) {
		super(driver);
	}
	
	/* ----- getters ----- */
	private WebElement getAutomaticallyApproveRadioButton() { return findByVisibility(By.xpath(this.automaticallyApproveRadioButton_xpath)); }
	private WebElement getSaveAndContinueBtn()              { return findByVisibility(By.xpath(this.saveAndContinueBtn_xpath));              }
	
	/* ----- methods ----- */
	
	/***
	 * <h1>selectAutomaticApproval</h1>
	 * <p>purpose: Click on the "Automatic approval" radio button on the Approvals page</p>
	 * @return ApprovalsScreen
	 */
	public ApprovalsScreen selectAutomaticApproval() {
		this.getAutomaticallyApproveRadioButton().click();
		return this;
	}
	
	/***
	 * <h1>clickSaveAndContinueBtn</h1>
	 * <p>purpose: Click on the "Save & Continue" button on the Bid Bidders tab</p>
	 * @return ApprovalsScreen
	 */
	public ApprovalsScreen clickSaveAndContinueBtn() {
		this.getSaveAndContinueBtn().click();
		return this;
	}

	/***
	 * <h1>automaticallyApprove</h1>
	 * <p>purpose: Select "Automatically approve" radio button, and then <br>
	 * 	click on the "Save & Continue" button on the Bid Bidders tab</p>
	 * @return ApprovalsScreen
	 */
	public void automaticallyApprove() {
		this.selectAutomaticApproval()
			.clickSaveAndContinueBtn();
	}
	
	
}
