/**
 * 
 */
package pages.bidSyncPro.supplier.account.accountSettings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.pageObjects.toBeDeprecated.Document;

/**
 * @author jlara
  * <p>TODO: Refactor this class</p>*
 */
public class ChangeEmailModal_Step2 extends Document{

	private final String xpathModal = "//bidsync-change-email/div//mat-card[@id='matChangeEmailComponentCard']";
	
	//text: Verify
	private final String xpathHeaderLabel = xpathModal + "//mat-card-title[@id='matChangeEmailTitle']";
	//text: To confirm the change, you will receive a verification email to your new email address.
	private final String xpathDescriptionLabel = xpathModal + "//mat-card-content/p";
	
	//conatins text: CLOSE
	private final String xpathCloseButton = xpathModal + "//mat-dialog-actions/button[@id='btnConfirm']";
	
	//snackbar text: 'Email was sent, please check your email. '
	private final String xpathEmailSentSnackBar = "//snack-bar-container/simple-snack-bar[text()='Email was sent, please check your email. ']";
	
	
	public ChangeEmailModal_Step2(EventFiringWebDriver driver) {
		super(driver);

		this.setPagePath(System.getProperty("supplierBaseURL") + "admin/agency-interaction");
	}


	public AccountSettings clickCloseButton() {
		getCloseButton().click();
		return new AccountSettings(driver);
	}

	private WebElement getHeaderLabel() {	return driver.findElement(By.xpath(xpathHeaderLabel));	}
	private WebElement getDescriptionLabel() {	return driver.findElement(By.xpath(xpathDescriptionLabel));	}
	private WebElement getCloseButton() {	return driver.findElement(By.xpath(xpathCloseButton));	}
	private WebElement getEmailSentSnackBar() {	return driver.findElement(By.xpath(xpathEmailSentSnackBar));	}

	

}
