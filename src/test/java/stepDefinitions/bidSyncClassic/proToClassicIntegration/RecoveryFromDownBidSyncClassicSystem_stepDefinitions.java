package stepDefinitions.bidSyncClassic.proToClassicIntegration;

import java.util.UUID;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.bidSyncClassic.BSClassicMyRFQLoginPage;
import pages.bidSyncClassic.proToClassicIntegration.DatabaseClassicLinkage;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.common.helpers.GlobalVariables;

public class RecoveryFromDownBidSyncClassicSystem_stepDefinitions {

	private EventFiringWebDriver driver;

	public RecoveryFromDownBidSyncClassicSystem_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
	}

	@And("^I verify that my user was created in Classic$")
	public void I_verify_that_my_user_was_created_in_Classic() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying user was created in BidSync Classic");

		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		String email = user.getEmailAddress();

		new BSClassicMyRFQLoginPage(driver).loginToMyRFQ("mandrews@bidsync.com", "bidsync569").clickSupplierCRMLink()
				.selectUserSearch().searchForUserByEmail(email).verifyMinNumberRecordsReturned(1)
				.selectReturnedUserRecord(email).verifySupplierInformation(user);

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Verified Pro user is in Classic");
	}

	@And("^I will verify that I can punchout to the BidSync bid$")
	public void I_will_verify_that_I_can_punchout_to_the_BidSync_bid() {

		// Assuming that a BidSync bid is displayed on the screen
		// verify that user can punchout.
		new BidList(driver).getFirstVisibleBid().viewDetails().clickGoToBids().verifyPageLoad().clickBackToBids()
				.verifyColumnHeadersLoaded();
	}

	@And("^I retrieve my user_link_id from the ARGO database$")
	public void I_retrieve_my_user_link_id_from_the_ARGO_database() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Retrieving user_link_id from ARGO database");

		// Fail test if configuration of Pro to Classic linkage is wrong
		DatabaseClassicLinkage database = new DatabaseClassicLinkage(driver);
		Assert.assertEquals("ERROR: Pro to Classic linkage is not enabled. Please enable for this test!", true,
				database.getBidSyncClassicLinkEnabledState());

		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		UUID userLinkId = database.getUserLinkIdFromUserName(user.getEmailAddress());
		GlobalVariables.storeObject("userLinkId", userLinkId);
	}

	@When("^I delete my users_link from the ARGO database$")
	public void I_delete_my_users_link_from_the_ARGO_database() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Deleting user_link_id in ARGO database");
		UUID userLinkId = GlobalVariables.getStoredObject("userLinkId");
		new DatabaseClassicLinkage(driver).deleteUserLinkFromDatabase(userLinkId);
	}

	@Then("^I will verify that my user_link_id has been recreated$")
	public void I_will_verify_that_my_user_link_id_has_been_recreated() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that user_link_id was recreated in ARGO database");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		UUID oldUserLinkId = GlobalVariables.getStoredObject("userLinkId");

		UUID userLinkId = new DatabaseClassicLinkage(driver).getUserLinkIdFromUserName(user.getEmailAddress());
		Assert.assertNotEquals("ERROR: Recreated user_link_id is the same as the previous value", oldUserLinkId,
				userLinkId);

		System.out.println(GlobalVariables.getTestID() + " Test Passed: user_link_id was recreated in ARGO database");
	}

	@And("^I log out of classic$")
	public void I_log_out_of_classic() {

		// logout
		driver.switchTo().defaultContent();
		new WebDriverWait(driver, 10).withMessage("ERROR: Could not switch to frame to access \"Logout\"")
				.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("footer"));
		driver.findElement(By.xpath("//img[@title='Logout']")).click();
		// are you sure you want to log out?
		driver.switchTo().alert().accept();
	}
}
