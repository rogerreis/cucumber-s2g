package pages.bidSyncPro.registration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.bidSyncPro.common.BidSyncProCommon;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.common.helpers.GlobalVariables;

/***
 * <h1>Class YourBidsAreReady</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects related to the objects on the "Your Bids are Ready" screen that is part of supplier registration</p>
 */
public class YourBidsAreReady extends BidSyncProCommon {

    // xpaths
	
	// Header
	private final String title_xpath         = "//h1[contains(., 'Congratulations, your business profile is complete!')]";
	private final String cardTitle_xpath     = "//mat-card-title[contains(., 'Your Bids are Ready')]";
	
	// Card
	private final String bidsAvailableText_xpath           = "//span[contains(.,\"We've created a list of bids, just for you!\")]";
	private final String takeMeToTheBidListButton_xpath    = "//button[@id='AllDone'][contains(span, 'TAKE ME TO THE BID LIST')]";
	private final String additionalOptionsText_xpath       = "//span[contains(.,'You can use the additional edit options now or later in the settings to further refine your bid list.')]";
	
	// Bottom Buttons
	private final String increaseBidRelevancyButton_xpath  = "//button[@id='aAdvancedProfileSetup'][contains(span, 'INCREASE BID RELEVANCY')]";
	private final String editCommodityCodesButton_xpath    = "//button[@id='NIGP'][contains(span, 'EDIT COMMODITY CODES')]";
    
    // Footer
	private final String footerPhone_xpath   = "//footer[@id='fxFooterLayout']//span[contains(.,'(800) 990-9339')]";
	private final String footerEmail_xpath   = "//footer[@id='fxFooterLayout']//span[contains(.,'s2g-support@periscopeholdings.com')]";
    
	// Other
	private EventFiringWebDriver driver;
	
	public YourBidsAreReady (EventFiringWebDriver driver) {
		super(driver);
		this.driver   = driver;
	}
	
	/* ----- getters ----- */
	
	// Header
    private WebElement getTitle()     { return findByVisibility(By.xpath(this.title_xpath));     }
    private WebElement getCardTitle() { return findByVisibility(By.xpath(this.cardTitle_xpath)); }
	
	// Card
	private WebElement getBidsAvailableText()        { return findByVisibility(By.xpath(this.bidsAvailableText_xpath));        }
	private WebElement getTakeMeToTheBidListButton() { return findByVisibility(By.xpath(this.takeMeToTheBidListButton_xpath)); }
	private WebElement getAdditionalOptionsText()    { return findByVisibility(By.xpath(this.additionalOptionsText_xpath));    }

	// Bottom Buttons
	private WebElement getIncreaseBidRelevancyButton() {
		findByScrollIntoViewBottomOfScreen(By.xpath(this.increaseBidRelevancyButton_xpath));
		return findByClickability   (By.xpath(this.increaseBidRelevancyButton_xpath)); }

	private WebElement getEditCommodityCodesButton() {
		findByScrollIntoViewBottomOfScreen(By.xpath(this.editCommodityCodesButton_xpath));
		return findByClickability   (By.xpath(this.editCommodityCodesButton_xpath)); }
	
	// Footer
    private WebElement getFooterPhone() { return findByVisibility(By.xpath(this.footerPhone_xpath)); }
    private WebElement getFooterEmail() { return findByVisibility(By.xpath(this.footerEmail_xpath)); }

	/* ----- methods ----- */
    
    /***
     * <h1>clickEditCommodityCodes</h1>
     * <p>purpose: Click on the "EDIT COMMODITY CODES" button to navigate to the<br>
     * 	"Select NIGP Codes" screen</p>
     * @return IncreaseBidRelevancy
     */
    public SelectNIGPCodes clickEditCommodityCodes() {
    	this.getEditCommodityCodesButton().click();
    	return new SelectNIGPCodes(driver);
    }
	    
    /***
     * <h1>clickIncreaseBidRelevancyBtn</h1>
     * <p>purpose: Click on the "INCREASE BID RELEVANCY" button to navigate to the<br>
     * 	"Increase Bid Relevancy" screen</p>
     * @return IncreaseBidRelevancy
     */
    public IncreaseBidRelevancy clickIncreaseBidRelevancyBtn() {
    	this.getIncreaseBidRelevancyButton().click();
    	return new IncreaseBidRelevancy(driver);
    }
    
	/***
	 * <h1>clickTakeMeToTheBidListBtn</h1>
	 * <p>purpose: Click on "TAKE ME TO THE BID LIST".<br>
	 * 	After clicking, verify that the user is logged into BidSync for the first time by:<br>
	 * 	1. Verifying that the user is at /dashboard/new-bids<br>
	 * 	2. The "Match" column is displaying in the "New For You" bids<br>
	 * 	Fail test if conditions are not met</p>
	 * @return BidList
	 */
    public BidList clickTakeMeToTheBidListBtn() {
    	System.out.println(GlobalVariables.getTestID() + " INFO: Clicking on \"TAKE ME TO THE BID LIST\"");
        getTakeMeToTheBidListButton().click();
		// Give the app a second to complete first login before returning the Bid List
		// Waiting for URL to display the login 
		// and the "Match" column header in the bid list to display
        int timeout = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, 35)
				.withMessage("Test Timed Out: After clicking on \"TAKE ME TO THE BID LIST\", " +
						"the user was not logged into BidSync and able to view the \"New For You\" bid list within " + timeout + " seconds")
				.until(ExpectedConditions.and(
					ExpectedConditions.urlContains("/dashboard/new-bids"),
					ExpectedConditions.presenceOfElementLocated(By.xpath("//a[contains(text(),'Match')]"))
					));
		   
			return new BidList(driver);
        }
    
    /* ----- verifications ----- */
    
    /***
     * <h1>verifyPageInitialization</h1>
     * <p>purpose: Verify that the following objects are visible on the "Your Bids are Ready" screen:<br>
     * 	1. All page objects<br>
     * 	2. All butttons<br>
     * 	Fail test if any of the above do not load</p>
     * @return YourBidsAreReady
     */
    public YourBidsAreReady verifyPageInitialization() {

    	// Header
    	this.getTitle();
    	this.getCardTitle();

    	// Card
    	this.getAdditionalOptionsText();
    	this.getTakeMeToTheBidListButton();
    	this.getBidsAvailableText();
    	
    	// Buttons
//    	this.getEditCommodityCodesButton(); --Removed as part of BIDS-234
    	this.getIncreaseBidRelevancyButton();
    	
    	// Footer
    	this.getFooterEmail();
    	this.getFooterPhone();

    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"Your Bids Are Ready\" initialized");
    	return this;
    }
}
