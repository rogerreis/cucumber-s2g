package pages.bidSyncPro.registration;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import data.bidSyncPro.registration.RandomChargebeeUser;
import data.bidSyncPro.registration.SubscriptionPlan;
import pages.bidSyncPro.common.BidSyncProCommon;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.helpers.RandomHelpers;

/***
 * <h1>Class CreateNewCompany</h1>
 * <p>details: "Create New Company" screen appears in the registration flow after user has entered a company<br>
 * 	into the company lookup, but does not return results OR if this is the first user to join an existing (clearbit)<br>
 * 	company. The user's address information is then ported onto the Create New Company page</p>
 */
public class CreateNewCompany extends BidSyncProCommon {
    
    private EventFiringWebDriver driver;
    // xpaths
    
    // Header
    private final String title_xpath         = "//h1[contains(., 'Create New Company')]";
    private final String summaryTxt_xpath    = "//div[@id='joinOrCreateCompanyProfile']" +
                                               "//span[contains(., 'This information helps agencies find you and invite you to participate in bids!')]";
    private final String cardTitle_xpath     = "//mat-card-title[@id='createMatSendRequestTitle'][contains(., 'Company Information')]";
    
    // Labels
    private final String companyNameLabel_xpath   = "//label[@for='companyName'][contains(.,'Company Name')]";
    private final String selectCountryLabel_xpath = "//label[@for='companyCountry'][contains(.,'Select Country')]";
    private final String stateProvinceLabel_xpath = "//label[@for='companyState'][contains(.,'State/Province')]";
    private final String cityLabel_xpath          = "//label[@for='city'][contains(.,'City')]";
    private final String postalCodeLabel_xpath    = "//label[@for='postal'][contains(.,'Postal Code')]";
    private final String addressLabel_xpath       = "//label[@for='address'][contains(.,'Address')]";
    private final String addressLine2Label_xpath  = "//label[@for='address2'][contains(.,'Address Line 2')]";
    
    // Inputs
    private final String companyNameInput_xpath         = "//input[@id='companyName']";
    private final String countryDropdown_xpath          = "//mat-select[@id='companyCountry']//span//span";
    private final String stateProvinceDropdown_xpath    = "//mat-select[@id='companyState']//span//span";
    private final String stateInput_id					="companyState";
    private final String cityInput_xpath                = "//input[@id='city']";
    private final String postalCodeInput_xpath          = "//input[@id='postal']";
    private final String addressInput_xpath             = "//input[@id='address']";
    private final String addressLine2Input_xpath        = "//input[@id='address2']";
    
    // Buttons
    private final String nextButton_xpath               = "//button[@id='CreateNextButton']";
    private final String nextButton2_xpath               = "//button[@id='CreateNextButton2']";
    private final String backButton_xpath               = "//button[@id='createBbackButton']";
    
    // Footer
    private final String footerPhone_xpath              = "//footer[@id='fxFooterLayout']//span[contains(.,'(800) 990-9339')]";
    private final String footerEmail_xpath              = "//footer[@id='fxFooterLayout']//span[contains(.,'s2g-support@periscopeholdings.com')]";
    
    public CreateNewCompany(EventFiringWebDriver driver) {
    	super(driver);
        this.driver   = driver;
    }
    
    /* ----- getters ----- */
    
    // Header
    private WebElement getSummaryTxt() { return findByVisibility(By.xpath(this.summaryTxt_xpath)); }
    private WebElement getCardTitle()  { return findByVisibility(By.xpath(this.cardTitle_xpath));  }
    private WebElement getTitle()      { return findByVisibility(By.xpath(this.title_xpath));      }

    // Labels
    private WebElement getCompanyNameLabel()   { return findByVisibility(By.xpath(this.companyNameLabel_xpath));   }
    private WebElement getSelectCountryLabel() { return findByVisibility(By.xpath(this.selectCountryLabel_xpath)); }
    private WebElement getStateProvinceLabel() { return findByVisibility(By.xpath(this.stateProvinceLabel_xpath)); }
    private WebElement getCityLabel()          { return findByVisibility(By.xpath(this.cityLabel_xpath));          }
    private WebElement getPostalCodeLabel()    { return findByVisibility(By.xpath(this.postalCodeLabel_xpath));    }
    private WebElement getAddressLabel()       { return findByVisibility(By.xpath(this.addressLabel_xpath));       }
    private WebElement getAddressLine2Label()  { return findByVisibility(By.xpath(this.addressLine2Label_xpath));  }

    // Inputs
    private WebElement getCompanyNameInput()      { return findByVisibility(By.xpath(this.companyNameInput_xpath));      }
    private WebElement getCountryDropdown()       { return findByVisibility(By.xpath(this.countryDropdown_xpath));       }
    private WebElement getStateInput()			  { return findByVisibility(By.id(this.stateInput_id));					 }
    private WebElement getStateProvinceDropdown() { return findByVisibility(By.xpath(this.stateProvinceDropdown_xpath)); }
    private WebElement getCityInput()             { return findByVisibility(By.xpath(this.cityInput_xpath));             }
    private WebElement getPostalCodeInput()       { return findByVisibility(By.xpath(this.postalCodeInput_xpath));       }
    private WebElement getAddressInput()          { return findByVisibility(By.xpath(this.addressInput_xpath));          }
    private WebElement getAddressLine2Input()     { return findByVisibility(By.xpath(this.addressLine2Input_xpath));     }
   
    // Buttons
    private WebElement getNextButton() { return findByVisibility(By.xpath(this.nextButton_xpath)); }
    private WebElement getNextButton2() { return findByVisibility(By.xpath(this.nextButton2_xpath)); }
    private WebElement getBackButton() { return findByVisibility(By.xpath(this.backButton_xpath)); }
    
    // Footer
    private WebElement getFooterPhone() { return findByVisibility(By.xpath(this.footerPhone_xpath)); }
    private WebElement getFooterEmail() { return findByVisibility(By.xpath(this.footerEmail_xpath)); }
    
    /* ----- helpers ----- */
    /***
     * <h1>getInputDisplayedValue</h1>
     * <p>purpose: Return the currently displayed value in an input field</p>
     * @param ele WebElement for the input field
     * @return String with displayed value | null if nothing is currently displayed
     */
    private String getInputDisplayedValue(WebElement ele) {
    	return ele.getAttribute("value");
    }
    
    /* ----- methods ----- */
    /***
     * <h1>acceptDefaultsAndContinue</h1>
     * <p>purpose: Verify that the "Create New Company" screen has initialized<br>
     * 	with all expected objects. Each of the inputs and dropdowns should be displaying<br>
     * 	values. After verifying that inputs and dropdowns display values, click "NEXT"<br>
     * 	Note: This method does not validate that the inputs and dropdowns are defaulting to<br>
     * 	registration values, just that they are displaying text</p>
     * @return CompanyAdded
     */
    public CompanyAdded acceptDefaultsAndContinue() {
    	new HelperMethods().addSystemWait(1);
    	this.verifyPageInitialization();
        this.getNextButton().click();
        return new CompanyAdded(driver);
    }
    
    /* ----- verifications ----- */
    
    /***
     * <h1>verifyUserDataDisplays</h1>
     * <p>purpose: Verify that the fields for "Create New Company" screen<br>
     * 	are populated with the user registration data. Fail test if any of the fields<br>
     * 	is not correctly populated with user registration data. Fields include:<br>
     * 	1. Company Name<br>
     * 	2. Address Line1, Address Line2<br>
     * 	3. City, State, Zip<br>
     * 	4. Country</p>
     * @param userData = RandomChargeeUser that user registered with
     * @return CreateNewCompany
     */
    public CreateNewCompany verifyUserDataDisplays(RandomChargebeeUser userData) {

    	Assert.assertEquals("Test Failed: Incorrect value of Company Name",  userData.getCompany().getName(), this.getInputDisplayedValue(this.getCompanyNameInput()) );
    	Assert.assertEquals("Test Failed: Incorrect value of Address Line1", userData.getAddress(),           this.getInputDisplayedValue(this.getAddressInput())     );
    	Assert.assertEquals("Test Failed: Incorrect value of State",         userData.getState(),             this.getStateProvinceDropdown().getText()               );
    	Assert.assertEquals("Test Failed: Incorrect value of City",          userData.getCity(),              this.getInputDisplayedValue(this.getCityInput())        );
    	Assert.assertEquals("Test Failed: Incorrect value of Postal Code",   userData.getZipCode(),           this.getInputDisplayedValue(this.getPostalCodeInput())  );
    	Assert.assertEquals("Test Failed: Incorrect value of Country",       userData.getCompany().getCountry(),this.getCountryDropdown().getText()                     );

    	// Basic users can only register with Address Line1 through the Masonry site
    	if (userData.getSubscriptionPlan().equals(SubscriptionPlan.BASIC)) {
    		Assert.assertTrue("Test Failed: Address Line2 should be blank for Basic user", this.getInputDisplayedValue(this.getAddressLine2Input()).isEmpty());
    	/// All other subscriptions register an Address Line2
    	} else {
    		Assert.assertEquals("Test Failed: Incorrect value of Address Line2", userData.getAddressLine2(),      this.getInputDisplayedValue(this.getAddressLine2Input()));}

    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"Create New Company\" screen is correctly populated with user registration data");
    	return this;
    }
    
    /***
     * <h1>verifyCompanyNameNotEditable</h1>
     * <p>purpose: Verify that the Company field cannot be edited. Fail test if field is editable</p>
     * @return CreateNewCompany
     */
    public CreateNewCompany verifyCompanyNameNotEditable() {
    	Assert.assertFalse("Test Failed: Company Name field is editable", this.getCompanyNameInput().isEnabled());

    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified that Company Name field cannot be edited on \"Create New Company\" screen");
    	return this;
    }
    
    /***
     * <h1>verifyAddressFieldsEditable</h1>
     * <p>purpose: Verify that the address field can be edited. Fail test if any field is not editable.<br>
     * 	Fields include:<br>
     * 	1. Address Line1, Address Line2<br>
     * 	2. City, State, Zip<br>
     * 	3. Country</p>
     * @return CreateNewCompany
     */
    public CreateNewCompany verifyAddressFieldsEditable() {
    	Assert.assertTrue("Test Failed: Address Line1 field is not editable", this.getAddressInput().isEnabled());
    	Assert.assertTrue("Test Failed: Address Line2 field is not editable", this.getAddressLine2Input().isEnabled());
    	Assert.assertTrue("Test Failed: City field is not editable",          this.getCityInput().isEnabled());
    	Assert.assertTrue("Test Failed: State field is not editable",         this.getCityInput().isEnabled());
    	Assert.assertTrue("Test Failed: Postal Code field is not editable",   this.getPostalCodeInput().isEnabled());
    	Assert.assertTrue("Test Failed: Country field is not editable",       this.getPostalCodeInput().isEnabled());

    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified that Company Name field cannot be edited on \"Create New Company\" screen");
    	return this;
    }
 
    /***
     * <h1>verifyPageInitialization</h1>
     * <p>purpose: Verify that the following are present when page initializes:<br>
     * 	1. All form labels<br>
     * 	2. Objects exist for all inputs and dropdowns<br>
     * 	   (Note: use verifyUserDataDisplays() method to validate quality of data display)<br>
     *  3. Buttons</p>
     * @return CreateNewCompany
     */
    public CreateNewCompany verifyPageInitialization() {
    	
    	// Header Elements
    	this.getTitle();
    	this.getCardTitle();
    	this.getSummaryTxt();
    	// Labels
    	this.getCompanyNameLabel();
    	this.getSelectCountryLabel();
    	this.getStateInput();
//    	this.getStateProvinceLabel();//when auto populating data, could not find this-:added stateInput() instead
    	this.getCityLabel();
    	this.getPostalCodeLabel();
    	this.getAddressLabel();
    	this.getAddressLine2Label();
    	
    	// Inputs
    	this.getCompanyNameInput();
    	this.getCountryDropdown();
    	this.getStateProvinceDropdown();
    	this.getCityInput();
    	this.getPostalCodeInput();
    	this.getAddressInput();
    	this.getAddressLine2Input();
    	
    	// Buttons
    	this.getNextButton();
    	this.getBackButton();
    	
    	// Footer Elements
    	this.getFooterEmail();
    	this.getFooterPhone();
    	
    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"Create New Company\" (form) page initialized.");
    	return this;
    }

	public CreateNewCompany clickNextButton() {
		this.getNextButton().click();
		return this;
	}
    
	public CreateNewCompany clickNextButton2() {
		this.getNextButton2().click();
		return this;
	}
    
	public CreateNewCompany inputRandomState(String state){
		getStateInput().click();
		WebElement elm = findByVisibility(By.xpath("//mat-option[3]"));
		elm.click();
		GlobalVariables.addGlobalVariable("selectedState", elm.getText());
		return this;
	}
	
	public CreateNewCompany verifyStateCanBeEditable(){
		String xpath = "//mat-select[@id='companyState']//div[@class='mat-select-value']//span[contains(text(),'"+GlobalVariables.getGlobalVariable("selectedState")+"')]";
		WebElement stateElment = findByVisibility(By.xpath(xpath));
		new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("State can not be editable")
		.until(ExpectedConditions.visibilityOf(stateElment));
		return this;
	}
	
	
}
