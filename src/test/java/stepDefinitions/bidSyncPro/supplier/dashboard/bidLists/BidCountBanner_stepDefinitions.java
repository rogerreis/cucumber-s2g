package stepDefinitions.bidSyncPro.supplier.dashboard.bidLists;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidCountBanner;
import pages.common.helpers.GlobalVariables;

public class BidCountBanner_stepDefinitions {
	
	private EventFiringWebDriver driver;
	private BidCountBanner bidCountBanner;
	public BidCountBanner_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
		bidCountBanner = new BidCountBanner(driver);
	}

	@When ("^I close the Bid Count Banner$")
	public void I_close_the_Bid_Count_Banner() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on \"close\" button to close Bid Count Banner");
		bidCountBanner.clickCloseButton();
	}

	@And ("^I will verify that the Bid Count Banner is correctly displaying information$")
	public void I_will_verify_that_the_Bid_Count_Banner_is_correctly_displaying_information() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying Bid Count Banner is correctly displaying");
		bidCountBanner.verifyBidCountBannerInitialization();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Bid Count Banner is correctly displaying");
	}
	
	@Then ("^I will verify that the Bid Count Banner is visible$")
	public void I_will_verify_that_the_Bid_Count_Banner_is_visible() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify Bid Count Banner is displaying");
		bidCountBanner.verifyBidCountBannerDisplays();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Bid Count Banner is displaying");
	}
	
	@Then ("^I will verify that the Bid Count Banner is not visible$")
	public void I_will_verify_that_the_Bid_Count_Banner_is_not_visible() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify Bid Count Banner is not displaying");
		bidCountBanner.verifyBidCountBannerDoesNotDisplay();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Bid Count Banner does not display");
	}

	@When ("^I retrieve the Bid Count Banner value for a generic search with \"(.*)\" bid access$")
	public void I_retrieve_the_Bid_Count_Banner_value_for_a_generic_search_with_user_bid_access(String userName){
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Retrieving total and access counts from Bid Count Banner for user \"" + userName + "\"");

		// Navigate to All Bids and run a generic search for "water"
		bidCountBanner.searchFor("water");

		// Extract counts from Banner Count Banner
		String accessCount = String.format("%sAccessCount", userName);
		String totalCount  = String.format("%sTotalCount", userName);
		GlobalVariables.storeObject(accessCount, bidCountBanner.getAccessBidsCount());
		GlobalVariables.storeObject(totalCount, bidCountBanner.getTotalBidsCount());
	}

	@Then ("^I will verify that \"(.*)\" can access more bids than \"(.*)\"$")
	public void I_will_verify_that_user1_can_access_more_bids_than_user2(String user1, String user2) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that \"" + user1 + "\" user can access more bids than \"" + user2 + "\" user");
		String user1Txt = String.format("%sAccessCount", user1);
		String user2Txt = String.format("%sAccessCount", user2);
		int user1AccessCount = GlobalVariables.getStoredObject(user1Txt);
		int user2AccessCount = GlobalVariables.getStoredObject(user2Txt);
				
		String errorMsg = String.format("Test Failed: User \"%s\" accesses <%s> bids, but User \"%s\" accesses <%s> bids", user1, user1AccessCount, user2, user2AccessCount);
		Assert.assertTrue(errorMsg, user1AccessCount > user2AccessCount);
		System.out.println(GlobalVariables.getTestID() + " Test Passed: \"" + user1 + "\" user can access more bids (" + user1AccessCount + ") than \"" + user2 + "\" user (" + user2AccessCount + ")");
	}
	
	@And ("^I will verify that the total number of bids listed on the Bid Count Banner is the same for \"(.*)\" and \"(.*)\"$")
	public void I_will_verify_that_the_total_number_of_bids_listed_on_the_Bid_Count_Banner_is_the_same_for_both_users(String user1, String user2) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that \"" + user1 + "\" user sees same bids total as \"" + user2 + "\" user");
		String user1Txt = String.format("%sTotalCount", user1);
		String user2Txt = String.format("%sTotalCount", user2);
		int user1TotalCount = GlobalVariables.getStoredObject(user1Txt);
		int user2TotalCount = GlobalVariables.getStoredObject(user2Txt);
				
		String errorMsg = String.format("Test Failed: User \"%s\" sees <%s> total bids, but User \"%s\" sees <%s> total bids", user1, user1TotalCount, user2, user2TotalCount);
		Assert.assertTrue(errorMsg, user1TotalCount == user2TotalCount);
		System.out.println(GlobalVariables.getTestID() + " Test Passed: \"" + user1 + "\" user sees same total number of bids (" + user1TotalCount + ") as \"" + user2 + "\" user (" + user2TotalCount + ")");
		
	}

	 @Then("^verify the same number of bids are available in the bid list for user \"(.*)\"$")
	    public void verifySameNumberOfBidsAvailableInBidList(String userName){
	    	try{
	    		String userAccessCountTxt = String.format("%sAccessCount", userName);
	    		String userTotalCountTxt  = String.format("%sTotalCount", userName);
	    		int accessCount = GlobalVariables.getStoredObject(userAccessCountTxt);
	    		int totalCount = GlobalVariables.getStoredObject(userTotalCountTxt);
	    		bidCountBanner.verifyBidCount(accessCount, totalCount);
	    	}catch(Exception exp){
	    		fail("Bid list are mismatching after sorting the results");
	    	}
	    }
}
