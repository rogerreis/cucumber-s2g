package stepDefinitions.bidSyncClassic.agency.createBid;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import cucumber.api.java.en.And;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;


public class CreateExtractorBid_stepDefinitions {	
		public CoreAutomation automation;
		public EventFiringWebDriver driver;	

		public CreateExtractorBid_stepDefinitions(CoreAutomation automation) throws Throwable {
		this.automation = automation;
		this.driver = automation.getDriver();		
	}

	@Given("^I navigate my browser to the BidSync internal login page$")
		public void iNavigateToTheBidSyncLoginPage() throws Throwable {
			try {
			driver.get("https://classic.phi-stage.cloud/MyRFQ");
			//automation.maximizeBrowser();
			//System.out.print("Navigating to login page\n");
			} catch (Exception e) {	
				Assert.fail("Unable to nagivate to login page.\n");
			}
		}	
	
	
	//a[contains(text(),'Bid Extract')]
	@Then("^I will create a extrator new bid$")
	public void I_will_create_a_new_extractor_bid() {
		System.out.println("Ready to create a new extractor Bid.\n");		
		
		//a[contains(text(),'Bid Extract')]
		
		driver.switchTo().frame("docable");
		driver.findElement(By.xpath("//a[contains(text(),'Bid Extract']")).click();		
		driver.findElement(By.xpath("//a[contains(text(),'Search']")).click();
	}
	//a[contains(text(),'Bid Extract')]	
	
	
}
	