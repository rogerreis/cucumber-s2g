package stepDefinitions.bidSyncPro.registration;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.bidSyncPro.registration.SelectNIGPCodes;
import pages.common.helpers.GlobalVariables;
import static org.junit.Assert.fail;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.util.Arrays;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SelectNIGPCodes_stepDefinitions {
	
	private EventFiringWebDriver driver;
	private SelectNIGPCodes selectNigpCodes;
	
	public SelectNIGPCodes_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
		selectNigpCodes = new SelectNIGPCodes(driver);
	}
	
	@When("^I will verify keywords added during registration populated$")
    public void I_will_verify_keywords_added_during_registration_populated() {
    	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify “Use keywords in my profile” button populates keywords added during supplier registration");
    	RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
    	
    	List<String> nigpKeywords = selectNigpCodes.clickUseKeywordsInMyProfileButton().getNIGPKeywords(); 
    	Assert.assertTrue("Expected NIGP Keywords not displayed.. "+new ArrayList<String>(user.getPositiveKeywords()).removeAll(new ArrayList<String>(nigpKeywords)), nigpKeywords.containsAll(user.getPositiveKeywords()));
    }
    
    @When("^I will verify NIGP codes addition and removal$")
    public void I_will_verify_NIGP_codes_addition_and_removal() {
    	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verify ability to add/remove NIGP codes");
    	int nigpCodesToAdd = 3;
    	int nigpCodesToRemove = 1;
    	int addedNIGPCodesCount = selectNigpCodes.selectAvailableNIGPCodes(nigpCodesToAdd).returnSelectedNIGPCodesCount();
    	Assert.assertTrue("Added NIGP Codes not showing as selected", addedNIGPCodesCount==nigpCodesToAdd);
    	addedNIGPCodesCount = selectNigpCodes.removeSelectedNIGPCodes(nigpCodesToRemove).returnSelectedNIGPCodesCount();
    	Assert.assertTrue("NIGP codes expected : "+(nigpCodesToAdd-nigpCodesToRemove)+" but available is : "+addedNIGPCodesCount, addedNIGPCodesCount==nigpCodesToAdd-nigpCodesToRemove);
    	selectNigpCodes.clickOnSaveAndContinueButton();
    }
	
	
	@Then ("^I will verify that the Select NIGP Codes screen has properly intialized in modal dialog window$")
	public void I_will_verify_that_the_Select_NIGP_Codes_screen_has_properly_initialized_in_modal_dialog_windo() {
		try {
			System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that \"Select NIGP Codes\" screen has properly initialized");
			selectNigpCodes.verifyPageInitializationinModalDialogWindow();
			System.out.println(GlobalVariables.getTestID() + " Test Passed: \"Select NIGP Codes\" screen has properly initialized");
		}catch(Exception exp) {
			fail("Not able to verify NIGP code page initialization");
		}
	}
	
//	Commented bcos no usage found
	
//	@Then("^I will verify that the Select NIGP Codes screen has properly intialized$")
//	public void I_will_verify_that_the_Select_NIGP_Codes_screen_has_properly_initialized() {
//		try {
//			System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that \"Select NIGP Codes\" screen has properly initialized");
//			selectNigpCodes.verifyPageInitialization();
//			System.out.println(GlobalVariables.getTestID() + " Test Passed: \"Select NIGP Codes\" screen has properly initialized");
//		} catch (Exception exp) {
//			fail("Not able to verify NIGP code page initialization");
//		}
//	}
//	
//	@Then ("^I will verify that the Select NIGP Codes screen has properly intialized in dialog window$")
//	public void I_will_verify_that_the_Select_NIGP_Codes_screen_has_properly_initialized_in_dialog_window() {
//		try {
//			System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that \"Select NIGP Codes\" screen has properly initialized");
//			selectNigpCodes.verifyPageInitialization();
//			System.out.println(GlobalVariables.getTestID() + " Test Passed: \"Select NIGP Codes\" screen has properly initialized");
//		}catch(Exception exp) {
//			fail("Not able to verify NIGP code page initialization");
//		}
//	}

	
	
	@Then("^I verify the nigp page initialization$")
	public void verifyNigpPageInitialization() {
		try {
			selectNigpCodes.verifyPageInitialization_RegistrationFlow();
		}catch(Exception exp) {
			fail("Not able to verify NIGP code page initialization on registration flow");
		}
	}
	
	@And("^click on edit nigp codes$")
	public void clickOnEditNigpCodes() {
		try {
			selectNigpCodes.clickOnEditNigp();
		}catch(Exception exp) {
			fail("Not able to edit NIGP code");
		}
	}
	
	
	@And("^I will verify that the Select NIGP Codes are populated on bid profile page$")
	public void verifySelectedNigpCodesArePopulatedOnBidProfilePage() {
		try {
			selectNigpCodes.verifyNigpOnBidProfile();
		}catch(Exception exp) {
			fail("Not able to verify selected NIGP code on BidProfile");
		}
	}
	
	@And("^I click on 'Back' button and verify the redirection$")
	public void iClickOnBackButtonAndVerifyTheRedirection() {
		try {
			selectNigpCodes.clickOnBackButton()
							.verifyBackButton();
		}catch(Exception exp) {
			fail("Back button is not redirecting properly");
		}
	}
	
	@Then("enter {string} as keywords")
	public void enterKeywords(String keywordString) {
		try {
			String[] keyword = keywordString.split(",");
			for (int i = 0; i < keyword.length; i++) {
				selectNigpCodes.getSearchNIGPCodesInput().sendKeys(keyword[i]);
				selectNigpCodes.getSearchNIGPCodesInput().sendKeys(Keys.ENTER);
			}

		} catch (Exception exp) {
			fail("Failed to enter the keywords");
		}
	}
	
	@Then("enter {string} as keywords via clipboard paste")
	public void enterKeywordsViaClipboardPaste(String keywordString) {
		try {
			String[] keyword = keywordString.split(",");
			for (int i = 0; i < keyword.length; i++) {
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(keyword[i]), null);
				selectNigpCodes.getSearchNIGPCodesInput().click();
				selectNigpCodes.getSearchNIGPCodesInput().sendKeys(Keys.chord(Keys.CONTROL,"v"));
//				new Actions(driver).sendKeys(Keys.chord(Keys.CONTROL, "v")).perform();
//				new Actions(driver).contextClick(selectNigpCodes.getSearchNIGPCodesInput()).sendKeys(Keys.chord(Keys.COMMAND, keyword[i])).build().perform();
				selectNigpCodes.getSearchNIGPCodesInput().sendKeys(Keys.ENTER);
			}

		} catch (Exception exp) {
			fail("Failed to enter the keywords");
		}
	}
	
	
	

	@Then("^verify 'No Matching Codes' message$")
	public void verifyTheMessage() {
		try {
			selectNigpCodes.verifyNoMatchingCodeMessage(); 
		} catch (Exception exp) {
			fail("Failed to verify 'No Matching NIGP Codes' message");
		}
	}
	
	@Then("I verify that the keywords {string} can be removed one by one")
	public void verifyTheKeywordsCanBeRemovedOneByOne(String keywordString) {
		try {
			String[] keyword = keywordString.split(",");
			selectNigpCodes.verifyKeywordsCanBeRemovedOneByOne(keyword); 
		} catch (Exception exp) {
			fail("Failed to verify 'No Matching NIGP Codes' message");
		}
	}
	
}