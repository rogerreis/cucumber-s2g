package stepDefinitions.bidSyncPro.internalAdmin;

import core.CoreAutomation;

import org.junit.Assert;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.events.EventFiringWebDriver;

/***
 * <h1>CreateNewAgency</h1>
 * @author kmatheson
 * <p>details: Creates a new BidSync Pro agency and searches for it and users<br>  	
 */

public class CreateNewAgencyandSearch_stepDefinitions {

	public CoreAutomation automation;
	public EventFiringWebDriver driver;
	private String agencyName;
	private HelperMethods helperMethods;
	
	public CreateNewAgencyandSearch_stepDefinitions(CoreAutomation automation) {
		this.automation = automation;
		this.driver = automation.getDriver();
		this.helperMethods = new HelperMethods();
	}
	
	@When("^I create a new agency as \"([^\"]*)\"$")
	public void i_create_a_new_agency_as (String arg1) {
	System.out.println(GlobalVariables.getTestID() + " Ready to create new agency.\n");	
		//Click Bid Central followed by Agencies		
		driver.findElement(By.xpath("//mat-icon[contains(.,'settings_input_composite')]")).click();
		driver.findElement(By.xpath("//button[@id='btnAddAgency']")).click();	
		//Now click Add Agency	
		helperMethods.addSystemWait(15);
		//switchToWindow("BidSync Admin");
		
		System.out.println(GlobalVariables.getTestID() + " Enter agency info.\n");	
		driver.findElement(By.id("agencyName")).sendKeys(arg1);
		helperMethods.addSystemWait(5);
		driver.findElement(By.id("agencyCity")).sendKeys("American Fork");
		helperMethods.addSystemWait(5);
		driver.findElement(By.id("agencyZip")).sendKeys("84003");
		helperMethods.addSystemWait(5);
		driver.findElement(By.xpath("//span[@class='ng-tns-c16-32 ng-star-inserted']")).sendKeys("Utah");
		helperMethods.addSystemWait(5);
		driver.findElement(By.xpath("//span[contains(.,'SpecialDistrictAuthority')]")).click();
		helperMethods.addSystemWait(5);
		driver.findElement(By.xpath("//span[contains(.,'In-network')]")).click();
		helperMethods.addSystemWait(5);
	}	
	
	//Search for the newly created agency. Get name from feature file
	@When("^I Search for my agency as \"([^\"]*)\"$")
	public void i_search_for_my_new_agency_as (String arg0) {
		agencyName = arg0;	
		//This is the Bid Central link. We have to triple tab back to where we want to be
		driver.findElement(By.xpath("//mat-icon[contains(.,'settings_input_composite')]")).click();	
		driver.findElement(By.tagName("Body")).sendKeys(Keys.TAB);
		driver.findElement(By.tagName("Body")).sendKeys(Keys.TAB);
		driver.findElement(By.tagName("Body")).sendKeys(Keys.TAB);
		driver.findElement(By.id("txtAgencyName")).sendKeys(agencyName);			
		driver.findElement(By.xpath("//button[contains(.,'Search')]")).click();
		System.out.println(GlobalVariables.getTestID() + " Now searching for: "+agencyName+".\n" );
		helperMethods.addSystemWait(5);
	}
	
	@Then("^I will find my new agency")	
	public void i_will_find_my_new_agency() {
	    String message = String.format("Test failed: %s was not found.", agencyName);
	    Assert.assertTrue(message, driver.getPageSource().contains(agencyName));
        System.out.println(String.format("Test passed: %s was found.\n", agencyName));
	}
	
	
	//Load the bid Metrics
	@When("^I select Metrics")
	public void i_select_metrics() {
		driver.findElement(By.xpath("//mat-icon[contains(.,'settings_input_composite')]")).click();
		helperMethods.waitForVisibilityOfId(driver, "aMetrics", 10);
		driver.findElement(By.xpath("//a[@id='aMetrics']")).click();
		helperMethods.addSystemWait(10);
	}
		
	@Then("^I will see the Metrics results")
	public void i_will_see_metrics() {
	    try {
            helperMethods.waitForVisibilityOfXpath(driver, "//span[contains(., 'Parsehub Token')]", 10);
	    } catch(TimeoutException e) {
            Assert.fail("Test failed: Metrics page NOT loaded.");
        }
        System.out.println(GlobalVariables.getTestID() + " Test passed: Metrics page loaded.");
		helperMethods.addSystemWait(5);
	}
	
	
	@When("^I select collected solicitations")
	public void i_select_collected_bids() {
		driver.findElement(By.xpath("//mat-icon[contains(.,'settings_input_composite')]")).click();
		helperMethods.addSystemWait(5);
		driver.findElement(By.xpath("//a[@id='aCollectedSolicitations']")).click();
		String message = "Test failed: Collected Solictions was no loaded.";
		Assert.assertTrue(message, driver.getPageSource().contains("Title"));
        System.out.println(GlobalVariables.getTestID() + " Test passed: Collected Solictions was successfully loaded.\n");
		helperMethods.addSystemWait(5);
	}	
	
	@When("^I search for my user as \"([^\"]*)\"$")
	public void i_search_for_my_user_as (String userName) {
		try {
			driver.findElement(By.xpath("//mat-icon[contains(.,'person')]")).click();
			driver.findElement(By.xpath("//button[@id='btnSearch']")).click();
			driver.findElement(By.tagName("Body")).sendKeys(Keys.TAB);
			driver.findElement(By.xpath("//input[@id='txtUserName']")).sendKeys(userName);
			driver.findElement(By.xpath("//button[@id='btnSearch']")).click();
			helperMethods.addSystemWait(5);
			System.out.println(GlobalVariables.getTestID() + " Looking for user: "+userName+".\n");
			String message = String.format("Test failed: %s was not found.", userName);
			Assert.assertTrue(message, driver.getPageSource().contains(userName));
            System.out.println(GlobalVariables.getTestID() + " Test passed: "+userName+" was found.\n");
		}
		catch (Exception e) {
			Assert.fail("Test failed: An exception occurred");
		}
	}

}