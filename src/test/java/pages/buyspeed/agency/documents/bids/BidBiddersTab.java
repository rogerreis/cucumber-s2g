package pages.buyspeed.agency.documents.bids;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

/***
 * <h1>Class BidBiddersTab</h1>
 * @author dmidura
 * <p>details: This class houses page objects and methods for the bid Bidders tab
 */
public class BidBiddersTab extends BidNavigation {
	
	// xpaths
	// Top
	private final String unrestrictedBidsRadioButton_xpath = "//input[@type='radio'][@name='bidderParticipateType']";
	
	// Bottom Button Panel
	private final String saveAndContinueBtn_xpath = "//input[@type='button'][@value='Save & Continue']";

	public BidBiddersTab (EventFiringWebDriver driver) {
		super(driver);
	}
	
	/* ------ getters ----- */
	// Top
	private WebElement getUnrestrictedBidsRadioButton() { return findByVisibility(By.xpath(this.unrestrictedBidsRadioButton_xpath)); }
	
	// Bottom Button Panel
	private WebElement getSaveAndContinueBtn()          { return findByScrollIntoViewTopOfScreen(By.xpath(this.saveAndContinueBtn_xpath)); }
	
	/* ----- methods ----- */

	/****
	 * <h1>setUnrestrictedBid</h1>
	 * <p>purpose: Click on the "Unrestricted bid, all vendors can view and respond" radio button</p>
	 * @return BidBiddersTab
	 */
	public BidBiddersTab setUnrestrictedBid() {
		this.getUnrestrictedBidsRadioButton().click();
		return this;
	}
	
	/***
	 * <h1>clickSaveAndContinueBtn</h1>
	 * <p>purpose: Click on the "Save & Continue" button on the Bid Bidders tab</p>
	 * @return BidBiddersTab
	 */
	public BidBiddersTab clickSaveAndContinueBtn() {
		this.getSaveAndContinueBtn().click();
		return this;
	}	
}
