@Forgot_Password @must @administration @level3
Feature: Login - Forgot Password
This feature file tests the Forgot your password links
	BIDSYNC-77:  Resetting the password needs to delete the JWT token
	BIDSYNC-68:  Clicking enter on password reset takes you to login
	BIDSYNC-85:  Reset Password returning 500 Error when new password was recently used
	BIDSYNC-121: Password reset can send multiple emails due to unlocked input field
	BIDSYNC-368: Expand Test Coverage of Forgot Password Flows

@BIDSYNC-77
Scenario: If user changes password from "Forgot Password", then JWT token is deleted and then reissued in local storage. User can login.
  Given I have registered a random user
  When I try to change my password from the Forgot Your Password page off the login screen
  Then I will verify that my original JWT token is deleted in local browser storage and a new JWT token is issued
  And That user can log in successfully

 
# New User : so password not in password history  
@BIDSYNC-68 
Scenario: New User :- Enter email address and press enter key for page submission. 
	Given I have registered a random user 
	Given I navigate my browser to the supplier login page 
	When I click Forgot Your Password 
	Then I will be on the Forgot your password page 
	And I enter mail id to reset my password as directed and press enterKey 
	Then I will see the confirmation 
	Then I retrieve my password reset email for new user from Mailinator 
	When I use valid newPassword and confirmPassword 
	Then I should be in bid list page 
	When I navigate my browser to the supplier login page 
	And I enter email and new password 
	Then I will be on the supplier Home page 
	

# Registered user with password not in password history
@BIDSYNC-68 
Scenario Outline: Existing User :- Enter email address and press enter key for page submission. 
	Given I navigate my browser to the supplier login page 
	When I click Forgot Your Password 
	Then I will be on the Forgot your password page 
	And I enter email id "<mailId>" and press enter key 
	Then I will see the confirmation 
	Then I retrieve my password reset email for existing user "<mailId>" from Mailinator 
	When I use valid newPassword and confirmPassword 
	Then I should be in bid list page
	When I navigate my browser to the supplier login page  
	And I enter my "<mailId>" and new password 
	Then I will be on the supplier Home page 
	Examples: 
		|mailId|
		|qaPasswordReset@phimail.mailinator.com|
		
		
@BIDSYNC-368
Scenario: Test to make sure that the page loaded properly 
	When I navigate my browser to the supplier login page 
	And I click Forgot Your Password 
	Then I will be on the Forgot your password page 
	And the page should load properly 
	
@BIDSYNC-368 
Scenario Outline: Test to verify the cancel button functionality 
	When I navigate my browser to the supplier login page 
	And I click Forgot Your Password 
	Then I will be on the Forgot your password page 
	When I enter my email as "<mailId>" 
	And I click Cancel 
	Then I will see the Login Page 
	Examples: 
		|mailId|
		|qaPasswordReset@phimail.mailinator.com|
	
@BIDSYNC-368
Scenario Outline: Test to verify the submit button functionality - button click
	When I navigate my browser to the supplier login page 
	And I click Forgot Your Password 
	Then I will be on the Forgot your password page 
	When I enter my email as "<mailId>" 
	And I click Continue on the Forgot Password screen 
	Then I will see the confirmation
	Then I retrieve my password reset email for existing user "<mailId>" from Mailinator 
	When I use valid newPassword and confirmPassword
	Then I should be in bid list page  
	When I navigate my browser to the supplier login page 
	And I enter my "<mailId>" and new password 
	Then I will be on the supplier Home page 
	Examples: 
		|mailId|
		|qaPasswordReset@phimail.mailinator.com|
		

# Registered user with password in password history
# Resetting password 2 times making sure that the second time we used the password which is in password history
@BIDSYNC-85
Scenario Outline: Existing User :- Enter password which is in password history
	Given I navigate my browser to the supplier login page 
	When I click Forgot Your Password 
	Then I will be on the Forgot your password page 
	And I enter email id "<mailId>" and press enter key 
	Then I will see the confirmation 
	Then I retrieve my password reset email for existing user "<mailId>" from Mailinator 
	When I use valid newPassword and confirmPassword 
	Then I should be in bid list page
	And I logout of BidSync Pro
	And I enter my "<mailId>" and new password 
	Then I should be in bid list page
	And I logout of BidSync Pro
	And I click Forgot Your Password 
	Then I will be on the Forgot your password page 
	And I enter email id "<mailId>" and press enter key 
	Then I will see the confirmation 
	And I wait "8" seconds for the backend to catch up
	Then I retrieve my password reset email for existing user "<mailId>" from Mailinator
	Then I use the same password in password history
	Then I should see the passowrd history error message on the screen
	Examples: 
		|mailId									|
		|qaPasswordReset@phimail.mailinator.com	|
		
		
@BIDSYNC-121
Scenario: After initiating a password reset, email input field is disabled. Password reset does not generate duplicate reset emails.
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I complete Supplier Registration
	And I logout of BidSync Pro
	And I have navigated to the Forgot Password screen
	Then I will verify that the email input field is locked for user input on the Forgot Password screen
	And I wait "10" seconds for the backend to catch up
	And I will verify that only one password reset email was sent to my new user by checking their inbox
	
	
@BIDS-290
Scenario Outline: Test to verify case sensitivity of reset password functionality
	When I navigate my browser to the supplier login page 
	And I click Forgot Your Password 
	Then I will be on the Forgot your password page 
	When I enter my email as "<mailId>" 
	And I click Continue on the Forgot Password screen 
	Then I will see the confirmation
	Then I retrieve my password reset email for existing user "<mailId>" from Mailinator 
	When I use valid newPassword and confirmPassword
	Then I should be in bid list page  
	When I navigate my browser to the supplier login page 
	And I enter my "<mailId>" and new password 
	Then I will be on the supplier Home page 
	Examples: 
		|mailId								 |
		|RESETPASSWORD@phimail.mailinator.com|
		|ResetPassword@phimail.mailinator.com|
		
		