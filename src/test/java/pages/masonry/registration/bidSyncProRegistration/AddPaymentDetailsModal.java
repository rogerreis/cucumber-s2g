package pages.masonry.registration.bidSyncProRegistration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>AddPaymentDetailsModal</h1>
 * <p>details: This class houses page objects and methods for the "Add Payment Details" modal (3rd modal in Chargebee<br>
 * 	punchout registration flow) that is part of the BidSync Pro registration off the Masonry site</p>
 */
public class AddPaymentDetailsModal extends BasePageActions {
    
    // ids
    private final String cardNumberInput_id    = "number";
    private final String cvvInput_id           = "cvv";
    private final String header_id             = "cb-header-title";
    
    // xpaths
    private final String expiryLabel_xpath     = "//div[contains(@class, 'cb-field__expiry')]";
    private final String expiryMonthInput_xpath= "//input[contains(@placeholder, 'MM')]";
    private final String expiryYearInput_xpath = "//input[contains(@placeholder, 'YY')]";
    private final String nextButton_xpath      = "//button[contains(@data-cb-id, 'card_payment_submit')]";
    
    // Other
    private EventFiringWebDriver driver;

    AddPaymentDetailsModal(EventFiringWebDriver driver) {
    	super(driver);
        this.driver        = driver;
    }
    
    /* ----- getters ----- */
    private WebElement getHeader()           { return findByVisibility(By.id(this.header_id));                 }

    private WebElement getExpiryLabel() {
    	// Sometimes the credit card autocomplete can obscure the expirary label field,
    	// so we want to wait for it to close before searching for the expirary label field
    	waitForInvisibility(By.xpath("//div[@class=\"cb-pmcard__option\"]"), 8);
        return findByPresence(By.xpath(this.expiryLabel_xpath), 10); }
    
    private WebElement getCardNumberInput()  { return findByVisibility(By.id(this.cardNumberInput_id), 8);     } 
    private WebElement getCvvInput()         { return findByVisibility(By.id(this.cvvInput_id));               } 
    private WebElement getExpiryMonthInput() { return findByVisibility(By.xpath(this.expiryMonthInput_xpath)); } 
    private WebElement getExpiryYearInput()  { return findByVisibility(By.xpath(this.expiryYearInput_xpath));  } 
    private WebElement getNextButton()       { return findByVisibility(By.xpath(this.nextButton_xpath));       } 

    /* ----- methods ----- */
    /***
     * <h1>fillInPaymentDetailsAndSubmit</h1>
     * <p>purpose: Add user payment info into the "Add Payment Details" modal (3rd modal in Chargebee punchout registration)<br>
     * 	and then click on the "NEXT" button to submit. Note: We use the canned credit card for payment</p>
     * @param user = user's registration data
     * @return ConfirmPaymentModal
     */
    public ConfirmPaymentModal fillInPaymentDetailsAndSubmit(RandomChargebeeUser user) {
        getCardNumberInput() .sendKeys(user.getCreditCard().getCardNumber());
        getHeader()     .click(); // click outside the dropdown to dismiss it
        getExpiryLabel().click(); // open up the individual month/year inputs
        getExpiryMonthInput().sendKeys(user.getCreditCard().getExpiryMonth());
        getExpiryYearInput() .sendKeys(user.getCreditCard().getExpiryYear());
        getCvvInput()        .sendKeys(user.getCreditCard().getCvv());
        
        getNextButton().click();
        return new ConfirmPaymentModal(driver);
    }
}
