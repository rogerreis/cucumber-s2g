package pages.bidSyncPro.supplier.account.myInfo;

import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import data.bidSyncPro.registration.RandomCompanyInformation;
import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.BasePageActions;

import java.util.concurrent.TimeUnit;

/***
 * <h1>Class CompanyLookupDialog</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects on the Change Company (1st screen) Dialog, that is part of the Update User Profile Dialog<br>
 * 	flows off of the My Profile (admin/my-profile) page</p>
 */
public class CompanyLookupDialog extends BasePageActions {
	
	/*------ xpaths ------*/
	
	// top bar
	private final String closeBtn_xpath           = "//mat-card[@id='matUpdateUserProfileCard']//button";
	private final String changeCompanyTitle_xpath = "//mat-card-title[@id='matUpdateUserProfileTitle'][contains(span,'CHANGE COMPANY')]";
	
	// mid section
	private final String companyNameInput_id      = "company";
	
	// bottom bar
	private final String backBtn_id = "backButton2";
	private final String nextBtn_id = "nextButton2";
	
	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;

	public CompanyLookupDialog(EventFiringWebDriver driver) {
		super(driver);
	    this.driver = driver;
	    this.helperMethods = new HelperMethods();
	}
	
	/*------ getters -----*/
	
	// top bar
	private WebElement getCloseBtn()           { return findByVisibility(By.xpath(this.closeBtn_xpath));           }
	private WebElement getChangeCompanyTitle() { return findByVisibility(By.xpath(this.changeCompanyTitle_xpath)); }
	
	// mid section
	private WebElement getCompanyNameInput()   { return findByVisibility(By.id(this.companyNameInput_id));         }
	
	// bottom bar
	private WebElement getBackBtn() { return findByVisibility(By.id(this.backBtn_id)); }
	private WebElement getNextBtn() { return findByVisibility(By.id(this.nextBtn_id)); }
	
	/*----- methods -----*/
	
	/***
	 * <h1>verifyCompanyLookupIsVisible</h1>
	 * <p>purpose: Verify that the company lookup is visible</p>
	 * @return CompanyLookup
	 */
	public CompanyLookupDialog verifyCompanyLookupIsVisible() {
		if (!this.getCompanyNameInput().isDisplayed()) { fail("Test Failed: Company Lookup is not visible on the \"Change Company\" dialog of the Update User Profile flow"); }
		return this;
	}

	/***
	 * <h1>verifyDialogTitleIsCorrect</h1>
	 * <p>purpose: Verify that the dialog title displays as "Change Company"</p>
	 * @return CompanyLookup
	 */
	public CompanyLookupDialog verifyDialogTitleIsCorrect() {
		if (!this.getChangeCompanyTitle().isDisplayed()) { fail("Test Failed: Dialog title is not displaying as \"Change Company\" during Update User Profile flow"); }
		return this;
	}
	
	/***
	 * <h1>closeCurrentCompanyDialog</h1>
	 * <p>purpose: Click on the "X" to close the Change Company Dialog</p>
	 * @return MyProfile
	 */
	public MyInfo closeCurrentCompanyDialog() {
		this.getCloseBtn().click();
		return new MyInfo(driver);
	}
	
	/***
	 * <h1>clickOnBackBottomBtnBtn</h1>
	 * <p>purpose: Click on the "BACK" button to navigate back from the Change Company Dialog<br>
	 * 	OnClick, user should navigate to Current Company dialog</p>
	 * @return CurrentCompanyDialog
	 */
	public CurrentCompanyDialog clickOnBackBtn() {
		this.getBackBtn().click();
		return new CurrentCompanyDialog(driver);
	}

	/***
	 * <h1>clickOnNextBtn</h1>
	 * <p>purpose: Click on the "NEXT" button on the Change Company Dialog<br>
	 * 	OnClick, dialog should navigate forward to the Change Company (Company details) screen</p>
	 * @return CompanyDetails
	 */
	public void clickOnNextBtn() {
		this.getNextBtn().click();
		//return new CompanyDetails(driver);
	}
    
	/***
	 * <h1>changeTo</h1>
	 * <p>purpose: Given RandomCompanyInformation which includes a company name,<br>
	 * 	either select the company from the lookup (if company exists), or <br>
	 * 	create a new company if the company does not exist in lookup</p>
	 * @param newCompany = company to join or add
	 * @return MyInfo
	 */
    public MyInfo changeTo(RandomCompanyInformation newCompany) {
	    this.getCompanyNameInput().sendKeys(newCompany.getName());
	    if (this.selectCompanyInDropdown(newCompany.getName())) {
	        this.getNextBtn().click();
	        return new RequestToJoinCompanyDialog(driver)
                    .sendRequest();
        }
        helperMethods.addSystemWait(1);
        this.getNextBtn().click();
        return new ChangeCompanyDialog(driver)
                .saveCompany(newCompany);
    }
    
    /***
     * <h1>selectCompanyInDropdown</h1>
     * <p>purpose: Given a String containing the companyName,<br>
     * 	search for and select the name from the dropdown.</p>
     * @param companyName = name to search for in company lookup
     * @return true if name locate and selected | false if name not located and selected
     */
    private boolean selectCompanyInDropdown(String companyName) {
	    try {
            String xpath = String.format("//span[contains(., '%s')]", companyName);
            helperMethods.addSystemWait(5000, TimeUnit.MILLISECONDS);
            waitForVisibility(By.xpath(xpath));
            driver.findElement(By.xpath(xpath)).click();
            return true;
        } catch(TimeoutException e) {
	        return false;
        }
    }
}
