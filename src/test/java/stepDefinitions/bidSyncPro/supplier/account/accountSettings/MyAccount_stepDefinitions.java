package stepDefinitions.bidSyncPro.supplier.account.accountSettings;
 
import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.bidSyncPro.registration.RandomChargebeeUser;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.account.accountSettings.AccountSettings;
import pages.bidSyncPro.supplier.account.accountSettings.ResetPasswordModal;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.helpers.RandomHelpers;
import pages.mailinator.Mailinator;
import utility.database.SelectQueryBuilder;
import utility.encryption.CustomPasswordEncoder;
import utility.encryption.HashUtil;

import java.util.List;

/***
 * <h1>Class MyAccount_stepDefinitions</h1>
 * <p>details: This class houses test steps to test the My Account Page</p>
 * @author jlara
 * <p>TODO: Refactor this class</p>
 */
public class MyAccount_stepDefinitions {
 
	// Other
	private AccountSettings accountSettings;
	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;
	
	public MyAccount_stepDefinitions(CoreAutomation automation) throws Throwable {
		driver          = automation.getDriver();
		helperMethods   = new HelperMethods();
		accountSettings = new AccountSettings(driver);
		
	} // end of constructor
	
	/* ----- helpers ----- */

    private String getEmailForUserFromClassic(RandomChargebeeUser user) {
        List<String> result = new SelectQueryBuilder()
                .useClassicDatabase()
                .setDatabase("DPX")
                .setTable("ORG_USER")
                .setResultColumn("USERNAME")
                .setFilterColumn("BIDSYNCPRO_USER_UUID")
                .setFilterTerm(user.getId())
                .run();
        return CollectionUtils.extractSingleton(result);
    }
    
    private String getEmailForUserFromPro(RandomChargebeeUser user) {
        List<String> result = new SelectQueryBuilder()
                .setDatabase("auth")
                .setTable("users")
                .setResultColumn("username")
                .setFilterColumn("user_id")
                .setFilterTerm(user.getId())
                .run();
        return CollectionUtils.extractSingleton(result);
    }
    
    private String getPasswordForUserFromClassic(RandomChargebeeUser user) {
        List<String> result = new SelectQueryBuilder()
                .useClassicDatabase()
                .setDatabase("DPX")
                .setTable("ORG_USER")
                .setResultColumn("USERPWD")
                .setFilterColumn("BIDSYNCPRO_USER_UUID")
                .setFilterTerm(user.getId())
                .run();
        return CollectionUtils.extractSingleton(result);
    }
    
    private String getPasswordForUserFromPro(RandomChargebeeUser user) {
        List<String> result = new SelectQueryBuilder()
                .setDatabase("auth")
                .setTable("users")
                .setResultColumn("password")
                .setFilterColumn("user_id")
                .setFilterTerm(user.getId())
                .run();
        return CollectionUtils.extractSingleton(result);
    }
 
	
	/* ------------ Test Steps ---------------- */

	@Given("^I change that users email to a new random email$")
	public void IChangeThatUsersEmailToANewRandomEmail() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Change the user's email");
		String newEmail = new RandomHelpers().getRandomEmail();
        GlobalVariables.storeObject("NewEmail", newEmail);
		new BidList(driver)
                .openAccountDropDown()
                .clickAccountSettings()
                .changeEmailTo(newEmail);
        new Mailinator(driver)
                .navigateToMailinatorInbox(newEmail)
                .openChangeEmailEmail()
                .clickOnTokenURLInEmail();
	}
	
	@And("^I have clicked on the \"(.*)\" Section on Account > My Account$")
	public void Ihaveclickedonthe__Section_onAccountMyAccount(String strSectionName) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Navigate to the "+strSectionName+" Section");

		switch(strSectionName) {
			case "Change Email":    accountSettings.clickChangeEmailButton();    break;
			case "Change Password": accountSettings.clickChangePasswordButton(); break;
			//case "Accessibility Setting": myAccount.clickAccessibilitySettingsButton(); break;
		}
		
	}
    
    @When("^I change that users password to a new random password$")
    public void iChangeThatUsersPasswordToANewRandomPassword() {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Change the user's password");
        String newPassword = RandomStringUtils.randomAlphanumeric(8) + "%$#";
        RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
        GlobalVariables.storeObject("NewPassword", newPassword);
        new BidList(driver)
                .openAccountDropDown()
                .clickAccountSettings()
                .changePassword();
        new Mailinator(driver)
                .navigateToMailinatorInbox(user.getEmailAddress())
                .openChangePasswordEmail()
                .clickOnTokenURLInEmail();
        new ResetPasswordModal(driver)
                .setPasswordInput(newPassword)
                .setConfirmPasswordInput(newPassword)
                .clickResetPasswordButton();
    }
    
    @Then("^the password change will be reflected in both the Pro and Classic databases$")
    public void thePasswordChangeWillBeReflectedInBothTheProAndClassicDatabases() {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Checking that the new password is in both Pro and Classic databases.");
        helperMethods.addSystemWait(5); // wait for database to be populated

        RandomChargebeeUser user                = GlobalVariables.getStoredObject("StoredUser");
        String expectedPassword                 = GlobalVariables.getStoredObject("NewPassword");
        String passwordFromPro                  = getPasswordForUserFromPro(user);
        String classicEncryptedExpectedPassword = HashUtil.generateHash(expectedPassword, HashUtil.Type.SSHA);
        String proEncryptedExpectedPassword     = new CustomPasswordEncoder().encode(expectedPassword);

        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(HashUtil.isValidPasswordHash(classicEncryptedExpectedPassword, expectedPassword)).as("Password from Classic").isEqualTo(true);
        softly.assertThat(passwordFromPro).as("Password from Pro").isEqualTo(proEncryptedExpectedPassword);
        softly.assertAll();
    }

 @Then("^the email change will be reflected in both the Pro and Classic databases$")
    public void theEmailChangeWillBeReflectedInBothTheProAndClassicDatabases() {
        System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Checking that the new email is in both Pro and Classic databases.");

        helperMethods.addSystemWait(5); // wait for database to be populated
        RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
	    String expectedEmail     = GlobalVariables.getStoredObject("NewEmail");
	    String emailFromClassic  = getEmailForUserFromClassic(user);
	    String emailFromPro      = getEmailForUserFromPro(user);
    
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(emailFromClassic).as("Email from Classic").isEqualTo(expectedEmail);
        softly.assertThat(emailFromPro)    .as("Email from Pro")    .isEqualTo(expectedEmail);
        softly.assertAll();
    }
   
}

