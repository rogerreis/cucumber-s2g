@must  @BIDSYNC-282 @bidSearch_detail @level3
Feature: Dashboard - Match Score, Part 1
  This feature tests the match score indicators for bids found on the supplier dashboard
  BIDSYNC-282: Match Score Not Consistent
  
@BIDSYNC-282
Scenario Outline: Match Score displays correctly per bids returned for user search in "All Bids" list
Note: By design, running a search on the Filter Results section will auto-navigate the user to the "All Bids" list
#Do not change the date range below

	 Given I have logged into the Supplier side as user "User to Verify Default Keywords"
	 When I run a search on the Filter Results section with filters:
		 | Filter Name       | Bogus Name   		|
		 | Keywords          | IBM		    		|
		 | Negative Keywords | 						|
		 | States/Provinces  | 		 				|
		 | Time Frame        | Custom Time Frame	|
		 | Show Bids From    | 9/1/2019  	      	|
		 | Show Bids to      | 10/27/2019          	|
		#| Show Bids From    | 5/1/2016  	      	| // returns 12K+ records; will be replaced with dynamic time span
	 And I select a bid with a match score between "<lowerBounds>" and "<upperBounds>" from the bid list of match type "<text>"
	 When I click on load more, if it is available
	 Then I will verify that the match score indicator displays the text "<text>" with the indicator color "<color>"
	 
	Examples: Required matches
	| lowerBounds | upperBounds | text       | color                  |
    | 90          | 100         | BEST MATCH | rgba(114, 172, 82, 1)  |
    | 75          | 89          | GOOD       | rgba(114, 172, 82, 1)  |
    | 50          | 74          | POSSIBLE   | rgba(253, 189, 55, 1)  |
    | 0           | 49          | UNLIKELY   | rgba(255, 41, 4, 0.56) |

@BIDSYNC-282
Scenario: Match Score Indicator does not display for any bids saved in the "Your Saved Bids" list
dmidura: Test requires starred bids to be saved in the "Your Saved Bids" list before this test is run

	 Given I have logged into the Supplier side as user "User to Verify Default Keywords"
	 When I have navigated to the "Your Saved Bids" bid tab
	 Then I will verify that no match indicator is displayed for each bid