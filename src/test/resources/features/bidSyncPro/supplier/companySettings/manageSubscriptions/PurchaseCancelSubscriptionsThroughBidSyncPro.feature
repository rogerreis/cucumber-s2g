@must @subscriptions @level2
Feature: Company Settings > Manage Subscriptions - Purchase and Cancel Subscriptions through Chargebee Punchout in BidSync Pro
ARGO-1397 "Turning on/off subscription plans in New BidSync for Subscription owner"


@ARGO-1397
Scenario Outline: Chargebee: Purchase and cancel subscriptions
	Given I have logged into the Supplier side as user "UserForChargebeeSubscriptionsTest"
	And I have navigated to Company Settings > "Manage Subscriptions"
	And I have clicked on Manage Subscriptions
	And I have clicked on the current subscription
	And I have clicked on Edit Subscription
	When I change my subscription to "<Subscription>"
	And click the Update Subscription button
	Then the subscriptions for user "UserForChargebeeSubscriptionsTest" will be updated in the database to show "<Subscription>"

	Examples: 
	| Subscription		         |
	| BidSync National - Yearly  |
	| BidSync National - Monthly |
	| BidSync National - 6 Month |
	| BidSync Regional - Yearly  |
	| BidSync Regional - Monthly |
	| BidSync Regional - 6 Month |
	| BidSync State - Yearly     |
	| BidSync State - Monthly    |
	| BidSync State - 6 Month    |
 
