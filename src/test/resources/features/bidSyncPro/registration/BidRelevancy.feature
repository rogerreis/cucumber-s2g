@ignore @refactor @registration
Feature: Supplier Registration - Bid Relevancy Screen

@refactor @ARGO-374
Scenario: Each bid in Bid Relevancy includes both a title and a bid description.

	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to Bid Relevancy
	Then I will verify each bid in Bid Relevancy contains both a title and a bid description

	
@refactor @ARGO-374
Scenario: Each bid in Bid Relevancy contains at least one of the keywords in title or bid description, 
			but does not contain any of the negative keywords in title or bid description
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to Bid Relevancy
	Then I will verify each bid in Bid Relevancy contains at least one of the keywords, and does not contain any of the negative keywords

	
@refactor @ARGO-374
Scenario: Each bid in Bid Relevancy should be unique

	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to Bid Relevancy
	Then I will verify each bid in Bid Relevancy is unique


@refactor @ARGO-374
Scenario: User can Thumbs up or Thumbs down bids. After five bids, user is navigated forward to "Your Bids are Ready"

	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to Bid Relevancy
	When I randomly Thumbs Up or Thumbs Down five bids
	Then I will be navigated to the Your Bids are Ready screen