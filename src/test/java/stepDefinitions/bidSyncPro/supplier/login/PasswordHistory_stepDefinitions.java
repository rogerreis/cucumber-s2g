package stepDefinitions.bidSyncPro.supplier.login;

import core.CoreAutomation;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.common.helpers.GlobalVariables;
import cucumber.api.java.en.Given;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

/***
 * 
 * <p>TODO: Refactor this class</p>
 *
 */
public class PasswordHistory_stepDefinitions {
	private EventFiringWebDriver driver;

	public PasswordHistory_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
	}
	
/*
The class to login and be on the Vendor Home is declared in other classes. We will pick up with following the link to 
change the password	
*/
	
	@Then("^I am logged in as a vendor$")
	public void I_am_logged_in() throws Throwable {
		
		if(driver.getCurrentUrl().equalsIgnoreCase(System.getProperty("supplierBaseURL") + "dashboard/new-bids")) {
			System.out.println(GlobalVariables.getTestID() + " Test Result: Pass.\n");
			} else {
			System.out.println(GlobalVariables.getTestID() + " Test Result: Fail.\n");
			}
	}
	
	@When("^I select My User Info$")
	public void I_select_My_User_Info() throws Throwable {
		driver.navigate().to(System.getProperty("supplierBaseURL") + "admin/my-user-info");
		if(driver.getCurrentUrl().equalsIgnoreCase(System.getProperty("supplierBaseURL") + "admin/my-user-info")) {
			System.out.println(GlobalVariables.getTestID() + " Test Result: Pass.\n");
			} else {
			System.out.println(GlobalVariables.getTestID() + " Test Result: Fail.\n");
			}
	}
	
	//Check to ensure that the password change dialog is loaded, by checking for the URL
	@Given("^I am on the change password page$")
	public void I_am_on_the_Change_Password_Page() throws Throwable {
		if(driver.getCurrentUrl().equalsIgnoreCase(System.getProperty("supplierBaseURL") + "admin/my-user-info")) {
		System.out.println(GlobalVariables.getTestID() + " Test Result Pass.\n");
		} else {
		System.out.println(GlobalVariables.getTestID() + " Test Result Fail.\n");
		}
	}
	
		//This is where you enter the existing password and new password twice, and submit	
	@Given("^I enter my old passsword as \"([^\"]*)\"$ and my new password as \"([^\"]*)\"$") 
	public void I_enter_my_old_password_as_and_my_new_password_as(String arg1, String arg2) throws Throwable {
		driver.findElement(By.id("")).sendKeys(arg1);
		
		driver.findElement(By.id("")).sendKeys(arg2);
		driver.findElement(By.id("")).sendKeys(arg2);
		driver.findElement(By.id("submit")).click();
		//if(driver).getCurrentUrl()
		
	}

	
}

