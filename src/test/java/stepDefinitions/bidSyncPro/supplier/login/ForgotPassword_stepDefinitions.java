package stepDefinitions.bidSyncPro.supplier.login;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.bidSyncPro.supplier.common.LocalBrowserStorageBidSyncPro;
import pages.bidSyncPro.supplier.login.ForgotYourPassword;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.mailinator.Mailinator;

public class ForgotPassword_stepDefinitions {

	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;
	private ForgotYourPassword forgotPassword;

	public ForgotPassword_stepDefinitions(CoreAutomation automation) throws Throwable {
		this.driver = automation.getDriver();
		helperMethods = new HelperMethods();
		forgotPassword = new ForgotYourPassword(driver);
	}
	
	@Given ("^I have navigated to the Forgot Password screen$")
	public void I_have_navigated_to_the_Forgot_Password_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Navigating to Forgot Password screen off of user login");

		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
		new SupplierLogin(driver)
			.navigateToHere()
			.clickForgotYourPassword()
			.setForgotPasswordInputAndPressEnterKey(user.getEmailAddress());
	}
	
	@Then ("^I will verify that the email input field is locked for user input on the Forgot Password screen$")
	public void I_will_verify_that_the_email_input_field_is_locked_for_user_input_on_the_Forgot_Password_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that email input field is locked to user input");
		forgotPassword.verifyEmailInputIsDisabled();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Email input field is locked");
	}

	@And ("^I will verify that only one password reset email was sent to my new user by checking their inbox$")
	public void I_will_verify_that_only_one_password_reset_email_was_sent_to_my_new_user_by_checking_their_inbox() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that only one Password Reset Email exists in user inbox");

		RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");

		new Mailinator(driver)
			.navigateToMailinatorInbox(user.getEmailAddress())
			.verifyOnlyOneResetPasswordEmailSent();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Only one Password Reset Email was sent after Forgot Password");
	}

	@When("^I click Forgot Your Password?$")
	public void iClickForgotyourPassword() throws Throwable {
		new SupplierLogin(driver).clickForgotYourPassword();
	}

	@Then("^I will be on the Forgot your password page$")
	public void i_will_be_on_the_Forgot_Your_Password_page() throws Throwable {
		forgotPassword.verifyPageInitialization();
	}

	// Enter the email to send for reset
	@When("^I enter my email as \"(.*)\"$")
	public void i_enter_my_email_as(String email) throws Throwable {
		try {
			forgotPassword.enterEmailId(email);
		} catch (NoSuchElementException e) {
			Assert.fail("Test Failed: \"Forgot Password\" screen - i_enter_my_email_as() :\n" + e);
		}
	}

	@And("^I click Continue on the Forgot Password screen$")
	public void I_click_Continue_on_the_Forgot_Password_screen() throws Throwable {
		try {
			forgotPassword.clickContinueBtn();
		} catch (NoSuchElementException e) {
			Assert.fail("Test Failed: \"Forgot Password\" screen - iClickContinue() :\n" + e);
		}
	}

	@Then("^I will see the confirmation$")
	public void iWillSeeTheConfirmation() {
		try {
			forgotPassword.verifyPageTransition();
		} catch (NoSuchElementException e) {
			Assert.fail("No password reset email confirmation text found.");
		}
	}

	// Cancel to return to Login Page
	@When("^I click Cancel$")
	public void i_click_Cancel() throws Throwable {
		try {
			forgotPassword.clickCancelBtn();
		} catch (NoSuchElementException e) {
			Assert.fail("Test Failed: \"Forgot Password\" screen - i_click_Cancel() :\n" + e);
		}
	}

	@Then("^I will see the Login Page$")
	public void i_will_see_the_Login_Page() throws Throwable {
		if (driver.findElement(By.id("loginButton")) != null) {
			System.out.println(GlobalVariables.getTestID() + " Test passed: Login page reloaded.\n");
		} else {
			System.out.println(GlobalVariables.getTestID() + " Test failed: Login page not re-loaded!\n");
		}
	}

	@When("^I try to change my password from the Forgot Your Password page off the login screen$")
	public void I_try_to_change_my_password_from_the_Forgot_Your_Password_page_off_the_login_screen() throws InterruptedException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Initiating password change from Forgot Your Password and retrieving email");

		// 1. Get the JWT Token for this session
		String token = new LocalBrowserStorageBidSyncPro(driver).getToken();
		Assert.assertNotNull("Test Failed: JWT token is null or blank when logged in", token);
		GlobalVariables.storeObject("JWTToken", token);

		// 2. Now logout of BidSync
		new CommonTopBar(driver).logout();

		// 3. Run through the "Forgot Password" flows off the supplier login screen
		RandomChargebeeUser storedUser = GlobalVariables.getStoredObject("StoredUser");
		forgotPassword.forgotPassword(storedUser.getEmailAddress());

		// 4. Retrieve the email
		forgotPassword.retrieveEmail(storedUser.getEmailAddress());
	}

	@And("^I enter mail id to reset my password as directed and press enterKey$")
	public void i_enter_mail_id_to_reset_my_password_as_directed_and_press_enterKey() throws Throwable {
		try {
			forgotPassword.setForgotPasswordInputForRegisteredUser();
		} catch (Exception e) {
			Assert.fail(
					"Test Failed: \"Forgot Password\" screen - i_enter_mail_id_to_reset_my_password_as_directed_and_press_enterKey() :\n"
							+ e);
		}
	}

	@And("^I enter email id \"(.*)\" and press enter key$")
	public void i_enter_email_id_and_press_enter_key(String mailId) {
		try {
			forgotPassword.setForgotPasswordInputAndPressEnterKey(mailId);
		} catch (Exception e) {
			Assert.fail("Test Failed: \"Forgot Password\" screen - i_enter_email_id_and_press_enter_key() :\n" + e);
		}
	}

	@When("I retrieve my password reset email for new user from Mailinator$")
	public void i_retrieve_my_password_reset_email_for_new_user_from_Mailinator() throws Throwable {
		try {
			RandomChargebeeUser storedUser = GlobalVariables.getStoredObject("StoredUser");
			forgotPassword.retrieveEmailFromMailinator(storedUser.getEmailAddress());
		} catch (Exception e) {
			Assert.fail(
					"Test Failed: \"Forgot Password\" screen - i_retrieve_my_password_reset_email_for_new_user_from_Mailinator() :\n"
							+ e);
		}
	}

	@Then("^I retrieve my password reset email for existing user \"(.*)\" from Mailinator$")
	public void i_retrieve_my_password_reset_email_for_existing_user_from_Mailinator(String userEmail){
		try {
			forgotPassword.retrieveEmailFromMailinator(userEmail);
		} catch (Exception e) {
			Assert.fail(
					"Test Failed: \"Forgot Password\" screen - i_retrieve_my_password_reset_email_for_existing_user_from_Mailinator() :\n"
							+ e);
		}
	}

	@When("^I use valid newPassword and confirmPassword$")
	public void use_validPassword_to_reset_password() {
		try {

			forgotPassword.setNewPasswordsAndTapEnterKey();
		} catch (Exception e) {
			Assert.fail("Test Failed: \"Forgot Password\" screen - use_validPassword_to_reset_password() :\n" + e);
		}
	}

	@Then("^I should be in bid list page$")
	public void i_should_be_in_bid_list_page() {
		try {
			forgotPassword.verifybidListPage();
		} catch (Exception e) {
			Assert.fail("Test Failed: \"Forgot Password\" screen - i_should_be_in_bid_list_page() :\n" + e);
		}
	}

	@And("^I enter email and new password$")
	public void i_enter_email_and_new_password() {
		try {
			RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
			new SupplierLogin(driver).loginWithCredentials(user.getEmailAddress(),
					GlobalVariables.getStoredObject("newPassword"));
		} catch (Exception e) {
			Assert.fail("Test Failed: \"Forgot Password\" screen - i_enter_email_and_new_password() :\n" + e);
		}
	}

	@And("^I enter my \"(.*)\" and new password$")
	public void i_enter_my_email_and_new_password_for_a_givenUser(String email) {
		try {
			new SupplierLogin(driver).loginWithCredentials(email, GlobalVariables.getStoredObject("newPassword"));
		} catch (Exception e) {
			Assert.fail(
					"Test Failed: \"Forgot Password\" screen - i_enter_my_email_and_new_password_for_a_givenUser() :\n"
							+ e);
		}
	}

	@And("^the page should load properly$")
	public void the_page_should_load_properly() {
		try {
			forgotPassword.verifyPageInitialization();
		} catch (Exception e) {
			Assert.fail("Test Failed: \"Forgot Password\" screen - the_page_should _load_properly() :\n" + e);
		}
	}

	@Then("^I use the same password in password history$")
	public void use_same_password_in_password_history() {
		try {
			forgotPassword.enterPasswordInPasswordHistory();
		} catch (Exception e) {
			Assert.fail("Test Failed: \"Forgot Password\" screen - use_same_password_in_password_history() :\n" + e);
		}
	}

	@Then("^I should see the passowrd history error message on the screen$")
	public void validateErrorMessage() {
		try {
			forgotPassword.verifyPasswordHistoryErrorMessage();
		} catch (Exception e) {
			Assert.fail("Test Failed: \"Forgot Password\" screen - validateErrorMessage() :\n" + e);
		}
	}

}