package utility.database;


import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/***
 * <h1>Class SQLConnector</h1>
 * <p>details: This class contains setup for using SQL to the Pro and Classic databases.
 * 	User SelectQueryBuilder() class to actually run queries</p>
 */
public class SQLConnector {
	
	/***
	 * <h1>establishConnectionToProDatabase</h1>
	 * <p>purpose: Establish a connection to the Pro database</p>
	 * @param dbName = Name of ARGO database to connect to
	 * @return Connection
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	private static Connection establishConnectionProDatabase(String dbName) throws SQLException, ClassNotFoundException {
		String dbUrl    = "jdbc:postgresql://" + System.getProperty("databaseURL") + ":5432/";
		String username = System.getProperty("databaseLogin");
		String password = System.getProperty("databasePassword");

		// Load mysql jdbc driver
		Class.forName("org.postgresql.Driver");

		// Create Connection to DB
		Connection con = DriverManager.getConnection(dbUrl + dbName, username, password);
		
		return con;
	}

	/***
	 * <h1>executeCommand</h1>
	 * <p>purpose: Run command on the database (ex: Update, delete, insert)
	 * 	Note: Database connection details will be automatically set based<br>
	 * 	on the "server" property set in "config.json". server can be "dev" |" qa" | "prod"</p>
	 * @param dbName = database name<br>
	 * 	ex: "auth" </p>
	 * @param command = query to update, insert, or delete in the database (no results should be returned).
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void executeCommand(String dbName, String command) throws ClassNotFoundException, SQLException {
		// Create statement
		Connection con = establishConnectionProDatabase(dbName);
		Statement stmt = con.createStatement();
		
		// Update
		stmt.executeUpdate(command);
		
		// Close connection
		con.close();
	}
			
	/***
	 * <h1>executeQuery</h1>
	 * <p>purpose: Run query on database.<br>
	 * 	Note: Database connection details will be automatically set based<br>
	 * 	on the "server" property set in "config.json". server can be "dev" |" qa" | "prod"</p>
	 * @param dbName = database name<br>
	 * 	ex: "auth" </p>
	 * @param query = query<br>
	 * 	ex: "select * from users;"</p>
	 * @return ResultSet containing results of search
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public static ResultSet executeQuery(String dbName, String query) throws SQLException, ClassNotFoundException {
		
		// Create Statement Object
		Connection con = establishConnectionProDatabase(dbName);
		Statement stmt = con.createStatement();

		// Execute the SQL Query. Store results in ResultSet
		ResultSet rs = stmt.executeQuery(query);

		// closing DB Connection
		con.close();
		
		return rs;
	}
	
	
    /***
     * <h1>executeQueryOnBidsyncClassic</h1>
     * <p>purpose: Run query on the bidsync classic database.<br>
     * 	Note: Database connection details will be automatically set based<br>
     * 	on the "server" property set in "config.json". server can be "dev" |" qa" | "prod"</p>
     * @param dbName = database name<br>
     * 	ex: "auth" </p>
     * @param query = query<br>
     * 	ex: "select * from users;"</p>
     * @param resultColumn
     * @return ResultSet containing results of search
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    @SuppressWarnings("unchecked") // impossible to avoid with conversion to a generic type, as far as I know
    public static <T> List<T> executeQueryOnBidsyncClassic(String dbName, String query, String resultColumn) throws SQLException, ClassNotFoundException {
        
        String dbUrl    = "";
        String username = "";
        String password = "";
        
        switch (System.getProperty("server")) {
            case "qa":
                // TODO dmidura: add QA connection info after qa database is configured
            case "dev":
                dbUrl    = "jdbc:oracle:thin:@172.17.50.54:1521/";
                username = "stage";
                password = "p2ssst2g3";
                break;
            case "prod":
                dbUrl    = "jdbc:oracle:thin:@172.17.50.54:1521:dpxstg1";
                username = "stage";
                password = "p2ssst2g3";
                //dbUrl    = "jdbc:oracle:thin:@//scan01-453651.racscan.com:1521/dpx";
                //username = "dpxro";
                //password = "w9bdnsRc";
                break;
        }
        
        // Load oracle jdbc driver
        //Class.forName("org.postgresql.Driver");
        Class.forName("oracle.jdbc.driver.OracleDriver");
        
        // Create Connection to DB
        Connection con = DriverManager.getConnection(dbUrl, username, password);
        
        // Create Statement Object
        Statement stmt = con.createStatement();
        
        // Execute the SQL Query. Store results in ResultSet
        ResultSet rs = stmt.executeQuery(query);
    
        // the oracle ResultSet closes when its parent connection closes, so we have to populate the results array in here
        List<T> results = new ArrayList<>();
        while (rs.next()) {
            results.add((T)rs.getObject(resultColumn));
        }
        
        // closing DB Connection
        con.close();
        
        return results;
    }
}