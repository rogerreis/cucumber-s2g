package data.bidSyncPro.BidCard;

public class SubscribedBidCard {

	private String bidName;
	private String city;
	private String description1;
	private String description2;
	private String viewBidLink;
	private String bidNumber;
	private String dueDate;
	private String noOfDays;
	private String noOfDaysLeft;
	private String matchType;
	
	
	public String getbidName() {
        return bidName;
    }
 
    public void setBidTitle(String bidName) {
        this.bidName = bidName;
    }
    
	public String getcity() {
        return city;
    }
 
    public void setBidLocation(String city) {
        this.city = city;
    }
    
	public String getdescription1() {
        return description1;
    }
 
    public void setDescription(String description1) {
        this.description1 = description1;
    }
    
	public String getdescription2() {
        return description2;
    }
 
    public void setSpyGlassImage(String description2) {
        this.description2 = description2;
    }
    
	public String getviewBidLink() {
        return viewBidLink;
    }
 
    public void setViewBidLink(String viewBidLink) {
        this.viewBidLink = viewBidLink;
    }
    
	public String getbidNumber() {
        return bidNumber;
    }
 
    public void setBidNumber(String bidNumber) {
        this.bidNumber = bidNumber;
    }
    
	public String getdueDate() {
        return dueDate;
    }
 
    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }
    
	public String getnoOfDays() {
        return noOfDays;
    }
 
    public void setNoOfDaysLeft(String noOfDays) {
        this.noOfDays = noOfDays;
    }
    
	public String getnoOfDaysLeft() {
        return noOfDaysLeft;
    }
 
    public void setDaysLeftText(String noOfDaysLeft) {
        this.noOfDaysLeft = noOfDaysLeft;
    }
    
	public String getmatchType() {
        return matchType;
    }
 
    public void setMatchType(String matchType) {
        this.matchType = matchType;
    }

}
