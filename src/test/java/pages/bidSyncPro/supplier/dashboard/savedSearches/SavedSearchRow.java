package pages.bidSyncPro.supplier.dashboard.savedSearches;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.dashboard.DashboardCommon;

/***
 * <h1>Class SavedSearchRow</h1>
 * @author dmidura
 * <p>details: This class houses the objects and methods for a search row in the Saved Search dropdown.<br>
 * 	The Saved Search dropdown appears on the dashboard (top left)</p>
 */
public class SavedSearchRow extends SavedSearchesDropdown{
	
	// Elements of a search
	private String searchName            = "";
	private String searchNameBtn_xpath   = "";
	private String deleteSearchBtn_xpath = "";
	
	// Other
	private EventFiringWebDriver driver;
	
	public SavedSearchRow(EventFiringWebDriver driver, String ele_xpath) {
		super(driver);
		this.driver = driver;
		this.searchNameBtn_xpath   = ele_xpath + "//button[1]";
		this.deleteSearchBtn_xpath = ele_xpath + "//button[2]";
		this.setSearchNameTxt();
	}
	
	/* ----- getters ----- */
	private WebElement getSearchNameBtn()   { return findByPresence(By.xpath(this.searchNameBtn_xpath));   }
	private WebElement getDeleteSearchBtn() { return findByPresence(By.xpath(this.deleteSearchBtn_xpath)); }
	
	/* ----- methods ----- */

	/***
	 * <h1>setSearchNameTxt</h1>
	 * <p>purpose: Set the search name text</p>
	 */
	private void setSearchNameTxt() {
		this.searchName = this.getSearchNameBtn().getText();
	}

	/***
	 * <h1>selectSearchFromSavedSearches</h1>
	 * <p>purpose: Select the search from the Saved Searches dropdown<br>
	 * 	by clicking on the search name</p>
	 * @return DashboardCommon 
	 */
	public DashboardCommon clickOnSearchInSavedSearches() {
		findByScrollIntoViewBottomOfScreen(By.xpath(this.searchNameBtn_xpath));
		this.getSearchNameBtn().click();
		return new DashboardCommon(driver);
	}
	
	/***
	 * <h1>deleteSearchFromSavedSearches</h1>
	 * <p>purpose: Delete the search from the Saved Searches dropdown<br>
	 * 	by clicking on the "X" button</p>
	 * @return ConfirmDeleteSearchPopup
	 */
	public ConfirmDeleteSearchPopup deleteSearchFromSavedSearches() {
		findByScrollIntoViewBottomOfScreen(By.xpath(this.deleteSearchBtn_xpath));
		this.getDeleteSearchBtn().click();
		return new ConfirmDeleteSearchPopup(driver, this.getSavedSearchName());
	}
	
	/***
	 * <h1>getSavedSearchName</p>
	 * <p>purpose: Return the name of the search in the Saved Search dropdown</p>
	 * @return String = search name, as displayed in Saved Search dropdown
	 */
	public String getSavedSearchName() {
		return this.searchName;
	}

}
