package pages.chargebeeSite.subscriptions.subscriptionDetails.cancelSubscriptionFlow;

import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.chargebeeSite.subscriptions.subscriptionDetails.SubscriptionDetails;
import pages.common.pageObjects.BasePageActions;
/***
 * <h1>CancelSubscription</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects "Cancel Subscription Immediately" popup that is<br>
 * 	accessed by clicking on "Cancel Subscription" button on the Subscriptions->Subscription Detail page, then<br>
 * 	choosing to "Schedule Cancelation" on the Cancel Subscription Popup, then clicking on "Cancel Immediately" on the<br>
 *	Subscriptions->Subscription Detail page off of the Chargebee test site</p>
 */
public class CancelSubscriptionImmediatelyPopup extends BasePageActions {
	
	// xpaths -- add as needed
	private final String cancelNowBtn_xpath = "//span[contains(@class,'danger')][contains(text(),'Cancel Now')]";
	
	// Other
	private EventFiringWebDriver driver;
	
	public CancelSubscriptionImmediatelyPopup(EventFiringWebDriver driver ){
		super(driver);
		this.driver = driver;
	}
	
	/* ----- getters ----- */
	private WebElement getCancelNowBtn() { return findByVisibility(By.xpath(this.cancelNowBtn_xpath));} 

	/* ----- methods ----- */
	/****
	 * <h1>clickOnCancelNow</h1>
	 * <p>purpose: Click on the "Cancel Now" button on the <br>
	 * 	"Cancel Subscription - <company name>" popup that is accessed via <br>
	 * 	Subscriptions -> Subscription Details -> Cancel Immediately </p>
	 * @return SubscriptionDetails
	 */
	public SubscriptionDetails clickOnCancelNow() {
		this.getCancelNowBtn().click();
		return new SubscriptionDetails(driver);
	}

}
