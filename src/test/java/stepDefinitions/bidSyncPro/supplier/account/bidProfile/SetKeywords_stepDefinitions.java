package stepDefinitions.bidSyncPro.supplier.account.bidProfile;

import core.CoreAutomation;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.common.helpers.GlobalVariables;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class SetKeywords_stepDefinitions {

	Random rand = new Random();	
	public CoreAutomation automation;
	public EventFiringWebDriver driver;
	public SupplierLogin supplierLoginPage;	
		
	public SetKeywords_stepDefinitions(CoreAutomation automation) throws Throwable {
		this.automation = automation;
		this.driver = automation.getDriver();
		supplierLoginPage = new SupplierLogin(driver);
	}
	
	@Given("^I navigate to the notifications page$")
	public void i_navigate_to_the_notifications_page() throws Throwable {
		TimeUnit.SECONDS.sleep(5);
		driver.get(System.getProperty("supplierBaseURL") + "admin/bid-notifications");		
		System.out.print("Navigating to Notifications page\n");
		TimeUnit.SECONDS.sleep(5);
	}
	
	//Load the notifications page, and select Edit
	@Then("^I will be on the notifications page$")
	public void i_will_be_on_the_notifications_page() throws Throwable {
		if(driver.getPageSource().contains("Keywords")) {
			System.out.println(GlobalVariables.getTestID() + " Test passed: Keyword selection page was loaded.\n");
		} else {
			System.out.println(GlobalVariables.getTestID() + " Test failed: Keyword selection page was not loaded.\n");
		}
		driver.findElement(By.xpath("//button[@aria-label='update keywords']")).click();
	}	
	
	@Given("^I navigate to the keywords page$")
	public void i_navigate_to_the_keywords_page() throws Throwable {
		TimeUnit.SECONDS.sleep(5);
		driver.get(System.getProperty("supplierBaseURL") + "setup/keywords");		
		System.out.print("Navigating to keyword selection page\n");
		TimeUnit.SECONDS.sleep(5);
	}	
	
	@Then("^I will be on the keywords page$")
	public void i_will_be_on_the_keywords_page() throws Throwable {
		if(driver.getPageSource().contains("Keywords")) {
			System.out.println(GlobalVariables.getTestID() + " Test passed: Keyword page loaded.\n");
		} else {
			System.out.println(GlobalVariables.getTestID() + " Test failed: Keyword page not loaded.\n");
		}
	}	
	
	@When("^I add keywords$")
	public void i_add_keywords() throws Throwable {		
		String[] keyword = {"Tools","Horses","Doors","Windows","Awnings","Feed","baking","Paving","Concrete","Valves"};
		for (int i = 0; i < 10; i++) {
			System.out.println(GlobalVariables.getTestID() + " Keyword: "+keyword[i]+" was added.\n");
			driver.findElement(By.id("keyword")).sendKeys(keyword[i]);
			driver.findElement(By.id("keyword")).sendKeys(Keys.RETURN);
			TimeUnit.SECONDS.sleep(1);
			}
		
		driver.findElement(By.id("negKeyword")).sendKeys("snakes");
		driver.findElement(By.id("negKeyword")).sendKeys(Keys.RETURN);
		driver.findElement(By.id("negKeyword")).sendKeys("lizards");
		driver.findElement(By.id("negKeyword")).sendKeys(Keys.RETURN);				
		for (int i = 0; i < 7; i++) {
			driver.findElement(By.xpath("//mat-icon[@class='mat-icon mat-chip-remove material-icons ng-star-inserted']")).click();
			//driver.findElement(By.xpath("//mat-icon[contains(.,'cancel')]")).click();
			TimeUnit.SECONDS.sleep(1);
			}		
	}	
	
	@And("I will save my changes$")
	public void i_will_save_my_changes() throws Throwable {
		driver.findElement(By.xpath("//button[contains(.,'close')]")).click();
	}	
	
	@Then("^I can continue")
	public void i_can_continue() throws Throwable {
		try{
			driver.findElement(By.id("continueButton"));
			System.out.println(GlobalVariables.getTestID() + " Test passed: Continue is there.\n");
			driver.findElement(By.id("continueButton")).click();
		}
		catch(Exception e) {		
			System.out.println(GlobalVariables.getTestID() + " Test failed: Continue is NOT there.\n");
		}		
	}
	
	@When("^I select thumbs up 5 times")
	public void i_select_thumbs_up() throws Throwable {
		try{
			TimeUnit.SECONDS.sleep(5);
			int x = 1;
			while (x <= 5) {
			driver.findElement(By.id("btnVoteUp")).click();
			TimeUnit.SECONDS.sleep(5);
			x++;
			}
		}
		catch(Exception e) {
			System.out.println(GlobalVariables.getTestID() + " Could not find thumbs up.\n");
		}
	}	
	
	@Then("^I can go to the bid list")
	public void i_can_go_to_the_bid_list() throws Throwable {
		try{
			driver.findElement(By.id("AllDone"));		
			System.out.println(GlobalVariables.getTestID() + " View bids.\n");			
			driver.findElement(By.id("AllDone")).click();
		}	
		catch(Exception e){	
			System.out.println(GlobalVariables.getTestID() + " Test failed: Cannot contine to bid list.\n");			
		}
	TimeUnit.SECONDS.sleep(10);
	//driver.close();
	}
}