package stepDefinitions.bidSyncPro.supplier.login;

import core.CoreAutomation;

import org.junit.Assert;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.support.events.EventFiringWebDriver;

/***
 * <h1>LockedUserClear</h1>
 * @author kmatheson
 * <p>details: Clears a locked user, and then attempts to re login<br>  	
 */

public class LockedUserClear_stepDefinitions {

	public CoreAutomation automation;
	public EventFiringWebDriver driver;

	public LockedUserClear_stepDefinitions(CoreAutomation automation) {
		this.automation = automation;
		this.driver = automation.getDriver();
	}
	
	@Given("^I navigate to the Admin login page$")
	public void iNavigate_to_the_admin_login_page() {
		driver.get(System.getProperty("adminBaseURL"));
		// mandrews: maximize blows up for some reason when using grid,
		// so only maximize when running locally.
		if (System.getProperty("env").equals("local") && !System.getProperty("os.name").toLowerCase().startsWith("mac os x")) {
			driver.manage().window().maximize();
		}
		// don't let IE11 systems resize the browser smaller
		if (System.getProperty("browser").equals("IE11")) {
			driver.manage().window().maximize();
		}
		if (System.getProperty("env").equals("grid")) {
			if (System.getProperty("browser").equals("chrome")) {
				// can't just maximize chrome, you have to set specific resolution
				//driver.manage().window().setSize(new Dimension(1366, 768));
				driver.manage().window().setSize(new Dimension(1440, 1440));
			}
		}		
		System.out.printf(GlobalVariables.getTestID() + " INFO: Browser resolution = %s\n", driver.manage().window().getSize());
	}		

	//Log in as admin
	@And("^I enter my admin as \"([^\"]*)\" and Password as \"([^\"]*)\"$")
	public void I_enter_my_Email_as_and_Password_as (String arg1, String arg2) throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " Logging in as system admin.\n");
		driver.findElement(By.id("email")).sendKeys(arg1);
		driver.findElement(By.id("password")).sendKeys(arg2);		
		driver.findElement(By.xpath("//button[contains(.,'Log In')]")).click();
		TimeUnit.SECONDS.sleep(2);
	}
	
	// Successful login result. I will be on the admin home page
	@Then("^I will be on the admin Home page$")
	public void i_will_be_on_the_admin_home_page() {
	    new HelperMethods().addSystemWait(2);	    
		String actual_url = driver.getCurrentUrl();
		String message = "Test failed: Admin login failed!";
		Assert.assertTrue(message, actual_url.contains("admin"));		
        System.out.println(GlobalVariables.getTestID() + " Test passed: Admin login was successful!\n");
	}

	@When("^I search for my locked user as \"([^\"]*)\"$")
		public void i_search_for_my_locked_user_as(String arg1) {
		System.out.println(GlobalVariables.getTestID() + " Search for user to unlock.\n");
		driver.get((System.getProperty("adminBaseURL") + "users/search"));
		driver.findElement(By.xpath("//input[@id='txtUserName']")).sendKeys(arg1);
		driver.findElement(By.xpath("//button[contains(.,'Search')]")).click();
	}	
	
	
	@Then("^I will clear the lock for my user")
	public void i_will_clear_the_lock__for_my_user() throws Throwable {
		//Unlock link won't be there unless the user is too
		Boolean islinkthere = driver.findElements(By.xpath("//button[contains(.,'Unlock')]")).size()!=0;				 
		if(islinkthere) {
			System.out.println(GlobalVariables.getTestID() + " Test passed: Unlock link is found.\n");			
			TimeUnit.SECONDS.sleep(2);
			driver.findElement(By.xpath("//button[contains(.,'Unlock')]")).click();
			System.out.println(GlobalVariables.getTestID() + " Test passed: User unlocked.\n");
			System.out.println(GlobalVariables.getTestID() + " Ready to login with unlocked user.\n");
			TimeUnit.SECONDS.sleep(2);
		} else {
			System.out.println(GlobalVariables.getTestID() + " Test failed: Missing user or Unlock link.\n");
		}		
	}	
}
	
