@must @Bid_List @bidSearch_detail @level2
Feature: Dashboard - Bid Lists - New For You 
  This feature tests the "New For You" bid list
  ARGO-1234: New For You - time filters not working (10 day,Expired,blank)
  ARGO-1235: Your Saved Bids
  ARGO-1499: No logic to deal with no results for "LOAD MORE BIDS"	
  ARGO-1813: Sorting the bid table before loading more bids causes the load to hang 
  ARGO-2124: Dates and expiration displaying incorrectly for bids
  ARGO-2281: Huge slowdown on "Load More" bids
  BIDSYNC-152: Make column titles static when user scrolls bids list

 
@ARGO-1235
Scenario Outline: "New For You" bid list displays correct table headers
	Given I have logged into the Supplier side as user "National FMC"
	When I have navigated to the "<tab>" bid tab
#	Then I will verify that the List columns are <columns>
#	Fix for release OCT 7
	Then I verify that the List columns are displayed

    
Examples:
	| tab             | columns                                                  |
	| New For You     | Match, Bid Title, Bid ID, Agency, State, End Date, Added |


Scenario Outline: Load more bids
	Given I have logged into the Supplier side as user "National FMC"
	When I have navigated to the "<tab>" bid tab
	Then I will verify that the List can load more bids
  
	Examples:
	| tab             |
	| New For You     |
  

@ARGO-1234
Scenario: All bids in New For You tab are from the last 10 days
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to the "New For You" bid tab
	Then all bids in the table are from the last 10 days
    
  
@ARGO-1234
Scenario: No bids in New For You tab are expired
Note: Currently "Upgrade Plan" bids are excluded from this test
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to the "New For You" bid tab
	Then no bids in the table are expired
    
  
@ARGO-1234
Scenario: The bids in the New For You tab are sorted by relevance
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to the "New For You" bid tab
	Then the List is sorted in descending order by "Match" in New for you Bid tab

    
@ARGO-2124
Scenario: End dates in the bid detail page are correct
Note: Currently "Upgrade Plan" bids are excluded from this test
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to the "New For You" bid tab
	And I view the details of the first bid in the list
	Then the bid details has the correct end date

	
@BIDSYNC-152
Scenario Outline: Column titles should be static when the user scrolls the "New For You" bid list or loads more
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to the "<tab>" bid tab
	When I view the last bid currently displaying in the bid list in "New For You" bid tab
	#Then I will verify that the List columns are <columns>
	#Fix for release OCT 7
	Then I verify that the List columns are displayed
	When I click on load more, if it is available
	And I view the last bid currently displaying in the bid list in "New For You" bid tab
	#Then I will verify that the List columns are <columns>
	#Fix for release OCT 7
	Then I verify that the List columns are displayed

Examples:
	| tab             | columns                                                  |
	| New For You     | Match, Bid Title, Bid ID, Agency, State, End Date, Added |

  
#dmidura: timeouts on spinner do not seem to be properly functioning in jenkins. Ignoring test until can figure out why.
@ignore @ARGO-1499 @ARGO-2281
Scenario: "Load More Bids" button only appears when there are more bids to load on New For You. Pagination is not slow.
	Given I have logged into the Supplier side as user "National FMC"
	When I am viewing all the bids on the "New For You" tab
	Then I will not see a load more bids button
	

@ignore @ARGO-1813
Scenario Outline: Lxoad more bids obeys previous sort
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to the "<tab>" bid tab
	And I sort by "<column>"
	When I load more bids
	Then the List is sorted in ascending order by "<column>" in New for you Bid tab
  
	Examples:
	| tab         | column    |
	| New For You | Bid Title |
	| New For You | Bid ID    |
	| New For You | Agency    |
	| New For You | State     |
	| New For You | End Date  |
	| New For You | Added     |

   
@ignore @ARGO-2124
Scenario Outline: Dates in the "New For You" Bid list are correct
Note: Currently "Upgrade Plan" bids are excluded from this test
dmidura: Test is currently broken. Marking @should until can fix
	Given I have logged into the Supplier side as user "National FMC"
	When I have navigated to the "<tab>" bid tab
	Then I will verify that the bids in the list have the correct dates

Examples:
    | tab             |
    | New For You     |
   