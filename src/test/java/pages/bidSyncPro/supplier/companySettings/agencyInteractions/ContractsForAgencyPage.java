/**
 *
 */
package pages.bidSyncPro.supplier.companySettings.agencyInteractions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import pages.common.pageObjects.toBeDeprecated.Document;

/**
 * @author jlara
 * <p>TODO: Refactor this class</p>
 */
public class ContractsForAgencyPage extends Document{
	

	private final String xpath = "//form[@name='theform' and @action='/DPX']";
	private final String xpathPageTitle = xpath + "//table/tbody/tr/td[@class='OPENING' and contains(.,'Contracts')]";

	private final String xpathSectionSearchHdrLabel = xpath + "//table/tbody/tr/th[@class='blueF' and contains(.,'Search') and not(@nowrap)]";

	private final String xpathSearchForLabel = xpath + "//table/tbody/tr/td[contains(.,'Search For')]";
	private final String xpathSearchForTextBox = xpath + "//div[@class='tooltipcontainer']/input[@type='text' and @name='srch']";
	private final String xpathSearchForTitleDescriptionRadioButton = xpath + "//div[@class='tooltipcontainer']/input[@type='radio' and @name='srchtype' and @value='1']";
	private final String xpathSearchForContractNumberRadioButton = xpath + "//div[@class='tooltipcontainer']/input[@type='radio' and @name='srchtype' and @value='2']";

	private final String xpathContractTypeLabel = xpath + "//table/tbody/tr/td[contains(.,'Contract Type')]";
	private final String xpathContractTypeSelect = xpath + "//table/tbody/tr/td/select[@name='contracttype']";

	private final String xpathQualificationsLabel = xpath + "//table/tbody/tr/td[normalize-space()='Qualifications']";
	private final String xpathQualificationsCheckBox = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='<NAME>']";

	private final String xpathExpiredBetweenLabel = xpath + "//table/tbody/tr/td[contains(.,'Expires between')]";
	private final String xpathExpiredBetweenFromLabel = xpath + "//table/tbody/tr/td/b[text()='From']";
	private final String xpathExpiredBetweenToLabel = xpath + "//table/tbody/tr/td/strong[text()='To']";
	private final String xpathExpiredBetweenFromTextBox = xpath + "//table/tbody/tr/td/input[@type='text' and @name='fromdate']";
	private final String xpathExpiredBetweenToTextBox = xpath + "//table/tbody/tr/td/input[@type='text' and @name='todate']";
	private final String xpathDateFormatDirections = xpath + "//font[@class='small' and contains(.,'format date')]";

	private final String xpathClassificationsLabel = xpath + "//table/tbody/tr/td[normalize-space()='Classifications']";
	private final String xpathOptionsLabel = xpath + "//table/tbody/tr/td[normalize-space()='Options']";
	private final String xpathIncludeExpiredContractsCheckBox = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='incexp']";
    private final String xpathSearchButton = xpath + "//table/tbody/tr[last()]/td/a[@href='javascript:document.theform.submit();']";
	private final String xpathResetButton = xpath + "//table/tbody/tr[last()]/td/a[@href='/DPX?ac=agencycontlist' and @class='secondaryButton']";
	
	public ContractsForAgencyPage(EventFiringWebDriver driver) throws Throwable {
		super(driver);

		//this is the url of the parent page. this page and its elements are contained in an iframe
		this.setPagePath(System.getProperty("supplierBaseURL") + "admin/agency-interaction");

      //iframe[@src='https://stage.bidsync.com/DPX?ac=agencycontlist&foroid=<agency_id>']
	} // end of constructor

	
	public ContractsForAgencyPage sendKeysSearchForTextBox(CharSequence... keys) {
		this.getSearchForTextBox().sendKeys(keys);
		return this;
	}

	public ContractsForAgencyPage clickSearchForTitleDescriptionRadioButton() {	
		getSearchForTitleDescriptionRadioButton().click();
		return this;	
	}

	public ContractsForAgencyPage clickSearchForContractNumberRadioButton() {	
		getSearchForContractNumberRadioButton().click();
		return this;	
	}
	
	public ContractsForAgencyPage selectContractTypeOptionByValue(String optionValue) {
		getContractTypeSelect().selectByValue(optionValue);
		return this;
	}
	
	public ContractsForAgencyPage selectContractTypeOptionByText(String optionText) {
		getContractTypeSelect().selectByVisibleText(optionText);
		return this;
	}
	
	public ContractsForAgencyPage clickQualificationsCheckBox(String name) {
		getQualificationsCheckBox(name).click();
		return this;
	}

	public ContractsForAgencyPage sendKeysExpiredBetweenFromTextBox(CharSequence... keys) {
		this.getExpiredBetweenFromTextBox().sendKeys(keys);
		return this;
	}
	
	public ContractsForAgencyPage sendKeysExpiredBetweenToTextBox(CharSequence... keys) {
		this.getExpiredBetweenToTextBox().sendKeys(keys);
		return this;
	}
	
	public ContractsForAgencyPage clickIncludeExpiredContractsCheckBox() {	
		getIncludeExpiredContractsCheckBox().click();
		return this;	
	}
	public ContractsForAgencyPage clickSearchButton() {	
		getSearchButton().click();
		return this;	
	}
	
	public ContractsForAgencyPage clickResetButton() {	
		getResetButton().click();
		return this;	
	}
	
	public WebElement getPageTitle() {	return driver.findElement(By.xpath(xpathPageTitle));	}

	public WebElement getSectionSearchHdrLabel() {	return driver.findElement(By.xpath(xpathSectionSearchHdrLabel));	}

	public WebElement getSearchForLabel() {	return driver.findElement(By.xpath(xpathSearchForLabel));	}

	public WebElement getSearchForTextBox() {	return driver.findElement(By.xpath(xpathSearchForTextBox));	}

	public WebElement getSearchForTitleDescriptionRadioButton() {	return driver.findElement(By.xpath(xpathSearchForTitleDescriptionRadioButton));	}

	public WebElement getSearchForContractNumberRadioButton() {	return driver.findElement(By.xpath(xpathSearchForContractNumberRadioButton));	}

	public WebElement getContractTypeLabel() {	return driver.findElement(By.xpath(xpathContractTypeLabel));	}

	public Select getContractTypeSelect() {	return new Select(driver.findElement(By.xpath(xpathContractTypeSelect)));	}

	public WebElement getQualificationsLabel() {	return driver.findElement(By.xpath(xpathQualificationsLabel));	}

	public WebElement getQualificationsCheckBox(String name) {	
		return driver.findElement(By.xpath(xpathQualificationsCheckBox.replace("<NAME>", name)));	
	}

	public WebElement getExpiredBetweenLabel() {	return driver.findElement(By.xpath(xpathExpiredBetweenLabel));	}

	public WebElement getExpiredBetweenFromLabel() {	return driver.findElement(By.xpath(xpathExpiredBetweenFromLabel));	}

	public WebElement getExpiredBetweenToLabel() {	return driver.findElement(By.xpath(xpathExpiredBetweenToLabel));	}

	public WebElement getExpiredBetweenFromTextBox() {	return driver.findElement(By.xpath(xpathExpiredBetweenFromTextBox));	}

	public WebElement getExpiredBetweenToTextBox() {	return driver.findElement(By.xpath(xpathExpiredBetweenToTextBox));	}

	public WebElement getDateFormatDirections() {	return driver.findElement(By.xpath(xpathDateFormatDirections));	}

	public WebElement getClassificationsLabel() {	return driver.findElement(By.xpath(xpathClassificationsLabel));	}

	public WebElement getOptionsLabel() {	return driver.findElement(By.xpath(xpathOptionsLabel));	}

	public WebElement getIncludeExpiredContractsCheckBox() {	return driver.findElement(By.xpath(xpathIncludeExpiredContractsCheckBox));	}

	public WebElement getSearchButton() {	return driver.findElement(By.xpath(xpathSearchButton));	}

	public WebElement getResetButton() {	return driver.findElement(By.xpath(xpathResetButton));	}
	
	/*
	 BRG = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_2921']";	//BRG
	private final String xpathQualificationsCheckboxBRG2 = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_2922']";	//BRG2
	private final String xpathQualificationsCheckboxBRG3 = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_2923']";	//BRG3
	private final String xpathQualificationsCheckboxDBE = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_1521']";	//DBE
	private final String xpathQualificationsCheckboxGBAA = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_3262']";	//GB-AA
	private final String xpathQualificationsCheckboxGBAM = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_3241']";	//GB-AM
	private final String xpathQualificationsCheckboxGBAUO = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_3221']";	//GB-AUO
	private final String xpathQualificationsCheckboxGBNOTHING = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_3261']";	//GB-NOTHING
	private final String xpathQualificationsCheckboxMBE = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_1522']";	//MBE
	private final String xpathQualificationsCheckboxMIKE5 = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_3121']";	//MIKE5
	private final String xpathQualificationsCheckboxMIKETEST1WOCERT = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_3681']";	//MIKETEST1WOCERT
	private final String xpathQualificationsCheckboxMIKETEST2WCERT = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_3682']";	//MIKETEST2WCERT
	private final String xpathQualificationsCheckboxNOTHING = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_3161']";	//NOTHING
	private final String xpathQualificationsCheckboxPOOL1 = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_3761']";	//POOL-1
	private final String xpathQualificationsCheckboxSB = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_1524']";	//SB
	private final String xpathQualificationsCheckboxTEMP4 = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_3622']";	//TEMP 4
	private final String xpathQualificationsCheckboxWBE = xpath + "//table/tbody/tr/td/input[@type='checkbox' and @name='qualid_1523']";	//WBE
	 */

}
