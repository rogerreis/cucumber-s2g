@must @registration @subscriptions @level1
Feature: Supplier Registration - End-to-End Registration Tests
	BIDSYNC-36: Clicking "TAKE ME TO THE BID LIST" will sometimes take user to login screen
	BIDSYNC-408: Vendors unable to access Pro Subscription Bids
	BIDSYNC-476: Duplicate Customers and Subscriptions being created in ChargeBee


@BIDSYNC-408 @BIDSYNC-476
Scenario: Supplier Registers for a BidSync Basic Account and Completes Supplier Registration
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	When I complete Supplier Registration
	Then I will be logged into BidSync for the first time and verify page initialization
	And That user's data will be stored in the database
	And I will verify that only one user customer account exists for my user in the Chargebee test site
	And I will verify that my user's customer_id matches in the ARGO database and Chargebee test site
	And I will verify that my user only has a "BidSync Basic" subscription plan listed in the ARGO database
	And I will verify that my user only has a "BidSync Basic" subscription plan listed in the Chargebee test site 

	
@BIDSYNC-408 @BIDSYNC-476
Scenario Outline: Supplier Registers for a BidSync Pro Account and Completes Supplier Registration
	Given I register a new user in Masonry with a "<subscriptionType>" subscription plan
	When I complete Supplier Registration
	Then I will be logged into BidSync for the first time and verify page initialization
	And That user's data will be stored in the database
	And I will verify that only one user customer account exists for my user in the Chargebee test site
	And I will verify that my user's customer_id matches in the ARGO database and Chargebee test site
	And I will verify that my user only has a "<subscriptionType>" subscription plan listed in the ARGO database
	And I will verify that my user only has a "<subscriptionType>" subscription plan listed in the Chargebee test site 

  
  	Examples:
	| subscriptionType           |
	| BidSync National - Yearly  |
	| BidSync National - Monthly |
	| BidSync National - 6 Month |
	| BidSync Regional - Yearly  |
	| BidSync Regional - Monthly |
	| BidSync Regional - 6 Month |
	| BidSync State - Yearly     |
	| BidSync State - Monthly    |
	| BidSync State - 6 Month    |
 
	
# TODO:
# 1. Verify that when positive and negative keywords are added, the keywords appear on first login