package pages.chargebeeSite.subscriptions.subscriptionDetails.cancelSubscriptionFlow;

import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.chargebeeSite.subscriptions.subscriptionDetails.SubscriptionDetails;
import pages.common.pageObjects.BasePageActions;
/***
 * <h1>CancelSubscriptionPopup</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects for the  "Cancel Subscription" popup that is <br>
 * 	accessed by clicking on "Cancel Subscription" button on the Subscriptions->Subscription Detail page<br>
 * 			   on the Chargebee test site</p>
 */
public class CancelSubscriptionPopup extends BasePageActions {
	
	// xpaths -- add as needed
	private final String scheduleCancelationBtn_xpath = "//span[contains(@class, 'danger')][contains(text(), 'Schedule Cancellation')]";
	
	// Other
	private EventFiringWebDriver driver;
	
	public CancelSubscriptionPopup(EventFiringWebDriver driver ){
		super(driver);
		this.driver = driver;
	}
	
	/* ----- getters ----- */
	private WebElement getSchedulateCancelationBtn() { return findByVisibility(By.xpath(this.scheduleCancelationBtn_xpath));} 

	/* ----- methods ----- */
	/****
	 * <h1>clickOnScheduleCancelation</h1>
	 * <p>purpose: Click on the "Schedule Cancelation" button on the <br>
	 * 	"Cancel Subscription - <company name>" popup that is accessed via <br>
	 * 	Subscriptions -> Subscription Details -> Cancel Subscription </p>
	 * <p>Note: After scheduling the cancelation, the subscription may <br>
	 * 	be cancelled immediately by clicking on "Cancel Immediately" button<br>
	 * 	that appears on the Subscription Details after scheduling the cancelation</p>
	 * @return SubscriptionDetails
	 */
	public SubscriptionDetails clickOnScheduleCancelation() {
		this.getSchedulateCancelationBtn().click();
		return new SubscriptionDetails(driver);
	}

}
