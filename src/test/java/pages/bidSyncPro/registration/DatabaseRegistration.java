package pages.bidSyncPro.registration;

import static org.junit.Assert.fail;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;

import data.bidSyncPro.registration.RandomChargebeeUser;
import data.bidSyncPro.registration.SubscriptionPlan;
import pages.bidSyncPro.common.DatabaseCommonSearches;
import pages.common.helpers.GeographyHelpers;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import utility.database.SelectQueryBuilder;

/***
 * <h1>Class DatabaseRegistration</h1>
 * @author dmidura
 * <p>details: This class contains methods to access the ARGO database to verify values were correctly<br>
 * 	set during registration</p>
 * <p>NOTE: This class is currently under refactor</p>
 */
public class DatabaseRegistration {
	
	private Map <String, String> registrationValues;	 // Registration input stored in ARGO
															 // (static so we don't waste overhead accessing db multiple times)
															 // in the same test
	private GeographyHelpers geographyHelpers;
	HelperMethods helperMethods = new HelperMethods();
	long TIMEOUT_MS = 60000;
	/***
	 * <h1>DatabaseRegistration</h1>
	 * <p>purpose: Use this constructor to immediately run call to get database values</p>
	 * @param userEmail = user's registration email
	 */
	public DatabaseRegistration(String userEmail) {
		registrationValues 	= new HashMap <String,String>(); 
		geographyHelpers    = new GeographyHelpers();
		this.getUserRegistrationRecordFromDatabase(userEmail);
	}
	
	/* ----- methods ----- */
	/***
	 * <h1>getUserRegistrationRecordFromDatabase</h1>
	 * <p>purpose: Retrieve the ARGO database registration record for the strUserName<br>
	 * 	Expectation is that this user record should match the registration information <br>
	 * 	user inputed during registration</p>
     * @param strUserName
     * @return Map<String, String> are the database values in form <column_name, value>
     */
	private Map<String, String> getUserRegistrationRecordFromDatabase(String strUserName) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Searching ARGO database for user record for user \"%s\"\n", strUserName);
		
		// Database query for recorded registration values
		String strQuery  = "select * from users where username = \'" + strUserName +"\'";
		String strDBName = "auth";

		// dmidura: additional column labels may be added if Chargebee registration 
		// 				 requires more inputs
		String [] returnFields = {"user_id", "supplier_id", "customer_id", "status",
									 "username", "first_name", "last_name","phone", "company_name", "country",
				 					 "email", "line1", "line2", "city", "state", "zip", "phone_ext"};

		Map <String, String> databaseValues = new HashMap <String, String>();
		databaseValues = new SelectQueryBuilder().runSearchReturnFields(strQuery, strDBName, returnFields);

		registrationValues.putAll(databaseValues);
		
		System.out.println(GlobalVariables.getTestID() + " ------------ ARGO Database Registration Values ----------");
		System.out.println(databaseValues);
		System.out.println(GlobalVariables.getTestID() + " ---------------------------------------------------------");
		
		return databaseValues;
	}

	/***
	 * <h1>isUserRegistrationCustomerIdBlank</h1>
	 * <p>purpose: Determine if the customer_id in the user record returned from the ARGO<br>
	 * 	database is blank. Expectation is that customer_id is blank after initial Chargebee registration<br>
	 * 	(before extracting email token), and then customer_id is set after token is initialized</p>
	 * @return true if customer_id is blank in ARGO database | false if customer_id is not blank in ARGO database
	 */
	private Boolean isUserRegistrationCustomerIdBlank() {
		if(registrationValues.get("customer_id") != null) {
			return (registrationValues.get("customer_id").isEmpty());
		} else { 
			return true; }
	}

	/***
	 * <h1>isUserRegistrationStatusRegistered</h1>
	 * <p>purpose: Determine if the status of the user record returned from the ARGO<br>
	 * 	database is registered. Expectation is that status is registered (not active) after initial Chargebee registration<br>
	 * 	(before extracting email token), and then status is set to active after token is initialized</p>
	 * @return true if user status is registered in ARGO database | false if user status is not registered in ARGO database
	 */
	private Boolean isUserRegistrationStatusRegistered() {
		return registrationValues.get("status").equals("R");
	}	
		

	/***
	 * <h1>isUserRegistrationStatusVerified</h1>
	 * <p>purpose: Determine if the status of the user record returned from the ARGO<br>
	 * 	database is verified. Expectation is that status is registered (not active) after initial Chargebee registration<br>
	 * 	(before extracting email token), verified after token initialization, and active after completed registration</p>
	 * @return true if user status is active in ARGO database | false if user status is registered in ARGO database
	 */
	private Boolean isUserRegistrationStatusVerified() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Determining if user registration status is verified in ARGO database record");
		return registrationValues.get("status").equals("V");
	}

	/***
	 * <h1>isUserRegistrationStatusActive</h1>
	 * <p>purpose: Determine if the status of the user record returned from the ARGO<br>
	 * 	database is active. Expectation is that status is registered (not active) after initial Chargebee registration<br>
	 * 	(before extracting email token), verified after token initialization, and active after completed registration</p>
	 * @return true if user status is active in ARGO database | false if user status is registered in ARGO database
	 */
	@SuppressWarnings("unused")
	private Boolean isUserRegistrationStatusActive() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Determining if user registration status is active in ARGO database record");
		return registrationValues.get("status").equals("A");
	}	
	

//	/***
//	 * <h1>getUserSubscriptionRecordFromDatabase</h1>
//	 * <p>purpose: Retrieve the ARGO database subscription record for the strUserName<br>
//	 * 	Expectation is that this user subscription record should match the registration information <br>
//	 * 	user inputed during the Chargebee registration</p>
//	 * @param strUserName
//	 * @throws SQLException
//     * @throws ClassNotFoundException
//	 */
//	public void getUserSubscriptionRecordFromDatabase(String strUserName) throws SQLException, ClassNotFoundException {
//		System.out.printf(GlobalVariables.getTestID() + " INFO: Searching ARGO database for user record for user \"%s\"\n", strUserName);
//		
//		// Subscription data is housed among multiple tables:
//		// Query 1:
//		String strQuery  = "select * from subscription where user_id = \'" + registrationValues.get("user_id") +"\'";
//		String strDBname = "subscription";
//		System.out.printf(GlobalVariables.getTestID() + " INFO: Running query \"%s\" on database \"%s\" \n", strQuery, strDBname);
//		ResultSet rs = SQLConnector.executeQuery(strDBname, strQuery);
//		Map <String, String> databaseValues = new HashMap <String, String> ();
//
//		// TODO dmidura: additional column labels may be added if Chargebee subscription verification
//		// 				 requires matching to additional data 
//		String [] strColumnLabels = {"subscription_id", "user_id", "name", "type",
//									 "renewal_date", "status", "external_id"};
//		while (rs.next()) {
//			for (String strColumnLabel : strColumnLabels) {
//				databaseValues.put(strColumnLabel, rs.getString(strColumnLabel)); }}
//
//		// We should only have one user subscription record in ARGO for this newly registered user. If not... something is really wrong
//		if (databaseValues.isEmpty()) {
//			fail ("Test Failed: ARGO database does not contain user subscription record for user " + strUserName);
//		} else if (databaseValues.size() > strColumnLabels.length) {
//			fail("Test Failed: ARGO database has more than one user subscription record for the user " + strUserName); }
//		
//		// Query 2:
//		strQuery  = "select * from subscription_groupings where subscription_id = \'" + databaseValues.get("subscription_id") +"\'";
//		System.out.printf(GlobalVariables.getTestID() + " INFO: Running query \"%s\" on database \"%s\" \n", strQuery, strDBname);
//		rs = SQLConnector.executeQuery(strDBname, strQuery);
//
//		// we only need the grouping_id from this query
//		String strGroupingId="";
//		while (rs.next()) { strGroupingId = rs.getString("grouping_id");}
//		if (strGroupingId.isEmpty()) {fail("Test Failed: ARGO database did not contain a grouping_id while returning user subscription record.");}
//		
//		// Query 3:
//		strQuery  = "select * from grouping where grouping_id = \'" + strGroupingId +"\'";
//		System.out.printf(GlobalVariables.getTestID() + " INFO: Running query \"%s\" on database \"%s\" \n", strQuery, strDBname);
//		rs = SQLConnector.executeQuery(strDBname, strQuery);
//		
//		// we only care about the state from this query
//		while (rs.next()) {
//			databaseValues.put("state", rs.getString("name")); }
//
//		subscriptionValues.putAll(databaseValues);
//		
//		System.out.println(GlobalVariables.getTestID() + " ------------ ARGO Database Subscription Values ----------");
//		System.out.println(databaseValues);
//		System.out.println(GlobalVariables.getTestID() + " ---------------------------------------------------------");
//	}
//	
//	/***
//	 * <h1>isSubscriptionIdValueAsExpected</h1>
//	 * <p>purpose: Determine if the subscription id set by Chargebee matches the value stored in ARGO database<br>
//	 * Note: In ARGO database, this value is labeled external_id (subscription_id is used elsewhere by ARGO)</p>
//	 * @param strSubscriptionID = expected Subscription ID
//	 * @return true if ARGO database subscription ID matches strSubscriptionID | false if it doesn't
//	 */
//	public Boolean isSubscriptionIdValueAsExpected(String strSubscriptionID) {
//		System.out.printf(GlobalVariables.getTestID() + " INFO: Determining if subscription id matches \"%s\"\n", strSubscriptionID);
//		return subscriptionValues.get("external_id").equals(strSubscriptionID);
//	}
//	
//	/***
//	 * <h1>isSubscriptionStatusActive</h1>
//	 * <p>purpose: Determine if the subscription status is active</p>
//	 * @return true if subscription status is active | false if subscription status is not active
//	 */
//	public Boolean isSubscriptionStatusActive() {
//		System.out.println(GlobalVariables.getTestID() + " INFO: Determining if subscription status is active");
//		return subscriptionValues.get("status").equals("A");
//	}
//	
//	/***
//	 * <h1>isSubscriptionStateAsExpected</h1>
//	 * <p>purpose: Determine if the state recorded for the user's subscription in the ARGO database<br>
//	 * 	matches what the user registered for during Chargebee registration</p>
//	 * @param strStateAbbrev = state of registration<br>
//	 * 	ex: "TX" </p>
//	 * @return true if ARGO database has recorded the state with the user's subscription | false if not
//	 */
//	public Boolean isSubscriptionStateAsExpected(String strStateAbbrev) {
//		System.out.printf(GlobalVariables.getTestID() + " INFO: Verifying that ARGO database correctly recorded subscription as \"%s\"\n", strStateAbbrev);
//		
//		// ARGO database records the state name, and our input was a state abbrev
//		String strDatabaseStateAbbrev = helperMethods.getStateOrProvinceAbbreviationFromStateName(subscriptionValues.get("state"));
//
//		return strDatabaseStateAbbrev.equals(strStateAbbrev);
//	}
//
//	
//	
//
	/* ----- verifications ----- */
	/***
	 * <h1>verifyUserRegistrationCustomerIdAsExpected</h1>
	 * <p>purpose: Determine if the customer_id in the user record returned from the ARGO<br>
	 * 	equals the expected value. Fail test if it doesn't</p>
	 * @param strExpectedValue = expected customer id (as set in the Chargebee test site)
	 * @return true if customer_id is blank in ARGO database | false if customer_id is not blank in ARGO database
	 */
	public DatabaseRegistration verifyUserRegistrationCustomerIdAsExpected(String strExpectedValue) {
		Assert.assertTrue("Test Failed: Customer id stored in ARGO database is incorrect", registrationValues.get("customer_id").equals(strExpectedValue));
		System.out.printf(GlobalVariables.getTestID() + " INFO: Verified that the customer_id in ARGO database record was recorded as \"%s\"\n", strExpectedValue);
		return this;
	}

	/***
	 * <h1>verifySubscriptionPlan</h1>
	 * <p>purpose: This is a quick verification to determine if the user's subscription plan, as<br>
	 * 	currently recorded in the ARGO database matches expectedPlan. Fail test if the plan recorded<br>
	 * 	in the ARGO database does not matched expectedPlan, or if there are multiple plans attributed<br>
	 * 	to this user's email</p>
	 * @param email = user's registration email
	 * @param expectedPlan = SubscriptionPlan that you expect to be recorded in the ARGO database
	 * @return DatabaseRegistration
	 */
	public DatabaseRegistration verifySubscriptionPlan(String email, SubscriptionPlan expectedPlan) {
		
		final long deadline = System.currentTimeMillis() + TIMEOUT_MS;
		long msRemaining;
		boolean stopDBCall =false;
		ResultSet result = null;
		try {
			while(!stopDBCall){
				result = new DatabaseCommonSearches().getChargebeeSubscriptionPlanFromUserEmail_ResultSet(email);
				stopDBCall = result.next();
				if(stopDBCall) {
					if (result.getString("name").equalsIgnoreCase(expectedPlan.getChargebeeSubscriptionName())) {
						stopDBCall = true;
						
					} else
						stopDBCall=false;
				}
				
				msRemaining = deadline - System.currentTimeMillis();
			    if(msRemaining > 0){ helperMethods.addSystemWait(5); } else{ stopDBCall = true; }
			    if(!stopDBCall) result.close();
			};
			
			// Get the value stored in the ARGO database
			Assert.assertEquals("ERROR: Subscription in ARGO database is not correct!", 
					expectedPlan, SubscriptionPlan.fromString(result.getString("name").toString()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that user \"" + email + "\" only has \"" + expectedPlan.getChargebeeSubscriptionName() 
			+ "\" subscription plan recorded in the ARGO database");
		return this;
	}
	
	/***
	 * <h1>verifyRegistrationCustomerIdIsBlank</h1>
	 * <p>purpose: Verify that the customer_id in the user record returned from the ARGO<br>
	 * 	database is blank.<br>
	 *  Note: Expectation is that customer_id is blank after initial Chargebee registration<br>
	 * 	(before extracting email token), and then customer_id is set after token is initialized</p>
	 * @return DatabaseRegistration
	 */
	public DatabaseRegistration verifyRegistrationCustomerIdIsNotBlank() {
		if (isUserRegistrationCustomerIdBlank()) {
			fail("Test Failed: customer_id field for user record in ARGO database is blank immediately after registration\n"
					+  " (but before email token activation)");}

		System.out.println(GlobalVariables.getTestID() + " Verified that customer_id associated with Chargebee registration record is not blank in ARGO database");
		return this;
	}
	
	
	/***
	 * <h1>verifyRegistrationStatusIsRegistered</h1>
	 * <p>purpose: Verify that the status of the user record returned from the ARGO<br>
	 * 	database shows registered.<br>
	 * 	Note: Expectation is that status is registered (not active) after initial Chargebee registration<br>
	 * 	(before extracting email token), and then status is set to active after token is initialized</p>
	 * @return DatabaseRegistration
	 */
	public DatabaseRegistration verifyRegistrationStatusIsRegistered() {
		if (!isUserRegistrationStatusRegistered()) {
			fail("Test Failed: status field for user record in ARGO database is not displaying as registered immediately after Chargebee registration\n"
					+  " (but before email token activation)");
		}

		System.out.println(GlobalVariables.getTestID() + " Verified that status associated with Chargebee registration record is Registered in ARGO database");

		return this;
	}
	/***
	 * <h1>verifyRegistrationStatusIsVerified</h1>
	 * <p>purpose: Verify that the status of the user record returned from the ARGO<br>
	 * 	database shows verified after email verification.<br>
	 * 
	 * @return DatabaseRegistration
	 */
	public DatabaseRegistration verifyRegistrationStatusIsVerified() {
		if (!isUserRegistrationStatusVerified()) {
			fail("Test Failed: status field for user record in ARGO database is not displaying as verified after email token activation");
		}

		System.out.println(GlobalVariables.getTestID() + " Verified that status associated with Chargebee registration record is Verified in ARGO database");

		return this;
	}
	
	/***
	 * <h1>verifyDatabaseRegistrationMatchesRegistrationValues</h1>
	 * <p>purpose: Given the registration data used for Pro or Basic Masonry registration<br>
	 * 	verify that these values are correctly stored in the ARGO database</p>
	 * @param user = user registration data for a BidSync Pro | BidSync Basic registration
	 * @return DatabaseRegistration
	 */
	public DatabaseRegistration verifyDatabaseRegistrationMatchesRegistrationValues(RandomChargebeeUser user) {

		// Grab the registration data from the database
		Map <String,String> databaseValues = registrationValues;
	
		// And verify each registration field matches
		
		 // User Email
        Assert.assertEquals(user.getEmailAddress(), databaseValues.get("username"));
        Assert.assertEquals(user.getEmailAddress(), databaseValues.get("email"));
        
        // User Name
        Assert.assertEquals(user.getFirstName(),    databaseValues.get("first_name"));
        Assert.assertEquals(user.getLastName(),     databaseValues.get("last_name"));
        
        // User Phone
        // In Masonry, Basic has a phone ext, but Pro Plans don't
        Assert.assertEquals(user.getPhoneNumber(),  databaseValues.get("phone")); 
        if (user.getSubscriptionPlan().equals(SubscriptionPlan.BASIC)) {
        	// TODO dmidura: This will currently bomb out b/c Masonry is not sending the phone_ext
        	// Remove this Assert if Masonry decides to pull the phone_ext field
        	// or uncomment when Masonry starts sending

        	//Assert.assertEquals(user.getPhoneExtension(), databaseValues.get("phone_ext"));
        }
        
        // Company
        // In Masonry, Pro Plans have a company, but Basic Plan doesn't have a company
        if (!user.getSubscriptionPlan().equals(SubscriptionPlan.BASIC)) {
        	Assert.assertEquals(user.getCompany().getName(), databaseValues.get("company_name"));}
        
        // Address
        // In Masonry, Basic only has one address line. Pro Plans have two lines for address
        Assert.assertEquals(user.getAddress(), databaseValues.get("line1"));

        if (!user.getSubscriptionPlan().equals(SubscriptionPlan.BASIC)) {
        	Assert.assertEquals(user.getAddressLine2(), databaseValues.get("line2"));}
        Assert.assertEquals(user.getCity(),    databaseValues.get("city"));
        Assert.assertEquals(geographyHelpers.getStateOrProvinceAbbreviationFromStateName(user.getState()), databaseValues.get("state"));
        Assert.assertEquals(user.getZipCode(), databaseValues.get("zip"));
        
        // In Masonry, Basic plans assume that country is USA, and do not register a country
        if (!user.getSubscriptionPlan().equals(SubscriptionPlan.BASIC)) {
        	Assert.assertEquals(geographyHelpers.getCountryCodeFromCountryName(user.getCountry()), databaseValues.get("country"));}
        
        
        System.out.println(GlobalVariables.getTestID() + " Verified that user registration is correctly stored in ARGO database");
		return this;
	}

}
