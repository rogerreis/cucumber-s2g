package stepDefinitions.buyspeed;

import core.CoreAutomation;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.buyspeed.BuySpeedLogin;
import pages.buyspeed.seller.SellerHome;
import pages.common.helpers.GlobalVariables;

import org.openqa.selenium.support.events.EventFiringWebDriver;

/***
 * <h1>Class Integrations_BuySpeedToARGO_stepDefinitions</h1>
 * @author dmidura
 * <p>details: This class houses test steps to test the BuySpeed to new BidSync linkages<br>
 *          Pages: BuySpeed login, BuySpeed Seller home</p>
 */
public class Integrations_BuySpeedToARGO_stepDefinitions {
	public CoreAutomation automation;
	public EventFiringWebDriver driver;

	public Integrations_BuySpeedToARGO_stepDefinitions(CoreAutomation automation) throws Throwable {
		this.automation = automation;
		this.driver     = automation.getDriver();
	}

	/* ------------ Test Steps ---------------- */
	
	@Given ("^I have logged into BuySpeed as seller \"(.*)\"$")
	public void I_have_logged_into_BuySpeed_as_user (String strUserReference) {
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Logging into BuySpeed as user %s\n", strUserReference);
		new BuySpeedLogin(driver).loginAsSellerUser(strUserReference);
	}
	
	@When ("^I click on the Register With BidSync button on the BuySpeed Seller Home Screen$")
	public void I_click_on_the_Register_With_BidSync_button_on_the_BuySpeed_Seller_Home_Screen() {
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on the \"Register With BidSync\" button on the BuySpeed Seller Home Screen.\n");
		new SellerHome(driver).clickOnRegisterWithBidSyncBtn();
	}

	@Then ("^I will see the Register With BidSync popup$")
	public void I_will_see_the_Register_With_BidSync_popup() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that user is viewing the \"Register with BidSync\" popup off the BuySpeed Seller Home Screen");
		new SellerHome(driver).verifyBidSyncRegistrationPopupIsDisplayed();
		System.out.printf(GlobalVariables.getTestID() + " Test Passed: \"Register With BidSync\" popup is visible");
	}
}
