@must @ARGO-363 @administration @level3
Feature: Login - Three Level Security Access and Remember Me
	ARGO-363: UI - 3-Level Access & Remember Me Checkbox

Scenario: User can select/deselect Remember Me Checkbox
	Given I am on the login screen
	When I check the Remember Me Checkbox
	Then I will verify that the checkbox is selected
	When I uncheck the Remember Me Checkbox
	Then I will verify that the checkbox is unselected

Scenario: If Remember Me was selected, then user does not need to login for a month
	Given I have selected Remember Me and logged into Bidsync Pro
	When I have waited 30 minutes for my token to expire
	Then I will verify in the browser local storage that Remember Me is set to 30 days from now
	When I change my timestamp for partialTokenExpiration in local storage to be one second behind the current time
	Then I will verify that Remember Me has expired because I need to provide credentials to view each of the privileged pages in BidSync Pro
	
Scenario: If Remember Me was selected, then user can view all public and privileged pages
	Given I have selected Remember Me and logged into Bidsync Pro
	When I have closed my current browser window, waited 30 minutes, and navigated directly to the Bidsync Dashboard in a new browser window
	Then I will verify that I do not need to provide credentials to view each of the public and privileged pages in BidSync Pro
	
Scenario: If Remember Me was selected, then user must provide credentials to view secure pages
	Given I have selected Remember Me and logged into Bidsync Pro
	When I have closed my current browser window, waited 30 minutes, and navigated directly to the Bidsync Dashboard in a new browser window
	Then I will verify that I need to provide credentials to view each of the secure pages in BidSync Pro
    When I provide credentials to one of the secure pages
    Then I will verify that I do not need to provide credentials to view each of the secure pages in BidSync Pro

# Other tests:
Scenario: User who is not Remember Me can still access all pages in Bidsync without being prompted to login during session
	Given I have logged into the Supplier side as user "Basic"
	Then I will verify that I am not required to provide credentials to view each page in BidSync Pro

Scenario: User who is not Remember Me does not have rememberMe or partialToken variables set in local storage
	Given I have logged into the Supplier side as user "Basic"
	Then I will verify that I do not have rememberMe or partialToken variables set in the local storage