package pages.bidSyncPro.supplier.companySettings.manageUsers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.pageObjects.BasePageActions;

public class ManageUsersConfirmRevokeLicense extends BasePageActions {
	
	// xpaths
	private final String cancelBtn_xpath   = "//button[contains(span,'Cancel')]";
	private final String confirmBtn_xpath  = "//button[contains(span,'Confirm')]";
	private final String closeBtn_xpath    = "//mat-card//div[contains(@class,'close_button')]//button";
	
	// Other
	EventFiringWebDriver driver;

	public ManageUsersConfirmRevokeLicense(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	/* ----- getters ----- */
	private WebElement getCancelBtn()  {return findByPresence(By.xpath(this.cancelBtn_xpath));  }
	private WebElement getConfirmBtn() {return findByPresence(By.xpath(this.confirmBtn_xpath)); }
	private WebElement getCloseBtn()   {return findByPresence(By.xpath(this.closeBtn_xpath));   }
	
	/* ----- methods ----- */
	
	/***
	 * <h1>clickCancelBtn</h1>
	 * <p>purpose: Click on the Cancel button to cancel a subscription revoke</p>
	 * @return ManageUsers
	 */
	public ManageUsers clickCancelBtn() {
		this.getCancelBtn().click();
		return new ManageUsers(driver);
	}

	/***
	 * <h1>clickConfirmBtn</h1>
	 * <p>purpose: Click on the Confirm button to confirm a subscription revoke</p>
	 * @return ManageUsers
	 */
	public ManageUsers clickConfirmBtn() {
		this.getConfirmBtn().click();
		return new ManageUsers(driver);
	}

	/***
	 * <h1>clickCloseBtn</h1>
	 * <p>purpose: Click on the Close button to close the confirm revoke subscription popup</p>
	 * @return ManageUsers
	 */
	public ManageUsers clickCloseBtn() {
		this.getCloseBtn().click();
		return new ManageUsers(driver);
	}
}
