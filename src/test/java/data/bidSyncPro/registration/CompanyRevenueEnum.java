package data.bidSyncPro.registration;

/**
 * <h1>enum: CompanyRevenueEnum</h1>
 * @author ssomaraj
 *<p>details: This enum defines the range of values for company revenue the supplier can specify during registration
 */
public enum CompanyRevenueEnum {
	_0_500K, _500K_1M, _1M_10M, _10M_50M, _50M_100M, _100M_500M, _500M_PLUS;


}
