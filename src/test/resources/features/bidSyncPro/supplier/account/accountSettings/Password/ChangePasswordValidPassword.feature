@must @BidSync_AccountChangePassword @administration @level3
Feature: Account > Account Settings - Change Password: Valid Passwords
	ARGO-1112: My Account Tab - Change Password
	BIDSYNC-77: Resetting the password needs to delete the JWT token
	BIDSYNC-238: Password Reset Screen, Incorrect Error

@ARGO-1112
Scenario Outline: Account Settings Change Password With Valid Password
	Given I have registered a random user
	When I try to change my password from the My Account page
	And I enter and confirm a new "<password>" in the Reset Your Password dialog
	Then I will see the appropriate password validation criteria highlighted
	And I will verify that the Reset Your Password dialog does not display any error message
	And I can successfully submit my new password through the Reset Your Password dialog and log in with this new password
	
	Examples: Valid Passwords
	| password     |
	|Password1     |
	|Password@     |
	|password@1    |
	|P@$$W0RD!     |
	|Password@1    |
	
	# Passwords - [8, 128] chars with at least three criteria met:
	# 1. lower, upper, and digits
	# 2. lower, upper, and special chars
	# 3. lower, digit, and special chars
	# 4. upper, digits, and special chars
	# 5. upper, lower, special chars, and digits