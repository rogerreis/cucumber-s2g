package stepDefinitions.bidSyncPro.api;

import org.openqa.selenium.support.events.EventFiringWebDriver;
import core.CoreAutomation;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.helpers.LocalStorage;

public class GetToken_stepDefinitions {

	private EventFiringWebDriver driver;

	public GetToken_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
	}

	public void GetUserToken(String strUser) {

		// Sometimes we're coming in off login.  Make sure page is fully loaded
		new HelperMethods().addSystemWait(3);  
		LocalStorage storage = new LocalStorage(driver);
		String strToken = storage.getItemFromLocalStorage("token");
		System.out.printf(GlobalVariables.getTestID() + " INFO: user token = %s\n", strToken);
		new GlobalVariables();
		GlobalVariables.addGlobalVariable("userToken", strToken);
		
		return;
	}

}
