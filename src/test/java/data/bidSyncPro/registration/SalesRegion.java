package data.bidSyncPro.registration;

public enum SalesRegion {
    	US,
    	US_CANADA,
    	STATES_PROVINCE,
//    	#Fix for release OCT 7
//    	Added None handle when there is no data during initialization
    	None
}
