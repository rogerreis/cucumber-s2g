package pages.bidSyncPro.registration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.bidSyncPro.common.BidSyncProCommon;


/**
 * This file hold the page objects and verification methods on Token Resent process
 * @author ssomaraj
 *
 */
public class TokenResend extends BidSyncProCommon{
	EventFiringWebDriver driver;
	
	public TokenResend(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	private final String title_id 							= "matVerificationTitle";
	private final String incompleteRegistrationMsg_xpath 	= "//strong[contains(text(),'Our records indicate that there is an incomplete r')]";
	private final String resendVerificationEmailBtn_xpath 	= "//span[contains(text(),'Resend Verification Email')]";
	private final String emailSentMsg_xpath 				= "//div[contains(text(),'Email sent!')]";
	private final String closeBtn_xpath 					= "//button//span[contains(text(),'Close')]";
	
	
	private final String incompleteRegistrationMsgOnResetPassword_xpath	= "//h2[contains(text(),'Our records indicate that there is an incomplete registration with your email!')]";
	private final String closeBtnOnResetPassword_xpath 					= "//div[@id='modal3']//div[@class='close-btn']";
	private final String resendVerificationEmailBtnOnResetPassword_xpath= "//button[contains(text(),'RESEND VERIFICATION EMAIL')]";
	
	/*  ----------     Getters -----------------*/
	
	
	public WebElement getTitle() 						{ return findByVisibility(By.id(title_id)) ;    }
	public WebElement getIncompleteRegistrationMsg() 	{ return findByVisibility(By.xpath(incompleteRegistrationMsg_xpath)) ;    }
	public WebElement getIncompleteRegistrationMsgOnResetPassword()	{ return findByVisibility(By.xpath(incompleteRegistrationMsgOnResetPassword_xpath)) ;    }
	public WebElement getResendVerificationEmailBtn() 	{ return findByVisibility(By.xpath(resendVerificationEmailBtn_xpath)) ;    }
	public WebElement getResendVerificationEmailBtnOnResetPassword(){ return findByVisibility(By.xpath(resendVerificationEmailBtnOnResetPassword_xpath)) ;    }
	public WebElement getEmailSentMsg() 				{ return findByVisibility(By.xpath(emailSentMsg_xpath)) ;    }
	public WebElement getCloseBtn() 					{ return findByVisibility(By.xpath(closeBtn_xpath)) ;    }
	public WebElement getCloseBtnOnResetPassword() 		{ return findByVisibility(By.xpath(closeBtnOnResetPassword_xpath)) ;    }
	
	/**
	 * <h1> verifyIncompleteRegistrationPopup() </h1>
	 * <p> Method to verify Incomplete registration message pop up
	 * <p> A user with a status R 
	 */
	public void verifyIncompleteRegistrationPopup() {
		this.getTitle(); 
		this.getIncompleteRegistrationMsg();
		this.getResendVerificationEmailBtn();
	}
	
	public void verifyIncompleteRegistrationPopupOnResetPassword() {
		this.getIncompleteRegistrationMsgOnResetPassword();
		this.getResendVerificationEmailBtnOnResetPassword();
	}
	
	/**
	 * <h1> verifyIncompleteRegistrationPopupAfterSentMail() </h1>
	 * <p> Method to verify Incomplete registration message pop up with sent mail confirmation message
	 * <p> A user with a status R 
	 */
	
	public void verifyIncompleteRegistrationPopupAfterSentMail() {
		this.getTitle();
		this.getIncompleteRegistrationMsg();
		this.getEmailSentMsg();
		this.getCloseBtn();
	}
	
	public void verifyIncompleteRegistrationPopupAfterSentMailOnResetPassword() {
		this.getIncompleteRegistrationMsgOnResetPassword();
//		this.getEmailSentMsg();
		this.getCloseBtnOnResetPassword();
		
	}
	
	/**
	 * <h1>verifyRedirectionToRegistrationPage </h1>
	 * <p> Verify that the user is redirected to the registration process
	 */
	public void verifyRedirectionToRegistrationPage() {
		new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Unable to redirect to Pro registration process")
						.until(ExpectedConditions.urlContains("/setup"));
	}
}


