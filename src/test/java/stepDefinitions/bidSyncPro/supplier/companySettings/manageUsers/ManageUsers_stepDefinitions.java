package stepDefinitions.bidSyncPro.supplier.companySettings.manageUsers;


import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.bidSyncPro.registration.AdditionalStateAddOn;
import data.bidSyncPro.registration.RandomChargebeeUser;
import data.bidSyncPro.registration.SubscriptionAddOns;
import data.bidSyncPro.registration.SubscriptionGrouping;
import data.bidSyncPro.registration.SubscriptionPlan;
import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.bidSyncPro.supplier.companySettings.manageUsers.ActiveRequestRow;
import pages.bidSyncPro.supplier.companySettings.manageUsers.DatabaseUserManagement;
import pages.bidSyncPro.supplier.companySettings.manageUsers.ManageUsers;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.mailinator.Mailinator;

public class ManageUsers_stepDefinitions {

	public EventFiringWebDriver driver;
	
	private ManageUsers manageUsers;
	private HelperMethods helperMethods;
	private String strSender;
	
	public ManageUsers_stepDefinitions(CoreAutomation automation) {
		this.driver     = automation.getDriver();
		manageUsers		= new ManageUsers(driver);
		helperMethods  = new HelperMethods();
		switch(System.getProperty("server")) {
		  case "dev":
		    strSender = "notify-qa";
		    break;
		  case "qa":
			strSender = "notify-qa";
		    break;
		  case "stage":
			strSender = "notify";
		    break;
		}
	}
	
	/* ----- helpers ----- */
	
	/***
	 * <h1>login</h1>
	 * <p>purpose: Navigate to login screen and login
	 * @param email = user email
	 * @param password = user password
	 * @return BidList
	 */
	public BidList login(String email, String password){
		return new SupplierLogin(driver)
				.navigateToHere()
				.loginWithCredentials(email, password);
	}
	
	/***
	 * <h1>loginAndNavigateToUserManagement</h1>
	 * <p>purpose: Log into Pro. Navigate to Manage Users, wait for Users table to load</p>
	 * @param email = user email
	 * @param password = user password
	 * @return ManageUsers
	 */
	public ManageUsers loginAndNavigateToUserManagement(String email, String password) {
		return this.login(email, password)
			.openCompanySettingsDropDown()
			.openManageUsers()
			.waitOnUsersTableToLoad();
	}
	
	/**
	 * <h1>logout</h1>
	 * <p>purpose: Log out of BidSync Pro</p>
	 * @throws InterruptedException 
	 */
	public void logout() throws InterruptedException {
		new CommonTopBar(driver).logout();
	}
	
	/* ----- Bid Access ----- */
	
	/***
	 * <h1>verifyBidAccessAndLogout</h1>
	 * <p>purpose: Assuming user is on a bid list, verify that all visible bids 
	 * 	are displaying correct access permissions.
	 * @param locationsUserBelongsTo = list of all locations my user belongs to
	 * 	Can be created with the getUserBidAccessPrivileges() methods
	 * @throws InterruptedException 
	 */
	public void verifyBidAccessAndLogout(List<String> locationsUserBelongsTo) throws InterruptedException {
		new BidList(driver)
			.allVisibleBids()
			.forEach(bid->bid.verifyBidAccessIsCorrect(locationsUserBelongsTo));
		this.logout();
	}

	/***
	 * <h1>getUserBidAccessPrivileges</h1>
	 * <p>purpose: Based on my user's subscription/license + addOns,<br>
	 * 	return a list of all the locations that the user should be able to <br>
	 * 	access bids from. (Note: All subscriptions can access Basic Bids). </p>
	 * @param subscriptionGrouping = user's subscription plan grouping (or grouping from license)
	 * @param addOns = user's addOns (or addOns in license)
	 * @param stateAddOns = user's Additional State addOns (can only be purchased AFTER user has registered)
	 * @return List of all the locations the user can access bids from based on<br>
	 * 	the subscription grouping+ addOns they are subscribed/licensed to</p>
	 * <p>Note: For a Basic user:<br>
	 * 	subscriptionGrouping = SubscriptionGrouping.NONE<br>
	 * 	addOns = SubscriptionAddOns.NONE</p>
	 */
	private List <String> getUserBidAccessPrivileges(SubscriptionGrouping subscriptionGrouping, List <SubscriptionAddOns> addOns, List <AdditionalStateAddOn> statesAddOns) {
			List <String> locationsUserBelongsTo = new ArrayList<String>();

			locationsUserBelongsTo.addAll(subscriptionGrouping.getLocationsInMyGrouping());
			addOns.forEach(addOn->locationsUserBelongsTo.addAll(addOn.getLocationsInMyAddOns()));
			statesAddOns.forEach(stateAddOn->locationsUserBelongsTo.add(stateAddOn.getLocationInMyAdditionalStateAddOn()));

			System.out.println(GlobalVariables.getTestID() + " In addition to Basic Bids, here are the locations my user can access");
			System.out.println(locationsUserBelongsTo.toString());	
			System.out.println(GlobalVariables.getTestID() + " Note: Federal and Military addOns only apply to the states my user is subscribed to");

			return locationsUserBelongsTo;
	}

	/***
	 * <h1>getUserBidAccessPriviledes</h1>
	 * <p>purpose: Based on my user's subscription/license + addOns,<br>
	 * 	return a list of all the locations that the user should be able to <br>
	 * 	access bids from. (Note: All subscriptions can access Basic Bids). </p>
	 * @param subscriptionGrouping = user's subscription plan grouping (or grouping from license)
	 * @param addOns = user's addOns (or addOns in license)
	 * @return List of all the locations the user can access bids from based on<br>
	 * 	the subscription grouping+ addOns they are subscribed/licensed to</p>
	 * <p>Note: For a Basic user:<br>
	 * 	subscriptionGrouping = SubscriptionGrouping.NONE<br>
	 * 	addOns = SubscriptionAddOns.NONE</p>
	 */
	private List <String> getUserBidAccessPrivileges(SubscriptionGrouping subscriptionGrouping, List <SubscriptionAddOns> addOns) {
		List <AdditionalStateAddOn> stateAddOns = new ArrayList<>();
		return this.getUserBidAccessPrivileges(subscriptionGrouping, addOns, stateAddOns);
	}
	
	/***
	 * <h1>getUserBidAccessPrivileges</h1>
	 * <p>purpose: Based on my user's subscription/license<br>
	 * 	return a list of all the locations that the user should be able to <br>
	 * 	access bids from. (Note: All subscriptions can access Basic Bids). </p>
	 * @param subscriptionGrouping = user's subscription plan grouping (or grouping from license)
	 * @return List of all the locations the user can access bids from based on<br>
	 * 	the subscription grouping they are subscribed/licensed to</p>
	 * <p>Note: For a Basic user:<br>
	 * 	subscriptionGrouping = SubscriptionGrouping.NONE</p>
	 */
	private List <String> getUserBidAccessPrivileges(SubscriptionGrouping subscriptionGrouping){
		List <SubscriptionAddOns>   addOns     = new ArrayList<>();
		List <AdditionalStateAddOn> stateAddOns = new ArrayList<>();
		return this.getUserBidAccessPrivileges(subscriptionGrouping, addOns, stateAddOns);
	}
	
	/* ----- Emails ----- */
	
	/***
	 * <h1>verifyRequestEmail</h1>
	 * <p>purpose: Company Owner should receive an email that a user
	 * 	wants to join the company. Verify this email is received by company owner.
	 * @param email = Company Owner's email
	 */
	private void verifyRequestEmail(String email) {
		
        List<WebElement> emails = new Mailinator(driver)
                .navigateToMailinatorInbox(email)
                .getEmails(strSender, "User requests to join");
        String message = "Company join request notification email not found in user's inbox.";
        Assert.assertTrue(message, emails.size() > 0);	
	}
	
	/***
	 * <h1>verifyAcceptanceEmail</h1>
	 * <p>purpose: When user tries to join a company, company owner can accept the join request.
	 * 	In this case, the user will receive an Acceptance email. Verify user received the email.
	 * @param email = email of accepted user
	 */
	private void verifyAcceptanceEmail(String email) {
        List<WebElement> emails = new Mailinator(driver)
                .navigateToMailinatorInbox(email)
                .getEmails(strSender,"User request to join approved");
        String message = "Acceptance email not found in user's inbox.";
        Assert.assertTrue(message, emails.size() > 0);	
	}
	
	/***
	 * <h1>verifyRejectionEmail</h1>
	 * <p>purpose: When user tries to join a company, the company owner
	 * 	can reject the the join. In this case, the user will receive an
	 * 	email that they were rejected. Verify rejected user receives this email.
	 * @param email = email of rejected user
	 */
	private void verifyRejectionEmail(String email) {
        List<WebElement> emails = new Mailinator(driver)
                .navigateToMailinatorInbox(email)
                .getEmails(strSender, "User request to join has been disapproved.");
        String message = "Rejection email not found in user's inbox.";
        Assert.assertTrue(message, emails.size() > 0);	
	}
	
	
	/***
	 * <h1>verifyRevokeEmail</h1>
	 * <p>purpose: When user license is revoked, the user
	 * being revoked will receive an email. Verify revoked user receives this email.
	 * @param email = email of user being revoked
	 * @param plan = SubscriptionPlan being revoked
	 */
	private void verifyRevokeEmail(String email, SubscriptionPlan plan) {
		new Mailinator(driver)
		.navigateToMailinatorInbox(email)
		.openRevokeSubscriptionLicenseEmail()
		.verifyEmailIsCorrect(plan);	
	}

    

	
	/* ----- test steps ----- */
	
	@Then("^I will verify that the user plan plus add-ons will be displayed in the subscriptions column$")
	public void I_will_verify_that_the_user_plan_plus_add_ons_will_be_displayed_in_the_subscriptions_column() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that user plan plus add-ons that were just purchase are displaying");
		
		RandomChargebeeUser       owner                      = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		
		// Verify owner's subscription plan, grouping, and add-ons are correct 
		manageUsers
				.getUserForName( owner.getFirstName(), owner.getLastName())	
				.verifySubscriptionAndGroupingCorrect(
						owner.getSubscriptionPlan(), 
						owner.getSubscriptionGrouping())
				.verifyAddOnCorrect(owner.getSubscriptionAddOns());
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: After purchasing new subscription plus add-ons, Manage Users section displays subscription plus add-ons");
	}
	
	
	@Then ("^I will verify that the new user has received the additional license with add-ons$")
	public void I_will_verify_that_the_new_user_has_received_the_additional_license_with_add_ons() throws InterruptedException{
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that the new user has received additional license with add-ons");

		RandomChargebeeUser basic   = GlobalVariables.getStoredObject("StoredUser");
		RandomChargebeeUser owner   = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		 
		
		// Verify that the company owner's Manage Users screen shows correct subscription, grouping, and add-ons for new user
		this.loginAndNavigateToUserManagement(owner.getEmailAddress(), owner.getPassword())
			.getUserForName(basic.getFirstName(), basic.getLastName())	
			.verifySubscriptionAndGroupingCorrect(owner.getSubscriptionPlan(), owner.getSubscriptionGrouping())
			.verifyAddOnCorrect(owner.getSubscriptionAddOns());
		this.logout();
		
		// Also verify that the new user's Manage Users screen shows correct subscription, grouping, and add-ons for new user
		this.loginAndNavigateToUserManagement(basic.getEmailAddress(), basic.getPassword())
			.getUserForName(basic.getFirstName(), basic.getLastName())	
			.verifySubscriptionAndGroupingCorrect(owner.getSubscriptionPlan(), owner.getSubscriptionGrouping())
			.verifyAddOnCorrect(owner.getSubscriptionAddOns());

		this.logout();
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Subscription Owner was able to assigned additional license with add-ons to basic user");
	}
	
	@When("^the new user views search results in their All Bids tab$")
	public void the_new_user_views_search_results_in_their_All_Bids_tab() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: New user navigating their All Bids tab");

		RandomChargebeeUser basic = GlobalVariables.getStoredObject("StoredUser");
		
		this.login(basic.getEmailAddress(), basic.getPassword())
			.waitForPageLoad()
			.searchFor("van");
	}

	@When ("^the company owner revokes the additional license from the new user$")
	public void the_company_owner_revokes_the_additional_license_from_the_new_user() throws InterruptedException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Revoking additional license from new user");
		
		RandomChargebeeUser owner  = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		RandomChargebeeUser basic  = GlobalVariables.getStoredObject("StoredUser");

		this.loginAndNavigateToUserManagement(owner.getEmailAddress(), owner.getPassword())
			.getUserForName(basic.getFirstName(), basic.getLastName())
			.revokeLicense();
		helperMethods.addSystemWait(5,TimeUnit.MINUTES);//Delay for BIDS-365 : issue in QA
		this.logout(); 
	}
	
	@When("^the new user is assigned a subscription license by the company owner$")
	public void the_new_user_is_assigned_a_subscription_license_by_the_company_owner() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Assigning license to new user");
			RandomChargebeeUser owner = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
			RandomChargebeeUser basic = GlobalVariables.getStoredObject("StoredUser");

			this.loginAndNavigateToUserManagement(owner.getEmailAddress(), owner.getPassword()).selectMaximumNumberOfUsersPerPage()
					.getUserForName(basic.getFirstName(), basic.getLastName()).assignLicense();
			helperMethods.addSystemWait(5,TimeUnit.MINUTES);//Delay for BIDS-365 : issue in QA
	}
	
	@When ("^I remove the user \"([^\"]*)\" \"([^\"]*)\" as owner \"([^\"]*)\" with a pwd \"([^\"]*)\"$")     		
	public void when_I_remove_the_user_(String arg0, String arg1, String arg2, String arg3) throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Removing vendor user");

		//RandomChargebeeUser owner  = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		//RandomChargebeeUser basic  = GlobalVariables.getStoredObject("StoredUser");	
		RandomChargebeeUser basic = new RandomChargebeeUser();
		RandomChargebeeUser owner = new RandomChargebeeUser();
		basic.setFirstName(arg0);
		basic.setLastName(arg1);
		owner.setEmailAddress(arg2);
		owner.setPassword(arg3);
		
		this.loginAndNavigateToUserManagement(owner.getEmailAddress(), owner.getPassword())		
			.getUserForName(basic.getFirstName(), basic.getLastName())
			.removeUser();			
			Thread.sleep(5000);
			logout();		
	}	

	@When ("^the Company Owner clicks on the Accept button multiple times to accept the new user$")
	public void the_Company_Owner_clicks_on_the_Accept_button_multiple_times_to_accept_the_new_user() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on \"Accept\" button multiple times to accept new user");

		RandomChargebeeUser basic = GlobalVariables.getStoredObject("StoredUser");
		RandomChargebeeUser owner = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");

		// Locate the user request and click Accept three times
		this.loginAndNavigateToUserManagement(owner.getEmailAddress(), owner.getPassword())
				.getRequestForEmail(basic.getEmailAddress())
				.clickAcceptRequest()
				.clickAcceptRequest()
				.clickAcceptRequest()
				.confirmRequestFromPopup()
				.verifyConfirmRequestPopupDoesNotAppear();
	}

	@When ("^the Company Owner clicks on the Accept button$")
	public void the_Company_Owner_clicks_on_the_Accept_button() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on the \"Accept\" button");

		RandomChargebeeUser owner   = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		RandomChargebeeUser basic   = GlobalVariables.getStoredObject("StoredUser");
        
		// Try to grab the request for our user (assuming it's present)
		// If present, click Accept for the request
        ActiveRequestRow requestForEmail = 
        		this.loginAndNavigateToUserManagement(owner.getEmailAddress(), owner.getPassword())
                .getRequestForEmail(basic.getEmailAddress());
        try {
            requestForEmail.clickAcceptRequest();
        } catch (NullPointerException e) {
            Assert.fail("No request present to accept.");
        }
	}

	@Then ("^I will verify that the Accept button disappears$")
	public void I_will_verify_that_the_Accept_button_disappears() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that the \"Accept\" button disappears");

		RandomChargebeeUser basic = GlobalVariables.getStoredObject("StoredUser");

		// Grab the user request row and verify the Accept button is gone
		try {
			manageUsers
				.getRequestForEmail(basic.getEmailAddress())
				.verifyAcceptButtonIsNotVisible();
		} catch (Exception e) {
			// if the element is stale, it might no longer be in the Request table 
			// so verify it's now in the User's table
			manageUsers
				.getUserForName(basic.getFirstName(), basic.getLastName());
		}
	}
	
	@Then ("^the new user will only be added to the company one time$")
	public void the_new_user_will_only_be_added_to_the_company_one_time() throws InterruptedException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that new user is only added one time to the company");
		
		RandomChargebeeUser owner   = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		RandomChargebeeUser basic   = GlobalVariables.getStoredObject("StoredUser");
		
		// 1. Log in and out to update screen to new user addition to company
		this.logout();
		this.loginAndNavigateToUserManagement(owner.getEmailAddress(), owner.getPassword());

		// 2. Verify that the user appears in the user's table, and only appears once
		new DatabaseUserManagement()
			.verifyUserHasJoinedCompanyAsOwnerInDatabase(owner.getEmailAddress(), owner.getCompany().getName())
			.verifyUserHasJoinedCompanyAsUserInDatabase(basic.getEmailAddress(), owner.getEmailAddress(), owner.getCompany().getName());
	}
	

	@Then ("^the new user will only have access to BidSync Basic bids$")
	public void the_new_user_will_only_have_access_to_BidSync_Basic_bids() throws InterruptedException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that user can only view BidSync Basic bids");
		
		// Grab all the locations that my user is a part of - Basic is NONE for grouping and addOn
		List<String> locationsUserBelongsTo = this.getUserBidAccessPrivileges(SubscriptionGrouping.NONE);
		this.verifyBidAccessAndLogout(locationsUserBelongsTo);
	}
	
	@Then("^I will verify that in BidSync Pro the company owner can view bids in their subscription and addOns$")
	public void I_will_verify_that_in_BidSync_Pro_the_company_owner_can_view_bids_in_their_subscription_and_addOns() throws InterruptedException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying company owner can view their bids");
		RandomChargebeeUser owner = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");

		List <String> locationsUserBelongsTo = 
				this.getUserBidAccessPrivileges(owner.getSubscriptionGrouping(), owner.getSubscriptionAddOns(), owner.getAdditionalStateAddOns());

		this.login(owner.getEmailAddress(), owner.getPassword())
			.waitForPageLoad().searchFor("road");
		this.verifyBidAccessAndLogout(locationsUserBelongsTo);
	}

	@Then("^the new user will be able to view bids per their newly assigned license$")
	public void the_new_user_will_be_able_to_view_bids_per_their_newly_assigned_license() throws InterruptedException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that user can only view bids per their newly assigned license");

		SubscriptionGrouping      ownerGrouping = GlobalVariables.getStoredObject("ownerSubscriptionGrouping");
		List <SubscriptionAddOns> ownerAddOns   = GlobalVariables.getStoredObject("ownerSubscriptionAddOns");
		RandomChargebeeUser       basic         = GlobalVariables.getStoredObject("StoredUser");

		// Grab all the locations that my user is a part of
		List<String> locationsUserBelongsTo = this.getUserBidAccessPrivileges(ownerGrouping, ownerAddOns);

		// Login as the new user
		// Run a generic search and verify that the bids returned
		// match the new subscription level
		this.login(basic.getEmailAddress(), basic.getPassword())
			.waitForPageLoad().searchFor("van");
		helperMethods.addSystemWait(10);
		this.verifyBidAccessAndLogout(locationsUserBelongsTo);
	}
	
	@Then ("^the revoked user will only have access to BidSync Basic bids$")
	public void the_revoked_user_will_only_have_access_to_BidSync_Basic_bids() throws InterruptedException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that revoked user only has access to BidSync Basic bids");

		RandomChargebeeUser basic = GlobalVariables.getStoredObject("StoredUser");
		List<String> locationsUserBelongsTo = this.getUserBidAccessPrivileges(SubscriptionGrouping.NONE);

		this.login(basic.getEmailAddress(), basic.getPassword()).searchFor("van");
		this.verifyBidAccessAndLogout(locationsUserBelongsTo);
	}
	
	@Then ("^I will verify that the license was revoked and the new user receives a revoke email$")
	public void I_will_verify_that_the_license_was_revoked_and_the_new_user_receives_a_revoke_email() throws InterruptedException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that additional license was revoked from new user and that the user receives a revoke email");

		RandomChargebeeUser basic   = GlobalVariables.getStoredObject("StoredUser");
		RandomChargebeeUser owner   = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		SubscriptionPlan ownerSubscriptionPlan = GlobalVariables.getStoredObject("ownerSubscriptionPlan");

		// Verify that new user shows only Basic account when viewed from owner's User Management
		this.loginAndNavigateToUserManagement(owner.getEmailAddress(), owner.getPassword())
				.getUserForName(basic.getFirstName(), basic.getLastName())	
				.verifyOnlyBidSyncBasicSubscription();
		this.logout();
		
		// Verify that new user shows only Basic account when viewed from owner's User Management
		this.loginAndNavigateToUserManagement(basic.getEmailAddress(), basic.getPassword())
			.getUserForName(basic.getFirstName(), basic.getLastName())	
			.verifyOnlyBidSyncBasicSubscription();
		this.logout();
		
		// Also verify that the new user receives an email about the license being revoked
		this.verifyRevokeEmail(basic.getEmailAddress(), ownerSubscriptionPlan);

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Subscription Owner was able to revoke license with add-ons from new user");
	}
	
	

	@Then ("^I will verify that the new user cannot manage subscription licenses$")
	public void I_will_verify_that_the_new_user_cannont_manage_subscription_licenses() throws InterruptedException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that just-joined user cannot manange subscription licenses");
		
		RandomChargebeeUser basic  = GlobalVariables.getStoredObject("StoredUser");

		// Login as the new user and verify that the Manage Users screen does not allow 
		// non-subscription owners to manage subscriptions
		this.loginAndNavigateToUserManagement(basic.getEmailAddress(), basic.getPassword())
				.allUsers().forEach(UserRow ->UserRow.verifyCannotManageSubscription(basic.getFirstName(), basic.getLastName()));

		this.logout();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Only subscription owner can manage subscriptions");
	}

	
	@When("^The first user accepts the second user's join request$")
    public void theFirstUserAcceptsTheSecondUserSJoinRequest() {
        RandomChargebeeUser companyOwner = GlobalVariables.getStoredObject("StoredUser");
        RandomChargebeeUser companyJoiner = GlobalVariables.getStoredObject("CompanyJoiner");

        this.loginAndNavigateToUserManagement(companyOwner.getEmailAddress(), companyOwner.getPassword())
                .getRequestForEmail(companyJoiner.getEmailAddress())
                .acceptRequest();
    }
    
    @When("^The first user declines the second user's join request$")
    public void theFirstUserDeclinesTheSecondUserSJoinRequest() {
        RandomChargebeeUser companyOwner = GlobalVariables.getStoredObject("StoredUser");
        RandomChargebeeUser companyJoiner = GlobalVariables.getStoredObject("CompanyJoiner");
        
        this.loginAndNavigateToUserManagement(companyOwner.getEmailAddress(), companyOwner.getPassword())
                .getRequestForEmail(companyJoiner.getEmailAddress())
                .declineRequest();
    }
    
    @Then("^The second user will have joined the company$")
    public void theSecondUserWillHaveJoinedTheCompany() throws InterruptedException {
        RandomChargebeeUser companyOwner = GlobalVariables.getStoredObject("StoredUser");
        RandomChargebeeUser companyJoiner = GlobalVariables.getStoredObject("CompanyJoiner");
        
        this.logout();
        String companyName = 
        		this.login(companyJoiner.getEmailAddress(), companyJoiner.getPassword())
                .openCompanySettingsDropDown()
                .openCompanyProfile()
                .getCompanyName();
        String message = String.format("Requesting user has not joined company '%s'", companyOwner.getCompany().getName());
        Assert.assertEquals(message, companyOwner.getCompany().getName(), companyName);
    }
    
    @And("^The second user will not have joined the company$")
    public void theSecondUserWillNotHaveJoinedTheCompany() throws InterruptedException {
        RandomChargebeeUser companyOwner = GlobalVariables.getStoredObject("StoredUser");
        RandomChargebeeUser companyJoiner = GlobalVariables.getStoredObject("CompanyJoiner");

        this.logout();
        boolean isCompanyProfileAvailable = 
        		this.login(companyJoiner.getEmailAddress(), companyJoiner.getPassword())
                .openCompanySettingsDropDown()
                .openCompanyProfile()
                .isTheUserAllowedToViewThisPage();
        String message = String.format("Requesting user has not joined company '%s'", companyOwner.getCompany().getName());
        Assert.assertFalse(message, isCompanyProfileAvailable);
    }
    
    @And("^The second user will get an email about his acceptance$")
    public void theSecondUserWillGetAnEmailAboutHisAcceptance() {
        RandomChargebeeUser companyJoiner = GlobalVariables.getStoredObject("CompanyJoiner");
        this.verifyAcceptanceEmail(companyJoiner.getEmailAddress());
    }
    
    @And("^The second user will get an email about his rejection$")
    public void theSecondUserWillGetAnEmailAboutHisRejection() {
        RandomChargebeeUser companyJoiner = GlobalVariables.getStoredObject("CompanyJoiner");
        this.verifyRejectionEmail(companyJoiner.getEmailAddress());
    }
    
    @Then("^The second user will be removed from the first user's active requests list$")
    public void theSecondUserWillBeRemovedFromTheFirstUserSActiveRequestsList() {
        RandomChargebeeUser companyJoiner = GlobalVariables.getStoredObject("CompanyJoiner");
	    String message = "Rejected user still found in company manager's active request list.";
        Assert.assertNull(message, manageUsers.getRequestForEmail(companyJoiner.getEmailAddress()));
    }
    
    @And("^The second user will be added to the first user's user list$")
    public void theSecondUserWillBeAddedToTheFirstUserSUserList() throws InterruptedException {
        RandomChargebeeUser companyOwner = GlobalVariables.getStoredObject("StoredUser");
        RandomChargebeeUser companyJoiner = GlobalVariables.getStoredObject("CompanyJoiner");

        // Logout and then back in to see changes
        this.logout();
        this.loginAndNavigateToUserManagement(companyOwner.getEmailAddress(), companyOwner.getPassword());

        String message = "Added user not found in company manager's user list.";
        Assert.assertNotNull(message, manageUsers.getUserForName(companyJoiner.getFirstName(), companyJoiner.getLastName()));
    }
    
    @Then("^The first user should receive an email about the join request$")
    public void theFirstUserShouldReceiveAnEmailAboutTheJoinRequest() {
        RandomChargebeeUser companyOwner = GlobalVariables.getStoredObject("StoredUser");
        this.verifyRequestEmail(companyOwner.getEmailAddress());
    }  
    
    
	@Then("verify that paginator is displayed as expected$")
	public void verifyThatPaginatorIsDisplayedAsExpected() {
		try {
			manageUsers.verifyPaginator();
			manageUsers.verifyPaginatorOptions();
		} catch (Exception exp) {
			fail("TEST FAILED : iVerifyThatPaginatorIsDisplayedAsExpected() " + exp);
		}

	}

	@And("verify that the number of records matches the selected page size$")
	public void verifyThatTheNumberOdRecordsMatchesTheSelectedPageSize() {
		try {
			manageUsers.verifyNumberOfUsersPerPage();
			manageUsers.verifyNextPageIfExists();
		} catch (Exception exp) {
			fail("TEST FAILED : verifyThatTheNumberOdRecordsMatchesTheSelectedPageSize() " + exp);
		}

	}

}