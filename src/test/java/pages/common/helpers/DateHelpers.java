package pages.common.helpers;

import static org.junit.Assert.fail;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import org.junit.Assert;

/**
 * <h1>Class DateHelpers</h1>
 * @author dmidura
 * <p>details: This class houses methods for manipulating dates and date input/output</p>
 */
public class DateHelpers {
	
	public DateHelpers() {
		
	}

	/***
	 * <h1>getCurrentTimeAsCalendar</h1>
	 * <p>purpose: Return current time (calculated per GMT time zone) in Calendar form.<br>
	 * @return today's date in Calendar format
	 */
	public Calendar getCurrentTimeAsCalendar() {
		TimeZone gmt   = TimeZone.getTimeZone("GMT0");
		Calendar today = Calendar.getInstance();
		today.setTimeZone(gmt);
        return today;
	}

	/***
	 * <h1>addNumberOfMonthsToCalendarDate</h1>
	 * @param date = a Calendar date that you want to add one month to<br>
	 * 	Note: You can use this method with getCurrentTimeFromCalendar() <br>
	 * 	to add x months from the current time</p>
	 * @param numMonths = number of months to add/subtract (negative for subtraction)
	 * ex: 10 will add 10 months
	 * ex: -1 will subtract 1 month
	 * @return a new Calendar date x months from date
	 */
	public Calendar addNumberOfMonthsToCalendarDate(Calendar date, int numMonths) {
		Calendar endDate = Calendar.getInstance();
		endDate.setTime(date.getTime());
		endDate.add(Calendar.MONTH, numMonths);
		return  endDate;
	}

	/***
	 * <h1>addNumberOfDaysToCalendarDate</h1>
	 * @param date = a Calendar date that you want to add one day to<br>
	 * 	Note: You can use this method with getCurrentTimeFromCalendar() <br>
	 * 	to add one day from today</p>
	 * @param numDays = number of days to add/subtract (ex: 1 | -10)
	 * @return a Calendar date one day from date
	 */
	public Calendar addNumberOfDaysToCalendarDate(Calendar date, int days) {
		Calendar endDate = Calendar.getInstance();
		endDate.setTime(date.getTime());
		endDate.add(Calendar.HOUR, +24 * days);
		return  endDate;
	}
	 /***
	  * <h1>flipDateMonthFirst</h1>
	  * <p>Given a date of form MM/DD/YYYY, return as YYYY/MM/DD<br>
	  *  Given a date of form MM-DD-YYYY, return a YYYY-MM-DD</p>
	  * @param strDate = date of form MM/DD/YYYY or MM-DD-YYYY
	  * @return String = YYYY/MM/DD or YYYY-MM-DD corresponding to input
	  */
	 public String flipDateMonthFirst(String strDate) {
		 if (strDate.length() != 10) { fail("ERROR: String \""+ strDate +"\" is not of appropriate size for flipDate() method"); }
		 String strReturn =  strDate.substring(6,10) + strDate.charAt(5) + strDate.substring(0,5);
		 System.out.printf(GlobalVariables.getTestID() + " INFO: Flipping date with month first for \"%s\" to \"%s\"\n", strDate, strReturn);
		 return strReturn;
	 }
	 
	 /***
	  * <h1>flipDateYearFirst</h1>
	  * <p>Given a date of form YYYY/MM/DD, return as MM/DD/YYYY<br>
	  *  Given a date of form YYYY-MM-DD, return a MM-DD-YYYY</p>
	  * @param strDate = date of form YYYY/MM/DD or YYYY-MM-DD
	  * @return String = MM/DD/YYYY or MM-DD-YYYY corresponding to input
	  */
	 public String flipDateYearFirst(String strDate) {
		 if (strDate.length() != 10) { fail("ERROR: String is not of appropriate size for flipDate() method"); }
		 String strReturn =  strDate.substring(5,10) + strDate.charAt(4) + strDate.substring(0,4);
		 System.out.printf(GlobalVariables.getTestID() + " INFO: Flipping date with year first for \"%s\" to \"%s\"\n", strDate, strReturn);
		 return strReturn;
	 }
	 
	/***
	 * <h1>formatDateWithWrongLength</h1>
	 * <p>purpose: A date is expected to be 10 chars long (MM/DD/YYYY or MM-DD-YYYY).<br>
	 * 	However, sometimes UI will return a date of form M/DD/YYYY, M/D/YYYY, or MM/D/YYYY.<br>
	 *	For these cases, reformat the date to be 10 chars. If a date of form MM/DD/YYYY is passed <br>
	 *	to this method, then the date will be returned unchanged</p>
	 *	Ex1: strDate = 5/13/2017 will be returned as 05/13/2017<br>
	 *	Ex2: strDate = 12/5/2017 will be returned as 12/05/2017<br>
	 *	Ex3: strDate = 5/5/2017 will be returned as 05/05/2017<br>
	 *	Ex4: strDate = 12/12/2017 will be unchanged and returned as 12/12/2017</p>
	 * @param strDate = Date of format M/DD/YYYY, MM/D/YYYY, MM/DD/YYYY
	 * @param charDelimiter = the Date delimiter (ex: "/" or  "-")
	 * @return String formatted to be MM/DD/YYYY
	 */
	public String formatDateWithWrongLength(String strDate, char charDelimiter) {
		String strReturn = strDate;
		
		// Currently expecting strings to be 10 chars (MM/DD/YYYY)
		if (strDate.length() != 10 ) {
			//Form M/D/YYYY    ex: 5/5/2018
			if (strDate.charAt(1) == charDelimiter & strDate.charAt(3) == charDelimiter) {
				strReturn = "0" + strDate.substring(0,2) + "0" + strDate.substring(2, strDate.length()); }
			// Form M/DD/YYYY  ex: 5/12/2018
			else if (strDate.charAt(1) == charDelimiter) {
				strReturn = "0" + strDate; }
			// Form MM/D/YYYY  ex: 12/5/2018
			else if (strDate.charAt(2) == charDelimiter) { 
				strReturn = strDate.substring(0, 3) + "0" + strDate.substring(3,strDate.length()); } 
			else {
				fail("ERROR: formatDate cannot condition date"); }
		} 

		return strReturn;
	}

   /***
    * <h1>getStringDateAsCalendar</h1>
    * <p>purpose: Given a string for a date, return as a Calendar object.</p>
    * @param strDate in MM/dd/yyyy form | MM/dd/yyyy HH:mm:ss form
    * @return Calendar
    */
   public Calendar getStringDateAsCalendar(String strDate) {
       Calendar calendar    = Calendar.getInstance();
       SimpleDateFormat sdf = null;
       if (strDate.contains(":")) {
    	   sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.getDefault());
       } else {
    	   sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault()); }

       try {
           calendar.setTime(sdf.parse(strDate));
       } catch (ParseException e) {
           Assert.fail(String.format("ERROR: Invalid due date '%s'.\n", strDate)); }
       return calendar;
   }

   /***
    * <h1>getCalendarDateAsString</h1>
    * <p>purpose: Given a Calendar, return as string in form (MM/dd/yyyy)</p>
    * @param date
    * @return String date in MM/dd/yyyy form
    */
   public String getCalendarDateAsString(Calendar date) {
       SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
       sdf.setTimeZone(date.getTimeZone());
       return sdf.format(date.getTime());
   }	
}
