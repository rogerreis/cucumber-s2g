package runners.externalScripts;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin ={"pretty" , "json:target/cucumber/cucumber.json", "html:target/cucumber"},
        features = "src/test/resources/features/externalScripts",
        glue = {"stepDefinitions.externalScripts",
        		"stepDefinitions.hooks",
        		"stepDefinitions.chargebeeSite"},
        dryRun = false,
        tags = {"@skip"}, //dmidura: This should be tagged as @skip 
        				  // so we only run these scripts manually
        snippets=SnippetType.CAMELCASE
)

public class ChargebeeTestSiteExternalScriptsRunnerTest {
}