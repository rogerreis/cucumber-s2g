package pages.bidSyncPro.supplier.companySettings.manageUsers;

import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.bidSyncPro.supplier.companySettings.CompanySettingsCommon;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Class ManageUsers</h1>
 * @author dmidura
 * <p>details: This class contains methods and page Objects to manipulate the Manage Users screen
 * 	located at Company Settings > Manage Users.
 */
public class ManageUsers extends CompanySettingsCommon {
    
    // xpaths
    private final String listRowActiveRequests_xpath 		= "//mat-card-content[@id='matUserProfileContent']//mat-list-item";
    private final String listRowUsersList_xpath      		= "//mat-card-content[@id='matCompanyProfileContent']//mat-list-item";
    
    private final String paginatorItemPerPageLabel_xpath    = "//div[@class='mat-paginator-page-size-label'][contains(text(),'per page:')]";
    private final String paginatorItemPerPage_xpath    		= "//span[contains(text(),'10')]";
    private final String ItemPerPage_List_xpath	    		= "//div[contains(@class,'mat-select-panel')]/mat-option";
    private final String paginatorSelectArrow_xpath    		= "//div[@class='mat-select-arrow']";
    private final String paginatorRangeLabel_xpath    		= "//div[@class='mat-paginator-range-label']";
    private final String paginatorPreviousPageBtn_xpath    	= "//button[contains(@class,'mat-paginator-navigation-previous')]";
    private final String paginatorNextPageButton_xpath    	= "//button[contains(@class,'mat-paginator-navigation-next')]";
    private final String numberOfUsersPerPage_xpath			= "//mat-option[@class='mat-option ng-star-inserted mat-selected mat-active']/span";
    
    
    // ids
    private final String activeRequestsList_id       		= "matUserProfileContent";
    private final String usersList_id                		= "matCompanyProfileContent";
    
    private int expectedUserPerPage = 0;

    
    public ManageUsers(EventFiringWebDriver driver) {
        super(driver);
    }
    
    /* ----- getters ----- */
    
    private WebElement getActiveRequestsList() 				{ return findByVisibility(By.id(activeRequestsList_id)); }
    private WebElement getUsersList()          				{ return findByVisibility(By.id(this.usersList_id));     }
    public WebElement getPaginatorItemPerPageLabel() 		{ return findByVisibility(By.xpath(this.paginatorItemPerPageLabel_xpath)); }
    public WebElement getPaginatorItemPerPage() 			{ return findByVisibility(By.xpath(this.paginatorItemPerPage_xpath));}
    public WebElement getPaginatorSelectArrow() 			{ return findByVisibility(By.xpath(this.paginatorSelectArrow_xpath));}
    public WebElement getPaginatorRangeLabel() 				{ return findByVisibility(By.xpath(this.paginatorRangeLabel_xpath));}
    public WebElement getPaginatorPreviousPageBtn() 		{ return findByVisibility(By.xpath(this.paginatorPreviousPageBtn_xpath));}
    public WebElement getPaginatorNextPageButton() 			{ return findByVisibility(By.xpath(this.paginatorNextPageButton_xpath));}
    
   
    
    /* ----- methods ----- */

    /***
     * <h1>allActiveRequests</h1>
     * <p>purpose: Return a list of all the active user requests in the "Active Request" table
     * @return List <ActiveRequestRow> of all user requests
     */
    public List<ActiveRequestRow> allActiveRequests() {
        List<WebElement> userElements;
        try {
            userElements = getActiveRequestsList().findElements(By.xpath(this.listRowActiveRequests_xpath));
        } catch (TimeoutException e) {
            return new ArrayList<>();
        }
        List<ActiveRequestRow> requestRows = new ArrayList<>();
        for(int i=1; i <= userElements.size(); ++i) {
            String thisRowsXpath = String.format("%s[%d]", this.listRowActiveRequests_xpath, i);
            requestRows.add(new ActiveRequestRow(driver, thisRowsXpath));
        }
        return requestRows;
    }
    
    /***
     * <h1>allUsers</h1>
     * <p>purpose: Return a list of all the users in the "Users" table
     * @return List <UserRow> of all users in company
     */
    public List<UserRow> allUsers() {
        List<WebElement> userElements;
        try {
            userElements = getUsersList().findElements(By.xpath(this.listRowUsersList_xpath));
        } catch (NoSuchElementException e) {
            return new ArrayList<>();
        }
        List<UserRow> userRows = new ArrayList<>();
        for(int i=1; i < userElements.size() +1; ++i) {
            String thisRowsXpath = String.format("%s[%d]", this.listRowUsersList_xpath, i);
            userRows.add(new UserRow(driver, thisRowsXpath));
        }
        return userRows;
    }
    
    /***
     * <h1>getRequestForEmail</h1>
     * <p>purpose: Given an emailAddress, return the request row in the <br>
     * 	user active request table corresponding to emailAddress</p>
     * @param emailAddress = email address of user in active request table
     * @return ActiveRequestRow corresponding to emailAddress | null if email isn't in request table
     */
    public ActiveRequestRow getRequestForEmail(String emailAddress) {
        List<ActiveRequestRow> allReqs = allActiveRequests();
        try {
            return CollectionUtils.extractSingleton(CollectionUtils.select(allReqs, req -> req.getEmail().equals(emailAddress)));
        } catch(NullPointerException | IllegalArgumentException e) {
            return null;
        }
    }
    
    /***
     * <h1>getUserForName</h1>
     * <p>purpose: Return access to the user in the User's table<br>
     * 	Fail test if user is not visible in the table OR if the user<br>
     * 	exists multiple times within the user table</p> 
     * @param firstName = user's first name per registration
     * @param lastName = user's last name per registration
     * @return UserRow
     */
    public UserRow getUserForName(String firstName, String lastName) {
        List<UserRow> allUsers = allUsers();
        try {
            return CollectionUtils.extractSingleton(CollectionUtils.select(allUsers, user ->
                user.getFirstName().equals(firstName) && user.getLastName().equals(lastName)
            ));
        } catch(NullPointerException | IllegalArgumentException e) {
            return null;
        }
    }
    
    /***
     * <h1>waitOnUsersTableToLoad</h1>
     * <p>purpose: For some tests, it can a take a really long<br>
     * 	time for the Users table to correctly load. Insert this<br>
     * 	method before a step to wait up to 10 seconds for the table to load</p>
     * @return ManageUsers
     */
    public ManageUsers waitOnUsersTableToLoad() {
    	// dmidura: timeout here is ridiculous. Waiting on fix for this bug.
    	waitForVisibility(By.xpath(this.listRowUsersList_xpath + "[1][contains(@class, 'company-user-contianer')]"), 30);
    	helperMethods.addSystemWait(3);
    	return this;
    }
    
    /***
     * <h1>acceptAllRequests</h1>
     * <p>purpose: For each user in the "Request" table, click "Accept
     * @return ManageUsers
     */
    public ManageUsers acceptAllRequests() {
        while(!driver.findElements(By.id(this.activeRequestsList_id)).isEmpty()) {
            List<ActiveRequestRow> requests = this.allActiveRequests();
            requests.get(0).acceptRequest();
            helperMethods.addSystemWait(1);
        }
        return this;
    }
    
    /***
     * <h1>getUsersPerPage</h1>
     * <p>purpose: Get the current nuber of users per page
     * @return numberOfUserPerPage
     */
    public int getUsersPerPage() {
    	WebElement element = findByVisibility(By.xpath(numberOfUsersPerPage_xpath));
    	return Integer.parseInt(element.getText());
    }
    

    /* ----- verifications ----- */

    public boolean isTheUserAllowedToViewThisPage() {
        return new WaitingForApproval(driver).areWeHere();
    }
    
    public void verifyPaginator() {
    	scrollIntoViewBottomOfScreen(By.xpath(paginatorItemPerPageLabel_xpath));
    	this.getPaginatorItemPerPageLabel();
    	this.getPaginatorItemPerPage();
    	this.getPaginatorSelectArrow();
    	this.getPaginatorRangeLabel();
    	this.getPaginatorPreviousPageBtn();
    	this.getPaginatorNextPageButton();
    	this.verifyLeftNavOnPageload();
    }
    
    public void verifyPaginatorOptions() {
    	this.getPaginatorSelectArrow().click();
    	new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Error : Mismatch in page range options, Expected 4")
    	.until(ExpectedConditions.numberOfElementsToBe(By.xpath(ItemPerPage_List_xpath), 4));
    	expectedUserPerPage = getUsersPerPage();
    	findByVisibility(By.xpath(numberOfUsersPerPage_xpath)).click();
    }
    
    public void verifyNumberOfUsersPerPage() {
    	new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Error : Mismatch in number user paer page")
    	.until(ExpectedConditions.or(ExpectedConditions.numberOfElementsToBeLessThan(By.xpath("//mat-card[@id='manage-users-card']//mat-list-item"), expectedUserPerPage),
    			ExpectedConditions.numberOfElementsToBe(By.xpath("//mat-card[@id='manage-users-card']//mat-list-item"), expectedUserPerPage)));
    }
    
    public ManageUsers selectMaximumNumberOfUsersPerPage() {
    	this.getPaginatorSelectArrow().click();
    	new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Error : Mismatch in page range options, Expected 4")
    	.until(ExpectedConditions.numberOfElementsToBe(By.xpath(ItemPerPage_List_xpath), 4));
    	List<WebElement> paginatorItemPerPageList = this.findAllOrNoElements(driver, By.xpath(ItemPerPage_List_xpath));
    	paginatorItemPerPageList.get(paginatorItemPerPageList.size()-1).click();
    	return this;
    }
    
    public void verifyLeftNavOnPageload() {
    	new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Error : Left navigation button is enabled on pageload")
    	.until(ExpectedConditions.not(ExpectedConditions.elementToBeClickable(By.xpath(paginatorPreviousPageBtn_xpath))));
    }
    
    public void verifyNextPageIfExists() {
    	if(getPaginatorNextPageButton().isEnabled()) {
			getPaginatorNextPageButton().click();
			verifyNumberOfUsersPerPage();
		}
    }
}
