package stepDefinitions.bidSyncPro.dailyBidNotifications;

import static org.junit.Assert.fail;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import pages.bidSyncPro.dailyBidNotifications.dailyBidNotification;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.common.helpers.GlobalVariables;
import pages.mailinator.Mailinator;

public class DailyBidNotifications_stepDefinitions {
	
	private EventFiringWebDriver driver;
	
	public DailyBidNotifications_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
	}
	
	@Given ("^I have retrieved the newest bid notification email for user \"(.*)\"$")
	public void I_have_retrieved_the_newest_bid_notification_email_for_user(String userReference) {
		try {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Navigating to mailinator inbox for \"" + userReference + "\" and retrieving newest bid notification");

		new SupplierLogin(driver).setThisUserFromAccountReference(userReference);
		new Mailinator(driver)
			// Fix for release OCT 7
		    //.navigateToMailinatorInbox(SupplierLogin.thisUser.getEmail())
			.navigateToMailinatorInbox("")
			.openDailyBidNotificationEmail();
		}catch(Exception exp) {
			fail("TEST FAILED : Error while retrieving newest bid notification email "+exp);
			exp.printStackTrace();
		}
		
	}
	
	@Then ("^I will verify that each non-subscribed bid is correctly obfuscated in the bid notification email$")
	public void I_will_verify_that_each_non_subscribed_bid_is_correctly_obfuscated_in_the_bid_notification_email() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying all bids correctly display in the obfuscated bids section of bid notification email");
		new dailyBidNotification(driver)
			.allUnsubscribedBids().stream()
			.forEach(r->r.printRow()
					.verifyGenericNotification()
					.verifyNotSubscribed());
		System.out.println(GlobalVariables.getTestID() + " Test Passed: All bids correctly display in the non-obfuscated bids section of bid notification email");
	}
	

	@Then ("^I will verify that each subscribed bid is correctly displayed in the bid notification email$")
	public void I_will_verify_that_each_subscribed_bid_is_correctly_displayed_in_the_bid_notification_email(){
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying all bids correctly display in the non-obfuscated bids section of bid notification email");
		new dailyBidNotification(driver)
			.allSubscribedBids().stream()
			.forEach(r->r.printRow()
					.verifyGenericNotification()
					.verifySubscribed());
		System.out.println(GlobalVariables.getTestID() + " Test Passed: All bids corectly display in the non-obfuscated bids section of bid notification email");
	}
	
	@Then("^I will verify that all bid notification email images are displayed$")
	public void I_will_verify_that_all_bid_notification_email_images_are_displayed() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying all bid images are displayed");
		
		new dailyBidNotification(driver)
			.verifyAllNotificationImages();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: All bid images are displayed");
	}

}
