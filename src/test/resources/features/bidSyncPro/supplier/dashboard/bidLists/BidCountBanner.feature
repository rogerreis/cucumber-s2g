@must @bidSearch_detail @level3
Feature: Dashboard - Bid Count Banner
	BIDSYNC-23: Bid Count Banner
	BIDSYNC-624: Update Bid Count Banner
	

@BIDSYNC-23 @BIDSYNC-624
Scenario Outline: Bid Count Banner appears and correctly initalizes when searching All Bids per:
#   1. Close button visible
#	2. Text in one of these forms: 
#     a. "Currently only able to view [num1] of [num2] bids / UPGRADE PLAN TODAY to access all [num2]"
#        where:
#        i.   num1 < num2 
#        ii.  num2 displays consistently in message (since it displays twice)
#        iii. UPGRADE PLAN TODAY link navigates to manage subscriptions
#     b. "Currently able to view [num1] of [num2] bids"
#        where:
#        i. num1 = num2
	Given I have logged into the Supplier side as user "<user>"
	When I run a search on the Top Bar with keyword "road"
	Then I will verify that the Bid Count Banner is visible
	And I will verify that the Bid Count Banner is correctly displaying information
	
	Examples: Accounts to trigger each message
	| user             |
	| National FMC     |
	| Basic            |


@BIDSYNC-23
Scenario Outline: Bid Count Banner does not appear on "New For You" or "Your Saved Bids" lists
	Given I have logged into the Supplier side as user "National FMC"
	When I have navigated to the "<bidTab>" bid tab
	Then I will verify that the Bid Count Banner is not visible
	
	Examples: Bid Tabs that should not display Bid Count Banner
	| bidTab          |
	| New For You     |
	| Your Saved Bids |
	

@BIDSYNC-23
Scenario: Closing the Bid Count Banner will close until user logs out and back in
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to the "All Bids" bid tab
	When I close the Bid Count Banner
	Then I will verify that the Bid Count Banner is not visible
	When I run a search on the Top Bar with keyword "airplane"
	Then I will verify that the Bid Count Banner is not visible
	When I log out and then back into BidSync Pro
	And I have navigated to the "All Bids" bid tab
	Then I will verify that the Bid Count Banner is visible
	

@BIDSYNC-23
Scenario Outline: Bid Count Banner will display realistic results
	Note: Generic Search is for "water" in Oregon and California
	Given I have logged into the Supplier side as user "<user1>"
	And I retrieve the Bid Count Banner value for a generic search with "<user1>" bid access
	And I logout of BidSync Pro
	When I have logged into the Supplier side as user "<user2>"
	And I retrieve the Bid Count Banner value for a generic search with "<user2>" bid access
	Then I will verify that "<user1>" can access more bids than "<user2>"
	And I will verify that the total number of bids listed on the Bid Count Banner is the same for "<user1>" and "<user2>"
	
	Examples: Canned Accounts
		| user1           | user2            |
    | National FMC    | Regional - West  |
    | Regional - West | State - CA       |
    | State - CA      | Basic            |