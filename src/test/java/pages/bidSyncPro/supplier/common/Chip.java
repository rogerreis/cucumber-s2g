package pages.bidSyncPro.supplier.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.pageObjects.BasePageActions;

public class Chip extends BasePageActions {
	
	
	// Elements of the chip
	public String text="";
	
	// Other
	private String xpath="";
	
	public Chip (EventFiringWebDriver driver, String chipXPath){
		super(driver);
		this.xpath    = chipXPath;
		this.text     = this.setChipText();
	}
	
	/* ----- getters ----- */
	public WebElement getChip ()          { return findByVisibility(By.xpath(this.xpath));} 
	public WebElement getDeleteChipBtn () { 
		String strXPath = this.xpath + "//mat-icon[contains(text(), 'cancel')]";
		return findByVisibility(By.xpath(strXPath)); }
	
	public WebElement getDeleteChipBtnByKeyword () { 
		String strXPath = this.xpath.substring(0,this.xpath.length()-3)+"[contains(text(),'"+getChipText()+"')]" + "//mat-icon[contains(text(), 'cancel')]";
		return findByVisibility(By.xpath(strXPath)); }
	
	/* ------ methods ----- */
	
	/**
	 * <h1>getChipText</h1>
	 * <p>purpose: Grab the chip's text</p>
	 * @return String = chip text, as displayed to user
	 */
	private String setChipText() { 
		// because of how the xml is constructed, we might also get "cancel" returned
		// to indicate the X delete button. Remove that.
		return getChip().getText().replaceFirst("cancel", "").trim();
	}

	public String getChipText() {
		return this.text;
	}
	
	/***
	 * <h1>deleteChip</h1>
	 * <p>purpose: Click on the "X" on the chip to delete it</p>
	 */
	public void deleteChip () { this.getDeleteChipBtn().click(); }
	public void deleteChipByKeyword () { this.getDeleteChipBtnByKeyword().click(); }
}
