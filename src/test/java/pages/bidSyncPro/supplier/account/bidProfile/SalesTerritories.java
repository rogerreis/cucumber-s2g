package pages.bidSyncPro.supplier.account.bidProfile;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import data.bidSyncPro.registration.SalesRegion;
import pages.bidSyncPro.registration.SelectRegions;
import pages.bidSyncPro.supplier.account.AccountCommon;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.StatesProvincesSection;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

/**
 * This class houses the page objects, getters and verification methods for Sales Territories on Account >> bid profile
 * @author ssomaraj
 *
 */
public class SalesTerritories extends AccountCommon {
	
	private final String clearAllButton_id 			 	    	= "select_all_states";
    private final String selectAllButton_id 					= "clear_all_states";
    private final String searchState_id							= "state";
    
	 // xpaths
    private final String salesTerritoriesPageHead_xpath         = "//span[contains(text(),'Sales Territories')]";
//    private final String editSalesTerritoriesIcon_xpath         = "//div[@id='divMapTitle']//mat-icon[@class='mat-icon material-icons'][contains(text(),'mode_edit')]";
    private final String editSalesTerritoriesIcon_xpath         = "//div[@id='divMapTitle']//mat-icon[@class='mat-icon notranslate material-icons mat-icon-no-color'][contains(text(),'mode_edit')]";
    private final String regionPageHead_xpath					="//mat-card-title[@id='matRegionsTitle']";
    
    private final String unitedStatesTab_xpath 					= "//div[contains(text(),'UNITED STATES')]";
    private final String selectedUSTab_xpath					= "//div[@class='mat-tab-label mat-ripple mat-tab-label-active ng-star-inserted']";
    private final String canadaTab_xpath 						= "//div[contains(text(),'CANADA')]";
    private final String listViewTab_xpath 						= "//div[contains(text(),'LIST VIEW')]";
    
    private final String usImage_xpath             				= "//div[@class='country-image us-only']";
    private final String usCanadaImage_xpath     				= "//div[@class='country-image both-countries']";
    private final String statesImage_xpath     					= "//div[@id='map_base1']";
    private final String selectedStateList_xpath 				= "//*[@class='selected ng-star-inserted']";
    
    private final String selectCountryButton_xpath 				= "//span[contains(text(),'SELECT COUNTRY')]";
    private final String saveButton_xpath 						= "//span[contains(text(),'SAVE')]";
    private final String cancelButton_xpath						= " //span[contains(text(),'CANCEL')]";
    private final String selectStatesProvincesButton_xpath		="//span[@class='ng-star-inserted'][contains(text(),'SELECT STATES/PROVINCES')]";
    
    private final String usAndCanada_xpath 						= "//div[@class='both-image']";
	private final String usRegion_xpath							= "//div[@class='us-image']";
	private final String statesOrProvinces_xpath				= "//div[@class='map-image']";
	
	private final String listViewStates_xpath					= "//div[@class='searchState chipSearch ng-star-inserted']//mat-chip-list//div[@class='mat-chip-list-wrapper']//mat-chip";
	private final String listViewStatesOnBidProfile_xpath		= "//div[@id='stateListContainer']/following-sibling::div//mat-chip-list//div[@class='mat-chip-list-wrapper']//mat-chip";
	private final String stateOptionList_xpath					= "//div[@class='mat-autocomplete-panel mat-autocomplete-visible ng-star-inserted']//mat-option";
	private final String listViewStatesChipCloseButton_xpath	= "//mat-chip[@color='accent']/mat-icon";
	
	
	private final String allUsStatesCheckBox_xpath 				 = "//mat-slide-toggle[@id='searchUSStates']";

    private SalesRegion selectedRegion;

    
    public SalesTerritories(EventFiringWebDriver driver) {
        super(driver);
        selectedRegion = (SalesRegion)GlobalVariables.getStoredObject("salesRegion");
    }
    
    /* ----- getters ----- */
    
    public WebElement getSalesTerritoriesPageHead() 		{ return findByVisibility(By.xpath(this.salesTerritoriesPageHead_xpath));   }
    public WebElement getRegionPageHead() 					{ return findByVisibility(By.xpath(this.regionPageHead_xpath));  			}
    public WebElement getEditSalesTerritoriesIcon()  		{ return findByVisibility(By.xpath(this.editSalesTerritoriesIcon_xpath)); 	}
	public WebElement getUnitedStatesTab()					{ return findByVisibility(By.xpath(unitedStatesTab_xpath));					}
	public WebElement getCanadaTab()						{ return findByVisibility(By.xpath(canadaTab_xpath));						}
	public WebElement getListViewTab()						{ return findByVisibility(By.xpath(listViewTab_xpath));						}
	public WebElement getSelectedUsTab()					{ return findByVisibility(By.xpath(selectedUSTab_xpath));					}

	public WebElement getSelectAllButton()					{ return findByVisibility(By.id(selectAllButton_id));						}
	public WebElement getClearAllButton()					{ return findByVisibility(By.id(clearAllButton_id));						}
	public WebElement getSelectCountryButton()				{ return findByVisibility(By.xpath(selectCountryButton_xpath));				}
	public WebElement getSaveButton()						{ return findByVisibility(By.xpath(saveButton_xpath));						}
	public WebElement getCancelButton()						{ return findByVisibility(By.xpath(cancelButton_xpath));					}
	public WebElement getSelectStatesOrProvinceButton()		{ return findByVisibility(By.xpath(selectStatesProvincesButton_xpath));		}
    
	public WebElement getUsRegion() 						{ return findByVisibility(By.xpath(usRegion_xpath));						}
	public WebElement getUsAndCanada() 						{ return findByVisibility(By.xpath(usAndCanada_xpath));						}
	public WebElement getStatesOrProvinces()				{ return findByVisibility(By.xpath(statesOrProvinces_xpath));				}
	
	public WebElement getSearchState()  					{	return findByVisibility(By.id(searchState_id));							}

	public List<WebElement> getListViewStatesChipCloseButtons() { return findAllOrNoElements(driver, By.xpath(listViewStatesChipCloseButton_xpath)); }
	
	/* ----- methods ----- */

	public void editSalesTerritories() {
		this.getEditSalesTerritoriesIcon().click();
		if(selectedRegion.equals(SalesRegion.US)) {
			
		}else if(selectedRegion.equals(SalesRegion.US_CANADA)) {
			
		}else if (selectedRegion.equals(SalesRegion.STATES_PROVINCE)) {
			verifyEditSalesTerritoriesPageElements();
			new SelectRegions(driver).selectRandomStates(5);
			this.getSaveButton().click();
			new HelperMethods().addSystemWait(2);
			this.verifyStatesSelected();
		}
			
	}
	/**
	 * <h1>salesTerritories_click</h1>
	 * <p> Click on edit icon
	 */
	public SalesTerritories clickOnEditSalesTerritories() {
		this.getEditSalesTerritoriesIcon().click();
		return this;
	}
	
	/**
	 * <h1>clickOnUSSalesTerritories</h1>
	 * <p> Click on US sales region
	 */
	public SalesTerritories clickOnUSSalesTerritories() {
		this.getUsRegion().click();
		GlobalVariables.storeObject("salesRegion", SalesRegion.US);
		selectedRegion = SalesRegion.US;
		return this;
	}
	
	/**
	 * <h1>clickOnUsAndCanadaSalesTerritories</h1>
	 * <p> Click on US and Canada sales region
	 */
	public SalesTerritories clickOnUsAndCanadaSalesTerritories() {
		this.getUsAndCanada().click();
		GlobalVariables.storeObject("salesRegion", SalesRegion.US_CANADA);
		selectedRegion = SalesRegion.US_CANADA;
		return this;
	}
	
	/**
	 * <h1>clickOnStatesOrProvinceSalesTerritories</h1>
	 * <p> Click on states or Province sales region
	 */
	public SalesTerritories clickOnStatesOrProvinceSalesTerritories() {
		this.getStatesOrProvinces().click();
		GlobalVariables.storeObject("salesRegion", SalesRegion.STATES_PROVINCE);
		selectedRegion = SalesRegion.STATES_PROVINCE;
		return this;
	}
	
	/**
	 * <h1>clickOnSaveButton</h1>
	 * <p> Click on Save Button
	 */
	public SalesTerritories clickOnSaveButton() {
		this.getSaveButton().click();
		return this;
	}
	
	
	/**
	 * <h1>clickOnSaveButton</h1>
	 * <p> Click on Save Button
	 */
	public SalesTerritories clickOnSelectStatesOrProvinceButton() {
		this.getSelectStatesOrProvinceButton().click();
		return this;
	}
	
	/**
	 * <h1>selectStateFromDropdownList</h1>
	 * <p>purpose: Method to add states from the drop down list - list view
	 * @param List of states to be added
	 * @return SalesTerritories
	 */
	public SalesTerritories selectStateFromDropdownList(List<String> stateList) {
		for(String state : stateList) {
			getSearchState().sendKeys(state,Keys.ARROW_DOWN , Keys.ENTER);
			helperMethods.addSystemWait(1);
		}
		return this;
	}
	
	public SalesTerritories deleteStatesChipsFromListView() {
		List<WebElement> chipsList = getListViewStatesChipCloseButtons();
		for (int i = 0; i < chipsList.size(); i++) {
			WebElement stateChipCloseButton = chipsList.get(i);
			stateChipCloseButton.click();
		}
		System.out.println(GlobalVariables.getTestID() + " INFO : States chips were deleted by clicking on close buttons");
		return this;
	}
	
	/* -----Verification ---- */

	/**
	 * <h1>verifyPageElements</h1>
	 * <p>
	 * purpose: Method to verify all elements on the page loaded properly
	 */
	public SalesTerritories verifyPageElements() {
		//page head
		this.getSalesTerritoriesPageHead();
		//Edit icon
		this.getEditSalesTerritoriesIcon();
		//Elements on landing page
		this.verifySalesRegion();
		if (selectedRegion.equals(SalesRegion.STATES_PROVINCE)) {
			this.getUnitedStatesTab();
			this.getCanadaTab();
			this.getListViewTab();
			this.verifyUSTabSelectedOnPageLoad();
			this.verifyStatesSelected();
			this.verifyListView();
		}
		return this;
	}
	
	/**
	 * <h1>verifyRegionPageElements</h1>
	 * <p>
	 * purpose: Method to verify all elements on the page loaded properly
	 */
	public SalesTerritories verifyRegionPageElements() { //TODO : Verification for Selected region
		//header
		this.getRegionPageHead();
		//All three images
		this.getUsRegion();
		this.getUsAndCanada();
		this.getStatesOrProvinces();
		//Button
		this.getCancelButton();
		this.getSaveButton();
		
		return this;
	}
	/**
	 * <h1>verifyEditSalesTerritoriesPageElements</h1>
	 * <p>
	 * purpose: Method to verify all elements on the page loaded properly on edit state/province screen
	 */
	public SalesTerritories verifyEditSalesTerritoriesPageElements() {
		this.getUnitedStatesTab();
		this.getCanadaTab();
		this.getListViewTab();
		this.verifyUSTabSelectedOnPageLoad();
		this.verifyStatesSelected();
		this.getSelectAllButton();
		this.getClearAllButton();
		this.getSelectCountryButton();
		this.getSaveButton();
		return this;
	}
	
	/**
	 * <h1>verifyUSTabSelectedOnPageLoad</h1>
	 * <p>
	 * purpose: Method to verify US tab is selected by default if individually
	 * selects state /province
	 */

	private void verifyUSTabSelectedOnPageLoad() {
		new WebDriverWait(driver, 10).withMessage("The US tab  is not selected")
				.until(ExpectedConditions.attributeContains(getSelectedUsTab(), "aria-selected", "true"));
	}

	/**
	 * <h1>verifySalesRegion</h1>
	 * <p>
	 * purpose: Here we verify the sales region opted while registration is same in
	 * bid profile page
	 */

	private void verifySalesRegion() {
		String xpath = null;
		if (selectedRegion.equals(SalesRegion.US)) {
			xpath = usImage_xpath;
		} else if (selectedRegion.equals(SalesRegion.US_CANADA)) {
			xpath = usCanadaImage_xpath;
		} else if (selectedRegion.equals(SalesRegion.STATES_PROVINCE)) {
			xpath = statesImage_xpath;
		} else {
			fail("Unable to find the sales region");
		}
		new WebDriverWait(driver, DEFAULT_TIMEOUT * 2)
				.withMessage("Unable to find a match with the sales region selected during registration process")
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
	}

	/**
	 * <h1>verifyStatesSelected</h1>
	 * <p>
	 * purpose: Here we verify the no. of states selected while registration is same
	 * as bid profile page
	 */

	private void verifyStatesSelected() {
		List<String> selectedStates = new ArrayList<>();
		if(GlobalVariables.containsObject("selectedStateList")) {
			selectedStates.addAll(GlobalVariables.getStoredObject("selectedStateList"));
		}
		new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("There is a miss match in state selection").until(
				ExpectedConditions.numberOfElementsToBe(By.xpath(selectedStateList_xpath), selectedStates.size()));
	}
	
	/**
	 * <h1>verifyListView</h1>
	 * <p>Purpose : Verifies the number of selected states on map and list view are same
	 */
	private void verifyListView() {
		this.getListViewTab().click();
		List<String> selectedStates = new ArrayList<>();
		if(GlobalVariables.containsObject("selectedStateList")) {
			selectedStates.addAll(GlobalVariables.getStoredObject("selectedStateList"));
		}
		List<WebElement> stateList = findAllOrNoElements(driver, By.xpath(listViewStatesOnBidProfile_xpath));
		System.out.println(GlobalVariables.getTestID() + " INFO : selectedStates on registration : " + selectedStates.size() + "Selected state on Bid Profile : " + stateList.size());
		new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("There is a miss match in state selection on \"List View\"").until(
				ExpectedConditions.numberOfElementsToBe(By.xpath(listViewStatesOnBidProfile_xpath), selectedStates.size()));
	}
	
	public SalesTerritories verifyStatesChipsRemovedFromListView() {
		
		new WebDriverWait(driver, DEFAULT_TIMEOUT)
		.withMessage("FAILED : Expected number of state chips are NOT matching actual number")
		.until(ExpectedConditions.numberOfElementsToBe(By.xpath(listViewStatesChipCloseButton_xpath), 0));
		return this;
	}
	
	public void veryfyStateOrProvinceFilterSection() {
		
		StatesProvincesSection statesProvincesSection = new StatesProvincesSection(driver);
		if(selectedRegion.equals(SalesRegion.US)) {
			
			new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Invalid State/Province filter section")
			.until(ExpectedConditions.and(ExpectedConditions.attributeContains(By.xpath(allUsStatesCheckBox_xpath), "class", "mat-checked"),
					ExpectedConditions.not(ExpectedConditions.attributeContains(statesProvincesSection.getSearchAllStatesProvincesCheckbox(), "class", "mat-checked"))));
			
		} else if (selectedRegion.equals(SalesRegion.US_CANADA)) {
			
			new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Invalid State/Province filter section")
			.until(ExpectedConditions.and(ExpectedConditions.invisibilityOfElementLocated(By.xpath(allUsStatesCheckBox_xpath)),
					ExpectedConditions.attributeContains(statesProvincesSection.getSearchAllStatesProvincesCheckbox(), "class", "mat-checked")));
			
		} else if (selectedRegion.equals(SalesRegion.STATES_PROVINCE)) {
			
			new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Invalid State/Province filter section")
			.until(ExpectedConditions.and(
					ExpectedConditions.not(ExpectedConditions.attributeContains(By.xpath(allUsStatesCheckBox_xpath), "class", "mat-checked")),
					ExpectedConditions.not(ExpectedConditions.attributeContains(statesProvincesSection.getSearchAllStatesProvincesCheckbox(), "class", "mat-checked"))));
			
			List<String> stateList =  GlobalVariables.getStoredObject("selectedStateList");
			System.out.println(GlobalVariables.getTestID() + " INFO : Selected State List in profile : " + Arrays.toString(stateList.toArray()));
			List<WebElement> elmList = findAllOrNoElements(driver, By.xpath(listViewStates_xpath));
			
			System.out.println(elmList.size() + "INFO : Selected State List in filter section : " + Arrays.toString(elmList.toArray()));
			new WebDriverWait(driver, DEFAULT_TIMEOUT)
			.withMessage("There is a miss match in state selection on filter section")
			.until(ExpectedConditions.numberOfElementsToBe(By.xpath(listViewStates_xpath), stateList.size()));
		}
	}
		
}
