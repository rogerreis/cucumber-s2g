package runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

// This runner can be used to run single tests.
// Simply tag your run @qt and then run with this runner.
@RunWith(Cucumber.class)
@CucumberOptions(
		plugin ={"pretty" , "json:target/cucumber/cucumber.json", "html:target/cucumber"},
        features = "src/test/resources/features",
        glue = {"stepDefinitions"},
        tags = {"not @ignore", "@qt"},
        dryRun = false,
        junit = "--step-notifications",
        snippets=SnippetType.CAMELCASE
)

public class SingleTestRunner {

}
