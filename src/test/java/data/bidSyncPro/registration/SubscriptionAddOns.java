package data.bidSyncPro.registration;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.fail;

/***
 * <h1>enum: SubscriptionAddOns</h1>
 * @author dmidura
 * <p>details: This enum defines what addons a user can select<br>
 * during the Chargebee registration (pro registration only)<br>
 * AddOn choices are: Canada, Military, Federal</p>
 */
public enum SubscriptionAddOns {
	FEDERAL,
	MILITARY,
	CANADA;

	/***
	 * <h1>getCanadaProvinces<h1>
	 * <p>purpose: return the provinces (abbreviations) that are included<br>
	 * 	in the Canada AddOn</p>
	 * @return String [] provinces = all the abbreviations for the Canada provinces<br>
	 * 	that are included in the Canada AddOn</p>
	 */
	public List<String> getCanadaProvinces() {
        return Arrays.asList("AB", "BC", "MB", "NB", "NL", "NT", "NS" ,"NU" ,"ON" ,"PE" ,"QC" ,"SK","YT");
	}
	
	/***
	 * <h1>getLocationsInMyAddOns</h1>
	 * <p>purpose: Get a list of all the locations that my user<br>
	 * 	should be able to access bids from based on their addOns<br>
	 * 	Canadian locations will be returned in abbreviation form,<br>
	 * 	federal and military bids are not dependent on state of<br>
	 * 	origin, but are labeled on each bid.</p>
	 * @return String []
	 */
	public List<String> getLocationsInMyAddOns() {
		switch(this) {
			case CANADA:
				// Canada bids are comprised of bids that originate from Canadian provinces.
				return this.getCanadaProvinces();
			case MILITARY:
				// Military bids are not dependent on the state, but are labeled on each bid.
				return Arrays.asList("military");
			case FEDERAL:
				// Federal bids are not dependent on the state, but labeled on each bid.
				return Arrays.asList("federal");
			default:
				fail("ERROR: Selected AddOn is invalid");
				return null;
		}
	}
	
    /***
     * <h1>getChargbeeAddOnName</h1>
     * <p>purpose: Return a string for the addOns that<br>
     * 	displays per Chargebee text</p>
     * @return String
     */
	public String getChargebeeAddOnName() {
		switch (this) {
		case FEDERAL:
			return "Federal";
		case MILITARY:
			return "Military";
		case CANADA:
			return "Canada";
		default:
			fail("ERROR: Selected AddOn is invalid");
			return null;
		}
	}
	
	/***
	 * <h1>getChargebeeFormattedAddOn</h1>
	 * <p>purpose: In the Chargebee site, a Subscription addOn appears per:<br>
	 *	[SubscriptionAddOn] Bids - [planLocation] Plan<br>
	 *	Return in this format<br>
	 * 	Ex:<br>
	 *	Military Bids - State Plan
	 * @param plan = user's SubscriptionPlan 
	 * @return String in Chargebee site format
	 */
	public String getChargebeeFormattedAddOn (SubscriptionPlan plan) {
		// format: as displays in Chargebee
		// Military Bids - State Plan 
		// Canada Bids - State Plan
		// Federal Bids - State Plan	
		
		return String.format("%s Bids - %s Plan", this.getChargebeeAddOnName(), plan.getLocationType());
	}

}
