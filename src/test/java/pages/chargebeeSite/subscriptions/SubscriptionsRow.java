package pages.chargebeeSite.subscriptions;

import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import data.bidSyncPro.registration.SubscriptionPlan;
import pages.chargebeeSite.subscriptions.subscriptionDetails.SubscriptionDetails;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

/***
 * <h1>SubscriptionsRow</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects for a single row in the Subscriptions table<br>
 * 			   on the Chargebee test site</p>
 */
public class SubscriptionsRow extends SubscriptionsTable {
	// Elements in the table - add more as needed
	// Column 1
	private final String subscriptionId_xpath = "//dl[@data-cb-col-index='1']/dd[1]/a";
	private final String status_xpath         = "//dl[@data-cb-col-index='1']/dd[2]/span";
	private final String recurringItems_xpath = "//dl[@data-cb-col-index='1']/dd[3]/a/span";

	// Column 2
	private final String name_xpath           = "//dl[@data-cb-col-index='2']/dd[1]/span";

	// Other
	private EventFiringWebDriver driver;
	private String strRootPath = "";
	private HelperMethods helperMethods;
	public SubscriptionsRow (EventFiringWebDriver driver, String strXPathToRow) {
		super(driver);
		this.driver      = driver;
		this.strRootPath = strXPathToRow;
		helperMethods = new HelperMethods();
	}
	
	/* ----- getters ----- */
	private WebElement getSubscriptionId() {return findByVisibility(By.xpath(this.strRootPath + this.subscriptionId_xpath));}
	private WebElement getStatus()         {return findByVisibility(By.xpath(this.strRootPath + this.status_xpath));        }
	private WebElement getRecurringItems() {return findByVisibility(By.xpath(this.strRootPath + this.recurringItems_xpath));}
	private WebElement getName()           {return findByVisibility(By.xpath(this.strRootPath + this.name_xpath));          }
	
	/* ----- helpers ----- */

	/***
	 * <h1>printSubscriptionInfo</h1>
	 * <p>purpose: Print subscription info to screen<br>
	 * 	For debug purposes</p>
	 */
	@SuppressWarnings("unused")
	private void printSubscriptionInfo() {
		System.out.println(GlobalVariables.getTestID() + " Name   = " + this.getName().getText());
		System.out.println(GlobalVariables.getTestID() + " Plan   = " + this.getRecurringItems().getText());
		System.out.println(GlobalVariables.getTestID() + " Status = " + this.getStatus().getText());
	}

	/* ----- methods ----- */
	
	/***
	 * <h1>clickSubscriptionId</h1>
	 * <p>purpose: Click on the subscription Id (link) to be navigated to the Subscription Details</p>
	 * @return SubscriptionDetails
	 */
	public SubscriptionDetails clickSubscriptionId() {
		helperMethods.addSystemWait(1);
		this.getSubscriptionId().click();
		return new SubscriptionDetails(driver);
	}
	
	/* ----- verifications ----- */
	/***
	 * <h1>verifyStatusIsActive</h1>
	 * <p>purpose: Verify that "ACTIVE" displays as the "Status" field
	 * 	Fail test if "Status" is not "ACTIVE".
	 * @return SubscriptionRow
	 */
	public SubscriptionsRow verifyStatusIsActive() {
		if (!this.getStatus().getText().toUpperCase().equals("ACTIVE")){
			fail("Test Failed: Subscription status is not ACTIVE"); }
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified status is active");
		return this;
	}

	/***
	 * <h1>verifyStatusIsCancelled</h1>
	 * <p>purpose: Verify that "CANCELLED" displays as the "Status" field
	 * 	Fail test if "Status" is not "CANCELLED".
	 * @return SubscriptionRow
	 */
	public SubscriptionsRow verifyStatusIsCancelled() {
		if (!this.getStatus().getText().toUpperCase().equals("CANCELLED")){
			fail("Test Failed: Subscription status is not CANCELLED"); }
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified status is cancelled");
		return this;
	}
	
	/***
	 * <h1>verifyRecurringItemIsValue</h1>
	 * <p>purpose: verify that the "Recurring Item" field is displaying the subscription plan
	 * @param expectedValue = SubscriptionPlan
	 * @return SubscriptionsRow  
	 */
	public SubscriptionsRow verifyRecurringItemIsValue(SubscriptionPlan expectedValue) {
		String strExpectedValue = expectedValue.getChargebeeSubscriptionBackEndName();
		int time = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, time)
			.withMessage("Test Failed: Recurring Item value is not \"" + strExpectedValue + "\" subscription plan after \"" + time + "\" seconds")
			.until(ExpectedConditions.attributeToBe(this.getRecurringItems(), "innerText", strExpectedValue));

		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that recurring item is \"" + strExpectedValue + "\"");
		return this;
	}

	/***
	 * <h1>verifyRecurringItemIsNotValue</h1>
	 * <p>purpose: verify that the "Recurring Item" field is not displaying the subscription plan
	 * @param expectedValue = SubscriptionPlan
	 * @return SubscriptionsRow  
	 */

	public SubscriptionsRow verifyRecurringItemIsNotValue(SubscriptionPlan expectedValue) {
		String strExpectedValue = expectedValue.getChargebeeSubscriptionName();
		if (this.getRecurringItems().getText().equals(strExpectedValue)) {
			fail("Test Failed: Recurring Item value is \"" + strExpectedValue + "\" subscription plan"); }
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that recurring item is not \"" + strExpectedValue + "\"");
		return this;
	}
	
}
