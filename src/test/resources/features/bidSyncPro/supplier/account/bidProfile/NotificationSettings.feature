@must @retrytest @administration @level3
Feature: Account > Bid Profile - Notifications Tab - Notification Settings
  ARGO-1624

  Scenario: Notifications Tab - Notification Settings on/off
    1) log in as qa@periscopeholdings.com
    2) turn switch to on and verify the database is updated
    3) turns switch to off and verify the database is updated
    There is separate Karate testing to verify the notifications are sent or not

    Given I navigate my browser to the supplier login page
    When I enter my Email as "qa@periscopeholdings.com" and Password as "test_1234"
    Then I will be on the supplier Home page
    Given I have navigated to Account > "Bid Profile"
    When I turn Notifications on
    Then the query "select users.username, user_setting.setting_value FROM users INNER JOIN user_setting ON users.user_id=user_setting.user_id WHERE username = 'qa@periscopeholdings.com'" against database "auth" will contain "true" in column 2
    When I turn Notifications off
    Then the query "select users.username, user_setting.setting_value FROM users INNER JOIN user_setting ON users.user_id=user_setting.user_id WHERE username = 'qa@periscopeholdings.com'" against database "auth" will contain "false" in column 2

    
@BIDS-476
Scenario: Notifications Tab - Notification Settings is on by default for a new user 
	Given I have registered a random user 
	Then I will be on the supplier Home page 
	And I wait "10" seconds for the backend to catch up
	Then I have navigated to Account > "Bid Profile" 
	And verify daily bid sent notification is turned on by default