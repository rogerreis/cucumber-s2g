package stepDefinitions.bidSyncPro.supplier.account.bidProfile;

import core.CoreAutomation;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.bidSyncPro.supplier.account.bidProfile.BidNotifications;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

import org.assertj.core.api.Fail;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BidNotifications_stepDefinitions {

	private EventFiringWebDriver driver;
	private BidNotifications bidNotifications;

	public BidNotifications_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
		bidNotifications = new BidNotifications(driver);
	}

	/* ------------ Test Steps ---------------- */

	@When("^I turn Notifications on$")
	public void iTurnNotificationsOn() throws Throwable {
		new WebDriverWait(driver, 10).until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='mat-slide-toggle-thumb']")));

		// wait for it to draw itself
		Thread.sleep(1000);

		// if the toggle ss already on, turn it off and then on again
		Boolean isPresent = driver.findElements(By.className("mat-checked")).size() > 0;

		// if it's on, click it to turn it off
		if (isPresent) {
			System.out.printf(GlobalVariables.getTestID() + " INFO: the toggle is on so turning it off first\n");
			driver.findElement(By.xpath("//div[@class='mat-slide-toggle-thumb']")).click();
			Thread.sleep(500);
			
		} else {
			//System.out.printf(GlobalVariables.getTestID() + " INFO: the toggle is off\n");
		}

		// turn it on
		driver.findElement(By.xpath("//div[@class='mat-slide-toggle-thumb']")).click();
		System.out.printf(GlobalVariables.getTestID() + " INFO: the toggle is on\n");
	}

	@When("^I turn Notifications off$")
	public void iTurnNotificationsOff() throws Throwable {
		new WebDriverWait(driver, 10).until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='mat-slide-toggle-thumb']")));

		// wait for it to draw itself
		Thread.sleep(1000);

		// if the toggle ss already on, turn it off and then on again
		Boolean isPresent = driver.findElements(By.className("mat-checked")).size() > 0;

		// if it's off, click it to turn it on
		if (isPresent) {
			//System.out.printf(GlobalVariables.getTestID() + " INFO: the toggle is on\n");
		} else {
			System.out.printf(GlobalVariables.getTestID() + " INFO: the toggle is off so turning it on\n");
			driver.findElement(By.xpath("//div[@class='mat-slide-toggle-thumb']")).click();
			Thread.sleep(1000);
		}

		driver.findElement(By.xpath("//div[@class='mat-slide-toggle-thumb']")).click();
		System.out.printf(GlobalVariables.getTestID() + " INFO: the toggle is off\n");
		new HelperMethods().addSystemWait(2);
	}
	
	@Then("verify daily bid sent notification is turned on by default")
	public void verifyDailyBidSentNotificationIsTurnedOnByDefault() {
			new HelperMethods().addSystemWait(5);  // wait for page to load to combat stale reference error
			bidNotifications.verifyBidNotificationIsOn();
	}
}
