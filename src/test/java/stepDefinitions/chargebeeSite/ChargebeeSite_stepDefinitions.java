package stepDefinitions.chargebeeSite;

import static org.junit.Assert.fail;

import java.util.ArrayList;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.bidSyncPro.registration.RandomChargebeeUser;
import data.bidSyncPro.registration.SubscriptionGrouping;
import data.bidSyncPro.registration.SubscriptionPlan;
import pages.bidSyncPro.registration.DatabaseRegistration;
import pages.bidSyncPro.supplier.companySettings.manageSubscriptions.ManageSubscriptions;
import pages.bidSyncPro.supplier.companySettings.manageSubscriptions.ManageSubscriptionsChargebeePopup;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.chargebeeSite.ChargebeeLoginPage;
import pages.chargebeeSite.common.CommonNavSliderMenu;
import pages.chargebeeSite.customers.ChargebeeCustomersDashboard;
import pages.chargebeeSite.customers.CustomersRow;
import pages.chargebeeSite.subscriptions.ChargebeeSubscriptionsDashboard;
import pages.chargebeeSite.subscriptions.SubscriptionsRow;
import pages.chargebeeSite.subscriptions.subscriptionDetails.SubscriptionDetails;
import pages.chargebeeSite.subscriptions.subscriptionDetails.cancelSubscriptionFlow.CancelSubscriptionImmediatelyPopup;
import pages.chargebeeSite.subscriptions.subscriptionDetails.cancelSubscriptionFlow.CancelSubscriptionPopup;
import pages.chargebeeSite.subscriptions.subscriptionDetails.changeSubscriptionFlow.ChangeSubscription;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

public class ChargebeeSite_stepDefinitions {
	private EventFiringWebDriver driver;	
	private HelperMethods helperMethods;
	private SupplierLogin supplierLogin;

	public ChargebeeSite_stepDefinitions(CoreAutomation automation) {
		//this.automation = automation;
		this.driver     = automation.getDriver();	
		helperMethods 	= new HelperMethods() ;
		supplierLogin 	= new SupplierLogin(driver);
	}
	
	/* ----- helpers ----- */
	
	/***
	 * <h1>login</h1>
	 * <p>purpose: Log into Chargebee test site with admin credentials. Navigate to Subscriptions dashboard and verify navigation</p>
	 * @return ChargebeeSubscriptionsDashboard
	 */
	private ChargebeeSubscriptionsDashboard login() {
		return new ChargebeeLoginPage(driver) 
			.loginAsChargebeeAdmin()
			.clickOnSubscriptionsButton()
			.verifyPageLoad();
	}
	
	/***
	 * <h1>searchCustomerForUser</h1>
	 * <p>purpose: In Chargebee test site, run top bar Customer search for user's email. Verify only one user record returned.
	 * @param email = user's registration email
	 * @return CustomersRow
	 */
	private CustomersRow searchCustomerForUser(String email) {
		new CommonNavSliderMenu(driver)
			.clickOnCustomerButton()
			.getSearchBar()
			.SearchTable(email);
		
		// Verify only one result was returned (can't have multiple records for email)
		return new ChargebeeCustomersDashboard(driver)
			.getCustomersTable()
			.verifyNumberOfRows(1)
			.getFirstResult();
	}
	
	/***
	 * <h1>searchSubscriptionForUser</h1>
	 * <p>purpose: In Chargebee test site, run top bar Subscription search for user's email. Verify only one user record returned.
	 * @param email = user's registration email
	 * @return SubscriptionsRow
	 */
	private SubscriptionsRow searchSubscriptionForUser(String email) {
		ChargebeeSubscriptionsDashboard dash = new ChargebeeSubscriptionsDashboard(driver);
		// Search Subscriptions for email
		dash
			.getSearchBar()
			.SearchTable(email);
		// Only one email per user
		return dash
			.getSubscriptionsTable()
			.verifyNumberOfRows(1)
			.getFirstResult();
		
	}
	
	/***
	 * <h1>cancelSubscription</h1>
	 * <p>purpose: In Chargebee test site, given a user's registration email, locate the Subscription record and Cancel it Immediately
	 * @param email = user's registration email
	 * @return CommonNavSliderMenu
	 */
	private CommonNavSliderMenu cancelSubscription(String email) {
		 this.searchSubscriptionForUser(email)
		.clickSubscriptionId()
		.clickOnCancelSubscriptionBtn();
		 
		 helperMethods.addSystemWait(2);
		 
		 new CancelSubscriptionPopup(driver).clickOnScheduleCancelation();
		 helperMethods.addSystemWait(1);
		 
		 new SubscriptionDetails(driver).clickOnCancelImmediatelyBtn();
		 helperMethods.addSystemWait(1);
		 new CancelSubscriptionImmediatelyPopup(driver).clickOnCancelNow();
		 helperMethods.addSystemWait(1);
		 return new SubscriptionDetails(driver).verifySuccessCancellingSubscriptionMsg();
	}
	
	/***
	 * <h1>changeUserSubscription</h1>
	 * <p>purpose: In Chargebee test site, given a user's registration email, locate the Subscription record and click on the id
	 * 	to view the "Customer Details" screen. From here, click on "Change Subscription" button to view
	 * 	the "Change Subscription" page
	 * @param email
	 * @return
	 */
	public ChangeSubscription changeUserSubscription(String email) {
		return this.searchSubscriptionForUser(email)
			.clickSubscriptionId()
			.clickOnChangeSubscriptionBtn()
			.verifyPageLoad();
	}
	
	/***
	 * <h1>verifyManageSubscriptionsInPro</h1>
	 * <p>purpose: Assuming user logged into BidSync Pro, navigate to the Manage Subscriptions > Chargebee punchout. Verify that the subscription is displayed
	 * @param subscription = SubscriptionPlan to verify
	 * @return ManageSubscriptions
	 * @throws InterruptedException 
	 */
	public ManageSubscriptions verifyManageSubscriptionsInPro(SubscriptionPlan subscription) throws InterruptedException {
		return new ManageSubscriptions(driver)
			.openCompanySettingsDropDown()
			.openManageSubscriptions()
			.clickManageSubscriptions()
			.verifyManageSubscriptionsOnlyDisplaysSubscription(subscription)
			.closePopup();
	}
	
	/* ----- test steps ----- */
	
	@Given ("^I have logged into the Chargebee test site as the admin user - Old$")
	public void I_have_logged_into_the_Chargebee_test_site_as_the_admin_user() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Logging into the Chargebee test site with dmidura credentials");
		this.login();
	}
	
	@And ("^I will verify that my user only has a \"(.*)\" subscription plan listed in the Chargebee test site - Old$")
	public void I_will_verify_that_my_user_only_has_a_subscription_plan_listed_in_the_Chargebee_test_site(String subscription) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Logging into chargebee and verifying that user only has \"" + subscription + "\" subscription");

		String email = SupplierLogin.thisUser.getEmail();
		
		this.login();
		this.searchSubscriptionForUser(email)
			.verifyRecurringItemIsValue(SubscriptionPlan.fromString(subscription))
			.logout();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: User only has a one subscription listed in Chargebee");
	}

	@Then ("^I will verify that the company owner only has a \"(.*)\" subscription plan listed in the Chargebee test site - Old$")
	public void I_will_verify_that_the_company_owner_only_has_a_subscription_plan_listed_in_the_Chargebee_test_site(String subscription) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying in Chargebee that company owner only has \"" + subscription + "\" subscription");
		
		RandomChargebeeUser owner = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");

		this.login();
		this.searchSubscriptionForUser(owner.getEmailAddress())
			.verifyRecurringItemIsValue(SubscriptionPlan.fromString(subscription))
			.logout();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Company Owner only has a one subscription listed in Chargebee");
	}

	@When ("^I cancel the user subscription in the Chargebee test site for my user account - Old$")
	public void I_cancel_the_user_subscription_in_the_Chargebee_test_site_my_user_account() {
		String email = SupplierLogin.thisUser.getEmail();
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Locating user \"" + email + "\" in Chargebee test site and then cancelling");
		
		new ChargebeeSubscriptionsDashboard(driver).navigateToHere();
		this.cancelSubscription(email)
			.logout();
	}

	@Then ("^I will verify that the user subscription is cancelled and that only a BidSync Basic subscription is created in BidSync Pro - Old$")
	public void I_will_verify_that_the_user_subscription_is_cancelled_and_that_only_a_BidSync_Basic_subscription_is_created_in_BidSync_Pro() throws InterruptedException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that subscription is cancelled and that only a BidSync Basic Subscription was created");
		String email            =  SupplierLogin.thisUser.getEmail();
		String accountReference =  SupplierLogin.thisUser.getAccountReference();

		// Verify BASIC subscription in Chargebee test site
		this.login();
		this.searchSubscriptionForUser(email)
			.verifyRecurringItemIsValue(SubscriptionPlan.BASIC)
			.verifyStatusIsActive();
		
		// Log into BidSyn Pro and verify "BASIC" in the Chargebee punchout
		supplierLogin.navigateToHere().loginAsUser(accountReference);
		this.verifyManageSubscriptionsInPro(SubscriptionPlan.BASIC).logout();
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Subscription cancelled and converted to BidSync Basic Plan");
	}
	
	
	@When ("^I create a user \"(.*)\" subscription in the Chargebee test site for my user account \"(.*)\" - Old$")
	public void I_create_a_user_subscription_in_the_Chargebee_test_site_for_my_user_account(String strSubscriptionPlan, String strUserAccount){
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Creating a new \"" + strSubscriptionPlan + "\" subscription for our user in Chargebee");
		try {
		// Get our user account for later user
		supplierLogin.setThisUserFromAccountReference(strUserAccount);
		String email = SupplierLogin.thisUser.getEmail();
		
		// Update the user subscription in Chargebee
		new ChargebeeSubscriptionsDashboard(driver).navigateToHere();
		this.changeUserSubscription(email)
			.updatePlanName(strSubscriptionPlan)
			.clickChangeSubscriptionBtn()
			.clickConfirmBtn();

		// Let Chargebee backend catch up to the subscription change
		helperMethods.addSystemWait(2);
		}catch(Exception exp) {
			fail("TEST FAILED :  I_create_a_user_subscription_in_the_Chargebee_test_site_for_my_user_account() " +  exp);
		}
	}
	
    @When ("^I change my user subscription to \"(.*)\" from the Manage Subscriptions screen - Old$")
    public void I_change_my_user_subscription_to_Subscription_from_the_Manage_Subscriptions_screen(String strSubscriptionPlan) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Changing user subscription to \"" + strSubscriptionPlan + "\" subscription in \"Manage Subscriptions\" screen");
		// 1. Navigate to the Manage Subscriptions. 
		// 2. Click on "Manage Subscriptions" button. Manage Subscriptions Chargbee portal appears.
		// 3. Update the subscription
		try {
		new ManageSubscriptions(driver)
			.openCompanySettingsDropDown()
			.openManageSubscriptions()
			.clickManageSubscriptions()
			.clickOnSubscriptionToUpdate() 
			.clickOnEditSubscription()
			.selectSubscriptionPlanFromDropdown(SubscriptionPlan.fromString(strSubscriptionPlan))
			.clickOnUpdateSubscription()
			.clickOnSubscriptionDetailsBackBtn();
		
		GlobalVariables.storeObject("strSubscriptionPlan", strSubscriptionPlan);
		}catch(Exception exp) {
			fail("Changing user subscription to \"" + strSubscriptionPlan + "\" subscription in \"Manage Subscriptions\" screen" + exp);
		}
    }

    @Then ("^I will verify that the subscription has changed and properly updates the ARGO database - Old$")
    public void I_will_verify_that_the_subscription_has_changed_and_properly_updates_the_ARGO_database() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that subscription has changed and properly updates the ARGO database");
		String strSubscriptionPlan = GlobalVariables.getStoredObject("strSubscriptionPlan");
		
		// Let everything on the ARGO backend catch up before we try to retrieve from the database
		// and yah, it really does take this long for the backend to sync with the front... ewww
		helperMethods.addSystemWait(12);
		
		// Verify in bidsync pro that the subscription is displaying
		// Verify in ARGO database that subscription has updated
		new ManageSubscriptionsChargebeePopup(driver)
				.verifyManageSubscriptionsOnlyDisplaysSubscription(SubscriptionPlan.fromString(strSubscriptionPlan))
				.verifySubscriptionPlanUpdatesInARGODatabase(SubscriptionPlan.fromString(strSubscriptionPlan), SupplierLogin.thisUser.getEmail()) 
				.closePopup();
    }

	@Then ("^I will verify that the \"(.*)\" subscription is updated in BidSync Pro - Old$")
	public void I_will_verify_that_the_subscription_is_updated_in_BidSync_Pro(String strSubscriptionPlan) throws InterruptedException {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that the \"" + strSubscriptionPlan + "\" subscription is updated in BidSync Pro");

		String email = SupplierLogin.thisUser.getEmail();
		String accountReference = SupplierLogin.thisUser.getAccountReference();

		// Verify the Subscription plan in Chargebee
		new ChargebeeSubscriptionsDashboard(driver).navigateToHere();
		new HelperMethods().addSystemWait(2);
		this.searchSubscriptionForUser(email)
			.verifyRecurringItemIsValue(SubscriptionPlan.fromString(strSubscriptionPlan))
			.verifyStatusIsActive();
	
		// Log into BidSyn Pro and verify subscription plan displays in Chargebee punchout
		supplierLogin.navigateToHere().loginAsUser(accountReference);
		this.verifyManageSubscriptionsInPro(SubscriptionPlan.fromString(strSubscriptionPlan)).logout();	
	}
	
	@Then ("^I will verify that my user only has a \"(.*)\" subscription plan listed in the ARGO database - Old$")
	public void I_will_verify_that_my_user_only_has_a_subscription_plan_listed_in_the_ARGO_database(String subscriptionPlan) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that user only has a \""  + subscriptionPlan + "\" listed in ARGO database");

		String email = SupplierLogin.thisUser.getEmail();
		new DatabaseRegistration(email)
			.verifySubscriptionPlan(email, SubscriptionPlan.fromString(subscriptionPlan));
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Only one subscription for this user");
	}
	
	
	@And ("^I will verify that the company owner only has a \"(.*)\" subscription plan listed in the ARGO database - Old$")
	public void I_will_verify_that_the_company_owner_only_has_a_subscription_plan_listed_in_the_ARGO_database(String subscriptionPlan) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that the company owner only has a \""  + subscriptionPlan + "\" listed in ARGO database");

		RandomChargebeeUser owner = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		new DatabaseRegistration(owner.getEmailAddress())
			.verifySubscriptionPlan(owner.getEmailAddress(), SubscriptionPlan.fromString(subscriptionPlan));
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Only one subscription for this the company owner");
	}
	
	@When ("^the Chargebee admin cancels the subscription for the company owner - Old$")
	public void the_Chargebee_admin_cancels_the_subscription_for_the_company_owner() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Canceling the subscription for the company owner via Chargebee");
		RandomChargebeeUser owner = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");

		this.login();
		this.cancelSubscription(owner.getEmailAddress())
			.logout();
		
		// Update the stored values to reflect the cancellation
		// Note: This is what we expect to happen
		owner.setSubscriptionPlan(SubscriptionPlan.BASIC);
		owner.setSubscriptionGrouping(SubscriptionGrouping.NONE);
		owner.setAddOns(new ArrayList<>());
		owner.setAdditionalStateAddOns(new ArrayList<>());
		GlobalVariables.storeObject("ownerRandomChargebeeUser", owner);
	}
	
	@Then ("^I will verify that only one user customer account exists for my user in the Chargebee test site - Old$")
	public void I_will_verify_that_only_one_user_customer_account_was_created_for_my_user_in_the_Chargebee_test_site() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that only one customer account was created for user in Chargebee" );
		RandomChargebeeUser newUser = GlobalVariables.getStoredObject("StoredUser");
		
		// Grab customer id from Chargebee
		this.login();
		String id = this.searchCustomerForUser(newUser.getEmailAddress())
				.verifyCustomerRecord(newUser)
				.getCustomerIdText();
		new CommonNavSliderMenu(driver).logout();

		GlobalVariables.storeObject("customerId", id);
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Verified only one Chargebee customer account was created for new user");
			
	}
	
	@And ("^I will verify that my user's customer_id matches in the ARGO database and Chargebee test site - Old$")
	public void I_will_verify_that_my_user_customer_id_matches_in_the_ARGO_database_and_Chargebee_test_site(){
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that customer id in Chargebee is stored in the ARGO database");
		RandomChargebeeUser newUser = GlobalVariables.getStoredObject("StoredUser");
		String customerId = GlobalVariables.getStoredObject("customerId");
		
		new DatabaseRegistration(newUser.getEmailAddress())
			.verifyUserRegistrationCustomerIdAsExpected(customerId);
	
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Verified that Chargebee customer id is stored in ARGO database");
	}
	
	@When ("^I Change Subscription for the first visible subscription record in the Chargebee test site - Old$")
	public void I_Change_Subscription_for_the_first_visible_subscription_record_in_the_Chargebee_test_site() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Selecting first visible subscription record in Chargebee test site, and clicking \"Change Subscription\"");
		new ChargebeeSubscriptionsDashboard(driver)
			.getSubscriptionsTable()
			.getFirstResult()
			.clickSubscriptionId()
			.clickOnChangeSubscriptionBtn();
	}
	
	@Then ("^I will verify that the five Additional State dropdowns appear with populated values on the Chargeebee test site - Old$")
	public void I_will_verify_that_the_five_Additional_State_dropdowns_appear_with_populated_values_on_the_Chargeebee_test_site() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that the five \"Additional State\" dropdowns appear with populated values on the Chargebee test site");

		new ChangeSubscription(driver).verifyAdditionalStateDropdownsContainAllOptions();
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Five \"Additional State\" dropdowns are visible in Chargebee test site and are correctly populated");
	}
	
	@When ("^I randomly purchase Additional States addOns from the Chargebee test site - Old$")
	public void I_randomly_purchase_Additional_States_addOns_from_the_Chargebee_test_site() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Randomly purchasing Additional States addOns from Chargebee test site");

		RandomChargebeeUser user = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		user.setRandomAdditionalStateAddOns();

		this.login();
		this.changeUserSubscription(user.getEmailAddress())
			.purchaseAdditionalStates(user.getAdditionalStateAddOns())
			.verifySuccessChangingSubscriptionMsg()
			.logout();
	}
	
	@Then ("^I will verify that the Chargebee test site correctly displays the company owner subscription and addOns - Old$")
	public void I_will_verify_that_the_Chargebee_test_site_correctly_displays_the_company_owner_subscription_and_addOns() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Chargebee test site correctly displays company owner's subscription and addOn");
		RandomChargebeeUser user = GlobalVariables.getStoredObject("ownerRandomChargebeeUser");
		user.printSubscriptionDetails();

		this.login();
		this.searchSubscriptionForUser(user.getEmailAddress())
			.clickSubscriptionId()
			.verifySubscriptionDetails(user)
			.logout();
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Chargebee test site correctly displays the company owner's subscription and addOns");
	}

}











