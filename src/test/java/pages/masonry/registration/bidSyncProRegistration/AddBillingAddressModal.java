package pages.masonry.registration.bidSyncProRegistration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.Select;

import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class AddBillingAddressModal</h1>
 * <p>details: This class houses page objects and methods for the "Add Billing Address" modal (2nd modal in Chargebee<br>
 * 	punchout registration flow) that is part of the BidSync Pro registration off the Masonry site</p>
 */
public class AddBillingAddressModal extends BasePageActions {
    
    // ids
    private final String firstNameInput_id    = "first_name";
    private final String lastNameInput_id     = "last_name";
    private final String addressLine1Input_id = "line1";
    private final String addressLine2Input_id = "line2";
    private final String cityInput_id         = "city";
    private final String zipCodeInput_id      = "zip";
    private final String stateSelect_id       = "state_code";
    private final String countrySelect_id     = "country";
    
    // xpaths
    private final String nextButton_xpath     = "//button[contains(@data-cb-id, 'address_submit')]";
    
    // Other
    private EventFiringWebDriver driver;

    public AddBillingAddressModal(EventFiringWebDriver driver) {
    	super(driver);
        this.driver        = driver;
    }
    
    /* ----- getters ----- */

    private WebElement getFirstNameInput()    { return findByVisibility(By.id(this.firstNameInput_id));             } 
    private WebElement getLastNameInput()     { return findByVisibility(By.id(this.lastNameInput_id));              } 
    private WebElement getAddressLine1Input() { return findByVisibility(By.id(this.addressLine1Input_id));          } 
    private WebElement getAddressLine2Input() { return findByVisibility(By.id(this.addressLine2Input_id));          } 
    private WebElement getCityInput()         { return findByVisibility(By.id(this.cityInput_id));                  } 
    private WebElement getZipCodeInput()      { return findByVisibility(By.id(this.zipCodeInput_id));               } 
    private Select     getStateInput()        { return new Select (findByVisibility(By.id(this.stateSelect_id)));   } 
    private Select     getCountrySelect()     { return new Select (findByVisibility(By.id(this.countrySelect_id))); } 
    private WebElement getNextButton()        { return findByVisibility(By.xpath(this.nextButton_xpath));           } 

    /* ----- methods ----- */
    /****
     * <h1>fillInBillingAddressAndSubmit</h1>
     * <p>purpose: Fill in billing info (from user registration data) into the "Add Billing Address" modal<br>
     *  (2nd Chargebee punchout registration modal) and then click "NEXT" to submit</p>
     * @param user = user registration data
     * @return AddPaymentDetailsModal
     */
    public AddPaymentDetailsModal fillInBillingAddressAndSubmit(RandomChargebeeUser user) {
        getFirstNameInput()   .sendKeys(user.getFirstName());
        getLastNameInput()    .sendKeys(user.getLastName());
        getAddressLine1Input().sendKeys(user.getAddress());
        getAddressLine2Input().sendKeys(user.getAddressLine2());
        getCityInput()        .sendKeys(user.getCity());
        getZipCodeInput()     .sendKeys(user.getZipCode());
        getCountrySelect()    .selectByVisibleText(user.getCountry());
        getStateInput().selectByVisibleText(user.getState());
        
        getNextButton().click();
        return new AddPaymentDetailsModal(driver);
    }
}
