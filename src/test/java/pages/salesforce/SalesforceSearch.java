package pages.salesforce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;

public class SalesforceSearch extends BasePageActions{

	private EventFiringWebDriver driver;
	// xpaths
	private final String accountsTab_xpath     = "//a[contains(text(),'Accounts')]";
	private final String headerSearchField_xpath  = "//input[@id='phSearchInput']";
	private final String headerSearchButton_xpath = "//input[@id='phSearchButton']";
	private final String contactsTab_xpath		= "//li[@id='Contact_Tab']/a[contains(text(),'Contacts')]";
	private final String subscriptionInfo_xpath = "//h3[contains(text(),'Subscription Information')]";

	public SalesforceSearch (EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	/* ----- getters ----- */

	
	@SuppressWarnings("unused")
	private WebElement getAccountsTab()  { return findByPresence(By.xpath(this.accountsTab_xpath));  }
	private WebElement getHeaderSearchField() { return findByPresence(By.xpath(this.headerSearchField_xpath)); }
	private WebElement getHeaderSearchButton()     { return findByPresence(By.xpath(this.headerSearchButton_xpath));     }
	public WebElement getcontactsTab()     { return findByVisibility(By.xpath(this.contactsTab_xpath));     }
	
	/* --------------- Methods --------------- */
	
	/***
	 * <h1>Login</h1>
	 * <p>
	 * purpose: Login
	 * </p>
	 * 
	 * @return BuySpeedLogin
	 */
	public SalesforceSearch HeaderSearch(String searchTerm) {
		this.getHeaderSearchField().sendKeys(searchTerm);
		this.getHeaderSearchButton().click();
		return this;
	}
	
	public void clickOnGivenUserName(String username) {
		String name_xpath = "//a[contains(text(),'"+username+"')]";
		findByVisibility(By.xpath(name_xpath)).click();
	}
	
	public void verifyBidSyncProData(RandomChargebeeUser user) {
		scrollIntoViewBottomOfScreen(By.xpath(subscriptionInfo_xpath));
		String industry_xpah = "//div[contains(text(),'"+ user.getIndustry() + "')]";
		String companySize_xpath = "//div[contains(text(),'"+ user.getCompanySize() + "')]";
		String companyRevenue_xpath = "//div[contains(text(),'"+ user.getCompanyRevenue() + "')]";
		String reasonForReg_xpath = "//div[contains(text(),'"+ user.getReasonForRegistration() + "')]";
		System.out.println(GlobalVariables.getTestID() + " INFO : industry_xpah : " + industry_xpah);
		System.out.println(GlobalVariables.getTestID() + " INFO : companySize_xpath : " + companySize_xpath);
		System.out.println(GlobalVariables.getTestID() + " INFO : companyRevenue_xpath : " + companyRevenue_xpath);
		System.out.println(GlobalVariables.getTestID() + " INFO : reasonForReg_xpath : " + reasonForReg_xpath);
		new WebDriverWait(driver, DEFAULT_TIMEOUT).withMessage("Data not pushed from BidSync Pro to Salesforce")
		.until(ExpectedConditions.and(ExpectedConditions.visibilityOfElementLocated(By.xpath(industry_xpah)),
				ExpectedConditions.visibilityOfElementLocated(By.xpath(companySize_xpath)),
				ExpectedConditions.visibilityOfElementLocated(By.xpath(companyRevenue_xpath)),
				ExpectedConditions.visibilityOfElementLocated(By.xpath(reasonForReg_xpath))));
	}
}
