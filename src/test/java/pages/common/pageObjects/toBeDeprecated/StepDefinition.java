/**
 * 
 */
package pages.common.pageObjects.toBeDeprecated;

import static org.junit.Assert.fail;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

/**
 * @author jlara
 *
 */
@Deprecated
public class StepDefinition {

	protected CoreAutomation automation;
	protected EventFiringWebDriver driver;
	protected HelperMethods helper;
	
	public StepDefinition(CoreAutomation automation) throws Throwable  {
		this.automation = automation;
		this.driver = automation.getDriver();
		helper = new HelperMethods();
	} //end of constructor
	


	/***
	 * Verify that inner text matches expected value (exact)
	 * @param element
	 * @param expectedValue
	 * @return 
	 */
	public boolean verifyInnerTextExactMatch(WebElement element, String expectedValue) {
		if(!element.getText().equals(expectedValue))
			fail(String.format("FAILED: [%s] doesn't match %s",expectedValue, element.getText()));
		
		System.out.printf(GlobalVariables.getTestID() + " PASSED: [%s] matches %s%n",expectedValue,element.getText());
		return true;
	} // end of verifyInnerTextExactMatch()
	
	/***
	 * Verify that inner text matches expected value (contains)
	 * @param element
	 * @param expectedValue
	 * @return 
	 */
	public boolean verifyInnerText(WebElement element, String expectedValue) {
		if(!element.getText().contains(expectedValue))
			fail(String.format("FAILED: [%s] not found in %s",expectedValue, element.getText()));
		
		System.out.printf(GlobalVariables.getTestID() + " PASSED: [%s] found in %s (Raw Text: %s)%n",expectedValue,element.toString(),element.getText());
		return true;
	} // end of verifyInnerText()
	

	/***
	 * Verify that the element is visible
	 * @param element
	 * @return boolean
	 */
	public boolean verifyVisible(WebElement element) {
		if (element.isDisplayed()) {
			System.out.printf(GlobalVariables.getTestID() + " PASSED: [%s] is visible%n",element.toString());
			return true;
		} // end of if
		else {
			fail(String.format("FAILED: [%s] is not visible%n",element.toString()));
			return false;
		} // end of else
	} // end of verifyVisible()
	
	/***
	 * Verify that the element is notvisible
	 * @param element
	 * @return boolean
	 */
	public boolean verifyNotVisible(WebElement element) {
		if (!element.isDisplayed()) {
			System.out.printf(GlobalVariables.getTestID() + " PASSED: [%s] is not visible%n",element.toString());
			return true;
		} // end of if
		else {
			fail(String.format("FAILED: [%s] is visible%n",element.toString()));
			return false;
		} // end of else
	} // end of verifyNotVisible()
}
