package data.bidSyncPro.registration;

/**
 * <h1>enum: CompanySizeEnum</h1>
 * @author ssomaraj
 *<p>details: This enum defines the range of values for size of company the supplier can specify during registration
 */
public enum CompanySizeEnum {
	SIZE_1_50,
	SIZE_51_100,
	SIZE_101_250,
	SIZE_251_500,
	SIZE_501_1000,
	SIZE_1001_5000,
	SIZE_5001_10000,
	SIZE_10001_PLUS;
}
