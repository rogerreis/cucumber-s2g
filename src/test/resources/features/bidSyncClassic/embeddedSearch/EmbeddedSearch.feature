@must
Feature: Integrations > BidSync Classic - Embedded Search Test
	This feature file used to test the BidSync embedded search

@EmbeddedSearching @Searchwithagencies 
Scenario Outline: BidSync embedded searches
	Given I navigate to BidSync embedded search page		
		And I perform a basic search with all options "<searchOptions>" and search for "<item>"		
		And I wait "5" seconds for the backend to catch up		
	Then We are done with searching		
		
	Examples:
	|searchOptions|item|		
	|bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?||
	|bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?&hideLocationCol=Y||
	|bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?&hideTabulationCol=Y||
	|bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?&curronly=1||	
	|bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?|tools|
	|bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?hideLocationCol=Y|construction|
	|bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?hideTabulationCol=Y|electrical|
	|bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?curronly=1|paper|
		

Scenario Outline: Search for Marocopa County Bids with all options
	Given I navigate to BidSync embedded search page	   
	  And I search for Maricopa County Bids with all options "<searchURLs>"
	  And I wait "5" seconds for the backend to catch up
  Then We are done with searching
  
  Examples:
	|searchURLs|
	|bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?srchoid_override=217285|
	|bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?srchoid_override=217285&hideLocationCol=Y|
	|bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?srchoid_override=217285&hideTabulationCol=Y|	
	|bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?hideLocationCol=Y|
	|bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?srchoid_override=217285&dept_override=020|
	|bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?srchoid_override=217285&dept_override=050|
	|bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?srchoid_override=217285&dept_override=060|


Scenario Outline: Search other agency sites	
	Given I navigate to BidSync embedded search page
		And I begin searching other agency sites "<searchURLs>"
		And I wait "5" seconds for the backend to catch up
  Then We are done with searching
    
  Examples:
	|searchURLs|
	|https://classic.phi-production.cloud/bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?srchoid_override=178|
	|https://classic.phi-production.cloud/bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?srchoid_override=178&curronly=1|	
	|http://www.yolocounty.org/general-government/general-government-departments/financial-services/procurement/current-advertised-bids|
	|https://fremont.gov/1217/Bidding-on-Services-Supplies-Equip-IT|
	|http://www.cityofmoorhead.com/government/rfps-and-bids|
	|https://sasktenders.ca/content/public/Search.aspx|
	
Scenario Outline: Search for current bids	
	Given I navigate to BidSync embedded search page
		And I search for current only bids "<searchURL>"		 
		And I wait "5" seconds for the backend to catch up
  Then We are done with searching
    
  Examples:
	|searchURL|	
	|https://classic.phi-production.cloud/bidsync-app-web/shared/shared/embeddedSearchResults.xhtml?&curronly=1|