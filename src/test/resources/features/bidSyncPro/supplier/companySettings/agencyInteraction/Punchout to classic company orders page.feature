@must @integrations @level3
Feature: Company Settings > Agency Interaction - Punchout to Classic Company Orders Page
  ARGO-1243: Ability to view My Company Purchase Orders in Classic (Punchout)
  BIDSYNC-354: Cucumber: Refactor punchout to classic company orders tests

  dmidura: poggle@mailiantor.com requires specific profile information and we don't have access to that account
  Need to determine this information and create a new user account for this test. 
  Setting last two tests to @should until we can get an account

@ARGO-1243 @BIDSYNC-354
  Scenario: Company Orders Initializes All Fields as Expected
    Given I have logged into the Supplier side as user "Basic"
    And I have navigated to Company Settings > "Agency Interaction"
    When I click on the Agency Interaction Company Orders Tab
	Then I will verify that the Company Orders Page has correctly initialized
    
@ARGO-1243 @ignore @refactor
Scenario: Company Orders Can be Searched and Sorted 
    Given I have logged into the Supplier side as user "Basic"
    And I have navigated to Company Settings > "Agency Interaction"
    And I click on the Agency Interaction Company Orders Tab
    #When I search for a term
    #Then I will verify that the Company Orders Page Search returns Results
    #And I will verify that search column headers appear
    #And I will verify that the Company Orders Page Results table can be sorted