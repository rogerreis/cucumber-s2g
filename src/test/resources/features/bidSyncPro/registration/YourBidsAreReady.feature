@must @registration @level3
Feature: Supplier Registration - Your Bids are Ready Screen
	BIDSYNC-310: " "YOUR BIDS ARE READY" is loading incorrectly"
	ARGO-2581: "Expand coverage of Your Bids Are Ready page in supplier registration"

@BIDSYNC-310 @ARGO-2581
Scenario: Your Bids are Ready Should Correctly Initialize
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	When I have completed supplier registration up to Your Bids Are Ready
	Then I will verify that the Your Bids are Ready screen has properly initialized

@ARGO-2581
Scenario: "INCREASE BID RELEVANCY" will navigate to the "Increase Bid Relevancy" screens
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to Your Bids Are Ready
	When I click on the INCREASE BID RELEVANCY button on the Your Bids are Ready screen
	Then I will be on the first Increase Bid Relevancy page
	And I can Increase Bid Relevancy five times before returning to the Your Bids are Ready screen
