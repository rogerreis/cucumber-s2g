package pages.common.helpers;


/***
 * <h1>StringHelpers</h1>
 * @author dmidura
 * <p>details: This class contains methods to manipulate strings</p>
 */
public class StringHelpers {

	public StringHelpers() {
		
	}
	
	 /***
	 * <h1>removeWhitespaceAndSpecialCharsFromString</h1>
	 * <p>purpose: Give a string, this helper method returns the string stripped of whitespace and special chars.</p>
	 *             Ex: input "Negative Keywords" will be returned as "NegativeKeywords".<br>
	 * @param strInputText = text to strip of whitespace and special chars
	 * @return String = string stripped of whitespace and special chars
	 */
	 public String removeWhitespaceAndSpecialCharsFromString(String strInputText) {
		 return strInputText.replaceAll("[^\\w]", "");
	 }
	 
	 /***
	 * <h1>removeWhitespaceNumbersAndSpecialCharsFromString</h1>
	 * <p>purpose: Give a string, this helper method returns the string stripped of whitespace, numbers, and special chars.</p>
	 *             Ex: input "Negative Keywords1!" will be returned as "NegativeKeywords".<br>
	 * @param strInputText = text to strip of whitespace and special chars
	 * @return String = string stripped of whitespace and special chars
	 */
	 public String removeWhitespaceNumbersAndSpecialCharsFromString(String strInputText) {
		 return strInputText.replaceAll("[^a-zA-Z]", "");
	 }
	 
	 /***
	  * <h1>formatStringForPattern</h1>
	  * <p>purpose: Given a string, return formatted for Pattern matching
	  * @param stringToFormat = exact string to format
	  * @return String formatted for Pattern compile
	  */
	 public String formatStringForPattern(String stringToFormat) {
		 return stringToFormat
				 .replaceAll("\\.", "\\\\\\.")
				 .replaceAll("\\?", "\\\\\\?")
				 .replaceAll("\\-", "\\\\\\-");
	 }
}
