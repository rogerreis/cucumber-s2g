package stepDefinitions.bidSyncPro.supplier.dashboard.filterResultsSection;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import io.cucumber.datatable.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.bidSyncPro.supplier.dashboard.BidListTabNames;
import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidListRow;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterCriteria;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterResultsSectionNewClass;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.TimeFrameOptions;
import pages.common.helpers.GlobalVariables;

public class FilterResultsSectionSearches_stepDefinitions {
	
	private EventFiringWebDriver driver;
	public FilterResultsSectionSearches_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
	}

	/* Each DataTable that is used to input a filter has format as below:
	
	 * @param dTable = DataTable formatted as:
	 *    | Filter Name            | <Filter_Name>                    |                                 | ... |                                 |
	 *    | Keywords               | [keyword1]                       | [keyword2]                      | ... | [keywordx]                      | 
	 *    | Negative Keywords      | [keyword1]                       | [keyword2]                      | ... | [keywordx]                      | 
	 *    | States/Provinces       | [state/province1_from_dropdown]  | [state/province2_from_dropdown> | ... | [state/provincex_from_dropdown] | 
	 *    | Time Frame             | [time_frame_from_dropdown]       |                                 | ... |                                 | 
	 *    | [Show Bids From]       | [start_date]                     |                                 | ... |                                 | 
	 *    | [Show Bids to]         | [end_date]                       |                                 | ... |                                 | 
	 *    
	 *    (start_date and end_date should be in MM/DD/YYYY format)
	 *    Here, you can add as many keywords and states as you want, BUT the table must be balanced (i.e. if you
	 *    add 3 states, then there should be 4 columns in the datatable). Filters that can only be set to one value should
	 *    then be input with blanks.
	 *    Note: if any field in the datatable is blank, then that filter will be skipped.
	 *    "Show Bids From" and "Show Bids to" fields should be provided only when the Time Frame = "Custom Time Frame"
	 *    Note: Filter Name is required
	 *
	 *    Ex:  
	 *    | Filter Name            | filter              |      |         |
	 *    | Keywords               | test                |      |         | 
	 *    | Negative Keywords      |                     |      |         | 
	 *    | States/Provinces       | Texas               | Utah | Alabama | 
	 *    | Time Frame             | Past 6 months       |      |         | 
	 *    
	 *    Ex: Time Frame = "Custom Time Frame"
	 *    | Filter Name            | filter              |      |         |
	 *    | Keywords               | test                |      |         | 
	 *    | Negative Keywords      | bad                 | no   |         | 
	 *    | States/Provinces       | Texas               | Utah | Alabama | 
	 *    | Time Frame             | Custom Time Frame 	 |      |         | 
	 *    | Show Bids From         | 12/12/2016          |      |         | 
	 *    | Show Bids to           | 02/02/2018          |      |         | 
	 *    
	 */
	
	/* ----- helpers ----- */
	
	/***
	 * <h1>setFilterAndSearch</h1>
	 * <p>purpose: Assuming Filter Results section is open, clear out previous filter.
	 * 	Set new filter. Click "Search"
	 * @param filter = FilterCriteria for new filter to search on
	 */
	private void setFilterAndSearch(FilterCriteria filter) {
		new FilterResultsSectionNewClass(driver)
				.clickClearAllBtn()
				.setFilter(filter)
				.clickSearchBtn();	
	}

	/***
	 * <h1>setFilterAndSearch</h1>
	 * <p>purpose: Assuming Filter Results section is open, clear out previous filter.
	 * 	Set new filter via DataTable. Click "Search"
	 * @param dTable = DataTable with search filter (see above for DataTable defn)
	 */
	private void setFilterAndSearch(DataTable dTable) {
		new FilterResultsSectionNewClass(driver)
				.clickClearAllBtn()
				.setFilterFromDataTable(dTable)
				.clickSearchBtn();
	}
	
	/***
	 * <h1>verifyUnsavedFilterDisplays</h1>
	 * <p>purpose: After seeing a filter in the Filter Results, 
	 * 	verify that Filter Results is open, then verify that Filter
	 * 	Results displays the filter correctly.<br>
	 * 	Note: This method should not be used for filter retrieved via Saved Searches
	 * @param filter
	 */
	private void verifyUnsavedFilterDisplays(FilterCriteria filter) {
			new DashboardCommon(driver)
				.verifyFilterResultsSectionOpen()
				.getFilterResultsSectionNewClass()
				.verifyFilterResultsDisplaysUnsavedFilter(filter);
	}
//#Fix for release OCT 7
	/***
	 * <h1>verifyUnsavedFilterDisplays</h1>
	 * <p>purpose: After seeing a filter in the Filter Results,
	 * 	verify that Filter Results is open, then verify that Filter
	 * 	Results displays the filter correctly. After viewing bid
	 * 	and click back tob bid<br>
	 * 	Note: This method should not be used for filter retrieved via Saved Searches
	 * @param filter
	 */
	private void verifyUnsavedFilterDisplaysAfterBackToBid(FilterCriteria filter) {
		new DashboardCommon(driver)
				.verifyFilterResultsSectionOpen()
				.getFilterResultsSectionNewClass()
				//#Fix for release OCT 7
				.verifyFilterResultsDisplaysUnsavedFilterAfterBackToBid(filter);
	}
	
	/***
	 * <h1>verifySearchResultsDisplayPerFilterResults</h1>
	 * <p>purpose: After running a search on Filter Results:<br>
	 * 	1. Verify that All Bids is hightlighted and displayed<br>
	 * 	2. Verify that each bid in the All Bids list is displaying per criteria in filter
	 * @param filter = FilterCriteria for the search
	 */
	private void verifySearchResultsDisplayPerFilterResults(FilterCriteria filter) {
		new FilterResultsSectionNewClass(driver)
				.verifyTabIsHighlightedAndSelected(BidListTabNames.ALL_BIDS)
				.getFilterResultsSectionNewClass()
				.verifyFilterSearchResultsDisplayCorrectlyInAllBidsTab(filter);
	}

	/***
	 * <h1>verifySearchResultsDisplayPerFilterResults</h1>
	 * <p>purpose: After running a search on Filter Results:<br>
	 * 	1. Verify that All Bids is hightlighted and displayed<br>
	 * 	2. Verify that each bid in the All Bids list is displaying per criteria DataTable
	 * @param dTable = DataTable with search filter (see above for DataTable defn)
	 */
	private void verifySearchResultsDisplayPerFilterResults(DataTable dTable) {
		new FilterResultsSectionNewClass(driver)
				.verifyTabIsHighlightedAndSelected(BidListTabNames.ALL_BIDS)
				.getFilterResultsSectionNewClass()
				.verifyFilterSearchResultsDisplayCorrectlyInAllBidsTab(dTable);
		}

	/* ----- test steps ----- */
	
	@Then ("^I will verify that bids within All Bids tab are displayed within the Custom Time Frame$")
	public void Then_I_will_verify_that_bids_within_All_Bids_tab_are_displayed_within_the_Custom_Time_Frame () {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that bid results appear within the specified time range");
		
		FilterCriteria filter = GlobalVariables.getStoredObject("filter");
		this.verifySearchResultsDisplayPerFilterResults(filter);

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Each bid in All Bids list is within the custom time frame");
	}
	

	/***
	 * <h1>I_run_a_search_on_the_Filter_Results_section_with_filters</h1>
	 * @param DataTable (see comment at top of this class for DataTable format)
	 */
	@When("^I run a search on the Filter Results section with filters:$")
	public void I_run_a_search_on_the_Filter_Results_section_with_filters(DataTable dTable) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Adding filter criteria to search on");

		this.setFilterAndSearch(dTable);
	}
	
	
	/***
	 * <h1>I_will_verify_that_All_Bids_tab_is_both_highlighted_and_selected_and_also_displays_correct_results</h1>
	 * @param DataTable (see comment at top of this class for DataTable format)
	 */
	@Then("^I will verify that All Bids tab is both highlighted and selected, and also displays correct results per selected filters:$")
	public void I_will_verify_that_All_Bids_tab_is_both_highlighted_and_selected_and_also_displays_correct_results_per_selected_filters(DataTable dTable) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that All Bids tab is selected and highlighted, and bid results appear as per filter criteria");

		this.verifySearchResultsDisplayPerFilterResults(dTable);

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Each bid Result validated");
	}
	
	@And("^I will verify that All Bids is both highlighted and selected, and also displays correct results per the displayed Filter Results search filter$")
	public void I_will_verify_that_All_Bids_is_both_highlighted_and_selected_and_also_displays_correct_results_per_the_displayed_Filter_Results_search_filter(){
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that All Bids tab is selected and highlighted, and bid results appear as per filter criteria");

		FilterCriteria filter = GlobalVariables.getStoredObject("filter");
		this.verifySearchResultsDisplayPerFilterResults(filter);
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Each bid Result validated");
	}
    
	@Then ("^I will verify that my unsaved search correctly displays filters in the Filter Results section$")
	public void I_will_verify_that_my_unsaved_search_correctly_displays_filters_in_the_Filter_Results_section() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Filter Results section displays filter search criteria for unsaved search");

		FilterCriteria filter = GlobalVariables.getStoredObject("filter");
		this.verifyUnsavedFilterDisplays(filter);

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Filter Results section displays filter search criteria for unsaved search");
	}
	
	@And ("^I will verify the Filter Results section displays a filter with only my Top Bar search term as the keyword$")
	public void I_will_verify_the_Filter_Results_section_displays_a_filter_with_only_my_Top_Bar_search_term_as_the_keyword() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Filter Results section displays only Top Bar search term as keyword");

		FilterCriteria filter = GlobalVariables.getStoredObject("filter");
		this.verifyUnsavedFilterDisplays(filter);

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Filter Results section displays only Top Bar search term as keyword");
	}

////#Fix for release OCT 7
	@And ("^I will verify the Filter Results section displays a filter with only my Top Bar search term as the keyword after clicking Back to Bid$")
	public void I_will_verify_the_Filter_Results_section_displays_a_filter_with_only_my_Top_Bar_search_term_as_the_keyword_ClickBAckToBid() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Filter Results section displays only Top Bar search term as keyword after clicking BAck to Bid");

		FilterCriteria filter = GlobalVariables.getStoredObject("filter");
		this.verifyUnsavedFilterDisplaysAfterBackToBid(filter);

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Filter Results section displays only Top Bar search term as keyword");
	}
	
	@When("^I run a search on the Filter Results section with a Custom Timeframe$")
	public void I_run_a_search_on_the_Filter_Results_section_with_a_Custom_Timeframe() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Running search with custom time frame of now's date to one month in the future");
		
		// Set a filter with a custom date
		// Bid Start Date = today
		// Bid End Date = one month from now
		FilterCriteria filter = new FilterCriteria();
		filter.setGenericKeywords()
				.setSelectTimeFrame(TimeFrameOptions.CUSTOM_TIME_FRAME)
				.setBidStartDateToToday()
				.setBidEndDateToOneMonthFromToday();

		this.setFilterAndSearch(filter);
		GlobalVariables.storeObject("filter", filter);
	}
	
	@When ("^I run a search on the Filter Results section using the bid number as the keyword$")
	public void I_run_a_search_on_the_Filter_Results_section_using_the_bid_number_as_the_keyword() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Running search on Filter Results using bid number as keyword");

		// Create a filter with the keyword to search for being the bid number of our bid
		BidListRow bid         = GlobalVariables.getStoredObject("randomBid");
		List <String> keywords = new ArrayList<String>();
		FilterCriteria filter  = new FilterCriteria();
		keywords.add(bid.bidInfo.getBidNumber());
		filter.setKeywords(keywords);
		
		this.setFilterAndSearch(filter);
	}
}
