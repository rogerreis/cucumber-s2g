package pages.common.pageObjects;

import static org.junit.Assert.fail;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

/***
 * <h1>Class BasePageActions</h1>
 * @author dmidura
 * <p>details: This class defines methods for pageObject locator and manipulation. Should be inherited.</p>
 */
public class BasePageActions {
	
	// Other
	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;
	public final int DEFAULT_TIMEOUT = Integer.parseInt(System.getProperty("defaultTimeOut"));
	public final int MINIMAL_TIMEOUT = Integer.parseInt(System.getProperty("minimalTimeOut"));
	
	public BasePageActions(EventFiringWebDriver driver) {
		this.driver = driver;
		helperMethods = new HelperMethods();
	}
	
	
	/* ----- Find Methods for Page Objects ----- */
 
    /***
    * <h1>doesPageObjectExist</h1> 
    * <p>purpose: Sometimes a page object may appear onscreen conditionally.
    * Use this method to quickly determine if the page object is currently
    * on the screen</p>
    * @param driver = EventFiringWebDriver
    * @param locator = By locator
    * @return true if visible on screen | false if not
    */
	protected Boolean doesPageObjectExist(EventFiringWebDriver driver, By locator){
		this.setManualTimeOut(driver, 500, TimeUnit.MILLISECONDS);
		List<WebElement> ele = driver.findElements(locator);
		this.setTimeOutForFindElements(driver);
		return !ele.isEmpty();
	} 
	

	/***
	 * <h1>findAllOrNoElements</h1>
	 * <p>purpose: Find all WebElements or no WebElements (will not fail out if no elements are returned)<br>
	 * specifically: List [WebElement] elements = driver.findElements(locator)<br>
	 * Note: This is a useful method when you need to determine the number of elements that each use the same locator
	 * @param driver = EventFiringWebDriver
	 * @param locator = By locator
	 * @return list of WebElements if they exist | empty list of WebElements if none exist
	 */
	protected List <WebElement> findAllOrNoElements(EventFiringWebDriver driver, By locator) {
		this.setTimeOutForFindElements(driver);
		List<WebElement> ele = driver.findElements(locator);
		this.setDefaultTimeOut(driver);
		return ele;
	}
	
	/***
	 * <h1>findByVisibility</h1>
	 * <p>purpose: Verify that the pageObject described by locator is visible on screen within DEFAULT_TIMEOUT of searching.
	 * 	Return the WebElement if visible and found. Fail test if pageObject is not visible or <br>
	 * 	WebElement for pageObject is not found within DEFAULT_TIMEOUT</p>
	 * @param locator = locator to create WebElement for pageObject
	 * @return WebElement
	 */
	protected WebElement findByVisibility(By locator) { return findByVisibility(locator, DEFAULT_TIMEOUT); }

	/***
	 * <h1>findByVisibility</h1>
	 * <p>purpose: Verify that the pageObject described by locator is visible on screen within timeout of searching.
	 * 	Return the WebElement if visible and found. Fail test if pageObject is not visible or <br>
	 * 	WebElement for pageObject is not found within timeout</p>
	 * @param locator = locator to create WebElement for pageObject
	 * @param timeout = how long to search for the pageObject, in seconds
	 * @return WebElement
	 */
	protected WebElement findByVisibility(By locator, int timeout) {
			return new WebDriverWait(driver, timeout)
	        		.withMessage("Test Timed out: Object was not visible after " + timeout + " seconds\n" +
	        					  "object By = \"" + locator.toString() + "\"")
		    		.until(ExpectedConditions.visibilityOfElementLocated(locator)); 
	}

	/***
	 * <h1>findByPresence</h1>
	 * <p>purpose: Verify that the pageObject described by locator is present on screen within DEFAULT_TIMEOUT of searching.
	 * 	Return the WebElement if present and found. Fail test if pageObject is not present or <br>
	 * 	WebElement for pageObject is not found within DEFAULT_TIMEOUT</p>
	 * @param locator = locator to create WebElement for pageObject
	 * @return WebElement
	 */
	protected WebElement findByPresence(By locator) { return this.findByPresence(locator, DEFAULT_TIMEOUT); }

	/***
	 * <h1>findByPresence</h1>
	 * <p>purpose: Verify that the pageObject described by locator is present on screen within timeout of searching.
	 * 	Return the WebElement if present and found. Fail test if pageObject is not present or <br>
	 * 	WebElement for pageObject is not found within timeout</p>
	 * @param locator = locator to create WebElement for pageObject
	 * @param timeout = how long to search for the pageObject, in seconds
	 * @return WebElement
	 */
	protected WebElement findByPresence(By locator, int timeout) {
	        return new WebDriverWait(driver, timeout)
	        		.withMessage("Test Timed out: Object was not present after " + timeout + " seconds\n" +
	        					  "object xpath = \"" + locator.toString() + "\"")
	        		.until(ExpectedConditions.presenceOfElementLocated(locator)); 
	}
	    
	/***
	 * <h1>findByClickability</h1>
	 * <p>purpose: Verify that the pageObject described by locator is clickable on screen within DEFAULT_TIMEOUT of searching.
	 * 	Return the WebElement if clickable and found. Fail test if pageObject is not clickable or <br>
	 * 	WebElement for pageObject is not found within DEFAULT_TIMEOUT</p>
	 * @param locator = locator to create WebElement for pageObject
	 * @return WebElement
	 */
	protected WebElement findByClickability(By locator) { return this.findByClickability(locator, DEFAULT_TIMEOUT); }

	/***
	 * <h1>findByClickability</h1>
	 * <p>purpose: Verify that the pageObject described by locator is clickable on screen within timeout of searching.
	 * 	Return the WebElement if clickable and found. Fail test if pageObject is not clickable or <br>
	 * 	WebElement for pageObject is not found within timeout</p>
	 * @param locator = locator to create WebElement for pageObject
	 * @param timeout = how long to search for the pageObject, in seconds
	 * @return WebElement
	 */
    protected WebElement findByClickability(By locator, int timeout) {
        return new WebDriverWait(driver, timeout)
        		.withMessage("Test Timed out: Object was not clickable (enabled and visible) after " + timeout + " seconds\n" +
        					  "object xpath = \"" + locator.toString() + "\"")
        		.until(ExpectedConditions.elementToBeClickable(locator));
    }
    
    /***
     * <h1>findByScrollIntoViewBottomOfScreen</h1>
     * <p>purpose: After verifying that the pageObject described by locator is present on the screen,<br>
     * 	scroll the screen such that the pageObject is situated in the bottom of the view port.<br>
     * 	Finally, verify that the pageObject is visible and return its WebElement.<br>
     * 	Fail test if pageObject is not initially present on screen, is not visible in view port after<br>
     * 	scroll, or if WebElement for pageObject cannot be found.<br>
     * 	Note: Methods to locate pageObject by presence and then by visibility are subject to DEFAULT_TIMEOUT<br>
     * 	Note: If you don't know which scroll method to use, default to findByScrollIntoViewBottomOfScreen</p>
	 * @param locator = locator to create WebElement for pageObject
     * @return  WebElement
     */
    protected WebElement findByScrollIntoViewBottomOfScreen(By locator) {
    	this.scrollIntoViewBottomOfScreen(locator);
    	return this.findByVisibility(locator);
    }
    
    /***
     * <h1>findByScrollIntoViewBottomOfScreen</h1>
     * <p>purpose: After verifying that the pageObject described by locator is present on the screen,<br>
     * 	scroll the screen such that the pageObject is situated in the bottom of the view port.<br>
     * 	Finally, verify that the pageObject is visible and return its WebElement.<br>
     * 	Fail test if pageObject is not initially present on screen, is not visible in view port after<br>
     * 	scroll, or if WebElement for pageObject cannot be found within timeOut.<br>
     * 	Note: If you don't know which scroll method to use, default to findByScrollIntoViewBottomOfScreen</p>
	 * @param locator = locator to create WebElement for pageObject
	 * @param timeout = how long to search for the pageObject, in seconds
     * @return  WebElement
     */
    protected WebElement findByScrollIntoViewBottomOfScreen(By locator, int timeout) {
    	this.scrollIntoViewBottomOfScreen(locator);
    	return this.findByVisibility(locator, timeout);
    }    
    
    /***
     * <h1>findByScrollIntoViewTopOfScreen</h1>
     * <p>purpose: After verifying that the pageObject described by locator is present on the screen,<br>
     * 	scroll the screen such that the pageObject is situated in the top of the view port.<br>
     * 	Finally, verify that the pageObject is visible and return its WebElement.<br>
     * 	Fail test if pageObject is not initially present on screen, is not visible in view port after<br>
     * 	scroll, or if WebElement for pageObject cannot be found.<br>
     * 	Note: Methods to locate pageObject by presence and then by visibility are subject to DEFAULT_TIMEOUT<br>
     * 	Note: If you don't know which scroll method to use, default to findByScrollIntoViewBottomOfScreen</p>
	 * @param locator = locator to create WebElement for pageObject
     * @return  WebElement
     */
    protected WebElement findByScrollIntoViewTopOfScreen(By locator) {
    	this.scrollIntoViewTopOfScreen(locator);
        return this.findByVisibility(locator);
    }

    /***
     * <h1>findByScrollIntoViewTopOfScreen</h1>
     * <p>purpose: After verifying that the pageObject described by locator is present on the screen,<br>
     * 	scroll the screen such that the pageObject is situated in the top of the view port.<br>
     * 	Finally, verify that the pageObject is visible and return its WebElement.<br>
     * 	Fail test if pageObject is not initially present on screen, is not visible in view port after<br>
     * 	scroll, or if WebElement for pageObject cannot be found within timeout.<br>
     * 	Note: Methods to locate pageObject by presence and then by visibility are subject to DEFAULT_TIMEOUT<br>
     * 	Note: If you don't know which scroll method to use, default to findByScrollIntoViewBottomOfScreen</p>
	 * @param locator = locator to create WebElement for pageObject
	 * @param timeout = how long to search for the pageObject, in seconds
     * @return  WebElement
     */
    protected WebElement findByScrollIntoViewTopOfScreen(By locator, int timeout) {
    	this.scrollIntoViewTopOfScreen(locator);
        return this.findByVisibility(locator, timeout);
    }

	/* ---- Scrolling Methods ----- */

    /***
     * <h1>scrollIntoViewBottomOfScreen</h1>
     * <p>purpose: After verifying that the pageObject described by locator is present on the screen,<br>
     * 	scroll the screen such that the pageObject is situated in the bottom of the view port.<br>
     * 	Fail test if pageObject is not initially present on screen</p>
     * 	Note: Methods to locate pageObject by presence is subject to DEFAULT_TIMEOUT<br>
	 * @param locator = locator to create WebElement for pageObject
     */
    protected void scrollIntoViewBottomOfScreen(By locator) {
    	WebElement element = this.findByPresence(locator);
    	this.scrollIntoViewBottomOfScreen(element);
    }
    
    /***
     * <h1>scrollIntoViewBottomOfScreen</h1>
     * <p>purpose: Scroll the screen such that the WebElement is situated in the bottom of the view port.<br>
	 * @param WebElement = WebElement for pageObject
     */
    protected void scrollIntoViewBottomOfScreen(WebElement element) {
    	// true  = top of the element will be aligned to the top of the visible area of the scrollable ancestor
    	// false = bottom of the element will be aligned to the bottom of the visible area of the scrollable ancestor
    	driver.executeScript("arguments[0].scrollIntoView(false);", element);
    	new WebDriverWait(driver, DEFAULT_TIMEOUT).until(ExpectedConditions.visibilityOf(element));
    }

     /***
     * <h1>scrollIntoViewTopOfScreen</h1>
     * <p>purpose: After verifying that the pageObject described by locator is present on the screen,<br>
     * 	scroll the screen such that the pageObject is situated in the top of the view port.<br>
     * 	Fail test if pageObject is not initially present on screen</p>
     * 	Note: Methods to locate pageObject by presence is subject to DEFAULT_TIMEOUT<br>
	 * @param locator = locator to create WebElement for pageObject
     */
    protected void scrollIntoViewTopOfScreen(By locator) {
    	WebElement element = this.findByPresence(locator);
    	this.scrollIntoViewTopOfScreen(element);
    }
    
    /***
     * <h1>scrollIntoViewTopOfScreen</h1>
     * <p>purpose: Scroll the screen such that the WebElement is situated in the top of the view port.<br>
	 * @param WebElement = WebElement for pageObject
     */
    protected void scrollIntoViewTopOfScreen(WebElement element) {
        // true  = top of the element will be aligned to the top of the visible area of the scrollable ancestor
        // false = bottom of the element will be aligned to the bottom of the visible area of the scrollable ancestor
    	driver.executeScript("arguments[0].scrollIntoView(true);", element);
    	new WebDriverWait(driver, DEFAULT_TIMEOUT).until(ExpectedConditions.visibilityOf(element));
    }
    
	    
	/* ----- Page Object Wait Methods ----- */

    /***
     * <h1>waitForInvisibility</h1>
     * <p>purpose: Verify that pageObject described by locator is no longer visible on screen by DEFAULT_TIMEOUT.<br>
     * 	Fail test if pageObject is still visible after DEFAULT_TIMEOUT </p>
	 * @param locator = locator describing pageObject
     */
	protected void waitForInvisibility(By locator) { this.waitForInvisibility(locator, DEFAULT_TIMEOUT); }
    /***
     * <h1>waitForInvisibility</h1>
     * <p>purpose: Verify that pageObject described by locator is no longer visible on screen by timeout.<br>
     * 	Fail test if pageObject is still visible after timeout </p>
	 * @param locator = locator describing pageObject
	 * @param timeout = how long to search for invisibility of pageObject described by locator, in seconds
     */
	protected void waitForInvisibility(By locator, int timeout) {
	        new WebDriverWait(driver, timeout)
	        		.withMessage("Test Timed out: Object was still visible after " + timeout + " seconds\n" +
	        					  "object xpath = \"" + locator.toString() + "\"")
	        		.pollingEvery(Duration.ofMillis(200))
	        		.until(ExpectedConditions.invisibilityOfElementLocated(locator)); }
	
	/***
     * <h1>waitForVisibility</h1>
     * <p>purpose: Verify that pageObject described by locator is visible on screen by DEFAULT_TIMEOUT.<br>
     * 	Fail test if pageObject is still not visible after DEFAULT_TIMEOUT </p>
	 * @param locator = locator describing pageObject
     */
	protected void waitForVisibility(By locator, int timeout) { this.findByVisibility(locator, timeout);     }
	/***
     * <h1>waitForVisibility</h1>
     * <p>purpose: Verify that pageObject described by locator is visible on screen by timeout.<br>
     * 	Fail test if pageObject is still not visible after timeout </p>
	 * @param locator = locator describing pageObject
	 * @param timeout = how long to search for visibility of pageObject described by locator, in seconds
     */
	protected void waitForVisibility(By locator) { this.findByVisibility(locator, DEFAULT_TIMEOUT);          }

	/***
     * <h1>waitForClickability</h1>
     * <p>purpose: Verify that pageObject described by locator is clickable on screen by DEFAULT_TIMEOUT.<br>
     * 	Fail test if pageObject is not clickable after DEFAULT_TIMEOUT </p>
	 * @param locator = locator describing pageObject
     */
	protected void waitForClickability(By locator) { this.findByClickability(locator, DEFAULT_TIMEOUT);      }
	/***
     * <h1>waitForClickability</h1>
     * <p>purpose: Verify that pageObject described by locator is clickable on screen by timeout.<br>
     * 	Fail test if pageObject is still not clickable after timeout </p>
	 * @param locator = locator describing pageObject
	 * @param timeout = how long to search for clickability of pageObject described by locator, in seconds
     */
    protected void waitForClickability(By locator, int timeout) { this.findByClickability(locator, timeout); }

	/***
     * <h1>waitForPresence</h1>
     * <p>purpose: Verify that pageObject described by locator is present on screen by DEFAULT_TIMEOUT.<br>
     * 	Fail test if pageObject is not present after DEFAULT_TIMEOUT </p>
	 * @param locator = locator describing pageObject
     */
	protected void waitForPresence(By locator) { this.findByPresence(locator, DEFAULT_TIMEOUT);              }
	/***
     * <h1>waitForPresence</h1>
     * <p>purpose: Verify that pageObject described by locator is present on screen by timeout.<br>
     * 	Fail test if pageObject is still not present after timeout </p>
	 * @param locator = locator describing pageObject
	 * @param timeout = how long to search for presence of pageObject described by locator, in seconds
     */
	protected void waitForPresence(By locator, int timeout) { this.findByPresence(locator, timeout);         }
	
	/* ------ Window methods ----- */
	
	/***
	 * <h1>waitForNumberOfWindowsToEqual</h1>
	 * <p>purpose: Wait for the number of windows to equal numberOfWindows.<br>
	 * 	Fail test if this does not happen within timeout</p>
	 * @param numberOfWindows = number of expected windows
	 * @param timeout = timeout, in seconds
	 */
	protected void waitForNumberOfWindowsToEqual(int numberOfWindows, int timeout) {
        new WebDriverWait(driver, timeout) {
        }.until((ExpectedCondition<Boolean>) driver1 -> (driver1.getWindowHandles().size() == numberOfWindows));
    }
    
    /***
     * <h1>switchToWindow</h1>
     * <p>purpose: Assuming that you have multiple tabs open, switch to the window<br>
     * 	containing windowTitle in its title. Fail test if window is not available</p>
     * @param windowTitle = partial or full title of the window you want to switch to
     * @return BasePageActions
     */
    public BasePageActions switchToWindow(String windowTitle) {
    	helperMethods.addSystemWait(2);
        for(String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);
            System.out.printf(GlobalVariables.getTestID() + " Window title: %s\n", driver.getTitle());
            if(driver.getTitle().contains(windowTitle)) {
                return this;
            }
        }
        fail(String.format("No such window '%s'", windowTitle));
        return null;
    }
    
    /***
     * <h1>closeWindow</h1>
     * <p>purpose: From your current browser tab, switch
     * 	to windowTitle and then close that tab. Return
     * 	to the current tab.<br>
     * 	Note 1: If you only have one tab open,
     * 	and you close that tab, then you just killed your test!<br>
     * 	Note 2: Test will fail if windowTitle isn't visible from
     * 	the available tabs
     * @param windowTitle = name of tab to close
     * @return BasePageActions
     */
    public BasePageActions closeWindow(String windowTitle) {
    	if (this.isWindowVisible(windowTitle)) {
    		fail("ERROR: \"" + windowTitle + "\" is not available to close"); }

    	String currentWindow = driver.getTitle();
    	this.switchToWindow(windowTitle);
    	driver.close();

    	// Return to original window, if the original window wasn't just closed
    	if (!windowTitle.contains(currentWindow)) { this.switchToWindow(currentWindow); }
    	return this;
    }
    
    /***
     * <h1>isWindowVisible</h1>
     * <p>purpose: Determine if one of the open tabs contains windowTitle in its name
     * @param windowTitle = name of tab you're looking for
     * @return true if windowTitle is an open tab | false if windowTitel is not an open tab
     */
    public boolean isWindowVisible(String windowTitle) {
    	boolean bReturn = false;
    	String currentWindow = driver.getTitle();

    	// Is windowTitle visible?
        for(String handle : driver.getWindowHandles()) {
            driver.switchTo().window(handle);
            if(driver.getTitle().contains(windowTitle)) {bReturn = true; }
        }
        
        // Return to original window
        this.switchToWindow(currentWindow);
        return bReturn;
    }

   	/* ----- private class helpers ----- */
	/***
	 * <h1>setManualTimeOut</h1>
	 * <p>purpose: Use this to manually set the system timeout</p>
	 * @param driver = EventFiringWebDriver
	 * @param timeOut = time out duration
	 * @param timeUnit = TimeUnit for timeOut
	 * @return None
	 */
	 private void setManualTimeOut(EventFiringWebDriver driver, Integer timeOut, TimeUnit timeUnit ) {
		 driver.manage().timeouts().implicitlyWait(timeOut, timeUnit);
	}
	
	/***
	 * <h1>setTimeOutForFindElements</h1>
	 * <p>purpose: Use this method to set the timeout before running a findElements() call in Selenium.<br>
	 * 	This method will change the system timeout to a standard timeout (currently 3 seconds).<br>
	 *  Be sure to run a setDefaultTimeout() immediately after completing the findElements() to<br>
	 *  return the system timeout to the default (as specified in config.json)</p>
	 * @param driver = EventFiringWebDriver
	 * @return None
	 */
	 private void setTimeOutForFindElements (EventFiringWebDriver driver) {
		setManualTimeOut(driver, 3, TimeUnit.SECONDS);
	}
	
	
	/***
	 * <h1>setDefaultTimeOut</h1>
	 * <p>purpose: Reset the system timeout to its default value (as specified in the config.json)</p>
	 * @param driver = EventFiringWebDriver
	 * @return None
	 */
	 private void setDefaultTimeOut(EventFiringWebDriver driver) {
		Integer timeOut    = Integer.valueOf(System.getProperty("defaultTimeOut"));
		this.setManualTimeOut(driver, timeOut, TimeUnit.SECONDS);
	}    
    
}

