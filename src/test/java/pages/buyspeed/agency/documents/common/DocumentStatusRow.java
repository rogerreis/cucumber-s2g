package pages.buyspeed.agency.documents.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class DocumentStatusRow</h1>
 * @author dmidura
 * <p>details: This class holds page objects and methods for single row in a DocumentStatusPopup
 */
public class DocumentStatusRow extends BasePageActions {
	
	// Row elements
	private String statusDate;
	private String level;
	private String item;
	private String majorStatus;
	private String user;
	private String row_xpath;
	
	// xpaths
	// dmidura: yah, these are the best locators we have. but it's bso, so
	// what did I expect? sigh.
	private String statusDate_xpath  = "//td[1]";
	private String level_xpath       = "//td[2]";
	private String item_xpath        = "//td[3]";
	private String majorStatus_xpath = "//td[4]";
	private String user_xpath        = "//td[5]";
	
	public DocumentStatusRow(EventFiringWebDriver driver, String row_xpath) {
		super(driver);
		this.row_xpath = row_xpath;

		this.statusDate  = this.getStatusDate().getText();
		this.level       = this.getLevel().getText();
		this.item        = this.getItem().getText();
		this.majorStatus = this.getMajorStatus().getText();
		this.user        = this.getUser().getText();
	}
	
	/* ----- getters ----- */

	private WebElement getStatusDate()  { return findByVisibility(By.xpath(this.row_xpath + this.statusDate_xpath));  }	
	private WebElement getLevel()       { return findByVisibility(By.xpath(this.row_xpath + this.level_xpath));       } 
	private WebElement getItem()        { return findByVisibility(By.xpath(this.row_xpath + this.item_xpath));        } 
	private WebElement getMajorStatus() { return findByVisibility(By.xpath(this.row_xpath + this.majorStatus_xpath)); } 
	private WebElement getUser()        { return findByVisibility(By.xpath(this.row_xpath + this.user_xpath));        } 

	/* ----- methods ----- */

	/**
	 * <h1>getStatusDateAsCalendar()</h1>
	 * <p>purpose: Return text in the "Status Date" column as a Calendar object</p>
	 * @return Calendar
	 */
	public Calendar getStatusDateAsCalendar() {
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		Calendar cal = Calendar.getInstance();

		try {
			cal.setTime(sdf.parse(this.statusDate));
		} catch (ParseException e) {
			e.printStackTrace(); }

		return cal;
	}

	/**
	 * <h1>getLevelText()</h1>
	 * <p>purpose: Return text in the "Level" column</p>
	 * @return String
	 */
	public String getLevelText() {
		return this.level;
	}
	
	/**
	 * <h1>getItemText()</h1>
	 * <p>purpose: Return text in the "Item" column</p>
	 * @return String
	 */
	public String getItemText() {
		return this.item;
	}

	/**
	 * <h1>getMajorStatusText()</h1>
	 * <p>purpose: Return text in the "Major Status" column</p>
	 * @return String
	 */
	public String getMajorStatusText() {
		return this.majorStatus;
	}
	
	/**
	 * <h1>getUserText()</h1>
	 * <p>purpose: Return text in the "User" column</p>
	 * @return String
	 */
	public String getUserText() {
		return this.user;
	}
}
