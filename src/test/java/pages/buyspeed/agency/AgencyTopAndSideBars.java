package pages.buyspeed.agency;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.buyspeed.common.TopSearchBar;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class AgencyTopAndSideBars</h1>
 * @author dmidura
 * <p>details: This class houses access to the agency navigation. All agency classes should inherit this class.
 * <p>Includes: Top nav menus, search field, document creation, side bar
 */
public class AgencyTopAndSideBars extends BasePageActions {

	public AgencyTopAndSideBars(EventFiringWebDriver driver) {
		super(driver);
		searchBar = new TopSearchBar(driver);
	}
	
	// Agency user has the top nav menus
	// Search field
	// Document creation
	// Side menus
	private TopSearchBar searchBar;
	
	/***
	 * <h1>getTopSeachBar</h1>
	 * <p>purpose: Access the Top Search Bar
	 * @return TopSearchBar
	 */
	public TopSearchBar getTopSearchBar() {
		return this.searchBar;
	}

}
