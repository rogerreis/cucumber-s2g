package stepDefinitions.bidSyncPro.supplier.common;

import core.CoreAutomation;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import pages.bidSyncPro.supplier.common.AccountDropDown;
import pages.bidSyncPro.supplier.common.AccountDropdownOptions;
import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.bidSyncPro.supplier.common.CommonTopBarMenuNames;
import pages.bidSyncPro.supplier.common.CompanySettingsDropDown;
import pages.bidSyncPro.supplier.common.CompanySettingsDropdownOptions;
import pages.bidSyncPro.supplier.common.Marketplace;
import pages.bidSyncPro.supplier.companySettings.agencyInteractions.AgencyInteraction;
import pages.bidSyncPro.supplier.companySettings.companyProfile.CompanyProfile;
import pages.bidSyncPro.supplier.companySettings.manageSubscriptions.ManageSubscriptions;
import pages.bidSyncPro.supplier.companySettings.manageUsers.ManageUsers;
import pages.bidSyncPro.supplier.dashboard.BidListTabNames;
import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

import static org.junit.Assert.fail;

import org.openqa.selenium.support.events.EventFiringWebDriver;

public class TopBarFunctionality_stepDefinitions {
	private EventFiringWebDriver driver;
    
	public TopBarFunctionality_stepDefinitions(CoreAutomation automation) {
		this.driver     = automation.getDriver();
	}
	
	/* ----- helpers ----- */

	/***
	 * <h1>openAccount</h1>
	 * <p>purpose: Click on the Top Bar "Account" menu to open/view
	 * @return AccoutnDropDown
	 */
	private AccountDropDown openAccount() {
		return new CommonTopBar(driver)
	    		.waitForPageLoad()
	    		.openAccountDropDown();
	}

	/***
	 * <h1>openCompanySettings</h1>
	 * <p>purpose: Click on the Top Bar "Company Settings" menu to open/view
	 * @return CompanySettingsDropDown
	 */
	private CompanySettingsDropDown openCompanySettings() {
	    return new CommonTopBar(driver)
	    	.waitForPageLoad()
        	.openCompanySettingsDropDown();
	}

    /* ------------ Test Steps ---------------- */

	@When("^I click Account$")
	public void I_click_Account() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking the Accounts top menu button");
		this.openAccount();
	}

    @Given("^I click on Company Settings$")
	public void iClickOnCompanySettings() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking the Company Settings top menu button");
    	this.openCompanySettings();
	}

	@When("^I click on the All Bids Header Button$")
	public void I_click_on_the_All_Bids_Header_Button() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking the All Bids Header Button");
		new DashboardCommon(driver).openAllBidsTab();
	}
	
	@When ("^I open the top bar nav menu \"(.*)\"$")
	public void I_open_the_top_bar_nav_menu(String strNavButton) {
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Opening \"%s\" top nav menu\n", strNavButton);
		switch(CommonTopBarMenuNames.fromString(strNavButton)) {
            case BID_LIST:
                new CommonTopBar(driver).openBidListDropDown();
                break;
            case COMPANY_SETTINGS:
            	this.openCompanySettings();
                break;
            case ACCOUNT:
            	this.openAccount();
                break;
            case MARKET_PLACE:
            	new Marketplace(driver).clickOnMarketplaceIcon();
                break;
            default:
                String msg = String.format("ERROR: Unrecognized navigation button '%s'.\n", strNavButton);
                fail(msg);
        }
	}

    @When("^I have navigated to the \"(.*)\" bid tab$")
    public void iHaveNavigatedToTheGivenBidTab(String tabName) {
        System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Navigating to the \"%s\" bid tab\n", tabName);
        new DashboardCommon(driver)
        	.openBidTab(BidListTabNames.fromString(tabName))
        	.verifyTabIsHighlightedAndSelected(BidListTabNames.fromString(tabName))
        	.waitForSpinner(driver);
        new HelperMethods().addSystemWait(3);
    }
    
    @And ("^I have navigated to Company Settings > \"(.*)\"$")
    public void I_have_navigated_to_Company_Settings_select_option(String selection) throws InterruptedException {
        System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Navigating to the Company Settings > \"%s\"  page\n", selection);
    	
		switch (CompanySettingsDropdownOptions.fromString(selection)) {
		case AGENCY_INTERACTION:
			this.openCompanySettings().openAgencyInteraction();
			break;
		case MANAGE_SUBSCRIPTIONS:
			this.openCompanySettings().openManageSubscriptions();
			break;
		case MANAGE_USERS:
			this.openCompanySettings().openManageUsers().waitOnUsersTableToLoad();
			break;
		case COMPANY_PROFILE:
			this.openCompanySettings().openCompanyProfile();
			break;
		default:
			String msg = String.format("ERROR: Unrecognized option \"%s\" for Company Settings menu.\n", selection);
			fail(msg);
		}
    }

    @And ("^I have navigated to Account > \"(.*)\"$")
    public void I_have_navigated_to_Account_select_option(String selection) throws InterruptedException {
        System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Navigating to the Account > \"%s\"  page\n", selection);
    	
		switch (AccountDropdownOptions.fromString(selection)) {
		case BID_PROFILE:
			this.openAccount().clickBidProfile();
			break;
		case MY_INFO:
			this.openAccount().clickMyInfo();
			break;
		case TRAINING:
			this.openAccount().clickTraining();
			break;
		case SUPPORT:
			this.openAccount().clickSupport();
			break;
		case ACCOUNT_SETTINGS:
			this.openAccount().clickAccountSettings();
			break;
		case LOGOUT:
			this.openAccount().clickLogout();
			break;
		default:
			String msg = String.format("ERROR: Unrecognized option \"%s\" for Account menu.\n", selection);
			fail(msg);
		}
    }

	@Then ("^I will verify that the Supplier Home Screen Header Elements correctly display MouseOver actions$")
	public void I_will_verify_that_the_Supplier_Home_Screen_Header_Elements_correctly_display_MouseOver_actions() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that the Supplier Dashboard Screen Header Elements correctly display MouseOver actions");
		new CommonTopBar(driver).verifyToolTipsAppearForNavMenu();
	
		System.out.println(GlobalVariables.getTestID() + " Test Passed: All Supplier Dashboard Screen Header Elements correctly display MouseOver actions.");
	}
	
	@Then ("^I will verify that the options and icons for \"(.*)\" nav menu display as expected in the dropdown$")
	public void I_will_verify_that_options_and_icons_for_nav_menu_display_as_expected_in_the_dropdown (String strNavButton) {
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that the Supplier Dashboard Menu dropdown for \"%s\" Nav Button contains all expected elements.\n", strNavButton);
		new CommonTopBar(driver).verifyNavMenuOptions(CommonTopBarMenuNames.fromString(strNavButton));
	
		System.out.printf(GlobalVariables.getTestID() + " Test Passed: Navigation dropdown for \"%s\" Nav Button contains all expected elements.\n", strNavButton);
	}
	
}
