package pages.bidSyncPro.supplier.companySettings;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.bidSyncPro.supplier.companySettings.agencyInteractions.AgencyInteraction;
import pages.bidSyncPro.supplier.companySettings.manageSubscriptions.ManageSubscriptions;
import pages.bidSyncPro.supplier.companySettings.manageUsers.ManageUsers;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;

public class CompanySettingsCommon extends CommonTopBar {
    
    // xpaths
    private final String backToBidsButton_xpath  = "//button[contains(@aria-label, 'navigate to bids')]";
    
    // ids
    private final String companyProfile_id       = "AdminProfile_0";
    private final String manageUsers_id          = "AdminProfile_1";
    private final String manageSubscriptions_id  = "AdminProfile_2";
    private final String agencyInteraction_id    = "AdminProfile_3";
    
    public CompanySettingsCommon(EventFiringWebDriver driver) {
        super(driver);
    }
    
    /* ----- getters ----- */
    private WebElement getBackToBidsButton()    { return findByVisibility(By.xpath(this.backToBidsButton_xpath)); }
    private WebElement getCompanyProfile()      { return findByVisibility(By.id(this.companyProfile_id));         }
    private WebElement getManageUsers()         { return findByVisibility(By.id(this.manageUsers_id));            }
    private WebElement getManageSubscriptions() { return findByVisibility(By.id(this.manageSubscriptions_id));    }
    private WebElement getAgencyInteraction()   { return findByVisibility(By.id(this.agencyInteraction_id));      }
    
    
    /* ----- methods ----- */
    public BidList goBackToBids() {
        getBackToBidsButton().click();
        return new BidList(driver);
    }
    
    public void openCompanyProfileTab() {
        getCompanyProfile().click();
        // TODO: return new CompanyProfile(driver);
    }
    
    public ManageUsers openManageUsersTab() {
        getManageUsers().click();
        return new ManageUsers(driver);
    }
    
    public ManageSubscriptions openManageSubscriptionsTab() {
        getManageSubscriptions().click();
        return new ManageSubscriptions(driver);
    }
    
    public AgencyInteraction openAgencyInteractionTab() {
        getAgencyInteraction().click();
        return new AgencyInteraction(driver);
    }
}
