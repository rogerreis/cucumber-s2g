@must @administration @level2
Feature: Integrations > Pro/Classic BidSync - Recovery from Downed BidSync Classic System
	BIDSYNC-530: Punching out to Classic failing

@BIDSYNC-530 
Scenario: Verify recovery from downed BidSync Classic system
	1. When Pro account is created and user logs in, Classic account is created and linked
	2. When Pro user logs in, then if no linkage exists to Classic account, 
	   Pro account links to Classic account with same email
	3. With linkage, user can punchout to bid details of BidSync bid without requiring a login
	   to BidSync Classic
	Note: The filter results bid search is expected to return a BidSync Agency bid.
	      This test will need to be updated when the bid expires in Pro.

	Given I have registered a random user
	And I logout of BidSync Pro
	And I verify that my user was created in Classic
	And I log out of classic
	And I retrieve my user_link_id from the ARGO database
	When I delete my users_link from the ARGO database
	And I navigate my browser to the supplier login page	
	And I log into BidSync Pro as my new BidSync Basic user
	Then I will verify that my user_link_id has been recreated
	And I run a search on the Filter Results section with filters:
	| Filter Name      | BidSync Bid 	       		|			|
	| Keywords         | Smitha Classic Test Bid #6	|	|
	| States/Provinces | Minnesota      			|       	|
	Then I will verify that I can punchout to the BidSync bid
