package pages.bidSyncPro.supplier.dashboard.filterResultsSection;

import static org.junit.Assert.fail;

/***
 * <h1>Enum TimeFrameOptions</h1>
 * @author dmidura
 * <p>details: This enum defines the available time frame options (i.e. what the user can select from<br>
 * 	the "Select Time Frame" dropdown under the Bid End Date section of the Filter Results Collapsible section</p>
 */
public enum TimeFrameOptions {
		PAST_6_MONTHS, 
		PAST_YEAR, 
		PAST_3_YEARS, 
		CUSTOM_TIME_FRAME,
		NONE;
	
	/***
	 * <h1>fromString</h1>
	 * <p>purpose: Given text that matches the "SELECT TIME FRAME"<br>
	 * 	from the "Bid End Date" section of the Filter Results section<br>
	 *  return the enum value of the TimeFrameOptions</p>
	 * @param timeFrame<br>
	 * 	= "Past 6 Months" | "Past Year" | "Past 3 Years" | <br>
	 *  "Custom Time Frame" | "None"</p>
	 * @return TimeFrameOptions
	 */
	public static TimeFrameOptions fromString(String timeFrame) {
		if (timeFrame.equalsIgnoreCase("Past 6 Months")) {
			return PAST_6_MONTHS;
		} else if (timeFrame.equalsIgnoreCase("Past Year")) {
			return PAST_YEAR;
		} else if (timeFrame.equalsIgnoreCase("Past 3 Years")) {
			return PAST_3_YEARS;
		} else if (timeFrame.equalsIgnoreCase("None")) {
			return NONE;
		} else if (timeFrame.equalsIgnoreCase("Custom Time Frame")) {
			return CUSTOM_TIME_FRAME;
		} else {
			fail("ERROR: Time frame \"" + timeFrame + "\" is an invalid option to TimeFrameOptions");
			return null;
		}
	}
	
	@Override
	public String toString() {
		if (this.equals(TimeFrameOptions.PAST_6_MONTHS)) {
			return "Past 6 Months";
		} else if (this.equals(TimeFrameOptions.PAST_YEAR)) {
			return "Past Year";
		} else if (this.equals(TimeFrameOptions.PAST_3_YEARS)) {
			return "Past 3 Years";
		} else if (this.equals(TimeFrameOptions.NONE)) {
			return "None";
		} else if (this.equals(TimeFrameOptions.CUSTOM_TIME_FRAME)) {
			return "Custom Time Frame";
		} else {
			fail("ERROR: Time frame \"" + this + "\" is an invalid option to TimeFrameOptions");
			return null;
		}
	}
}

