@must @ManageUsers @subscriptions @level4
Feature: Company Settings > Management Users - Cancelling Subscriptions Reverts Licensed Users, Regional Plan
	BIDSYNC-22: "Subscription Plans Cancelled/Expired - Turning off subscription plans for additional user licenses of Regional Plan"

dmidura: Per 12.10.2018 discussion with jbarlow, during registration,  Federal and Military addOn packages are correlated to
the user's subscription plan (i.e. Federal addOn to National sees all states, but Federal addOn to KS State plan will only see the KS Federal bids)
Canada addOn is for all Canadian provinces. This behavior may change in the future, in which case the BidListRow.verifyBidAccessIsCorrect()
method should be updated.


@BIDSYNC-22
Scenario Outline: After company owner subscription is cancelled, all license holders are cancelled and revert to Basic accounts

	Given I register a new company owner with a "<subscriptionType>" subscription plan, with add-ons and some licenses
	And I logout of BidSync Pro
	And I register a new user with a BidSync Basic Plan from the Masonry website
	And the new user joins the new company with available subscription licenses
	And the new user is assigned a subscription license by the company owner
	And I logout of BidSync Pro
	And the new user will be able to view bids per their newly assigned license
	When the Chargebee admin cancels the subscription for the company owner
	And I wait "10" seconds for the backend to catch up
	Then I will verify that the company owner only has a "BidSync Basic" subscription plan listed in the Chargebee test site 
	And I will verify that the company owner only has a "BidSync Basic" subscription plan listed in the ARGO database
	And I will verify that my user only has a "BidSync Basic" subscription plan listed in the Chargebee test site 
	And I will verify that my user only has a "BidSync Basic" subscription plan listed in the ARGO database
	When I navigate my browser to the supplier login page
	And I log into BidSync Pro as my new BidSync Basic user
	And I have navigated to the "All Bids" bid tab
	Then the new user will only have access to BidSync Basic bids


	Examples:
	| subscriptionType           |
	| BidSync Regional - Yearly  |
	| BidSync Regional - Monthly |
	| BidSync Regional - 6 Month |
