package pages.buyspeed.seller;

import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.GlobalVariables;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Class BuySpeedLogin</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects related to the BuySpeed Seller home screen</p>
 */
public class SellerHome extends BasePageActions {

	// xpaths
	// Panel Elements
	private final String welcomeTxt_xpath                  = "//td[contains(text(), 'Home - Welcome Back')]";

	// Bottom Panel
	private final String registerWithBidSyncBtn_xpath      = "//button[@id=\"registerWithBidSyncBtn\"]";
		
	// Register with BidSync popup
	private final String registrationPopup_id           = "registerHeader";
	private final String bidSyncLinksLogo_xpath            = "//div[@aria-labelledby=\"ui-id-2\"]//img[@src=\"images/bidsync_logo.svg\"]";
	private final String closeBtn_xpath                    = "//div[@aria-labelledby=\"ui-id-2\"]//button[@title=\"close\"]";
	private final String registerWithBidSyncTxt_xpath      = "//div[@aria-labelledby=\"ui-id-2\"]//h1[contains(.,\"Register With BidSync\")]";
	private final String registerWithBidSyncPopupBtn_xpath = "//div[@aria-labelledby=\"ui-id-2\"]//input[@class=\"button\" and @value=\"Click to Register\"]";

	// Other 
	private String strPagePath = System.getProperty("buyspeedURL") + "seller/sellerHome.sdo";

	public SellerHome (EventFiringWebDriver driver) {
		super(driver);
	}

	/* ----- getters ----- */
	
	// Panel Elements
	private WebElement getWelcomeTxt()                  { return findByVisibility(By.xpath(this.welcomeTxt_xpath));                  }
	
	// Bottom panel
	private WebElement getRegisterWithBidSyncBtn()      { return  findByVisibility(By.xpath(this.registerWithBidSyncBtn_xpath));     } 

	// Register with BidSync popup
	private WebElement getRegistrationPopup()           { return findByVisibility(By.id(this.registrationPopup_id));           } 
	private WebElement getBidSyncLinksLogo()            { return findByVisibility(By.xpath(this.bidSyncLinksLogo_xpath));            } 
	private WebElement getCloseBtn()                    { return findByVisibility(By.xpath(this.closeBtn_xpath));                    } 
	private WebElement getRegisterWithBidSyncTxt()      { return findByVisibility(By.xpath(this.registerWithBidSyncTxt_xpath));      } 
	private WebElement getRegisterWithBidSyncPopupBtn() { return findByVisibility(By.xpath(this.registerWithBidSyncPopupBtn_xpath)); } 
	
	/* ----- methods ----- */
	
	/***
	 * <h1>waitForPageLoad</h1>
	 * <p>purpose: Wait for seller home screen</p>
	 * @return
	 */
	public SellerHome waitForPageLoad() {
		this.getWelcomeTxt();
		return this;
	}

	/***
	 * <h1>clickOnBuySpeedButton</h1>
	 * <p>purpose: Click on a "Register with BidSync" button to view popup</p>
	 */	
	//public RegisterWithBidSyncPopup clickOnRegisterWithBidSyncBtn() {
	public void clickOnRegisterWithBidSyncBtn() {
		this.getRegisterWithBidSyncBtn().click();

		// TODO dmidura: todo if we ever do more buyspeed integration work
		//return new RegisterWithBidSyncPopup(driver);
	}

	/**
	 * <h1>getBuySpeedLoginPageURL</h1>
	 * <p>purpose: Get the URL for the BuySpeed Seller Home page</p>
	 * @return String = URL of the BuySpeed Seller Home Page
	 */
	public String getPageURL() {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Getting the Page Path = %s\n", strPagePath);
		return strPagePath;
	}
	
	/**
	 * <h1>isBidSyncRegistrationPopupVisible</p>
	 * <p>purpose: Determine if the "Register with BidSync" popup is currently visible on the bso Seller Home Screen</p>
	 * @return true if visible | false if not visible
	 */
	private Boolean isBidSyncRegistrationPopupVisible() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Determining if the \"Register with BidSync\" popup is visible on the BuySpeed Seller Home Screen");
		//return PageElements.get("RegistrationPopup").getCssValue("display").equals("block");
		return this.getRegistrationPopup().getCssValue("display").equals("block");
	}
	
	/* ----- verifications ----- */
	
	/***
	 * </h1>verifyBidSyncRegistrationPopupIsDisplayed</h1>
	 * <p>purpose: Verify that the "Register With BidSync" popup<br>
	 * 	is currently displaying. Fail test if it is not</p>
	 * @return SellerHome
	 */
	public SellerHome verifyBidSyncRegistrationPopupIsDisplayed() {
		if (!this.isBidSyncRegistrationPopupVisible()) {
			fail("Test Failed: \"Register With BidSync\" popup is not currently visible"); }
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"Register With BidSync\" popup is currently visible");
		return this;
	}
	
	
}