package pages.bidSyncPro.supplier.dashboard.filterResultsSection;

import java.lang.reflect.InvocationTargetException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.helpers.GeographyHelpers;

/***
 * <h1>Class StatesProvincesDropdown</h1>
 * @author dmidura
 * <p>details: This class houses the objects and methods for the States/Provinces dropdown on the Filter Results<br>
 * 	This dropdown becomes visible under the States/Provinces section after the "Search all states/provinces" checkbox<br>
 * 	has been unchecked.</p>
 */
@SuppressWarnings("unused")
public class StatesProvincesDropdown extends StatesProvincesSection {
	
	// Other
	private EventFiringWebDriver driver;
	private GeographyHelpers geographyHelpers;

	// xpaths
	// State Name
	private final String alberta_xpath                     = "//mat-option[contains(span,'Alberta')]";
	private final String alabama_xpath                     = "//mat-option[contains(span, 'Alabama')]";
	private final String alaska_xpath                      = "//mat-option[contains(span, 'Alaska')]"; 
	private final String americanSamoa_xpath               = "//mat-option[contains(span, 'American Samoa')]";
	private final String arizona_xpath                     = "//mat-option[contains(span, 'Arizona')]"; 
	private final String arkansas_xpath                    = "//mat-option[contains(span, 'Arkansas')]";
	private final String britishColumbia_xpath             = "//mat-option[contains(span, 'British Columbia')]";
	private final String california_xpath                  = "//mat-option[contains(span, 'California')]";
	private final String colorado_xpath                    = "//mat-option[contains(span, 'Colorado')]";
	private final String connecticut_xpath                 = "//mat-option[contains(span, 'Connecticut')]";
	private final String delaware_xpath                    = "//mat-helperMethodsoption[contains(span, 'Delaware')]";
	private final String districtOfColumbia_xpath          = "//mat-option[contains(span, 'District Of Columbia')]";
	private final String federatedStatesOfMicronesia_xpath = "//mat-option[contains(span, 'Federated States Of Micronesia')]";
	private final String florida_xpath                     = "//mat-option[contains(span, 'Florida')]";
	private final String georgia_xpath                     = "//mat-option[contains(span, 'Georgia')]";
	private final String guam_xpath                        = "//mat-option[contains(span, 'Guam')]";
	private final String hawaii_xpath                      = "//mat-option[contains(span, 'Hawaii')]";
	private final String idaho_xpath                       = "//mat-option[contains(span, 'Idaho')]";
	private final String illinois_xpath                    = "//mat-option[contains(span, 'Illinois')]";
	private final String indiana_xpath                     = "//mat-option[contains(span, 'Indiana')]";
	private final String iowa_xpath                        = "//mat-option[contains(span, 'Iowa')]";
	private final String kansas_xpath                      = "//mat-option[contains(span, 'Kansas')]";
	private final String kentucky_xpath                    = "//mat-option[contains(span, 'Kentucky')]";
	private final String louisiana_xpath                   = "//mat-option[contains(span, 'Louisiana')]";
	private final String maine_xpath                       = "//mat-option[contains(span, 'Maine')]";
	private final String manitoba_xpath                    = "//mat-option[contains(span, 'Manitoba')]";
	private final String marshallIslands_xpath             = "//mat-option[contains(span, 'Marshall Islands')]";
	private final String maryland_xpath                    = "//mat-option[contains(span, 'Maryland')]";
	private final String massachusetts_xpath               = "//mat-option[contains(span, 'Massachusetts')]";
	private final String michigan_xpath                    = "//mat-option[contains(span, 'Michigan')]";
	private final String minnesota_xpath                   = "//mat-option[contains(span, 'Minnesota')]";
	private final String mississippi_xpath                 = "//mat-option[contains(span, 'Mississippi')]";
	private final String missouri_xpath                    = "//mat-option[contains(span, 'Missouri')]";
	private final String montana_xpath                     = "//mat-option[contains(span, 'Montana')]";
	private final String nebraska_xpath                    = "//mat-option[contains(span, 'Nebraska')]";
	private final String nevada_xpath                      = "//mat-option[contains(span, 'Nevada')]";
	private final String newBrunswick_xpath                = "//mat-option[contains(span, 'New Brunswick')]";
	private final String newHampshire_xpath                = "//mat-option[contains(span, 'New Hampshire')]";
	private final String newJersy_xpath                    = "//mat-option[contains(span, 'New Jersey')]";
	private final String newMexico_xpath                   = "//mat-option[contains(span, 'New Mexico')]";
	private final String newYork_xpath                     = "//mat-option[contains(span, 'New York')]";
	private final String newfoundlandAndLabrador_xpath     = "//mat-option[contains(span, 'Newfoundland and Labrador')]";
	private final String northCarolina_xpath               = "//mat-option[contains(span, 'North Carolina')]";
	private final String northDakota_xpath                 = "//mat-option[contains(span, 'North Dakota')]";
	private final String northernMarianaIslands_xpath      = "//mat-option[contains(span, 'Northern Mariana Islands')]";
	private final String novaScotia_xpath                  = "//mat-option[contains(span, 'Nova Scotia')]";
	private final String northwestTerritories_xpath        = "//mat-option[contains(span, 'Northwest Territories')]";
	private final String nunavut_xpath                     = "//mat-option[contains(span, 'Nunavut')]";
	private final String ohio_xpath                        = "//mat-option[contains(span, 'Ohio')]";
	private final String oklahoma_xpath                    = "//mat-option[contains(span, 'Oklahoma')]";
	private final String ontario_xpath                     = "//mat-option[contains(span, 'Ontario')]";
	private final String oregon_xpath                      = "//mat-option[contains(span, 'Oregon')]";
	private final String palau_xpath                       = "//mat-option[contains(span, 'Palau')]";
	private final String pennsylvania_xpath                = "//mat-option[contains(span, 'Pennsylvania')]";
	private final String princeEdwardIsland_xpath          = "//mat-option[contains(span, 'Prince Edward Island')]";
	private final String puertoRico_xpath                  = "//mat-option[contains(span, 'Puerto Rico')]";
	private final String quebec_xpath                      = "//mat-option[contains(span, 'Quebec')]";
	private final String rhodeIsland_xpath                 = "//mat-option[contains(span, 'Rhode Island')]";
	private final String saskatchewan_xpath                = "//mat-option[contains(span, 'Saskatchewan')]";
	private final String southCarolina_xpath               = "//mat-option[contains(span, 'South Carolina')]";
	private final String southDakota_xpath                 = "//mat-option[contains(span, 'South Dakota')]";
	private final String tennessee_xpath                   = "//mat-option[contains(span, 'Tennessee')]";
	private final String texas_xpath                       = "//mat-option[contains(span, 'Texas')]";
	private final String utah_xpath                        = "//mat-option[contains(span, 'Utah')]";
	private final String vermont_xpath                     = "//mat-option[contains(span, 'Vermont')]";
	private final String virginIslands_xpath               = "//mat-option[contains(span, 'Virgin Islands')]";
	private final String virginia_xpath                    = "//mat-option[contains(span, 'Virginia')]";
	private final String washington_xpath                  = "//mat-option[contains(span, 'Washington')]";
	private final String westVirginia_xpath                = "//mat-option[contains(span, 'West Virginia')]";
	private final String wisconsin_xpath                   = "//mat-option[contains(span, 'Wisconsin')]";
	private final String wyoming_xpath                     = "//mat-option[contains(span, 'Wyoming')]";
	private final String yukon_xpath                       = "//mat-option[contains(span, 'Yukon')]";

	public StatesProvincesDropdown(EventFiringWebDriver driver) {
		super(driver);
		this.driver      = driver;
		geographyHelpers = new GeographyHelpers();
	}

	/* ----- getters ----- */
	private WebElement getAlberta()                     {return findByPresence(By.xpath(this.alberta_xpath));                    }
	private WebElement getAlabama()                     {return findByPresence(By.xpath(this.alabama_xpath));                    }
	private WebElement getAlaska()                      {return findByPresence(By.xpath(this.alaska_xpath));                     } 
	private WebElement getAmericanSamoa()               {return findByPresence(By.xpath(this.americanSamoa_xpath));              } 
	private WebElement getArizona()                     {return findByPresence(By.xpath(this.arizona_xpath));                    }
	private WebElement getArkansas()                    {return findByPresence(By.xpath(this.arkansas_xpath));                   }
	private WebElement getBritishColumbia()             {return findByPresence(By.xpath(this.britishColumbia_xpath));            }
	private WebElement getCalifornia()                  {return findByPresence(By.xpath(this.california_xpath));                 }
	private WebElement getColorado()                    {return findByPresence(By.xpath(this.colorado_xpath));                   }
	private WebElement getConnecticut()                 {return findByPresence(By.xpath(this.connecticut_xpath));                }
	private WebElement getDelaware()                    {return findByPresence(By.xpath(this.delaware_xpath));                   }
	private WebElement getDistrictOfColumbia()          {return findByPresence(By.xpath(this.districtOfColumbia_xpath));         }
	private WebElement getFederatedStatesOfMicronesia() {return findByPresence(By.xpath(this.federatedStatesOfMicronesia_xpath));}
	private WebElement getFlorida()                     {return findByPresence(By.xpath(this.florida_xpath));                    }
	private WebElement getGeorgia()                     {return findByPresence(By.xpath(this.georgia_xpath));                    }
	private WebElement getGuam()                        {return findByPresence(By.xpath(this.guam_xpath));                       }
	private WebElement getHawaii()                      {return findByPresence(By.xpath(this.hawaii_xpath));                     }
	private WebElement getIdaho()                       {return findByPresence(By.xpath(this.idaho_xpath));                      }
	private WebElement getIllinois()                    {return findByPresence(By.xpath(this.illinois_xpath));                   }
	private WebElement getIndiana()                     {return findByPresence(By.xpath(this.indiana_xpath));                    }
	private WebElement getIowa()                        {return findByPresence(By.xpath(this.iowa_xpath));                       }
	private WebElement getKansas()                      {return findByPresence(By.xpath(this.kansas_xpath));                     }
	private WebElement getKentucky()                    {return findByPresence(By.xpath(this.kentucky_xpath));                   }
	private WebElement getLouisiana()                   {return findByPresence(By.xpath(this.louisiana_xpath));                  }
	private WebElement getMaine()                       {return findByPresence(By.xpath(this.maine_xpath));                      }
	private WebElement getManitoba()                    {return findByPresence(By.xpath(this.manitoba_xpath));                   }
	private WebElement getMarshallIslands()             {return findByPresence(By.xpath(this.marshallIslands_xpath));            }
	private WebElement getMaryland()                    {return findByPresence(By.xpath(this.maryland_xpath));                   }
	private WebElement getMassachusetts()               {return findByPresence(By.xpath(this.massachusetts_xpath));              }
	private WebElement getMichigan()                    {return findByPresence(By.xpath(this.michigan_xpath));                   }
	private WebElement getMinnesota()                   {return findByPresence(By.xpath(this.minnesota_xpath));                  }
	private WebElement getMississippi()                 {return findByPresence(By.xpath(this.mississippi_xpath));                }
	private WebElement getMissouri()                    {return findByPresence(By.xpath(this.missouri_xpath));                   }
	private WebElement getMontana()                     {return findByPresence(By.xpath(this.montana_xpath));                    }
	private WebElement getNebraska()                    {return findByPresence(By.xpath(this.nebraska_xpath));                   }
	private WebElement getNevada()                      {return findByPresence(By.xpath(this.nevada_xpath));                     }
	private WebElement getNewBrunswick()                {return findByPresence(By.xpath(this.newBrunswick_xpath));               }
	private WebElement getNewHampshire()                {return findByPresence(By.xpath(this.newHampshire_xpath));               }
	private WebElement getNewJersy()                    {return findByPresence(By.xpath(this.newJersy_xpath));                   }
	private WebElement getNewMexico()                   {return findByPresence(By.xpath(this.newMexico_xpath));                  }
	private WebElement getNewYork()                     {return findByPresence(By.xpath(this.newYork_xpath));                    }
	private WebElement getNewfoundlandAndLabrador()     {return findByPresence(By.xpath(this.newfoundlandAndLabrador_xpath));    }
	private WebElement getNorthCarolina()               {return findByPresence(By.xpath(this.northCarolina_xpath));              }
	private WebElement getNorthDakota()                 {return findByPresence(By.xpath(this.northDakota_xpath));                }
	private WebElement getNorthernMarianaIslands()      {return findByPresence(By.xpath(this.northernMarianaIslands_xpath));     }
	private WebElement getNovaScotia()                  {return findByPresence(By.xpath(this.novaScotia_xpath));                 }
	private WebElement getNorthwestTerritories()        {return findByPresence(By.xpath(this.northwestTerritories_xpath));       }
	private WebElement getNunavut()                     {return findByPresence(By.xpath(this.nunavut_xpath));                    }
	private WebElement getOhio()                        {return findByPresence(By.xpath(this.ohio_xpath));                       }
	private WebElement getOklahoma()                    {return findByPresence(By.xpath(this.oklahoma_xpath));                   }
	private WebElement getOntario()                     {return findByPresence(By.xpath(this.ontario_xpath));                    }
	private WebElement getOregon()                      {return findByPresence(By.xpath(this.oregon_xpath));                     }
	private WebElement getPalau()                       {return findByPresence(By.xpath(this.palau_xpath));                      }
	private WebElement getPennsylvania()                {return findByPresence(By.xpath(this.pennsylvania_xpath));               }
	private WebElement getPrinceEdwardIsland()          {return findByPresence(By.xpath(this.princeEdwardIsland_xpath));         }
	private WebElement getPuertoRico()                  {return findByPresence(By.xpath(this.puertoRico_xpath));                 }
	private WebElement getQuebec()                      {return findByPresence(By.xpath(this.quebec_xpath));                     }
	private WebElement getRhodeIsland()                 {return findByPresence(By.xpath(this.rhodeIsland_xpath));                }
	private WebElement getSaskatchewan()                {return findByPresence(By.xpath(this.saskatchewan_xpath));               }
	private WebElement getSouthCarolina()               {return findByPresence(By.xpath(this.southCarolina_xpath));              }
	private WebElement getSouthDakota()                 {return findByPresence(By.xpath(this.southDakota_xpath));                }
	private WebElement getTennessee()                   {return findByPresence(By.xpath(this.tennessee_xpath));                  }
	private WebElement getTexas()                       {return findByPresence(By.xpath(this.texas_xpath));                      }
	private WebElement getUtah()                        {return findByPresence(By.xpath(this.utah_xpath));                       }
	private WebElement getVermont()                     {return findByPresence(By.xpath(this.vermont_xpath));                    }
	private WebElement getVirginIslands()               {return findByPresence(By.xpath(this.virginIslands_xpath));              }
	private WebElement getVirginia()                    {return findByPresence(By.xpath(this.virginia_xpath));                   }
	private WebElement getWashington()                  {return findByPresence(By.xpath(this.washington_xpath));                 }
	private WebElement getWestVirginia()                {return findByPresence(By.xpath(this.westVirginia_xpath));               }
	private WebElement getWisconsin()                   {return findByPresence(By.xpath(this.wisconsin_xpath));                  }
	private WebElement getWyoming()                     {return findByPresence(By.xpath(this.wyoming_xpath));                    }
	private WebElement getYukon()                       {return findByPresence(By.xpath(this.yukon_xpath));                      }
	
	/* ----- methods ----- */
	private StatesProvincesSection clickAlberta()                     {this.getAlberta().click();                     return this;}
	private StatesProvincesSection clickAlabama()                     {this.getAlabama().click();                     return this;}
	private StatesProvincesSection clickAlaska()                      {this.getAlaska().click();                      return this;} 
	private StatesProvincesSection clickAmericanSamoa()               {this.getAmericanSamoa().click();               return this;} 
	private StatesProvincesSection clickArizona()                     {this.getArizona().click();                     return this;}
	private StatesProvincesSection clickArkansas()                    {this.getArkansas().click();                    return this;}
	private StatesProvincesSection clickBritishColumbia()             {this.getBritishColumbia().click();             return this;}
	private StatesProvincesSection clickCalifornia()                  {this.getCalifornia().click();                  return this;}
	private StatesProvincesSection clickColorado()                    {this.getColorado().click();                    return this;}
	private StatesProvincesSection clickConnecticut()                 {this.getConnecticut().click();                 return this;}
	private StatesProvincesSection clickDelaware()                    {this.getDelaware().click();                    return this;}
	private StatesProvincesSection clickDistrictOfColumbia()          {this.getDistrictOfColumbia().click();          return this;}
	private StatesProvincesSection clickFederatedStatesOfMicronesia() {this.getFederatedStatesOfMicronesia().click(); return this;}
	private StatesProvincesSection clickFlorida()                     {this.getFlorida().click();                     return this;}
	private StatesProvincesSection clickGeorgia()                     {this.getGeorgia().click();                     return this;}
	private StatesProvincesSection clickGuam()                        {this.getGuam().click();                        return this;}
	private StatesProvincesSection clickHawaii()                      {this.getHawaii().click();                      return this;}
	private StatesProvincesSection clickIdaho()                       {this.getIdaho().click();                       return this;}
	private StatesProvincesSection clickIllinois()                    {this.getIllinois().click();                    return this;}
	private StatesProvincesSection clickIndiana()                     {this.getIndiana().click();                     return this;}
	private StatesProvincesSection clickIowa()                        {this.getIowa().click();                        return this;}
	private StatesProvincesSection clickKansas()                      {this.getKansas().click();                      return this;}
	private StatesProvincesSection clickKentucky()                    {this.getKentucky().click();                    return this;}
	private StatesProvincesSection clickLouisiana()                   {this.getLouisiana().click();                   return this;}
	private StatesProvincesSection clickMaine()                       {this.getMaine().click();                       return this;}
	private StatesProvincesSection clickManitoba()                    {this.getManitoba().click();                    return this;}
	private StatesProvincesSection clickMarshallIslands()             {this.getMarshallIslands().click();             return this;}
	private StatesProvincesSection clickMaryland()                    {this.getMaryland().click();                    return this;}
	private StatesProvincesSection clickMassachusetts()               {this.getMassachusetts().click();               return this;}
	private StatesProvincesSection clickMichigan()                    {this.getMichigan().click();                    return this;}
	private StatesProvincesSection clickMinnesota()                   {this.getMinnesota().click();                   return this;}
	private StatesProvincesSection clickMississippi()                 {this.getMississippi().click();                 return this;}
	private StatesProvincesSection clickMissouri()                    {this.getMissouri().click();                    return this;}
	private StatesProvincesSection clickMontana()                     {this.getMontana().click();                     return this;}
	private StatesProvincesSection clickNebraska()                    {this.getNebraska().click();                    return this;}
	private StatesProvincesSection clickNevada()                      {this.getNevada().click();                      return this;}
	private StatesProvincesSection clickNewBrunswick()                {this.getNewBrunswick().click();                return this;}
	private StatesProvincesSection clickNewHampshire()                {this.getNewHampshire().click();                return this;}
	private StatesProvincesSection clickNewJersy()                    {this.getNewJersy().click();                    return this;}
	private StatesProvincesSection clickNewMexico()                   {this.getNewMexico().click();                   return this;}
	private StatesProvincesSection clickNewYork()                     {this.getNewYork().click();                     return this;}
	private StatesProvincesSection clickNewfoundlandAndLabrador()     {this.getNewfoundlandAndLabrador().click();     return this;}
	private StatesProvincesSection clickNorthCarolina()               {this.getNorthCarolina().click();               return this;}
	private StatesProvincesSection clickNorthDakota()                 {this.getNorthDakota().click();                 return this;}
	private StatesProvincesSection clickNorthernMarianaIslands()      {this.getNorthernMarianaIslands().click();      return this;}
	private StatesProvincesSection clickNovaScotia()                  {this.getNovaScotia().click();                  return this;}
	private StatesProvincesSection clickNorthwestTerritories()        {this.getNorthwestTerritories().click();        return this;}
	private StatesProvincesSection clickNunavut()                     {this.getNunavut().click();                     return this;}
	private StatesProvincesSection clickOhio()                        {this.getOhio().click();                        return this;}
	private StatesProvincesSection clickOklahoma()                    {this.getOklahoma().click();                    return this;}
	private StatesProvincesSection clickOntario()                     {this.getOntario().click();                     return this;}
	private StatesProvincesSection clickOregon()                      {this.getOregon().click();                      return this;}
	private StatesProvincesSection clickPalau()                       {this.getPalau().click();                       return this;}
	private StatesProvincesSection clickPennsylvania()                {this.getPennsylvania().click();                return this;}
	private StatesProvincesSection clickPrinceEdwardIsland()          {this.getPrinceEdwardIsland().click();          return this;}
	private StatesProvincesSection clickPuertoRico()                  {this.getPuertoRico().click();                  return this;}
	private StatesProvincesSection clickQuebec()                      {this.getQuebec().click();                      return this;}
	private StatesProvincesSection clickRhodeIsland()                 {this.getRhodeIsland().click();                 return this;}
	private StatesProvincesSection clickSaskatchewan()                {this.getSaskatchewan().click();                return this;}
	private StatesProvincesSection clickSouthCarolina()               {this.getSouthCarolina().click();               return this;}
	private StatesProvincesSection clickSouthDakota()                 {this.getSouthDakota().click();                 return this;}
	private StatesProvincesSection clickTennessee()                   {this.getTennessee().click();                   return this;}
	private StatesProvincesSection clickTexas()                       {this.getTexas().click();                       return this;}
	private StatesProvincesSection clickUtah()                        {this.getUtah().click();                        return this;}
	private StatesProvincesSection clickVermont()                     {this.getVermont().click();                     return this;}
	private StatesProvincesSection clickVirginIslands()               {this.getVirginIslands().click();               return this;}
	private StatesProvincesSection clickVirginia()                    {this.getVirginia().click();                    return this;}
	private StatesProvincesSection clickWashington()                  {this.getWashington().click();                  return this;}
	private StatesProvincesSection clickWestVirginia()                {this.getWestVirginia().click();                return this;}
	private StatesProvincesSection clickWisconsin()                   {this.getWisconsin().click();                   return this;}
	private StatesProvincesSection clickWyoming()                     {this.getWyoming().click();                     return this;}
	private StatesProvincesSection clickYukon()                       {this.getYukon().click();                       return this;}
	
	/* ----- methods ----- */
	
	/***
	 * <h1>clickOnStateProvinceFromStatesProvincesDropdown</h1>
	 * <p>Click on the state/province from the States/Provinces dropdown</p>
	 * @param strStateProvince = the abbreviation for you state or province
	 * @return StatesProvincesSection
	 */
	public StatesProvincesSection clickOnStateProvinceFromStatesProvincesDropdown (String strStateProvince) {
		
		// Using recursion to click the appropriate State | Province, rather than having to select the
		// the appropriate click method.

		String strMethodName = "click" + geographyHelpers.getStateOrProvinceAbbreviationFromStateName(strStateProvince).replaceAll(" ", "");
		try {
			this.getClass().getDeclaredMethod(strMethodName).invoke(this);
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			e.printStackTrace();
		}
		
		return this;
	}
	
}