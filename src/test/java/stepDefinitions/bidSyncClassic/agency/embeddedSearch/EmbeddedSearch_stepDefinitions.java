//This is where the search begins
package stepDefinitions.bidSyncClassic.agency.embeddedSearch;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import pages.common.helpers.GlobalVariables;


public class EmbeddedSearch_stepDefinitions {

	private EventFiringWebDriver driver;
	private String strBaseURL;

	public EmbeddedSearch_stepDefinitions(CoreAutomation automation) throws Throwable {
		this.driver = automation.getDriver();		
		strBaseURL = System.getProperty("bidSyncClassicBaseURL");
	}	
	
	@Given("^I navigate to BidSync embedded search page$")
	public void iNavigateToTheEmbeddedSearchPage() throws Throwable {
		try {
			//automation.maximizeBrowser();
			System.out.println(GlobalVariables.getTestID() + " Navigating to the embedded search page:\n");
		} catch(Exception e) {
			System.out.println("Unable to naviage to embedded search page.\n");
			}
	}	
		
	@Given("I perform a basic search with all options {string} and search for {string}")
	public void iBeginSearchingWithAllOptions(String arg1, String arg2) throws Throwable {
		try {
			System.out.print("Navigating to the search page: " + strBaseURL + arg1 + ".\n");		
			driver.get(strBaseURL + arg1);		
			System.out.print("Do a search from the iFrame for an item.\n");
			driver.findElement(By.xpath("//input[contains(@placeholder,'search by keyword, title or bid number (click the question icon to see search instructions)')]")).click();
			driver.findElement(By.xpath("//input[contains(@placeholder,'search by keyword, title or bid number (click the question icon to see search instructions)')]")).sendKeys(arg2);		
			driver.findElement(By.xpath("//span[@class='icon icon-search-find']")).click();
		} catch(Exception e) {
			System.out.println("Unable to perform search with all options.\n");
		  }
	}
	
	@And("^I search for Maricopa County Bids with all options \"([^\"]*)\"$")
	public void iSearchforMaricopaCountyBids(String arg1) throws Throwable {
		try {
			System.out.print("Navigating to the search page: " + strBaseURL + arg1 +" to search for all options.\n");				 
			driver.get(strBaseURL + arg1);
		} catch(Exception e) {
			System.out.println("Unable to navigate to Maricopa search page.\n");
		  }		
	}				

	@And("^I search for current only bids \"([^\"]*)\"$")
	public void iSearchforCurrentOnlyBids(String arg1) throws Throwable {
		try {
			System.out.print("Search all bids with only current bids being displayed...\n");
			driver.get(strBaseURL + arg1);		
			Boolean areBidsCurrent = driver.getPageSource().contains("Bid has ended");
			if(areBidsCurrent==true) {
					System.out.println(GlobalVariables.getTestID() + " Test failed: Ended bids are being returned.\n");
			} else {
					System.out.println ("Test Passed: Only current bids are being displayed.\n");
			  }
		  } catch(Exception e) {
				System.out.println("Unable to search for current bids.\n");
		    }		
	}
		
	
	@And("^I begin searching other agency sites \"([^\"]*)\"$")
	public void iBeginSearchingOtherAgencys(String arg1) throws Throwable {
		System.out.print("Searching other agency sites...\n");
			driver.get(strBaseURL + arg1);
	}	
	
	@Then("^We are done with searching")
	public void weAreDoneSearching() throws Throwable {
		System.out.print("All embedded search tests are done. Thanks for your ongoing support.\n");
	}
}