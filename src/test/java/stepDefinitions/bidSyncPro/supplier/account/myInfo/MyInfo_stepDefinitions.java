package stepDefinitions.bidSyncPro.supplier.account.myInfo;

import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import io.cucumber.datatable.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data.bidSyncPro.registration.RandomChargebeeUser;
import pages.bidSyncPro.supplier.account.accountSettings.AccountSettings;
import pages.bidSyncPro.supplier.account.myInfo.*;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.common.helpers.GeographyHelpers;
import pages.common.helpers.GlobalVariables;

import static org.junit.Assert.fail;

public class MyInfo_stepDefinitions {
	
	public EventFiringWebDriver driver;
	private GeographyHelpers geographyHelpers;
	private MyInfo myInfo;

	public MyInfo_stepDefinitions(CoreAutomation automation) {
		this.driver      = automation.getDriver();
		geographyHelpers = new GeographyHelpers();
		myInfo 			 = new MyInfo(driver);
	}

	@When("^I try to edit my email from the Update User Profile Dialog$")
	public void I_try_to_edit_my_email_from_the_Update_User_Profile_Dialog() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on \"To change your email visit the my account tab\" button on the Update User Profile Dialog");
		UpdateProfileDialog updateProfileDialog = new UpdateProfileDialog(driver);
		updateProfileDialog.openAccountDropDown()
							.clickMyInfo()
							.openUpdateProfileDialog()
							.clickOnChangeEmailBtn();
	}

	@When("^I click on the close button on the Update User Profile Dialog$")
	public void I_click_on_the_close_button_on_the_Update_User_Profile_Dialog() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking close button on User Profile Dialog");
		UpdateProfileDialog updateProfileDialog = new UpdateProfileDialog(driver);
		updateProfileDialog.openAccountDropDown()
							.clickMyInfo()
							.openUpdateProfileDialog()
							.closeUserProfileDialog();
	}
	
	@When ("^I click close on the Current Company screen on the Update User Profile Dialog$")
	public void I_click_close_on_the_Current_Company_screen_on_the_Update_User_Profile_Dialog() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Navigating to the Current Company screen in Update User Profile Dialog and then clicking \"X\" to close");
        new BidList(driver)
                .openAccountDropDown()
                .clickMyInfo()
                .openUpdateProfileDialog()
                .clickOnCompanyBtn()
                .closeCurrentCompanyDialog();
	}
	
	@When("^I click cancel on the Current Company screen on the Update User Profile Dialog$")
	public void I_click_cancel_on_the_Current_Company_screen_on_the_Update_User_Profile_Dialog() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Navigating to the Current Company screen in Update User Profile Dialog and then clicking \"Cancel\" to close");
        new BidList(driver)
                .openAccountDropDown()
                .clickMyInfo()
                .openUpdateProfileDialog()
                .clickOnCompanyBtn()
                .clickOnCancelBnt();
	}
	
	@When("^I try to join a new company from the User Profile Dialog$")
	public void I_try_to_join_a_new_company_from_the_User_Profile_Dialog() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Navigating to the Current Company screen in Update User Profile Dialog and then clicking \"Join New Company\"");
        new BidList(driver)
                .openAccountDropDown()
                .clickMyInfo()
                .openUpdateProfileDialog()
                .clickOnCompanyBtn()
                .clickOnJoinNewCompanyBtn();
	}
	
	@When("^I view the Current Company screen on the Update User Profile Dialog$")
	public void I_view_the_Current_Company_screen_on_the_Update_User_Profile_Dialog() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Navigating to the Current Company screen in Update User Profile Dialog viewing info");
        new BidList(driver)
                .openAccountDropDown()
                .clickMyInfo()
                .openUpdateProfileDialog()
                .clickOnCompanyBtn();
	}
	
	@When("^I edit and save my user profile information on the Update User Profile Dialog per:$")
	public void	I_edit_and_save_my_user_profile_information_on_the_Update_User_Profile_Dialog(DataTable dTable) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Editing user profile info on \"Update User Profile\" dialog and then saving");
		UpdateProfileDialog updateProfileDialog = new UpdateProfileDialog(driver);
		updateProfileDialog
				.openAccountDropDown()
				.clickMyInfo();

		updateProfileDialog
				.verifyPageInitialization()
				.openUpdateProfileDialog()
				.updateUserProfileFromDataTable(dTable)
				.clickOnSaveBtn();
	}
	
	@When("^I edit and cancel my user profile information on the Update Profile Dialog per:$")
	public void I_edit_and_cancel_my_user_profile_information_on_the_Update_Profile_Dialog(DataTable dTable) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Editing user profile info on \"Update User Profile\" dialog and then clicking CANCEL button");
		UpdateProfileDialog updateProfileDialog = new UpdateProfileDialog(driver);
		updateProfileDialog
				.openAccountDropDown()
				.clickMyInfo();
		
		updateProfileDialog
				.verifyPageInitialization()
				.openUpdateProfileDialog()
				.updateUserProfileFromDataTable(dTable)
				.clickOnCancelBtn();
	}
	
	@Then("^I will verify that Current Company fields are displaying$")
	public void I_will_verify_that_Current_Company_fields_are_displaying() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying Current Company displays expected fields");
		CurrentCompanyDialog currentCompanyDialog = new CurrentCompanyDialog(driver);
		currentCompanyDialog.verifyCurrentCompanyIsNotBlank();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Current Company displays information about my user's current company");
	}

	 @Then("^verify phone number validation error on screen$")
		public void verify_phone_number_validation_error_on_screen() {
		 try {
			System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying phone number validation message");
			new UpdateProfileDialog(driver).validatePhoneNumber();
			System.out.println(GlobalVariables.getTestID() + " Test Passed: Phone number validation is on screen");
		 }catch(Exception exp) {
			 fail("TEST FAILED : Phone number validation failed on \"Edit Profile\" page.   " + exp);
		 }
		}
	 
	@Then("^I will verify that the Profile Information tab does not update with the cancelled profile information per:$")
	public void I_will_verify_that_the_Profile_Information_tab_does_not_update_with_the_cancelled_profile_information(DataTable dTable) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying \"My Profile\" tab does not display profile update that was cancelled in \"Update User Profile\" dialog");
        try {
            new UpdateProfileDialog(driver).waitForDialogToDisappear();
        } catch (TimeoutException e) {
            fail("Test Failed: Update Profile Dialog is not closed and is currently displaying");
        }
        
        ProfileElements p1 = new MyInfo(driver).getCurrentProfileElements();
        ProfileElements p2 = new ProfileElements(dTable);
        
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(p1.getFirstName()).as("First Name").isNotEqualTo(p2.getFirstName());
        softly.assertThat(p1.getLastName()).as("Last Name").isNotEqualTo(p2.getLastName());
        softly.assertThat(p1.getJobTitle()).as("Job Title").isNotEqualTo(p2.getJobTitle());
        softly.assertThat(p1.getAddressLine1()).as("Address Line 1").isNotEqualTo(p2.getAddressLine1());
        softly.assertThat(p1.getAddressLine2()).as("Address Line 2").isNotEqualTo(p2.getAddressLine2());
        softly.assertThat(p1.getCity()).as("City").isNotEqualTo(p2.getCity());
        softly.assertThat(p1.getState()).as("State").isNotEqualTo(p2.getState());
        softly.assertThat(p1.getZip()).as("Zip Code").isNotEqualTo(p2.getZip());
        softly.assertThat(p1.getPhoneNumber()).as("Phone Number").isNotEqualTo(p2.getPhoneNumber());
        softly.assertThat(p1.getPhoneExt()).as("Phone Extension").isNotEqualTo(p2.getPhoneExt());
        softly.assertThat(p1.getFaxNumber()).as("Fax Number").isNotEqualTo(p2.getFaxNumber());
        softly.assertThat(p1.getFaxExt()).as("Fax Extension").isNotEqualTo(p2.getFaxExt());
        softly.assertAll();
        
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Cancel of \"Update User Profile\" dialog does not write out profile to \"My Profile\" tab");
	}
	
	@Then("^I will verify that the Profile Information tab updates with my profile information per:$")
	public void I_will_verify_that_the_Profile_Information_tab_updates_with_my_profile_information(DataTable dTable) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying \"My Profile\" tab does not displays profile update after save in \"Update User Profile\" dialog");
        try {
            new UpdateProfileDialog(driver).waitForDialogToDisappear();
        } catch (TimeoutException e) {
            fail("Test Failed: Update Profile Dialog is not closed and is currently displaying");
        }
        
        ProfileElements p1 = new MyInfo(driver).getCurrentProfileElements();
        ProfileElements p2 = new ProfileElements(dTable);
        
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(p1.getFirstName()).as("First Name").isEqualTo(p2.getFirstName());
        softly.assertThat(p1.getLastName()).as("Last Name").isEqualTo(p2.getLastName());
        softly.assertThat(p1.getJobTitle()).as("Job Title").isEqualTo(p2.getJobTitle());
        softly.assertThat(p1.getAddressLine1()).as("Address Line 1").isEqualTo(p2.getAddressLine1());
        softly.assertThat(p1.getAddressLine2()).as("Address Line 2").isEqualTo(p2.getAddressLine2());
        softly.assertThat(p1.getCity()).as("City").isEqualTo(p2.getCity());
        softly.assertThat(p1.getState()).as("State").isEqualTo(geographyHelpers.getStateOrProvinceAbbreviationFromStateName(p2.getState()));
        softly.assertThat(p1.getZip()).as("Zip Code").isEqualTo(p2.getZip());
        softly.assertThat(p1.getPhoneNumber()).as("Phone Number").isEqualTo(p2.getPhoneNumber());
        softly.assertThat(p1.getPhoneExt()).as("Phone Extension").isEqualTo(p2.getPhoneExt());
        softly.assertThat(p1.getFaxNumber()).as("Fax Number").isEqualTo(p2.getFaxNumber());
        softly.assertThat(p1.getFaxExt()).as("Fax Extension").isEqualTo(p2.getFaxExt());
        softly.assertAll();
        
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Save of \"Update User Profile\" dialog correctly writes out profile to \"My Profile\" tab");
		
	}
	
	@Then ("^I will verify that I am accessing the Company Lookup on the Change Company screen$")
	public void I_will_verify_that_I_am_accessing_the_Company_Lookup_on_the_Change_Company_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying user is viewing the Company Lookup on the Change Company screen");
		CompanyLookupDialog companyLookup = new CompanyLookupDialog(driver);
		companyLookup.verifyDialogTitleIsCorrect()
					 .verifyCompanyLookupIsVisible();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: User is accessing Company Lookup on the \"Change Company\" screen");
	}


	@Then("I will be navigated from the My Profile tab to the My Accounts tab")
	public void I_will_be_navigated_from_the_My_Profile_tab_to_the_My_Accounts_tab() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying user has navigated from My Profile tab to the My Accounts tab");
		AccountSettings accountSettingsTab = new AccountSettings(driver);
		accountSettingsTab.verifyUserIsOnMyAccountTab();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: User has navigated to \"My Accounts\" tab");
	}

	@Then("^I will verify that the Update User Profile Dialog has closed$")
	public void I_will_verify_that_the_Update_User_Profile_Dialog_has_closed() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that the User Profile Dialog has closed");
        try {
            new UpdateProfileDialog(driver).waitForDialogToDisappear();
        } catch (TimeoutException e) {
            fail("Test Failed: Update Profile Dialog is not closed and is currently displaying");
        }
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Update User Profile Dialog is closed");
	}
	
	@Then("^I verify that the company name is displayed on profile information page$")
	public void I_verify_that_the_company_name_is_displayed_on_profile_information_page() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that the company name is displayed on my info");
        try {
        	RandomChargebeeUser user = GlobalVariables.getStoredObject("StoredUser");
            myInfo.verifyCompanyName(user);
        } catch (TimeoutException e) {
            fail("Test Failed: Failed to verify company name on My Info page");
        }
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Company name during registration is populated in My info page");
	}

}
