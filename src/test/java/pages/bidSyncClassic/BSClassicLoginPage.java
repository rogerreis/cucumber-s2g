package pages.bidSyncClassic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.common.pageObjects.BasePageActions;

/***
 * <h1>BSClassicLoginPage</h1>
 * @author dmidura
 *<p>details: This class houses methods and page objects for the BidSync Classic Login Page
 *</p>
 */
public class BSClassicLoginPage extends BasePageActions {
	
	// xpaths
	private final String nameInput_xpath     =  "// input[@id='selectProductsForm:username']";
    private final String passwordInput_xpath =  "// input[@id='selectProductsForm:password']";
    private final String signInBtn_xpath     =  "// a[@id='selectProductsForm:loginButton']";
	
	// Other
	private EventFiringWebDriver driver;
	private final String strPagePath = System.getProperty("bidSyncClassicBaseURL") + "bidsync-app-web/vendor/register/Login.xhtml";

	public BSClassicLoginPage(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	/* ----- getters ----- */
	private WebElement getNameInput()     { return findByVisibility(By.xpath(this.nameInput_xpath));     }
	private WebElement getPasswordInput() { return findByVisibility(By.xpath(this.passwordInput_xpath)); }
	private WebElement getSignInBtn()     { return findByVisibility(By.xpath(this.signInBtn_xpath));     }
	
	/* ----- methods ----- */
	
	/***
	 * <h1>loginToClassic</h1>
	 * <p>purpose: Navigate to classic and then sign with credentials</p>
	 * @param userName = username for login
	 * @param password = password for login
	 * @return BSClassicLoginPage
	 */
	public BSClassicLoginPage loginToClassic(String userName, String password) {
		this.navigateToHere();
		this.getNameInput().sendKeys(userName);
		this.getPasswordInput().sendKeys(password);
		this.clickSignInBtn();
		return this;
	}
	
	/***
	 * <h1>navigateToHere</h1>
	 * <p>purpose: Navigate to BidSync Classic login screen
	 * @return BSClassicLoginPage
	 */
	public BSClassicLoginPage navigateToHere() {
		driver.navigate().to(this.strPagePath);
		return this;
	}
	
	/***
	 * <h1>clickSignInBtn</h1>
	 * <p>purpose: Click on the "Sign In" button
	 * @return BSClassicLoginPage
	 */
	public BSClassicLoginPage clickSignInBtn() {
		this.getSignInBtn().click();
		return this;
	}

} 
