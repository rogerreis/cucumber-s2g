package pages.bidSyncPro.registration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.bidSyncPro.common.BidSyncProCommon;
import pages.common.helpers.GlobalVariables;

/***
 * <h1>Class CompanyAdded</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects for the "Company Added" screen that is part of Supplier Registration</p>
 *
 */
public class CompanyAdded extends BidSyncProCommon {
    
    private EventFiringWebDriver driver;
    
    // xpaths
    
    // Labels
    private final String companyAddedLabel_xpath = "//h2[contains(., 'Company Added!')]";
    
    // Text
    private final String companyAddedText_xpath  = "//p[contains(text(), 'You can edit the company profile under your user Settings after logging into your new Periscope S2G account.')]";
    
    // Buttons
    private final String nextButton_xpath        = "//button[@id='CreateNextButton2']";
    private final String backButton_xpath        = "//button[@id='createBbackButton']";
    
    public CompanyAdded(EventFiringWebDriver driver) {
    	super(driver);
        this.driver        = driver;
    }
    
    /* ----- getters ----- */
    // Labels
    private WebElement getCompanyAddedLabel() { return findByVisibility(By.xpath(this.companyAddedLabel_xpath)); }
    
    // Text
    private WebElement getCompanyAddedText () { return findByVisibility(By.xpath(this.companyAddedText_xpath));  }
    
    // Buttons
    private WebElement getNextButton() { return findByVisibility(By.xpath(this.nextButton_xpath)); }
    private WebElement getBackButton() { return findByVisibility(By.xpath(this.backButton_xpath)); }
    
    /* ----- methods ----- */
    
    /***
     * <h1>clickNextButton</h1>
     * <p>purpose: Click the "NEXT" button on the "Company Added" page</p>
     * @return EnterKeywords
     */
    public EnterKeywords clickNextButton() {
        getNextButton().click();
        return new EnterKeywords(driver);
    }
    
    /* ----- verifications ----- */
    
    /***
     * <h1>verifyPageInitialization</h1>
     * <p>purpose: Verify that the "Company Added" page initializes as follows:<br>
     * 	1. Card label displays<br>
     * 	2. Card Text displays<br>
     *  3. Buttons display</p>
     * @return CompanyAdded
     */
    public CompanyAdded verifyPageInitialization() {

    	// Labels
    	this.getCompanyAddedLabel();
    	
    	// Text
    	this.getCompanyAddedText();
    	
    	// Buttons
    	this.getNextButton();
    	this.getBackButton();

    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified that \"Company Added\" page initialized.");
    	return this;
    }
}
