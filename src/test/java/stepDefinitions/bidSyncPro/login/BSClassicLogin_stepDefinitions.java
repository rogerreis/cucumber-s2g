package stepDefinitions.bidSyncPro.login;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.Given;
import pages.bidSyncClassic.BSClassicLoginPage;
import pages.bidSyncClassic.BSClassicMyRFQLoginPage;
import pages.common.helpers.GlobalVariables;

public class BSClassicLogin_stepDefinitions {

	private EventFiringWebDriver driver;
	
	public BSClassicLogin_stepDefinitions(CoreAutomation automation) {
		this.driver = automation.getDriver();
	} 
	
	/* ------------ Test Steps ---------------- */


	@Given("^I log into BidSync Classic as user \"(.*)\" and password \"(.*)\"$")
	public void I_have_navigated_to_the_Bid_Sync_Classic_Login_Page(String userName, String password) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Logging into BS Classic as user \"" + userName + "\"");
		new BSClassicLoginPage(driver)
			.loginToClassic(userName, password);
	}
	
	@Given("^I log into BidSync Classic MyRFQ as username \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void iLogIntoClassicMyRFQAsUsernameAndPassword(String userName, String password) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Logging into BS Classic My RFQ as user \"" + userName + "\"");
		new BSClassicMyRFQLoginPage(driver)
			.loginToMyRFQ(userName, password);
	}

}

