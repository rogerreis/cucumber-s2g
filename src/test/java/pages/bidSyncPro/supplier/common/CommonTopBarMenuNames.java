package pages.bidSyncPro.supplier.common;

import static org.junit.Assert.fail;

/***
 * <h1>enum: CommonTopBarMenuNames</h1>
 * @author dmidura
 * <p>details: This enum defines the names of the top level menus that are common across BidSync Pro<br>
 * 	Menu Names are: Bid List, Company Settings, Account<br>
 * 	Note: Options for Bid List menu are defined in BidListTabNames, Options for Company Settings are
 * 	defined in CompanySettingsDropdownOptions, and options for Account are defined in AccountDropdownOptions</p>
 */
public enum CommonTopBarMenuNames {
	BID_LIST,
	COMPANY_SETTINGS,
	ACCOUNT,
	MARKET_PLACE;
	
	/***
	 * <h1>fromString</h1>
	 * <p>purpose: Given text that matches the Common Top Bar menu name,<br>
	 *  return the enum value of the CommonTopBarMenuNames</p>
	 * @param menuName<br>
	 * 	= "Bid List" | "Company Settings" | "Account"
	 * @return CommonTopBarMenuNames
	 */
	public static CommonTopBarMenuNames fromString(String menuName) {
		if (menuName.equalsIgnoreCase("Bid List")) {
			return BID_LIST;
		} else if (menuName.equalsIgnoreCase("Company Settings")) {
			return COMPANY_SETTINGS;
		} else if (menuName.equalsIgnoreCase("Account")) {
			return CommonTopBarMenuNames.ACCOUNT;
		} else if (menuName.equalsIgnoreCase("Marketplace")) {
			return CommonTopBarMenuNames.MARKET_PLACE;
		} else {
			fail("ERROR: Top Bar Menu name \"" + menuName + "\" is an invalid option to CommonTopBarMenuNames");
			return null;
		}
	}
	
	@Override
	public String toString() {
		if (this.equals(CommonTopBarMenuNames.BID_LIST)) {
			return "Bid List";
		} else if (this.equals(CommonTopBarMenuNames.COMPANY_SETTINGS)) {
			return "Company Settings";
		} else if (this.equals(CommonTopBarMenuNames.ACCOUNT)) {
			return "Account";
		} else if (this.equals(CommonTopBarMenuNames.MARKET_PLACE)) {
			return "Marketplace";
		} else {
			fail("ERROR: Top Bar Menu name value \"" + this + "\" is an invalid option to CommonTopBarMenuNames");
			return null;
		}
	}
	

}
