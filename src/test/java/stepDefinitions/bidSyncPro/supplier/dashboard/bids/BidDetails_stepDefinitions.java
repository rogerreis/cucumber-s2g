package stepDefinitions.bidSyncPro.supplier.dashboard.bids;

import core.CoreAutomation;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.openqa.selenium.TimeoutException;

import pages.bidSyncPro.supplier.dashboard.bidLists.BidListRow;
import pages.bidSyncPro.supplier.dashboard.bids.BidDetails;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class BidDetails_stepDefinitions {

	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;
	private BidDetails bidDetails;
	public CoreAutomation automation;

	public BidDetails_stepDefinitions(CoreAutomation automation) {
		this.automation = automation;
		this.driver     = automation.getDriver();
		bidDetails      = new BidDetails(driver);
		helperMethods   = new HelperMethods();
	}

    /* ------------ Test Steps ---------------- */
	
	/***
	 * <h1>I_try_to_view_an_invalid_bid</h1>
	 * <p>purpose: Navigate user to an invalid bid</p>
	 */
	@When("^I try to view an invalid bid$")
	public void I_try_to_view_an_invalid_bid() {
		helperMethods.addSystemWait(3);
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: User is attempting to view invalid bid");
		bidDetails.navigateToBadBid();
	}
	
	/***
	 * <h1>I_will_see_a_bid_not_found_screen</h1>
	 * <p>purpose: Verify that the user is viewing a "Bid not Found" screen.</p>
	 */
	@Then ("^I will see a bid not found screen$")
	public void I_will_see_a_bid_not_found_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that a \"Bid not Found\" screen appears");
		bidDetails.verifyBadBidScreen();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: When user attempts to view bad bid, a \"Bid not Found\" screen appears");
	}
	
	@Then ("^I will verify that all of the objects on the bid details screen have correctly populated$")
	public void I_will_verify_that_all_of_the_objects_on_the_bid_details_screen_have_correctly_populated() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that the \"Bid Details\" screen has correctly populated");
		bidDetails.verifyNotDisplayingBidNotFoundScreen()
				.verifyBidDetailsHaveCorrectlyLoaded();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: OnLoad of a random bid, \"Bid Details\" screen correctly populates");
	}
	
	@Given("^I navigate to the bid detail page where the bid ID matches that of global variable \"([^\"]*)\"$")
	public void iNavigateToTheBidDetailPageWhereTheBidIDMatchesThatOfGlobalVariable(String strGlobalVariable) {
		String strUUID = GlobalVariables.getStoredObject(strGlobalVariable);
		System.out.println(System.getProperty("supplierBaseURL") + "bid-detail/" + strUUID);
		driver.get(System.getProperty("supplierBaseURL") + "bid-detail/" + strUUID);
		
	}
	
	@And ("^I click on the BACK TO BIDS button to return to the bid list$")
	public void I_click_on_the_BACK_TO_BIDS_button_to_return_to_the_bid_list() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on \"BACK TO BIDS OPPORTUNITIES\" button to return to bid list");
		bidDetails.goBackToBids();
	}

	@When("^I click the link for the first document$")
	public void iClickTheLinkForTheFirstDocument() {
	    String xpath = "//*[contains(@id,'bidDetailAttachment')]";
	    helperMethods.waitForVisibilityOfXpath(driver, xpath, Integer.parseInt(System.getProperty("defaultTimeOut")));
		driver.findElement(By.xpath(xpath)).click();
		// allow download to start from who-knows-what 3rd party site
        helperMethods.addSystemWait(5);
	}

	// used in conjunction with "I click the link for the first document" above
	@Then("^a download should begin$")
	public void aDownloadShouldBegin() {
	    try {
            automation.waitForNumberOfWindowsToEqual(2);
        } catch (TimeoutException e) {
            Assert.fail("No download window opened.");
        }
	}
    
    @Then("^the bid details has the correct end date$")
    public void theBidDetailsHasTheCorrectEndDate() {
    	System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that the bid details displays the correct end date");
        BidListRow bid = GlobalVariables.getStoredObject("StoredBid");
	    BidDetails bidDetails = new BidDetails(driver);
        GregorianCalendar actualEndDate = GregorianCalendar.from(bidDetails.getEndDateInUTC());
        Calendar expectedEndDate = bid.bidInfo.getPreciseEndDate();
        String actualEndDateStr = String.format("%s-%s-%s",
                actualEndDate.get(Calendar.YEAR),
                actualEndDate.get(Calendar.MONTH),
                actualEndDate.get(Calendar.DAY_OF_MONTH));
        String expectedEndDateStr = String.format("%s-%s-%s",
                expectedEndDate.get(Calendar.YEAR),
                expectedEndDate.get(Calendar.MONTH),
                expectedEndDate.get(Calendar.DAY_OF_MONTH));
        SoftAssertions softly = new SoftAssertions();
        softly.assertThat(actualEndDateStr)
                .as(String.format("End Date in Bid Details for bid '%s'", bid.bidInfo.getBidId()))
                .isEqualTo(expectedEndDateStr);
        softly.assertAll();
    }
}
