@must @registration @level3
Feature: Supplier Registration - Create New Company Screen

@ARGO-948 
Scenario: Company Lookup Initializes "Create New Company" Screen With Correct Information for new company (not located in lookup)
	Information Includes:
		1. Company Name Matches Selection From Company Lookup
		2. User Address Populates Given Fields: State, City, Postal Code, Address, Address Line 2
		3. Address Fields are Editable
		4. Company Name Field is Not Editable
	dmidura: Workaround in place for BIDSYNC-318: Company Name field should not be editable when creating new company registration

	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to the Get Invited to Bids screen
	When I have added a new company into the Company Lookup and clicked NEXT
	Then I will verify that the Create New Company screen correctly populates with my user registration address and company name
	And I will verify that the Create New Company address fields are editable
	And I will verify that the Create New Company company name field is not editable
	
# TODO: ARGO-2579

@BIDSYNC-42
Scenario: Test to verify company address is populating while joining a existing company
 	Given I register a new user with a BidSync Basic Plan from the Masonry website
	And I complete Supplier Registration
	Given I register second user with a BidSync Basic Plan from the Masonry website
	And I have completed supplier registration up to select company
	Then I verify that the company address is populated in the drodown
