package pages.mailinator;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.server.handler.FindChildElements;
import org.openqa.selenium.remote.server.handler.FindElements;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.bidSyncPro.dailyBidNotifications.dailyBidNotification;
import pages.bidSyncPro.supplier.emails.ChangeEmailEmail;
import pages.bidSyncPro.supplier.emails.ChangePasswordEmail;
import pages.bidSyncPro.supplier.emails.PasswordResetEmail;
import pages.bidSyncPro.supplier.emails.RegistrationCompleteEmail;
import pages.bidSyncPro.supplier.emails.RevokeSubscriptionLicenseEmail;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.BasePageActions;

/***
 * <h1>Mailinator</h1>
 * @author dmidura
 * <p>details: This class houses methods to access and manipulate emails in mailinator.</p>
 * <p>TODO: Refactor this class</p>
 */
public class Mailinator extends BasePageActions {

	
	// Other 
	private HelperMethods helperMethods;
    private EventFiringWebDriver driver;
	private String strPagePath                = System.getProperty("mailinatorURL");
	private String strPublicQueryRelativePath = "v3/index.jsp?token=b312ec9815a4432c9cf436cfe744d312";
	private String BidSyncProSender     = "notify-qa";  // Sender of the BidSync Pro email

	// Opened Email icons
//	private final String garbageCanButton_xpath = "//span[@title='Delete Emails']//i[contains(@class,'fa-trash')]";
	private final String garbageCanButton_xpath = "//button[@title='Delete Emails']";
	private final String viewingModeDropdown_xpath = "//select[@id='contenttypeselect']";
	private final String privateTeamInbox_xpath = "//div[contains(text(),'Private Team Inbox')]";
	private final String searchInbox_id = "inbox_field";
	private final String searchIcon_id = "go_inbox";

	public Mailinator(EventFiringWebDriver driver) {
		super(driver);
		this.driver 	= driver;
		helperMethods	= new HelperMethods();
		if (System.getProperty("server").contentEquals("stage")) { 
			this.BidSyncProSender = "notify";
		}
	}
	
	/* ----- getters ----- */

	private WebElement getGarabageCanButton() { return findByClickability(By.xpath(this.garbageCanButton_xpath)); }
	private Select getViewingModeDropdown()   { return new Select (findByVisibility(By.xpath(this.viewingModeDropdown_xpath)));}
	private WebElement getPrivateTeamInbox()  { return findByVisibility(By.xpath(privateTeamInbox_xpath)); }
	private WebElement getSearchInbox()		  { return findByVisibility(By.id(searchInbox_id)); }
	private WebElement getSearchIcon()		  { return findByVisibility(By.id(searchIcon_id)); }
	/* ----- helpers ----- */

	/***
	 * <h1>switchFocusToEmail</h1>
	 * <p>purpose: After the email has been opened (see openEmail method) inside of the Mailinator inbox<br>
	 * 	switch focus to the iframe actually containing the email</p>
	 * @return None
	 */
	private Mailinator switchFocusToEmail() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Switching focus to the iframe containing the email");
		//sometimes the test says it can't find the link in the email
        try {
            Thread.sleep(3000);  // let's see if this it more reliable
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.switchTo().defaultContent();
		driver.switchTo().frame("msg_body");
		return this;
	}
	
	/***
	 * <h1>setViewingMode</h1>
	 * <p>purpose: Set the viewing mode for an open email (default is "text/html")
	 * @param selection = "text/plain" | "text/html" | "show original" | "json"
	 * @return Mailinator
	 */
	private Mailinator setViewingMode(String selection) {
		this.getViewingModeDropdown().selectByVisibleText(selection);
		return this;
	}
	/* --------------- Methods --------------- */
 
	/***
	 * <h1>navigateToMailinatorInbox</h1>
	 * <p>purpose: Navigate directly to the mailinator public inbox for strEmail</p>
	 * @param strEmail = String of your mailinator email<br>
	 * 	ex: "test@phimail.mailinator.com"
	 * @return None
	 */
	public Mailinator navigateToMailinatorInbox(String strEmail) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Navigating to \"%s\" mailinator inbox \n", strEmail);
		String inboxName = strEmail.replace("@phimail.mailinator.com", "");
		String strInboxURL = strPagePath + strPublicQueryRelativePath;
		System.out.println(GlobalVariables.getTestID() + " Mailinator inbox URL: \"" + strInboxURL + "\"");
		
		driver.get(strInboxURL); 
		
		// Try again -- sometimes mailinator rejects the initial call
//		if (!driver.getCurrentUrl().equals(strInboxURL)) {
//			System.out.println(GlobalVariables.getTestID() + " INFO: Did not navigate to Mailinator inbox, trying again");
//			driver.navigate().to(strInboxURL);
//			assertTrue("ERROR: Did not navigate to \"" + strInboxURL + "\" after two attempts", driver.getCurrentUrl().equals(strInboxURL));
//		}
		getPrivateTeamInbox().click();
		helperMethods.addSystemWait(3);
		System.out.println(GlobalVariables.getTestID() + " Mailinator inbox: \"" + inboxName + "\"");
		// mandrews:  some tests involve creating multiple users in the same tests.  In these cases, the search box in the 
		// (newly created) mailinator window will still contain the email from the previous user.  So make sure the search
		// box is clear or you'll just be adding more characters to the previous search (which will then give no results)
		getSearchInbox().clear();
		getSearchInbox().sendKeys(inboxName);
		getSearchIcon().click();
		helperMethods.addSystemWait(3);
		
		//return openUserInbox(inboxName); // This line commented out due to Mailinator not showing inboxes recently.  We may need this again if they come back.
        return this;
	}
	

	/***
	 * <h1>getEmails</h1>
	 * <p>purpose: Return all emails that match subjectLine and sender<br>
	 * TODO: convert this to private helper method after updating checks on acceptance, rejection, and request
	 * 	emails in the User Management tests
	 * @param subjectLine = expected subject line in email (Note: can be a partial subjectLine)
	 * @param sender = expected sender of email
	 * @return List of WebElements, each list element contains WebElement for one email row in Mailinator inbox
	 */
	public List<WebElement> getEmails(String sender,String subjectLine) {
        System.out.printf(GlobalVariables.getTestID() + " INFO: Looking for emails with subject line \"%s\" from sender \"%s\"\n", subjectLine, sender);
//        String xpath = "//ul[@id='inboxpane']//li[contains(div,'" + sender + "') and " + "contains(div,'" + subjectLine + "')]";
//        String xpath = "//tr[td//text()[contains(.,'"+sender + "')] and td//a[contains(.,'" + subjectLine + "')]]";
        String xpath = "//tr[td[contains(text(),'"+sender+"')] and td//a[contains(text(),'"+subjectLine+"')]]";
        System.out.println(GlobalVariables.getTestID() + " INFO : Xpath to select email: " + xpath );
        List <WebElement> numEmails = null ;
        int timeInterval    = 6;
        int maxIntervals    = 20;
        Boolean foundEmails = false;

        // How many emails matching subjectLine and sender are in the inbox?
        // Try maxIntervals to locate the emails with wait durations of timeInterval
        int attemptNumber   = 0;
        while (attemptNumber < maxIntervals) {
			try {
				numEmails = new WebDriverWait(driver, timeInterval)
						.pollingEvery(Duration.ofSeconds(1))
						.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
				foundEmails = true;
				System.out.printf(GlobalVariables.getTestID() + " INFO: Retrieved \"%s\" email from sender \"%s\" within %s - %s sec intervals\n", 
						subjectLine, sender, attemptNumber + 1, timeInterval);
			} catch (TimeoutException t) {
				attemptNumber++;
				driver.navigate().refresh();
				foundEmails = false;
			} finally {
				if (foundEmails) { break; }
				String errMsg = String.format("ERROR: Unable to locate \"%s\" email from sender \"%s\" after %s - %s sec intervals", 
						subjectLine, sender, maxIntervals, timeInterval);
				Assert.assertTrue(errMsg, attemptNumber < maxIntervals );
			}
        }
        // Return all of the emails matching subjectLine and sender in order of top-bottom in Mailinator inbox
        List <WebElement> returns = new ArrayList<>();
        for (int i = 1; i < numEmails.size() + 1 ; i ++) {
        	returns.add(findByVisibility(By.xpath(xpath + "[" + i + "]"))); }
        return returns;
    }
    

	/***
	 * <h1>deleteOpenEmail</h1>
	 * <p>purpose: An email was opened and is currently visible in Mailinator.
	 * 	Click on the Mailinator garbage can icon to delete the email</p>
	 * @return Mailinator
	 */
	public Mailinator deleteOpenEmail() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Deleting open email");
		this.switchToWindow("Mailinator");
		this.getGarabageCanButton().click();
		helperMethods.addSystemWait(5);
		driver.navigate().refresh();
		return this;
	}
	
	/* ----- open email methods ----- */
	
	/***
	 * <h1>openEmail</h1>
	 * <p>purpose: Locate the newest email sent from strEmailSender with subject line strEmailSubjectLine<br>
	 * 		and then click on this email to view</p>
	 * @param subjectLine = subject line of email, as appears in mailinator inbox<br>
	 * 		ex: "Registration Email"
	 * @param sender = name of sender (sans email domain)<br>
	 * 		ex: "argo" for email sent from argo@periscopeholdings.com<br>
	 * 		ex: "notify" for email sent from notify@bidsync.com</p>
	 * @return Mailinator
	 */
	public Mailinator openEmail (String sender,String subjectLine) {
		List <WebElement> possibleEmails = getEmails(sender, subjectLine);
		if (possibleEmails.isEmpty()) {
			fail ("ERROR: No \"" + subjectLine + "\" emails from \"" + sender + "\" exist in inbox");
		} else if (possibleEmails.size() > 1) {
			System.out.printf(GlobalVariables.getTestID() + " INFO: There are %s emails from sender \"%s\" with subject line \"%s\n", possibleEmails.size(), sender, subjectLine); }
		System.out.println(GlobalVariables.getTestID() + " INFO: Opening the most recent email."); 
		possibleEmails.get(0).findElement(By.xpath(".//a")).click();
		return this;
	}
	
	/***
	 * <h1>openDailyBidNotificationEmail</h1>
	 * <p>purpose: Locate the daily bid notification email and open it<br>
	 * 	Note: Display will be set to "text/plain"
	 * @return dailyBidNotification
	 */
	public dailyBidNotification openDailyBidNotificationEmail() {
		// TODO: Update after QA env gets bids sending correctly
		
	    // Fix for release OCT 7
		// this.openEmail("qaqasupplier","[QA] New Bids!");
		this.openEmail("notify-qa","[QA] New Bid Opportunities!");
//			.setViewingMode("text/html")
//			.switchFocusToEmail();
		return new dailyBidNotification(driver);
	}
	
	/***
	 * <h1>openRevokeSubscriptionLicenseEmail</h1>
	 * <p>purpose: Locate the Revoke Subscription email in the inbox and open it</p>
	 * @return RevokeSubscriptionLicenseEmail
	 */
	public RevokeSubscriptionLicenseEmail openRevokeSubscriptionLicenseEmail() {
		this.openEmail( this.BidSyncProSender, "Subscription has been removed") 
			.switchFocusToEmail();
		return new RevokeSubscriptionLicenseEmail(driver);
	}
	
	/***
	 * <h1>openRegistrationCompleteEmail</h1>
	 * <p>purpose: Locate and then open the Registration Complete Email email. Switch focus to the email iframe</p>
	 * @return RegistrationCompleteEmail
	 */
	public RegistrationCompleteEmail openRegistrationCompleteEmail() {
	    openEmail(this.BidSyncProSender, "Please Complete Your Periscope S2G Registration");
	    switchFocusToEmail();
	    return new RegistrationCompleteEmail(driver);
    }

	/***
	 * <h1>openResetPasswordEmail</h1>
	 * <p>purpose: Locate and then open the Password Reset Email.<br>
	 * 	(This email is sent when the user generates a password reset via "Forgot Your Password"<br>
	 * 	flows off the Supplier login screen. Switch focus to the email iframe<br>
	 * 	Note: Email is different then changing password flows<br>
	 * 	email that is generated off the Admin>My Account screen</p>
	 * @return RegistrationCompleteEmail
	 */
	public PasswordResetEmail openPasswordResetEmail() {
	    openEmail(this.BidSyncProSender,"Password Reset Email");
	    switchFocusToEmail();
	    return new PasswordResetEmail(driver);
	}

	/***
	 * <h1>openChangePasswordEmail</h1>
	 * <p>purpose: Locate and then open the Change Password email. Switch focus to the email iframe</p>
	 * @return ChangePasswordEmail
	 */
	public ChangePasswordEmail openChangePasswordEmail() {
	    openEmail(this.BidSyncProSender,"Change Password Email");
	    switchFocusToEmail();
	    return new ChangePasswordEmail(driver);
	}

	/***
	 * <h1>openChangeEmailEmail</h1>
	 * <p>purpose: Locate and then open the Change Email email. Switch focus to the email iframe</p>
	 * @return ChangeEmailEmail
	 */
	public ChangeEmailEmail openChangeEmailEmail() {
	    openEmail(this.BidSyncProSender,"Verify email request");
	    switchFocusToEmail();
	    return new ChangeEmailEmail(driver);
	}
	
    /* ----- verifications ----- */

    /***
     * <h1>verifyOnlyOneEmailSent</h1>
     * <p>purpose: Verify that only one email with subjectLine from sender<br>
     * 	sent within the last two minutes to the mailinator inbox<br>
     * 	Fail test if there is more than one email with subjectLine from sender<br>
     * 	with a timestamp of "moments ago", "1 minute ago", or "2 minutes ago",<br>
     * 	or if there are no emails with subjectLine from sender<br>
     * 	with a timestamp of "moments ago", "1 minute ago", or "2 minutes ago".</p>
     * <p>This method should be called after navigateToMailinatorInbox()</p>
     * @param subjectLine = email subject line
     * @param sender = email sender
     * @return Mailinator
     */
    private Mailinator verifyOnlyOneEmailSent(String sender, String subjectLine) {
    	List <WebElement> emails = this.getEmails(sender,subjectLine);
    	emails.stream()
    		.filter(ele->ele.getText().trim()
    				// Text displayed is for the entire email row. Strip out extraneous text and only consider timestamp
    				.replaceAll(sender, "").replaceAll(subjectLine, "").replaceAll("\\W*\\w*@phimail.mailinator.com", "").replaceAll("\\n", "")
    				.matches("(moments ago|minute ago|2 minutes ago$)"))
    		.collect(Collectors.toList());
    	
    	helperMethods.addSystemWait(2);
    	int numEmails = emails.size();
    	Assert.assertTrue("Test Failed: More than one \"" + subjectLine + "\" email from \"" + sender + "\" was sent within the last 2 minutes", 
    			numEmails<2);
    	Assert.assertFalse("Test Failed: No \"" + subjectLine + "\" emails from \"" + sender + "\" were sent within the last 2 minutes", 
    			numEmails==0);

    	System.out.println(GlobalVariables.getTestID() + " INFO: Verified that only one \""+ subjectLine + "\" email from \"" + sender + "\"  was sent to the mailinator inbox within the last 2 minutes");
    	return this;
    }

	/***
	 * <h1>verifyOnlyOneResetPasswordEmailSent</h1>
	 * <p>purpose: Verify that only one Password Reset Email from BidSync Pro was<br>
	 * 	sent within the last two minutes to the mailinator inbox<br>
	 * 	Fail test if there is more than one Password Reset Emails sent from BidSync Pro <br>
	 * 	with a timestamp of "moments ago", "1 minute ago", or "2 minutes ago",<br>
	 * 	or if there are no Password Reset Emails sent from BidSync Pro<br>
	 * 	with a timestamp of "moments ago", "1 minute ago", or "2 minutes ago".</p>
	 * 	<p>(Password Reset Email is sent when the user generates a password reset via "Forgot Your Password"<br>
	 * 	flows off the Supplier login screen. Note: Email is different then changing password flows<br>
	 * 	email that is generated off the Admin>My Account screen</p>
	 * <p>This method should be called after navigateToMailinatorInbox()</p>
	 * @return Mailinator
	 */ 
    public Mailinator verifyOnlyOneResetPasswordEmailSent() {
    	this.verifyOnlyOneEmailSent(this.BidSyncProSender,"Password Reset Email");
    	return this;
    }
}
