@must @notifications @level2
Feature: Daily Bid Notifications > Notification Images
	BIDSYNC-635: Bad Links in Bid Notifications
	
@BIDSYNC-635
Scenario: Images in the notification email are displayed
	Given I have retrieved the newest bid notification email for user "Basic"
	Then I will verify that all bid notification email images are displayed