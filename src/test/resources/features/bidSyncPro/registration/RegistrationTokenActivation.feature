@must @registration @level2
Feature: Supplier Registration - Registration Token Activation
	ARGO-969: Create Basic Account in Chargebee (if None)
	ARGO-1214: Creating a user with a Basic Plan (Coming from BidSync.com)
	BIDSYNC-476: Duplicate Customers and Subscriptions being created in ChargeBee

@ARGO-1214 @ARGO-969 @BIDSYNC-476
Scenario: Supplier Registers for a BidSync Basic Account via Chargebee on Masonry site and Verifies Their Email Address. Confirm that:
	1. A BidSync - Basic account was created in ARGO
	2. A customer account was created in Chargebee (test site)
	3. A BidSync - Basic subscription is listed in Chargebee (test site) 

	Given I register a new user with a BidSync Basic Plan from the Masonry website
	When I activate the email token sent in my registration complete email
	And I wait "10" seconds for the backend to catch up
	Then I will verify that only one user customer account exists for my user in the Chargebee test site
	And I will verify that my user's customer_id matches in the ARGO database and Chargebee test site
	And I will verify that my user only has a "BidSync Basic" subscription plan listed in the Chargebee test site 
	And I will verify that my user only has a "BidSync Basic" subscription plan listed in the ARGO database

	
@BIDSYNC-476
Scenario Outline: Supplier Registers for a BidSync Pro Account via Chargebee on Masonry site and Verifies Their Email Address. Confirm that:
	1. A BidSync Pro account was created in ARGO
	2. A customer account was created in Chargebee (test site)
	3. A BidSync Pro subscription is listed in Chargebee (test site) 
	
	Given I register a new user in Masonry with a "<subscriptionType>" subscription plan
	When I activate the email token sent in my registration complete email
	And I wait "5" seconds for the backend to catch up
	Then I will verify that only one user customer account exists for my user in the Chargebee test site
	And I will verify that my user's customer_id matches in the ARGO database and Chargebee test site
	And I will verify that my user only has a "<subscriptionType>" subscription plan listed in the ARGO database
	And I will verify that my user only has a "<subscriptionType>" subscription plan listed in the Chargebee test site 
  
  	Examples:
	| subscriptionType           |
	| BidSync National - Yearly  |
	| BidSync National - Monthly |
	| BidSync National - 6 Month |
	| BidSync Regional - Yearly  |
	| BidSync Regional - Monthly |
	| BidSync Regional - 6 Month |
	| BidSync State - Yearly     |
	| BidSync State - Monthly    |
	| BidSync State - 6 Month    |
 	
	
@BIDSYNC-90
Scenario: Activation Email contains all expected elements
	Given I register a new user with a BidSync Basic Plan from the Masonry website
	When I retrieve my registration email from Mailinator for my new generic Chargebee registration
	Then I will verify that the registration email contains all expected elements

	
@BIDSYNC-317
Scenario: Automate Vendor registration when in status V
	Given I register a new user with a BidSync Basic Plan from the Masonry website
		And I activate the email token sent in my registration complete email
		And I wait "5" seconds for the backend to catch up
	Then Verify that the users's registration status is 'V' in database
	When I Navigate back to the Masonry site and enter the same email
	Then Verify that the email is rejected with correct rejection message
		
	
