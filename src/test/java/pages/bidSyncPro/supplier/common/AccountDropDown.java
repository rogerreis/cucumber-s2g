package pages.bidSyncPro.supplier.common;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.bidSyncPro.supplier.account.accountSettings.AccountSettings;
import pages.bidSyncPro.supplier.account.bidProfile.BidNotifications;
import pages.bidSyncPro.supplier.account.myInfo.MyInfo;
import pages.bidSyncPro.supplier.account.support.Support;
import pages.bidSyncPro.supplier.account.training.Training;
import pages.bidSyncPro.supplier.companySettings.manageUsers.ManageUsers;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.pageObjects.BasePageActions;

import java.util.concurrent.TimeUnit;

public class AccountDropDown extends BasePageActions {

	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;

	public AccountDropDown(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
		helperMethods = new HelperMethods();
	}

	// xpaths
	private final String bidProfile_xpath = "//button[@role='menuitem' and contains(span, 'Bid Profile')]";
	private final String bidProfileIcon_xpath = "//button[@role='menuitem' and contains(span, 'Bid Profile')]/mat-icon[@role='img' and contains(text(), 'mail')]";
	private final String myInfo_xpath = "//button[@role='menuitem' and contains(span, 'My Info')]";
	private final String myInfoIcon_xpath = "//button[@role='menuitem' and contains(span, 'My Info')]/mat-icon[@role='img' and contains(text(), 'person')]";
	private final String training_xpath = "//button[@role='menuitem' and contains(span, 'Training')]";
	private final String trainingIcon_xpath = "//button[@role='menuitem' and contains(span, 'Training')]/mat-icon[@role='img' and contains(text(), 'school')]";
	private final String support_xpath = "//button[@role='menuitem' and contains(span, 'Support')]";
	private final String supportIcon_xpath = "//button[@role='menuitem' and contains(span, 'Support')]/mat-icon[@role='img' and contains(text(), 'headset_mic')]";
	private final String accountSettings_xpath = "//button[@role='menuitem' and contains(span, 'Account Settings')]";
	private final String accountSettingsIcon_xpath = "//button[@role='menuitem' and contains(span, 'Account Settings')]/mat-icon[@role='img' and contains(text(), 'star')]";
	private final String logout_xpath = "//button[@role='menuitem' and contains(span, 'Logout')]/span";
	private final String logoutIcon_xpath = "//button[@role='menuitem' and contains(span, 'Logout')]/mat-icon[@role='img' and contains(text(), 'exit_to_app')]";
private final String pageOverlay = "//div[contains(@class,'cdk-overlay-pane')]";
	/* ----- getters ----- */

	private WebElement getBidProfile() {
		return findByVisibility(By.xpath(this.bidProfile_xpath));
	}

	private WebElement getBidProfileIcon() {
		return findByVisibility(By.xpath(this.bidProfileIcon_xpath));
	}

	private WebElement getMyInfo() {
		return findByVisibility(By.xpath(this.myInfo_xpath));
	}

	private WebElement getMyInfoIcon() {
		return findByVisibility(By.xpath(this.myInfoIcon_xpath));
	}

	private WebElement getTraining() {
		return findByVisibility(By.xpath(this.training_xpath));
	}

	private WebElement getTrainingIcon() {
		return findByVisibility(By.xpath(this.trainingIcon_xpath));
	}

	private WebElement getSupport() {
		return findByVisibility(By.xpath(this.support_xpath));
	}

	private WebElement getSupportIcon() {
		return findByVisibility(By.xpath(this.supportIcon_xpath));
	}

	private WebElement getAccountSettings() {
		return findByVisibility(By.xpath(this.accountSettings_xpath));
	}

	private WebElement getAccountSettingsIcon() {
		return findByVisibility(By.xpath(this.accountSettingsIcon_xpath));
	}

	private WebElement getLogoutIcon() {
		return findByVisibility(By.xpath(this.logoutIcon_xpath));
	}

	private WebElement getLogout() {
		waitForVisibility(By.xpath(this.logout_xpath));
		helperMethods.addSystemWait(500, TimeUnit.MILLISECONDS);
		return findByClickability(By.xpath(this.logout_xpath));
	}

	/* ----- methods ----- */

	/***
	 * <h1>clickBidProfile</h1>
	 * <p>
	 * purpose: Select "Bid Profile" from the Account nav menu<br>
	 * App should navigate forward
	 * </p>
	 * 
	 * @return BidNotifications
	 */
	public BidNotifications clickBidProfile() {
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getBidProfile());
		return new BidNotifications(driver);
	}

	/***
	 * <h1>clickMyInfo</h1>
	 * <p>
	 * purpose: Select "My Info" from the Account nav menu<br>
	 * App should navigate forward
	 * </p>
	 * 
	 * @return MyInfo
	 */
	public MyInfo clickMyInfo() {
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getMyInfo());
		return new MyInfo(driver);
	}

	/***
	 * <h1>clickTraining</h1>
	 * <p>
	 * purpose: Select "Training" from the Account nav menu<br>
	 * App should navigate forward
	 * </p>
	 * 
	 * @return Training
	 */
	public Training clickTraining() {
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getTraining());
		return new Training(driver);
	}

	/***
	 * <h1>clickSupport</h1>
	 * <p>
	 * purpose: Select "Support" from the Account nav menu<br>
	 * App should navigate forward
	 * </p>
	 * 
	 * @return Support
	 */
	public Support clickSupport() {
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getSupport());
		return new Support(driver);
	}

	/***
	 * <h1>clickAccountSettings
	 * <h1>
	 * <p>
	 * purpose: Select "Account Settings" from the Account nav menu
	 * </p>
	 * 
	 * @return AccountSettings
	 */
	public AccountSettings clickAccountSettings() {
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getAccountSettings());
		return new AccountSettings(driver);
	}

	/***
	 * <h1>logout</h1>
	 * <p>
	 * purpose: Select "Logout" from the Account nav menu<br>
	 * App should log out
	 * </p>
	 * 
	 * @return SupplierLogin
	 * @throws InterruptedException
	 */
	public SupplierLogin clickLogout() throws InterruptedException {
		((JavascriptExecutor)driver).executeScript("arguments[0].click();",this.getLogout());
		return new SupplierLogin(driver);
	}

	/* ----- verifications ----- */

	/***
	 * <h1>verifyAllOptionsInAccountDropdown</h1>
	 * <p>
	 * purpose: Verify that all the options (buttons + images) in the account
	 * dropdown appear
	 * </p>
	 * 
	 * @return AccountDropDown
	 */
	public AccountDropDown verifyAllOptionsInAccountDropdown() {
		getBidProfile();
		getBidProfileIcon();
		getMyInfo();
		getMyInfoIcon();
		getTraining();
		getTrainingIcon();
		getSupport();
		getSupportIcon();
		getAccountSettings();
		getAccountSettingsIcon();
		getLogout();
		getLogoutIcon();

		System.out.println(GlobalVariables.getTestID() + " INFO: Verified all options and images in the \"Account\" menu have correctly loaded");
		return this;
	}
}
