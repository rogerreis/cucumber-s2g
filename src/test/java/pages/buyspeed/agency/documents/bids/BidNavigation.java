package pages.buyspeed.agency.documents.bids;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import pages.buyspeed.agency.AgencyTopAndSideBars;

/***
 * <h1>Class BidNavigation</h1>
 * @author dmidura
 * <p>details: This class holds page objects and methods to enable navigation on a bid.
 * This class should be inherited by individual bid tab pages
 * <p>includes: Top level bid navigation tabs (General, Bidders, Summary, etc)
 */
public class BidNavigation extends AgencyTopAndSideBars {
	
	// xpaths
	private final String bidGeneralTabLink_xpath = "//table[contains(@class, 'tabbed-nav')]//a[contains(text(), 'General')]";
	private final String bidBiddersTabLink_xpath = "//table[contains(@class, 'tabbed-nav')]//a[contains(text(), 'Bidders')]";
	private final String bidSummaryTabLink_xpath = "//table[contains(@class, 'tabbed-nav')]//a[contains(text(), 'Summary')]";
	
	// other
	private EventFiringWebDriver driver;
	
	public BidNavigation(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}
	
	/* ------ getters ------ */
	private WebElement getBidGeneralTabLink() { return findByVisibility(By.xpath(this.bidGeneralTabLink_xpath)); }
	private WebElement getbidBiddersTabLink() { return findByVisibility(By.xpath(this.bidBiddersTabLink_xpath)); }
	private WebElement getbidSummaryTabLink() { return findByVisibility(By.xpath(this.bidSummaryTabLink_xpath)); }

	/* ------ methods ------ */
	/***
	 * <h1>clickOnBidGeneralTab</h1>
	 * <p>purpose: Click on the bid "General" tab link to<br>
	 * 	navigate to the General tab</p>
	 * @return
	 */
	public BidGeneralTab clickOnBidGeneralTab(){
		this.getBidGeneralTabLink().click();
		return new BidGeneralTab(driver);
	}

	/***
	 * <h1>clickOnBidSummaryTab</h1>
	 * <p>purpose: Click on the bid "Summary" tab link to<br>
	 * 	navigate to the Summary tab</p>
	 * @return
	 */
	public BidSummaryTab clickOnBidSummaryTab(){
		this.getbidSummaryTabLink().click();
		return new BidSummaryTab(driver);
	}

	/***
	 * <h1>clickOnBidBiddersTab</h1>
	 * <p>purpose: Click on the bid "Bidders" tab link to<br>
	 * 	navigate to the Bidders tab</p>
	 * @return
	 */
	public BidBiddersTab clickOnBidBiddersTab(){
		this.getbidBiddersTabLink().click();
		return new BidBiddersTab(driver);
	}
}
