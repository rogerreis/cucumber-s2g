package pages.bidSyncPro.registration;

import static org.junit.Assert.fail;

import java.util.List;

import pages.bidSyncPro.common.BidSyncProCommon;
import pages.bidSyncPro.supplier.common.ChipList;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

/***
 * <h1>Class EnterKeywords</h1>
 * @author dmidura
 * <p>details: This class houses methods and objects related to the objects on the "Enter Keywords" screen that is part of supplier registration</p>
 */
public class EnterKeywords extends BidSyncProCommon {

	// Other
	private EventFiringWebDriver driver;
	HelperMethods helperMethods = new HelperMethods();
	
	// ids
    private final String positiveKeywordInput_id  = "keyword";
    private final String negativeKeywordInput_id  = "negKeyword";
    
    // xpaths
    // Keywords
    private final String posKeywordChipList_xpath = "//mat-chip[@color='accent']/../.";
    private final String negKeywordChipList_xpath = "//mat-chip[@color='danger']/../.";
    
    // Sample Bids
    private final String sampleBidsTxt_xpath      = "//strong[contains(.,'Sample Bids')]";
    
    // Bottom Buttons
    private final String nextButton_xpath         = "//button[contains(span, 'NEXT')]";

    private final String profileErrorTxt_xpath    = "//*[contains(text(), 'Error occurred attempting to update user profile.')]";
	
	public EnterKeywords (EventFiringWebDriver driver) {
		super(driver);
	    this.driver        = driver;
	}
	
	/* ----- getters ----- */
	
	private WebElement getPositiveKeyWordInput() { return findByVisibility(By.id(this.positiveKeywordInput_id));               }
	private WebElement getNegativeKeyWordInput() { return findByVisibility(By.id(this.negativeKeywordInput_id));               }
	private WebElement getNextButton()           { return findByClickability(By.xpath(this.nextButton_xpath)); }
    
    /* ----- methods ----- */
	
	/***
	 * <h1>setPositiveKeyword</h1>
	 * <p>purpose: Input positive keyword, strKeyword, into the "Add Keywords" input and then press RETURN</p>
	 * @param strKeyword = Positive keyword to enter
	 * @return None
	 */
	private EnterKeywords setPositiveKeyword (String strKeyword) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Inputting positive keyword \"%s\" into the keyword input field\n", strKeyword);
        getPositiveKeyWordInput().sendKeys(strKeyword);
        getPositiveKeyWordInput().sendKeys(Keys.RETURN);
		return this;
	}

	/***
	 * <h1>setNegativeKeyword</h1>
	 * <p>purpose: Input negative keyword, strKeyword, into the "Add Negative Keywords (Optional)" input and then press RETURN</p>
	 * @param strKeyword = Negative keyword to enter
	 * @return None
	 */
	private EnterKeywords setNegativeKeyword (String strKeyword) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Inputting negative keyword \"%s\" into the keyword input field\n", strKeyword);
		getNegativeKeyWordInput().sendKeys(strKeyword);
		getNegativeKeyWordInput().sendKeys(Keys.RETURN);
		return this;
	}
	

	/***
	 * <h1>deletePositiveKeyword</h1>
	 * <p>purpose: Delete a keyword from the positive keywords chip list<br>
	 * 	Fail test if keyword isn't in the list</p>
	 * @param strKeyword = keyword to delete (should exist in the list)
	 * @return EnterKeywords
	 */
	public EnterKeywords deletePositiveKeyword (String strKeyword) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Deleting positive keyword \"%s\"\n", strKeyword);
		ChipList keywordChipList = new ChipList(driver, this.posKeywordChipList_xpath);
		keywordChipList.getChipInChipList(strKeyword).deleteChip();
		return this;
	}

	/***
	 * <h1>deleteNegativeKeyword</h1>
	 * <p>purpose: Delete a keyword from the negative keywords chip list<br>
	 * 	Fail test if keyword isn't in the list</p>
	 * @param strKeyword = keyword to delete (should exist in the list)
	 * @return EnterKeywords
	 */
	public EnterKeywords deleteNegativeKeyword (String strKeyword) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Deleting negative keyword \"%s\"\n", strKeyword);
		ChipList keywordChipList = new ChipList(driver, this.negKeywordChipList_xpath);
		keywordChipList.getChipInChipList(strKeyword).deleteChip();
		return this;
	}
	
	/***
	 * <h1>clickNextButton</h1>
	 * <p>purpose: Click on the Next button to navigate to "Get Invited to Bids" screen</p>
	 * @return IncreaseBidRelevancy
	 */
	public SelectNIGPCodes clickNextButton() {
		System.out.println(GlobalVariables.getTestID() + " INFO: Clicking \"NEXT\" button on \"Enter Keywords\" screen");

		// We need to wait for the Sample Bids section to appear,
		// because otherwise it can appear while clicking Next 
		this.scrollIntoViewBottomOfScreen(this.findByPresence(By.xpath(this.sampleBidsTxt_xpath)));

		// Now scroll to NEXT and click
		this.scrollIntoViewBottomOfScreen(By.xpath(this.nextButton_xpath));
	    getNextButton().click();
        return new SelectNIGPCodes(driver);
    }

	/**
	 * <h1>addPositiveKeywords</h1>
	 * <p>purpose: Add positive keywords into the keywords section, clicking <br>
	 * 	return after each addition</p>
	 * @param keywords
	 * @return EnterKeywords
	 */
    public EnterKeywords addPositiveKeywords(String... keywords) {
	    for(String keyword: keywords) {
	    	helperMethods.addSystemWait(1);
	        this.setPositiveKeyword(keyword);
        }
	    return this;
    }

	/**
	 * <h1>addPositiveKeywords</h1>
	 * <p>purpose: Add positive keywords into the keywords section, clicking <br>
	 * 	return after each addition</p>
	 * @param keywords
	 * @return EnterKeywords
	 */
    public EnterKeywords addPositiveKeywords(List<String> keywords) {
        for(String keyword: keywords) {
        	helperMethods.addSystemWait(1);
            this.setPositiveKeyword(keyword);
        }
        return this;
    }

	/**
	 * <h1>addNegativeKeywords</h1>
	 * <p>purpose: Add negative keywords into the keywords section, clicking <br>
	 * 	return after each addition</p>
	 * @param negative keywords
	 * @return EnterKeywords
	 */
    public EnterKeywords addNegativeKeywords(String... keywords) {
        for(String keyword: keywords) {
        	helperMethods.addSystemWait(1);
            this.setNegativeKeyword(keyword);
        }
        return this;
    }
    
    
 	/**
	 * <h1>addNegativeKeywords</h1>
	 * <p>purpose: Add negative keywords into the keywords section, clicking <br>
	 * 	return after each addition</p>
	 * @param negative keywords
	 * @return EnterKeywords
	 */
    public EnterKeywords addNegativeKeywords(List<String> keywords) {
        for(String keyword: keywords) {
        	helperMethods.addSystemWait(1);
            this.setNegativeKeyword(keyword);
        }
        return this;
    }
	
	/* ----- verifications ----- */
	/***
	 * <h1>verifyProfileErrorNotDisplayed</h1>
	 * <p>purpose: When setting keywords, an error can display if something<br>
	 * 	went wrong on the backend. Search for the error for 7 seconds.<br>
	 *  Verify that error doesn't display, and fail test if does</p>
	 *  @return EnterKeywords
	 */
	public EnterKeywords verifyProfileErrorNotDisplayed() {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Verifying that profile error is not displaying on \"Enter Keywords\"");

		List <WebElement> ret = this.findAllOrNoElements(driver, By.xpath(this.profileErrorTxt_xpath));
		
		if(!ret.isEmpty()) {fail("Test Failed: Profile error is displaying on \"Enter Keywords\" screen"); }

		return this;
	}
}
