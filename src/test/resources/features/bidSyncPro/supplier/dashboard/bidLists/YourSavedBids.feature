@must @Bid_List @bidSearch_detail @level3
Feature: Dashboard - Bid List - Your Saved Bids
  This feature tests "Your Saved Bids" bid list
  ARGO-1235: Your Saved Bids
  ARGO-2124: Dates and expiration displaying incorrectly for bids  
  BIDSYNC-152: Make column titles static when user scrolls bids list
  

@ARGO-1235
Scenario Outline: "Your Saved Bids" bid list displays correct table headers
	Given I have logged into the Supplier side as user "National FMC"
	When I have navigated to the "<tab>" bid tab
	Then I verify the List columns are displayed in your saved bids
    
Examples:
	| tab             |
	| Your Saved Bids |

 
@BIDSYNC-152
Scenario Outline: Column titles should be static when the user scrolls the "Your Saved Bids" bid list 
Note: Workaround in place for BIDSYNC-546: Improve performance of bid searches.
	Given I have logged into the Supplier side as user "National FMC"
	And I have saved 8 bids from the "New For You" tab
	And I have navigated to the "<tab>" bid tab
	And those saved bids are displayed in the Your Saved Bids list
	When I view the last bid currently displaying in the bid list in "your saved bid" bid tab
	Then I verify the List columns are displayed in your saved bids
	And I unsave all bids

Examples:
	| tab             |
	| Your Saved Bids |


@ARGO-1235
Scenario Outline: Sort bids using column headers
Note: Workaround in place for BIDSYNC-546: Improve performance of bid searches.
	Given I have logged into the Supplier side as user "National FMC"
	And I have saved 5 bids from the "New For You" tab
	And I have navigated to the "Your Saved Bids" bid tab
	And those saved bids are displayed in the Your Saved Bids list
	When I sort by "<column>"
	Then the List is sorted in <sortOrder1> order by "<column>"
	When I sort by "<column>"
	Then the List is sorted in <sortOrder2> order by "<column>"
	And I unsave all bids
    
	Examples:
	| column    | sortOrder1 | sortOrder2 |
	| Bid Title | ascending | descending  |
	| Bid ID    | ascending | descending  |
	| Agency    | ascending | descending  |
	| State     | ascending | descending  |
	| End Date  | ascending | descending  |
	| Added     | descending  | ascending |

    
@ARGO-2124
Scenario: Dates in the Your Saved Bids tab are correct
Note: Currently "Upgrade Plan" bids are excluded from this test
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to the "New For You" bid tab
	When I select 3 bids by clicking their stars
	When I have navigated to the "Your Saved Bids" bid tab
	Then I will verify that the bids in the list have the correct dates
	And I unsave all bids
   	
	
@ignore @ARGO-1235 @ARGO-2331
Scenario Outline: Save bids for later review
The defect in ARGO-2331 is making this test unstable. Marking as @should until fixed
    
	Given I have logged into the Supplier side as user "National FMC"
	And I have navigated to the "<tab>" bid tab
	When I select <numBids> bids by clicking their stars
	Then those bids stars will show as checked
	When I have navigated to the "Your Saved Bids" bid tab
	Then those bids will be shown in the bid List
	When I select <numBids> bids by clicking their stars
	Then those bids will not be shown in the bid List
	When I have navigated to the "<tab>" bid tab
	Then those bids stars will show as unchecked
    
	Examples:
	| tab         | numBids |
	| New For You | 1       |
	| New For You | 3       |
	| All Bids    | 1       |
	| All Bids    | 3       |
