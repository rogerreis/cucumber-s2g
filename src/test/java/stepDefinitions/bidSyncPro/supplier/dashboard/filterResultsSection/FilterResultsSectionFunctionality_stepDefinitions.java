package stepDefinitions.bidSyncPro.supplier.dashboard.filterResultsSection;

import core.CoreAutomation;

import java.util.*;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.bidSyncPro.common.BidSyncProCommon;
import pages.bidSyncPro.supplier.account.bidProfile.BidNotifications;
import pages.bidSyncPro.supplier.dashboard.BidListTabNames;
import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.bidSyncPro.supplier.dashboard.bidLists.BidList;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.BidEndDateSection;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.CustomTimeFrameSection;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterCriteria;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.FilterResultsSectionNewClass;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.KeywordsSection;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.NegativeKeywordsSection;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.StatesProvincesSection;
import pages.bidSyncPro.supplier.dashboard.filterResultsSection.TimeFrameOptions;
import pages.common.helpers.GlobalVariables;
import pages.common.helpers.HelperMethods;
import pages.common.helpers.RandomHelpers;

import org.junit.Assert;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class FilterResultsSectionFunctionality_stepDefinitions {

	private EventFiringWebDriver driver;
	private HelperMethods helperMethods;
	private BidSyncProCommon bidSyncProCommon;
	private KeywordsSection keywordsSection;

	public FilterResultsSectionFunctionality_stepDefinitions(CoreAutomation automation) {
		this.driver     = automation.getDriver();
		helperMethods   = new HelperMethods();
		bidSyncProCommon = new BidSyncProCommon(driver);
		keywordsSection = new KeywordsSection(driver);
	}
	
	/* ----- helpers ----- */
	
	/***
	 * <h1>inputFilterIntoFilterResults</h1>
	 * <p>purpose: Open filter results section and clear existing filters.
	 * 	Input all values of filter into Filter Results.<br>
	 * 	Note: After setting filter values, all sections header dropdowns will be closed,
	 *	except for the bid end date section (if bid end date was set)
	 * @param filter = FilterCriteria for your filter
	 * @return FilterResultsSectionNewClass
	 */
	private FilterResultsSectionNewClass inputFilterIntoFilterResults (FilterCriteria filter) {
		// Enter this filter into Filter Results
		// and save out with the random name
		return	new FilterResultsSectionNewClass(driver)
//				.openFilterResultsSection()
				.waitForFilterResultsSectionToLoad()
				.closeKeywordsSection()
				.clickClearAllBtn()
				.setFilter(filter);
	}
	
	/***
	 * <h1>inputFilterIntoFilterResults</h1>
	 * <p>purpose: Open filter results section and clear existing filters.
	 * 	Input all values of filter into Filter Results. Click on "Search" button. Wait for spinner to complete<br>
	 * 	Note: After setting filter values, all sections header dropdowns will be closed,
	 *	except for the bid end date section (if bid end date was set)
	 * @param filter = FilterCriteria for your filter
	 */
	private void searchFilterResultsSection(FilterCriteria filter) {
		this.inputFilterIntoFilterResults(filter)
					.clickSearchBtn()
					.waitForSpinner(driver);
	}
	
	/* ----- test steps ----- */
	
	@And("^I have entered generic filters into the Filter Results$")
	public void I_have_entered_generic_filters_into_the_Filter_Results() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Entering generic filters in to Filter Results section");

		// Create a generic filter with a random name
		FilterCriteria filter = new FilterCriteria();
		String strRandomName  = new RandomHelpers().getRandomString();
		filter.setGenericFilter()
			.setFilterName(strRandomName);

		this.inputFilterIntoFilterResults(filter);
	}
	
	
	@And ("^I run a generic search from the Filter Results section$")
	public void I_run_a_generic_search_from_the_Filter_Results_section() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Running generic search from Filter Results");

		// Create a generic search
		FilterCriteria filter = new FilterCriteria();
		filter.setGenericFilter();
		
		// Search
		this.searchFilterResultsSection(filter);
		GlobalVariables.storeObject("filter", filter);
	}

	@When("^I open the Filter Results Section")
	public void I_open_the_Filter_Results_Section () {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Opening Filter Results Section");

		new DashboardCommon(driver)
				.waitForPageLoad();

		new FilterResultsSectionNewClass(driver)
				.verifyFilterResultsSectionClosed()
				.openFilterResultsSection()
				.verifyFilterResultsSectionOpen();
	}
	
	@And ("^I have verified that \"(.*)\" positive default keywords are displayed on the Filter Results section")
	public void I_have_verified_that_positive_default_keywords_are_displayed_on_the_Filter_Results_section( String keywords) {
		String[] keywordArray = keywords.split(",");
		List<String> keywordList = Arrays.asList(keywordArray);
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Verifying default keywords \"%s\" display in Filter Results Section\n", keywordList.toString());
		
		new FilterResultsSectionNewClass(driver)
			.verifyFilterResultsSectionOpen()
			.getFilterResultsSectionNewClass()
			.openKeywordsSection()
			.getKeywordsSectionAccess()
			.getPositiveKeywordsChipList()
			.verifyChipsInChipList(keywordList);
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Positive keywords appear in Filter Results section");
	}

	@When("^I close the Filter Results Section")
	public void I_close_the_Filter_Results_Section () {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Closing Filter Results Section");

		new FilterResultsSectionNewClass(driver)
				.verifyFilterResultsSectionOpen()
				.closeFilterResultsSection()
				.verifyFilterResultsSectionClosed();
	}

	@When("^I click on the Clear All button on the Filter Results Section$")
	public void I_click_on_the_Clear_All_button_on_the_Filter_Results_Section() {
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on the Clear All button on the Filter Results Section\n");

		new FilterResultsSectionNewClass(driver)
				.clickClearAllBtn();
	}
	
	@When ("^I click on the Edit Default Filters button on the Filter Results Collapsible section$")
	public void I_click_on_the_Edit_Default_Filters_button_on_the_Filter_Results_Collapsible_section () {
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on the \"Edit Default Filters\" button on the Filter Bids Collapsible section\n");
		
		// Open filter results section
		// And click on the Edit Default button
		new FilterResultsSectionNewClass(driver)
//				.openFilterResultsSection()
				.clickEditDefaultBtn();
	}

	@When("^I click on the Load Default button on the Filter Results Collapsible section$")
	public void I_click_on_the_Load_Default_button_on_the_Filter_Results_Collapsible_section () {
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Clicking on the \"Load Default\" button on the Filter Bids Collapsible section");
		helperMethods.addSystemWait(3);

		// Open Filter Results section
		// And click on Load Default button
		new FilterResultsSectionNewClass(driver)
//				.openFilterResultsSection()
				.clickLoadDefaultBtn();
	}


	@When ("^I manually enter a Show Bids to date that is before the Show Bids From date$")
	public void I_manually_enter_a_Show_Bids_to_date_that_is_before_the_Show_Bids_From_date() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Running search with custom time frame of start date = one month from today, end date = today");
		
		// Set a filter with a custom date
		// Bid Start Date = today
		// Bid End Date = one month from now
		FilterCriteria filter = new FilterCriteria();
		filter.setGenericKeywords()
				.setSelectTimeFrame(TimeFrameOptions.CUSTOM_TIME_FRAME)
				.setBidStartDateToOneMonthFromToday()
				.setBidEndDateToToday();
		
		this.searchFilterResultsSection(filter);
	}
	
	@When ("^I uncheck the Include past bids checkbox and click Search$")
	public void I_uncheck_the_Include_past_bids_checkbox_toggle_and_click_Search() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Unchecking the \"Include past bids\" checkbox and clicking \"Search\"");

		new FilterResultsSectionNewClass(driver)
				.openBidEndDateSection()
				.uncheckIncludePastBidsCheckbox()
				.clickSearchBtn();
	}

    @When ("^I enter and save generic keywords for the Filter Results Negative Keywords section$")
    public void I_enter_and_save_generic_keywords_for_the_Filter_Results_Negative_Keywords_section() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Entering generic keywords into the Filter Results Negative Keywords section");

		// 1. Grab some generic negative keywords
		FilterCriteria filter = new FilterCriteria();
		filter.setGenericNegativeKeywords();

		// 2. Open Filter Results
		// 3. Add negative keywords into the Negative Keywords section
		new FilterResultsSectionNewClass(driver)
//				.openFilterResultsSection()
				.openNegativeKeywordsSection()
				.addNegativeKeywords(filter.getNegativeKeywords());
    }

    @When ("^I enter and save generic keywords for the Filter Results States/Provinces section$")
    public void I_enter_and_save_generic_keywords_for_the_Filter_Results_StatesProvinces_section() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Entering generic keywords into the Filter Results States/Provinces section");
		// 1. Grab some generic states/provinces
		FilterCriteria filter = new FilterCriteria();
		filter.setGenericStatesProvinces();

		// 2. Open Filter Results
		// 3. Add states/provinces into the States/Provinces section
		new FilterResultsSectionNewClass(driver)
//				.openFilterResultsSection()
				.openStatesProvincesSection()
				.uncheckSearchAllStatesProvincesCheckbox()
				.addStatesProvinces(filter.getStatesProvinces());
    }

    @When ("^I enter and save generic keywords for the Filter Results Keywords section$")
    public void I_enter_and_save_generic_keywords_for_the_Filter_Results_Keywords_section() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Entering generic keywords into the Filter Results Keywords section");
		// 1. Grab some generic keywords
		FilterCriteria filter = new FilterCriteria();
		filter.setGenericKeywords();

		// 2. Open Filter Results
		// 3. Add keywords into the Keywords section
		new FilterResultsSectionNewClass(driver)
//				.openFilterResultsSection()
				.openKeywordsSection()
				.addKeywords(filter.getKeywords());
    }
    
    @When ("^I select \"(.*)\" from the SELECT TIME FRAME dropdown$")
    public void I_select_value_from_the_SELECT_TIME_FRAME_dropdown(String timeFrameValue) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Selecting \"" + timeFrameValue + "\" from \"SELECT TIME FRAME\" dropdown");

		// 1. Open Filter Results
		// 2. Open Bid End Date section
		// 3. Check Include Past Bids checkbox
		// 4. Select the timeFrameValue from "SELECT TIME FRAME" dropdown
		new FilterResultsSectionNewClass(driver)
//				.openFilterResultsSection()
				.openBidEndDateSection()
				.checkIncludePastBidsCheckbox()
				.openTimeFrameDropdown()
				.selectTimeFrame(TimeFrameOptions.fromString(timeFrameValue));
    }

    @When ("^I view the Filter Results Keywords Section$")
    public void I_view_the_Filter_Results_Keywords_Section() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Opening Filter Results section and viewing Keywords Section");

		new FilterResultsSectionNewClass(driver)
//				.openFilterResultsSection()
				.openKeywordsSection();
    }

    @When ("^I view the Filter Results Negative Keywords Section$")
    public void I_view_the_Filter_Results_Negative_Keywords_Section() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Opening Filter Results section and viewing Negative Keywords Section");

		new FilterResultsSectionNewClass(driver)
//				.openFilterResultsSection()
				.openNegativeKeywordsSection();
    }

    @When ("^I view the Filter Results States/Provinces Section$")
    public void I_view_the_Filter_Results_StatesProvinces_Section() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Opening Filter Results section and viewing States/Provinces Section");
		bidSyncProCommon.waitForSpinner(driver);
		new FilterResultsSectionNewClass(driver)
//				.openFilterResultsSection()
				.openStatesProvincesSection();
    }

    @When ("^I view the Filter Results Bid End Date Section$")
    public void I_view_the_Filter_Results_Bid_End_Date_Section() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Opening Filter Results section and viewing Bid End Date Section");

		new FilterResultsSectionNewClass(driver)
//				.openFilterResultsSection()
				.openBidEndDateSection();
    }

	@Then("^I will verify that no search filters are currently displaying on the Filter Results Section$")
	public void I_will_verify_that_no_search_filters_are_currently_displaying_on_the_Filter_Results_Section() {
		System.out.printf(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that no filters are currently displaying on the Filter Results Section\n");

		new FilterResultsSectionNewClass(driver)
				.verifyFilterResultsSectionIsCleared();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Filter Results section has been cleared");
	}

    @Then ("^I will verify that the Filter Results Keywords Section initializes correctly$")
    public void I_will_verify_that_the_Filter_Results_Keywords_Section_initializes_correctly() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Keywords section has properly intialized");
		keywordsSection.verifySectionInitialization();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Keywords section properly initialized");
    }

    @Then ("^I will verify that the Filter Results Negative Keywords Section initializes correctly$")
    public void I_will_verify_that_the_Filter_Results_Negative_Keywords_Section_initializes_correctly() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Negative Keywords section has properly intialized");

		new NegativeKeywordsSection(driver)
				.verifySectionInitialization();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Negative Keywords section properly initialized");
    }

    @Then ("^I will verify that the Filter Results States/Provinces Section initializes correctly$")
    public void I_will_verify_that_the_Filter_Results_StatesProvinces_Section_initializes_correctly() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that States/Provinces section has properly intialized");

		new StatesProvincesSection(driver)
				.verifySectionInitialization();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: States/Provinces section properly initialized");
    }

    @Then ("^I will verify that the Filter Results Bid End Date Section initializes correctly$")
    public void I_will_verify_that_the_Filter_Results_Bid_End_Date_Section_initializes_correctly() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Bid End Date section has properly intialized");

		new BidEndDateSection(driver)
				.verifySectionInitialization();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Bid End Date section properly initialized");
    }


    @Then ("^I will verify I will not see the Custom Time Frame date selectors$")
    public void I_will_verify_I_will_not_see_the_Custom_Time_Frame_date_selectors() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Custom Time Frame date selectors do not appear");

		new CustomTimeFrameSection(driver)
				.verifyCustomTimeFrameDateSelectorsAreNotVisible();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Custom date selectors are not visible");
    }

    @Then ("^I will verify I will see the Custom Time Frame date selectors$")
    public void I_will_verify_I_will_see_the_Custom_Time_Frame_date_selectors() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that Custom Time Frame date selectors appear");

		new CustomTimeFrameSection(driver)
				.verifyCustomTimeFrameDateSelectorsAreVisible();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Custom date selectors are visible");
    }
    
    @Then("^I will verify that Show Bids From date selector is defaulted to 1/1/2016$")
    public void I_will_verify_that_Show_Bids_From_date_selector_is_defaulted_to_1_1_2016(){
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that \"Show Bids From\" selector defaults to 1/1/2016");
    	
		new CustomTimeFrameSection(driver)
			.verifyShowBidsFromDisplaysDefault();
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: \"Show Bids From\" selector defaults to 1/1/2016");
    }

	@Then ("^I will verify that the Include past bids checkbox does not uncheck$")
	public void I_will_verify_that_the_Include_past_bids_checkbox_does_not_uncheck() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that \"Include past bids\" checkbox does not uncheck");

		new BidEndDateSection(driver)
				.verifyIncludePastBidsCheckboxIsUnchecked();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Include Past Bids checkbox is unchecked");
	}
	
	@Then ("^validation will trigger on Show Bids From and I will not be returned any bids$")
	public void validation_will_trigger_on_Show_Bids_From_and_I_will_not_be_returned_any_bids() throws Throwable {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that validation triggers on \"Show Bids to\" and no search results are returned in \"All Bids\" tab");

		// 1. Verify validation was triggered on "Show Bids to" field
		new CustomTimeFrameSection(driver)
				.verifyShowBidsToValidationWasTriggered();
		
		// 2. Verify there are no returned bids in the All Bids list
		new BidList(driver)
				.verifyNoSearchResultsReturn();
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: \"Show Bids to\" validation was triggered and \"All Bids\" tab doesn't return results");
	}

	@Then("^I will verify that the filters set from my user profile are displaying on the Filter Results$")
	public void I_will_verify_that_the_filters_set_from_my_user_profile_are_displaying_on_the_Filter_Results() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying that positive and negative keywords displaying on Filter Results match User Profile Keywords section");
		FilterResultsSectionNewClass filterResults = new FilterResultsSectionNewClass(driver);

		// Grab displaying positive and negative keywords
		List <String> positiveKeywords = filterResults.openKeywordsSection()
													.getPositiveKeywordsChipList()
													.getTextForAllChipsInChipList();

		List <String> negativeKeywords = filterResults.openNegativeKeywordsSection()
													.getNegativeKeywordsChipList()
													.getTextForAllChipsInChipList();

		System.out.println(GlobalVariables.getTestID() + " Positive Keywords = "+ positiveKeywords.toString());
		System.out.println(GlobalVariables.getTestID() + " Negative Keywords = "+ negativeKeywords.toString());
		
		Assert.assertFalse("Test Failed: Default Keywords are not displayed on Filter Results after clicking \"Load Defaults\"",          positiveKeywords.isEmpty());
		Assert.assertFalse("Test Failed: Default Negative Keywords are not displayed on Filter Results after clicking \"Load Defaults\"", negativeKeywords.isEmpty());
		
		// Verify that positive and negative keywords displaying on the Filter Results are displaying
		// on the Bid Profile
		new BidNotifications(driver)
				.openAccountDropDown()
				.clickBidProfile()  
				.waitForKeywordChipListToLoad()
				.openKeywordsDialog()
				.verifyPositiveKeywordsSavedInChipList(positiveKeywords)
				.verifyNegativeKeywordsSavedInChipList(negativeKeywords);
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: OnClick of \"Load Defaults\" displays positive and negative keywords that match the user's"
					+ " keywords on the User Profile");
	}
	
	 @Then ("^I will verify that I can edit my default filters from the User Profile page$")
	 public void I_will_verify_that_I_can_edit_my_default_filters_from_the_User_Profile_page () {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying user can edit default filters from User Profile page");
		String strPositiveKeyword = "water";
		String strNegativeKeyword = "river";
		helperMethods.addSystemWait(3);

		// 1. Delete all the Positive and Negative keywords
		// 2. Set a positive keyword and verify it displays
		// 3. Set a negative keyword and verify it displays
		// 4. Close popup and click "Back to Bids"
		// 5. Verify "Back to Bids" navigation to New For You bid list
		new BidNotifications(driver)
				.waitForKeywordChipListToLoad()
				.openKeywordsDialog()
				.verifyPositiveKeywordsNotEmpty()
				.verifyNegativeKeywordsNotEmpty()
				.deleteAllPositiveKeywords()
				.verifyPositiveKeywordsChipListIsEmpty()
				.deleteAllNegativeKeywords()
				.verifyNegativeKeywordsChipListIsEmpty()
				.setPositiveKeyword(strPositiveKeyword)
				.verifyPositiveKeywordSavedInChipList(strPositiveKeyword)
				.setNegativeKeyword(strNegativeKeyword)
				.verifyNegativeKeywordSavedInChipList(strNegativeKeyword)
				.clickCloseKeywordsDialog()
				.clickBackToBids()
				.verifyTabIsHighlightedAndSelected(BidListTabNames.NEW_FOR_YOU);
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Use was able to edit default filters on User Profile page by clicking \"Edit Default Filters\" on the Filter Results Collapsible section");
	 }
	
	@Then ("^I will verify that the Filter Results collapsible section is closed$")
	public void I_will_verify_that_the_Filter_Results_collapsible_section_is_closed() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying Filter Results collapsible section is closed");

		new DashboardCommon(driver)
				.verifyFilterResultsSectionClosed();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Filter Results section is closed");
	}

	@Then ("^I will verify that the Filter Results collapsible section is open$")
	public void I_will_verify_that_the_Filter_Results_collapsible_section_is_open() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying Filter Results collapsible section is open");

		new DashboardCommon(driver)
				.verifyFilterResultsSectionOpen();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Filter Results section is open");
	}

    @Then ("^I will verify that the generic keywords display in the Filter Results Keywords section chip list$")
    public void I_will_verify_that_the_keyword_displays_in_the_Filter_Results_Keywords_section_chip_list() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying generic keywords displays in the Filter Results Keywords section chip list");

		// Grab our generic keywords
		FilterCriteria filter = new FilterCriteria();
		filter.setGenericKeywords();
		// And verify they're showing up in the Keywords chip list
		keywordsSection
				.getPositiveKeywordsChipList()
				.verifyChipsInChipList(filter.getKeywords());

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Chips appear in Keywords chip list");
    }
    
    @Then ("^I will verify that I can delete each chip from the Filter Results Keywords chip list")
    public void I_will_verify_that_I_can_delete_each_chip_from_the_Filter_Results_Keywords_chip_list() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Deleting each chip in Keywords chip list and verifying deletion");

		// Grab our generic keywords
		FilterCriteria filter = new FilterCriteria();
		filter.setGenericKeywords();
	
		// Delete and verify each deletion
		keywordsSection
				.getPositiveKeywordsChipList()
				.verifyDeletionOfEachChipInChipList(filter.getKeywords());

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Chips successfully deleted from Keywords chip list");
    }

    @Then ("^I will verify that the generic keywords display in the Filter Results Negative Keywords section chip list$")
    public void I_will_verify_that_the_keyword_displays_in_the_Filter_Results_Negative_Keywords_section_chip_list() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying generic negative keywords displays in the Filter Results Negative Keywords section chip list");

		// Grab our generic negative keywords
		FilterCriteria filter = new FilterCriteria();
		filter.setGenericNegativeKeywords();

		// And verify they're showing up in the Negative Keywords chip list
		new NegativeKeywordsSection(driver)
				.getNegativeKeywordsChipList()
				.verifyChipsInChipList(filter.getNegativeKeywords());

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Chips appear in Negative Keywords chip list");
    }

    @Then ("^I will verify that I can delete each chip from the Filter Results Negative Keywords chip list")
    public void I_will_verify_that_I_can_delete_each_chip_from_the_Filter_Results_Negative_Keywords_chip_list() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Deleting each chip in Negative Keywords chip list and verifying deletion");

		// Grab our generic negative keywords
		FilterCriteria filter = new FilterCriteria();
		filter.setGenericNegativeKeywords();
	
		// Delete and verify each deletion
		new NegativeKeywordsSection(driver)
				.getNegativeKeywordsChipList()
				.verifyDeletionOfEachChipInChipList(filter.getKeywords());

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Chips successfully deleted from Negative Keywords chip list");
    }

    @Then ("^I will verify that the generic keywords display in the Filter Results States/Provinces section chip list$")
    public void I_will_verify_that_the_keyword_displays_in_the_Filter_Results_StatesProvinces_section_chip_list() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying generic states/provinces displays in the Filter Results States/Provinces section chip list");

		// Grab our generic states/provinces 
		FilterCriteria filter = new FilterCriteria();
		filter.setGenericStatesProvinces();

		// And verify they're showing up in the States/Provinces chip list
		new StatesProvincesSection(driver)
				.getStatesProvincesChipList()
				.verifyChipsInChipList(filter.getStatesProvinces());

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Chips displaying in States/Provinces chip list");
    }

    @Then ("^I will verify that I can delete each chip from the Filter Results States/Provinces chip list")
    public void I_will_verify_that_I_can_delete_each_chip_from_the_Filter_Results_StatesProvinces_chip_list() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Deleting each chip in States/Provinces chip list and verifying deletion");

		// Grab our generic keywords
		FilterCriteria filter = new FilterCriteria();
		filter.setGenericStatesProvinces();
	
		// Delete and verify each deletion
		new StatesProvincesSection(driver)
				.getStatesProvincesChipList()
				.verifyDeletionOfEachChipInChipList(filter.getStatesProvinces());

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Chips successfully deleted from States/Provinces chip list");
    }

	@Then ("^I will verify that the Filter Results Section initializes correctly$") 
	public void I_will_verify_that_the_Filter_Results_Section_initializes_correctly () {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Determining if Filter Results section intialized properly");

		// Verify Filter Results initializes correctly:
		// 1. Keywords section open, others closed
		// 2. Each section displays correct title and a dropdown toggle icon
		// 3. Bottom buttons are all displaying
		new FilterResultsSectionNewClass(driver)
			.verifyFilterResultsInitialization();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Filter Results Section initialized properly.");
	}

	@When("^I enter and save keyword \"(.*)\" for the Filter Results Keywords section$")
	public void I_enter_and_save_keyword_for_the_Filter_Results_Keywords_section(String strKeyword) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Entering given keyword into the Filter Results Keywords section");
		try {
			keywordsSection.addKeyword(strKeyword);
		} catch (Exception exp) {
			Assert.fail("TEST FAILED : Failed to enter keyword");
		}
	}

	@Then("^I will verify that the keyword \"(.*)\" display in the Filter Results Keywords section chip list$")
	public void I_will_verify_that_given_keyword_displays_in_the_Filter_Results_Keywords_section_chip_list(
			String keyword) {
		try {
			System.out.println(
					"---TEST STEP: Verifying given keywords displays in the Filter Results Keywords section chip list");
			//UI shows keyword text as 25 char. But the whole text can be visible on mouse hover
			keywordsSection.getPositiveKeywordsChipList().verifyChipInChipList(keyword.substring(0, 25)+"...");
		} catch (Exception exp) {
			Assert.fail("TEST FAILED : Keyword not in Keywords chip list");
		}
	}

	@Then("^I will verify that I can delete keyword \"(.*)\" from the Filter Results Keywords chip list")
	public void I_will_verify_that_I_can_delete_keyword_from_the_Filter_Results_Keywords_chip_list(String keyword) {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Deleting Keyword from chip list and verifying deletion");
		try {
			//UI shows keyword text as 25 char. But the whole text can be visible on mouse hover
			keywordsSection.getPositiveKeywordsChipList().verifyDeletionOfChipInChipList(keyword.substring(0, 25)+"...");
		} catch (Exception exp) {
			Assert.fail("TEST FAILED : Not able to delete Keyword from Keywords chip list");
		}
	}
}

