/**
 * 
 */
package pages.common.pageObjects.toBeDeprecated;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pages.common.helpers.GlobalVariables;

/**
 * @author jlara
 *
 */
@Deprecated
public class DocElement {
	
	//driver shared by all DocElement objects.
	private static WebDriver myDriver = null;
	
	/***
	 * 
	 * @param driver
	 */
	public static void setDriver(WebDriver driver) {
		myDriver = driver;
	} // end of static setDriver()
	
	private String myLocationExpression;
	private ByTypes myByType;
	private String myName;
	private WebElement myWebElement;
	
	/**
	 * @throws Exception *
	 * 
	 */
	public DocElement() throws Exception {
		if(DocElement.myDriver == null)
			throw new Exception("Must call DocElement.setDriver(driver); before use.");
	} // end of constructor
	
	/***
	 * 
	 * @param name
	 * @param byType
	 * @param locationExpression
	 * @throws Exception
	 */
	public DocElement(String name, ByTypes byType, String locationExpression) throws Exception {
		if(DocElement.myDriver == null)
			throw new Exception("Must call DocElement.setDriver(driver); before use.");
		
		this.setName(name);
		this.setByType(byType);
		this.setLocationExpression(locationExpression);
		System.out.printf(GlobalVariables.getTestID() + " INFO: Adding PageElement %s {%s: %s } in %s%n",myName, myByType.toString(), myLocationExpression, this.getClass());
		//this.findElementByType();
	} // end of constructor
	
	public void findElement() {
		if (!myName.isEmpty() && !myLocationExpression.isEmpty())
			this.findElementByType();
	} // end of findElement()
	
	/***
	 * 
	 */
	private void findElementByType() {
		By by = null;
		switch(myByType) {
		    case ClassName: 		by = By.className(myLocationExpression);  break;
		    case CssSelector: 		by = By.cssSelector(myLocationExpression);  break;
		    case Id: 				by = By.id(myLocationExpression);  break;
		    case LinkText: 			by = By.linkText(myLocationExpression);   break;
		    case Name: 				by = By.name(myLocationExpression);              break;
		    case PartialLinkText: 	by = By.partialLinkText(myLocationExpression);   break;
		    case TagName: 			by = By.tagName(myLocationExpression);	         break;
		    case XPath: 			by = By.xpath(myLocationExpression);	    break;
		    default:				by = null;  break;
		} // end of switch

		System.out.printf(GlobalVariables.getTestID() + " INFO: Finding/Registering PageElement %s {%s: %s } in %s%n",myName, myByType.toString(), myLocationExpression, this.getClass());
		myWebElement = myDriver.findElement(by);
	} // end of findElementByType()
	
	/***
	 * 
	 * @return
	 */
	public WebElement getElement() {
		return myWebElement;
	} // end of getElement()
	
	
	/**
	 * @return the name
	 */
	public String getName() {
		return myName;
	} // end of getName()

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.myName = name;
	} // end of setName()
	
	/**
	 * @return the byType
	 */
	public ByTypes getByType() {
		return myByType;
	} // end of getByType()

	/**
	 * @param byType the byType to set
	 */
	public void setByType(ByTypes byType) {
		this.myByType = byType;
	} // end of setByType()
	
	/**
	 * @return the locationExpression
	 */
	public String getLocationExpression() {
		return myLocationExpression;
	} // end of getLocationExpression()

	/**
	 * @param locationExpression the locationExpression to set
	 */
	public void setLocationExpression(String locationExpression) {
		this.myLocationExpression = locationExpression;
	} // end of setLocationExpression()
	
	
	public boolean isVisible() {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Determining visibility of %s {%s: %s } in %s%n",myName, myByType.toString(), myLocationExpression, this.getClass());	
		return myWebElement.getCssValue("visibility").equals("visible") && myWebElement.isDisplayed();
	} // end of isVisible()
	
	
	public String getText() {
		String text = myWebElement.getText().trim();
		return text;
	}
		
}
