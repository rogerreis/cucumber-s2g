package pages.bidSyncPro.registration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;

import pages.bidSyncPro.common.BidSyncProCommon;
import pages.common.helpers.Constants;
import pages.common.helpers.GlobalVariables;

/***
 * <h1>Class SetYourPassword</h1>
 * 
 * @author dmidura
 * <p>
 * details: This class houses methods and objects related to the objects
 * on the "Set Your Password" screen that is part of supplier
 * registration
 * </p>
 */
public class SetYourPassword extends BidSyncProCommon {

	// ids
	private final String nextButton_id           = "setPasswordButton";
	private final String setPasswordInput_id     = "passwordSet";
	private final String confirmPasswordInput_id = "confirmPassword";
	private final String passwordTitle_id        = "passwordTitle";
	// xpaths
	private final String tAndC_xpath            		= "//mat-checkbox[@id='acceptTerms'][contains(.,\"Yes, I understand and agree to the Bidsync Terms of Service, including the User Agreement and Privacy Policy.\")]";
	private final String setPasswordHeader_xpath		= "//div[@id='setYourPassword']/h1";
	private final String setPasswordSubHeader_xpath		= "//*[@id='setYourPasswordReason']/span";
	private final String showHideNewPassword_xpath		= "//mat-form-field[@id='matPasswordFormField']//button[@type='button']"; //to ne removed if not using -same as  showhidePassword_xpath
	private final String showHideConfirmPassword_xpath 	= "//mat-form-field[@id='matConfirmPasswordFormField']//button[@type='button']";
	private final String footerHelpPhone_xpath 			= "//*[@id='fxFooterLayout']/span[contains(text(),'(800) 990-9339')]";
	private final String footerHelpEmail_xpath 			= "//*[@id='fxFooterLayout']/span[contains(text(),'s2g-support@periscopeholdings.com')]";
	private final String errorMessage_xpath 			= "//*[@id='matConfirmPasswordError']/span";
	private final String invalidPasswordError_xpath     ="//*[@id='passwordSecurityRule']";
	
	private String passwordRequirementPartial_xpath		= "//*[@id=\"passwordForm\"]//li[contains(text(),'";
	
	// Other

	private EventFiringWebDriver driver;

	private String testPassword = "Pass1234";
	
	public SetYourPassword(EventFiringWebDriver driver) {
		super(driver);
		this.driver = driver;
	}

	/* ----- getters ----- */

	private WebElement getSetPasswordInput() {
		return findByVisibility(By.id(this.setPasswordInput_id));
	}

	private WebElement getConfirmPasswordInput() {
		return findByVisibility(By.id(this.confirmPasswordInput_id));
	}

	private WebElement getNextButtonId() {
		return findByVisibility(By.id(this.nextButton_id));
	}

	private WebElement getNextButton() {

		// Next Button starts out disabled.
		// Do not click until it "Next" button is enabled and the validation for
		// password is complete
		int time = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, time).withMessage(
				"Test Timed Out: Password Validation did not successfully trigger or \"NEXT\" button did not become clickable after \"" + time + "\" seconds")
				.until(ExpectedConditions.and(ExpectedConditions.visibilityOfElementLocated(By.id(this.nextButton_id)),
						ExpectedConditions.elementToBeClickable(By.id(this.nextButton_id)),
						ExpectedConditions.attributeContains(
								By.xpath("//li[contains(text(), 'between 8 and 128 characters')]"), "class", "success"),
						ExpectedConditions.attributeContains(
								By.xpath("//p[contains(., 'Any 3 of the following criteria:')]"), "class", "success"),
						ExpectedConditions.invisibilityOfElementLocated(By.xpath("//mat-error"))));

		return findByClickability(By.id(this.nextButton_id));
	}

	private WebElement getPasswordTitle() {
		return findByVisibility(By.id(this.passwordTitle_id));
	}

	private WebElement getSetPasswordHeader() {
		return findByVisibility(By.xpath(this.setPasswordHeader_xpath));
	}
	
	private WebElement getSetPasswordSubHeader() {
		return findByVisibility(By.xpath(this.setPasswordSubHeader_xpath));
	}
	
	private WebElement getShowHideNewPassword() {
		return findByVisibility(By.xpath(this.showHideNewPassword_xpath));
	}
	
	private WebElement getShowHideConfirmPassword() {
		return findByVisibility(By.xpath(this.showHideConfirmPassword_xpath));
	}
	
	private WebElement getFooterHelpPhone() {
		return findByVisibility(By.xpath(footerHelpPhone_xpath));
	}
	
	private WebElement getFooterHelpEmail() {
		return findByVisibility(By.xpath(footerHelpEmail_xpath));
	}
	
	private WebElement getPasswordReqMessage(String reqMsg) {
		passwordRequirementPartial_xpath = passwordRequirementPartial_xpath + reqMsg +"')]";
		return findByVisibility(By.xpath(passwordRequirementPartial_xpath));
	}
    
	/* ------ helpers ----- */
    
    private boolean isEnabled(WebElement element) {
    	if(element.isEnabled()) {
    		return true;
    	}else {
    		return false;
    	}
    }	

	/* ----- methods ----- */

	/***
	 * <h1>setAndConfirmPassword</h1>
	 * <p>
	 * purpose: Given a strPassword, enter into both "Set Password" and "Confirm
	 * Password" fields.
	 * 
	 * @param strPassword = the password to set
	 * @return None
	 */
	public GetInvitedToBids setAndConfirmPassword(String strPassword) {
		System.out.printf(GlobalVariables.getTestID() + " INFO: Entering \"%s\" password into Set Password and Confirm Password fields\n",
				strPassword);

		getSetPasswordInput().sendKeys(strPassword);
		getConfirmPasswordInput().sendKeys(strPassword);
		getNextButton().click();
		return new GetInvitedToBids(driver);
	}

	private void inputValue(WebElement element, String value) {
		element.clear();
		element.sendKeys(value != null ? value.trim() : value);
	}

	/**
	 * private method to reset the show and hide password button
	 * 
	 * @throws ElementNotFoundException
	 */
	private void resetShowHideNewPassword() throws ElementNotFoundException {
		if (getShowHideNewPassword().getAttribute("class").equals("mat-icon-button mat-button-base active")) {
			getShowHideNewPassword().click();
		}
	}
	
	/**
	 * private method to reset the show and hide confirm password button
	 * 
	 * @throws ElementNotFoundException
	 */
	private void resetShowHideConfirmPasswordIcon() throws ElementNotFoundException {
		if (getShowHideConfirmPassword().getAttribute("class").equals("mat-icon-button mat-button-base active")) {
			getShowHideConfirmPassword().click();
		}
	}

	/* ------ verifications ------ */

	/***
	 * <h1>verifyNoTandC</h1>
	 * <p>
	 * purpose: Verify that no Terms & Conditions checkbox or text<br>
	 * appears on the "Set Your Password" screen. Fail test if either is visible
	 * </p>
	 * 
	 * @return SetYourPassword
	 */
	public SetYourPassword verifyNoTandC() {
		if (this.doesPageObjectExist(driver, By.xpath(this.tAndC_xpath))) {
			fail("Test Failed: T&C is visible on \"Set Your Password\" screen");
		}
		System.out.println(GlobalVariables.getTestID() + " INFO: Verified no T&C appears on \"Set You Password\" screen");
		return this;
	}
	
	/**
	 * <h1>verifySetPasswordUrl()</h1>
	 * Method to verify set password url
	 * 
	 * 
	 * @return SetYourPassword
	 */
	public SetYourPassword verifySetPasswordUrl() {
		int time = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, time)
			.withMessage("Test Failed: Page did not navigate to " + Constants.setPasswordUrl + " after \"" + time + "\" seconds")
			.until(ExpectedConditions.urlToBe(Constants.setPasswordUrl));
		return this;
	}

	/**
	 * <h1>verifyNextButtonDisabled()</h1>
	 * Method to verify that the next button is disabled
	 * 
	 * @return SetYourPassword
	 */
	public SetYourPassword verifyNextButtonDisabled() {
		if (isEnabled(getNextButtonId())) {
			fail("Test Failed: Next Button is visible on \"Set Your Password\" screen");
		}
		return this;
	}

	/**
	 * <h1>verifyNewPasswordEnabled()</h1>
	 * Method to verify that the new password input is enabled
	 * 
	 * @return SetYourPassword
	 */
	public SetYourPassword verifyNewPasswordEnabled() {
		if (!isEnabled(getSetPasswordInput())) {
			fail("Test Failed: New Password input is not enabled on \"Set Your Password\" screen");
		}
		return this;
	}

	/**
	 * <h1>verifyConfirmPasswordEnabled()</h1>
	 * Method to verify that the confirm password input is enabled
	 * 
	 * @return SetYourPassword
	 */
	public SetYourPassword verifyConfirmPasswordEnabled() {
		if (!isEnabled(getConfirmPasswordInput())) {
			fail("Test Failed: Confirm Password input is not enabled on \"Set Your Password\" screen");
		}
		return this;
	}

	/**
	 * <h1>verifyPageInitialization()</h1>
	 * <p>Verify that the Set password page is loaded properly
	 * <p>Included all UI elements including header and footer
	 * 
	 * @return SetYourPassword
	 * @throws ElementNotFoundException
	 */
	public SetYourPassword verifyPageInitialization() throws ElementNotFoundException {
		this.getSetPasswordHeader();
		this.getSetPasswordSubHeader();
		this.getPasswordTitle();
		this.getSetPasswordInput();
		this.getShowHideNewPassword();
		this.getConfirmPasswordInput();
		this.getShowHideConfirmPassword();
		this.getNextButtonId();
		this.getFooterHelpPhone();
		this.getFooterHelpEmail();
		return this;
	}
	/**
	 * <h1>verifyPasswordIsHiddenOnPageLoad</h1>
	 * Method to verify password entered is hidden by default
	 * 
	 * @return SetYourPassword
	 */
	
	public SetYourPassword verifyPasswordIsHiddenOnPageLoad() {
		inputValue(getSetPasswordInput(), testPassword);
		assertEquals(getSetPasswordInput().getAttribute("type"), "password");
		return this;
	}
	
	/**
	 * <h1>verifyConfirmPasswordIsHiddenOnPageLoad</h1>
	 * 
	 * <p>Method to verify confirm password is hidden by default
	 * @return SetYourPassword
	 */
	public SetYourPassword verifyConfirmPasswordIsHiddenOnPageLoad() {
		inputValue(getConfirmPasswordInput(), testPassword);
		assertEquals(getConfirmPasswordInput().getAttribute("type"), "password");
		return this;
	}
	
	/**
	 * <h1>inputPasswords</h1>
	 * Method to enter passwords
	 * 
	 * @param newPassword
	 * @param confirmPassword
	 * @return SetYourPassword
	 */
	public SetYourPassword inputPasswords(String newPassword, String confirmPassword) {
		inputValue(this.getSetPasswordInput(), newPassword);
		inputValue(this.getConfirmPasswordInput(), confirmPassword);
		getConfirmPasswordInput().sendKeys(Keys.TAB);
		return this;
	}

	/**
	 * <h1>verifyPasswordDoesNotMatchErrorMessage</h1>
	 *<p> Method to verify Password does not match error message.
	 * 
	 * @return SetYourPassword
	 */
	public SetYourPassword verifyPasswordDoesNotMatchErrorMessage() {
		int time = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, time)
			.withMessage("Test failed : Could not locate error message after \"" + time + "\" seconds")
			.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(this.errorMessage_xpath)));
		return this;
	}
	
	/**
	 * <h1>verifyPasswordDoesNotMatchErrorMessage</h1>
	 *<p> Method to verify Password does not match error message.
	 * 
	 * @return SetYourPassword
	 */
	public SetYourPassword verifyInvalidPasswordErrorMessage() {
		int time = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, time)
			.withMessage("Test failed : Could not locate error message after \"" + time + "\" seconds")
			.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(this.invalidPasswordError_xpath)));
		return this;
	}

	
	/**
	 * <h1>verifyCompanySetUpUrl</h1>
	 * <p>Method to verify company setup url
	 * 
	 * @return SetYourPassword
	 */
	public SetYourPassword verifyCompanySetUpUrl() {
		this.getNextButton().click();
		int time = DEFAULT_TIMEOUT * 3;
		new WebDriverWait(driver, time)
		.withMessage("Test Failed: Page did not navigate to \"" + Constants.supplierCompanySetupUrl + "\" after \"" + time + "\" seconds")
		.until(ExpectedConditions.urlToBe(Constants.supplierCompanySetupUrl));
		return this;
	}

	/**
	 * <h1>verifyViewNewPassword()<h1>
	 * <p>Method to verify the show password functionality for new password input field
	 * 
	 * @return SetYourPassword
	 * @throws ElementNotFoundException
	 */
	public SetYourPassword verifyViewNewPassword() throws ElementNotFoundException {
		resetShowHideNewPassword();
		getShowHideNewPassword().click();
		inputValue(getSetPasswordInput(), testPassword);
		assertEquals(getShowHideNewPassword().getAttribute("class"), "mat-icon-button mat-button-base active");
		assertEquals(getSetPasswordInput().getAttribute("type"), "text");
		return this;
	}
	
	/**
	 * <h1>verifyViewConfirmPassword()<h1>
	 * <p>Method to verify the show password functionality for confirm password input field
	 * 
	 * @return SetYourPassword
	 * @throws ElementNotFoundException
	 */
	
	public SetYourPassword verifyViewConfirmPassword() throws ElementNotFoundException { 
		resetShowHideConfirmPasswordIcon();
		getShowHideConfirmPassword().click();
		inputValue(getConfirmPasswordInput(), testPassword);
		assertEquals(getShowHideConfirmPassword().getAttribute("class"), "mat-icon-button mat-button-base active");
		assertEquals(getConfirmPasswordInput().getAttribute("type"), "text");
		return this;
	}

	/**
	 * <h1>verifyHideNewPassword() </h1>
	 * <p>Method to verify the hide password functionality for new password input field
	 * 
	 * @return SetYourPassword
	 * @throws ElementNotFoundException
	 */
	public SetYourPassword verifyHideNewPassword() throws ElementNotFoundException {
		resetShowHideNewPassword();
		inputValue(getSetPasswordInput(), testPassword);
		assertEquals(getShowHideNewPassword().getAttribute("class"), "mat-icon-button mat-button-base");
		assertEquals(getSetPasswordInput().getAttribute("type"), "password");
		return this;
	}
	
	/**
	 * <h1>verifyHideConfirmPassword()</h1>
	 * <p>Method to verify the hide password functionality for confirm password input field
	 * 
	 * @return SetYourPassword
	 * @throws ElementNotFoundException
	 */
	public SetYourPassword verifyHideConfirmPassword() throws ElementNotFoundException {
		resetShowHideConfirmPasswordIcon();
		inputValue(getConfirmPasswordInput(), testPassword);
		assertEquals(getShowHideConfirmPassword().getAttribute("class"), "mat-icon-button mat-button-base");
		assertEquals(getConfirmPasswordInput().getAttribute("type"), "password");
		return this;
	}
	
	/**
	 * <h1>enterPassword</h1>
	 * <p>Method to enter password into password field
	 * 
	 * @param password
	 */
	public void enterPassword(String password) {
		getSetPasswordInput().clear();
		inputValue(getSetPasswordInput(), password!= null? password.trim(): password);
		getSetPasswordInput().sendKeys(Keys.TAB);
	}
	
	/**
	 * <h1>verifyMessageIsHighlighted</h1>
	 * <p>Method to verify the highlighted message field
	 * 
	 * @param message
	 */
	
	public void verifyMessageIsHighlighted(String message) {
		int time = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, time)
			.withMessage("Password Requirement is not highlighted after \"" + time + "\" seconds")
			.until(ExpectedConditions.attributeContains(getPasswordReqMessage(message), "class", "success"));
		
	}
	
	/**
	  * <h1>verifyMessageIsNotHighlighted</h1>
	 * <p>Method to verify the message field is not highlighted
	 * 
	 * @param message
	 */
	public void verifyMessageIsNotHighlighted(String message) {
		int time = DEFAULT_TIMEOUT;
		new WebDriverWait(driver, time)
			.withMessage("Password Requirement is highlighted after \"" + time + "\" seconds")
			.until(ExpectedConditions.not(ExpectedConditions.attributeContains(getPasswordReqMessage(message), "class", "success")));
		
	}
	
}
