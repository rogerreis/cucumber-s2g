package stepDefinitions.bidSyncPro.supplier.companySettings;

import core.CoreAutomation;
import cucumber.api.java.en.Given;
import pages.bidSyncPro.supplier.common.CommonTopBar;
import pages.bidSyncPro.supplier.dashboard.DashboardCommon;
import pages.common.helpers.HelperMethods;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SupplierAdmin_stepDefinitions {

	public CoreAutomation automation;
	public EventFiringWebDriver driver;
	public DashboardCommon dashboardCommon;
	public CommonTopBar dashboardTopBar;
	public HelperMethods helperMethods;

	public SupplierAdmin_stepDefinitions(CoreAutomation automation) throws Throwable {
		this.automation = automation;
		this.driver = automation.getDriver();
		dashboardCommon = new DashboardCommon(driver);
		dashboardTopBar = new CommonTopBar(driver);
		helperMethods = new HelperMethods();

	}

	/* ------------ Test Steps ---------------- */

	@Given("^I have clicked on Manage Subscriptions$")
	public void iHaveClickedOnManageSubscriptions() throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver, 10, 200);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("// mat-card-content[@class='mat-card-content']//button[@color='primary']")));
		driver.findElement(By.xpath("//span[contains(text(),'Manage Subscriptions')]")).click();		
	}
}
