package stepDefinitions.bidSyncPro.supplier.login;

import org.openqa.selenium.support.events.EventFiringWebDriver;

import core.CoreAutomation;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.bidSyncPro.supplier.login.SupplierLogin;
import pages.bidSyncPro.supplier.login.ThreeLevelSecureAccess;
import pages.common.helpers.GlobalVariables;

public class ThreeLevelSecureAccess_stepDefinitions {
	
	protected CoreAutomation automation;
	protected EventFiringWebDriver driver;

	public ThreeLevelSecureAccess_stepDefinitions (CoreAutomation automation) {
		this.automation = automation;
		this.driver     = automation.getDriver();
	}
	
	
	@Given ("^I am on the login screen$")
	public void I_am_on_the_login_screen() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Navigating to login screen");

		new SupplierLogin(driver).navigateToHere();
	}

	@When ("^I check the Remember Me Checkbox$")
	public void  I_check_the_Remember_Me_Checkbox() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Checking \"Remember Me\" checkbox");
		
		new SupplierLogin(driver).checkRememberMeCheckbox();
	}

	@When ("^I uncheck the Remember Me Checkbox$")
	public void I_uncheck_the_Remember_Me_Checkbox() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Unchecking \"Remember Me\" checkbox");
		
		new SupplierLogin(driver).uncheckRememberMeCheckbox();
	}

	@Then ("^I will verify that the checkbox is selected$")
	public void I_will_verify_that_the_checkbox_is_selected() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying \"Remember Me\" checkbox is checked");

		new SupplierLogin(driver).verifyRememberMeCheckboxIsChecked();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: \"Remember Me\" checkbox is checked");
	}

	@Then ("^I will verify that the checkbox is unselected$")
	public void I_will_verify_that_the_checkbox_is_unselected() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying \"Remember Me\" checkbox is unchecked");
		new SupplierLogin(driver).verifyRememberMeCheckboxIsUnchecked();
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: \"Remember Me\" checkbox is unchecked");
	}

	/* --------------------------- */
	
	@Given ("^I have selected Remember Me and logged into Bidsync Pro$")
	public void I_have_selected_Remember_Me_and_logged_into_Bidsync_Pro() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Selecting \"Remember Me\" checkbox and logging into BidSync Pro");
		
		// Note: Token will only remain in local storage for the duration of a test.
		//       As soon as the test ends, the token is deleted
		
		// 1. Navigate to the login screen
		// 2. Now click on "Remember me" checkbox
		// 3. Log into ARGO
		new SupplierLogin(driver).navigateToHere()
				.setThisUserFromAccountReference("National FMC")
				.checkRememberMeCheckbox()
				.loginAsUser(SupplierLogin.thisUser.getAccountReference())
				.waitForPageLoad();
		
	}

	@When ("^I have closed my current browser window, waited 30 minutes, and navigated directly to the Bidsync Dashboard in a new browser window$")
	public void I_have_closed_my_current_browser_window_waited_30_minutes_and_navigated_directly_to_the_Bidsync_Dashboard_in_a_new_browser_window(){
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Closing browser, waiting 30 minutes, and then navigating directly to BidSync Dashboard in new browser window");
		
		// 1. Close browser window
		// 2. Wait 30 minutes TODO: update when complete. Currently deleting token to simulate timeout
		// 3. Navigate directly to BidSync Dashboard
		new ThreeLevelSecureAccess(driver)
				.closeBrowserWindow()
				.waitForTokenToExpireFromLoginScreen()
				.navigateToDashboardNewForYou();
	}

	@Then ("^I will verify that I do not need to provide credentials to view each of the public and privileged pages in BidSync Pro$")
	public void I_will_verify_that_I_do_not_need_to_provide_credentials_to_view_each_of_the_public_and_privileged_pages_in_BidSync_Pro(){
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying all public and priviledged pages can be viewed without providing credentials");
		new ThreeLevelSecureAccess(driver).verifyCredentialsNotRequiredForPublicPages()
				.verifyCredentialsNotRequiredForPrivilegedPages();

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Credentials are not required to view all public and privileged pages");
	}
	
	/* --------------------------- */
	
	@Then ("^I will verify that I need to provide credentials to view each of the secure pages in BidSync Pro$")
	public void I_will_verify_that_I_need_to_provide_credentials_to_view_each_of_the_secure_pages_in_BidSync_Pro() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying credentials are required to view each secure page in BidSync Pro");

		new ThreeLevelSecureAccess(driver).verifyCredentialsRequiredForSecurePages();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Credentials are required to view all secure pages");
	}

    @When ("^I provide credentials to one of the secure pages$")
    public void I_provide_credentials_for_a_randomly_selected_secure_page(){
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Randomly selecting a secure page, navigating to it, and then logging in");

		// Note: After providing credentials for one privileged page, user can access ALL of the secure pages
		new ThreeLevelSecureAccess(driver).randomlySelectSecurePageAndNavigateThere()
				.provideLoginCredentials();
    }

    @Then ("^I will verify that I do not need to provide credentials to view each of the secure pages in BidSync Pro$")
    public void I_will_verify_that_I_do_not_need_to_provide_credentials_to_view_each_of_the_secure_pages_in_BidSync_Pro() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying credentials are not required to view all secure pages");

		new ThreeLevelSecureAccess(driver).verifyCredentialsNotRequiredForSecurePages();
		// TODO: Here, need to add the manage subscriptions->billing and Saved Searches

		System.out.println(GlobalVariables.getTestID() + " Test Passed: Credentials are not required to view all secure pages");
    }
    
	/* --------------------------- */

    @Then ("^I will verify that I am not required to provide credentials to view each page in BidSync Pro$")
    public void I_will_verify_that_I_am_not_required_to_provide_credentials_to_view_each_page_in_BidSync_Pro() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying credentials are not required to view all public, priviledged, and secure pages");

		new ThreeLevelSecureAccess(driver).verifyCredentialsNotRequiredForPublicPages()
				.verifyCredentialsNotRequiredForPrivilegedPages()
				.verifyCredentialsNotRequiredForSecurePages();
    	
		// TODO: Here, need to add the manage subscriptions->billing and Saved Searches
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Credentials are not required to view all pages BidSync Pro");
    }

	/* --------------------------- */
    
    @When ("^I have waited 30 minutes for my token to expire$")
    public void I_have_waited_30_minutes_for_my_token_to_expire() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Waiting 30 minutes for token to expire");
		new ThreeLevelSecureAccess(driver).waitForTokenToExpire();
    }

	@Then ("^I will verify in the browser local storage that Remember Me is set to 30 days from now$")
	public void I_will_verify_in_the_browser_local_storage_that_Remember_Me_is_set_to_30_days_from_now() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying Remember Me is set to 30 days from now in the browser local storage");

		new ThreeLevelSecureAccess(driver).verifyRememberMeSetTo30DaysFromNowInBrowserLocalStorage();
		
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Remember Me is set to 30 days from now in the browser local storage");
	}

	@When ("^I change my timestamp for partialTokenExpiration in local storage to be one second behind the current time$")
	public void I_change_my_timestamp_for_partialTokenExpiration_in_local_storage_to_be_one_second_behind_the_current_time() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Changing timestamp for partialTokenExpiration in local storage to be one second in the past");
		
		new ThreeLevelSecureAccess(driver).changePartialTokenExpirationInLocalStorageSecondsFromNow(-1);
	}

	@Then ("^I will verify that Remember Me has expired because I need to provide credentials to view each of the privileged pages in BidSync Pro$")
	public void I_will_verify_that_Remember_Me_has_expired_because_I_need_to_provide_credentials_to_view_each_of_the_privileged_pages_in_BidSync_Pro() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying credentials are required to all priviledged pages in BidSync Pro");
		
		new ThreeLevelSecureAccess(driver).verifyCredentialsRequiredForPrivilegedPages();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: Credentials are required to view all privileged pages in BidSync Pro");
	}
	
	/* --------------------------- */

	@Then ("^I will verify that I do not have rememberMe or partialToken variables set in the local storage$")
	public void I_will_verify_that_I_do_not_have_rememberMe_or_partialToken_variables_set_in_the_local_storage() {
		System.out.println(GlobalVariables.getTestID() + " ---TEST STEP: Verifying user who is not Remember Me does not see rememberMe set or partialToken variables");
		
		new ThreeLevelSecureAccess(driver).verifyUserWhoIsNotRememberMe();
		System.out.println(GlobalVariables.getTestID() + " Test Passed: User who is not Remember Me does not see rememberMe set or partialToken variables");
	}
}
